<?php
/**
 * Main Lucid+ file. Can be loaded as a web application
 * or as a console app / CLI mode.
 *
 * Lucid+ loader.
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
if (file_exists(dirname(__FILE__).'/install/index.php')) {
    include_once(dirname(__FILE__).'/install/index.php');
    die;
}else {
    global $boot;
    include_once(dirname(__FILE__).'/boot/boot.php');
    $boot = new Boot();
}
?>
