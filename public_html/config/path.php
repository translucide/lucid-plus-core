<?php
//Defines the location of the folder containing available site profiles
//Previously it was defined as "../../private" (3 x dirname(__FILE)) for cPanel based hosting
//With other hosting setups, it might be everywhere up from current directory
$currentDirectory = dirname(__FILE__);
$i = 0;
while ($i < 10 && !is_dir($currentDirectory.'/private')) {
    $currentDirectory = dirname($currentDirectory);
    $i++;
}
define("SITESROOT",$currentDirectory.'/private/');
?>
