/**
 * lucidSlidesToggle class
 *
 * Links a nav element to a slides list
 * 
 * @class SlidesToggle
 * @public
 */
(function( $ ) {	
	var methods = {
		init: function(options) {
			var $this = $(this);
			$this.data('options', $.extend({
				slidesselector: 'section.slide.toggle',
				navselector: 'nav li>a.remote-toggle'
			}, options));
			$this.addClass('lucidslidestoggle');
            $this.lucidSlidesToggle("setup");
			return this;
		},
        setup: function() {
			var $this = $(this);
			var opt = $this.data('options');
			if(opt) {
				$this.find(opt.navselector).click(function(elclick){
					var href = $(this).attr("href");
					$this.find(opt.slidesselector).each(function(i,e){
						var el = $(e);
						if (!el.hasClass('active')) el.hide();
						else el.fadeOut().removeClass('active').hide();
					});
					$this.find(opt.slidesselector+href).addClass('active').fadeIn();
					elclick.preventDefault();
				});				
			}
        }
	};
	$.fn.lucidSlidesToggle = function(method) { 
		if (methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.lucidSlidesToggle' );
		}    
	};	
})(jQuery);
 
 /*
  * Lazy loading snippets
  */
$( document ).ready(function() {
    $('.lucidslidestoggle').lucidSlidesToggle();
});