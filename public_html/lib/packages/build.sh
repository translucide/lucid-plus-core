#!/bin/bash

###############################################################################
# Build "form" package...
###############################################################################
cd ../vendor/bootstrap-chosen-master/
lessc bootstrap-chosen.less > bootstrap-chosen.css
cd ../../packages
uglifyjs \
    ../vendor/chosen/chosen.jquery.min.js \
    ../vendor/datepicker/js/bootstrap-datepicker.js \
    ../vendor/dropzone/3.10.2/dropzone.min.js \
    ../vendor/fineuploader/jquery-fineuploader-3-3-0-min.js \
    ../vendor/lucideditor/0.1/js/lucideditor.js \
    ../vendor/spectrum/1.5.1/spectrum.js \
    ../vendor/tokens/0.6.0/tokens.js \
    ../vendor/ajaxform.js \
    -o ./form/form.js
uglifycss \
    ../vendor/chosen/chosen.min.css \
    ../vendor/bootstrap-chosen-master/bootstrap-chosen.css \
    ../vendor/datepicker/css/datepicker3.css \
    ../vendor/dropzone/3.10.2/css/dropzone.css \
    ../vendor/fineuploader/fineuploader-3.3.0.css \
    ../vendor/lucideditor/0.1/css/lucideditor.css \
    ../vendor/spectrum/1.5.1/spectrum.css \
    ../vendor/tokens/0.6.0/tokens.css \
    > ./form/form.css
cp ../vendor/chosen/chosen*.png ./form
cp -R ../vendor/datepicker/js/locales ./form
cp -R ../vendor/dropzone/3.10.2/images ./form/images
cp ../vendor/fineuploader/*.gif ./form
#sed -i 's/chosen-sprite.png/lib\/packages\/form\/chosen-sprite.png/g' form/form.css
