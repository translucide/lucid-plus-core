/*
 * arrive.js
 * v2.3.1
 * https://github.com/uzairfarooq/arrive
 * MIT licensed
 *
 * Copyright (c) 2014-2016 Uzair Farooq
 */
var Arrive=function(a,b,c){"use strict";function l(a,b,c){e.addMethod(b,c,a.unbindEvent),e.addMethod(b,c,a.unbindEventWithSelectorOrCallback),e.addMethod(b,c,a.unbindEventWithSelectorAndCallback)}function m(a){a.arrive=j.bindEvent,l(j,a,"unbindArrive"),a.leave=k.bindEvent,l(k,a,"unbindLeave")}if(a.MutationObserver&&"undefined"!=typeof HTMLElement){var d=0,e=function(){var b=HTMLElement.prototype.matches||HTMLElement.prototype.webkitMatchesSelector||HTMLElement.prototype.mozMatchesSelector||HTMLElement.prototype.msMatchesSelector;return{matchesSelector:function(a,c){return a instanceof HTMLElement&&b.call(a,c)},addMethod:function(a,b,c){var d=a[b];a[b]=function(){return c.length==arguments.length?c.apply(this,arguments):"function"==typeof d?d.apply(this,arguments):void 0}},callCallbacks:function(a){for(var c,b=0;c=a[b];b++)c.callback.call(c.elem)},checkChildNodesRecursively:function(a,b,c,d){for(var g,f=0;g=a[f];f++)c(g,b,d)&&d.push({callback:b.callback,elem:g}),g.childNodes.length>0&&e.checkChildNodesRecursively(g.childNodes,b,c,d)},mergeArrays:function(a,b){var d,c={};for(d in a)c[d]=a[d];for(d in b)c[d]=b[d];return c},toElementsArray:function(b){return"undefined"==typeof b||"number"==typeof b.length&&b!==a||(b=[b]),b}}}(),f=function(){var a=function(){this._eventsBucket=[],this._beforeAdding=null,this._beforeRemoving=null};return a.prototype.addEvent=function(a,b,c,d){var e={target:a,selector:b,options:c,callback:d,firedElems:[]};return this._beforeAdding&&this._beforeAdding(e),this._eventsBucket.push(e),e},a.prototype.removeEvent=function(a){for(var c,b=this._eventsBucket.length-1;c=this._eventsBucket[b];b--)a(c)&&(this._beforeRemoving&&this._beforeRemoving(c),this._eventsBucket.splice(b,1))},a.prototype.beforeAdding=function(a){this._beforeAdding=a},a.prototype.beforeRemoving=function(a){this._beforeRemoving=a},a}(),g=function(b,d){var g=new f,h=this,i={fireOnAttributesModification:!1};return g.beforeAdding(function(c){var i,e=c.target;c.selector,c.callback;(e===a.document||e===a)&&(e=document.getElementsByTagName("html")[0]),i=new MutationObserver(function(a){d.call(this,a,c)});var j=b(c.options);i.observe(e,j),c.observer=i,c.me=h}),g.beforeRemoving(function(a){a.observer.disconnect()}),this.bindEvent=function(a,b,c){b=e.mergeArrays(i,b);for(var d=e.toElementsArray(this),f=0;f<d.length;f++)g.addEvent(d[f],a,b,c)},this.unbindEvent=function(){var a=e.toElementsArray(this);g.removeEvent(function(b){for(var d=0;d<a.length;d++)if(this===c||b.target===a[d])return!0;return!1})},this.unbindEventWithSelectorOrCallback=function(a){var f,b=e.toElementsArray(this),d=a;f="function"==typeof a?function(a){for(var e=0;e<b.length;e++)if((this===c||a.target===b[e])&&a.callback===d)return!0;return!1}:function(d){for(var e=0;e<b.length;e++)if((this===c||d.target===b[e])&&d.selector===a)return!0;return!1},g.removeEvent(f)},this.unbindEventWithSelectorAndCallback=function(a,b){var d=e.toElementsArray(this);g.removeEvent(function(e){for(var f=0;f<d.length;f++)if((this===c||e.target===d[f])&&e.selector===a&&e.callback===b)return!0;return!1})},this},h=function(){function h(a){var b={attributes:!1,childList:!0,subtree:!0};return a.fireOnAttributesModification&&(b.attributes=!0),b}function i(a,b){a.forEach(function(a){var c=a.addedNodes,d=a.target,f=[];null!==c&&c.length>0?e.checkChildNodesRecursively(c,b,k,f):"attributes"===a.type&&k(d,b,f)&&f.push({callback:b.callback,elem:node}),e.callCallbacks(f)})}function k(a,b,f){if(e.matchesSelector(a,b.selector)&&(a._id===c&&(a._id=d++),-1==b.firedElems.indexOf(a._id))){if(b.options.onceOnly){if(0!==b.firedElems.length)return;b.me.unbindEventWithSelectorAndCallback.call(b.target,b.selector,b.callback)}b.firedElems.push(a._id),f.push({callback:b.callback,elem:a})}}var f={fireOnAttributesModification:!1,onceOnly:!1,existing:!1};j=new g(h,i);var l=j.bindEvent;return j.bindEvent=function(a,b,c){"undefined"==typeof c?(c=b,b=f):b=e.mergeArrays(f,b);var d=e.toElementsArray(this);if(b.existing){for(var g=[],h=0;h<d.length;h++)for(var i=d[h].querySelectorAll(a),j=0;j<i.length;j++)g.push({callback:c,elem:i[j]});if(b.onceOnly&&g.length)return c.call(g[0].elem);setTimeout(e.callCallbacks,1,g)}l.call(this,a,b,c)},j},i=function(){function d(a){var b={childList:!0,subtree:!0};return b}function f(a,b){a.forEach(function(a){var c=a.removedNodes,f=(a.target,[]);null!==c&&c.length>0&&e.checkChildNodesRecursively(c,b,h,f),e.callCallbacks(f)})}function h(a,b){return e.matchesSelector(a,b.selector)}var c={};k=new g(d,f);var i=k.bindEvent;return k.bindEvent=function(a,b,d){"undefined"==typeof d?(d=b,b=c):b=e.mergeArrays(c,b),i.call(this,a,b,d)},k},j=new h,k=new i;b&&m(b.fn),m(HTMLElement.prototype),m(NodeList.prototype),m(HTMLCollection.prototype),m(HTMLDocument.prototype),m(Window.prototype);var n={};return l(j,n,"unbindAllArrive"),l(k,n,"unbindAllLeave"),n}}(window,"undefined"==typeof jQuery?null:jQuery,void 0);

/**
* jquery.matchHeight-min.js v0.5.2
* http://brm.io/jquery-match-height/
* License: MIT
*/
(function(b){b.fn.matchHeight=function(a){if("remove"===a){var d=this;this.css("height","");b.each(b.fn.matchHeight._groups,function(b,a){a.elements=a.elements.not(d)});return this}if(1>=this.length)return this;a="undefined"!==typeof a?a:!0;b.fn.matchHeight._groups.push({elements:this,byRow:a});b.fn.matchHeight._apply(this,a);return this};b.fn.matchHeight._apply=function(a,d){var c=b(a),f=[c];d&&(c.css({display:"block","padding-top":"0","padding-bottom":"0","border-top-width":"0","border-bottom-width":"0",
height:"100px"}),f=k(c),c.css({display:"","padding-top":"","padding-bottom":"","border-top-width":"","border-bottom-width":"",height:""}));b.each(f,function(a,c){var d=b(c),f=0,e=d.parents().add(d).filter(":hidden");e.css({display:"block"});d.each(function(){var a=b(this);a.css({display:"block",height:""});a.outerHeight(!1)>f&&(f=a.outerHeight(!1));a.css({display:""})});e.css({display:""});d.each(function(){var a=b(this),c=0;"border-box"!==a.css("box-sizing")&&(c+=g(a.css("border-top-width"))+g(a.css("border-bottom-width")),
c+=g(a.css("padding-top"))+g(a.css("padding-bottom")));a.css("height",f-c)})});return this};b.fn.matchHeight._applyDataApi=function(){var a={};b("[data-match-height], [data-mh]").each(function(){var d=b(this),c=d.attr("data-match-height");a[c]=c in a?a[c].add(d):d});b.each(a,function(){this.matchHeight(!0)})};b.fn.matchHeight._groups=[];b.fn.matchHeight._throttle=80;var h=-1,e=-1;b.fn.matchHeight._update=function(a){if(a&&"resize"===a.type){a=b(window).width();if(a===h)return;h=a}-1===e&&(e=setTimeout(function(){b.each(b.fn.matchHeight._groups,
function(){b.fn.matchHeight._apply(this.elements,this.byRow)});e=-1},b.fn.matchHeight._throttle))};b(b.fn.matchHeight._applyDataApi);b(window).bind("load resize orientationchange",b.fn.matchHeight._update);var k=function(a){var d=null,c=[];b(a).each(function(){var a=b(this),e=a.offset().top-g(a.css("margin-top")),h=0<c.length?c[c.length-1]:null;null===h?c.push(a):1>=Math.floor(Math.abs(d-e))?c[c.length-1]=h.add(a):c.push(a);d=e});return c},g=function(a){return parseFloat(a)||0}})(jQuery);

/**
 * lucidCore
 *
 * Common lucid+ core functionnality, loaded as a jQuery plugin
 *
 * @class lucidCore
 * @public
 */
(function( $ ) {
	var methods = {
		init: function(options) {
			var $this = $(this);
			$this.data('options', $.extend({
                resetlogintimeout: 1
            }, options));
			$this.lucidCore("setup");
			return this;
		},
		setup: function(){
			var $this = $(this);
			var options = $this.data('options');
            $(document).ajaxComplete(function(event,xhr,options){
				$this.lucidCore("ajaxCompleteHandler",event,xhr,options);
			});
            options.timeout = setInterval(function(){
				$this.lucidCore("keepAlive");
			},1000*60);
            $this.data('options',options);
			return this;
		},
		keepAlive: function(){
			var $this = $(this);
			var options = $this.data('options');
            var callback = function(d){
				if (d.success) {
                    //If we are still connected, but less than 60 seconds, logout user.
                    if (!d.connected) {
                        clearInterval(options.timeout);
                    }
					if (d.connected && LucidConfig.connected && d.remaining < 60) {
                        LucidConfig.connected = false;
                        window.location = LucidConfig.logoutUrl;
					}
				}
			};
			$.post(LucidConfig.url+LucidConfig.lang+'/api/system/controller/session/keepalive',{
                'resetlogintimeout': options.resetlogintimeout
            },callback);
			return this;
		},

        /**
         * Adds a mixblendmode class to html tag.
         */
		testMixBlendMode: function (){
			if (Modernizr) {
				Modernizr.addTest('mix-blend-mode', function(){
					return Modernizr.testProp('mixBlendMode');
				});
				if (Modernizr.testProp('mixBlendMode')) {
					$(html).addClass("mixblendmode")
				}
			}
		},
		/**
		 * Binds to all ajax response call and load CSS & JS ressources
		 * needed by ajax call
		 *
		 * Also extends session on any ajax call other than a keepalive one.
		 */
		ajaxCompleteHandler: function(event,xhr,options){
			var $this = $(this);
            var options = $this.data('options');
			var ctype = xhr.getResponseHeader("content-type") || "";
			if (ctype.indexOf('json') > -1) {
				var response = JSON.parse(xhr.responseText);
				if (typeof response == 'object') {
					if (typeof response.ressources == 'object') {
						$this.lucidCore("loadRessources",response.ressources);
					}

                    //Make sure any ajax call other than a keepAlive call
                    //triggers a session logout timer reset.
                    if (typeof response.origin == 'object') {
                        if (response.origin.component == 'system' && response.origin.type == 'controller' &&
                            response.origin.name == 'session' && response.origin.op == 'keepalive' && response.connected){
                            options.resetlogintimeout = 0;
                        }else{
                            options.resetlogintimeout = 1;
                        }
                        $this.data('options', options);
                    }else{
                        options.resetlogintimeout = 1;
                    }
				}
			}
			return this;
		},

		/**
		 * Loads JS & CSS ressources returned by AJAX calls
		 *
		 * @public
		 * @memberOf lucidEditor
		 * @function
		 */
		loadRessources: function(ressources) {
			var $this = $(this);
			var code = '';
			if (typeof ressources != 'object') return;
			if (typeof ressources.urls == 'object') {
				if (typeof ressources.urls.js == 'object') {
					for (var i = 0; i < ressources.urls.js.length; i++){
                        console.info('lucidCore::loadRessources: '+ressources.urls.js[i].src);
						code += '<scrip'+'t language="javascript" type="text/javascript"';
						code += (ressources.urls.js[i].type != 'inline')?' src="'+ressources.urls.js[i].src+'"></scrip'+'t>':'>'+ressources.urls.js[i].src+'</scrip'+'t>';
					}
					if (code != '') $('body').append(code);

				}
				code = '';
				if (typeof ressources.urls.css == 'object') {
					for (var i = 0; i < ressources.urls.css.length; i++){
						console.info('lucidCore::loadRessources: '+ressources.urls.css[i].src);
						code += (ressources.urls.css[i].type != 'inline')?'<link href="'+ressources.urls.css[i].src+'" rel="stylesheet" type="text/css">':'<style>'+ressources.urls.css[i].src+'</style>';
					}
					$('head').append(code);
				}
			}
		}
	};
	$.fn.lucidCore = function(method) {
		if (methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.lucidCore' );
		}
	};
})(jQuery);

 /*
  * Lazy loading snippets
  */
$( document ).ready(function() {
    $(document).lucidCore();
});

/**
 * Old core/display/effects.js file
 */
$(document).ready(function() {
	if ($('.sticktotop').length > 0) {
		var dockedHeaderTop = $('.sticktotop').offset().top;
		var el = $('.sticktotop').eq(0);
		var updateSkipCounter;
		var stickToTopOnScroll = function(){
			if ($(window).scrollTop() > dockedHeaderTop) {
				el.addClass("docktotop");
				$('body').css('margin-top', el.outerHeight(true) + parseInt(el.css('marginBottom')));
			} else {
				el.removeClass("docktotop");
				$('body').css('margin-top', '0px');
			}
		}
		if ($('.sticktotop').length == 1 && $(window).width() >= 768) {
			var scrollTimer = null;
			$(window).scroll(function () {
				if (scrollTimer) {
					clearTimeout(scrollTimer);   // clear any previous pending timer
				}
				scrollTimer = setTimeout(stickToTopOnScroll, 10);   // set new timer
			});
		}
	}
});
