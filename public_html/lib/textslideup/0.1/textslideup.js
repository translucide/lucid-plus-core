/**
 * textSlideUp
 *
 * Common lucid+ core functionnality, loaded as a jQuery plugin
 *
 * @class textSlideUp
 * @public
 */
(function( $ ) {	
	var methods = {
		init: function(options) {
            var $this = $(this);
			if (!options) {
				options = new Array();
			}
			$this.data('options', $.extend({
				'updateinterval' : 100
			}, options));	
			$this.textSlideUp("update");
			var resizeTimer = null;
			$(window).resize(function(){
				if (resizeTimer ) {
					clearTimeout(resizeTimer );
				}
				resizeTimer = setTimeout($this.textSlideUp("update"), $this.data("options")["updateinterval"]);	
			})
			return this;
		},
		update: function(){
			var $this = $(this);
            var el = $this.find("section");
			var h1 = el.children("h1");
			var image = el.children(".image");
			el.css({"height":"auto","padding-top":0});
			h1.css({"height":"auto"});
			if ($(window).width() >= 768) {
				$this.textSlideUp("equalHeight",h1).textSlideUp("equalHeight",el);
			}			
			if ($(window).width() > 1023) {
				el.css({"height":"auto","padding-top":0});
				h1.css({"height":"auto"});
				setTimeout(function(){
					$this.textSlideUp("equalHeight",h1).textSlideUp("equalHeight",el);
					el.each(function(){
						var h1 = $(this).children("h1");
						var image = $(this).children(".image");
						var pt = $(this).eq(0).height() + parseInt($(this).eq(0).css("padding-top"))
							- image.eq(0).height()
							- parseInt(image.eq(0).css("margin-top")) - parseInt(image.eq(0).css("margin-bottom"))
							- h1.eq(0).height()
							- parseInt(h1.eq(0).css("margin-top")) - parseInt(h1.eq(0).css("margin-bottom"));
							
						$(this).css({"padding-top":(parseInt(pt)+1)+"px","overflow":"hidden"});
					})
				},200);
			}
			return this;
		},
		equalHeight: function(el){
			$(el).equalHeights();
			return this;
		}
	};
	$.fn.textSlideUp = function(method) { 
		if (methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.textSlideUp' );
		}    
	};	
})(jQuery);
 
 
/*!
 * Simple jQuery Equal Heights
 *
 * Copyright (c) 2013 Matt Banks
 * Dual licensed under the MIT and GPL licenses.
 * Uses the same license as jQuery, see:
 * http://docs.jquery.com/License
 *
 * @version 1.5.1
 */
(function($) {

    $.fn.equalHeights = function(options) {
        var maxHeight = 0,
            $this = $(this),
            equalHeightsFn = function() {
                var height = $(this).innerHeight();
    
                if ( height > maxHeight ) { maxHeight = height; }
            };
        options = options || {};

        $this.each(equalHeightsFn);

        if(options.wait) {
            var loop = setInterval(function() {
                if(maxHeight > 0) {
                    clearInterval(loop);
                    return $this.css('height', maxHeight);
                }
                $this.each(equalHeightsFn);
            }, 100);
        } else {
            return $this.css('height', maxHeight);
        }
    };

    // auto-initialize plugin
    $('[data-equal]').each(function(){
        var $this = $(this),
            target = $this.data('equal');
        $this.find(target).equalHeights();
    });

})(jQuery);
 
 /*
  * Lazy loading snippets
  */
$( document ).ready(function() {
	$("section.widget.textslideup").each(function(){
		$(this).textSlideUp();
	});
});