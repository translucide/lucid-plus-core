/**
 * Auto save queue. Fields that must be saved will be queued here,
 * and queue will be processed at a given rhythm to prevent
 * multiple saves or missed saves
 *
 * Usage: Include this file after defining the following variables in your code :
 * autosaveEventName = "autosave.lucidapp";
 * autosaveURL = '{url}en/api/component/controller/name/autosave';
 * autocreateURL = '{url}en/api/component/controller/name/autosave';
 * autocompleteURL = '{url}en/api/component/controller/name/autocomplete';
 * autocompleteBaseURL = '{url}en/api/component/controller/name/';
 * autocompleteLeaveFieldEventName = "leavefield.lucidapp";
 **/

autosaveEventName = "autosave.lucidapp";
autosaveURL = '{url}en/api/component/controller/name/autosave';
autocreateURL = '{url}en/api/component/controller/name/autosave';

autocompleteURL = '{url}en/api/component/controller/name/autocomplete';
autocompleteBaseURL = '{url}en/api/component/controller/name/';
autocompleteLeaveFieldEventName = "leavefield.lucidapp";

autosaveQueue = [];
autocreateQueue = [];

/**
 * Checks if an element is queued already
 *
 * @param Element el
 * @param string queueName 'autosave' or 'autocreate'
 * @return bool
 **/
function isQueued(el, queueName) {
    el = $(el);
    queue = (queueName == 'autosave') ? autosaveQueue : autocreateQueue;
    var found = false;
    for (var i = 0; i < queue.length; i++) {
        if (queue[i].fieldid == el.attr("id")) {
            //We have an item in queue already, lets update it before its sent
            queue[i].value = el.val();
            queue[i].millis = new Date().getTime();
            found = true;
        }
    }
    return found;
}

/**
 * Packs a number of items to send over to be created or saved.
 *
 * It also removes them from the given queue
 *
 * @param string queuename
 * @return Array queueItems
 **/

function getNextBatchFromQueue(queueName) {
    var sentItems = [];
    if ((queueName == 'autosave' && autosaveQueue.length > 0) || (queueName == 'autocreate' && autocreateQueue.length > 0)) {
        var millis = new Date().getTime();
        var queueItem = (queueName == 'autosave') ? autosaveQueue.shift() : autocreateQueue.shift();
        var items = 0;
        while (queueItem && items < 10) {
            if ($('#' + queueItem.fieldid).prev(".autocomplete:focus").length === 0 &&
                $('#' + queueItem.fieldid + ':focus').length === 0 &&
                millis - queueItem.millis >= 300) {
                sentItems.push(queueItem);
                queueItem = (queueName == 'autosave') ? autosaveQueue.shift() : autocreateQueue.shift();
                items++;
            } else {
                if (queueName == 'autosave') autosaveQueue.push(queueItem);
                else autocreateQueue.push(queueItem);
                break;
            }
        }
    }
    return sentItems;
}

function queueAutosaveItem(autosaveElement, isUrgent) {
    if (isUrgent) isUrgent = true;
    autosaveElement = $(autosaveElement);
    if (isQueued(autosaveElement, 'autosave') === false) {
        autosaveQueue.push({
            'millis': new Date().getTime() - ((isUrgent) ? 200 : 0),
            'fieldid': autosaveElement.attr("id"),
            'model': autosaveElement.attr("data-save-model"),
            'datatype': autosaveElement.attr("data-save-type"),
            'id': autosaveElement.attr("data-save-id"),
            'field': autosaveElement.attr("name"),
            'value': autosaveElement.val()
        });
    }
}

function processAutosaveQueue() {
    sentItems = getNextBatchFromQueue('autosave');
    if (sentItems.length > 0) {
        $.post(autosaveURL, {
            items: sentItems
        }, function(data) {
            if (data.success === true) {
                for (var i = 0; i < data.responsetriggers.length; i++) {
                    var el = $('[name="'+data.responsetriggers[i].field+'"][data-save-id="'+data.responsetriggers[i].id+'"]');
                    el.val(data.responsetriggers[i].value).trigger("change");
                    el.prev(".autocomplete").val(data.responsetriggers[i].label).trigger("change");
                }
                for (var i = 0; i < data.items.length; i++) {
                    data.items[i]['type'] = autosaveEventName;
                    var el = $('#' + data.items[i].fieldid).trigger(autosaveEventName,data.items[i]);
                }
                notify(data.msg, 'success');
            } else {
                //Put items back on queue since something went wrong.
                notify("Saving failed! I will retry in a couple of seconds for you !", 'error');
                autosaveQueue.push(sentItems);
            }
            console.info("autosave data saved successfully!");
            console.info(data);
        });
    }
    //Recall every 0.25 second.
    setTimeout(function() {
        processAutosaveQueue();
    }, 400);
}
//Start processing autosave 1.5sec after page load.
setTimeout(function() {
    processAutosaveQueue();
}, 1500);

function queueAutocreateItem(autocreateElement) {
    autocreateElement = $(autocreateElement);
    if (autocreateElement.attr("data-dirty") === "1" && isQueued(autocreateElement, 'autocreate') === false) {
        autocreateQueue.push({
            'millis': new Date().getTime(),
            'fieldid': autocreateElement.attr("id"),
            'model': autocreateElement.attr("data-autocomplete-model"),
            'titlefield': autocreateElement.attr('data-autocomplete-titlefield'),
            'valuefield': autocreateElement.attr('data-autocomplete-valuefield'),
            'field': autocreateElement.attr("name"),
            'value': autocreateElement.val()
        });
    }
}

function processAutocreateQueue() {
    sentItems = getNextBatchFromQueue('autocreate');
    if (sentItems.length > 0) {
        $.post(autocreateURL, {
            items: sentItems
        }, function(data) {
            if (data.success) {
                for (var i = 0; i < data.items.length; i++) {
                    var el = $('#' + data.items[i].fieldid);
                    el.attr("data-dirty", "0");
                    el.next(".autosave").val(data.items[i].value);
                    queueAutosaveItem(el.next(".autosave"), true); //Queue items as urgent autosave
                }
            } else {
                //Put items back on queue since something went wrong.
                notify("Saving failed! I will retry in a couple of seconds for you !", 'error');
                autocreateQueue.push(sentItems);
            }
            console.info("autocreate data created successfully!");
            console.info(data);
        });
    }
    //Recall every X second.
    setTimeout(function() {
        processAutocreateQueue();
    }, 100);
}
//Start processing autosave 1.5sec after page load.
setTimeout(function() {
    processAutocreateQueue();
}, 1500);

/**
 * Autosaves an autosave element
 *
 * Autosaves handles queuing the element and waiting to see if any changes occur before sending
 * them all to the backend.
 *
 * @param Element el
 **/
function autosave(el) {
    if ($(el).hasClass("autocomplete")) {
        el = el.next(".autosave");
    }
    if (el.attr("data-prevval") != el.val() && el.attr("data-save-model")) {
        var urgent = (el.attr("data-save-urgent") && el.attr("data-save-urgent") == "1")?true:false;
        queueAutosaveItem(el,urgent);
        el.attr("data-prevval", el.val());
    }else {
        return;
    }
}

/**
 * Autocreates a new list element. No need to call autosave after element creation
 * as it will be automatically queued for save after it is created.
 *
 * @access public
 * @param Element el The autocomplete element that needs to create its value from its current text value (title).
 **/
function autocreate(el) {
    if ($(el).hasClass("autocomplete")) {
        queueAutocreateItem(el);
    }
}

var autoComplete_OnClick = function(){
    $(this).attr("data-dirty",0);
    $(this).select();
};
var autoComplete_OnBlur = function() {
    console.info("blur event");
    var field = $(this);

    setTimeout(function(){
        if (field.attr('data-dirty') == "1") {
            if (field.attr("data-autocreate") === 'true') {
                autocreate(field);
            } else {
                //When data is not valid and field value is not empty, replace value with previous value.
                if (field.val() != "") {
                    if (field.attr("data-prevval") != '') field.val(field.attr("data-prevval"));
                    else field.val("");
                }
                //When data is not valid and field is empty,
                else{
                    field.next(".autosave").val("0");
                    autosave(field.next(".autosave"));
                    field.attr('data-dirty',0).blur();
                }
            }
        } else {
            //Only trigger change if data changed.
            if (field.attr("data-prevval") != field.val()) {
                  field.next(".autosave").trigger("change");
            }
            autosave(field.next(".autosave"));
        }
        /**
         * Triggers a leavefield.moneyqube event
         **/
        $(field).trigger({
            type: autocompleteLeaveFieldEventName,
            target: field
        });
    },150);
};
var currentAutoCompleteField = false;

/**
 * Activates autocomplete pulldowns
 **/
var enableAutocomplete = function(){
    $('#' + $(this).attr("id") + ' input.autocomplete').each(function() {
        var el = $(this);
        var serviceUrl = autocompleteURL;
        if (el.attr("data-autocomplete-api")) {
            serviceUrl = autocompleteBaseURL + el.attr("data-autocomplete-api");
        }
        el.attr("data-dirty", "0");
        el.off('keydown').on('keydown', function() {
            if (event.which == 13) {
                if (el.val() != "" && el.attr("data-dirty") == "1") {
                    event.preventDefault();
                    event.stopPropagation();
                    el.blur();
                }
            } else if (event.which == 9 && !event.shiftKey) {
                if (el.val() != "" && el.attr("data-dirty") == "1") {
                    event.preventDefault();
                    el.blur();
                }
            } else {
                $(this).attr("data-dirty", '1');
            }
        }).off('blur', autoComplete_OnBlur).on('blur', autoComplete_OnBlur).autocomplete({
            'serviceUrl': serviceUrl,
            'minChars': 0,
            'lookupLimit': 100,
            'orientation': 'auto',
            'groupBy': 'category',
            'noCache': true,
            "width": (el.attr('data-autocomplete-width'))?el.attr('data-autocomplete-width'):"content",
            'params': {
                "screen": (el.closest("table.dataTable").attr("id"))?el.closest("table.dataTable").attr("id"):el.closest('[data-screen]').attr("data-screen"),
                'model': el.attr('data-autocomplete-model'),
                'titlefield': el.attr('data-autocomplete-titlefield'),
                'valuefield': el.attr('data-autocomplete-valuefield'),
                'recordtype': el.attr('data-autocomplete-type'),
                'id': el.next('.autosave').attr('data-save-id'),
                'filters': function(param) {
                    if (!param) param = el;
                    if(!el.attr('data-autocomplete-ignorefilter') || !el.attr('data-autocomplete-filtercolumn')){
                        if (el.attr('data-autocomplete-filtercolumn').split('|').length == 1) {
                            return el.attr('data-autocomplete-filtercolumn');
                        } else {
                            // it is an array
                            columnArray = el.attr('data-autocomplete-filtercolumn').split('|');
                            valueArray = el.attr('data-autocomplete-filtervalue').split('|');
                            resultArray = new Array();
                            for (var i = columnArray.length - 1; i >= 0; i--) {
                                if (columnArray[i]) {
                                    var filterValue = 0;
                                    var filterEl = param.closest('tr').find('input[name=' + valueArray[i] + ']');
                                    //Look for filter on the row first.
                                    if (filterEl.length) filterValue = filterEl.val();
                                    else {
                                        //Filter value was not found, try to find it in the details form
                                        filterEl = param.closest('tr').next().not("[role='row']").find('form input[name=' + valueArray[i] + ']');
                                        if (!filterEl.length) {
                                            //Still cant find filter, look for it in the dataTables row data.
                                            var dt = param.closest('table.dataTable').DataTable();
                                            console.info(dt);
                                            var dtData = dt.row(findRow(param.closest('tr'))).data();
                                            if (dtData && typeof dtData[valueArray[i]] != undefined) {
                                                filterValue = dtData[valueArray[i]];
                                            }
                                            else {
                                                filterEl = param.closest('form').find('input[name=' + valueArray[i] + ']');
                                                if (filterEl.length) {
                                                    filterValue = filterEl.val();
                                                }else console.info("Could not find "+columnArray[i]+" filter by looking for a field named " + valueArray[i]);
                                            }
                                        }
                                        else {
                                            filterValue = filterEl.val();
                                        }
                                    }
                                    resultArray.push({
                                        'column': columnArray[i],
                                        'value': filterValue
                                    });
                                }
                            }
                            return JSON.stringify(resultArray);
                        }
                    }
                }
            },
            onSelect: function(suggestion) {
                console.info("onSelect");
                var dirty = el.attr("data-dirty");
                el.attr("data-dirty", '0');
                //Use value as field value when both label and value are not the same
                if (suggestion.value && suggestion.label && suggestion.label != suggestion.value) {
                    el.attr("data-prevval", suggestion.label);
                    el.val(suggestion.label);
                }else {
                    el.attr("data-prevval", suggestion.value);
                    el.val(suggestion.value);
                }
                if(el.attr('data-autocomplete-ignorefilter')) el.attr('data-autocomplete-ignorefilter', '0');
                var hiddenElement = el.next('.autosave');
                if (suggestion.hasOwnProperty('data') && suggestion.data.hasOwnProperty('data')) {
                    hiddenElement.val(suggestion.data.data);
                    hiddenElement.attr("data-selected-id",suggestion.id);
                    hiddenElement.trigger("change");
                }
                else {
                    hiddenElement.val(suggestion.data);
                    hiddenElement.attr("data-selected-id",suggestion.id);
                    hiddenElement.trigger("change");
                }
                var func = el.attr("data-on-select");
                if (func && typeof window[func] == 'function') window[func](hiddenElement);
                autosave(hiddenElement);
            },
            onSearchStart: function(params) {
                currentAutoCompleteField = $(this);
                // set the query to empty on starting the search if the control says so
                if (($(this).attr('data-dirty') == 0) && ($(this).attr('data-autocomplete-pulldownwhennotempty') == 1)) {
                    // send empty query to simulate pulldown.
                    params.query = '';
                }
                if (($(this).val()) && ($(this).attr('data-dirty') == 0) && ($(this).attr('data-autocomplete-pulldownwhennotempty') != 1)) {
                    // garble the query to avoid results.
                    params.query = '@#$@#^';
                }
            },
            onInvalidateSelection: function() {
                var input = el;
                if (input.attr("data-dirty") == "0") {
                    input.attr("data-dirty", '1');
                }
            }
        }).off('click',autoComplete_OnClick)
        .on('click', autoComplete_OnClick);
    });
    $('#' + $(this).attr("id") + ' .autosave').not("select").not("[type='hidden']").on('blur', function() {
        var hiddenElement = $(this);
        autosave(hiddenElement);
    });
    $('#' + $(this).attr("id") + ' select.autosave, #' + $(this).attr("id") + ' input.autosave[type="hidden"]').on('change', function() {
        var hiddenElement = $(this);
        autosave(hiddenElement);
    });
};

function enableAutosave(datatableId) {
    $('#' + datatableId).on('draw.dt newrow.dt', enableAutocomplete);
    if ($('#' + datatableId + '.dataTable').length < 1) {
        $('#' + datatableId + ' .autosave').not("select").not("[type='hidden']").on('blur', function() {
            var hiddenElement = $(this);
            autosave(hiddenElement);
        });
        $('#' + datatableId + ' select.autosave, #' + datatableId + ' input.autosave[type="hidden"]').on('change', function() {
            var hiddenElement = $(this);
            autosave(hiddenElement);
        });
    }
}

/**
 * App notification in bottom corner
 **/
function notify(msg, cls) {
   var n = $("#notify");
   if (n.length === 0) {
      $("body").append("<div id=\"notify\" class=\"notify\"></div>");
   }
   if (cls == 'success') {
      $('#notify').removeClass("error").addClass("success");
      msg = '<i class="glyphicon glyphicon-ok-sign"></i>' + msg;
   }
   if (cls == 'error') {
      $('#notify').removeClass("success").addClass("error");
      msg = '<i class="glyphicon glyphicon-remove-sign"></i>' + msg;
   }
   $('#notify').html(msg).fadeIn(100);
   setTimeout(function() {
      $('#notify').fadeOut('fast');
   }, 1000);
}
