/**
 * lucidDatatableRenderer class
 *
 * A collection of renderers, used mainly for DataTables cells inputs
 *
 * Usage:
 * $('#datatable').lucidDatatableRenderer();
 *
 * @class lucidDatatableRenderer
 * @public
 */
(function($) {
    $.fn.lucidDatatableRenderer = function(options) {
        var settings = jQuery.extend({}, options);
        var $jquery = this;
        var methods = {
            /**
             * Renders an autocomplete pulldown field
             * @param  string label Label
             * @param  string value Value
             * @param  object options
             */
            'getAutocompleteInput': function(options) {
                options.size = (options.size) ? options.size : '';
                options.maxlength = (options.maxlength) ? options.maxlength : '';
                options.name = (options.name) ? options.name : 'name';
                options.id = (options.id) ? options.id : '';
                options.columnlabel = (options.columnlabel) ? options.columnlabel : '';
                options.label = (typeof options.label == 'undefined')?'label':options.label;
                options.value = (options.value) ? options.value : '';
                options.autocompleteapi = (options.autocompleteapi) ? options.autocompleteapi : '';
                options.autocompletemodel = (options.autocompletemodel) ? options.autocompletemodel : 'autocompletemodel';
                options.autocompletetype = (options.autocompletetype) ? options.autocompletetype : 'autocompletetype';
                options.autocompletelabel = (options.autocompletelabel) ? options.autocompletelabel : 'autocompletelabel';
                options.autocompletevalue = (options.autocompletevalue) ? options.autocompletevalue : 'autocompletevalue';
                options.autocompletepulldownwhennotempty = (options.autocompletepulldownwhennotempty) ? options.autocompletepulldownwhennotempty : '0';
                options.autocompletewidth = (options.autocompletewidth) ? options.autocompletewidth : '';
                options.autocreate = (options.autocreate) ? options.autocreate : 'false';
                options.autosavemodel = (options.autosavemodel) ? options.autosavemodel : 'autosavemodel';
                options.autosavetype = (options.autosavetype) ? options.autosavetype : '';
                options.autosaveid = (typeof options.autosaveid == 'undefined')?options.id:options.autosaveid;

                //@TODO : Remove these options :
                options.autocompletefiltercolumn = (options.autocompletefiltercolumn) ? options.autocompletefiltercolumn : '';
                options.autocompletefiltervalue = (options.autocompletefiltervalue) ? options.autocompletefiltervalue : '';
                options.autocompletefiltermodel = (options.autocompletefiltermodel) ? options.autocompletefiltermodel : '';
                options.autocompleteignorefilter = (options.autocompleteignorefilter) ? options.autocompleteignorefilter : '';

                var id = (options.name) ? options.name : 'default';
                var html = (options.columnlabel) ? methods.renderPrefield(options.columnlabel, options.label) : '';
                html += '<input type="text" size="' + options.size + '" maxlength="' + options.maxlength + '" ' +
                    'id="' + options.name + ((options.id != '') ? '_' + options.id : '') + '_autocomplete" name="' + options.name + '_autocomplete" ' +
                    'class="autocomplete" value="' + options.label + '" ' +
                    ((options.autocompleteapi != '') ? 'data-autocomplete-api="' + options.autocompleteapi + '" ' : '') +
                    'data-autocomplete-model="' + options.autocompletemodel + '" ' +
                    'data-autocomplete-type="' + options.autocompletetype + '" ' +
                    'data-autocomplete-titlefield="' + options.autocompletelabel + '" ' +
                    'data-autocomplete-valuefield="' + options.autocompletevalue + '" ' +

                    /* TODO: Delete those : */
                    'data-autocomplete-filtercolumn="' + options.autocompletefiltercolumn + '" ' +
                    'data-autocomplete-filtervalue="' + options.autocompletefiltervalue + '" ' +
                    'data-autocomplete-filtermodel="' + options.autocompletefiltervalue + '" ' +
                    'data-autocomplete-ignorefilter="' + options.autocompleteignorefilter + '" ' +

                    'data-autocomplete-pulldownwhennotempty="' + options.autocompletepulldownwhennotempty + '" ' +
                    'data-autocreate="' + options.autocreate + '" data-prevval="' + options.label + '" ' +
                    'data-autocomplete-width="' + options.autocompletewidth + '" data-dirty="0" autocomplete="off">';
                html += '<input type="hidden" value="' + options.value + '" name="' + options.name + '" id="' + options.name + ((options.id != '') ? '_' + options.id : '') + '" ' +
                    'data-save-model="' + options.autosavemodel + '" ' +
                    ((options.autosavetype != '') ? 'data-save-type="' + options.autosavetype + '" ' : '') +
                    'data-save-id="' + options.autosaveid + '" data-prevval="' + options.value + '" class="autosave" ' +
                    'size="" maxlength="" data-save-urgent="0">';
                return html;
            },
            'getTextInput': function(options) {
                options.size = (options.size) ? options.size : '';
                options.maxlegth = (options.maxlength) ? options.maxlength : '';
                options.name = (options.name) ? options.name : 'name';
                options.id = (options.id) ? options.id : '';
                options.columnlabel = (options.columnlabel) ? options.columnlabel : '';
                options.label = (options.label) ? options.label : 'label';
                options.value = (options.value) ? options.value : '';
                options.autosavemodel = (options.autosavemodel) ? options.autosavemodel : 'autosavemodel';
                options.autosavetype = (options.autosavetype) ? options.autosavetype : 'autosavetype';
                options.autosaveid = (typeof options.autosaveid == 'undefined')?options.id:options.autosaveid;

                var id = (options.name) ? options.name : 'default';
                var html = (options.columnlabel) ? methods.renderPrefield(options.columnlabel, options.label) : '';
                html += '<input type="text" value="' + options.value + '" name="' + options.name + '" id="' + options.name + ((options.id != '') ? '_' + options.id : '') + '" ' +
                    'data-save-model="' + options.autosavemodel + '" data-save-type="' + options.autosavetype + '" data-save-id="' + options.autosaveid + '" data-prevval="' + options.value + '" class="autosave" ' +
                    'size="' + options.size + '" maxlength="' + options.maxlegth + '" data-save-urgent="0">';
                return html;
            },

            'getActions': function(options) {
                var html = '';
                for(var i = 0; i < options.actions.length; i++) {
                    var opt = options.actions[i];
                    html += '<a class="btn btn-default btn-xs action-'+opt.name+'" alt="';
                    var title = '';
                    if (opt.label) title += opt.label;
                    else {
                        if (opt.name == 'delete') title += 'Delete';
                        if (opt.name == 'edit') title += 'Edit';
                        if (opt.name == 'duplicate') title += 'Duplicate';
                    }
                    html += title+'" title="'+title+'" data-id="' + opt.id + '"><span class="';
                    if (opt.icon) html += opt.icon;
                    else {
                        if (opt.name == 'delete') html += 'glyphicon glyphicon-trash';
                        if (opt.name == 'edit') html += 'glyphicon glyphicon-pencil';
                        if (opt.name == 'duplicate') html += 'glyphicon glyphicon-clipboard';
                    }
                    html+= '"></span></a>';
                }
                return html;
            },

            'getCheckbox': function(options) {
                options.name = (options.name) ? options.name : 'name';
                options.id = (options.id) ? options.id : '';
                options.columnlabel = (options.columnlabel) ? options.columnlabel : '';
                options.label = (options.label) ? options.label : 'label';
                options.value = (options.value) ? options.value : '';
                options.autosavemodel = (options.autosavemodel) ? options.autosavemodel : 'autosavemodel';
                options.autosavetype = (options.autosavetype) ? options.autosavetype : 'autosavetype';
                options.autosaveid = (typeof options.autosaveid == 'undefined')?options.id:options.autosaveid;

                var html = (options.columnlabel) ? methods.renderPrefield(options.columnlabel, options.label) : '';
                html += '<input type="checkbox" value="1" name="'+ options.name + '" id="' + options.name + ((options.id != '') ? '_' + options.id : '') + '" onclick="$(this).next(\'.autosave\').val(($(this).prop(\'checked\'))?1:0);$(this).next(\'.autosave\').trigger(\'blur\').trigger(\'change\');$(this).trigger(\'blur\').change();"';
                if (options.value) html += " checked>";
                else html += ">";
                html += '<input type="hidden" value="' + options.value + '" name="' + options.name + '" id="' + options.name + ((options.id != '') ? '_' + options.id : '') + '" ' +
                    'data-save-model="' + options.autosavemodel + '" data-save-type="' + options.autosavetype + '" data-save-id="' + options.autosaveid + '" data-prevval="' + options.value + '" class="autosave" ' +
                    'size="' + options.size + '" maxlength="' + options.maxlegth + '" data-save-urgent="0">';
                return html;
            },

            'renderPrefield': function(label, value) {
                return '<label>' + label + ':</label><span class="hidden">' + value + '</span>';
            },

            /**
             * Initialize plugin
             * @return {[type]} [description]
             */
            'init': function() {
                return methods;
            }
        };
        methods.init();
        return methods;
    };
})(jQuery);

/*
 * Lazy loading snippets
 */
$(document).ready(function() {
    $(document).lucidDatatableRenderer();
});

/**
 * Datatables Renderer for supported input types
 */
$.fn.dataTable.render.autocompleteInput = function(options) {
    return function(data, type, row) {
        if (type === 'display') {
            options.value = (typeof data == 'object')? data.value: data;
            options.label = (typeof data == 'object')? data.label: data;
            options.id = row[options.idkey];

            //Unset the autosave ID as it prevails over our ID :
            delete options.autosaveid;

            return $(document).lucidDatatableRenderer().getAutocompleteInput(options);
        }
        if (type === 'order' || type === 'search') {
            return (typeof data.label != 'undefined') ? data.label : data;
        }
        return data;
    };
};
$.fn.dataTable.render.textInput = function(options) {
    return function(data, type, row) {
        if (type === 'display') {
            options.value = data;
            options.id = row[options.idkey];

            //Unset the autosave ID as it prevails over our ID :
            delete options.autosaveid;

            return $(document).lucidDatatableRenderer().getTextInput(options);
        }
        // Search, order and type can use the original data
        return data;
    };
};
$.fn.dataTable.render.actions = function(options) {
    return function(data, type, row) {
        if (type === 'display') {
            options.value = data;
            for(var i = 0; i < options.actions.length; i++) {
                options.actions[i].id = row[options.actions[i].idkey];
            }

            //Unset the autosave ID as it prevails over our ID :
            delete options.autosaveid;

            return $(document).lucidDatatableRenderer().getActions(options);
        }
        // Search, order and type can use the original data
        return data;
    };
};
$.fn.dataTable.render.checkbox = function(options) {
    return function(data, type, row) {
        if (type === 'display') {
            options.value = data;
            options.id = row[options.idkey];

            //Unset the autosave ID as it prevails over our ID :
            delete options.autosaveid;

            return $(document).lucidDatatableRenderer().getCheckbox(options);
        }
        // Search, order and type can use the original data
        return data;
    };
};



/*############################################################################################################################################################*/
/*############################################################################################################################################################*/
/**
 * lucidFilters class
 *
 * Page related filters handler
 *
 * Usage:
 * $(document).lucidFilters({
 *    'component': 'mycomponent',
 *    'controller': 'mycontroller',
 *    'renderer' : '',
 *    'filters': [{
         'name': 'cie',
         'label': 'Company',
         'default': 0,
         'autocompletemodel': '',
         'autocompletetype': '',
         'autocompletelabel': '',
         'autocompletevalue': ''
      }]
 * });
 *
 * Requires :
 * jquery.cookie (https://github.com/carhartl/jquery-cookie)
 * lucidDatatableRenderer (https://github.com/carhartl/jquery-cookie)
 *
 * @class lucidFilters
 * @public
 */
(function($) {
    $.fn.lucidFilters = function(options) {
        var settings = jQuery.extend({
            'component': '',
            'controller': '',
            'renderer': '',
            'autosaveEventName': 'autosave.lucidapp',
            'filters': []
        }, options);
        var $jquery = this;
        var methods = {
            /**
             * Sets filter value
             * @param  string name  Filter name
             * @param  string value Filter value
             * @param  string label Corresponding label
             */
            'set': function(name, value, label) {
                $.cookie('lucidfilter_' + settings.component + '_' + settings.controller + '_' + name, JSON.stringify({
                    'value': value,
                    'label': label
                }), {
                    path: "/",
                    expires: 7,
                    secure: true
                });
                return methods;
            },

            /**
             * Gets filter value
             * @param  string name  Filter name
             */
            'get': function(name) {
                var val = $.cookie('lucidfilter_' + settings.component + '_' + settings.controller + '_' + name);
                if (val && val.length) {
                    return JSON.parse(val);
                } else {
                    for (var i = 0; i < settings.filters.length; i++) {
                        if (name == settings['filters'][i]['name']) {
                            return {
                                'value': settings['filters'][i]['default'],
                                'label': settings['filters'][i]['label']
                            }
                        }
                    }
                }
            },

            /**
             * Clears all filters on screen
             */
            'clear': function() {
                for (var i = 0; i < settings.filters.length; i++) {
                    var label = '#filter_' + settings.component + '_' + settings.controller + '_' + settings['filters'][i]['name'] + '_autocomplete';
                    var value = '#filter_' + settings.component + '_' + settings.controller + '_' + settings['filters'][i]['name'];
                    $(label).val(settings['filters'][i]['label']);
                    $(value).val(settings['filters'][i]['default']);
                }
            },

            /**
             * Pack all filters values in an object to send as get or post request
             * @param  bool defaultValues Pack default values instead of real filter input values
             * @return object An object containing all filters for current component/controller
             */
            'pack': function(defaultValues) {
                var data = {};
                for (var i = 0; i < settings.filters.length; i++) {
                    data[settings[i].name] = (defaultValues) ? settings[i].default : $(settings['filters'][i]['selectors']['value']).val();
                }
                return data;
            },

            /**
             * Renders filters in place. Will replace content of matching tags.
             * Add these attributes to your filter's container :
             *     class="lucidfilter" data-filter-name="somefiltername"
             *
             * @return {[type]} [description]
             */
            'render': function() {
                if (settings.renderer == '') {
                    console.info('lucidFilters: Please specify a renderer!');
                    console.info(settings);
                    return;
                }
                for (var i = 0; i < settings.filters.length; i++) {
                    var name = settings['filters'][i]['name'];
                    var inputname = 'filter_' + settings.component + '_' + settings.controller + '_' + settings['filters'][i]['name'];
                    if (settings.renderer != '') {
                        console.info($('.lucidfilter[data-filter-name="' + name + '"]'));
                        var filterval = methods.get(name);
                        $('.lucidfilter[data-filter-name="' + name + '"]').html(settings.renderer.getAutocompleteInput({
                            'name': inputname,
                            'label': filterval['label'],
                            'value': filterval['value'],
                            'autocompletemodel': (settings['filters'][i]['autocompletemodel']) ? settings['filters'][i]['autocompletemodel'] : settings['autocompletemodel'],
                            'autocompletetype': (settings['filters'][i]['autocompletetype']) ? settings['filters'][i]['autocompletetype'] : settings['autocompletetype'],
                            'autocompletelabel': (settings['filters'][i]['autocompletelabel']) ? settings['filters'][i]['autocompletelabel'] : settings['autocompletelabel'],
                            'autocompletevalue': (settings['filters'][i]['autocompletevalue']) ? settings['filters'][i]['autocompletevalue'] : settings['autocompletevalue'],
                            'autosavemodel': 'filter',
                            'autosaveid': inputname,
                        }));
                    }
                }
            },

            'onchange': function(){
                var dtel = $(this).closest("table.dataTable");
                if (dtel.closest(".dataTables_scroll").length > 0) {
                    dtel = dtel.closest(".dataTables_scroll").find("table.dataTable.level1");
                }
                var dt = dtel.DataTable();
                dt.draw();
            },

            /**
             * Initialize plugin
             * @return {[type]} [description]
             */
            'init': function() {
                methods.render();
                $jquery.each(function() {
                    var selector = '#'+$(this).attr("id") + '_wrapper th.lucidfilter input.autosave';
                    setTimeout(function(){$(selector).parent().each(enableAutocomplete);},200);
                    $(selector).prev(".autocomplete").on('click', function(event) {
                        event.stopPropagation();
                    });
                    $(selector).off(settings.autosaveEventName, methods.onchange).on(settings.autosaveEventName, methods.onchange);
                });
                return methods;
            }
        };
        methods.init();
        return methods;
    };
})(jQuery);



/*############################################################################################################################################################*/
/*############################################################################################################################################################*/
/**
 * lucidDataTable class
 *
 * A collection of options to use on datatables
 *
 * Usage:
 * $('#datatable').lucidDataTable();
 *
 * Requires :
 * lucidFilters.js
 *
 * @class lucidDataTable
 * @public
 */
(function($) {
    $.fn.lucidDataTable = function(options) {

        var settings = jQuery.extend({
            'table': {},
            'autosave': true,
            'minSearchLength': 3,
            'filters': [],
            'actions': []
        }, options);

        var $jquery = this;
        var methods = {
            /**
             * Enables autosaving / autocomplete / auto create for row fields
             * @return object Methods object for methods chaining
             */
            'autosave': function() {
                var autosaveSettings = settings.autosave;
                $jquery.each(function() {
                    autosaveEventName = autosaveSettings.autosaveEventName;
                    autosaveURL = autosaveSettings.autosaveURL;
                    autocreateURL = autosaveSettings.autocreateURL;
                    autocompleteURL = autosaveSettings.autocompleteURL;
                    autocompleteBaseURL = autosaveSettings.autocompleteBaseURL;
                    autocompleteLeaveFieldEventName = autosaveSettings.autocompleteLeaveFieldEventName;
                    enableAutosave($(this).attr("id"));
                });
                return methods;
            },

            /**
             * Enables minimum search length
             * @return object Methods object for methods chaining
             */
            'minSearchLength': function() {
                console.info('lucidDataTable.minSearchLength: This method does not work yet!');
                /*var minLength = settings.minSearchLength;
                $jquery.each(function(){
                    var dt = $(this).dataTable();
                    $('#'+$(this).attr('id')+'_wrapper .dataTables_filter input').unbind('keypress keyup').bind('keypress keyup', function(e) {
                        if ($(this).val().length > 0 && $(this).val().length < minLength && e.keyCode != 13) return;
                        dt.search($(this).val()).draw();
                    });
                });*/
                return methods;
            },

            /**
             * Enables header filters
             * @return object Methods object for methods chaining
             */
            'filters': function() {
                var filter = settings.filters;
                if (!filter.renderer) filter.renderer = $(document).lucidDatatableRenderer();
                filter.autosaveEventName = settings.autosave.autosaveEventName;
                $jquery.each(function() {
                    $(this).lucidFilters(filter);
                });
                return methods;
            },

            'dt': function() {
                //Replace renderer for actions to specify only one
                //actions array in the options.
                if (settings.table.columns.length) {
                    for(var i =0; i < settings.table.columns.length; i++){
                        if (typeof settings.table.columns[i].render != 'function' &&
                            settings.table.columns[i].render == 'actions') {
                            settings.table.columns[i].render = $.fn.dataTable.render.actions({
                                'actions': settings.actions
                            });
                        }
                    }
                }
                var dtref = null;
                $jquery.each(function() {
                    var _this = $(this);
                    if (!$(this).hasClass('lucidDataTable')) {
                        $(this).addClass('lucidDataTable');
                        var dtref = $(this).DataTable(settings.table);
                        $('#' + $(this).attr('id') + '_wrapper').addClass('lucidDataTable wrapper');
                        dtref.off('draw',methods.draw).on('draw', methods.draw);
                        if (typeof settings.table.drawCallback == 'function') dtref.off('draw',settings.table.drawCallback).on('draw', settings.table.drawCallback);
                        if (typeof settings.drawCallback == 'function') dtref.off('draw',settings.drawCallback).on('draw', settings.drawCallback);
                        $(this).off('newrow.dt',methods.draw).on('newrow.dt',methods.draw);
                    }
                });
                if (!dtref) console.info("lucidDataTable.dt: No datatables found !");
                return dtref;
            },

            'draw': function() {
                //Apply actions handlers
                for (var i = 0; i < settings.actions.length; i++) {
                    if (settings.actions[i]['handler']) {
                        if (settings.actions[i]['handler'] == 'deletePrompt') {
                            $('#' + $(this).attr('id') + ' > tbody > tr[role="row"] .action-' + settings.actions[i]['name']).off('click', methods.deletePrompt(settings.actions[i])).on('click', methods.deletePrompt(settings.actions[i]));
                        }
                    } else $('#' + $(this).attr('id') + ' > tbody > tr[role="row"] .action-' + settings.actions[i]['name']).off('click', settings.actions[i].callback).on('click', settings.actions[i].callback);
                }
                //Apply datatable title attribute
                if (settings.title && $('#' + $(this).attr('id') + '_wrapper > .row:first-child > .col-sm-6:first-child h1.title').length === 0) {
                    $('#' + $(this).attr('id') + '_wrapper > .row:first-child > .col-sm-6:first-child').prepend("<h1 class='title pull-left'>" + settings.title + "</h1>");
                }

                //Create the NEW button...
                var buttonTitle = (settings.createbutton['title']) ? settings.createbutton.title : 'Add';
                var sel='#' + $(this).attr('id') + '_wrapper > .row:first-child > div[class^="col-sm-"]:last-child';
                if (settings.createbutton && settings.createbutton.top && !$(sel+' a.action-new').length) {
                    $(sel).prepend('<a class="pull-right btn btn-primary btn-sm action-new" alt="' + buttonTitle + '" title="' + buttonTitle + '"><span class="glyphicon glyphicon-plus-sign"></span> ' + buttonTitle + '</a>');
                    $(sel+' .action-new').off('click', methods.addFirstRowClick).on('click', methods.addFirstRowClick);
                }
                sel='#' + $(this).attr('id') + '_wrapper > .row:last-child > div[class^="col-sm-"]:last-child';
                if (settings.createbutton && settings.createbutton.bottom && !$(sel+' a.action-new').length) {
                    var buttonTitle = (settings.createbutton['title']) ? settings.createbutton.title : 'Add';
                    $(sel).prepend('<a class="pull-right btn btn-primary btn-sm action-new" alt="' + buttonTitle + '" title="' + buttonTitle + '"><span class="glyphicon glyphicon-plus-sign"></span> ' + buttonTitle + '</a>');
                    $(sel+' .action-new').off('click', methods.addLastRowClick).on('click', methods.addLastRowClick);
                }
            },

            /**
             * Click handler for new row button, first row
             */
            addFirstRowClick: function(){
                methods.addRow($.extend(settings.createbutton.options,{
                    index:0,
                    url: (settings.createbutton.url)?settings.createbutton.url:settings.table.ajax.url,
                    callback: (settings.createbutton.callback)?settings.createbutton.callback:null,
                }));
            },

            /**
             * Click handler for new row button, last row.
             */
            addLastRowClick: function(){
                methods.addRow($.extend(settings.createbutton.options,{
                    index:'last',
                    url: (settings.createbutton.url)?settings.createbutton.url:settings.table.ajax.url,
                    callback: (settings.createbutton.callback)?settings.createbutton.callback:null,
                }));
            },

            /**
             * Adds a new row to the datatable.
             **/
            'addRow': function(options) {
                //Make sure last element with focus gets saved.
                $('table.dataTable input[type="text"]:focus,table.dataTable select:focus,table.dataTable button:focus, table.dataTable textarea:focus').not(".chosen-search-input").trigger("blur").trigger("change");
                index = (options.index == 'last') ? 999999 : options.index;
                if (index > 0) index++; //Put duplicate after master item.
                var d = $.extend({
                    'create': true,
                    "index": index,
                }, options.data);
                $jquery.each(function() {
                    var _this = $(this);
                    $.post(options.url, d, function(data) {
                        params = {
                            'table': "#" + _this.attr('id'),
                            'data': data.data[0]
                        };
                        params.data.index = d.index;
                        $(params.table + ' > tbody > tr.new[role="row"]').removeClass("new");
                        var rowNode = $(params.table).DataTable().order([0, 'asc']).row.add(params.data).node();
                        var index = parseInt(params.data.index) + 1;
                        var rowPosition = $(params.table + ' > tbody > tr[role=row]').eq(index - 1);
                        if (!rowPosition.length) {
                            rowPosition = $(params.table + ' > tbody > tr[role=row]:last');
                            if (rowPosition.hasClass("shown")) rowPosition = rowPosition.next();
                            if (rowPosition.length) $(rowNode).addClass("new").insertAfter(rowPosition);
                            else {
                                //First item in table
                                $(params.table + ' > tbody > tr').remove();
                                $(rowNode).addClass("new").appendTo($(params.table + ' > tbody'));
                            }
                        } else {
                            if (rowPosition.attr("role") !== "row") rowPosition = rowPosition.next("[role='row']");
                            $(rowNode).addClass("new").insertBefore(rowPosition);
                        }
                        var newRow = $(params.table + ' > tbody > tr.new[role="row"]');
                        if ($(params.table).hasClass('level1')) newRow[0].scrollIntoView();
                        if (params.setvaluetarget) {
                            newRow.find(params.setvaluetarget).val(params.setvaluetargetvalue);
                        }

                        //Remove 1 row, either the first one or the last one if more than perpage rows.
                        perPage = $('select[name="' + String(params.table).replace(/#/g, '') + '_length"]');
                        if (perPage.length > 0 && perPage.val() < $(params.table + ' > tbody > tr[role=row]').length) {
                            if (index > ($(params.table + ' > tbody > tr[role=row]').length) / 2) {
                                var firstRow = $(params.table + ' > tbody > tr:first-child');
                                if (firstRow.hasClass("shown")) firstRow.next("tr").remove();
                                firstRow.remove();
                            } else {
                                var lastRow = $(params.table + ' > tbody > tr[role=row]:last');
                                if (lastRow.hasClass("shown")) lastRow.next("tr").remove();
                                lastRow.remove();
                            }
                        }

                        //Trigger new row event so inputs newly added will autosave themselves
                        $(params.table).trigger("newrow.dt");

                        if (typeof(options.callback) == 'function') {
                            options.callback();
                        }
                    });
                });
            },

            'deletePrompt': function(options) {
                var deletePromptOpt = options;
                return function(e) {
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_WARNING,
                        title: ((deletePromptOpt['title']) ? deletePromptOpt['title'] : 'Are you sure ?'),
                        message: ((deletePromptOpt['message']) ? deletePromptOpt['message'] : 'Hit "Yes" to delete this item. This action cannot be reverted.'),
                        buttons: [{
                            label: 'No',
                            hotkey: 27, // ESC.
                            cssClass: 'btn-default',
                            action: function(dialog) {
                                dialog.close();
                            }
                        }, {
                            label: 'Yes',
                            cssClass: 'btn-primary',
                            hotkey: 13, // Enter.
                            action: function(dialog) {
                                console.info(e);
                                deletePromptOpt.callback.apply(e.currentTarget,[e,dialog]);
                            }
                        }]
                    });
                };
            },

            /**
             * Initialize plugin
             * @return {[type]} [description]
             */
            'init': function() {
                if (settings.table) methods.dt();
                if (settings.autosave) methods.autosave();
                if (settings.minSearchLength > 0) methods.minSearchLength();
                if (settings.filters) methods.filters();
                return methods;
            }
        };
        methods.init();
        return methods;
    };
})(jQuery);
