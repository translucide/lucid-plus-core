/**
 * LucidEditor jQuery Plugin
 *
 * Allows easy editing of content
 * in Lucid+.
 *
 * Dec 26, 2014
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2014 Translucide
 * @license
 * @since 		0.1
 */

jQuery.expr[':'].withoutparent = function(a,i,m){
    return jQuery(a).parent(m[3]).length < 1;
};


/**
 * LucidEditor class
 *
 * @class LucidEditor
 * @public
 */
(function( $ ) {
	var methods = {
		/**
		 * Initialize the editor.
		 *
		 * @public
		 * @memberOf LucidEditor
		 * @function
		 * @returns {Object} A reference to the DOM object this plugin is working on
		 */
		init: function(options) {
			console.info("lucidEditor.init()");
			var $this = $(this);
			$this.addClass('lucideditor');
			$this.data('options', $.extend({
				name: 'lucideditor',
				value: '',
				gridsize: 12,
				fullScreen: false,
				url: '',
				saveurl: '',
				renderurl: '',
				removeurl: '',
				lang: 'en',
				ressources: ''
			}, options));
			$this.data("historyindex",0);
			$this.lucidEditor("setup");
			$this.lucidEditor("update");
			return this;
		},

        cleanUp: function(){
			console.info("lucidEditor.cleanUp()");
			var $this = $(this);
			var opt = $this.data('options');
            $this.lucidEditor("getDialog").off().remove();
        },

		/**
		 * update: Updates all cells
		 *
		 * @public
		 * @memberOf LucidEditor
		 */
		update: function(element){
			console.info("lucidEditor.update("+element+")");
			var $this = $(this);
			var opt = $this.data('options');
			var history = $this.children('.history').children();
			var hindex = $this.data('historyindex');

            //Update CKEditors values...
            try{
                if (CKEDITOR != undefined && CKEDITOR.instances != undefined) {
                    for (instance in CKEDITOR.instances) {
                        $('#'+instance).val(CKEDITOR.instances[instance].getData());
                        for(i=0;i<data.length;i++) if(data[i].name == instance) data[i].value = CKEDITOR.instances[instance].getData();
                    }
                }
            }catch(err){}

			//Clean markup only if it does not come from history (already cleaned)
			if (history.eq(hindex).html() != $this.children('.canvas').html()) {
				$this.lucidEditor("sanitize");
			}
			if ($this.children('.canvas').html() == "") {
				$this.children(".canvas").html('<div class="row">'+$this.lucidEditor("getNewCellCode",opt.gridsize)+'</div>');
			}
			//Update output
			var output = $this.children('.canvas').html();
			$this.lucidEditor("value",output);

			//Update history
			if (history.eq(hindex).html() != output || history.length <= hindex) {
				//Clear foward history and add this output to history.
				history.eq(hindex).nextAll().remove();
				history.parent().append('<div>'+output+'</div>');
				$this.data('historyindex',$this.children('.history').children().length-1);
				console.info('New history item added. Current history steps stored is now '+$this.children('.history').children().length);
			}
			$this.lucidEditor("updateCellEvents");
		},

		/**
		 * Creates the HTML markup mecessary for Lucid Editor to operate properly
		 *
		 * @public
		 * @memberOf LucidEditor
		 * @function
		 */
		setup : function() {
			console.info("lucidEditor.setup()");
			var $this = $(this);
			var opt = $this.data('options');
			var val = (opt.value != '' && opt.value != 'null')?opt.value:'<div class="row">'+$this.lucidEditor("getNewCellCode",opt.gridsize)+'</div>';
			var ressources = (typeof opt.ressources == 'object' || typeof opt.ressources == 'array')?opt.ressources:new Array();
			var ressources_inputname = $this.lucidEditor("getRessourcesInputName");
			console.info("lucidEditor.setup() : Initial ressources for '"+ressources_inputname+"'="+JSON.stringify(ressources));
            $this.lucidEditor("cleanUp");
			$this.html(
                "<div class='toolbar'>"+
					"<a class=\"btn btn-small\" id='undo' rel=\"tooltip\" title=\""+$this.lucidEditor("getLang",'undo')+"\"><span class=\"glyphicon glyphicon-circle-arrow-left\"></span></a>"+
					"<a class=\"btn btn-small\" id='redo' rel=\"tooltip\" title=\""+$this.lucidEditor("getLang",'redo')+"\"><span class=\"glyphicon glyphicon glyphicon-circle-arrow-right\"></span></a>"+
					"<a class=\"btn btn-small\" id=\"cell_insertbefore_trigger\" rel=\"tooltip\" title=\""+$this.lucidEditor("getLang",'cellinsertbefore')+"\"><span class=\"glyphicon glyphicon-chevron-left\"></span></a>"+
					"<a class=\"btn btn-small\" id=\"cell_insertafter_trigger\" rel=\"tooltip\" title=\""+$this.lucidEditor("getLang",'cellinsertafter')+"\"><span class=\"glyphicon glyphicon-chevron-right\"></span></a>"+
					"<a class=\"btn btn-small\" id=\"row_insertbefore_trigger\" rel=\"tooltip\" title=\""+$this.lucidEditor("getLang",'rowinsertbefore')+"\"><span class=\"glyphicon glyphicon-chevron-up\"></span></a>"+
					"<a class=\"btn btn-small\" id=\"row_insertafter_trigger\" rel=\"tooltip\" title=\""+$this.lucidEditor("getLang",'rowinsertafter')+"\"><span class=\"glyphicon glyphicon-chevron-down\"></span></a>"+

					"<a class=\"btn btn-small\" id=\"content_add\" rel=\"tooltip\" title=\""+$this.lucidEditor("getLang",'widgetadd')+"\"><span class=\"glyphicon glyphicon-list-alt\"></span></a></li>"+
					"<a class=\"btn btn-small\" id=\"content_refresh\" rel=\"tooltip\" title=\""+$this.lucidEditor("getLang",'widgetrefresh')+"\"><span class=\"glyphicon glyphicon glyphicon glyphicon-refresh\"></span></a></li>"+
					"<a class=\"btn btn-small\" id='textblock' rel=\"tooltip\" title=\""+$this.lucidEditor("getLang",'textblock')+"\"><span class=\"glyphicon glyphicon-text-size\"></span></a>"+
					"<a class=\"btn btn-small\" id='fullscreen' rel=\"tooltip\" title=\""+$this.lucidEditor("getLang",'fullscreen')+"\"><span class=\"glyphicon glyphicon-fullscreen\"></span></a>"+
					"<a class=\"btn btn-small\" id='source' rel=\"tooltip\" title=\""+$this.lucidEditor("getLang",'source')+"\"><span class=\"glyphicon glyphicon glyphicon-cog\"></span></a>"+
				"</div>"+
				"<div class='canvas'>"+val+"</div>"+
				"<div class='history'><div>"+val+"</div></div>"+
				"<div class=\"output\">"+
                    "<div class=\"title\">"+$this.lucidEditor("getLang",'sourcecode')+'</div>'+
					"<textarea name='"+opt.name+"' id='"+opt.name+"'>"+val+"</textarea>"+
                    "<div class=\"title\">"+$this.lucidEditor("getLang",'resssources')+'</div>'+
					"<textarea name='"+ressources_inputname+"' id='"+ressources_inputname+"'>"+JSON.stringify(ressources)+"</textarea>"+
				"</div>"
			);

            // Pre select first item
            $this.find(".selected").removeClass("selected");
            $this.find(".canvas > .row > .cell:first-child > .content:first-child").addClass("selected").parent().addClass("selected");

            /*Autoresize textarea fix*/
            function h(e) {
                $(e).css({'height':'auto','overflow-y':'hidden'}).height(e.scrollHeight);
            }
            $this.children(".output").children("textarea").each(function () {
                h(this);
            }).on('input', function () {
                h(this);
            });

			//If field value is not a lucidEditor produced value, try to wrap content around a lucideditor skeleton..
			if ($this.children(".canvas").children(".row").length == 0) {
				var tmpval = $this.children(".canvas").html();
				$this.children(".canvas").html('<div class="row">'+$this.lucidEditor("getNewCellCode",opt.gridsize)+'</div>');
				$this.find(".row .cell .content").html(tmpval);
			}

			//Fixes a bug: something captures spaces chars and we cannot type any space in CKEditor inline mode.
			$(document).off("keydown");

            //Register onSubmit event
			$this.parents("form").submit(function( event ) {
                $this.lucidEditor("onSubmit",event);
 			});

			$this.lucidEditor("setupToolbar");
			$this.lucidEditor("setupHoverTools");
			$this.lucidEditor("setupDialogs");
			return this;
		},

		/**
		 * Setup hover icons
		 *
		 * @public
		 * @memberOf lucidEditor
		 * @returns Object this
		 */
		setupHoverTools: function(){
			console.info("lucidEditor.setupHoverTools()");
			var $this = $(this);
			var opt = $this.data('options');
			$('body').append("<div class='lucideditor_hovertools lucideditor_hovertools_"+opt.name+"'><a id='smaller'><span class=\"glyphicon glyphicon-arrow-left\"></span></a><a id='larger'><span class=\"glyphicon glyphicon-arrow-right\"></span></a><a id='remove'><span class=\"glyphicon glyphicon-remove-sign\"></span></a></div>");
			$('body').append("<div class='lucideditor_hovertoolsct lucideditor_hovertoolsct_"+opt.name+"'><a id='editcontent'><span class=\"glyphicon glyphicon-pencil\"></span></a><a id='remove'><span class=\"glyphicon glyphicon-remove-sign\"></span></a></div>");
			$("body .lucideditor_hovertools_"+opt.name+",body .lucideditor_hovertoolsct_"+opt.name).css("display","none");
			$("body .lucideditor_hovertools_"+opt.name+" #smaller").click(function(){
				var el = $this.lucidEditor("getSelectedCell");
				var el2 = $this.lucidEditor("getSelectedContent");
				var newWidth = $this.lucidEditor("getCellWidth",el);
				if (newWidth > 1) newWidth--;
				$this.lucidEditor("resetCellWidth",el);
				el.addClass("col-sm-"+newWidth);
				$this.lucidEditor("update",el);
				el.addClass("selected");
				el2.addClass("selected");
				$this.lucidEditor("positionHoverTools");
			});
			$("body .lucideditor_hovertools_"+opt.name+" #larger").click(function(){
				var el = $this.lucidEditor("getSelectedCell");
				var el2 = $this.lucidEditor("getSelectedContent");
				var newWidth = $this.lucidEditor("getCellWidth",el);
				if (newWidth < 12) newWidth++;
				$this.lucidEditor("resetCellWidth",el);
				el.addClass("col-sm-"+newWidth);
				$this.lucidEditor("update",el);
				el.addClass("selected");
				el2.addClass("selected");
				$this.lucidEditor("positionHoverTools");
			});
			$("body .lucideditor_hovertoolsct_"+opt.name+" #editcontent").click(function(){
                $this.lucidEditor("onEdit");
			});
			$("body .lucideditor_hovertools_"+opt.name+" #remove").click(function(){
				$this.lucidEditor("remove");
				$this.lucidEditor("update");
			});
			$("body .lucideditor_hovertoolsct_"+opt.name+" #remove").click(function(){
				$this.lucidEditor("removeContent");
				$this.lucidEditor("update");
			});
            $(window).scroll(function(){
                $("body .lucideditor_hovertools_"+opt.name).hide();
                $("body .lucideditor_hovertoolsct_"+opt.name).hide();
            });
			return this;
		},

		/**
		 * Bind all events to toolbars buttons
		 *
		 * @public
		 * @memberOf lucidEditor
		 * @returns Object this
		 */
		setupToolbar: function(){
			console.info("lucidEditor.setupToolbar()");
			var $this = $(this);
			var opt = $this.data("options");
			$this.children('.toolbar').children('#undo').on('click',function(){
				var chi = $this.data('historyindex');
				if (chi > 0) {
					$this.data('historyindex',chi-1);
					$this.children('.canvas').html($this.children('.history').children().eq(chi-1).html());
					$this.lucidEditor('update');
					console.info('Current history index is now '+$this.data('historyindex')+' over a total of '+$this.children('.history').children().length);
				}
			});
			$this.children('.toolbar').children('#redo').on('click',function(){
				var chi = $this.data('historyindex');
				if (chi < $this.children('.history').children().length-1){
					$this.data('historyindex',chi+1);
					$this.children('.canvas').html($this.children('.history').children().eq(chi+1).html());
					$this.lucidEditor('update');
					console.info('Current history index is now '+$this.data('historyindex')+' over a total of '+$this.children('.history').children().length);
				}
			});
			$this.children('.toolbar').children('#source').on('click',function(){
				var isSourceView = false;
                $this.children(".output").toggle();
				$this.children(".canvas").toggle();
                var onChangeEvt = function(){
                    $this.children(".canvas").html($this.children(".output").children("textarea#"+opt.name).val());
                    $this.lucidEditor("update");
                }
				if ($this.children(".output").css("display") != 'none') {
                    isSourceView = true;
                    var el = $this.children(".output").children("textarea");
                    el.each(function(){
                        $(this).css({'height':'auto','overflow-y':'hidden'}).height((this.scrollHeight+25));
                    });
					$("body .lucideditor_hovertools_"+opt.name+", body .lucideditor_hovertoolsct_"+opt.name).css("display","none");
                    $this.children(".output").children("textarea#"+opt.name).on("change blur paste",onChangeEvt);
				}else {
                    $this.children(".canvas").html($this.children(".output").children("textarea#"+opt.name).val());
                    $this.children(".output").children("textarea#"+opt.name).off("change blur paste");
                }
			});
			$this.children('.toolbar').find('#cell_insertbefore_trigger').on('click',function(){
				var c = $this.lucidEditor('getCell',$this.lucidEditor("getSelectedCell"));
				$this.lucidEditor('insertBefore',c);
			});
			$this.children('.toolbar').find('#cell_insertafter_trigger').on('click',function(){
				var c = $this.lucidEditor('getCell',$this.lucidEditor("getSelectedCell"));
				$this.lucidEditor('insertAfter',c);
			});
			$this.children('.toolbar').find('#row_insertbefore_trigger').on('click',function(){
				var c = $this.lucidEditor('getRow',$this.lucidEditor("getSelectedCell"));
				$this.lucidEditor('insertBefore',c);
			});
			$this.children('.toolbar').find('#row_insertafter_trigger').on('click',function(){
				var c = $this.lucidEditor('getRow',$this.lucidEditor("getSelectedCell"));
				$this.lucidEditor('insertAfter',c);
			});
			$this.children('.toolbar').find('#content_add').on('click',function(){
				$this.lucidEditor("listWidgets");
			});
			$this.children('.toolbar').find('#content_refresh').on('click',function(){

                var ressources_inputname = $this.lucidEditor("getRessourcesInputName");
                var res = $this.children('.output').children('#'+ressources_inputname).val("[]");
				$this.find(".canvas > .row > .cell").each(function(i,e){
					var el = $(e);
					if (el.children(".content").html() != '' && el.children(".content").attr("id") && el.children(".content").hasClass('widget')) {
                        var idd = el.children(".content").attr("id").split("_");
                        $.post(opt.renderurl.replace(/{lang}/g,opt.lang), {id: idd[3]}, function(d){
                            $this.lucidEditor("onRender", d);
                        });
					}
				});
			});
			$this.children('.toolbar').find('#fullscreen').on('click',function(){
				$this.lucidEditor("fullScreen");
			});
			$this.children(".toolbar").find("#textblock").on("click",function(){
				$this.lucidEditor("addEditableContent");
			});
			$(document).keyup(function(e){
				var opt = $this.data("options");
				if(e.which == 27 && opt.fullScreen){
					$this.lucidEditor("fullScreen",false);
				}
			});
			return this;
		},

		/**
		 * Switches fullscreen mode on or off.
		 *
		 * @public
		 * @memberOf lucidEditor
		 * @param boolean active Whether to activate or deactivate fullscreen
		 */
		fullScreen: function(active) {
			console.info("lucidEditor.fullScreen()");
			var $this = $(this);
			var opt = $this.data("options");
			if (active == undefined) {
				active = false;
			}
			if (opt.fullScreen || active) {
				$this.css({
					position:"relative",
					left:'auto',
					top:'auto',
					width: '100%',
					height: 'auto',
					zIndex: 'auto',
					overflowY: 'visible',
					backgroundColor: 'none'
				}).removeClass("fullscreen");
				$("body").css({
					maxHeight: 'auto',
					overflow: 'visible'
				});
				opt.fullScreen = false;
				$(window).scrollTop(opt.scrollPos);
                $(window).off("resize.lucidEditor")
			}else {
				var off = $this.offset();
				var ol = 0;
				$this.parents().each(function(){
					var ml = parseInt($(this).css('margin-left').replace(/px/g,''));
					if (ml < 0) ol += Math.abs(ml);
				});
				$this.css({
					position:"absolute",
					left:-1*off.left+34,
					top:-1*off.top,
					width: '100vw',
					height: '100vh',
					zIndex: '900',
					overflowY: 'scroll',
					backgroundColor: 'white',
				}).addClass("fullscreen");
				$("body").css({
					maxHeight: '100vh',
					overflow: 'hidden'
				});
				opt.fullScreen = true;
				opt.scrollPos = $(window).scrollTop();
                var resizeFnc = function(){
                    $this.lucidEditor("fullScreen",false);
                }
				$(window).scrollTop(0).on('resize.lucidEditor',resizeFnc);
			}
			$this.data("options",opt);
			return this;
		},

		getDialog: function(){
			console.info("lucidEditor.getDialog()");
			var $this = $(this);
			var opt = $this.data("options");
			return $("#lucideditor_dialog");
		},

		setupDialogs: function(){
			console.info("lucidEditor.setupDialogs()");
			var $this = $(this);
			var opt = $this.data("options");
			var dialog = $this.lucidEditor('getDialog');

			if (dialog.length < 1) {
				$("body").append('<div id="lucideditor_dialog" class="modal fade"><div class="modal-dialog"><div class="modal-content"></div></div></div>');
			}
            $('#lucideditor_dialog').on('hidden.bs.modal', function (e) {
                $(e.target).removeData("bs.modal").find(".modal-content").empty();
            });
            return this;
		},

		loadDialog: function(url,dialogName,params){
			console.info("lucidEditor.loadDialog("+url+","+dialogName+","+params+")");
			var $this = $(this);
			var opt = $this.data("options");
			var dialog = $this.lucidEditor('getDialog');
			url = url+"?dialog="+dialogName;
			if (params != undefined) url += "&"+$.param(params);
			//dialog.removeData('bs.modal');
            $(document).off('ready');
			dialog.modal({
				show: true,
				cache: false,
				remote: url
			});
			dialog.modal('show');
            return this;
		},

		listWidgets: function(){
			console.info("lucidEditor.listWidgets()");
			var $this = $(this);
			var opt = $this.data("options");
            if ($this.lucidEditor("getSelectedContent").hasClass("cke_editable")) {
                return this;
            }
			var dialog = $this.lucidEditor('getDialog');
			dialog.off('loaded.bs.modal').on('loaded.bs.modal', function (e) {
				var buttons = dialog.find("#lucideditor_dialog_ok");
				buttons.off('click').on('click', function (e2) {
					var widgetname = dialog.find("#widget").val();
					dialog.off('hidden.bs.modal').on('hidden.bs.modal',function(){
						dialog.off('hidden.bs.modal').removeData("bs.modal").find(".modal-content").empty();
						console.info("List widgets dialog button clicked");
						console.info("widgetname: "+widgetname);
						if (widgetname != '') $this.lucidEditor("editWidget",widgetname,0);
					});
					dialog.modal('hide');
					$('.modal-backdrop').remove();
				});
                dialog.find(".lucideditor_widgetlist_element").dblclick(function(e){
                    $(this).click();
                    dialog.find("#lucideditor_dialog_ok").click();
                });
                $(document).keyup(function(e) {
                    //Enter key = SAVE
                    //Bad when user types something....
                    //if (e.keyCode == 13 && dialog.find("#widget").eq(0).val() != '') dialog.find("#lucideditor_dialog_ok").click();
                    //ESC key = Close dialog
                    if (e.keyCode == 27) dialog.modal('hide');
                });
			});
			$this.lucidEditor("loadDialog", opt.url, 'list');
            return this;
		},

		editWidget: function(widgetname,id){
			console.info("lucidEditor.editWidget("+widgetname+","+id+")");
			var $this = $(this);
			var opt = $this.data("options");
			var dialog = $this.lucidEditor('getDialog');
			dialog.off('loaded.bs.modal').on('loaded.bs.modal', function (e) {
				var buttons = dialog.find("#lucideditor_dialog_ok");
				//Edit widget dialog has loaded.
				setTimeout(function(){$this.lucidEditor("saveWidget");},1000);
				buttons.off('click').on('click',function(e2){
					dialog.find("form").trigger("submit");
				});
				dialog.off('hidden.bs.modal').on('hidden.bs.modal',function(){
					dialog.off('hidden.bs.modal').removeData("bs.modal").find(".modal-content").empty();
				});
			}); //end modal shown event for edit dialog
			$this.lucidEditor("loadDialog", opt.url, 'edit',[{name:'widget', value:widgetname},{name:'id',value:id}]);
            return this;
		},

		saveWidget: function(){
			console.info("lucidEditor.saveWidget()");
			var $this = $(this);
			var opt = $this.data("options");
			var dialog = $this.lucidEditor('getDialog');
			var widgetid = dialog.find("[name='widget_objid']").val();
			dialog.find("form").ajaxForm({
				url : opt.saveurl,
				beforeSubmit : function(data, jqForm, options){
					try{
						if (CKEDITOR != undefined && CKEDITOR.instances != undefined) {
							for (instance in CKEDITOR.instances) {
								$('#'+instance).val(CKEDITOR.instances[instance].getData());
								for(i=0;i<data.length;i++) if(data[i].name == instance) data[i].value = CKEDITOR.instances[instance].getData();
							}
						}
					}catch(err){}
					return true;
				},
				success: function(responseText, statusText, xhr, $form){
					if (responseText.success) {
						dialog.modal('hide');
						$('.modal-backdrop').remove();
						$.post(opt.renderurl.replace(/{lang}/g,opt.lang), {id: widgetid}, function( d ) {
                            $this.lucidEditor("onRender",d);
						});
					}
				}
			});//End ajaxform
            return this;
		},

		/**
		 * Process JS & CSS ressources returned by AJAX calls
		 *
		 * @public
		 * @memberOf lucidEditor
		 * @function
		 */
		processRessources: function(ressources) {
            console.info("lucidEditor(processRessources)");
			var $this = $(this);
			var ressources_inputname = $this.lucidEditor("getRessourcesInputName");
			if (typeof ressources != 'array' && typeof ressources != 'object') return;
			var res = $this.children('.output').children('#'+ressources_inputname).val();
			if (typeof(res) != 'string') res = new Array();
			try{
				res = JSON.parse(res);
			}
			catch(e){
				console.info("lucidEditor::processRessources : Unable to parse ressources string!")
				res = new Array();
			}
			var found = false, code = '';
			if (typeof ressources.files == 'array' || typeof ressources.files == 'object') {
				for (var k = 0; k < ressources.files.length; k++) {
					found = false;
					for (var i = 0; i < res.length; i++) {
						if (ressources.files[k]['ressource'] == res[i]['ressource']) found = true;
					}
					if (!found) res[res.length] = ressources.files[k];
				}
			}
			$this.children('.output').children('#'+ressources_inputname).val(JSON.stringify(res));
			var res = $this.children('.output').children('#'+ressources_inputname).val();
		},

		/**
		 * updateCellEvents
		 *
		 * @public
		 * @memberOf lucideditor
		 * @function
		 */
		updateCellEvents: function() {
			console.info("lucidEditor.updateCellEvents()");
			var $this = $(this);
			var opt = $this.data("options");

            //Save currently selected content block...
			var oldSel = $this.lucidEditor("getSelectedContent");

			$this.lucidEditor("getFiltered","div.cell > div.content")
                .removeClass("selected")
				.off()
				.click(function(){
                    console.info("event-->lucidEditor.updateCellEvents.click");
					$this.find(".canvas > .row > .cell.selected").removeClass("selected").children(".content").removeClass("selected");
					$(this).addClass("selected").parent().addClass("selected");
					var newSel = $this.lucidEditor("getSelectedContent");
					$this.lucidEditor("positionHoverTools");
					if ($(this).hasClass("cte") && $(this).hasClass("cke_editable") == false) {
						console.info("msg-->lucidEditor.updateCellEvents.click: invoking CKEDitor & update");
						$this.lucidEditor("invokeCKEditor");
						$this.lucidEditor("update");
					}
                    if ($(this).hasClass("cte")) {
                        try{$this.children(".canvas").sortable("disable");}catch(e){}
                    }else {
                        try{$this.children(".canvas").sortable("enable");}catch(e){}
                    }
                    return false;
				})
				.dblclick(function(){
                    console.info("event-->lucidEditor.updateCellEvents.dblclick");
					$(this).click();
					if ($(this).hasClass("cte") == false && $(this).hasClass("cke_editable") == false) {
						$this.lucidEditor("onEdit");
					}
                    return false;
				});

            //Restore selection...
            $(oldSel).addClass("selected").parent().addClass("selected");

			$this.lucidEditor("getFiltered","div.cell > div.content.cte")
				.attr("contenteditable","true");

            //Disable links in content blocks...
			$this.lucidEditor("getFiltered","div.cell").find(".content:not(.cte), a").click(function(e){
                e.preventDefault();
                return false;
            });

			//Drag N Drop functionnality requires jquery-ui.js
            try{
                $this.children(".canvas").sortable({
                    containment:$this.children(".canvas"),
                    delay:100,
                    items: '> .row',
                    update: function(event, ui) {
                        $this.lucidEditor("update");
                    }
                });
                /**
                 * @TODO Drag n drop of cells elements do not work at the moment.
                 */
                /*var sortables = $this.children(".canvas").children(".row");
                sortables.sortable({
                    containment:$this.children(".canvas"),
                    delay:100,
                    connectWith: sortables,
                    items: '> .cell',
                    start: function(e,ui){
                        console.info("test drapui");
                        e.stopPropagation();
                    }
                });
                */
            }
            catch(e){
                console.info("lucidEditor.updateCellEvents --> Could not initialize drag'n'drop. Include jquery-ui.")
            }
			return this;
		},

		/**
		 * Positions the cell hover tools correctly.
		 *
		 * @public
		 * @memberOf LucidEditor
		 * @function
		 */
		positionHoverTools: function(){
            console.info("lucidEditor(positionHoverTools)");
			var $this = $(this);
			var opt = $this.data("options");
			var cell = $this.lucidEditor("getSelectedCell");
			var ct = $this.lucidEditor("getSelectedContent");
			var el = $("body .lucideditor_hovertools_"+opt.name);
			var el2 = $("body .lucideditor_hovertoolsct_"+opt.name);
			var destination = $(cell).offset();
			var destinationct = $(ct).offset();
			if (destinationct == undefined) destinationct = destination;
			el.css({
				position: "absolute",
				display:'block',
				top: destination.top + $(cell).height() + parseInt($(this).css('margin-top').replace('px', '')) - el.height() - 1,
				left: destination.left + parseInt($(this).css('margin-left').replace('px', ''))
			});
			el2.css({
				position: "absolute",
				display:'block',
				top: destinationct.top + parseInt($(this).css('margin-top').replace('px', '')),
				left: destinationct.left + parseInt($(this).css('margin-left').replace('px', '') + ct.width()) - el2.width()
			});

		},

		/**
		 * Gets or set the current  Editor value
		 *
		 * @public
		 * @memberOf lucideditor
		 * @function
		 */
		value: function(val) {
            console.info("lucidEditor.value("+val+")");
			var $this = $(this);
			var opt = $this.data("options");
			if (typeof val != undefined) {
				$(this).children('.output').children('textarea#'+opt.name).val(val).trigger("change");
				var valopt = $(this).data('options');
				valopt.value = val;
				$(this).data('options',valopt);
				return this;
			}
			return $(this).children('.output').children('textarea#'+opt.name).val();
		},

		/**
		 * Gets an option value
		 *
		 * @public
		 * @memberOf Lucideditor
		 * @function
		 */
		option: function(o,v) {
            console.info("lucidEditor.option()");
			if (typeof v == undefined) {
				return $(this).data(o);
			}
			return $(this).data(o,v);
		},

		/**
		 * Cleans up the markup code to remove any jquery-ui classes and events on the markup
		 *
		 * @public
		 * @memberOf LucidEditor
		 * @returns void
		 */
		sanitize: function(){
			console.info("lucidEditor.sanitize()");
			var $this = $(this);
            var canvas = $this.children(".canvas");
            var rows = canvas.children(".row");
            var cells = rows.children(".cell");
            var rmClasses = 'ui-sortable ui-sortable-handle ui-draggable ui-droppable ui-draggable-handle ui-draggable-dragging';
			cells.off()
                .removeClass(rmClasses)
                .removeAttr("style");
            cells.children(".content").off().removeAttr("aria-describedby title aria-label role spellcheck tabindex contenteditable style")
                .removeClass("cke_editable cke_editable_inline cke_contents_ltr cke_show_borders");
			rows.off().removeClass(rmClasses).removeAttr("style").each(function(){
					if ($(this).children().length == 0) {
						$(this).remove();
					}
				});
            canvas.children(".row:empty").remove();
            /*if (typeof (cells.sortable) == 'function') cells.sortable("destroy");
            if (typeof (rows.sortable) != 'function') rows.sortable("destroy");
            if (typeof (canvas.sortable) != 'function') canvas.sortable("destroy");*/
			return this;
		},

		/**
		 * Rounds a number up to X decimals
		 *
		 * @public
		 * @memberOf LucidEditor
		 * @param float nb The number to round
		 * @param int decimals The number of decimals up to which nb should be rounded
		 *
		 * @returns float The rounded floating point value
		 */
		roundTo: function(nb,decimals) {
            console.info("lucidEditor.roundTo("+nb+","+decimals+")");
			return +(Math.round(nb + "e+" + decimals)  + "e-" + decimals);
		},

		/**
		 * Creates a new table inside a cell
		 *
		 * @public
		 * @memberOf LucidEditor
		 * @param Object cell
		 */
		getNewCellCode: function(cols){
            console.info("lucidEditor.getNewCellcode("+cols+")");
			return '<div class="cell col-sm-'+cols+'"><div class="content"></div></div>';
		},

		/**
		 * Inserts a new cell or row before cell/row element specified
		 *
		 * @public
		 * @memberOf LucidEditor
		 * @param Object cell
		 */
		insertBefore: function(cellOrRow){
			console.info("lucidEditor.insertBefore("+cellOrRow+")");
            var $this = $(this);
            var cellw = $this.lucidEditor("getNewCellWidth",cellOrRow);
			if (cellw) {
                var cell = $this.lucidEditor("getNewCellCode",cellw);
                if (cellOrRow.hasClass("row")) cell = '<div class="row">'+cell+'</div>';
                cellOrRow.before(cell);
                $this.lucidEditor("update",cellOrRow);
            }
			return this;
		},

		/**
		 * Inserts a new cell or row after cell/row element specified
		 *
		 * Behavior :
		 * - For new row, inserts a row and a full width cell
		 * - For existing row, adds a cell the same width as the previous one
		 *
		 * @public
		 * @memberOf LucidEditor
		 * @param Object cell
		 */
		insertAfter: function(cellOrRow){
            console.info("lucidEditor.insertAfter("+cellOrRow+")");
			var $this = $(this);
            var cellw = $this.lucidEditor("getNewCellWidth",cellOrRow);
            if (cellw) {
                var cell = $this.lucidEditor("getNewCellCode",cellw);
                if (cellOrRow.hasClass("row")) cell = '<div class="row">'+cell+'</div>';
                cellOrRow.after(cell);
                $this.lucidEditor("update",cellOrRow);
            }
			return this;
		},

        /**
         * Returns the new cell width
         *
         * @public@memberOf LucidEditor
         * @param Object cell
         */
        getNewCellWidth: function(cellOrRow) {
            console.info("lucidEditor.getNewCellWidth("+cellOrRow+")");
			var $this = $(this);
            var opt = $this.data("options");

            if (cellOrRow.hasClass("row")) return opt.gridsize;

            var availWidth = opt.gridsize;
            cellOrRow.parent().children().each(function(){
                availWidth -= $this.lucidEditor("getCellWidth",$(this));
            });
            var cellw = $this.lucidEditor("getCellWidth",cellOrRow);
            if (availWidth < cellw) cellw = availWidth;
            if (cellw < 0) cellw = 0;
            return cellw;
        },

		/**
		 * Removes an existing cell or row and its content
		 *
		 * @public
		 * @memberOf LucidEditor
		 * @param Object cellOrRow
		 */
		remove: function(){
            console.info("lucidEditor.remove()");
			var $this = $(this);
			var cell = $this.lucidEditor("getSelectedCell");
			var opt = $this.data("options");
			if ($this.children(".canvas").find(".cell").length <= 1) {
				return this;
			}
			if (cell.hasClass("cell")) {
				cell.remove();
				if (cell.parent(".row").children(".cell").length == 0) cell.parent(".row").remove();
			}
			var hasSelected = $this.lucidEditor("getSelectedCell");
			if (hasSelected.length == 0) {
				if (cell.prev(".cell").length > 0) cell.prev(".cell").addClass("selected").trigger("click");
				if (cell.next(".cell").length > 0) cell.next(".cell").addClass("selected").trigger("click");
			}
			$("body .lucideditor_hovertools_"+opt.name).css("display","none");
			$("body .lucideditor_hovertoolsct_"+opt.name).css("display","none");
			$this.lucidEditor("update",cell);
			return this;
		},


		removeContent: function(){
            console.info("lucidEditor.removeContent()");
			var $this = $(this);
			var opt = $this.data("options");
			var cellsleft = $this.children(".canvas").find(".cell").length;
			var el = $this.lucidEditor("getSelectedContent");
			if (cellsleft <= 1) {
				el.parent().html("<div class=\"content\"></div>");
			}else {
				if (el.parent().children().length > 1) el.remove();
				else el.parent().html("<div class=\"content\"></div>");
			}
			var hasSelected = $this.lucidEditor("getSelectedContent");
			$("body .lucideditor_hovertools_"+opt.name).css("display","none");
			$("body .lucideditor_hovertoolsct_"+opt.name).css("display","none");
			$this.lucidEditor("update");
			return this;
		},

		/**
		 * Gets the first cell element that is a parent of specified element
		 *
		 * @public
		 * @memberOf LucidEditor
		 * @param Object el
		 */
		getCell: function(el){
            console.info("lucidEditor.getCell("+el+")");
			if (!el.hasClass("cell")) {
				return el.parents('.cell').eq(0);
			}else return el;
		},

		/**
		 * Gets the first row element that is a parent of specified element
		 *
		 * @public
		 * @memberOf LucidEditor
		 * @param Object el
		 */
		getRow: function(el){
            console.info("lucidEditor.getRow("+el+")");
			if (!el.hasClass("row")) {
				return el.parents('.row').eq(0);
			}else return el;
		},

		/**
		 * Gets the number of cols in current row element
		 *
		 * @public
		 * @memberOf LucidEditor
		 * @param Object row
		 */
		getRowWidth: function(row){
            console.info("lucidEditor.getRowWidth("+row+")");
			var $this = $(this);
			if (!row.hasClass('row')) {
				return 0;
			}
			var count = 0;
			var cells = row.children(".cell").each ( function () {
				count += $this.lucidEditor("getCellWidth",$(this));
			});
			return count;
		},

		/**
		 * Gets the width of cell element
		 *
		 * @public
		 * @memberOf LucidEditor
		 * @param Object cell
		 */
		getCellWidth: function(cell){
            console.info("lucidEditor.getCellWidth("+cell+")");
			var $this = $(this);
			if (!cell.hasClass('cell')) {
				return 0;
			}
			var count = 0;
			var elClasses = $( cell ).attr ( 'class' ).split ( ' ' );
			for ( var index in elClasses ) {
				if ( elClasses[index].match ( /^col-sm-\d+$/ ) ) {
					var classNum = elClasses[index].split ( '-' )[2];
					count = classNum;
					break;
				}
			}
			return count;
		},

		getSelectedCell: function() {
            console.info("lucidEditor.getSelectedCell()");
			var $this = $(this);
			var el = $this.lucidEditor("getFiltered",".cell.selected");
			return el.eq(0);
		},

		getSelectedContent: function() {
            console.info("lucidEditor.getSelectedContent()");
			var $this = $(this);
			var el = $this.lucidEditor("getFiltered",".cell.selected > .content.selected");

            //No selected content found.
            //Create new row at end of document, and return it.
            if (el.length == 0) {
                $this.find(".canvas > .row:last-child > .cell:last-child").eq(0).addClass("selected").children(".content").addClass("selected");
                el = $this.lucidEditor("getFiltered",".cell.selected .content.selected");
            }
			return el.eq(0);
		},

		getFiltered: function(selector){
            console.info("lucidEditor.getFiltered("+selector+")");
			var $this = $(this);
			return $this.children(".canvas").find(selector).filter(':withoutparent(.content)').filter(':withoutparent(section)');
		},

		resetCellWidth: function(cell){
            console.info("lucidEditor.resetCellWidth("+cell+")");
			var $this = $(this);
			cell.attr('class',function(i, c){
				return c.replace(/(^|\s)col-\S+/g, '');
			});
			return this;
		},

		invokeCKEditor: function(){
            console.info("lucidEditor(invokeCKEditor)");
			var $this = $(this);
			var sel = $this.lucidEditor("getSelectedContent");
			var opt = $this.data("options");
			try{
				if (sel.hasClass("widget") == false && sel.hasClass("cte") && CKEDITOR != undefined) {
					$(sel).attr("contenteditable","true");
					$(sel).attr("id","lucideditor-ckeditor");
					CKEDITOR.inline( sel[0], {
                        forcePasteAsPlainText: true,
						extraAllowedContent: 'a(documentation);abbr[title];code',
						removePlugins: 'glyphicons,showblocks,flash,templates,widget,locationmap,NonverBlaster,grid,herounit,faq',
                        customConfig: opt.url+'/com/system/editor/ckeditor/4.5.9/config.js',
						startupFocus: true,
						language: opt.lang,
						filebrowserBrowseUrl: opt.filebrowserBrowseUrl,
						filebrowserImageBrowseUrl: opt.filebrowserImageBrowseUrl,
						filebrowserUploadUrl: opt.filebrowserUploadUrl,
						filebrowserImageUploadUrl: opt.filebrowserImageUploadUrl,
					} );
                    CKEDITOR.instances["lucideditor-ckeditor"].on('blur', function(){
                        $this.lucidEditor("update");
                        $(sel).removeAttr("aria-describedby title aria-label role spellcheck tabindex contenteditable style")
                            .removeClass("cke_editable cke_editable_inline cke_contents_ltr cke_show_borders");
                        CKEDITOR.instances["lucideditor-ckeditor"].destroy();
                    });
                    if ($this.parents("form")) {
                        $this.parents("form").find("button[type=submit]").click(function(e){
                            $this.lucidEditor("update");
                            $(sel).removeAttr("aria-describedby title aria-label role spellcheck tabindex contenteditable style")
                                .removeClass("cke_editable cke_editable_inline cke_contents_ltr cke_show_borders");
                            if (CKEDITOR.instances["lucideditor-ckeditor"]) {
                                CKEDITOR.instances["lucideditor-ckeditor"].destroy();
                            }
                        });
                    }
				}
			}catch(err){}
		},

		addEditableContent: function(){
			console.info("lucidEditor.addEditableContent()");
			var $this = $(this);
			var opt = $this.data("options");
			var sel = $this.lucidEditor("getSelectedContent");
			var ct = "<div class=\"content cte\" contenteditable=\"true\">"+$this.lucidEditor("getLang","placeholdertext")+"</div>";
			if (sel) {
				if (sel.hasClass("cte")) {
					sel.attr("contenteditable","true");
					return this;
				}
				if (!sel.hasClass("cte") && !sel.hasClass("widget")) {
					$(sel).attr('contenteditable','true').addClass("cte");
					var editor = sel;
				}else {
					$(sel).after(ct);
					var editor = $(sel).next();
				}
			}
			else {
				sel = $this.lucidEditor("getSelectedCell");
				if (sel) {
					sel.append(ct);
					var editor = $(sel).next();
				}
			}
			$this.lucidEditor("invokeCKEditor");
			return this;
		},

		/**
		 * Returns a translation of specified string ID
		 * @param String id The string ID to translate
		 * @returns String The given string translated into the editor current language
		 */
		getLang: function(id) {
            console.info("lucidEditor.getLang("+id+")");
			var $this = $(this);
			var opt = $this.data("options");
			var lang = {
				fr: {
					undo: 'Annuler',
					redo: 'R&eacute;tablir',
					grid: 'Grille',
					cell: 'Cellule',
					cellinsertbefore: 'Ins&eacute;rer une cellule &agrave; gauche',
					cellinsertafter: 'Ins&eacute;rer une cellule &agrave; droite',
					celldelete: 'Supprimer',
					row: 'Ligne',
					rowinsertbefore: 'Ins&eacute;rer une cellule au dessus',
					rowinsertafter: 'Ins&eacute;rer une cellule sous',
					rowdelete: 'Supprimer',
					widgetadd: 'Ajouter un widget',
					widgetedit: 'Modifier',
					widgetdelete: 'Supprimer',
					widgetrefresh: 'Rafra&icirc;chir',
					source: 'Afficher le code source HTML',
					sourcecode: 'Code source',
					resssources: 'Ressources',
					fullscreen: 'Plein &eacute;cran',
					placeholdertext: 'Tappez le texte ici.',
					sampletext: 'Exemple de texte',
					textblock: 'Bloc texte'
				},
				en: {
					undo: 'Undo last operation',
					redo: 'Redo operation',
					grid: 'Grid',
					cell: 'Cell',
					cellinsertbefore: 'Insert cell before',
					cellinsertafter: 'Insert cell after',
					celldelete: 'Remove',
					row: 'Row',
					rowinsertbefore: 'Insert cell on top',
					rowinsertafter: 'Insert cell below',
					rowdelete: 'Remove',
					widgetadd: 'Add widget',
					widgetedit: 'Edit',
					widgetdelete: 'Remove',
					widgetrefresh: 'Refresh',
					source: 'Show HTML source code',
					sourcecode: 'Source code',
					resssources: 'Ressources',
					fullscreen: 'Fullscreen mode',
					placeholdertext: 'Type here.',
					sampletext: 'Sample text',
					textblock: 'Text block'
				}
			}
			try{
				if (lang[opt.lang]) {
					return lang[opt.lang][id];
				}
				else return lang['en'][id];
			} catch(err) {}
		},

		/**
		 * Destroys lucidEditor instance. Removes any trace of it.
		 * @returns {Object}  Description
		 */
		destroy: function(){
            console.info("lucidEditor.destroy()");
			var $this = $(this);
			var opt = $this.data("options");
			var hoverTools = $("body .lucideditor_hovertools_"+opt.name+",body .lucideditor_hovertoolsct_"+opt.name);
			$(hoverTools).children().off();
			$(hoverTools).remove();
            $this.lucidEditor("getDialog").off().remove();
			/*$this.find("*").off();*/
			$this.remove();
		},

		/**
		 * Returns the ressources input name
		 *
		 * @returns {String} Input name
		 */
		getRessourcesInputName: function(){
            console.info("lucidEditor.getressourceInputName()");
			var $this = $(this);
			var opt = $this.data("options");
			var ressources_inputname = opt.name;
			if (opt.name.slice(opt.lang.length * -1) == opt.lang) {
				ressources_inputname = ressources_inputname.slice(0,opt.lang.length * -1-1)+"_ressources_"+opt.lang;
			}
			else ressources_inputname += "_ressources";
			return ressources_inputname;
		},

        onRender: function(d) {
            console.info("lucidEditor.onRender()");
			var $this = $(this);
			var opt = $this.data("options");
            if (d.ressources) $this.lucidEditor("processRessources",d.ressources);
            var elsel = $this.find(".canvas > .row > .cell > div#widget_"+d.type+"_"+d.name+"_"+d.id);

            //1. Find if widget already exists
            if (elsel.length > 0) {
                elsel.eq(0).addClass("widget");
                elsel.eq(0).html(d.src/*Removed: causes code/pre tags to be displayed on one line : .replace(/\r?\n|\r/g,"")*/);
            }else {
                elsel = $this.lucidEditor("getSelectedContent");
                elsel.after("<div id=\"widget_"+d.type+"_"+d.name+"_"+d.id+"\" class=\"content widget\">"+d.src/*Removed: causes code/pre tags to be displayed on one line : .replace(/\r?\n|\r/g,"")*/+"</div>");
                if (elsel.attr("id") == null || elsel.attr("id") == undefined || elsel.attr("id") == '') {
                    elsel.remove();
                }
            }
            /*if (elsel.attr("id") && elsel.attr("id") == "widget_"+d.type+"_"+d.name+"_"+d.id) {
                elsel.attr("id","widget_"+d.type+"_"+d.name+"_"+d.id).addClass("widget");
                elsel.html(d.src.replace(/\r?\n|\r/g,""));
            }else {
                elsel.after("<div id=\"widget_"+d.type+"_"+d.name+"_"+d.id+"\" class=\"content widget\">"+d.src.replace(/\r?\n|\r/g,"")+"</div>");
                if (elsel.attr("id") == null || elsel.attr("id") == undefined || elsel.attr("id") == '') {
                    elsel.remove();
                }
            }*/
            $this.lucidEditor("update");
            $this.lucidEditor("positionHoverTools");
        },

        onEdit: function(){
            var $this = $(this);
			var opt = $this.data("options");
            var el = $this.lucidEditor("getSelectedContent");
            if(el.attr("id") == undefined){
                $this.lucidEditor("listWidgets");
            }else {
                var id = el.attr("id").split("_");
                if (id[4]) id[3] += '_'+id[4];
                $this.lucidEditor("editWidget",id[2], id[3]);
            }
        },
        onSubmit: function(event){
            console.info("lucidEditor.onSubmit()");
            var $this = $(this);
			var opt = $this.data("options");
            $this.lucidEditor("update");
        }
	};

	$.fn.lucidEditor = function(method) {
		if (methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.lucidEditor' );
		}
	};

})(jQuery);
