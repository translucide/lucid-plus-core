/**
 * LucidInterface jQuery Plugin
 *
 * Provides methods to quickly build a JS Ajx driven application
 *
 * @access public
 * @example $('div.app.lucidinterface').lucidInterface({
 *      initScreen: 'default',
 *      initComponent: 'default',
 *      initController: 'default',
 *      root: '',
 *      lang: '',
 * });
 **/
(function( $ ) {	
	var methods = {
		init: function(options) {
			var $this = $(this);
			$this.data('options', $.extend({
                'initScreen': 'default',
                'initComponent': 'default',
                'initController': 'default',
                'root': '',
                'lang': ''
            }, options));
            
            /**
             * Auto save queue. Fields that must be saved will be queued here,
             * and queue will be processed at a given rhythm to prevent
             * multiple saves or missed saves
             **/            
            $this.data("autosaveQueue",[]);
            $this.data("autocreateQueue",[]);
			$this.lucidInterface("setup");
			return this;
		},
		setup: function(){
			var $this = $(this);
            var opt = $this.data('options');
            
            /**
             * Clears modal data on hide
             */
            $('body').on('hidden.bs.modal', '.modal', function () {
              $(this).removeData('bs.modal');
            });
            
            if ($("#lucidinterface_dialog").length === 0) {
                $("body").append("<div id=\"lucidinterface_dialog\" class=\"modal fade\"><div class=\"modal-dialog\"><div class=\"modal-content\"></div></div></div>");
            }
            if ($("#lucidinterface_modaldialog").length === 0) {
                $("body").append("<div id=\"lucidinterface_modaldialog\" class=\"modal fade\"><div class=\"modal-dialog\"><div class=\"modal-content\"></div></div></div>");
            }
            if ($("#lucidinterface_messagedialog").length === 0) {
                $("body").append("<div id=\"lucidinterface_messagedialog\" class=\"modal fade\"><div class=\"modal-dialog\"><div class=\"modal-content\"></div></div></div>");
            }
            
            /**
             * Listen for hash change and load corresponding screen
             **/
            window.addEventListener("hashchange", function(){
                $this.lucidInterface("changeScreenForHash");
            });
            
            /**
            * Loads default initial screen
            */
            if ($this.lucidInterface("changeScreenForHash") === false) {
                $this.lucidInterface("loadScreen",opt.initComponent, opt.initController, opt.initScreen);
            }
            
            //Start processing autosave 1.5sec after page load.
            setTimeout(function(){
                $this.lucidInterface("processAutosaveQueue");
            },1500);
            
            //Start processing autocreate 1.5sec after page load.
            setTimeout(function(){
                $this.lucidInterface("processAutocreateQueue");
            },1500);
            
            /**
             * Application hotkeys used across all screens
             *
             * @access public
             */
            $(document).bind('keydown', 'left', function(){
                //Move focus to previous column input is focussed in a datatable
                var inputs = $this.find('table.dataTable input:focus,table.dataTable select:focus,table.dataTable button:focus, table.dataTable textarea:focus');
                inputs = inputs.not(".chosen-search-input");
                if (inputs.length > 0  && getCaretPosition(inputs.eq(0)) === 0) {
                    var otherinputs = inputs.eq(0).closest("td").find("input[type=\"text\"],select,button");
                    var index = otherinputs.index(inputs.eq(0));
                    if (index > 0) {
                        otherinputs.eq(index-1).focus();
                    }
                    else {
                        var prevInput = inputs.closest("td").eq(0).prev("td").find("input[type=\"text\"],select,button");
                        while (prevInput.length === 0){
                            if (prevInput.closest("td").eq(0).prev("td").length === 0) break;
                            prevInput = prevInput.closest("td").eq(0).prev("td").find("input[type=\"text\"],select,button");
                        }
                        if (prevInput.length) prevInput.focus();
                    }
                }
            }); 
            $(document).bind('keydown', 'right', function() {
                //Move focus to previous column input is focussed in a datatable
                var inputs = $this.find('table.dataTable input[type="text"]:focus,table.dataTable select:focus,table.dataTable button:focus, table.dataTable textarea:focus');
                inputs = inputs.not(".chosen-search-input");
                if (inputs.length > 0 && getCaretPosition(inputs.eq(0),'right') === inputs.eq(0).val().length) {
                    console.info("input length= "+inputs.eq(0).val().length);
                    var otherinputs = inputs.eq(0).closest("td").find("input[type=\"text\"],select,button");
                    var index = otherinputs.index(inputs.eq(0));
                    console.info("input index = "+index+', over nb inputs ='+otherinputs.length);
                    if (index < otherinputs.length-1) {
                        otherinputs.eq(index+1).focus();
                    }
                    else inputs.closest("td").eq(0).next("td").find("input,select,button").focus();
                }
            });
            $(document).bind('keydown', 'up', function(){
                //Move focus to previous column input is focussed in a datatable
                var inputs = $this.find('table.dataTable input[type="text"]:focus,table.dataTable select:focus,table.dataTable button:focus');
                inputs = inputs.not(".chosen-search-input");
                if (inputs.length > 0) {
                    var index = inputs.eq(0).closest("tr").children("td").index(inputs.eq(0).closest("td").eq(0));
                    inputs.closest("tr").prev("tr").children("td:nth-child("+(index+1)+")").find("input[type=\"text\"],select,button").focus();
                }
            });
            $(document).bind('keydown', 'down', function(){
                //Move focus to previous column input is focussed in a datatable
                var inputs = $this.find('table.dataTable input[type="text"]:focus,table.dataTable select:focus,table.dataTable button:focus, table.dataTable textarea:focus');
                inputs = inputs.not(".chosen-search-input");
                console.info(inputs);
                if (inputs.length > 0) {
                    var index = inputs.eq(0).closest("tr").children("td").index(inputs.eq(0).closest("td").eq(0));
                    inputs.closest("tr").next("tr").children("td:nth-child("+(index+1)+")").find("input[type=\"text\"],select,button").focus();
                }
            });
            $.hotkeys.options.filterInputAcceptingElements = false;
            $.hotkeys.options.filterTextInputs = false;            
			return this;
		},

        /**
         * Loads a screen in main app area
         *
         * @param string screen
         * @param int recordId
         * @return string HTML Html markup
         **/
        'loadScreen' : function(component,controller,screen,data){
            var $this = $(this);
            var opt = $this.data('options');
            if ($('#lucidinterface_dialog').hasClass('in')) $('#lucidinterface_dialog').modal('hide');
            if ($('#lucidinterface_modaldialog').hasClass('in')) $('#lucidinterface_modaldialog').modal('hide');
            if (typeof data == "undefined") data = {};
            if (typeof data != 'object') data = {'id':data};
            data.screen=screen;
            $this.html('<div class="loader"><img src="data:image/gif;base64,R0lGODlhIAAgAPMAAP///wAAAMbGxoSEhLa2tpqamjY2NlZWVtjY2OTk5Ly8vB4eHgQEBAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAIAAgAAAE5xDISWlhperN52JLhSSdRgwVo1ICQZRUsiwHpTJT4iowNS8vyW2icCF6k8HMMBkCEDskxTBDAZwuAkkqIfxIQyhBQBFvAQSDITM5VDW6XNE4KagNh6Bgwe60smQUB3d4Rz1ZBApnFASDd0hihh12BkE9kjAJVlycXIg7CQIFA6SlnJ87paqbSKiKoqusnbMdmDC2tXQlkUhziYtyWTxIfy6BE8WJt5YJvpJivxNaGmLHT0VnOgSYf0dZXS7APdpB309RnHOG5gDqXGLDaC457D1zZ/V/nmOM82XiHRLYKhKP1oZmADdEAAAh+QQJCgAAACwAAAAAIAAgAAAE6hDISWlZpOrNp1lGNRSdRpDUolIGw5RUYhhHukqFu8DsrEyqnWThGvAmhVlteBvojpTDDBUEIFwMFBRAmBkSgOrBFZogCASwBDEY/CZSg7GSE0gSCjQBMVG023xWBhklAnoEdhQEfyNqMIcKjhRsjEdnezB+A4k8gTwJhFuiW4dokXiloUepBAp5qaKpp6+Ho7aWW54wl7obvEe0kRuoplCGepwSx2jJvqHEmGt6whJpGpfJCHmOoNHKaHx61WiSR92E4lbFoq+B6QDtuetcaBPnW6+O7wDHpIiK9SaVK5GgV543tzjgGcghAgAh+QQJCgAAACwAAAAAIAAgAAAE7hDISSkxpOrN5zFHNWRdhSiVoVLHspRUMoyUakyEe8PTPCATW9A14E0UvuAKMNAZKYUZCiBMuBakSQKG8G2FzUWox2AUtAQFcBKlVQoLgQReZhQlCIJesQXI5B0CBnUMOxMCenoCfTCEWBsJColTMANldx15BGs8B5wlCZ9Po6OJkwmRpnqkqnuSrayqfKmqpLajoiW5HJq7FL1Gr2mMMcKUMIiJgIemy7xZtJsTmsM4xHiKv5KMCXqfyUCJEonXPN2rAOIAmsfB3uPoAK++G+w48edZPK+M6hLJpQg484enXIdQFSS1u6UhksENEQAAIfkECQoAAAAsAAAAACAAIAAABOcQyEmpGKLqzWcZRVUQnZYg1aBSh2GUVEIQ2aQOE+G+cD4ntpWkZQj1JIiZIogDFFyHI0UxQwFugMSOFIPJftfVAEoZLBbcLEFhlQiqGp1Vd140AUklUN3eCA51C1EWMzMCezCBBmkxVIVHBWd3HHl9JQOIJSdSnJ0TDKChCwUJjoWMPaGqDKannasMo6WnM562R5YluZRwur0wpgqZE7NKUm+FNRPIhjBJxKZteWuIBMN4zRMIVIhffcgojwCF117i4nlLnY5ztRLsnOk+aV+oJY7V7m76PdkS4trKcdg0Zc0tTcKkRAAAIfkECQoAAAAsAAAAACAAIAAABO4QyEkpKqjqzScpRaVkXZWQEximw1BSCUEIlDohrft6cpKCk5xid5MNJTaAIkekKGQkWyKHkvhKsR7ARmitkAYDYRIbUQRQjWBwJRzChi9CRlBcY1UN4g0/VNB0AlcvcAYHRyZPdEQFYV8ccwR5HWxEJ02YmRMLnJ1xCYp0Y5idpQuhopmmC2KgojKasUQDk5BNAwwMOh2RtRq5uQuPZKGIJQIGwAwGf6I0JXMpC8C7kXWDBINFMxS4DKMAWVWAGYsAdNqW5uaRxkSKJOZKaU3tPOBZ4DuK2LATgJhkPJMgTwKCdFjyPHEnKxFCDhEAACH5BAkKAAAALAAAAAAgACAAAATzEMhJaVKp6s2nIkolIJ2WkBShpkVRWqqQrhLSEu9MZJKK9y1ZrqYK9WiClmvoUaF8gIQSNeF1Er4MNFn4SRSDARWroAIETg1iVwuHjYB1kYc1mwruwXKC9gmsJXliGxc+XiUCby9ydh1sOSdMkpMTBpaXBzsfhoc5l58Gm5yToAaZhaOUqjkDgCWNHAULCwOLaTmzswadEqggQwgHuQsHIoZCHQMMQgQGubVEcxOPFAcMDAYUA85eWARmfSRQCdcMe0zeP1AAygwLlJtPNAAL19DARdPzBOWSm1brJBi45soRAWQAAkrQIykShQ9wVhHCwCQCACH5BAkKAAAALAAAAAAgACAAAATrEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiRMDjI0Fd30/iI2UA5GSS5UDj2l6NoqgOgN4gksEBgYFf0FDqKgHnyZ9OX8HrgYHdHpcHQULXAS2qKpENRg7eAMLC7kTBaixUYFkKAzWAAnLC7FLVxLWDBLKCwaKTULgEwbLA4hJtOkSBNqITT3xEgfLpBtzE/jiuL04RGEBgwWhShRgQExHBAAh+QQJCgAAACwAAAAAIAAgAAAE7xDISWlSqerNpyJKhWRdlSAVoVLCWk6JKlAqAavhO9UkUHsqlE6CwO1cRdCQ8iEIfzFVTzLdRAmZX3I2SfZiCqGk5dTESJeaOAlClzsJsqwiJwiqnFrb2nS9kmIcgEsjQydLiIlHehhpejaIjzh9eomSjZR+ipslWIRLAgMDOR2DOqKogTB9pCUJBagDBXR6XB0EBkIIsaRsGGMMAxoDBgYHTKJiUYEGDAzHC9EACcUGkIgFzgwZ0QsSBcXHiQvOwgDdEwfFs0sDzt4S6BK4xYjkDOzn0unFeBzOBijIm1Dgmg5YFQwsCMjp1oJ8LyIAACH5BAkKAAAALAAAAAAgACAAAATwEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiUd6GGl6NoiPOH16iZKNlH6KmyWFOggHhEEvAwwMA0N9GBsEC6amhnVcEwavDAazGwIDaH1ipaYLBUTCGgQDA8NdHz0FpqgTBwsLqAbWAAnIA4FWKdMLGdYGEgraigbT0OITBcg5QwPT4xLrROZL6AuQAPUS7bxLpoWidY0JtxLHKhwwMJBTHgPKdEQAACH5BAkKAAAALAAAAAAgACAAAATrEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiUd6GAULDJCRiXo1CpGXDJOUjY+Yip9DhToJA4RBLwMLCwVDfRgbBAaqqoZ1XBMHswsHtxtFaH1iqaoGNgAIxRpbFAgfPQSqpbgGBqUD1wBXeCYp1AYZ19JJOYgH1KwA4UBvQwXUBxPqVD9L3sbp2BNk2xvvFPJd+MFCN6HAAIKgNggY0KtEBAAh+QQJCgAAACwAAAAAIAAgAAAE6BDISWlSqerNpyJKhWRdlSAVoVLCWk6JKlAqAavhO9UkUHsqlE6CwO1cRdCQ8iEIfzFVTzLdRAmZX3I2SfYIDMaAFdTESJeaEDAIMxYFqrOUaNW4E4ObYcCXaiBVEgULe0NJaxxtYksjh2NLkZISgDgJhHthkpU4mW6blRiYmZOlh4JWkDqILwUGBnE6TYEbCgevr0N1gH4At7gHiRpFaLNrrq8HNgAJA70AWxQIH1+vsYMDAzZQPC9VCNkDWUhGkuE5PxJNwiUK4UfLzOlD4WvzAHaoG9nxPi5d+jYUqfAhhykOFwJWiAAAIfkECQoAAAAsAAAAACAAIAAABPAQyElpUqnqzaciSoVkXVUMFaFSwlpOCcMYlErAavhOMnNLNo8KsZsMZItJEIDIFSkLGQoQTNhIsFehRww2CQLKF0tYGKYSg+ygsZIuNqJksKgbfgIGepNo2cIUB3V1B3IvNiBYNQaDSTtfhhx0CwVPI0UJe0+bm4g5VgcGoqOcnjmjqDSdnhgEoamcsZuXO1aWQy8KAwOAuTYYGwi7w5h+Kr0SJ8MFihpNbx+4Erq7BYBuzsdiH1jCAzoSfl0rVirNbRXlBBlLX+BP0XJLAPGzTkAuAOqb0WT5AH7OcdCm5B8TgRwSRKIHQtaLCwg1RAAAOwAAAAAAAAAAAA==" align="center"></div>');
            $.post(opt.root+opt.lang+'/api/'+component+'/controller/'+controller+'/loadscreen',
                data,function(d) {
                     $this.html(d);
                     $this.trigger("loadscreen.lucidinterface",d);
                }
            );
            return this;
        },
        
        /**
         * Loads a dialog in #lucidinterface_dialog div
         *
         * @param int recordId Optional record id to send along request
         * @return string HTML Html markup to inject in dialog
         **/
        'loadDialog': function(component,controller,screen,data) {
            var $this = $(this);
            var opt = $this.data('options');
            if (typeof data == "undefined") data = {};
            if (typeof data != 'object') data = {'id':data};
            $('#lucidinterface_dialog').modal({
                show: true,
                cache: false,
                backdrop: 'static',
                keyboard: false,
                remote: opt.root+opt.lang+'/api/'+component+'/controller/'+controller+'/loaddialog?screen='+screen+'&'+$.param(data)
            });
            data.screen = screen;
            $this.trigger("loaddialog.lucidinterface",data);
            $('#lucidinterface_dialog').modal('show');
            return this;
        },
        
        /**
         * Loads a dialog in #lucidinterface_modaldialog div
         *
         * @param int recordId Optional record id to send along request
         * @return string HTML Html markup to inject in dialog
         **/
        'loadModalDialog': function (component,controller,screen,data) {
            var $this = $(this);
            var opt = $this.data('options');
            if (typeof data == "undefined") data = {};
            if (typeof data != 'object') data = {'id':data};
            $('#lucidinterface_modaldialog').modal({
                show: true,
                cache: false,
                backdrop: 'static',
                keyboard: false,
                remote: opt.root+opt.lang+'/api/'+component+'/controller/'+controller+'/loaddialog?screen='+screen+'&'+$.param(data)
            });
            data.screen = screen;
            $this.trigger("loadmodaldialog.lucidinterface",data);
            $('#lucidinterface_modaldialog').modal('show');
            return this;
        },
        
        /**
         * Loads a message dialog. This is the highest level of "popup" the interface provides
         **/
        'loadMessageDialog': function (component,controller,screen,data) {
            var $this = $(this);
            var opt = $this.data('options');
            if (typeof data != 'object') data = {'id':data};
            $("#lucidinterface_messagedialog").removeClass("success error info warning").addClass(data.dialogtype);
            $('#lucidinterface_messagedialog').modal({
                show: true,
                cache: false,
                backdrop: 'static',
                keyboard: false,     
                remote: opt.root+opt.lang+'/api/'+component+'/controller/'+controller+'/loaddialog?screen='+screen+'&'+$.param(data)
            });
            data.screen = screen;
            $this.trigger("loadmessagedialog.lucidinterface",data);
            $('#lucidinterface_messagedialog').modal('show');
            return this;
        },
        
        /**
         * Outputs a message dialog with predefined button choices
         *
         * Available options :
         * type: Supported types are "error", "info", "warning", "success", "prompt"
         * title: Dialog title
         * message: Dialog text
         * actions:{'yes':function(){...}, 'no': true, ... }
         * }
         **/
        'messageDialog': function(options){
            var $this = $(this);
            $("#lucidinterface_messagedialog").removeClass("success error info warning prompt").addClass(options.type);
            $('#lucidinterface_messagedialog .modal-content').html(
                "<div class='modal-header'>"+
                    "<a type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></a><h4 class='modal-title'>"+options.title+"</h4>"+
                "</div>"+
                "<div class='modal-body'><p>"+options.message+"</p></div>"+
                "<div class='modal-footer'>"+
                    ((typeof options.actions.cancel == "undefined")?'':"<button type='cancel' id='cancel_btn' class='btn btn-default btn-default' data-dismiss='modal'><span class='glyphicon glyphicon-error'></span> Cancel</button>")+
                    ((typeof options.actions.no == "undefined")?'':"<button type='cancel' id='no_btn' class='btn btn-default btn-default' data-dismiss='modal'><span class='glyphicon glyphicon-error'></span> No</button>")+
                    ((typeof options.actions.close == "undefined")?'':"<button type='cancel' id='close_btn' class='btn btn-default btn-default' data-dismiss='modal'><span class='glyphicon glyphicon-error'></span> Close</button>")+
                    ((typeof options.actions.ok == "undefined")?'':"<button type='submit' id='ok_btn' class='btn btn-default btn-primary' data-dismiss='modal'><span class='glyphicon glyphicon-ok'></span> Ok</button>")+
                    ((typeof options.actions.yes == "undefined")?'':"<button type='submit' id='yes_btn' class='btn btn-default btn-primary' data-dismiss='modal'><span class='glyphicon glyphicon-ok'></span> Yes</button>")+
                "</div>"
            );
            var timeoutFnc = function(){
                if ($("#lucidinterface_messagedialog .modal-footer button").length > 0) {
                    $("#lucidinterface_messagedialog .modal-footer button").on("click",function(e){
                        visibledialogs = $("body .modal.in").length;
                        
                        $("body .modal-backdrop.fade.in").remove();
                        $("#lucidinterface_messagedialog").modal("hide");
                    });
                    if (typeof options.actions.cancel == "function") $('#lucidinterface_messagedialog button#cancel_btn').on('click',options.data,options.actions.cancel);
                    if (typeof options.actions.no == "function") $('#lucidinterface_messagedialog button#no_btn').on('click',options.data,options.actions.no);
                    if (typeof options.actions.yes == "function") $('#lucidinterface_messagedialog button#yes_btn').on('click',options.data,function(e){
                        options.actions.yes(e);
                    });
                    if (typeof options.actions.ok == "function") $('#lucidinterface_messagedialog button#ok_btn').on('click',options.data,options.actions.ok);
                    if (typeof options.actions.close == "function") $('#lucidinterface_messagedialog button#close_btn').on('click',options.data,options.actions.close);
                    
                }else setTimeout(timeoutFnc,25);
            };
            setTimeout(timeoutFnc,25);
            
            $('#lucidinterface_messagedialog').modal({
                show: true,
                cache: false,
                backdrop: 'static',
                keyboard: false
            });
            $('#lucidinterface_messagedialog').modal('show');
            return this;
        },

        /**
         * Checks for back button changes: loads corresponding screen to page change
         */
        'changeScreenForHash': function(){
            var $this = $(this);
            var hash = window.location.hash;
            hash = hash.split('/');
            if (hash.length == 5 && hash[0] == '#screen') {
                $this.lucidInterface("loadScreen",hash[1],hash[2],hash[3],hash[4]);
                return true;
            }
            return false;
        },
        
        /**
         * Sets the cursor to end of field
         */
        'positionCaretToEnd': function (el){
            var $this = $(this);
            var strLength = el.value.length;
            return function() {
                if(el.setSelectionRange !== undefined) {
                    el.setSelectionRange(strLength, strLength);
                } else {
                    $(el).val(el.value);
                }
            };
        },
        
        /**
         * Tries to return caret position optionaly based
         * on which side of the selection we are looking for
         **/
        'getCaretPosition': function (el,direction = 'left') {
            var $this = $(this);
            el = el[0];
            var position = 0;
            if (typeof el.selectionStart != "undefined" &&
                typeof el.selectionEnd != "undefined" &&
                el.selectionStart != el.selectionEnd) {
                if (direction == 'left') position = el.selectionStart;
                if (direction == 'right') position = el.selectionEnd;
            }else {
                position =el.selectionStart;
            }
            return parseInt(position);
        },        

        /**
         * Checks if an element is queued already
         *
         * @param Element el
         * @param string queueName 'autosave' or 'autocreate'
         * @return bool
         **/
        'isQueued' : function(el,queueName){
            var $this = $(this);
            el = $(el);
            queue = (queueName == 'autosave')?$this.data('autosaveQueue'):$this.data('autocreateQueue');
            var found = false;
            for(var i =0; i < queue.length; i++) {
                if (queue[i].fieldid == el.attr("id")) {
                    //We have an item in queue already, lets update it before its sent
                    queue[i].value = el.val();
                    queue[i].millis = new Date().getTime();
                    found = true;
                }
            }
            return found;
        },        

        /**
         * Packs a number of items to send over to be created or saved.
         *
         * It also removes them from the given queue
         *
         * @param string queuename
         * @return Array queueItems
         **/
        'getNextBatchFromQueue': function(queueName){
            var $this = $(this);
            var autosaveQueue = $this.data('autosaveQueue');
            var autocreateQueue = $this.data('autocreateQueue');
            var sentItems = [];
            if ((queueName == 'autosave' && autosaveQueue.length > 0) || (queueName == 'autocreate' && autocreateQueue.length > 0)) {
                var millis = new Date().getTime();
                var queueItem = (queueName == 'autosave')?autosaveQueue.shift():autocreateQueue.shift();
                var component = queueItem.component;
                var controller = queueItem.controller;
                var items = 0;
                while(queueItem && items < 10) {
                    if ($('#'+queueItem.fieldid).prev(".autocomplete:focus").length === 0 &&
                        $('#'+queueItem.fieldid+':focus').length === 0 &&
                        queueItem.component === component &&
                        queueItem.controller === controller &&
                        millis - queueItem.millis >= 750) {
                        sentItems.push(queueItem);
                        queueItem = (queueName == 'autosave')?autosaveQueue.shift():autocreateQueue.shift();
                        items++;
                    }else {
                        if (queueName == 'autosave') autosaveQueue.push(queueItem);
                        else autocreateQueue.push(queueItem);
                        break;
                    }
                }
            }            
            $this.data('autosaveQueue',autosaveQueue);
            $this.data('autocreateQueue',autocreateQueue);
            return sentItems;
        },
        
        'queueAutosaveItem': function(autosaveElement,isUrgent) {
            var $this = $(this);
            if (isUrgent) isUrgent = true;
            autosaveElement = $(autosaveElement);
            if ($this.lucidInterface("isQueued",autosaveElement,'autosave') === false) {
                var autosaveQueue = $this.data('autosaveQueue');
                autosaveQueue.push({
                    'millis': new Date().getTime()-((isUrgent)?1000:0),
                    'fieldid' : autosaveElement.attr("id"),
                    'component' : autosaveElement.attr("data-save-component"),
                    'controller' : autosaveElement.attr("data-save-controller"),
                    'model' : autosaveElement.attr("data-save-model"),
                    'id' : autosaveElement.attr("data-save-id"),
                    'field' : autosaveElement.attr("name"),
                    'value' : autosaveElement.val()
                });
                $this.data('autosaveQueue',autosaveQueue);
            }
            return this;
        },
        
        'processAutosaveQueue': function(){
            var $this = $(this);
            var opt = $this.data('options');
            sentItems = $this.lucidInterface("getNextBatchFromQueue",'autosave');
            if (sentItems.length > 0){
                var component = sentItems[0].component;
                var controller = sentItems[0].controller;
                $.post(opt.root+opt.lang+'/api/'+component+'/controller/'+controller+'/autosave',
                    {items: sentItems},
                    function(data){
                        if (data.success === true) {
                            $this.lucidInterface("notify",data.msg,'success');
                        }else {
                            //Put items back on queue since something went wrong.
                            $this.lucidInterface("notify","Saving failed! I will retry in a couple of seconds for you !",'error');
                            var autosaveQueue = $this.data('autosaveQueue');
                            autosaveQueue.push(sentItems);
                            $this.data('autosaveQueue',autosaveQueue);
                        }
                        console.info("autosave data saved successfully!");
                        console.info(data);
                    }
                );
            }
            //Recall every 0.8 second.
            setTimeout(function(){
                $this.lucidInterface("processAutosaveQueue");
            },400);
            return this;
        },
        
        'queueAutocreateItem': function(autocreateElement) {
            var $this = $(this);
            autocreateElement = $(autocreateElement);
            if (autocreateElement.attr("data-dirty") === "1" && $this.lucidInterface("isQueued",autocreateElement,'autocreate') === false) {
                var autocreateQueue = $this.data('autocreateQueue');
                autocreateQueue.push({
                    'millis': new Date().getTime(),
                    'fieldid' : autocreateElement.attr("id"),
                    'component' : autocreateElement.attr("data-autocomplete-component"),
                    'controller' : autocreateElement.attr("data-autocomplete-controller"),                    
                    'model' : autocreateElement.attr("data-autocomplete-model"),
                    'titlefield': autocreateElement.attr('data-autocomplete-titlefield'),
                    'valuefield': autocreateElement.attr('data-autocomplete-valuefield'),
                    'field' : autocreateElement.attr("name"),
                    'value' : autocreateElement.val()
                });
                $this.data('autocreateQueue',autocreateQueue);
            }
            return this;
        },
        
        /**
         * Process autocreate queue
         **/
        'processAutocreateQueue': function(){
            var $this = $(this);
            var opt = $this.data('options');
            sentItems = $this.lucidInterface("getNextBatchFromQueue",'autocreate');
            if (sentItems.length > 0){
                var component = sentItems[0].component;
                var controller = sentItems[0].controller;                
                $.post(opt.root+opt.lang+'/api/'+component+'/controller/'+controller+'/autocreate',
                    {items: sentItems},
                    function(data){
                        if (data.success) {
                            for(var i =0; i < data.items.length; i++) {
                                var el = $('#'+data.items[i].fieldid);
                                el.attr("data-dirty","0");
                                el.next(".autosave").val(data.items[i].value);
                                $this.lucidInterface("queueAutosaveItem",el.next(".autosave"),true); //Queue items as urgent autosave
                            }
                        }else {
                            //Put items back on queue since something went wrong.
                            notify("Saving failed! I will retry in a couple of seconds for you !",'error');
                            var autocreateQueue = $this.data('autocreateQueue');
                            autocreateQueue.push(sentItems);
                            $this.data('autocreateQueue',autocreateQueue);
                        }
                        console.info("autocreate data created successfully!");
                        console.info(data);
                    }
                );
            }
            //Recall every 0.8 second.
            setTimeout(function(){
                $this.lucidInterface("processAutocreateQueue");
            },750);
            return this;
        },
        
        /**
         * Autosaves an autosave element
         *
         * Autosaves handles queuing the element and waiting to see if any changes occur before sending
         * them all to the backend.
         *
         * @param Element el
         **/
        'autosave': function(el) {
            var $this = $(this);
            if ($(el).hasClass("autocomplete")) {
                el = el.next(".autosave");
            }
            if (el.attr("data-prevval") != el.val()) {
                $this.lucidInterface("queueAutosaveItem",el);    
            }
            return this;
        },
        
        /**
         * Autocreates a new list element. No need to call autosave after element creation
         * as it will be automatically queued for save after it is created.
         *
         * @access public
         * @param Element el The autocomplete element that needs to create its value from its current text value (title).
         **/
        'autocreate': function(el){
            var $this = $(this);            
            if ($(el).hasClass("autocomplete")) {
                $this.lucidInterface("queueAutocreateItem",el);    
            }
            return this;            
        },
        
        /**
         * Enables all input fields to auto-save themselves when they are in a Datatable
         *
         * @access public
         * @param string datatableId The datatable id for which you want autosave fields to work
         **/
        "enableAutosave": function(datatableId) {
            var $this = $(this);
            var opt = $this.data('options');
            $('#'+datatableId).on( 'draw.dt', function () {
                $('#'+datatableId+' input.autocomplete').each(function(){
                    var el = $(this);
                    el.attr("data-dirty","0");
                    el.on('keydown',function(){
                        $(this).attr("data-dirty",'1');
                    }).on('blur',function(){
                        var field = $(this);
                        if (field.attr('data-dirty') == "1") {
                            if (field.is(":focus") === false) {
                                $this.lucidInterface("autocreate",field);
                            }
                        }else $this.lucidInterface("autosave",field.next(".autosave"));
                    }).autocomplete({
                        'serviceUrl': opt.root+opt.lang+'/api/'+el.attr("data-autocomplete-component")+'/controller/'+el.attr("data-autocomplete-controller")+'/autocomplete',
                        'params':{
                            'model': el.attr('data-autocomplete-model'),
                            'titlefield': el.attr('data-autocomplete-titlefield'),
                            'valuefield': el.attr('data-autocomplete-valuefield')
                        },
                        onSelect: function (suggestion) {
                            el.attr("data-dirty",'0');
                            el.val(suggestion.value);   
                            var hiddenElement = el.next('.autosave');
                            hiddenElement.val(suggestion.data);
                            $this.lucidInterface("autosave",hiddenElement);
                        },
                        onSearchComplete: function(){
                        },
                        onInvalidateSelection: function(){
                            var input = el;
                            if (input.attr("data-dirty") == "0") {
                                input.attr("data-dirty",'1');
                            }
                        }
                    });
                });
                $('#'+datatableId+' .autosave').on('blur',function(){
                    var hiddenElement = $(this);
                    $this.lucidInterface("autosave",hiddenElement);
                });
                $('#'+datatableId+' select.autosave').on('change',function(){
                    var hiddenElement = $(this);
                    $this.lucidInterface("autosave",hiddenElement);
                });
            } );
            return this;    
        },
        
        /**
         * Populate pulldowns via Ajax as user types
         *
         * @access public
         * @param element Select element
         * @param options An options object.
         **/
        "autoloadSelectOptions": function(element,options) {
            var $this = $(this);
            var opt = $this.data('options');
            var el = $(element);
            options = $.extend({
                url: opt.root+opt.lang+'/api/'+el.attr("data-autocomplete-component")+'/controller/'+el.attr("data-autocomplete-controller")+'/autocomplete',
                model: '',
                screen: options.screen,
                titlefield: 'datalist_title',
                valuefield: 'datalist_value',
                onBeforeSend: function(d){
                    return d;
                }
            },options);
            if (el.length > 0) {
                el.on('change',options.onChange);
                var input = el.next("div.chosen-container").find("input.chosen-search-input");
                if (input.length > 0) {
                    input.on('keyup',function(e){
                        //Prevent up and down arrows from triggering the autocomplete.
                        if (input.length > 0 && e.which != 38 && e.which != 40 && e.which != 13) {
                            if (typeof autoloadSelectOptions_Timeout != 'undefined') {
                                clearTimeout(autoloadSelectOptions_Timeout);
                            }
                            var context = $(this);
                            autoloadSelectOptions_Timeout = setTimeout(function(){
                                $this.lucidInterface("autoloadSelectSendRequest",context,options);
                            },250);
                        }
                    });
                    el.on("chosen:showing_dropdown",function(){
                        $this.lucidInterface("autoloadSelectSendRequest",$(this),options);
                    });
                    el.on('chosen:hiding_dropdown',options.onSelect);
                }
            }
            return this;
        },
              
        'autoloadSelectSendRequest': function(context,options){
            context = $(context);
            if (!context.hasClass("chosen-search-input")) {
                context = context.next("div.chosen-container").find("input.chosen-search-input");
            }
            var curSelect = $(context).closest("div.chosen-container").prev("select");
            var curValue = curSelect.val();
            var queryVal = $(context).val();
            var input = $(context);
            var ajaxData = {
                model: options.model,
                titlefield: options.titlefield,
                valuefield: options.valuefield,
                query: queryVal,
                screen: options.screen
            };
            ajaxData = options.onBeforeSend(ajaxData);
            $.ajax({
                url:options.url,
                data:ajaxData,
                dataType: "json"
            }).done(function( data ) {
                console.info(data);
                $(curSelect).children("option").remove();
                var chosenResult = $(curSelect).next(".chosen-container").find("ul.chosen-results");
                var exactMatch = false;
                if (data.suggestions.length > 0) {
                    $(curSelect).append('<option value="0"></option>');
                    $.map( data.suggestions, function( item ) {
                        if (item.label.toUpperCase() == queryVal.toUpperCase()) exactMatch = true;                
                        $(curSelect).append('<option value="'+item.data+'"'+((curValue == item.data)?' selected':'')+'>' + item.label + '</option>');
                    });
                }
                var val = $(curSelect).next("div.chosen-container").find("input.chosen-search-input").val();
                $(curSelect).trigger("chosen:updated");
                $(curSelect).trigger("chosen:open");
                $(curSelect).trigger("chosen:activate");
                $(curSelect).next("div.chosen-container").find("input.chosen-search-input").val(val);
                if (!data.suggestions.length && !chosenResult.children(".no-results").length) {
                    chosenResult.append("<li class='no-results'>No results match <span>"+data.query+"</span></li>");
                    
                    //Chosen hides the search box when no results are show... bring it back here.
                    if ($(curSelect).hasClass('autocreate') === false){
                        var sel = $(curSelect).next("div.chosen-container").find("input.chosen-search-input");
                        sel.parent().css('position','relative');
                        sel.prop("readonly",false);
                        $(curSelect).next("div.chosen-container").find("input.chosen-search-input").parent().css('position','relative');
                    }
                }
                if (!exactMatch && !chosenResult.children(".create-option").length && $(curSelect).hasClass('autocreate') && queryVal.length) {
                    chosenResult.append("<li class='create-option active-result'><a>Add Option</a>: \""+data.query+"\"</li>");    
                }
            });
        },
        
        /**
         * App notification in bottom corner
         **/
        'notify': function (msg,cls) {
            var n = $("#notify");
            if (n.length === 0) {
                $("body").append("<div id=\"notify\" class=\"notify\"></div>");
            }
            if (cls == 'success') {
                $('#notify').removeClass("error").addClass("success");
                msg = '<i class="glyphicon glyphicon-ok-sign"></i>'+msg;
            }
            if (cls == 'error') {
                $('#notify').removeClass("success").addClass("error");
                msg = '<i class="glyphicon glyphicon-remove-sign"></i>'+msg;
            }
            $('#notify').html(msg).fadeIn(100);
            setTimeout(function() {
                $('#notify').fadeOut('fast');
            }, 1000);
        }
        
        
	};
	$.fn.lucidInterface = function(method) { 
		if (methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.lucidInterface' );
		}    
	};	
})(jQuery);    



/*
 * chosen-readonly - Readonly support for Chosen selects
 * @version v1.0.6
 * @link http://github.com/westonganger/chosen-readonly
 * @license MIT
 */

(function($){

  $.fn.chosenReadonly = function(isReadonly){
    var elements = this.filter(function(i, item){
      return $(item).data('chosen');
    });

    elements.on('chosen:updated', function(){
      var item = $(this);
      if(item.attr('readonly')){
        var wasDisabled = item.is(':disabled');
        
        item.attr('disabled', 'disabled');
        item.data('chosen').search_field_disabled();

        if(wasDisabled) {
          item.attr('disabled', 'disabled');
        }else{
          item.removeAttr('disabled');
        }
      }
    });

    if(isReadonly){
      elements.attr('readonly', 'readonly');
    }else if(isReadonly === false){
      elements.removeAttr('readonly');
    }

    elements.trigger('chosen:updated');

    return this;
  };

}(window.jQuery || window.Zepto || window.$));


/**
 * Formats numbers : (123456789.12345).formatMoney(2, '.', ',');
 **/
Number.prototype.formatMoney = function(c, d, t){
var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };