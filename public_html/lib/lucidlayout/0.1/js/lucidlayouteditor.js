/**
 * LucidLayout jQuery Plugin
 *
 * Allows easy editing of layout elements without using bootstrap grid markup
 * in LucidCMS.
 * 
 * How to use this plugin :
 *
 * $('.mydiv').lucidLayoutEditor({
 * 		name:'formfieldname',
 * 		value:'<div>..</div>',
 * });
 * 
 * Oct 26, 2013
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2013 Translucide
 * @license
 * @since 		0.1
 */

/**
 * LucidLayoutEditor class
 *
 * @class LucidLayoutEditor
 * @public
 */

jQuery.expr[':'].withoutparent = function(a,i,m){
    return jQuery(a).parent(m[3]).length < 1;
};


(function( $ ) {	
	var methods = {
		
		/**
		 * Initialize the layout editor.
		 *
		 * @public
		 * @memberOf LucidLayoutEditor
		 * @function
		 * @returns {Object} A reference to the DOM object this plugin is working on
		 */
		init: function(options) {
			var $this = $(this);
			$this.addClass('lucidlayouteditor');
			$this.data('options', $.extend({
				name: 'lucidlayouteditor',
				value: '',
				gridcols: 12,
				url: '',
				saveurl: '',
				renderurl: '',
				removeurl: ''
			}, options));
			$this.data("historyindex",0);
			$this.lucidLayoutEditor("setup");
			$this.lucidLayoutEditor("update");
			return this;
		},

		/**
		 * update: Updates all cells
		 *
		 * @public
		 * @memberOf LucidLayoutEditor
		 */
		update: function(element){
			var $this = $(this);
			var opt = $this.data('options');
			var history = $this.children('.history').children();
			var hindex = $this.data('historyindex');
			
			//Clean markup only if it does not come from history (already cleaned)
			if (history.eq(hindex).html() != $this.children('.canvas').html()) {
				$this.lucidLayoutEditor("sanitize");
			}

			//Update output
			var output = $this.children('.canvas').html();
			$this.lucidLayoutEditor("value",output);
			
			//Update history
			if (history.eq(hindex).html() != output || history.length <= hindex) {
				//Clear foward history and add this output to history.
				history.eq(hindex).nextAll().remove();
				history.parent().append('<div>'+output+'</div>');
				$this.data('historyindex',$this.children('.history').children().length-1);
				console.info('New history item added. Current history steps stored is now '+$this.children('.history').children().length);
			}
			$this.lucidLayoutEditor("updateCellEvents");
		},
		/**
		 * Creates the HTML markup mecessary for LucidLayout Editor to operate properly
		 *
		 * @public
		 * @memberOf LucidLayoutEditor
		 * @function
		 */
		setup : function() {
			var $this = $(this);
			var opt = $this.data('options');
			var val = (opt.value != '' || opt.value == 'null')?opt.value:'<div class="row">'+$this.lucidLayoutEditor("getNewCellCode",opt.gridcols)+'</div>';
			$this.html(
				"<div class='toolbar'>"+
					"<a class=\"btn btn-small\" id='undo'><span class=\"glyphicon glyphicon-circle-arrow-left\"></span>Undo</a>"+
					"<a class=\"btn btn-small\" id='redo'><span class=\"glyphicon glyphicon glyphicon-circle-arrow-right\"></span>Redo</a>"+
					"<div class='dropdown'><button class='btn btn-small dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'><span class=\"glyphicon glyphicon glyphicon-th\"></span>Layout <span class='caret'></span></button>"+
						"<ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>"+
						"<li role='presentation' class='dropdown-header'>Cell</li>"+
						"<li role='presentation'><a id=\"cell_insertbefore_trigger\" role='menuitem' tabindex='-1'><span class=\"glyphicon glyphicon-chevron-left\"></span>Insert before</a></li>"+
						"<li role='presentation'><a id=\"cell_insertafter_trigger\" role='menuitem' tabindex='-1'><span class=\"glyphicon glyphicon-chevron-right\"></span>Insert after</a></li>"+
						"<li role='presentation'><a id=\"cell_delete_trigger\" role='menuitem' tabindex='-1'><span class=\"glyphicon glyphicon-remove-sign\"></span>Delete</a></li>"+
						"<li role='presentation' class='divider'></li>"+
						"<li role='presentation' class='dropdown-header'>Row</li>"+
						"<li role='presentation'><a id=\"row_insertbefore_trigger\" role='menuitem' tabindex='-1'><span class=\"glyphicon glyphicon-chevron-up\"></span>Insert before</a></li>"+
						"<li role='presentation'><a id=\"row_insertafter_trigger\" role='menuitem' tabindex='-1'><span class=\"glyphicon glyphicon-chevron-down\"></span>Insert after</a></li>"+
						"<li role='presentation'><a id=\"row_delete_trigger\" role='menuitem' tabindex='-1'><span class=\"glyphicon glyphicon-remove-sign\"></span>Delete</a></li>"+
						"</ul>"+
					"</div>"+
					"<div class='dropdown'><button class='btn btn-small dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'><span class=\"glyphicon glyphicon glyphicon-list-alt\"></span>Content <span class='caret'></span></button>"+
						"<ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>"+
						"<li role='presentation'><a id=\"content_add\" role='menuitem' tabindex='-1' href='#'><span class=\"glyphicon glyphicon-plus-sign\"></span>Add</a></li>"+
						"<li role='presentation'><a id=\"content_edit\" role='menuitem' tabindex='-1' href='#'><span class=\"glyphicon glyphicon-pencil\"></span>Edit</a></li>"+
						"<li role='presentation'><a id=\"content_remove\" role='menuitem' tabindex='-1' href='#'><span class=\"glyphicon glyphicon-remove-sign\"></span>Remove</a></li>"+
						"<li role='presentation'><a id=\"content_refresh\" role='menuitem' tabindex='-1' href='#'><span class=\"glyphicon glyphicon glyphicon glyphicon-refresh\"></span>Refresh</a></li>"+
						"</ul>"+
					"</div>"+
					"<a class=\"btn btn-small\" id='source'><span class=\"glyphicon  glyphicon glyphicon-cog\"></span>Source</a>"+
					//"<a class=\"btn btn-small\" id='fullscreen'>Fullscreen</a>"+
				"</div>"+				
				"<div class='canvas'>"+val+"</div>"+
				"<div class='history'><div>"+val+"</div></div>"+
				"<div class=\"output\"><textarea name='"+opt.name+"'>"+val+"</textarea></div>"
			);
			$this.children(".output").children("textarea").on("change keyup paste",function(){
				$this.children(".canvas").html($this.children(".output").children("textarea").val());
				$this.lucidLayoutEditor("update");
			});
			$this.data('selectedcell',$this.lucidLayoutEditor("getSelectedElement"));
			$this.lucidLayoutEditor("updateCellEvents");
			$this.lucidLayoutEditor("setupToolbar");
			$this.lucidLayoutEditor("setupHoverTools");
			$this.lucidLayoutEditor("setupDialogs");
			return this;
		},
		
		/**
		 * Setup hover icons
		 * 
		 * @public
		 * @memberOf lucidLayoutEditor
		 * @returns Object this
		 */
		setupHoverTools: function(){
			var $this = $(this);
			var opt = $this.data('options');
			$('body').append("<div class='lucidlayouteditor_hovertools lucidlayouteditor_hovertools_"+opt.name+"'><a id='smaller'><span class=\"glyphicon glyphicon-arrow-left\"></span></a><a id='larger'><span class=\"glyphicon glyphicon-arrow-right\"></span></a><a id='editcontent'><span class=\"glyphicon glyphicon-pencil\"></span></a><a id='remove'><span class=\"glyphicon glyphicon-remove-sign\"></span></a></div>");
			$("body .lucidlayouteditor_hovertools_"+opt.name).css("display","none");
			$("body .lucidlayouteditor_hovertools_"+opt.name+" #smaller").click(function(){
				var el = $this.lucidLayoutEditor("getSelectedElement");
				var newWidth = $this.lucidLayoutEditor("getCellWidth",el);
				if (newWidth > 1) newWidth--;
				$this.lucidLayoutEditor("resetCellWidth",el);
				el.addClass("col-sm-"+newWidth);
				$this.lucidLayoutEditor("update",el);
				el.addClass("selected");
			});
			$("body .lucidlayouteditor_hovertools_"+opt.name+" #larger").click(function(){
				var el = $this.lucidLayoutEditor("getSelectedElement");
				var newWidth = $this.lucidLayoutEditor("getCellWidth",el);
				if (newWidth < 12) newWidth++;
				$this.lucidLayoutEditor("resetCellWidth",el);
				el.addClass("col-sm-"+newWidth);
				$this.lucidLayoutEditor("update",el);
				el.addClass("selected");
			});
			$("body .lucidlayouteditor_hovertools_"+opt.name+" #editcontent").click(function(){
				var el = $this.lucidLayoutEditor("getSelectedElement");
				el = el.children(".content").eq(0);
				if(el.html() == ""){
					$this.lucidLayoutEditor("listWidgets");
				}else {
					var id = el.attr("id").split("_");
					console.info(id);
					$this.lucidLayoutEditor("editWidget",id[2], id[3]);
				}
			});
			$("body .lucidlayouteditor_hovertools_"+opt.name+" #remove").click(function(){
				var el = $this.lucidLayoutEditor("getSelectedElement");
				el.remove();
				$this.lucidLayoutEditor("update",el);
			});
			return this;
		},
		
		/**
		 * Bind all events to toolbars buttons
		 *
		 * @public
		 * @memberOf lucidLayoutEditor
		 * @returns Object this
		 */
		setupToolbar: function(){
			var $this = $(this);
			var opt = $this.data("options");			
			$this.children('.toolbar').children('#undo').on('click',function(){
				var chi = $this.data('historyindex');
				if (chi > 0) {
					$this.data('historyindex',chi-1);
					$this.children('.canvas').html($this.children('.history').children().eq(chi-1).html());
					$this.lucidLayoutEditor('update');
					console.info('Current history index is now '+$this.data('historyindex')+' over a total of '+$this.children('.history').children().length);
				}
			});
			$this.children('.toolbar').children('#redo').on('click',function(){
				var chi = $this.data('historyindex');
				if (chi < $this.children('.history').children().length-1){
					$this.data('historyindex',chi+1);
					$this.children('.canvas').html($this.children('.history').children().eq(chi+1).html());
					$this.lucidLayoutEditor('update');
					console.info('Current history index is now '+$this.data('historyindex')+' over a total of '+$this.children('.history').children().length);
				}
			});
			$this.children('.toolbar').children('#source').on('click',function(){
				$this.children(".output").toggle();
				$this.children(".canvas").toggle();
				if ($this.children(".canvas").css("display") == 'none') {
					$("body .lucidlayouteditor_hovertools_"+opt.name).css("display","none");
				}
			});
			/*$this.children('.toolbar').children('#fullscreen').toggle($(document).fullScreen() != null);
			$this.children('.toolbar').children('#fullscreen').on('click',function(){
				$this.toggleFullScreen();
			});
			*/
			$this.children('.toolbar').find('#cell_insertbefore_trigger').on('click',function(){
				var c = $this.lucidLayoutEditor('getCell',$this.lucidLayoutEditor("getSelectedElement"));
				$this.lucidLayoutEditor('insertBefore',c);			
			});
			$this.children('.toolbar').find('#cell_insertafter_trigger').on('click',function(){
				var c = $this.lucidLayoutEditor('getCell',$this.lucidLayoutEditor("getSelectedElement"));
				$this.lucidLayoutEditor('insertAfter',c);			
			});
			$this.children('.toolbar').find('#cell_delete_trigger').on('click',function(){
				var c = $this.lucidLayoutEditor('getCell',$this.lucidLayoutEditor("getSelectedElement"));
				$this.lucidLayoutEditor('remove',c);			
			});
			$this.children('.toolbar').find('#row_insertbefore_trigger').on('click',function(){
				var c = $this.lucidLayoutEditor('getRow',$this.lucidLayoutEditor("getSelectedElement"));
				$this.lucidLayoutEditor('insertBefore',c);			
			});
			$this.children('.toolbar').find('#row_insertafter_trigger').on('click',function(){
				var c = $this.lucidLayoutEditor('getRow',$this.lucidLayoutEditor("getSelectedElement"));
				$this.lucidLayoutEditor('insertAfter',c);			
			});
			$this.children('.toolbar').find('#row_delete_trigger').on('click',function(){
				var c = $this.lucidLayoutEditor('getRow',$this.lucidLayoutEditor("getSelectedElement"));
				$this.lucidLayoutEditor('remove',c);			
			});

			
			$this.children('.toolbar').find('#content_add').on('click',function(){
				var el = $this.lucidLayoutEditor("getSelectedElement");
				$this.lucidLayoutEditor("listWidgets");
			});
			$this.children('.toolbar').find('#content_edit').on('click',function(){
				var el = $this.lucidLayoutEditor("getSelectedElement");
				$this.lucidLayoutEditor("editWidget",el.data("name"), el.data("id"));
			});
			$this.children('.toolbar').find('#content_remove').on('click',function(){
				var el = $this.lucidLayoutEditor("getSelectedElement");
				el.remove();
				$this.lucidLayoutEditor("update",el);
			});
			$this.children('.toolbar').find('#content_refresh').on('click',function(){
				$this.lucidLayoutEditor("getFiltered",".cell").each(function(i,e){
					var el = $(e);
					if (el.children(".content").html() != '' && el.children(".content").attr("id") != '') {
						var idd = el.children(".content").attr("id").split("_");
						$.post(opt.renderurl.replace(/{lang}/g,opt.lang), {id: idd[3]}, function( d ) {
							el.children(".content").attr("id","widget_"+d.type+"_"+d.name+"_"+d.id);
							el.children(".content").html(d.src.replace(/\r?\n|\r/g,""));
							$this.lucidLayoutEditor("update");
						});
					}					
				});
			});
			
			/*
			 *Removed we find a way to display components like CKEditor and Bootstrap modals over fullscreen mode element...
			$(document).bind("fullscreenchange", function() {
				if ($(document).fullScreen()){
					var d = $this.lucidLayoutEditor('getDialog');
					d.detach().appendTo($this).css('z-index',999);
					$("body .lucidlayouteditor_hovertools_"+opt.name).detach().appendTo($this);
					
				}else{
					var d = $this.lucidLayoutEditor('getDialog');
					d.detach().appendTo("body").css('z-index','auto');
					$("body .lucidlayouteditor_hovertools_"+opt.name).detach().appendTo("body");
				}
				console.log("Fullscreen " + ($(document).fullScreen() ? "on" : "off"));
			});
			*/
			return this;
		},
		
		getDialog: function(){
			var $this = $(this);
			var opt = $this.data("options");			
			return $("#lucidlayouteditor_dialog");
		},
		
		setupDialogs: function(){
			var $this = $(this);
			var opt = $this.data("options");
			var dialog = $this.lucidLayoutEditor('getDialog');
			
			if (dialog.length < 1) {
				$("body").append('<div id="lucidlayouteditor_dialog" class="modal fade"><div class="modal-dialog"><div class="modal-content"></div></div></div>');
				$('#lucidlayouteditor_dialog').on('hidden.bs.modal', function (e) {	
					$(e.target).removeData("bs.modal").find(".modal-content").empty();
				});
			}
		},
		
		loadDialog: function(url,dialogName,params){
			var $this = $(this);
			var opt = $this.data("options");
			var dialog = $this.lucidLayoutEditor('getDialog');
			url = url+"?dialog="+dialogName;
			if (params != undefined) url += "&"+$.param(params);
			//dialog.removeData('bs.modal');
			dialog.modal({
				show: true,
				cache: false,
				remote: url
			});
			dialog.modal('show');
		},
		
		listWidgets: function(){
			var $this = $(this);
			var opt = $this.data("options");
			var dialog = $this.lucidLayoutEditor('getDialog');
			dialog.off('shown.bs.modal').on('shown.bs.modal', function (e) {
				var buttons = dialog.find("#lucidlayouteditor_dialog_ok");
				buttons.off('click').on('click', function (e2) {
					var widgetname = dialog.find("#widget").val();
					dialog.off('hidden.bs.modal').on('hidden.bs.modal',function(){
						dialog.off('hidden.bs.modal').removeData("bs.modal").find(".modal-content").empty();
						console.info("List widgets dialog button clicked");
						console.info("widgetname: "+widgetname);
						if (widgetname != '') $this.lucidLayoutEditor("editWidget",widgetname,0);						
					});
					dialog.modal('hide');
					$('.modal-backdrop').remove();
				});
			});
			$this.lucidLayoutEditor("loadDialog", opt.url, 'list');			
		},
		
		editWidget: function(widgetname,id){
			var $this = $(this);
			var opt = $this.data("options");
			var dialog = $this.lucidLayoutEditor('getDialog');
			dialog.off('shown.bs.modal').on('shown.bs.modal', function (e) {
				console.info("edit dialog shown");
				var buttons = dialog.find("#lucidlayouteditor_dialog_ok");
				//Edit widget dialog has loaded.
				setTimeout(function(){$this.lucidLayoutEditor("saveWidget");},1000);
				buttons.off('click').on('click',function(e2){
					dialog.find("form").trigger("submit");
				});
				dialog.off('hidden.bs.modal').on('hidden.bs.modal',function(){
					dialog.off('hidden.bs.modal').removeData("bs.modal").find(".modal-content").empty();
				});
			}); //end modal shown event for edit dialog
			$this.lucidLayoutEditor("loadDialog", opt.url, 'edit',[{name:'widget', value:widgetname},{name:'id',value:id}]);
		},
		
		saveWidget: function(){
			var $this = $(this);
			var opt = $this.data("options");
			var dialog = $this.lucidLayoutEditor('getDialog');
			var widgetid = dialog.find("[name='widget_objid']").val();
			console.info("widgetid:"+widgetid);
			console.info(opt.saveurl);
			dialog.find("form").ajaxForm({
				url : opt.saveurl,
				beforeSubmit : function(data, jqForm, options){
					if (CKEDITOR.instances != undefined) {
						for (instance in CKEDITOR.instances) {
							$('#'+instance).val(CKEDITOR.instances[instance].getData());
							for(i=0;i<data.length;i++) if(data[i].name == instance) data[i].value = CKEDITOR.instances[instance].getData();
						}													
					}
					return true;
				},
				success: function(responseText, statusText, xhr, $form){
					if (responseText.success) {
						dialog.modal('hide');
						$('.modal-backdrop').remove();						
						$.post(opt.renderurl.replace(/{lang}/g,opt.lang), {id: widgetid}, function( d ) {
							var elsel = $this.lucidLayoutEditor("getSelectedElement");
							elsel.children(".content").attr("id","widget_"+d.type+"_"+d.name+"_"+d.id);
							elsel.children(".content").html(d.src.replace(/\r?\n|\r/g,""));
							$this.lucidLayoutEditor("update");
						});
					}
				}
			});//End ajaxform
		},
		
		/**
		 * updateCellEvents
		 *
		 * @public
		 * @memberOf lucidlayouteditor
		 * @function
		 */
		updateCellEvents: function() {
			var $this = $(this);
			var opt = $this.data("options");
			$this.lucidLayoutEditor("getFiltered",".cell").removeClass("selected").off("click").off("dblclick").click(function(){
				$this.lucidLayoutEditor("getSelectedElement").removeClass("selected");
				$(this).addClass("selected");
				var el = $("body .lucidlayouteditor_hovertools_"+opt.name);
				var destination = $(this).offset();			
				el.css({
					position: "absolute",
					display:'block',
					top: destination.top + parseInt($(this).css('margin-top').replace('px', '')),
					left: destination.left + parseInt($(this).css('margin-left').replace('px', ''))
				});				
			}).dblclick(function(){
				$(this).click();
				$("body .lucidlayouteditor_hovertools_"+opt.name+" #editcontent").click();
			});
			
			$this.lucidLayoutEditor("getFiltered",".cell").draggable({
				containment: '.lucidlayouteditor_'+opt.name+' .canvas',
				pointer: 'move',
				helper: 'clone',
				snap: '.lucidlayouteditor_'+opt.name+' .cell',
				revert: "invalid",
			}).disableSelection();
			
			$this.lucidLayoutEditor("getFiltered",".cell").droppable( {
				accept: ".lucidlayouteditor_"+opt.name+" .canvas .cell",
				tolerance : 'intersect',
				drop: function handleDropEvent( event, ui ) {
					console.info(event.target);
					console.info(ui.draggable);
					var parent = ui.draggable.parent();
					var draggedElement = $(ui.draggable);
					var dropZone = $(this);
					var offset = dropZone.offset().left - draggedElement.offset().left;
					console.info(offset);
					var el = draggedElement.detach();
					if (offset > 0) dropZone.after(el);
					else dropZone.before(el);
					$this.lucidLayoutEditor("update");
				}
			});
			return this;
		},
		
		/**
		 * Gets or set the current Layout Editor value
		 *
		 * @public
		 * @memberOf lucidLayouteditor
		 * @function
		 */
		value: function(val) {
			if (typeof val != undefined) {
				$(this).children('.output').children('textarea').val(val).trigger("change");
				var valopt = $(this).data('options');
				valopt.value = val;
				$(this).data('options',valopt);
				return this;
			}
			return $(this).children('.output').children('textarea').val();
		},
		
		/**
		 * Gets an option value
		 *
		 * @public
		 * @memberOf LucidLayouteditor
		 * @function
		 */
		option: function(o,v) {
			if (typeof v == undefined) {
				return $(this).data(o);
			}
			return $(this).data(o,v);
		},
		
		/**
		 * Cleans up the markup code to remove any jquery-ui classes and events on the markup
		 *
		 * @public
		 * @memberOf LucidLayoutEditor
		 * @returns void
		 */
		sanitize: function(){
			var $this = $(this);
			$this.lucidLayoutEditor("getFiltered",".row, .cell").each(function(i,e){
				if ($(e).draggable != undefined) {
					try{
						$(e).draggable().draggable('destroy');
					}catch(er){}
				}
				if ($(e).droppable != undefined) {
					try{
						$(e).droppable().droppable('destroy');
					}catch(er){}
				}
			});
			$this.lucidLayoutEditor("getFiltered",".cell")
				.off()
				.removeClass('ui-draggable')
				.removeClass('ui-droppable')
				.removeClass("ui-draggable-handle")
				.removeClass("ui-draggable-dragging")
				.removeAttr("style");
			$this.lucidLayoutEditor("getFiltered",".row")
				.off()
				.removeClass('ui-draggable')
				.removeClass('ui-droppable')
				.removeClass("ui-draggable-handle")
				.removeClass("ui-draggable-dragging")
				.removeAttr("style");			
			return this;
		},

		/**
		 * Rounds a number up to X decimals
		 *
		 * @public
		 * @memberOf LucidLayoutEditor
		 * @param float nb The number to round
		 * @param int decimals The number of decimals up to which nb should be rounded
		 *
		 * @returns float The rounded floating point value
		 */
		roundTo: function(nb,decimals) {
			return +(Math.round(nb + "e+" + decimals)  + "e-" + decimals);
		},

		/**
		 * Creates a new table inside a cell
		 *
		 * @public
		 * @memberOf LucidLayoutEditor
		 * @param Object cell
		 */
		getNewCellCode: function(cols){
			return '<div class="cell col-sm-'+cols+'"><div class="content"></div></div>';
		},
		
		/**
		 * Inserts a new cell or row before cell/row element specified
		 *
		 * @public
		 * @memberOf LucidLayoutEditor
		 * @param Object cell
		 */
		insertBefore: function(cellOrRow){
			var $this = $(this);
			var cell = $this.lucidLayoutEditor("getNewCellCode",2);
			if (cellOrRow.hasClass("row")) cell = '<div class="row">'+cell+'</div>';
			cellOrRow.before(cell);
			$this.lucidLayoutEditor("update",cellOrRow);
			return this;
		},
		
		/**
		 * Inserts a new cell or row after cell/row element specified
		 *
		 * @public
		 * @memberOf LucidLayoutEditor
		 * @param Object cell
		 */
		insertAfter: function(cellOrRow){
			var $this = $(this);
			var cell = $this.lucidLayoutEditor("getNewCellCode",2);
			if (cellOrRow.hasClass("row")) cell = '<div class="row">'+cell+'</div>';
			cellOrRow.after(cell);
			$this.lucidLayoutEditor("update",cellOrRow);
			return this;
		},
		
		/**
		 * Appends a new cell to the current row element specified
		 *
		 * @public
		 * @memberOf LucidLayoutEditor
		 * @param Object cell
		 */
		append: function(row) {
			var $this = $(this);
			var cell = $this.lucidLayoutEditor("getNewCellCode",2);
			row.append(cell);
			$this.lucidLayoutEditor("update",cellOrRow);
			return this;
		},
		
		/**
		 * Removes an existing cell or row and its content
		 *
		 * @public
		 * @memberOf LucidLayoutEditor
		 * @param Object cellOrRow
		 */
		remove: function(cellOrRow){
			if (cellOrRow.hasClass("cell") && (cellOrRow.parent().children().length > 1 || cellOrRow.parents('.row').length > 1)) {
				cellOrRow.remove();
				if (cellOrRow.parent().children().length == 0) cellOrRow.parent().remove();
			}
			if (cellOrRow.hasClass("row") && (cellOrRow.parent().children().length > 1 || cellOrRow.parents('.row').length >= 1)) {
				cellOrRow.remove();
			}
			var hasSelected = $this.lucidLayoutEditor("getSelectedElement");
			if (hasSelected.length == 0) {
				if (cellOrRow.prev(".cell").length > 0) cellOrRow.prev(".cell").addClass("selected").trigger("click");
				if (cellOrRow.next(".cell").length > 0) cellOrRow.next(".cell").addClass("selected").trigger("click");
			}
			$("body .lucidlayouteditor_hovertools_"+opt.name).css("display","none");
			$this.lucidLayoutEditor("update",cellOrRow);
			return this;
		},
		
		/**
		 * Gets the first cell element that is a parent of specified element
		 *
		 * @public
		 * @memberOf LucidLayoutEditor
		 * @param Object el
		 */
		getCell: function(el){
			if (!el.hasClass("cell")) {
				return el.parents('.cell').eq(0);
			}else return el;
		},
		
		/**
		 * Gets the first row element that is a parent of specified element
		 *
		 * @public
		 * @memberOf LucidLayoutEditor
		 * @param Object el
		 */
		getRow: function(el){
			if (!el.hasClass("row")) {
				return el.parents('.row').eq(0);
			}else return el;
		},
		
		/**
		 * Gets the number of cols in current row element
		 * 
		 * @public
		 * @memberOf LucidLayoutEditor
		 * @param Object row
		 */
		getRowWidth: function(row){
			var $this = $(this);
			if (!row.hasClass('row')) {
				return 0;
			}
			var count = 0;
			var cells = row.children(".cell").each ( function () {
				count += $this.lucidLayoutEditor("getCellWidth",$(this));
			});
			return count;
		},
		
		/**
		 * Gets the width of current cell element
		 * 
		 * @public
		 * @memberOf LucidLayoutEditor
		 * @param Object cell
		 */
		getCellWidth: function(cell){
			var $this = $(this);
			if (!cell.hasClass('cell')) {
				return 0;
			}
			var count = 0;
			var elClasses = $( cell ).attr ( 'class' ).split ( ' ' );
			for ( var index in elClasses ) {
				if ( elClasses[index].match ( /^col-sm-\d+$/ ) ) {
					var classNum = elClasses[index].split ( '-' )[2];
					count = classNum;
					break;
				}
			}
			return count;
		},
		
		getSelectedElement: function() {
			var $this = $(this);
			var el = $this.lucidLayoutEditor("getFiltered",".cell.selected").eq(0);
			console.info(el.children(".content").data());
			return el;
		},

		getFiltered: function(selector){
			var $this = $(this);
			return $this.children(".canvas").find(selector).filter(':withoutparent(.content)');
		},
		
		resetCellWidth: function(cell){
			var $this = $(this);
			cell.attr('class',function(i, c){
				return c.replace(/(^|\s)col-\S+/g, '');
			});
			return this;
		}
	};

	$.fn.lucidLayoutEditor = function(method) { 
		if (methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.lucidLayoutEditor' );
		}    
	};	

})(jQuery);		