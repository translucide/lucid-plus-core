<?php
/**
 * Controller class
 *
 * Handles and process the requested things
 *
 * Jan 17, 2013

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

interface ControllerInterface{
	function init();
	function execute();
	function getView();
	function translate($langid, $objid = 0, $url='');
}

class Controller extends BaseClass implements ControllerInterface {
	/**
	 * Hold a reference to this controller's View object
	 *
	 * @access private;
	 * @var View Controller's view object
	 */
	private $view;

	/**
	 * Holds a reference to the component in which the controller resides
	 *
	 * @access protected
	 * @var $component
	 */
	protected $component;
	/**
	 * Initializes the controller object
	 *
	 * @access public
	 *
	 * @return void Initializes the controller object
	 */
	public function __construct(){
        $this->component = 'core';
	}

    public function init(){
    }

	/**
	 * Executes the controller logic
	 *
	 * @access public
	 *
	 * @return View Returns the controller's view object
	 */
	public function execute(){
	}

	public function getView($viewname=''){
		global $service;
		if (!is_object($this->view)) {
			if ($viewname !='') $view = $viewname;
			else {
				$view = get_class($this);
				$view = str_replace('Controller','',$view);
			}
			if ($this->component != '' && $this->component != 'core') $service->get('Ressource')->get('com/'.strtolower($this->component).'/view/'.strtolower($view));
			else $service->get('Ressource')->get('core/view/'.strtolower($view));
			$view = ucfirst($view).'View';
			if (!class_exists($view)){
				$view = 'DefaultView';
				$log =& $service->get('log');
				$log->add("Controller::getView (".get_called_class().") : View class ($view) not found for this controller! Using DefaultView instead.",__FILE__,__LINE__,E_WARNING);
			}
			$this->view = new $view();
		}
		return $this->view;
	}

	/**
	 * Translate page URL to another language
	 *
	 * @access public
	 *
	 * @return string Page Url in the requested language
	 */
	public function translate($langid, $objid = 0, $url='') {
		return false;
	}

	public function getName(){
		return ucfirst($this->component).'->'.ucfirst(get_class($this));
	}

	public function hasAccess($op){
		return false;
	}
}
?>
