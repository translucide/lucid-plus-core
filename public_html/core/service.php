<?php
/**
 * Services manager
 *
 * Manages services
 * Init service and return service object created.
 * All services objects are declared on the global level.
 *
 * Dec 8, 2012
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class Service extends BaseClass{

	/**
	 * Holds a reference to all services started
	 *
	 * @access private
	 *
	 * @var $services
	 */
	private $services;

	/**
	 * Service Handler Constructor
	 *
	 * @access public
	 *
	 * @return void ServiceHandler()
	 */
	public function __construct(){}

	/**
	 * Returns a service object
	 *
	 * @access public
	 *
	 * @param string $name
	 * @param array $params
	 * @return mixed The service object or false on failure
	 */
	public function &get($service,$params = array()) {
		$service = strtolower($service);
		if (strpos($service,'/') !== false) {
			$sv = substr($service,strrpos($service,'/'));
		}else $sv = $service;
		$sv = strtolower($sv);
		if (isset($this->services[$sv])) {
			return $this->services[$sv];
		}else {
			$file = $service;
			if (stripos($service,'handler') !== false) $file = str_replace('handler','',$service);
			if (isset($this->services['ressource'])) {
				$this->services['ressource']->get('core/'.$file);
			}else {
				if (file_exists(ROOT.'core/'.$file.'.php')) {
					include_once(ROOT.'core/'.$file.'.php');
				}
			}

			if (strpos($service,'/') !== false) {
				$service = substr($service,strrpos($service,'/'));
			}
			$clsName = ucfirst($service);
			if (class_exists($clsName) || class_exists($clsName.'Handler')) {

				//Load Any class that has Handler after the desired name, with one exception: the builtin PHP
				//class SessionHandler which conflicts with our Session class...
				if (class_exists($clsName.'Handler') && $clsName != 'Session') $clsName .= 'Handler';
				if (count($params) > 0) {
					$this->services[$service] = new $clsName($params);
				}else $this->services[$service] = new $clsName();
				return $this->services[$service];
			}
		}
		$ret = false;
		return $ret;
	}

	public function &load($service) {
		return $this->get($service);
	}

	public function isLoaded($service){
		return isset($this->services[strtolower($service)]);
	}

	public function register($service,$obj) {
		$this->services[strtolower($service)] = $obj;
	}
}
