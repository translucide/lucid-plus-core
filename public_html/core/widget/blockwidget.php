<?php
/**
 * Block widget class
 *
 * Default widget options and values for all block widgets.
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/data');
$service->get('Ressource')->get('core/helper/actionmanager');
$service->get('Ressource')->get('core/lang/'.$service->get('Language')->getCode().'/blockwidget');

class BlockWidget extends Widget{

    /**
     * Called from Init when implementing this widget
     *
     * @access protected
     * @return array Options field names
     */
    protected function getOptionsNames(){
        return array(
            //Content
            'displaywidgettitle', //Text, YesNO
            'blockwidget_header',
            'blockwidget_footer',
            'actions', //List element

            //Style
            'variant', //Light or dark variant
            'textalign', //Text alignment

            'gradient', //Gradient code as generated with http://angrytools.com/gradient/
            'gradienttablet', //Gradient code as generated with http://angrytools.com/gradient/
            'gradientmobile', //Gradient code as generated with http://angrytools.com/gradient/

            'padding',
            'paddingtablet',
            'paddingmobile',

            'background', //Image or MP4 video without sound, applies to everything below unless set
            'backgroundtablet', //Image or MP4 video without sound
            'backgroundmobile', //Image or MP4 video without sound
            'backgroundposition',
            'backgroundpositiontablet',
            'backgroundpositionmobile',

            //Advanced options
            'css', //Additional CSS code to apply on this widget
            'js', //Additional JS code to apply to this widget
        );
    }

	public function render(){
		global $service;
		$gridSize = 12;
        $i = $this->getInfo();
		$w = $this->data;
		$o = $w->getVar('widget_options');
        if ($o['stylesheet'] == '') $o['stylesheet'] = 'default';

		$service->get('Ressource')->getStyle($i['component'],'widget',$i['name'],$o['stylesheet']);
        $service->get('Ressource')->addStyle($this->generateInlineStyles());

        $m = $this->getItems();
        $id = 'widget_'.$i['type'].'_'.$i['name']."_".$w->getVar('widget_objid');
        if ($o['widgetidattr']) $id = $o['widgetidattr'];

        $supcls = ' '.$this->options['blockwidget']['additionalclasses'];
		$content = "<section id='".$id."' class='widget html ".$i['name']." {$o['stylesheet']} {$o['variant']} {$o['textalign']} ".$o['widgetclasses'].$supcls."'>";
		$content .= "<div class='ct'>";
        if ($o['blockwidget_header'] && $o['blockwidget_header'] != '<p></p>') $content .= "<div class=\"ct_header\">".$o['blockwidget_header'].'</div>';
		if (!$this->options['blockwidget']['disablerendertitle']) $content .= "<h1".(($o['displaywidgettitle'])?"":" class='hidden'").">".$w->getVar('widget_title')."</h1>";
        $content .= "{content}";

        //Render widget actions
        if (!$this->options['blockwidget']['disablerenderactions']) {
            $action = new ActionManager();
    		$actions = $action->render($this->data->getVar('widget_objid'));
    		if ($actions != '') $content .= '<div class="actions">'.$actions.'</div>';
        }

        //Append footer content
        if ($o['blockwidget_footer'] && $o['blockwidget_footer'] != '<p></p>') $content .= "<div class=\"ct_footer\">".$o['blockwidget_footer'].'</div>';

        $content .= "</div></section>";
		return $content;
    }

	public function edit($objs,$form){
		global $service;
        $i = $this->getInfo();
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');

        //Header/footer tab :
        if (!isset($options['blockwidget_header']) && isset($options['toptext'])) $options['blockwidget_header'] = $options['toptext'];
        if (!isset($options['blockwidget_header'])) $options['blockwidget_header'] = '';
		$form->add(new HtmleditorFormField('blockwidget_header',$options['blockwidget_header'],array(
            'tab' => 'headerfooter',
			'title'=>_HEADER,
			'width' => 6,
			'length'=>255,
            'rows' => 1,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','blockwidget_header')
		)));
        if (!isset($options['blockwidget_footer']) && isset($options['bottomtext'])) $options['blockwidget_footer'] = $options['bottomtext'];
        if (!isset($options['blockwidget_footer'])) $options['blockwidget_footer'] = '';
		$form->add(new HtmleditorFormField('blockwidget_footer',$options['blockwidget_footer'],array(
            'tab' => 'headerfooter',
			'title'=>_FOOTER,
			'width' => 6,
			'length'=>255,
            'rows' => 1,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','blockwidget_footer')
		)));

        //Actions tab:
		$action = new ActionManager();
        $action->option('tab','actions');
		$form->add($action->getFormField(URL.$service->get('Language')->getCode().'/api/'.$i['component'].'/widget/'.$i['name'].'/',$defobj));

        //Style tab:
        if (!isset($options['variant'])) $options['variant'] = 'dark';
        $form->add(new SelectFormField('variant',$options['variant'],array(
			'tab'=> 'style',
			'title' => _VARIANT,
			'width' => 2,
            'options' => array(
                ['title'=> _LIGHT, 'value' => 'light'],
                ['title'=> _DARK, 'value' => 'dark']
            )
		)));
        if (!isset($options['textalign'])) $options['textalign'] = 'left';
        $form->add(new SelectFormField('textalign',$options['textalign'],array(
			'tab'=> 'style',
			'title' => _TEXTALIGN,
			'width' => 2,
            'options' => array(
                ['title' => _LEFT, 'value' => 'text-left'],
                ['title' => _RIGHT, 'value' => 'text-right'],
                ['title' => _CENTER, 'value' => 'text-center']
            )
		)));
        if (!isset($options['padding'])) $options['padding'] = '';
        $form->add(new TextFormField('padding',$options['padding'],array(
			'tab'=>'style',
			'title'=>_PADDINGS,
			'width' => 2,
		)));
        if (!isset($options['paddingtablet'])) $options['paddingtablet'] = '';
        $form->add(new TextFormField('paddingtablet',$options['paddingtablet'],array(
			'tab'=>'style',
			'title'=>_PADDINGSTABLET,
			'width' => 2,
		)));
        $form->add(new TextFormField('paddingmobile',$options['paddingmobile'],array(
			'tab'=>'style',
			'title'=>_PADDINGSMOBILE,
			'width' => 2,
		)));
        if (!isset($options['gradient'])) $options['gradient'] = '';
        $gradientlink = ' <a href="http://angrytools.com/gradient/" target="_blank" alt="'._GRADIENTEDITOR.'" title="'._GRADIENTEDITOR.'"><span class="glyphicon glyphicon-link"></span></a>';
        $form->add(new TextFormField('gradient',$options['gradient'],array(
			'tab'=>'style',
			'icontip'=>_GRADIENT_TIP,
			'title'=>_GRADIENT.$gradientlink,
			'width' => 2,
		)));
        if (!isset($options['gradienttablet'])) $options['gradienttablet'] = '';
        $form->add(new TextFormField('gradienttablet',$options['gradienttablet'],array(
			'tab'=>'style',
            'icontip'=>_GRADIENT_TIP,
			'title'=>_GRADIENTTABLET.$gradientlink,
			'width' => 2,
		)));
        if (!isset($options['gradientmobile'])) $options['gradientmobile'] = '';
        $form->add(new TextFormField('gradientmobile',$options['gradientmobile'],array(
			'tab'=>'style',
            'icontip'=>_GRADIENT_TIP,
			'title'=>_GRADIENTMOBILE.$gradientlink,
			'width' => 2,
		)));
        if (!isset($options['background']) && isset($options['bgimage'])) $options['background'] = $options['bgimage'];
        if (!isset($options['background'])) $options['background'] = '';
        $form->add(new ImageuploadFormField('background',$options['background'],array(
			'tab'=> 'style',
			'title' => _BGIMAGE,
			'width' => 2
		)));
        if (!isset($options['backgroundtablet']) && isset($options['bgimage'])) $options['backgroundtablet'] = $options['bgimage'];
        if (!isset($options['backgroundtablet'])) $options['backgroundtablet'] = '';
        $form->add(new ImageuploadFormField('backgroundtablet',$options['backgroundtablet'],array(
			'tab'=> 'style',
			'title' => _BGIMAGETABLET,
			'width' => 2
		)));
        if (!isset($options['backgroundmobile']) && isset($options['bgimage'])) $options['backgroundmobile'] = $options['bgimage'];
        if (!isset($options['backgroundmobile'])) $options['backgroundmobile'] = '';
        $form->add(new ImageuploadFormField('backgroundmobile',$options['backgroundmobile'],array(
			'tab'=> 'style',
			'title' => _BGIMAGEMOBILE,
			'width' => 2
		)));

        if (!isset($options['backgroundposition']) && isset($options['bgimageposition'])) $options['backgroundposition'] = $options['bgimageposition'];
        if (!isset($options['backgroundposition'])) $options['backgroundposition'] = '';
        $form->add(new SelectFormField('backgroundposition',$options['backgroundposition'],array(
			'tab'=> 'style',
			'width' => 2,
			'title' => _BGPOS,
			'options' => array(
				array('title' => _LEFT.' '._TOP, 'value' => 'left top'),
                array('title' => _LEFT.' '._CENTER, 'value' => 'left center'),
                array('title' => _LEFT.' '._BOTTOM, 'value' => 'left bottom'),
				array('title' => _RIGHT.' '._TOP, 'value' => 'right top'),
				array('title' => _RIGHT.' '._CENTER, 'value' => 'right center'),
				array('title' => _RIGHT.' '._BOTTOM, 'value' => 'right bottom'),
				array('title' => _CENTER.' '._CENTER, 'value' => 'center center'),
				array('title' => _CENTER.' '._TOP, 'value' => 'center top'),
				array('title' => _CENTER.' '._BOTTOM, 'value' => 'center bottom')
			)
		)));
        if (!isset($options['backgroundpositiontablet']) && isset($options['bgimageposition'])) $options['backgroundpositiontablet'] = $options['bgimageposition'];
        if (!isset($options['backgroundpositiontablet'])) $options['backgroundpositiontablet'] = '';
        $form->add(new SelectFormField('backgroundpositiontablet',$options['backgroundpositiontablet'],array(
			'tab'=> 'style',
			'width' => 2,
			'title' => _BGPOS,
			'options' => array(
				array('title' => _LEFT.' '._TOP, 'value' => 'left top'),
                array('title' => _LEFT.' '._CENTER, 'value' => 'left center'),
                array('title' => _LEFT.' '._BOTTOM, 'value' => 'left bottom'),
				array('title' => _RIGHT.' '._TOP, 'value' => 'right top'),
				array('title' => _RIGHT.' '._CENTER, 'value' => 'right center'),
				array('title' => _RIGHT.' '._BOTTOM, 'value' => 'right bottom'),
				array('title' => _CENTER.' '._CENTER, 'value' => 'center center'),
				array('title' => _CENTER.' '._TOP, 'value' => 'center top'),
				array('title' => _CENTER.' '._BOTTOM, 'value' => 'center bottom')
			)
		)));
        if (!isset($options['backgroundpositionmobile']) && isset($options['bgimageposition'])) $options['backgroundpositionmobile'] = $options['bgimageposition'];
        if (!isset($options['backgroundpositionmobile'])) $options['backgroundpositionmobile'] = '';
        $form->add(new SelectFormField('backgroundpositionmobile',$options['backgroundpositionmobile'],array(
			'tab'=> 'style',
			'width' => 2,
			'title' => _BGPOS,
			'options' => array(
				array('title' => _LEFT.' '._TOP, 'value' => 'left top'),
                array('title' => _LEFT.' '._CENTER, 'value' => 'left center'),
                array('title' => _LEFT.' '._BOTTOM, 'value' => 'left bottom'),
				array('title' => _RIGHT.' '._TOP, 'value' => 'right top'),
				array('title' => _RIGHT.' '._CENTER, 'value' => 'right center'),
				array('title' => _RIGHT.' '._BOTTOM, 'value' => 'right bottom'),
				array('title' => _CENTER.' '._CENTER, 'value' => 'center center'),
				array('title' => _CENTER.' '._TOP, 'value' => 'center top'),
				array('title' => _CENTER.' '._BOTTOM, 'value' => 'center bottom')
			)
		)));

        //Advanced tab:
		$form->add(new TextareaFormField('css',$options['css'],array(
			'tab'=>'advanced',
			'title'=>_CSS,
			'width' => 6,
            'rows' => 10
		)));
		$form->add(new TextareaFormField('js',$options['js'],array(
			'tab'=>'advanced',
			'title'=>_JS,
			'width' => 6,
            'rows' => 10
		)));
		return $form;
	}

    public function apiCall($op){
		global $service;
		switch($op) {
			case 'actionlist':
			case 'actionmove':
			case 'actiondelete':
			case 'actionduplicate':
			case 'actionedit':
			case 'actionsave': {
				$op = str_replace('action','',$op);
				$actionManager = new ActionManager();
				$ret = $actionManager->$op();
			}break;
			default : break;
		}
		return $ret;
	}

	public function hasAccess($op){
		global $service;
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}

    public function generateInlineStyles(){
        $w = $this->data;
        $i = $this->getInfo();
		$o = $w->getVar('widget_options');
        $id = 'widget_'.$i['type'].'_'.$i['name']."_".$w->getVar('widget_objid');
        if ($o['widgetidattr']) $id = $o['widgetidattr'];

        $url = UPLOADURL; //Use tags, will be replaced on CSS styles insertion.
        header('uploadurl: '.UPLOADURL);
        $style = '';
        //Generate Padding for mobile
        $style .= '#'.$id.' .ct{'.
            (($o['padding'])?'padding:'.$o['padding'].';':'').
            (($o['paddingmobile'])?'padding:'.$o['paddingmobile'].';':'').
        '}';
        //Generate BG directives
        $style .= '#'.$id.'{';
        $hasGradient = ($o['gradient'] || $o['gradientmobile'])?true:false;
        $hasBG = ($o['background'] || $o['backgroundmobile'])?true:false;
        $bg = '';
        if ($hasGradient) $bg .= (($o['gradientmobile'])?$o['gradientmobile']:$o['gradient']).(($hasBG)?',':'');
        if ($hasBG) $bg .= 'url("'.(($o['backgroundmobile'])?$url.$o['backgroundmobile']:$url.$o['background']).'") ';
        if ($hasBG) $bg .= (($o['backgroundpositionmobile'])?$o['backgroundpositionmobile']:$o['backgroundposition']).' ';
        if ($hasBG) $bg .= ' / cover no-repeat scroll';
        if ($hasBG || $hasGradient) $style.= 'background:'.$bg.';';
        $style .= '}';

        $style .= '@media (min-width:768px){';

        //Padding for tablets
        $style .= '#'.$id.' .ct{'.
            (($o['padding'])?'padding:'.$o['padding'].';':'').
            (($o['paddingtablet'])?'padding:'.$o['paddingtablet'].';':'').
        '}';
        //Generate BG directives
        $style .= '#'.$id.'{';
        $hasGradient = ($o['gradient'] || $o['gradienttablet'])?true:false;
        $hasBG = ($o['background'] || $o['backgroundtablet'])?true:false;
        $bg = '';
        if ($hasGradient) $bg .= (($o['gradienttablet'])?$o['gradienttablet']:$o['gradient']).(($hasBG)?',':'');
        if ($hasBG) $bg .= 'url("'.(($o['backgroundtablet'])?$url.$o['backgroundtablet']:$url.$o['background']).'") ';
        if ($hasBG) $bg .= (($o['backgroundpositiontablet'])?$o['backgroundpositiontablet']:$o['backgroundposition']).' ';
        if ($hasBG) $bg .= ' / cover no-repeat scroll';
        if ($hasBG || $hasGradient) $style.= 'background:'.$bg.';';
        $style .= '}';

        $style .= '} @media (min-width:1024px){';

        //Padding for desktops
        $style .= '#'.$id.' .ct{'.
            (($o['padding'])?'padding:'.$o['padding'].';':'').
        '}';
        //Generate BG directives
        $style .= '#'.$id.'{';
        $hasGradient = ($o['gradient'])?true:false;
        $hasBG = ($o['background'])?true:false;
        $bg = '';
        if ($hasGradient) $bg .= $o['gradient'].(($hasBG)?',':'');
        if ($hasBG) $bg .= 'url("'.$url.$o['background'].'") ';
        if ($hasBG) $bg .= $o['backgroundposition'];
        if ($hasBG) $bg .= ' / cover no-repeat scroll';
        if ($hasBG || $hasGradient) $style.= 'background:'.$bg.';';
        $style .= '}';

        $style .= '}';
        $style .= str_replace(['{cls}','{id}'],['.'.$o['widgetclasses'],'#'.$id],$o['css']);
        return $style;
    }

	protected function getItems() {
		$dstore = new DataStore();
		$crit = new CriteriaCompo();
		$crit->add(new Criteria('data_parent',$this->data->getVar('widget_objid')));
		$crit->add(new Criteria('data_type',$this->getInfo()['name'].'item'));
		$crit->setSort('data_position');
		return $dstore->get($crit);
	}
}
?>
