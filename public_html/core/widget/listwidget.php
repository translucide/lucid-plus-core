<?php
/**
 * List widget base class
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/data');
$service->get('Ressource')->get('core/helper/dbitemmanager');
$service->get('Ressource')->get('core/helper/actionmanager');

class ListWidget extends BlockWidget{

    /**
     * Called from Init when implementing this widget
     *
     * @access protected
     *
     * @return array Options field names
     */
    protected function getOptionsNames(){
        return array_merge(parent::getOptionsNames(),array('features','itemsperline'));
    }

    /**
     * Called from render when implementing this widget
     *
     * @access protected
     *
     * @return array Options field names
     */
    protected function renderItem(){
        return '';
    }

    /**
     * Called from save when implementing this widget
     *
     * @access protected
     *
     * @return array Options field names
     */
    protected function saveItem(){
        return [];
    }

	public function render(){
		global $service;
        if (isset($this->options['listwidget']['usecustomrendering']) && $this->options['listwidget']['usecustomrendering']){
            return parent::render();
        }

		$gridSize = 12;
        $i = $this->getInfo();
		$w = $this->data;
		$o = $w->getVar('widget_options');
		$m = $this->getItems();
        $content = '';
		$cellw = floor($gridSize/max(1,$o['itemsperline']));
		$mod = count($m) % max(1,$o['itemsperline']);
		$lastcellw = floor($gridSize/max(1,$mod));
		$sum = 0;
		foreach ($m as $k => $v){
            $classes = '';
            if (isset($this->options['listwidget']['itemrowclasses'])) {
                if (is_callable($this->options['listwidget']['itemrowclasses'])) $classes = $this->options['listwidget']['itemrowclasses']($v);
                else $classes = $this->options['listwidget']['itemrowclasses'];
            }
			if ($sum == 0) $content .= '<div class="row cellscount_'.(($k >= count($m)-$mod && $mod > 0)?$mod:$o['itemsperline']).' '.$classes.'">';

            $classes = '';
            if (isset($this->options['listwidget']['itemcellclasses'])) {
                if (is_callable($this->options['listwidget']['itemcellclasses'])) $classes = $this->options['listwidget']['itemcellclasses']($v);
                else $classes = $this->options['listwidget']['itemcellclasses'];
            }
			$content .= "<div class='cell col-sm-".(($k >= count($m)-$mod && $mod > 0)?$lastcellw:$cellw)." {$classes}'>";

            $classes = '';
            if (isset($this->options['listwidget']['itemsectionclasses'])) {
                if (is_callable($this->options['listwidget']['itemsectionclasses'])) $classes = $this->options['listwidget']['itemsectionclasses']($v);
                else $classes = $this->options['listwidget']['itemsectionclasses'];
            }
            $content .= "<section id='column_{$k}' class='column {$classes}'>";

            $content .= $this->renderItem($v,$w);
			$content .= "</section>";
            $content .= "</div>";

			if ($k >= count($m)-$mod && $mod > 0) $sum += $lastcellw;
			else $sum += $cellw;

			if ($sum >= $gridSize) {
				$content .= '</div>';
				$sum = 0;
			}
		}
		if ($sum > 0) $content .= "</div>";
		return str_replace('{content}',$content,parent::render());
    }

	public function edit($objs,$form){
		global $service;
        $i = $this->getInfo();
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		$baseurl = URL.$service->get('Language')->getCode().'/api/'.$i['component'].'/widget/'.$i['name'];
		$form->add(new ItemmanagerFormField('features',$defobj->getVar('widget_options')['features'],array(
			'title'=>_LISTITEMS,
			'tab'=>'basic',
			'parent' => $defobj->getVar('widget_objid'),
			'editlabel' => _EDIT,
			'deletelabel' => _DELETE,
			'duplicatelabel' => _DUPLICATE,
			'newlabel' => _NEW,
			'list' => $baseurl.'/list',
			'edit' => $baseurl.'/edit',
			'duplicate' => $baseurl.'/duplicate',
			'save' => $baseurl.'/save',
			'delete' => $baseurl.'/delete',
			'move' => $baseurl.'/move'
		)));
        $form->add(new TextFormField('itemsperline',$defobj->getVar('widget_options')['itemsperline'],array(
            'title'=>_PERLINE,
			'tab'=>'basic',
            'width' => 1,
            'length'=>255
        )));
		return parent::edit($objs,$form);
	}

	public function apiCall($op){
		global $service;
        $i = $this->getInfo();
		$store = new DataStore();
		$itemManager = new DBItemManager($store,$i['name'].'item');

		switch($op) {
			case 'list' :
			case 'move' :
			case 'delete' : {
				$ret = $itemManager->$op();
			}break;
			case 'duplicate':
			case 'edit' : {
                $ret = '';
				if ($op == 'duplicate') $ret = $itemManager->duplicate();
				$form = $this->editItem($itemManager->getEditObjects($ret));
				$ret = $itemManager->edit($form);
			}break;
			case 'save' : {
				$ret = $itemManager->save($this->saveItem());
			}break;
			case 'actionlist':
			case 'actionmove':
			case 'actiondelete':
			case 'actionduplicate':
			case 'actionedit':
			case 'actionsave': {
				$op = str_replace('action','',$op);
				$actionManager = new ActionManager();
				$ret = $actionManager->$op();
			}break;
			default : break;
		}
		return $ret;
	}
}
?>
