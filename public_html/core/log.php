<?php
/**
 * Logging class
 *
 * Logs messages and output them to either the standard output or to the
 * appropriate log file.
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

define('LOG_FILE_DATAINTEGRITY','dataintegrity');
define('LOG_FILE_DATABASE','database');
define('LOG_FILE_STATS','statistics');
define('LOG_FILE_SECURITY','security');
define('LOG_FILE_SYSTEM','system');
define('LOG_FILE_HTTPERRORS','httperrors');
define('E_INFO','E_INFO');

class Log extends BaseClass{

	/**
	 * An array containing log messages classified by type
	 *
	 * @access private
	 *
	 * @var array $messages
	 */
	private $messages;

	/**
	 * Whether the logging service is enabled or not.
	 *
	 * @access private
	 *
	 * @var bool $enabled
	 */
	private $enabled;

	/**
	 * Sets the log level to use
	 *
	 * @access public
	 *
	 * @var int $level
	 */
	private $level;

	/**
	 * The log destination
	 *
	 * @access public
	 *
	 * @var int $destination
	 */
	private $destination;


	/**
	 * Logging class constructor
	 *
	 * @access public
	 *
	 * @return void log()
	 */
	public function __construct(){
		//Start log service as enabled,
		//and disable it later when preferences are loaded if it is set to disable.
		$this->enabled = true;
		$this->level = null;
		$this->messages = array();
		set_error_handler('Log::handleError');
		register_shutdown_function(array($this,'shutdown'));
	}

	/**
	 * Logger error handler
	 *
	 * Called by PHP when an error is encountered.
	 *
	 * @access public
	 *
	 * @param int $errNo
	 * @param string $errStr
	 * @param string $errFile
	 * @param int $errLine
	 * @param null $errContext
	 * @return mixed handleError()
	 */
	static public function handleError($errNo, $errStr, $errFile, $errLine, $errContext = null){
		if ($errNo == '2048') {
			return true;
		}
		global $service;
		$log =& $service->get('log');
		if (!is_object($log)) $log = new Log();
		$log->add($errStr, $errFile, $errLine, $errNo);
	}

	/**
	 * Appends a message line to the log
	 *
	 * Before: $msg, $file=null, $line=null, $errtype=E_WARNING, $logfile=LOG_FILE_SYSTEM
	 * Current declaration: $msg, $errtype=E_WARNING, $logfile=LOG_FILE_SYSTEM, $file=null, $line=null
	 *
	 * @access public
	 *
	 * @param string $msg
	 * @param int $errtype
	 * @param string $logfile
	 * @param string $file
	 * @param string $line
	 * @return void add()
	 */
	public function add($msg, $errtype=E_WARNING, $logfile=LOG_FILE_SYSTEM, $file=null, $line=null) {
		//Switch parameters from old declaration to new declaration :
		if (!is_numeric($errtype)) {
			$params = [$msg,$errtype,$logfile,$file,$line]; //[$msg,$file,$line,$errtype,$logfile];
			$file = $params[1];
			$line = $params[2];
			$errtype = $params[3];
			$logfile = $params[4];
		}

		//Only debug backtrace if DEBUG mode is on.
		if (defined('DEBUG') && DEBUG && is_null($file) && is_null($line)) {
			$bt = debug_backtrace(1);
			$caller = array_shift($bt);
			$file = $caller['file'];
			$line = $caller['line'];
		}
		if (is_null($file)) $file = '';
		if (is_null($line)) $line = '';

		$file = str_replace(ROOT,'',$file);
		if ($this->enabled == false) return;
		if ($this->level == 0 || $this->level == null ||
			($this->level == 1 && in_array($errtype,array(E_ERROR ,E_CORE_ERROR,E_COMPILE_ERROR,E_USER_ERROR,E_RECOVERABLE_ERROR))) ||
			($this->level == 2 && in_array($errtype,array(E_ERROR ,E_CORE_ERROR,E_COMPILE_ERROR,E_USER_ERROR,E_RECOVERABLE_ERROR,E_WARNING,E_CORE_WARNING,E_COMPILE_WARNING,E_USER_WARNING))) ||
			($this->level == 3 && $errtype != E_DEPRECATED && $errtype != E_USER_DEPRECATED)){

			$this->messages[$logfile][] = array(
				'message' => $msg,
				'file' => $file,
				'line' => $line,
				'errtype' => $errtype,
				'time' => date('Y-m-d H:i:s',time())
			);
		}
	}

    /**
     * Clears the message logs.
     *
     * Useful for long running process that can run out of memory because of too much
     * logging stored.
     *
     * @return bool $success
     */
    public function flush(){
        $this->messages = array();
        return true;
    }

	/**
	 * Outputs the log messages to the concerned files
	 *
	 * Currently the only supported output modes are  OUTPUT_TEXT and OUTPUT_HTML
	 *
	 * @access public
	 *
	 * @param string $outputmode
	 * @param string $outputto
	 * @return string output()
	 */
	public function output($outputmode = OUTPUT_MODE_TEXT,$outputto = OUTPUT_TO_FILE){
		if ($this->enabled == false) return;

		global $service;

		$code = '';
		if ($outputmode == OUTPUT_MODE_HTML) {
			$code = '<div class="logoutput"><div class="tabbable"><ul class="nav nav-tabs">';
			$i = 1;
			foreach ($this->messages as $k => $v) {
				switch($k) {
					case LOG_FILE_DATAINTEGRITY : {$title = LOG_FILE_DATAINTEGRITY_DSC;}break;
					case LOG_FILE_DATABASE : {$title = LOG_FILE_DATABASE_DSC;}break;
					case LOG_FILE_STATS : {$title = LOG_FILE_STATS_DSC;}break;
					case LOG_FILE_SECURITY : {$title = LOG_FILE_SECURITY_DSC;}break;
					case LOG_FILE_SYSTEM : {$title = LOG_FILE_SYSTEM_DSC;}break;
					case LOG_FILE_HTTPERRORS : {$title = LOG_FILE_HTTPERRORS_DSC;}break;
				}
				$code .= '<li'.(($i==1)?' class="active"':'').'><a href="#logtab'.$i.'" data-toggle="tab">'.$title.'</a></li>';
				$i++;
			}
			$code .= '</ul><div class="tab-content">';
		}
		$i = 1;
		foreach ($this->messages as $k => $v) {
			if ($outputmode == OUTPUT_MODE_HTML) {
				$code .= '<div class="tab-pane'.(($i == 1)?' active':'').'" id="logtab'.$i.'">';
			}
			$output = '';
			switch ($outputto) {
				case OUTPUT_TO_FILE : {
					foreach ($v as $km => $vm) {
						if ($this->isLoggable($vm)){
							$output .= $this->renderLine($vm);
						}
					}
					file_put_contents(DATAROOT.'log/'.$k.'.log',$output,FILE_APPEND);
				}break;
				case OUTPUT_TO_SCREEN : {
					foreach ($v as $km => $vm) {
						if ($this->isLoggable($vm)){
							if ($outputmode == OUTPUT_MODE_HTML) {
								$errorcls = " class=\"".str_replace(' ','',strtolower('error_'.$vm['errtype'])).'"';
								$output .= '<p'.$errorcls.'><strong>'.$vm['message'].'</strong><br><small>File: '.$vm['file'].' &bull; Line: '.$vm['line'].' &bull; Error type: '.$errortypes[$vm['errtype']]."</small></p>";
							} else {
								$output .= $this->renderLine($vm);
							}
						}
					}
					$code .= $output;
				}break;
				case OUTPUT_TO_SYSLOG : {
					$output = array();
					foreach ($v as $km => $vm) {
						if ($this->isLoggable($vm)){
							$output .= $this->renderLine($vm);
						}
					}
					foreach ($output as $k => $v) syslog(LOG_ERR,$v);
				}break;
				case OUTPUT_TO_MAIL : {
					foreach ($v as $km => $vm) {
						if ($this->isLoggable($vm)){
							$output .= $this->renderLine($vm);
						}
					}
					if ($outputmode == OUTPUT_MODE_HTML) $output = '<p>'.str_replace("\n","</p><p>",$output).'</p>';
                    $code .= $output;
				}break;
				case OUTPUT_TO_SMS : {
                    //@TODO : Send logs via SMS
                }break;
				case OUTPUT_TO_RETURN  :{
                    //TODO : Return output
                }break;
			}
			if ($outputto == OUTPUT_TO_SCREEN && $outputmode == OUTPUT_MODE_HTML) {
				$code .= '</div>';
			}
			$i++;
		}
        //Output logs to screen
		if ($outputto == OUTPUT_TO_SCREEN && $outputmode == OUTPUT_MODE_HTML) {
			$code .= '</div></div></div>';
			global $hasTheme;
			if ($hasTheme) echo $code;
		}
        //Send logs by email
        if ($outputto == OUTPUT_TO_EMAIL) {
            if (defined('EMAIL_HOST') && defined('EMAIL_USER') && defined('EMAIL_PASS')) {
                $service->get('Ressource')->get('core/protocol/email');
                $email = new Email();
                $email->to($service->get('Setting')->get('adminmail'));
                $email->from($service->get('Setting')->get('adminmail'));
                $email->body($code);
                $email->send();
            }
            else {
                mail($service->get('Setting')->get('adminmail'),'Log file',$code);
            }
        }
	}

	/**
	 * Called on script termination
	 *
	 * @access public
	 *
	 * @return void shutdown()
	 */
	public function shutdown(){
		if ($this->enabled == false) return;
		global $service;
		if ($service->isLoaded('Db')) {
			$db =& $service->get('Db');
			$this->add(sprintf(LOG_SHUTDOWN_NBQUERIES,$db->getQueryCount(),$_SERVER['REQUEST_URI']),__FILE__,__LINE__,E_INFO, LOG_FILE_STATS);
			$this->add(sprintf(LOG_SHUTDOWN_QUERIESTIMER,$db->getTotalTime(),$_SERVER['REQUEST_URI']),__FILE__,__LINE__,E_INFO, LOG_FILE_STATS);
		}

		$this->add(sprintf(LOG_SHUTDOWN_PEAKMEM,number_format(memory_get_peak_usage()/1024,2),$_SERVER['REQUEST_URI']),__FILE__,__LINE__,E_INFO, LOG_FILE_STATS);
		$this->add(sprintf(LOG_SHUTDOWN_ENDMEM,number_format(memory_get_usage()/1024,2),$_SERVER['REQUEST_URI']),__FILE__,__LINE__,E_INFO, LOG_FILE_STATS);
		$outputmode = OUTPUT_MODE_TEXT;
		$outputdest = array(OUTPUT_TO_SCREEN,OUTPUT_TO_FILE,OUTPUT_TO_SYSLOG,OUTPUT_TO_MAIL);
		$outputdest = $outputdest[$this->destination];
		if ($outputdest == OUTPUT_TO_SCREEN || $outputdest == OUTPUT_TO_MAIL) {
            $outputmode = OUTPUT_MODE_HTML;
        }
		if ($this->destination != null) $this->output($outputmode,$outputdest);
		else $this->output();
	}

	/**
	 * Checks if logging functions are enabled.
	 *
	 * @access public
	 * @return bool True or False depending on whether logging is enabled.
	 */
	public function checkSettings(){
		if ($this->enabled == true) {
			global $service;
			$this->enabled = $service->get('Setting')->get('developmentmode');
            if ($this->enabled) {
                error_reporting(E_ALL);
                ini_set('display_errors', 1);
            }
		}
		if ($this->level == null) {
			global $service;
			$this->level = $service->get('Setting')->get('loglevel');
		}
		if ($this->destination == null) {
			global $service;
			$this->destination = $service->get('Setting')->get('logdestination');
		}
	}

	/**
	 * Checks if an element is loggable based on current log level and error type.
	 *
	 * @public
	 *
	 * @return bool True if message should be logged
	 */
	private function isLoggable($m){
		if ($this->level == 0 || $this->level == null ||
			($this->level == 1 && in_array($m['errtype'],array(E_ERROR ,E_CORE_ERROR,E_COMPILE_ERROR,E_USER_ERROR,E_RECOVERABLE_ERROR))) ||
			($this->level == 2 && in_array($m['errtype'],array(E_ERROR ,E_CORE_ERROR,E_COMPILE_ERROR,E_USER_ERROR,E_RECOVERABLE_ERROR,E_WARNING,E_CORE_WARNING,E_COMPILE_WARNING,E_USER_WARNING,))) ||
			($this->level == 3 && $m['errtype'] != E_DEPRECATED && $m['errtype'] != E_USER_DEPRECATED)){
			return true;
		}
		return false;
	}

	/**
	 * Renders a log line
	 *
	 * @public
	 * @paramarray $line A log message array
	 * @return string The log line rendered in text mode
	 */
	private function renderLine($line){
		$errortypes = array(
			E_ERROR => 'Error',
			E_WARNING => 'Warning',
			E_NOTICE => 'Notice',
			E_CORE_ERROR => 'Core Error',
			E_CORE_WARNING => 'Core Warning',
			E_COMPILE_ERROR => 'Compile Error',
			E_COMPILE_WARNING => 'Compile Warning',
			E_USER_ERROR => 'User Error',
			E_USER_WARNING => 'User Warning',
			E_USER_NOTICE => 'User Notice',
			E_RECOVERABLE_ERROR => 'Recoverable Error',
			E_DEPRECATED => 'Deprecated',
			E_USER_DEPRECATED => 'User Deprecated',
			E_INFO => 'Information',
			E_ALL => 'All',
		);
		return $line['time'].' :: '.str_pad($errortypes[$line['errtype']],15).' :: '.$line['message'].' :: File: '.$line['file'].' :: Line: '.$line['line']."\n";
	}
}
?>
