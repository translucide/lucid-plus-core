<?php
/**
 * Widgetzone classes
 *
 * Defines widgetzone classes (WidgetzoneData, WidgetzoneModel, WidgetzoneStore and Widgetzone)
 *
 * Sept 15, 2015
 *
 * @author 		Translucide
 * @copyright 	copyright (c) 2015 Translucide
 * @license		See docs/license.txt
 */

global $service;
$service->get('ressource')->get('core/user/group');

/**
 * Model class for Widgetzone data (WidgetzoneData) and Widgetzone (Widget)
 *
 * Usage: $obj = new WidgetzoneModel();
 *
 * @version 0.1
 * @since 0.1
 * @class WidgetzoneModel
 * @package	sys
 * @subpackage display
 * @see WidgetzoneModel
 */
class WidgetzoneModel extends Model {

	/**
	 * PHP 5 Constructor
	 * @return void
	 */
	public function __construct() {
		$this->WidgetzoneModel();
	}

	public function WidgetzoneModel(){
		global $service;
		$this->type = 'widgetzone';
		$this->initVar('widgetzone_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('widgetzone_objid',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('widgetzone_name',DATATYPE_STRING,array('length'=> 60));
		$this->initVar('widgetzone_provider',DATATYPE_STRING,array('length'=> 60));
		$this->initVar('widgetzone_title',DATATYPE_STRING,array('length'=> 60));
		$this->initVar('widgetzone_template',DATATYPE_INT);
		$this->initVar('widgetzone_options',DATATYPE_ARRAY);
		$this->initVar('widgetzone_site',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('widgetzone_created',DATATYPE_INT);
		$this->initVar('widgetzone_modified',DATATYPE_INT);
		$this->initVar('widgetzone_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
	}
}

class WidgetzoneData extends Data{
	function __construct(){
        parent::__construct('widgetzone');
	}
}

class WidgetzoneStore extends Store{
	public function __construct() {
		parent::__construct('widgetzone');
	}
}

class Widgetzone {
    public function __construct(){
    }

    /**
     * Creates missing zones defined in current theme
     *
     * @access public
     */
    public function createThemeZones(){
        global $service;
        $theme = $service->get('Ressource')->getTheme();
        $i = $theme->getInfo();
        $s = new WidgetzoneStore();
        $zones = $s->get(new Criteria('widgetzone_provider','theme'));
        $objs = array();
        foreach ($i['zones'] as $k => $v){
            $found = false;
            foreach ($zones as $kk => $vv){
                if ($vv->getVar('widgetzone_name') == $v['name']) {
                    $found = true;
                }
            }
            if (!$found) {
                $obj = $s->create();
                $obj->setVar('widgetzone_name',$v['name']);
                $obj->setVar('widgetzone_provider','theme');
                $obj->setVar('widgetzone_title',$v['title']);
                $objs[] = $obj;
            }
        }
        if (count($objs) > 0) $s->save($objs);
        return $this;
    }

    /**
     * Returns all widget zones, as an array of arrays with id and title members
     *
     * @access public
     *
     * @return array Array[][id,title]
     */
    public function get($objid=null){
        $s = new WidgetzoneStore();
        if ($objid != null && $objid > 0) $o = $s->get($objid);
        else $o = $s->get();
        $ret = array();
        foreach($o as $k => $v) {
            $ret[] = array(
                'name' => $v->getVar('widgetzone_name'),
                'title' => $v->getVar('widgetzone_title')
            );
        }
        return $ret;
    }
}
?>
