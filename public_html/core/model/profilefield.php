<?php
/**
 * Profilefield classes
 *
 * Defines Profilefield classes (ProfilefieldData, ProfilefieldModel, ProfilefieldStore)
 *
 * Sept 15, 2015
 *
 * @author 		Translucide
 * @copyright 	copyright (c) 2015 Translucide
 * @license		See docs/license.txt
 */

/**
 * Model class for Profilefield data (ProfilefieldData) and Profilefield (Widget)
 *
 * Usage: $obj = new ProfilefieldModel();
 *
 * @version 0.1
 * @since 0.1
 * @class ProfilefieldModel
 * @package	sys
 * @subpackage display
 * @see ProfilefieldModel
 */
class ProfilefieldModel extends Model {

	/**
	 * PHP 5 Constructor
	 * @return void
	 */
	public function __construct() {
		$this->ProfilefieldModel();
	}

	public function ProfilefieldModel(){
		global $service;
		$this->type = 'profilefield';
		$this->initVar('profilefield_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('profilefield_objid',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('profilefield_position',DATATYPE_INT);
		$this->initVar('profilefield_section',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('profilefield_component',DATATYPE_STRING, array('length' => 50));
		$this->initVar('profilefield_type',DATATYPE_STRING, array('length' => 30));
		$this->initVar('profilefield_key',DATATYPE_STRING, array('length' => 128));
		$this->initVar('profilefield_title',DATATYPE_STRING, array('length' => 128));
		$this->initVar('profilefield_defaultvalue',DATATYPE_STRING, array('length' => 128));
		$this->initVar('profilefield_options',DATATYPE_ARRAY);
		$this->initVar('profilefield_language',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('profilefield_site',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('profilefield_created',DATATYPE_INT);
		$this->initVar('profilefield_modified',DATATYPE_INT);
		$this->initVar('profilefield_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
	}
}
class ProfilefieldData extends Data{
	function __construct(){
        parent::__construct('profilefield');
	}
}

class ProfilefieldStore extends Store{
	public function __construct() {
		parent::__construct('profilefield');
	}
}
