<?php
/**
 * Tenants model
 *
 * Manages tenants
 *
 * Sept. 2, 2017

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

/**
 * MYSQL Table creation query...
 */
/*
DROP TABLE IF EXISTS `tenant`;
CREATE TABLE `tenant` (
  `tenant_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tenant_name` VARCHAR(30) NOT NULL,
  `tenant_adminuids` TEXT NOT NULL,
  `tenant_userids` TEXT NOT NULL,
  `tenant_created` INT UNSIGNED NOT NULL,
  `tenant_createdby` INT UNSIGNED NOT NULL,
  `tenant_modified` INT UNSIGNED NOT NULL,
  `tenant_modifiedby` INT UNSIGNED NOT NULL,
  `tenant_deleted` INT UNSIGNED NOT NULL,
  `tenant_deletedby` INT UNSIGNED NOT NULL,

  PRIMARY KEY (`tenant_id`)
)
ENGINE = MyISAM
CHARACTER SET utf8 COLLATE utf8_general_ci;
*/

class TenantModel extends Model {
	function __construct(){
		$this->initVar('tenant_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('tenant_name',DATATYPE_STRING,array('length'=>30));
		$this->initVar('tenant_default',DATATYPE_BOOL);
		$this->initVar('tenant_created',DATATYPE_TIMESTAMP);
		$this->initVar('tenant_createdby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('tenant_deleted',DATATYPE_TIMESTAMP);
		$this->initVar('tenant_deletedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('tenant_modified',DATATYPE_TIMESTAMP);
		$this->initVar('tenant_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->type = 'tenant';
	}
}

class TenantData extends Data {
	function __construct(){
        parent::__construct('tenant');
	}
}


class TenantStore extends Store{
	public function __construct(){
		parent::__construct('tenant');
		$this->setOption('ignorelangs',true);
		$this->setOption('ignoresites',true);
		$this->setOption('ignoretenant',true);
		$this->setOption('usecache',false);
	}

    /**
     * Returns Tenant Object when passed Tenant name
     *
     * You can also specify an user Id that must
     * match an admins or regular user in tenant record
     *
     * @access public
     * @param string $name
     * @param int $uid
     * @return TenantData $tenant
     */
    public function getByName($name) {
        $crit = $this->initCrit();
        $crit->add(new Criteria('tenant_name',$name));
		$t = $this->get($crit);
        return (count($t) > 0)?$t[0]:false;
    }

    public function getDefaultTenantId() {
        $crit = $this->initCrit();
        $crit->add(new Criteria('tenant_default',1));
        $crit->setLimit(0,1);
		$t = $this->get($crit);
        return (count($t) > 0)?$t[0]->getVar('tenant_id'):0;        
    }

    public function initCrit($params=['global' => true, 'deleted'=> false]) {
        $crit = new CriteriaCompo();
        if ($params['global'] == false) $crit->add(new Criteria('tenant_id',$_SESSION['tenant']));
        $crit->add(new Criteria('tenant_deleted',0));
        return $crit;
    }
}
