<?php
/**
 * Profilesection classes
 *
 * Defines Profilesection classes (ProfilesectionData, ProfilesectionModel, ProfilesectionStore)
 *
 * Sept 15, 2015
 *
 * @author 		Translucide
 * @copyright 	copyright (c) 2015 Translucide
 * @license		See docs/license.txt
 */

/**
 * Model class for Profilesection data (ProfilesectionData) and Profilesection (Widget)
 *
 * Usage: $obj = new ProfilesectionModel();
 *
 * @version 0.1
 * @since 0.1
 * @class ProfilesectionModel
 * @package	sys
 * @subpackage display
 * @see ProfilesectionModel
 */
class ProfilesectionModel extends Model {

	/**
	 * PHP 5 Constructor
	 * @return void
	 */
	public function __construct() {
		$this->ProfilesectionModel();
	}

	public function ProfilesectionModel(){
		global $service;
		$this->type = 'profilesection';
		$this->initVar('profilesection_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('profilesection_objid',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('profilesection_title',DATATYPE_STRING, array('length' => 128));
		$this->initVar('profilesection_language',DATATYPE_INT);
		$this->initVar('profilesection_site',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('profilesection_created',DATATYPE_INT);
		$this->initVar('profilesection_modified',DATATYPE_INT);
		$this->initVar('profilesection_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
	}
}
class ProfilesectionData extends Data{
	function __construct(){
        parent::__construct('profilesection');
	}
}

class ProfilesectionStore extends Store{
	public function __construct() {
		parent::__construct('profilesection');
	}
}
