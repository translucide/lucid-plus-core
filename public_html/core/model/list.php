<?php
/**
 * List model
 *
 * Manages lists, with multi-tenant support
 *
 * October 28, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class ListModel extends Model {
	function __construct(){
		$this->initVar('list_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('list_tenant',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('list_type',DATATYPE_STRING,array('length' => 50));
		$this->initVar('list_title',DATATYPE_STRING,array('length' => 100));
		$this->initVar('list_value',DATATYPE_INT);
		$this->initVar('list_options',DATATYPE_ARRAY);
		$this->initVar('list_deleted',DATATYPE_TIMESTAMP);
		$this->initVar('list_deletedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('list_created',DATATYPE_TIMESTAMP);
		$this->initVar('list_createdby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('list_modified',DATATYPE_TIMESTAMP);
		$this->initVar('list_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->type = 'list';
	}
}

class ListData extends Data {
	function __construct(){
        parent::__construct('list');
	}
}

class ListStore extends Store{
	public function __construct(){
		parent::__construct('list');
	}

    /**
     * Returns a list structured in a way that is ready
     * to pass along to select form field options lists
     *
     * @access public
     * @param string $listtype The list_type col value
     * @param Criteria $sel A criteria object, or a list of numeric values
     * @param bool $valueaskey Whether to use record values as array keys
     * @return array $options List array
     **/
    public function getList($listtype, $sel = false){
        $crit = $this->initCrit($listtype);
        if (!is_object($sel) && $sel != false) {
            if (!is_array($sel)) $sel = array($sel);
            $crit->add(new Criteria('list_value',$this->inClause($sel),'IN'));
        }else {
            if (is_object($sel)) $crit->add($sel);
        }
        if ($listtype == 'expensetype') $crit->setSort('list_title','DESC');
        else $crit->setSort('list_title','ASC');
        $objs = $this->get($crit);
        $data = array();
        if (count($objs) > 0) {
            foreach ($objs as $k => $v) {
                if ($v->getVar('list_value') > 0) {
                    if ($valueaskey) $data[$v->getVar('list_value')] = array('title' => $v->getVar('list_title'), 'value' => $v->getVar('list_value'));
                    else $data[] = array('title' => $v->getVar('list_title'), 'value' => $v->getVar('list_value'));
                }
            }
        }
        return $data;
    }

    /**
     * Modifies an options array to use the values as keys option.
     *
     * @access public
     * @param array $options
     * @return array $options
     **/
    public function valueKeys($listarray){
        $newlist = array();
        foreach ($listarray as $k => $v) {
            $newlist[$v['value']] = $v;
        }
        return $newlist;
    }

    public function initCrit($type,$params=['deleted'=> false]){
        $crit = new CriteriaCompo();
        if ($type != '') $crit->add(new Criteria('list_type',$type));
        if ($params['deleted'] == false) $crit->add(new Criteria('list_deleted',0));
        return $crit;
    }

    public function createItem($type,$title,$value=''){
        //1. Test if item already exists under current project...
        $crit = $this->initCrit($type);
        $crit->add(new Criteria('list_title',$title));
        $res = $this->get($crit);
        if (count($res) > 0) {
            $item = $res[0];
        }
        //2. Create item if not found
        else {
            if ($value == '') {
                $crit = new CriteriaCompo();
                $crit->add(new Criteria('list_type',$type));
                $value = $this->getNext('list_value',$crit);
            }
            $item = new ListData();
            $item->setVar('list_id',0);
            $item->setVar('list_type',$type);
            $item->setVar('list_tenant',$_SESSION['tenant']);
            $item->setVar('list_title',$title);
            $item->setVar('list_value',$value);
            $item->setVar('list_deleted',0);
            $item->setVar('list_deletedby',0);
            if ($this->save($item)) $item->setVar('list_id',$this->getLastId());
            else $item = false;
        }
        return $item;
    }
}
