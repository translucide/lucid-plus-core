<?php
/**
 * Profile classes
 *
 * Defines Profile classes (ProfileData, ProfileModel, ProfileStore)
 *
 * Sept 15, 2015
 *
 * @author 		Translucide
 * @copyright 	copyright (c) 2015 Translucide
 * @license		See docs/license.txt
 */

/**
 * Model class for Profile data (ProfileData) and Profile (Widget)
 *
 * Usage: $obj = new ProfileModel();
 *
 * @version 0.1
 * @since 0.1
 * @class ProfileModel
 * @package	sys
 * @subpackage display
 * @see ProfileModel
 */
class ProfileModel extends Model {

	/**
	 * PHP 5 Constructor
	 * @return void
	 */
	public function __construct() {
		$this->ProfileModel();
	}

	public function ProfileModel(){
		global $service;
		$this->type = 'profile';
		$this->initVar('profile_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('profile_userid',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('profile_key',DATATYPE_STRING,array('length'=>128));
		$this->initVar('profile_value',DATATYPE_STRING);
		$this->initVar('profile_site',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('profile_created',DATATYPE_INT);
		$this->initVar('profile_modified',DATATYPE_INT);
		$this->initVar('profile_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
	}
}
class ProfileData extends Data{
	function __construct(){
        parent::__construct('profile');
	}
}

class ProfileStore extends Store{
	public function __construct() {
		parent::__construct('profile');
	}

	/**
	 * Returns user profile for a given User ID
	 * or for the current user if none specified
	 *
	 * @access public
	 *
	 * @var int $uid User ID
	 * @return object $profile
	 */
	public function getByUser($uid=0){
		global $service;
		if (!$uid) $uid = $service->get('User')->getUid();
		if ($uid) {
			return $this->get(new Criteria('profile_userid',$uid));
		}
		return false;
	}

	/**
	 * Returns a single profile field
	 *
	 * @access public
	 *
	 * @var string $key Profile field name
	 * @var int $uid User ID
	 * @return mixed $value Profile variable value.
	 */
	public function getKey($key, $uid=0) {
		global $service;
		$crit = new CriteriaCompo();
		if (is_array($uid) || $uid > 0) {
			if (!is_array($uid)) $crit->add(new Criteria('profile_userid',$uid));
			else $crit->add(new Criteria('profile_userid','('.implode(',',$uid).')','IN'));
		}
		if ($uid == 0) {
			$crit->add(new Criteria('profile_userid',$service->get('User')->getUid()));
		}
		$crit->add(new Criteria('profile_key',$key));
		return $this->get($crit);
	}

	public function getValue($key,$uid=0) {
		$objs = $this->getKey($key,$uid);
		if (is_numeric($uid)) {
			if (is_object($objs[0])) {
				return $objs[0]->getVar('profile_value');
			}
			else return '';
		}
		else {
			$vals = array();
			foreach ($obj as $k => $v) {
				$vals[$v->getVar('profile_userid')] = $v->getVar('profile_value');
			}
		}
		return $vals;
	}
}
