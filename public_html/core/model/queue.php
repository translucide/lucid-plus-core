<?php
/**
 * Queue classes
 *
 * Defines queue classes (QueueData, QueueModel, QueueStore)
 *
 * August 4, 2016
 *
 * @author 		Translucide
 * @copyright 	copyright (c) 2015 Translucide
 * @license		See docs/license.txt
 */

/**
 * Model class for Queue data (QueueData) and Queue (Widget)
 *
 * Usage: $obj = new QueueModel();
 *
 * @version 0.1
 * @since 0.1
 * @class QueueModel
 * @package	sys
 * @subpackage display
 * @see QueueModel
 */
class QueueModel extends Model {

	/**
	 * PHP 5 Constructor
	 * @return void
	 */
	public function __construct() {
		$this->QueueModel();
	}

	public function QueueModel(){
		global $service;
		$this->type = 'queue';
		$this->initVar('queue_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('queue_type',DATATYPE_STRING,array('length'=> 100));
		$this->initVar('queue_name',DATATYPE_STRING,array('length'=> 100));
		$this->initVar('queue_component',DATATYPE_STRING,array('length'=> 60));
		$this->initVar('queue_action',DATATYPE_STRING,array('length'=> 45));
		$this->initVar('queue_status',DATATYPE_INT);
		$this->initVar('queue_data',DATATYPE_ARRAY);
		$this->initVar('queue_options',DATATYPE_ARRAY);
		$this->initVar('queue_site',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('queue_created',DATATYPE_INT);
		$this->initVar('queue_modified',DATATYPE_INT);
		$this->initVar('queue_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
	}
}

class QueueData extends Data{
	function __construct(){
        parent::__construct('queue');
	}
}

class QueueStore extends Store{
	public function __construct() {
		parent::__construct('queue');
	}
}
?>
