<?php
/**
 * Tag classes
 *
 * Defines tag classes (TagData, TagModel, TagStore)
 *
 * Sept 15, 2015
 *
 * @author 		Translucide
 * @copyright 	copyright (c) 2015 Translucide
 * @license		See docs/license.txt
 */

/**
 * Model class for Tag data (TagData) and Tag (Widget)
 *
 * Usage: $obj = new TagModel();
 *
 * @version 0.1
 * @since 0.1
 * @class TagModel
 * @package	sys
 * @subpackage display
 * @see TagModel
 */
class TagModel extends Model {

	/**
	 * PHP 5 Constructor
	 * @return void
	 */
	public function __construct() {
		$this->TagModel();
	}

	public function TagModel(){
		global $service;
		$this->type = 'tag';
		$this->initVar('tag_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('tag_objid',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('tag_title',DATATYPE_STRING,array('length'=> 45));
		$this->initVar('tag_url',DATATYPE_STRING,array('length'=> 45));
		$this->initVar('tag_weight',DATATYPE_INT);
		$this->initVar('tag_language',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('tag_site',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('tag_created',DATATYPE_INT);
		$this->initVar('tag_modified',DATATYPE_INT);
		$this->initVar('tag_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
	}
}

class TagData extends Data{
	function __construct(){
        parent::__construct('tag');
	}
}

class TagStore extends Store{
	public function __construct() {
		parent::__construct('tag');
	}
	public function createTags($tags) {
		global $service;
		$db =& $service->get('Db');
		if (!is_array($tags)) {
			$tags = explode(',',$tags);
			foreach ($tags as $k => $v) {
				if (trim($v) == '') unset($tags[$k]);
			}
		}
		$objs = $this->get(new Criteria('tag_title','(\''.implode('\',\'',$tags).'\')','IN'));
		$saved = array();
		foreach ($objs as $v) $saved[] = $v->getVar('tag_title');
		foreach ($tags as $k => $v){
			foreach ($saved as $s) {
				if (strtolower($v) == strtolower($s)) unset($tags[$k]);
			}
		}
		foreach($tags as $v) {
			$obj = $this->create();
			$obj->setVar('tag_title',$v);
			$obj->setVar('tag_url',$service->get('Url')->toUrl($v));
			$this->save($obj);
		}
	}
	public function toStrings($ids) {
		$objs = $this->get(new Criteria('content_objid','('.implode(',',$ids).')','IN'));
		$str = '';
		foreach ($objs as $k => $v) {
			if ($k > 0) $str .= ',';
			$str .= $v->getVar('tag_title');
		}
		return $str;
	}

	public function toIds($str){
		$this->setOption('ignorelangs',false);
		$str = explode(',',$str);
		$crit = new Criteria('tag_title','(\''.implode('\',\'',$str).'\')','IN');
		$objs = $this->get($crit);
		$ids = array();
		foreach ($objs as $v) {
			$key = array_search($v->getVar('tag_title'),$str);
			if ($key !== false) {
				unset($str[$key]);
			}
			$ids[] = $v->getvar('tag_objid');
		}
		foreach ($str as $v) {
			$obj = $this->create();
			$obj->setVar('tag_language',$service->get('Language')->getId());
			$obj->setVar('tag_title',$v);
			$obj->setVar('tag_objid',$this->getNext('tag_objid'));
			$result = $this->save($obj);
			if ($result > 0) {
				$ids[] = $result;
				unset($str);
			}
		}
		return $ids;
	}
}
