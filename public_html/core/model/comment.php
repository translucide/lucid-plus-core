<?php
/**
 * Comment classes
 *
 * Defines Comment classes (CommentData, CommentModel, CommentStore)
 *
 * Sept 15, 2015
 *
 * @author 		Translucide
 * @copyright 	copyright (c) 2015 Translucide
 * @license		See docs/license.txt
 */

/**
 * Model class for Comment data (CommentData) and Comment (Widget)
 *
 * Usage: $obj = new CommentModel();
 *
 * @version 0.1
 * @since 0.1
 * @class CommentModel
 * @package	sys
 * @subpackage display
 * @see CommentModel
 */
class CommentModel extends Model {

	/**
	 * PHP 5 Constructor
	 * @return void
	 */
	public function __construct() {
		$this->CommentModel();
	}

	public function CommentModel(){
		global $service;
		$this->type = 'comment';
		$this->initVar('comment_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('comment_contentobjid',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('comment_userid',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('comment_approved',DATATYPE_INT);
		$this->initVar('comment_parent',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('comment_position',DATATYPE_INT);
		$this->initVar('comment_index',DATATYPE_INT);
		$this->initVar('comment_level',DATATYPE_INT);
		$this->initVar('comment_content',DATATYPE_STRING);
		$this->initVar('comment_attachments',DATATYPE_STRING);
		$this->initVar('comment_language',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('comment_site',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('comment_created',DATATYPE_INT);
		$this->initVar('comment_modified',DATATYPE_INT);
		$this->initVar('comment_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
	}
}
class CommentData extends Data{
	function __construct(){
        parent::__construct('comment');
	}
}

class CommentStore extends Store{
	public function __construct() {
		parent::__construct('comment');
	}

    public function getCommentsForContent($contentobjid,$page=0,$perpage=20) {
        global $service;
        $db =& $service->get('Db');
        $res = $db->query('SELECT `comment_id`,`comment_parent`,`comment_position`,`comment_index`,`comment_level`,`comment_userid`,`comment_content`,`comment_attachments`,`comment_created`,`user_id`,`user_name` FROM `'.$db->prefix('comment').'` as `comment` '.
			'LEFT JOIN `'.$db->prefix('user').'` as `user` ON `comment`.`comment_userid` = `user`.`user_id` '.
			'WHERE '.
            '`comment`.`comment_contentobjid` = '.intval($contentobjid).' AND '.
            '`comment`.`comment_approved` = 1 AND'.
            '`comment`.`comment_site` = '.intval($service->get('Site')->getId()).' AND '.
            '`comment`.`comment_language` = '.intval($service->get('Language')->getId()).' '.
            'ORDER BY `comment`.`comment_position` ASC '.
            'LIMIT '.intval($page*$perpage).','.intval($perpage)
        );
        $s = array('{uploadurl}','{themeurl}','{url}');
        $r = array(UPLOADURL,THEMEURL,URL);
        foreach ($res as $k => $v) {
            $v['comment_content'] = str_replace('http:'.$s,$r,$v['comment_content']);
            $v['comment_content'] = str_replace('https:'.$s,$r,$v['comment_content']);
            $v['comment_content'] = str_replace($s,$r,$v['comment_content']);
            $res[$k]['comment_content'] = $v['comment_content'];
        }
        return $res;
    }

	public function updatePositions($cid,$minpos,$direction = 'bottom',$offset = 1) {
		global $service;
		$db = $service->get('Db');
		$offset = intval($offset);
		if ($direction == 'bottom') {
			$db->query('UPDATE `'.$db->prefix('comment').'` SET `comment_position` = `comment_position` + '.$offset.' WHERE '.
				'`comment_position` >= '.$minpos.' AND '.
				'`comment_contentobjid` = \''.intval($cid).'\' AND '.
				'`comment_site` = \''.intval($service->get('Site')->getId()).'\' AND '.
				'`comment_language` = \''.intval($service->get('Language')->getId()).'\''
			);
		}
		else {
			$db->query('UPDATE `'.$db->prefix('comment').'` SET `comment_position` = `comment_position` - '.$offset.' WHERE '.
				'`comment_position` >= '.$minpos.' AND '.
				'`comment_contentobjid` = \''.intval($cid).'\' AND '.
				'`comment_site` = \''.intval($service->get('Site')->getId()).'\' AND '.
				'`comment_language` = \''.intval($service->get('Language')->getId()).'\''
			);
		}
	}

	public function getParent($cobjid,$parent){
		global $service ;
		$crit = new CriteriaCompo();
		$crit->add(new Criteria('comment_contentobjid',$cobjid));
		$crit->add(new Criteria('comment_id',$parent));
		return $this->get($crit);
	}
}
