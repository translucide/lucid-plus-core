<?php
/**
 * View class
 *
 * Handles outputs
 *
 * Jan 17, 2013

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
interface ViewInterface{
	function init();
	function render();
}

class View extends BaseClass implements ViewInterface {

	/**
	 * Holds the data to insert into the template
	 *
	 * @access public
	 * @var array $data
	 */
	var $data;

	/**
	 * Hold a reference to the template file to use for rendering
	 *
	 * @access public
	 * @var string $template
	 */
	var $template;

	/**
	 * View constructor
	 *
	 * @access public
	 * @return void Initializes the object
	 */
	public function __construct(){
        $this->data = array();
		$this->template = '';
		$this->init();
	}

	/**
	 * Init method
	 *
	 * @access public
	 * @return void Initialize the view object
	 */
	public function init(){
	}

	/**
	 * Renders the view
	 *
	 * @access public
	 * @return void Renders the view
	 */
	public function render(){
		return $this->renderTemplate();
	}

	/**
	 * Renders the view template
	 *
	 * @access public
	 * @return string Rendered view
	 */
	public function renderTemplate(){
		$tpl = '';
		if ($this->template != '') {
			global $service;
			$service->get('Ressource')->get('core/display/template');
			$tpl = new Template(ROOT.$this->template);
		}
		if (is_object($tpl)){
			foreach ($this->data as $k => $v){
				$tpl->assign($k,$v);
			}
		}
		return $tpl->render();
	}

	/**
	 * Sets a view variable
	 *
	 * @access public
	 * @param string $var Variable name
	 * @param mixed $val Variable value
	 * @return void Sets a view variable
	 */
	public function setVar($var,$val) {
		$this->data[$var] = $val;
	}

	/**
	 * Gets a view variable
	 *
	 * @access public
	 * @param string $var Variable name
	 * @return mixed View variable
	 */
	public function getVar($var) {
		return $this->data[$var];
	}

	/**
	 * Sets all view variables at once
	 *
	 * @access public
	 *
	 * @param array $data The view data array
	 * @return void Sets the view variables
	 */
	public function set($data){
		$this->data = $data;
	}

	/**
	 * Sets a view template
	 *
	 * @access public
	 *
	 * @param string $template
	 * return void Sets the template file to use in this view.
	 */
	public function setTemplate($template){
		$this->template = $template;
	}

	/**
	 * Returns an HTML string from breadcrumbs data
	 *
	 * @access public
	 *
	 * @return string
	 */
	public function getBreadcrumbs(){
		$code .= '';
		if (isset($this->data['breadcrumbs'])) {
			$code = '<ul class="breadcrumb">';
			$passedfirst = false;
			$count = 0;
			$countb = count($this->data['breadcrumbs']);
			foreach ($this->data['breadcrumbs'] as $k => $v) {
				if ($passedfirst) $code .= '<span class="divider">/</span></li>';
				$code .= '<li';
				if ($count == $countb-1) $code .= ' class="active"';
				$code .= '>';
				if ($count < $countb-1) $code .= '<a href="'.$v['link'].'">';
				$code .= $v['title'];
				if ($count < $countb-1) $code .= '</a> ';
				$passedfirst = true;
				$count++;
			}
			$code .= '</li></ul>';
		}
		return $code;
	}
}

/**
 * Default route view
 *
 * Default view loaded on home page request or unknown page requests
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;

class DefaultView extends View{
	public function init(){
	}
	public function render(){
		return '';//parent::render();
	}
}

?>
