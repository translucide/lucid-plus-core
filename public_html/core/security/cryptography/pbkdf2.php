<?php
/**
 * Class Pbkdf2
 *
 * PKCS #5 v2.0 standard RFC 2898
 *
 * November 11, 2012
 *
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

 
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

/**
 * PKCS #5 v2.0 standard RFC 2898
 */
global $service;
$service->get('Ressource')->get('core/security/cryptography/lib');
 
class Pbkdf2CryptographyMethod extends CryptographyMethodInterface {
	
	/**
	 * Class options
	 *
	 * @public
	 * @var array $options
	 */
	private $options;
	
	/**
	 * Scrypt constructor
	 *
	 * @public
	 * @param $pass
	 * @param @salt
	 * @return void  Scrypt()
	 */
	public function __construct($pass='',$salt=''){
		$this->Construct($pass,$salt);
	}
	
	public function Scrypt($pass='',$salt=''){
		$this->setPassword($pass);
		$this->setSalt($salt);
	}
	
	/**
	 * Sets the password to encrypt or validate
	 *
	 * @param string $password
	 * @return void setPassword()
	 */
	public function setPassword($password) {
		$this->options['password'] = $password;
	}
	
	/**
	 * Sets the salt to use
	 *
	 * @param string $salt
	 * @return void setSalt()
	 */
	public function setSalt($salt) {
		$this->options['salt'] = $salt;
	}
	
	/**
	 * Encrypts the password
	 *
	 * @param string $password
	 * @param string $salt
	 * @return string encrypted password
	 */
	public function encrypt(){
		if ($this->options['password'] == '' || $this->options['salt'] == '') {
			trigger_error('No password or salt set for encryption!');
		}
		return $this->calc('',$this->options['password'],$this->options['salt'],1000,128);
	}

    /**
     * Generate the new key
     *
     * @param  string  $hash       The hash algorithm to be used by HMAC
     * @param  string  $password   The source password/key
     * @param  string  $salt
     * @param  integer $iterations The number of iterations
     * @param  integer $length     The output size
     * @throws Exception\InvalidArgumentException
     * @return string
     */
    public static function calc($hash, $password, $salt, $iterations, $length)
    {
        if (!Hmac::isSupported($hash)) {
            trigger_error("The hash algorithm $hash is not supported by " . __CLASS__);
        }

        $num    = ceil($length / Hmac::getOutputSize($hash, Hmac::OUTPUT_BINARY));
        $result = '';
        for ($block = 1; $block <= $num; $block++) {
            $hmac = hash_hmac($hash, $salt . pack('N', $block), $password, Hmac::OUTPUT_BINARY);
            $mix  = $hmac;
            for ($i = 1; $i < $iterations; $i++) {
                $hmac = hash_hmac($hash, $hmac, $password, Hmac::OUTPUT_BINARY);
                $mix ^= $hmac;
            }
            $result .= $mix;
        }
        return substr($result, 0, $length);
    }
}
