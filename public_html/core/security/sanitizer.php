<?php
class Sanitizer {
    /**
     * Removes a list of tags in an HTML fragment string
     */
    public function removeForbiddenTags($html,$tags=SANITIZER_FORBIDDEN_HTML_TAGS) {
        // create a new DomDocument object
        $doc = new DOMDocument();
        $doc->loadHTML($html);
        if (!is_array($tags)) $tags = explode(',',$tags);
        foreach ($tags as $v) {
            //$this->removeElementsByTagName($v, $doc);
        }
        return trim(str_replace(
            array('<html>','</html>','<body>','</body>',
                '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">'),
            '',
            $doc->saveHtml()
        ));
    }
    
    private function removeElementsByTagName($tagName, $document) {
        $nodeList = $document->getElementsByTagName($tagName);
        for ($nodeIdx = $nodeList->length; --$nodeIdx >= 0; ) {
            $node = $nodeList->item($nodeIdx);
            $node->parentNode->removeChild($node);
        }
    }                        
}
?>