<?php
/**
 * Generates UUIDs v3, v4 and v5
 *
 * The following class generates VALID RFC 4211 COMPLIANT Universally
 * Unique IDentifiers (UUID) version 3, 4 and 5.
 *
 * Version 3 and 5 UUIDs are named based. They require a namespace
 * (another valid UUID) and a value (the name). Given the same
 * namespace and name, the output is always the same.
 *
 * Version 4 UUIDs are pseudo-random.
 *
 * UUIDs generated below validates using OSSP UUID Tool, and output for
 * named-based UUIDs are exactly the same. This is a pure PHP implementation.
 *
 * @see http://php.net/manual/en/function.uniqid.php#94959
 * @see https://stackoverflow.com/questions/10867405/generating-v5-uuid-what-is-name-and-namespace
 **/

class UUID {
    public static function v3($namespace, $name) {
        if(!self::is_valid($namespace)) return false;

        // Get hexadecimal components of namespace
        $nhex = str_replace(array('-','{','}'), '', $namespace);

        // Binary Value
        $nstr = '';

        // Convert Namespace UUID to bits
        for($i = 0; $i < strlen($nhex); $i+=2) {
            $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
        }

        // Calculate hash value
        $hash = md5($nstr . $name);
        return sprintf('%08s-%04s-%04x-%04x-%12s',
            // 32 bits for "time_low"
            substr($hash, 0, 8),

            // 16 bits for "time_mid"
            substr($hash, 8, 4),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 3
            (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

            // 48 bits for "node"
            substr($hash, 20, 12)
        );
    }

    public static function v4() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public static function v5($namespace, $name) {
        if(!self::is_valid($namespace)) return false;

        // Get hexadecimal components of namespace
        $nhex = str_replace(array('-','{','}'), '', $namespace);

        // Binary Value
        $nstr = '';

        // Convert Namespace UUID to bits
        for($i = 0; $i < strlen($nhex); $i+=2) {
            $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
        }

        // Calculate hash value
        $hash = sha1($nstr . $name);

        return sprintf('%08s-%04s-%04x-%04x-%12s',
            // 32 bits for "time_low"
            substr($hash, 0, 8),

            // 16 bits for "time_mid"
            substr($hash, 8, 4),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 5
            (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

            // 48 bits for "node"
            substr($hash, 20, 12)
        );
    }

    public static function is_valid($uuid) {
        return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
            '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
    }

    public static function bin_to_uuid($binguid) {
        $unpacked = unpack('Va/v2b/n2c/Nd', $binguid);
        return sprintf('%08X-%04X-%04X-%04X-%04X%08X', $unpacked['a'], $unpacked['b1'], $unpacked['b2'], $unpacked['c1'], $unpacked['c2'], $unpacked['d']);
    }

    public static function uuid_to_bin($binguid) {
        $uuid = str_replace("-", "", $binguid);
        return pack('H*', $uuid);
    }

    public static function generate() {
        $ip = getenv('HTTP_CLIENT_IP')?:getenv('HTTP_X_FORWARDED_FOR')?:getenv('HTTP_X_FORWARDED')?:getenv('HTTP_FORWARDED_FOR')?:getenv('HTTP_FORWARDED')?:getenv('REMOTE_ADDR');
        if ($ip  == '') {
            $ip = getHostByName(getHostName());
        }
        //Use ISO OID namespace as default. see https://tools.ietf.org/html/rfc4122#appendix-C
        return self::v5('6ba7b812-9dad-11d1-80b4-00c04fd430c8',microtime().'-'.$ip.'-'.rand(0,1000000));
    }


    /**
     * Shortens given UUID into a base64 encoded version
     *
     * Splits uuid into packs of 2 characters defining an hex
     * value, converted into the raw ascii equivalent, then
     * base64 the result.
     * To make it URL-friendly, + and / characters are replaced
     * with _ and - resp.
     *
     * @param $uuid the UUID to convert
     * @return @type string the base64 encoded equivalent
     */
    public static function shorten($uuid) {
        return strtr(
                 substr(
                   base64_encode(
                     implode('',
                       array_map('chr',
                         array_map('hexdec', str_split($uuid, 2))))), 0, -2),
                 '/+', '_-');
    }
}
