<?php
/**
 * Class Cryptography
 *
 * Cryptography class
 *
 * November 11, 2012
 *
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class CryptographyMethodInterface {
	function __construct($pass='',$salt=''){}
	function setPassword($password){}
	function setSalt($salt){}
	function encrypt(){}
}

class Cryptography {
	
	/**
	 * The cryptography handler to use
	 *
	 * @private
	 * @var CryptographyMethod $handler
	 */
	private $handler;
	
	/**
	 * The crypt options
	 *
	 * @private
	 * @var $options
	 */
	private $options;
	
	/**
	 * The crypt constructor
	 *
	 * @public
	 * @param string $pwd The passwordd to encrypt
	 * @param string $method The encryption type, currently supported : pbkdf2, scrypt
 	 * @param string $salt The password salt, will be autogenerated if none
	 * @return void Cryptography()
	 */
	public function __construct($pwd,$method='scrypt',$salt='') {
		$this->Cryptography($pwd,$method,$salt);
	}
	
	public function Cryptography($pwd,$method='scrypt',$salt=''){
		$this->setMethod($method);
		$this->setPassword($pwd);
		$this->setSalt($salt);
	}
	
	/**
	 * Sets the password to crypt
	 *
	 * @public
	 * @param string $pwd
	 * @return void Sets the password to encrypt
	 */
	public function setPassword($pass){
		$this->options['password'] = $pass;
	}
	
	/**
	 * Sets the salt to use for encrypting password
	 *
	 * @public
	 * @param string $salt
	 * @return void setSalt
	 */
	public function setSalt($salt){
		if ($salt == '') {
			
			//Note: mcrypt_create_iv was disabled because thought it is more perfectly random, it
			//is way too slow for use on a website.
			/*if (function_exists('mcrypt_create_iv')) {
				$salt = mcrypt_create_iv(255,MCRYPT_DEV_RANDOM);
			}
			else*/if (function_exists('openssl_random_pseudo_bytes')) {
				$salt = openssl_random_pseudo_bytes(255);
			}
			else {
				//fallback to mt_rand if php < 5.3 or no openssl available
				$characters = '0123456789';
				$characters .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/+!@#$%%^&*()[]{}:;"\'.,><?/';
				$charactersLength = strlen($characters)-1;
				$salt = '';
		
				//select some random characters
				for ($i = 0; $i < $length; $i++) {
					$salt .= $characters[mt_rand(0, $charactersLength)];
				} 				
			}
		}
		$this->options['salt'] = $salt;
	}
	
	/**
	 * Sets the encryption method to use
	 *
	 * @public
	 * @param string $method The encryption medhod : pbkdf2, scrypt
	 * @return void setMethod
	 */
	public function setMethod($method){
		$this->options['method'] = $method;
		if (!class_exists(ucfirst($method).'CryptographyMethod')) {
			trigger_error('Cryptography method named '.ucfirst($method).' should be defined in a class named '.ucfirst($method).'CryptographyMethod but that class was not found !');
		}else {
			$clsName = ucfirst($method).'CryptographyMethod';
			$this->handler = new $clsName;
		}
	}
	
	/**
	 * Encrypt the current password using the current
	 * salt and the current encryption method
	 *
	 * @public
	 * @return array Returns an array containing ['pass'] & ['salt']
	 */
	public function encrypt(){
		if (is_object($this->handler)){
			$this->handler->setPassword($this->options['password']);
			$this->handler->setSalt($this->options['salt']);
			return array(
				'password' => $this->handler->encrypt(),
				'salt'=> $this->options['salt']
			);
		}
		return false;
	}	
}
?>