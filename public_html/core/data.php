<?php
/**
 * Public Data Storage class
 *
 * Defines data class (DataData, DataModel, DataStore, DataController)
 *
 * For use by plugins that manipulate data to display in a public way.
 * @TODO add access control to this class.
 *
 * Oct 30, 2012
 *
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('ressource')->get('core/db/model');


class DataModel extends Model {
	public function __construct() {
		global $service;
		$this->type = 'data';
		$this->initVars(array(
			'data_id' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'data_objid' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'data_type' => array(DATATYPE_STRING),
			'data_parent' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'data_category' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'data_position' => array(DATATYPE_INT),
			'data_title' => array(DATATYPE_STRING),
			'data_data' => array(DATATYPE_ARRAY),
			'data_language' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'data_site' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'data_created' => array(DATATYPE_INT),
			'data_modified'	=> array(DATATYPE_INT),
			'data_modifiedby' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT)
		));
		$this->setOption('usecache',false);
	}
}

class DataData extends Data{
	function __construct(){
		parent::__construct('data');
	}
}

class DataStore extends Store{
	public function __construct() {
		parent::__construct('data');
	}
}
?>
