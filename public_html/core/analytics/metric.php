<?php
/**
 * Metric classes
 *
 * Defines metric classes (MetricData, MetricModel, MetricStore and Metric)
 *
 * Sept 15, 2015
 *
 * @author 		Translucide
 * @copyright 	copyright (c) 2015 Translucide
 * @license		See docs/license.txt
 */

global $service;
$service->get('ressource')->get('core/user/group');

/**
 * Model class for Metric data (MetricData) and Metric (Widget)
 *
 * Usage: $obj = new MetricModel();
 *
 * @version 0.1
 * @since 0.1
 * @class MetricModel
 * @package	sys
 * @subpackage display
 * @see MetricModel
 */
class MetricModel extends Model {

	/**
	 * PHP 5 Constructor
	 * @return void
	 */
	public function __construct() {
		$this->MetricModel();
	}

	public function MetricModel(){
		global $service;
		$this->type = 'metric';
		$this->initVar('metric_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('metric_idstr',DATATYPE_STRING);
		$this->initVar('metric_section',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('metric_content',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('metric_type',DATATYPE_STRING,array('length'=> 40));
		$this->initVar('metric_name',DATATYPE_STRING,array('length'=> 40));
		$this->initVar('metric_value',DATATYPE_STRING);
		$this->initVar('metric_metric1',DATATYPE_INT);
		$this->initVar('metric_metric2',DATATYPE_INT);
		$this->initVar('metric_metric3',DATATYPE_INT);
		$this->initVar('metric_metric4',DATATYPE_INT);
		$this->initVar('metric_metric5',DATATYPE_INT);
		$this->initVar('metric_time',DATATYPE_INT);
		$this->initVar('metric_start',DATATYPE_INT);
		$this->initVar('metric_duration',DATATYPE_INT);
		$this->initVar('metric_language',DATATYPE_INT);
		$this->initVar('metric_site',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('metric_created',DATATYPE_INT);
		$this->initVar('metric_modified',DATATYPE_INT);
		$this->initVar('metric_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
	}
}

class MetricData extends Data{
	function __construct(){
        parent::__construct('metric');
	}
}

class MetricStore extends Store{
	public function __construct() {
		parent::__construct('metric');
	}
}

class Metric {
    public function __construct(){
        $this->Metric();
    }
    public function Metric(){
    }
}
?>
