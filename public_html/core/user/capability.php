<?php
/**
 * Capability manager
 *
 * Manages users capabilities
 *
 * May 9, 2018

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class CapabilityModel extends Model {
	function __construct(){
		$this->initVar('capability_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('capability_nameid',DATATYPE_STRING,array('length'=>50));
		$this->initVar('capability_group',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('capability_title',DATATYPE_STRING,array('length'=>50));
		$this->initVar('capability_description',DATATYPE_STRING,array('length'=>100));
		$this->initVar('capability_site',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('capability_deleted',DATATYPE_INT);
		$this->initVar('capability_deletedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('capability_created',DATATYPE_INT);
		$this->initVar('capability_createdby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('capability_modified',DATATYPE_INT);
		$this->initVar('capability_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->type = 'capability';
	}
}

class CapabilityData extends Data {
	function __construct(){
        parent::__construct('capability');
	}
}

class CapabilityStore extends Store {
	function __construct(){
		parent::__construct('capability');
	}
}

class Capability extends BaseClass {
	/**
	 * Holds current user Capabilities
	 *
	 * @access private
	 */
	private $data;

	/**
	 * Permission constructor
	 *
	 * @access public
	 *
	 * @return void Permission()
	 */
	public function __construct(){
		$this->load();
	}

	/**
	 * Loads the current user into $data
	 *
	 * @access public
	 * @return bool True or false depending if current user was found.
	 */
	public function load(){
		global $service;
		$store = new CapabilityStore();
        $crit = new CriteriaCompo();
        $crit->add(new Criteria('capability_group',$store->inClause($service->get('User')->getGroups()),'IN'));
        $crit->setSelection('capability_nameid');
        $data = $store->get($crit);
        foreach($data as $k => $v){
            $data[$k] = $v['capability_nameid'];
        }
		$this->data = $data;
		return ($data)?true:false;
	}

	/**
	 * Checks if user has a capability
	 *
	 * @access public
	 * @param string $capabilityNameId
	 * @return bool True or false
	 */
	public function can($capabilityNameId){
		return (in_array($capabilityNameId,$this->data) !== false)?true:false;
	}
}
