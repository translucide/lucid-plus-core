<?php
/**
 * Permission manager
 *
 * Manages users access rights
 *
 * October 28, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class PermissionModel extends Model {
	function __construct(){
		$this->initVar('permission_id',DATATYPE_INT);
		$this->initVar('permission_groupid',DATATYPE_INT);
		$this->initVar('permission_controller',DATATYPE_STRING);
		$this->initVar('permission_title',DATATYPE_STRING);
		$this->initVar('permission_description',DATATYPE_STRING);
		$this->initVar('permission_rights',DATATYPE_INT);
		$this->initVar('permission_site',DATATYPE_INT);
		$this->initVar('permission_created',DATATYPE_INT);
		$this->initVar('permission_modified',DATATYPE_INT);
		$this->initVar('permission_modifiedby',DATATYPE_INT);
		$this->type = 'user';
	}
}

class PermissionData extends Data {
	function __construct(){
        parent::__construct('permission');
	}
}

class PermissionStore extends Store {
	function __construct(){
		parent::__construct('permission');
	}
}

class Permission extends BaseClass {
	/**
	 * Holds current user permissions
	 *
	 * @access private
	 */
	private $data;

	/**
	 * Permission constructor
	 *
	 * @access public
	 *
	 * @return void Permission()
	 */
	public function __construct(){
		$this->load();
	}

	/**
	 * Loads the current user into $data
	 *
	 * @access public
	 * @return bool True or false depending if current user was found.
	 */
	public function load(){
		global $service;
		$groups = $service->get('User')->getGroups();
		$store = new PermissionStore();
		$data = $store->get(new Criteria('permission_groupid',implode(',',$groups)),'ÌN');
		if ($data) {
			foreach ($data as $k => $v){
				if (isset($this->data[$v->getVar('permission_controller')])){
					$value = $this->data[$v->getVar('permission_controller')];
				}else {
					$value = 0;
				}
				$this->data[$v->getVar('permission_controller')] = $value | $v->getVar('permission_rights');
			}
		}
		return ($data)?true:false;
	}

	/**
	 * Checks if user has admin rights on the given controller.
	 *
	 * @access public
	 * @param string $controller The controller to chekc access rights for
	 * @return bool True or false
	 */
	public function isAdmin($controller){
		return $this->checkRight($controller,1);
	}

	/**
	 * Checks if user can access the given controller
	 *
	 * @access public
	 * @param string $controller
	 * @return bool hasAccess()
	 */
	public function hasAccess($controller){
		return $this->checkRight($controller,2);
	}

	/**
	 * Checks if user can post content for the given controller
	 *
	 * @access public
	 * @param string $controller
	 * @return bool isEditor()
	 */
	public function isEditor($controller){
		return $this->checkRight($controller,4);
	}

	/**
	 * Checks if user can moderate content for the given controller
	 *
	 * @access public
	 * @param string $controller
	 * @return bool isModerator()
	 */
	public function isModerator($controller){
		return $this->checkRight($controller,8);
	}

	/**
	 * Checks if user has the required rights
	 *
	 * @access private
	 * @param string $controller
	 * @param int $right
	 * @return bool checkRight()
	 */
	private function checkRight($controller,$right){
		if (isset($this->data[$controller]) && is_array($this->data)) {
			if ($this->data[$controller] & $right) {
				return true;
			}
		}
		return false;
	}

}
