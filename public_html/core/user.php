<?php
/**
 * User manager
 *
 * Manages users
 *
 * October 28, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class UserModel extends Model {
	function __construct(){
		$this->initVar('user_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('user_name',DATATYPE_STRING);
		$this->initVar('user_email',DATATYPE_STRING);
		$this->initVar('user_encryptionmethod',DATATYPE_STRING,array('length' => 10));
		$this->initVar('user_pass',DATATYPE_STRING);
		$this->initVar('user_salt',DATATYPE_STRING);
		$this->initVar('user_groups',(DB_UUIDS)?DATATYPE_UUIDARRAY:DATATYPE_INTARRAY);
		$this->initVar('user_lastlogin',DATATYPE_INT);
		$this->initVar('user_timezone',DATATYPE_STRING,array('length' => 100));
		$this->initVar('user_failedattempts',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('user_sid',DATATYPE_STRING);
		$this->initVar('user_ip',DATATYPE_IP);
		$this->initVar('user_allowlogin',DATATYPE_INT);
		$this->initVar('user_actkey',DATATYPE_STRING);
		$this->initVar('user_site',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('user_tenant',(DB_UUIDS)?DATATYPE_UUIDARRAY:DATATYPE_INTARRAY);
		$this->initVar('user_language',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('user_created',DATATYPE_INT);
		$this->initVar('user_modified',DATATYPE_INT);
		$this->initVar('user_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('user_deleted',DATATYPE_INT);
		$this->initVar('user_deletedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->type = 'user';
	}
}

class UserData extends Data {
	function __construct(){
        parent::__construct('user');
	}
}

class UserStore extends Store {
	function __construct(){
		parent::__construct('user');
	}
}

class User extends BaseClass {
	/**
	 * Holds user session object
	 *
	 * @access private
	 */
	private $data;

	/**
	 * User constructor
	 *
	 * @access public
	 *
	 * @return void User()
	 */
	public function __construct(){
		$this->data['loaded'] = false;
		$this->load();
	}

    /**
     * Reloads current user data in case it changed.
     *
     * @access public
     **/
    public function reload(){
        $this->data['loaded'] = false;
        $this->load();
    }

	/**
	 * Loads the current user into $data
	 *
	 * @access public
	 * @return bool True or false depending if current user was found.
	 */
	public function load(){
		//Prevent repetitive user loading queries for anonymous users.
		if ($this->data['loaded']) {
			return (isset($data['user']) && is_object($data['user']))?true:false;
		}

		//Load user data.
		$store = new UserStore();
        $store->setOption('ignoretenant',true);
		$user = false;
		if (!session_id()) {
			global $service;
			$service->get('Session');
		}
		if (session_id()) {
            $crit = new Criteriacompo();
            $crit->add(new Criteria('user_sid',session_id()));
            $crit->add(new Criteria('user_deleted',0));
			$user = $store->get($crit);
			$this->data['loaded'] = true;
			if (!$user || !is_array($user) || count($user) != 1) {
				$this->data['user'] = false;
				return false;
			}else {
				$this->data['user'] = $user[0];
				return true;
			}
		}
		return false;
	}

	/**
	 * Determines if a user is currently logged in
	 *
	 * @access public
	 * @return bool True if user is logged in
	 */
	public function isLogged(){
		global $service;
		if (!is_object($this->data['user'])) {
			$this->load();
		}
		if ($service->get('Setting')->get('sessionlifetime') > 0) $expires = $service->get('Setting')->get('sessionlifetime')*60;
		else $expires = 25*60; //25 minutes sessions by default
		if (is_object($this->data['user']))  {
			return true;
		}
		return false;
	}

	/**
	 * Logs in a user
	 *
	 * @access public
	 * @param string $uname The user email address
	 * @param string $pass The user password, unencrypted
	 * @return bool True if successful
	 */
	public function login($uname,$pass,$tenant=0){
		global $service;
		$store = new UserStore();
        $store->setOption('ignoretenant',true);

        //find default tenant when none are specified.
        if (!$tenant) {
            $service->get('Ressource')->get('core/model/tenant');
            $tenantStore = new TenantStore();
            $tenant = $tenantStore->getDefaultTenantId();
        }
        
        $userCrit = new CriteriaCompo(new Criteria('user_tenant','%,'.$tenant.',%','LIKE'));
        $userCrit->add(new Criteria('user_deleted',0));
        $userCrit->add(new Criteria('user_allowlogin',1));
        $userCrit->add(new Criteria('user_email',trim($uname)));
		$user = $store->get($userCrit);
		if ($user && is_array($user) && count($user) == 1) {
			$user = $user[0];
			$crypted = $this->encryptPassword($pass,$user->getVar('user_salt'),$user->getVar('user_encryptionmethod'));
			if ($crypted['password'] == $user->getVar('user_pass')) {
		        //Update session ID & last login time
                $sid = session_id();
                $time = time();
				$user->setVar('user_lastlogin',$time);
				$user->setVar('user_sid',$sid);
				$user->setVar('user_ip',$_SERVER['REMOTE_ADDR']);
				$store->update([
                    'user_lastlogin' => $time,
                    'user_sid' => $sid,
                    'user_ip' => $_SERVER['REMOTE_ADDR']
                ],new Criteria('user_id',$user->getVar('user_id')));
				$this->data['user'] = $user;
                return true;
			}else{
                //Update failed attempts counter
                $store->update([
                    'user_failedattempts' => $user->getVar('user_failedattempts')+1,
                    'user_ip' => $_SERVER['REMOTE_ADDR']
                ],new Criteria('user_id',$user->getVar('user_id')));
                $service->get('Log')->add('Failed login attempt: user_email=\''.$uname.'\', user_tenant=\''.$tenant.'\'', E_ERROR, LOG_FILE_SYSTEM, __FILE__, __LINE__);
            }
		}
		return false;
	}

	/**
	 * Function encryptPassword()
	 *
	 * @access public
	 * @param $pass
	 * @return array Encrypted password & salt
	 */
	function encryptPassword($pass,$salt='',$method='scrypt'){
		global $service;
		$service->get('Ressource')->get('core/security/cryptography');
		if (in_array($method,array('scrypt','bcrypt')) === false) {
			$method = 'scrypt';
		}
		$crypt = new Cryptography($pass,$method,$salt);
		$crypted = $crypt->encrypt();
		return $crypted;
	}

	/**
	 * Checks if another user exists and has
	 * the same email as the current user
	 *
	 * @access public
	 *
	 * @param string $uname User name or email
	 * @return bool exists()
	 */
	public function exists($email){
		global $service;
		$store = new UserStore();
		$userCrit = new CriteriaCompo(new Criteria('user_email',$email));
        $userCrit->add(new Criteria('user_deleted',0));
		if ($this->isLogged()) $userCrit->add(new Criteria('user_id',$this->data['user']->getVar('user_id'),'<>'),'AND');
		$user = $store->get($userCrit);
		if ($user != false && is_array($user) && count($user) > 0) {
			return true;
		}
		return false;
	}

	/**
	 * Logs out the current user
	 *
	 * @access public
	 * @return bool True if successful
	 */
	public function logout(){
		$store = new UserStore();
		$user = $this->data['user'];
		$res = false;
		if ($user != false && is_object($user)) {
			$user->setVar('user_sid','');
			$res = $store->save($user);
			unset($this->data);
		}
		session_write_close();
		session_destroy();
		session_start();
		session_regenerate_id();
		return $res;
	}

	/**
	 * @access public
	 * @param User User object, with password unencrypted.
	 * @return bool True if successfull
	 */
	public function register($userobj) {
		$store = new UserStore();
		$user = $store->get(new Criteria('user_email',$userobj->getVar('user_email')));
		if ($user != false || !is_array($user) || count($user) > 0) {
			return false;
		}
        else {
            $crypted = $this->encryptPassword($userobj->getVar('user_pass'),'',USER_CRYPTOALGORITHM);
			$userobj->setVar('user_encryptionmethod',USER_CRYPTOALGORITHM);
			$userobj->setVar('user_pass',$crypt['password']);
			$userobj->setVar('user_salt',$crypt['salt']);
            $userobj->setVar('user_lastlogin','0');
            $userobj->setVar('user_sid','0');
            $userobj->setVar('user_ip','');
			$res = $store->save($userobj);
            if ($res) {
                $userobj->setVar('user_id',$store->getLastId());
                return $userobj;
            }
            else return false;
		}
	}

	/**
	 * Gets the current user var
	 *
	 * @access public
	 * @param string $var The user var to get
	 * @return mixed The var value
	 */
	public function get($var=''){
		if ($this->isLogged()) {
			if ($var == '') return $this->data['user'];
			else return $this->data['user']->getVar('user_'.str_replace('user_','',strtolower($var)));
		}
		return false;
	}
	/**
	 * Gets the current user var
	 *
	 * @access public
	 * @param string $var The user var to get
	 * @return mixed The var value
	 */
	public function getVar($var=''){
		return $this->get($var);
	}

	/**
	 * Gets the current user ID or 0 if anonymous
	 *
	 * @access public
	 * @return int The crurent user ID
	 */
	public function getUid() {
		if (isset($this->data['user']) && is_object($this->data['user'])) {
			return $this->data['user']->getVar('user_id');
		}else {
			return 0;
		}
	}

	/**
	 * Gets user profile
	 *
	 * @access public
	 * @return Profile The user profile object.
	 */
	public function getProfile(){
		if (!isset($this->data['profile'])) {
			global $service;
			$service->get('profile');
			$profile = new Profile();
			$this->data['profile'] = $profile->get($this->getUid());
		}
		return $this->data['profile'];
	}

	/**
	 *	Gets user groups
	 *
	 *	@access public
	 *	@return array The user group IDs the user is apart of.
	 */
	public function getGroups(){
		if ($this->isLogged()) {
			return $this->data['user']->getVar('user_groups');
		}
		else {
			global $service;
			return array($service->get('Group')->id('anonymous')); //Anonymous user
		}
	}

	/**
	 * Checks if current user is an administrator
	 *
	 * Use the controller parameter to determine if user is an
	 * admin for the given controller. When no controller is specified,
	 * the method will only check if user is a part of admins group.
	 * By beeing a member of admin user group, this function will always
	 * return true.
	 *
	 * @param string $controller Controller name for which to check if user has admin rights
	 * @return bool True or false depending if user is an admin for the given controller.
	 */
	public function isAdmin($controller=''){
		global $service;
		if (in_array($service->get('group')->id('admin'),$this->getGroups())) return true;
		else {
			global $service;
			$service->get('Ressource')->get('core/user/permission');
			$permission = new Permission();
			return $permission->isAdmin($controller);
		}
		return false;
	}

	/**
	 * Checks if current user is anonymous
	 *
	 * @access public
	 * @return bool True on anonymous user
	 */
	public function isAnonymous(){
		if (!$this->isLogged()) {
			return true;
		}
		return false;
	}
}
