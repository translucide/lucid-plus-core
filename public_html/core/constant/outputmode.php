<?php
//Output modes constants
define('OUTPUT_MODE_JS','js');
define('OUTPUT_MODE_XML','xml');
define('OUTPUT_MODE_CSS','css');
define('OUTPUT_MODE_JSON','json');
define('OUTPUT_MODE_TEXT','txt');
define('OUTPUT_MODE_HTML','html');

//Output destination constants
define('OUTPUT_TO_CONSOLE','console');
define('OUTPUT_TO_SCREEN','screen');
define('OUTPUT_TO_FILE','file');
define('OUTPUT_TO_SYSLOG','syslog');
define('OUTPUT_TO_MAIL','mail');
?>
