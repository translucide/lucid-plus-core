<?php
//All languages magic valuee
define('LANG_AUTO','0');
define('LANGTITLE_AUTO','');

//All supported languages
define('LANG_EN','1');
define('LANG_FR','2');
define('LANGTITLE_EN','English');
define('LANGTITLE_FR','Français');

define('LANG_AA','100'); define('LANGTITLE_AA','Afar');
define('LANG_AB','101'); define('LANGTITLE_AB','Abkhazian');
define('LANG_AE','102'); define('LANGTITLE_AE','Avestan');
define('LANG_AF','103'); define('LANGTITLE_AF','Afrikaans');
define('LANG_AK','104'); define('LANGTITLE_AK','Akan');
define('LANG_AM','105'); define('LANGTITLE_AM','Amharic');
define('LANG_AN','106'); define('LANGTITLE_AN','Aragonese');
define('LANG_AR','107'); define('LANGTITLE_AR','Arabic');
define('LANG_AS','108'); define('LANGTITLE_AS','Assamese');
define('LANG_AV','109'); define('LANGTITLE_AV','Avaric');
define('LANG_AY','110'); define('LANGTITLE_AY','Aymara');
define('LANG_AZ','111'); define('LANGTITLE_AZ','Azerbaijani');
define('LANG_BA','112'); define('LANGTITLE_BA','Bashkir');
define('LANG_BE','113'); define('LANGTITLE_BE','Belarusian');
define('LANG_BG','114'); define('LANGTITLE_BG','Bulgarian');
define('LANG_BH','115'); define('LANGTITLE_BH','Bihari languages');
define('LANG_BI','116'); define('LANGTITLE_BI','Bislama');
define('LANG_BM','117'); define('LANGTITLE_BM','Bambara');
define('LANG_BN','118'); define('LANGTITLE_BN','Bengali');
define('LANG_BO','119'); define('LANGTITLE_BO','Tibetan');
define('LANG_BR','120'); define('LANGTITLE_BR','Breton');
define('LANG_BS','121'); define('LANGTITLE_BS','Bosnian');
define('LANG_CA','122'); define('LANGTITLE_CA','Catalan; Valencian');
define('LANG_CE','123'); define('LANGTITLE_CE','Chechen');
define('LANG_CH','124'); define('LANGTITLE_CH','Chamorro');
define('LANG_CO','125'); define('LANGTITLE_CO','Corsican');
define('LANG_CR','126'); define('LANGTITLE_CR','Cree');
define('LANG_CS','127'); define('LANGTITLE_CS','Czech');
define('LANG_CU','128'); define('LANGTITLE_CU','Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic');
define('LANG_CV','129'); define('LANGTITLE_CV','Chuvash');
define('LANG_CY','130'); define('LANGTITLE_CY','Welsh');
define('LANG_DA','131'); define('LANGTITLE_DA','Danish');
define('LANG_DE','132'); define('LANGTITLE_DE','German');
define('LANG_DV','133'); define('LANGTITLE_DV','Divehi; Dhivehi; Maldivian');
define('LANG_DZ','134'); define('LANGTITLE_DZ','Dzongkha');
define('LANG_EE','135'); define('LANGTITLE_EE','Ewe');
define('LANG_EL','136'); define('LANGTITLE_EL','Greek, Modern (1453-)');
//define('LANG_EN','137'); define('LANGTITLE_EN','English');
define('LANG_EO','138'); define('LANGTITLE_EO','Esperanto');
define('LANG_ES','139'); define('LANGTITLE_ES','Spanish; Castilian');
define('LANG_ET','140'); define('LANGTITLE_ET','Estonian');
define('LANG_EU','141'); define('LANGTITLE_EU','Basque');
define('LANG_FA','142'); define('LANGTITLE_FA','Persian');
define('LANG_FF','143'); define('LANGTITLE_FF','Fulah');
define('LANG_FI','144'); define('LANGTITLE_FI','Finnish');
define('LANG_FJ','145'); define('LANGTITLE_FJ','Fijian');
define('LANG_FO','146'); define('LANGTITLE_FO','Faroese');
//define('LANG_FR','147'); define('LANGTITLE_FR','French');
define('LANG_FY','148'); define('LANGTITLE_FY','Western Frisian');
define('LANG_GA','149'); define('LANGTITLE_GA','Irish');
define('LANG_GD','150'); define('LANGTITLE_GD','Gaelic; Scottish Gaelic');
define('LANG_GL','151'); define('LANGTITLE_GL','Galician');
define('LANG_GN','152'); define('LANGTITLE_GN','Guarani');
define('LANG_GU','153'); define('LANGTITLE_GU','Gujarati');
define('LANG_GV','154'); define('LANGTITLE_GV','Manx');
define('LANG_HA','155'); define('LANGTITLE_HA','Hausa');
define('LANG_HE','156'); define('LANGTITLE_HE','Hebrew');
define('LANG_HI','157'); define('LANGTITLE_HI','Hindi');
define('LANG_HO','158'); define('LANGTITLE_HO','Hiri Motu');
define('LANG_HR','159'); define('LANGTITLE_HR','Croatian');
define('LANG_HT','160'); define('LANGTITLE_HT','Haitian; Haitian Creole');
define('LANG_HU','161'); define('LANGTITLE_HU','Hungarian');
define('LANG_HY','162'); define('LANGTITLE_HY','Armenian');
define('LANG_HZ','163'); define('LANGTITLE_HZ','Herero');
define('LANG_IA','164'); define('LANGTITLE_IA','Interlingua (International Auxiliary Language Association)');
define('LANG_ID','165'); define('LANGTITLE_ID','Indonesian');
define('LANG_IE','166'); define('LANGTITLE_IE','Interlingue; Occidental');
define('LANG_IG','167'); define('LANGTITLE_IG','Igbo');
define('LANG_II','168'); define('LANGTITLE_II','Sichuan Yi; Nuosu');
define('LANG_IK','169'); define('LANGTITLE_IK','Inupiaq');
define('LANG_IO','170'); define('LANGTITLE_IO','Ido');
define('LANG_IS','171'); define('LANGTITLE_IS','Icelandic');
define('LANG_IT','172'); define('LANGTITLE_IT','Italian');
define('LANG_IU','173'); define('LANGTITLE_IU','Inuktitut');
define('LANG_JA','174'); define('LANGTITLE_JA','Japanese');
define('LANG_JV','175'); define('LANGTITLE_JV','Javanese');
define('LANG_KA','176'); define('LANGTITLE_KA','Georgian');
define('LANG_KG','177'); define('LANGTITLE_KG','Kongo');
define('LANG_KI','178'); define('LANGTITLE_KI','Kikuyu; Gikuyu');
define('LANG_KJ','179'); define('LANGTITLE_KJ','Kuanyama; Kwanyama');
define('LANG_KK','180'); define('LANGTITLE_KK','Kazakh');
define('LANG_KL','181'); define('LANGTITLE_KL','Kalaallisut; Greenlandic');
define('LANG_KM','182'); define('LANGTITLE_KM','Central Khmer');
define('LANG_KN','183'); define('LANGTITLE_KN','Kannada');
define('LANG_KO','184'); define('LANGTITLE_KO','Korean');
define('LANG_KR','185'); define('LANGTITLE_KR','Kanuri');
define('LANG_KS','186'); define('LANGTITLE_KS','Kashmiri');
define('LANG_KU','187'); define('LANGTITLE_KU','Kurdish');
define('LANG_KV','188'); define('LANGTITLE_KV','Komi');
define('LANG_KW','189'); define('LANGTITLE_KW','Cornish');
define('LANG_KY','190'); define('LANGTITLE_KY','Kirghiz; Kyrgyz');
define('LANG_LA','191'); define('LANGTITLE_LA','Latin');
define('LANG_LB','192'); define('LANGTITLE_LB','Luxembourgish; Letzeburgesch');
define('LANG_LG','193'); define('LANGTITLE_LG','Ganda');
define('LANG_LI','194'); define('LANGTITLE_LI','Limburgan; Limburger; Limburgish');
define('LANG_LN','195'); define('LANGTITLE_LN','Lingala');
define('LANG_LO','196'); define('LANGTITLE_LO','Lao');
define('LANG_LT','197'); define('LANGTITLE_LT','Lithuanian');
define('LANG_LU','198'); define('LANGTITLE_LU','Luba-Katanga');
define('LANG_LV','199'); define('LANGTITLE_LV','Latvian');
define('LANG_MG','200'); define('LANGTITLE_MG','Malagasy');
define('LANG_MH','201'); define('LANGTITLE_MH','Marshallese');
define('LANG_MI','202'); define('LANGTITLE_MI','Maori');
define('LANG_MK','203'); define('LANGTITLE_MK','Macedonian');
define('LANG_ML','204'); define('LANGTITLE_ML','Malayalam');
define('LANG_MN','205'); define('LANGTITLE_MN','Mongolian');
define('LANG_MR','206'); define('LANGTITLE_MR','Marathi');
define('LANG_MS','207'); define('LANGTITLE_MS','Malay');
define('LANG_MT','208'); define('LANGTITLE_MT','Maltese');
define('LANG_MY','209'); define('LANGTITLE_MY','Burmese');
define('LANG_NA','210'); define('LANGTITLE_NA','Nauru');
define('LANG_NB','211'); define('LANGTITLE_NB','BokmÃ¥l, Norwegian; Norwegian BokmÃ¥l');
define('LANG_ND','212'); define('LANGTITLE_ND','Ndebele, North; North Ndebele');
define('LANG_NE','213'); define('LANGTITLE_NE','Nepali');
define('LANG_NG','214'); define('LANGTITLE_NG','Ndonga');
define('LANG_NL','215'); define('LANGTITLE_NL','Dutch; Flemish');
define('LANG_NN','216'); define('LANGTITLE_NN','Norwegian Nynorsk; Nynorsk, Norwegian');
define('LANG_NO','217'); define('LANGTITLE_NO','Norwegian');
define('LANG_NR','218'); define('LANGTITLE_NR','Ndebele, South; South Ndebele');
define('LANG_NV','219'); define('LANGTITLE_NV','Navajo; Navaho');
define('LANG_NY','220'); define('LANGTITLE_NY','Chichewa; Chewa; Nyanja');
define('LANG_OC','221'); define('LANGTITLE_OC','Occitan (post 1500); ProvenÃ§al');
define('LANG_OJ','222'); define('LANGTITLE_OJ','Ojibwa');
define('LANG_OM','223'); define('LANGTITLE_OM','Oromo');
define('LANG_OR','224'); define('LANGTITLE_OR','Oriya');
define('LANG_OS','225'); define('LANGTITLE_OS','Ossetian; Ossetic');
define('LANG_PA','226'); define('LANGTITLE_PA','Panjabi; Punjabi');
define('LANG_PI','227'); define('LANGTITLE_PI','Pali');
define('LANG_PL','228'); define('LANGTITLE_PL','Polish');
define('LANG_PS','229'); define('LANGTITLE_PS','Pushto; Pashto');
define('LANG_PT','230'); define('LANGTITLE_PT','Portuguese');
define('LANG_QU','231'); define('LANGTITLE_QU','Quechua');
define('LANG_RM','232'); define('LANGTITLE_RM','Romansh');
define('LANG_RN','233'); define('LANGTITLE_RN','Rundi');
define('LANG_RO','234'); define('LANGTITLE_RO','Romanian; Moldavian; Moldovan');
define('LANG_RU','235'); define('LANGTITLE_RU','Russian');
define('LANG_RW','236'); define('LANGTITLE_RW','Kinyarwanda');
define('LANG_SA','237'); define('LANGTITLE_SA','Sanskrit');
define('LANG_SC','238'); define('LANGTITLE_SC','Sardinian');
define('LANG_SD','239'); define('LANGTITLE_SD','Sindhi');
define('LANG_SE','240'); define('LANGTITLE_SE','Northern Sami');
define('LANG_SG','241'); define('LANGTITLE_SG','Sango');
define('LANG_SI','242'); define('LANGTITLE_SI','Sinhala; Sinhalese');
define('LANG_SK','243'); define('LANGTITLE_SK','Slovak');
define('LANG_SL','244'); define('LANGTITLE_SL','Slovenian');
define('LANG_SM','245'); define('LANGTITLE_SM','Samoan');
define('LANG_SN','246'); define('LANGTITLE_SN','Shona');
define('LANG_SO','247'); define('LANGTITLE_SO','Somali');
define('LANG_SQ','248'); define('LANGTITLE_SQ','Albanian');
define('LANG_SR','249'); define('LANGTITLE_SR','Serbian');
define('LANG_SS','250'); define('LANGTITLE_SS','Swati');
define('LANG_ST','251'); define('LANGTITLE_ST','Sotho, Southern');
define('LANG_SU','252'); define('LANGTITLE_SU','Sundanese');
define('LANG_SV','253'); define('LANGTITLE_SV','Swedish');
define('LANG_SW','254'); define('LANGTITLE_SW','Swahili');
define('LANG_TA','255'); define('LANGTITLE_TA','Tamil');
define('LANG_TE','256'); define('LANGTITLE_TE','Telugu');
define('LANG_TG','257'); define('LANGTITLE_TG','Tajik');
define('LANG_TH','258'); define('LANGTITLE_TH','Thai');
define('LANG_TI','259'); define('LANGTITLE_TI','Tigrinya');
define('LANG_TK','260'); define('LANGTITLE_TK','Turkmen');
define('LANG_TL','261'); define('LANGTITLE_TL','Tagalog');
define('LANG_TN','262'); define('LANGTITLE_TN','Tswana');
define('LANG_TO','263'); define('LANGTITLE_TO','Tonga (Tonga Islands)');
define('LANG_TR','264'); define('LANGTITLE_TR','Turkish');
define('LANG_TS','265'); define('LANGTITLE_TS','Tsonga');
define('LANG_TT','266'); define('LANGTITLE_TT','Tatar');
define('LANG_TW','267'); define('LANGTITLE_TW','Twi');
define('LANG_TY','268'); define('LANGTITLE_TY','Tahitian');
define('LANG_UG','269'); define('LANGTITLE_UG','Uighur; Uyghur');
define('LANG_UK','270'); define('LANGTITLE_UK','Ukrainian');
define('LANG_UR','271'); define('LANGTITLE_UR','Urdu');
define('LANG_UZ','272'); define('LANGTITLE_UZ','Uzbek');
define('LANG_VE','273'); define('LANGTITLE_VE','Venda');
define('LANG_VI','274'); define('LANGTITLE_VI','Vietnamese');
define('LANG_VO','275'); define('LANGTITLE_VO','VolapÃ¼k');
define('LANG_WA','276'); define('LANGTITLE_WA','Walloon');
define('LANG_WO','277'); define('LANGTITLE_WO','Wolof');
define('LANG_XH','278'); define('LANGTITLE_XH','Xhosa');
define('LANG_YI','279'); define('LANGTITLE_YI','Yiddish');
define('LANG_YO','280'); define('LANGTITLE_YO','Yoruba');
define('LANG_ZA','281'); define('LANGTITLE_ZA','Zhuang; Chuang');
define('LANG_ZH','282'); define('LANGTITLE_ZH','Chinese');
define('LANG_ZU','283'); define('LANGTITLE_ZU','Zulu');
?>
