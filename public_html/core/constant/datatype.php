<?php
//Basic data types
define('DATATYPE_INT','integer');
define('DATATYPE_STRING','string');
define('DATATYPE_ARRAY','array');
define('DATATYPE_FLOAT','float');
define('DATATYPE_BOOL','boolean');
define('DATATYPE_UUID','uuid');
define('DATATYPE_TIMESTAMP','timestamp');

//Mixed data types
define('DATATYPE_INTARRAY','intarray');
define('DATATYPE_STRINGARRAY','stringarray');
define('DATATYPE_BOOLARRAY','booleanarray');
define('DATATYPE_FLOATARRAY','floatarray');
define('DATATYPE_UUIDARRAY','uuidarray');

//High level datatypes
define('DATATYPE_EMAIL','email');
define('DATATYPE_COLOR','color');
define('DATATYPE_IMAGE','image');
define('DATATYPE_IP','ip');
define('DATATYPE_DOMAINNAME','domainname');
?>
