<?php
/**
 * Base class to handle package installation and removal
 *
 * Manages packages
 *
 * November 11, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class Package extends BaseClass {

	public function Package(){

	}

	public function install($packagename){
		//1. Check if already installed and run update if it is

		//2. Add ressources to the ressource table

		//3. Add events to the events table

		//4. Add package to the packages table

	}

	public function remove(){

	}

	public function update(){

	}

}
?>
