<?php
/**
 * Routing manager
 *
 * Manages routing
 *
 * Jan 19, 2012
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/content');

interface RouteInterface{
}

class RouteModel extends Model {
	public function __construct(){
		$this->type = 'route';
		$this->initVars(array(
			'route_id' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'route_component' => array(DATATYPE_STRING,array('length'=>60)),
			'route_url' => array(DATATYPE_STRING,array('length'=>255)),
			'route_controller' => array(DATATYPE_STRING),
			'route_language'	=> array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'route_site' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'route_created' => array(DATATYPE_INT),
			'route_modified'	=> array(DATATYPE_INT),
			'route_modifiedby' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT)
		));
		$this->setOption('usecache',true);
	}
}

class RouteData extends Data{
	public function __construct(){
        parent::__construct('route');
	}
}

class RouteStore extends Store{
	public function __construct(){
		parent::__construct('route');
	}
}

class Route extends BaseClass {

	/**
	 * Hold a reference to RouteData objects that match the requested route
	 *
	 * @private
	 *
	 * @var array Array of RouteData objects that matched the current requested route
	 */
	private $data;

	/**
	 * Route PHP5 constructor
	 *
	 * @public
	 * @return void Loads the current route information
	 */
	public function __construct(){
		$this->Route();
	}

	/**
	 * Route PHP4 constructor
	 *
	 * @public
	 * @return void Loads the current route information
	 */
	public function Route(){
		$this->data = false;
	}

	/**
	 * Loads the route object that match the URL
	 * given, or current URL if none specified
	 *
	 * @public
	 * @return RouteData
	 */
	public function load($url = ''){
		if ($this->data == false) {
			$this->data = $this->findRoute($url);
		}
	}

	/**
	 * Loads the route object that match the requested URL
	 *
	 * @public
	 * @return RouteData
	 */
	public function findRoute($url = ''){
		//Find the routes that match the requested one.
		global $service;
		$retdata = false;
		if ($url == '') $url = $service->get('Url')->get();
		else $url = $service->get('Url')->parseUrl($url);

		switch(strtolower($url['ext'])) {
			case '.js' : {
				$retdata = $this->getJSRoute();
			}break;
			case '.css' : {
				$retdata = $this->getCSSRoute();
			}break;

			default : {
				//Search for content first.
				if ($url['route'] == '') {
					$url['route'] = $service->get('Language')->getCode();
				}

				//Use default language as route for home page.
				$content = $service->get('Content')->load($url['route']);
				if (is_a($content,'ContentData')) {
					$retdata = $this->getContentRoute();
				}
				if ($retdata == false) {
					//Content not found... parse route and see if it matches any.
					$pieces = explode('/',$url['route']);
					if (!is_array($pieces)) $pieces = array($pieces);

					$criteria = new CriteriaCompo();
					$store = new RouteStore();
					$data = $store->get($criteria);
					foreach ($data as $v){
						$routepieces = $v->getVar('route_url');
						if ($v->getVar('route_url') == '*' && strstr($pieces[0],'.css') == false && strstr($pieces[0],'.js') == false) {
							$retdata = $v;
						}else {
							$routepieces = explode('/',$routepieces);
							if (!is_array($routepieces)) $routepieces = array($routepieces);
							if (count($routepieces) == count($pieces)) {
								$found=true;
								foreach ($routepieces as $k2 => $v2) {
									if (substr($v2,0,1) != '{' && substr($v2,-1,1) != '}') {
										if ($pieces[$k2] != $routepieces[$k2]) {
											$found = false;
										}
									}
								}
								if ($found) $retdata = $v;
							}
						}
					}
					if ($retdata == false) {
						//No section matches current route, use Error404 route.
						$retdata = $this->getError404Route();
					}
				}
			}break;
		}
		return $retdata;
	}

	/**
	 * Get a JS ressource route object.
	 *
	 * @return object@RouteData
	 */
	public function getJSRoute(){
		$routeObj = new RouteData();
		$routeObj->setVar('route_id',0);
		$routeObj->setVar('route_url','');
		$routeObj->setVar('route_component','');
		$routeObj->setVar('route_controller','js');
		return $routeObj;
	}

	/**
	 * Get a Css ressource route object.
	 *
	 * @return object@RouteData
	 */
	public function getCssRoute(){
		$routeObj = new RouteData();
		$routeObj->setVar('route_id',0);
		$routeObj->setVar('route_url','');
		$routeObj->setVar('route_component','');
		$routeObj->setVar('route_controller','css');
		return $routeObj;
	}

	/**
	 * Get a Content route object.
	 *
	 * @return object@RouteData
	 */
	public function getContentRoute(){
		$routeObj = new RouteData();
		$routeObj->setVar('route_id',0);
		$routeObj->setVar('route_url','');
		$routeObj->setVar('route_component','');
		$routeObj->setVar('route_controller','content');
		return $routeObj;
	}

	/**
	 * Get a Not found - 404 Error route object.
	 *
	 * @return object@RouteData
	 */
	public function getError404Route(){
		$routeObj = new RouteData();
		$routeObj->setVar('route_id',0);
		$routeObj->setVar('route_url','');
		$routeObj->setVar('route_component','');
		$routeObj->setVar('route_controller','Error404');
		return $routeObj;
	}


	/**
	 * Returns the matching route(s) found.
	 *
	 * @return array@RouteData Returns an array of RouteData objects
	 */
	public function get(){
		if ($this->data == false) $this->load();
		return $this->data;
	}

	/**
	 * Returns the matching controllers for the route found.
	 *
	 * @param array@RouteData $data The route data we need to match controllers for.
	 * @return object@Controller The controller matching corresponding route in $data
	 */
	public function getController(){
		global $service;
		if ($this->data == false) $this->get();
		//Load all controllers and get their output
		if ($this->data->getVar('route_component') != '') {
			$service->get('Ressource')->get('com/'.strtolower($this->data->getVar('route_component')).'/controller/'.strtolower($this->data->getVar('route_controller')));
		}else {
			$service->get('Ressource')->get('core/controller/'.strtolower($this->data->getVar('route_controller')));
		}

		if (class_exists(ucfirst($this->data->getVar('route_controller')).'Controller')) {
			$controllerName = ucfirst($this->data->getVar('route_controller')).'Controller';
			return new $controllerName();
		}
		return false;
	}
}
