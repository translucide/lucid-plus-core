<?php
/**
 * Site service
 *
 * Defines which site we are on
 *
 * Jan 19, 2012
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

interface SiteInterface{
}

class SiteModel extends Model {
	public function __construct(){
		$this->type = 'site';
		$this->initVars(array(
			'site_id' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'site_url' => array(DATATYPE_STRING,array('length'=>255)),
			'site_root' => array(DATATYPE_STRING),
			'site_default' => array(DATATYPE_BOOL),
			'site_created' => array(DATATYPE_INT),
			'site_modified'	=> array(DATATYPE_INT),
			'site_modifiedby' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT)
		));
		$this->setOption('usecache',true);
		$this->setOption('cachetime',60*60*24);
	}
}

class SiteData extends Data{
	function __construct(){
		parent::__construct('site');
	}
}

class SiteStore extends Store{
	public function __construct(){
		parent::__construct('site');
		$this->setOption('ignore_langs',true);
		$this->setOption('ignore_sites',true);
		$this->setOption('urlreplace',false);
	}
}

class Site {

	/**
	 * Hold a reference to SiteData objects that match the requested site
	 *
	 * @access private
	 *
	 * @var array Array of SiteData objects that matched the current requested site
	 */
	private $data;

	/**
	 * Site PHP5 constructor
	 *
	 * @access public
	 * @return void Loads the current site information
	 */
	public function __construct(){
		$this->data = false;
		$this->load();
	}

	/**
	 * Loads the site objects that match the current URL
	 *
	 * @access public
	 * @return array Array of SiteData objects
	 */
	public function load(){
		if ($this->data == false) {
			$this->data = $this->findSite();
		}
	}

	/**
	 * Loads the site objects that match the requested URL
	 *
	 * @access public
	 * @access public
	 * @return array Array of SiteData objects
	 */
	public function findSite($url = ''){
		global $service;
		$retdata = false;
		if ($url == '') $url = $service->get('Url')->get();
		else $url = $service->get('Url')->parseUrl($url);
		$host = substr(URL,strpos(URL,'://')+3,-1);
		$store = new SiteStore();
		$store->setOption('ignoresites',true);
		$data = $store->get();
		foreach ($data as $k => $v) {
			if ($v->getVar('site_url') == $host) {
				$retdata = $v;
			}
		}
		if ($retdata == false) {
			//No site record found
			if (count($data) == 0) {
				$service->get('Log')->add('No site found in DB! Your current DATAROOT is "'.DATAROOT.'". Current host is '.$host,E_ERROR,LOG_FILE_SYSTEM,__FILE__,__LINE__);
			}

			//Use default site.
			else {
				foreach ($data as $k => $v) {
					if ($v->getVar('site_default')){
						$v->setVar('site_url',str_replace('{url}',$host,$v->getVar('site_url')));
						$v->setVar('site_root',str_replace('{root}',ROOT,$v->getVar('site_root')));
						$retdata = $v;
					}
				}
			}
		}
        if (!is_object($retdata)){
            echo '<br>No site found in DB! Your current DATAROOT is "'.DATAROOT.'". Current host is '.$host;
            die;
        }
		return $retdata;
	}

	/**
	 * Returns the matching site(s) found.
	 *
	 * @return SiteData Returns a SiteData object
	 */
	public function get(){
		return $this->data;
	}

	/**
	 * Returns the matching site ID found.
	 *
	 * @return int Current site found
	 */
	public function getId(){
		if (isset($this->data) && $this->data && is_object($this->data)) {
			return $this->data->getVar('site_id');
		}else return false;
	}

	/**
	 * Returns the matching site ID found.
	 *
	 * @return int Current site found
	 */
	public function getUrl(){
		if (isset($this->data) && $this->data && is_object($this->data)) {
			return $this->data->getVar('site_url');
		}else return false;
	}
}
?>
