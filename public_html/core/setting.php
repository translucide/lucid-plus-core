<?php
/**
 * Settings service
 *
 * Manages system settings
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

interface SettingInterface{
}

class SettingModel extends Model {
	public function __construct(){
		$this->setType('setting');
		$this->initVars(array(
			'setting_id' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'setting_objid' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'setting_cid' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'setting_name' => array(DATATYPE_STRING,array('length'=>50)),
			'setting_value' => array(DATATYPE_STRING),
			'setting_type' => array(DATATYPE_STRING,array('length'=>50)),
			'setting_options' => array(DATATYPE_STRING),
			'setting_title' => array(DATATYPE_STRING),
			'setting_description' => array(DATATYPE_STRING),
			'setting_language'	=> array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'setting_site' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'setting_created' => array(DATATYPE_INT),
			'setting_modified'	=> array(DATATYPE_INT),
			'setting_modifiedby' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT)
		));
		$this->setOption('usecache',false);
		$this->setOption('cachetime',60*60*24);
	}
}

class SettingCategoryModel extends Model {
	public function __construct(){
		$this->setType('settingcategory');
		$this->initVars(array(
			'settingcategory_id' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'settingcategory_objid' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'settingcategory_name' => array(DATATYPE_STRING,array('length'=>50)),
			'settingcategory_title' => array(DATATYPE_STRING,array('length'=>50)),
			'settingcategory_language'	=> array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'settingcategory_site' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'settingcategory_created' => array(DATATYPE_INT),
			'settingcategory_modified'	=> array(DATATYPE_INT),
			'settingcategory_modifiedby' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT)
		));
		$this->setOption('usecache',false);
		$this->setOption('cachetime',60*60*24);
	}
}

class SettingData extends Data{
	function __construct(){
        parent::__construct('setting');
	}
}
class SettingcategoryData extends Data{
	function __construct(){
        parent::__construct('settingcategory');
	}
}

class SettingStore extends Store{
	public function __construct(){
		parent::__construct('setting');
	}
}
class SettingCategoryStore extends Store{
	public function __construct(){
		parent::__construct('settingcategory');
	}
}

class Setting {

	/**
	 * Hold a reference to loaded setting data
	 *
	 * @access private
	 *
	 * @var array Key value settings pairs
	 */
	private $data;

	/**
	 * Setting PHP5 constructor
	 *
	 * @access public
	 * @return void Loads the current Setting information
	 */
	public function __construct(){
		$this->data = false;
		$this->load();
	}

	/**
	 * Loads the website settings that are not related to language
	 *
	 * @access public
	 * @return void Loads settings into the data property
	 */
	public function load(){
		if ($this->data == false) {
			$criteria = new Criteria('setting_language','0');
			$settingStore = new SettingStore();
			$settingStore->setOption('ignorelangs',true);
			$objs = $settingStore->get($criteria);
			if (count($objs) == 0) {
                global $service;
                $service->get('Log')->add("Could not load settings. Aborting.",__FILE__,__LINE__,E_ERROR,LOG_FILE_DATABASE);
				echo '<br>Could not load settings. Aborting.';
                die;
			}
			foreach($objs as $k => $v) {
				$this->data[$v->getVar('setting_name')] = $v->getVar('setting_value');
			}
		}
	}

	/**
	 * Loads the website multilingual settings
	 *
	 * @access public
	 * @return void Loads settings into the data property
	 */
	public function loadLangSettings(){
		$criteria = new Criteria('setting_language','0', '!=');
		$settingStore = new SettingStore();
		$settingStore->setOption('ignorelangs',false);
		$objs = $settingStore->get($criteria);
		foreach($objs as $k => $v) {
			$this->data[$v->getVar('setting_name')] = $v->getVar('setting_value');
		}
	}

	/**
	 * Returns the specified setting or all if no setting name is specified.
	 *
	 * @access public
	 * @var string $name
	 * @return mixed Setting or settings values
	 */
	public function get($name=''){
		if ($name != '') return $this->data[$name];
		return $this->data;
	}

	/**
	 * Sets the specified setting(s) value(s)
	 *
	 * @access public
	 * @var array@Setting $settings
	 * @return mixed Setting or settings values
	 */
	public function set($settings){
		$settingStore = new SettingStore();
		return $settingStore->save($settings);
	}

	/**
	 * Checks if a setting exists
	 *
	 * @access public
	 *
	 * @return bool True if setting exists
	 */
	public function exist($name) {
		$settingStore = new SettingStore();
		$setting = $settingStore->get(new Criteria('setting_name',$name));
		return count($setting);
	}
}
