<?php
/**
 * Session manager
 *
 * Manages user sessions
 *
 * October 28, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
if (!interface_exists('SessionHandlerInterface')) {
	define('SESSION_OOPHANDLER',false);
	interface SessionHandlerInterface {
		public function close();
		public function destroy(string $session_id);
		public function gc(int $maxlifetime);
		public function open(string $save_path , string $session_name);
		public function read(string $session_id);
		public function write(string $session_id, string $session_data);
	}
}

class SessionModel extends Model {
	function __construct(){
		$this->initVar('session_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('session_sid',DATATYPE_STRING);
		$this->initVar('session_data',DATATYPE_STRING);
		$this->initVar('session_created',DATATYPE_INT);
		$this->initVar('session_modified',DATATYPE_INT);
		$this->initVar('session_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->type = 'session';
	}
}

class SessionData extends Data {
	function __construct(){
        parent::__construct('session');
	}
}

class SessionStore extends Store {
	function __construct(){
		parent::__construct('session');
	}

}

class Session implements SessionHandlerInterface {
    /**
     * Number of seconds to hold data
     *
     * @access private
     * @var int $duration Number of seconds a session will stay idle and open
     **/
    private $duration;

	/**
	 * Session constructor
	 *
	 * @access public
	 *
	 * @return void Session()
	 */
	public function __construct(){
        session_set_save_handler($this, true);
		$this->start();
	}

	public function __destruct(){
	}

	/**
	 * Starts a session
	 *
	 * @access public
	 *
	 * @return bool Success: true
	 */
	public function start(){
        if (substr(URL,0,strlen("https://")) == 'https://') {
            @ini_set('session.cookie_httponly', 1);
            @ini_set('session.cookie_secure', 1);
        }
        //This setting was set through : $service->get('Setting')->get('sessionlifetime') * 60;
        //Now automatic logout is done in com/system/controller/session via lucidCore plugin.
		$this->duration = 0;
        @ini_set("session.gc_maxlifetime",$this->duration);
        @ini_set("session.cookie_lifetime",$this->duration);
		$ret = session_start(['cookie_lifetime' => $this->duration]);
        $_SESSION['time'] = time();
        return $ret;
	}

	/**
	 * Functions called by PHP
	 */
    public function open($savepath, $sessionname){
        return true;
    }

	public function close(){
		return true;
	}

	public function read($id){
		global $service;
		$store = new SessionStore();
		$sessionCrit = new CriteriaCompo();
		$sessionCrit->add(new Criteria('session_sid',$id));
		$session = $store->get($sessionCrit);
		if (is_array($session) && count($session) > 0 && is_object($session[0])) {
			$session = $session[0];
		}
		if (is_object($session)) {
			return $session->getVar('session_data');
		}
		return "";
	}

	public function write($id,$data){
        $ret = false;
		$store = new SessionStore();
		$sessionCrit = new CriteriaCompo();
		$sessionCrit->add(new Criteria('session_sid',$id));
		$sessionCrit->setLimit(1);
		$exists = $store->count($sessionCrit);
        if ($exists > 0) {
            $session = $store->get($sessionCrit);
            if (is_array($session) && is_object($session[0])) {
                $session = $session[0];
            }
        }else {
            //Session does not exist yet, lets create it.
            $session = new SessionData();
            $session->setVar('session_id','0');
            $session->setVar('session_sid',$id);
        }
        $session->setVar('session_data',$data);
        if ($store->save($session) === false) {
            trigger_error('Unable to restore user session. Unserialize failed.');
        }
        else $ret = true;
        return $ret;
	}

	public function gc($maxlifetime){
		$store = new SessionStore();
        if ($maxlifetime > 0) {
    		return $store->delete(new Criteria('session_modified',time()-$maxlifetime,'<'));
        }
        return true;
	}

	public function destroy($id=0){
        if ($id == 0) $id = session_id();
		$store = new SessionStore();
		return $store->delete(new Criteria('session_sid',$id));
	}
}
