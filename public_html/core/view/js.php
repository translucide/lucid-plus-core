<?php
/**
 * Default JS view
 *
 * Default view loaded on JS page requests
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('obj/view');
  
class JsView extends View{
	public function init(){
	}
	
	public function render(){
		return $this->data['code'];
	}	
}
?>