<?php
/**
 * Default CSS view
 *
 * Default view loaded on CSS page requests
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('ressource')->get('obj/view');
  
class CssView extends View{
	public function init(){
	}
	
	public function render(){
		return $this->data['code'];
	}	
}
?>