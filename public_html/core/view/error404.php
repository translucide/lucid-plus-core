<?php
/**
 * Default Error 404 view
 *
 * Default view loaded on 404 not found
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('obj/view');

class Error404View extends View{
	public function init(){
	}
	public function render(){
		return '<h1 class="httperror error404 pagetitle">'.ERROR404_TITLE.'</h1>'.
            '<article class="httperror error404"><div class="content">'.
            '<h1 class="articletitle">'.ERROR404_TITLE.'</h1>'.
            '<p>'.ERROR404_MSG.'</p>.<p>'.ERROR404_GETBACK.'</p></div></article>';
	}
}
?>
