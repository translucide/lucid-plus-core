<?php
/**
 * Default redirect view
 *
 * Default view loaded on a redirect
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('obj/view');

class RedirectView extends View{
    public function __construct($statuscode,$url='') {
        parent::__construct();
        $this->setVar('errorcode',$errorcode);
    }

    public function setVar($var,$value) {
        if ($var == 'errorcode' && defined('HTTPERROR_'.$value.'_TITLE')){
            $this->setVar('title',constant('HTTPERROR_'.$value.'_TITLE'));
            $this->setVar('message',constant('HTTPERROR_'.$value.'_DESC'));
            $this->setVar('linktext',constant('HTTPERROR_'.$value.'_LINKTEXT'));
            switch($value){
                case '403': $this->setVar('linkurl','javascript:history.go(-1);'); break;
                case '401': $this->setVar('linkurl',URL.$service->get('Language')->getCode().'/user/login'); break;
            }
        }
        parent::setVar($var,$value);
    }
	public function render(){
        switch($this->getVar('errorcode')) {
            case 403: header("HTTP/1.1 403 Forbidden"); break;
            case 401: header("HTTP/1.1 401 Authorization Required"); break;
        }
        $this->setTemplate('code/view/template/redirect.tpl');
		return $this->renderTemplate();
	}
}
?>
