<?php
/**
 * Tree view
 *
 * Lucid+ default Tree view
 *
 * Jul 22, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/view');

class TreeView extends DefaultView{
    public function __construct(){
        $this->TreeView();
    }
    public function TreeView(){
        $this->mapCallbacks();
        $this->setVar('options',array(
            'contenttype' => 'widget',
            'excludedtypes' => array('section','folder'),
            'addtypes' => array('folder')
        ));
    }

    /**
     * Gets or sets the API base url to which ajax calls will be made.
     * Operation name will be appended to this URL.
     *
     * @access public
     * @param string $apiUrl The base API url to use, ending with a '/'.
     * @return string When called without parameter, returns the current API URL.
     */
    public function apiUrl($apiurl = false) {
        if ($apiurl) $this->setVar('apiurl',$apiurl);
        else return $this->getVar('apiurl');
    }

    /**
     * Gets or sets breadcrumbs path
     *
     * @access public
     * @param string $breadcrumbs
     * @return string Breadcrumbs string when called without any parameter
     */
    public function breadcrumbs($d=false){
        if ($d) $this->setVar('breadcrumbs',$d);
        else return $this->getVar('breadcrumbs');
    }

    /**
     * Sets the initialy displayed form
     *
     * @access public
     * @param Form $form
     * @return Form Current default form object.
     */
    public function form($form = false){
        if ($form) $this->setVar('form',$form);
        else return $this->getVar('form');
    }

    /**
     * Gets or sets different options
     *
     * Current options are:
     * - contenttype : 'widget', 'content', or other content type
     * - infotypekey : the type key in infos found
     *
     * @access public
     * @param string $option
     * @param string $value
     * @return mixed $value
     */
    public function option($option,$value=null){
        $options = $this->getVar('options');
        if ($value != null) {
            $options[$option] = $value;
            $this->setVar('options',$options);
        }
        else return $options[$option];
    }

    /**
     * Adds a callback mapping
     *
     * @access public
     * @param array $c
     * @return void
     */
    public function mapCallbacks($c = null) {
        if (is_array($c)) $this->setVar('callbacks',$c);
        else {
            $this->setVar('callbacks',array(
                'getchildren' => 'getchildren',
                'searchtree' => 'searchtree',
                'createnode' => 'createnode',
                'movenode' => 'movenode',
                'removenode' => 'removenode',
                'edit' => 'edit',
                'save' => 'save'
            ));
        }
    }

	public function render(){
		global $service;
        $service->get('Ressource')->get('core/display/interface');
        $service->get('Ressource')->get('lib/bootstrap3-dialog/1.35.4/js/bootstrap-dialog.min');
        $service->get('Ressource')->get('lib/bootstrap3-dialog/1.35.4/css/bootstrap-dialog.min');
        $infos = $this->getInfos();
        $map = $this->getVar('callbacks');
        $opt = $this->getVar('options');

        if (!is_object($this->data['form']) || !is_a($this->data['form'],'Form')) {
            $service->get('Log')->add("TreeView::render: Form object not found!",__FILE__,__LINE__,E_INFO);
            return '';
        }

		$validchilds = '';
		$treecontenttypes = '';
        $contenttypes = array();
        $si = 0;
        foreach($infos as $k => $v) {
            if (in_array($v[$opt['infotypekey']],$opt['excludedtypes']) === false) {
                $treecontenttypes .= $this->getContentIconCode($v).',';
                $validchilds .= "'{$v[$opt['infotypekey']]}',";
                $contenttypes[] = $this->getContentButton($v);
                $addcontentmenu[] = $this->getContentMenuItem($v);
            }
            else {
                $si = $k;
            }
        }
		$treecontenttypes.= "'folder' : {
            'valid_children' : [$validchilds'folder'],
            'icon' : {
                'image' : '".URL.$service->get('Ressource')->getIcon($infos[$si])."'
            }
        }";
		$contenttypes[] = new LucidButton([
            'id' => 'addtype_'.$infos[$si]['type'],
            'title' => $infos[$si]['title'],
            'icon' => URL.$service->get('Ressource')->getIcon($infos[$si]),
            'events' => [
                'onclick' => "$('#tree').jstree('create', null, 'last', {
                    'attr' : { 'rel' : 'folder' }
                });"
            ]
		]);
        $addcontentmenu[] = new LucidActionPanel([
            'title' => $infos[$si]['title'],
            'description' => $infos[$si]['description'],
            'childs' => [
                new LucidPrimaryButton([
                    'title' => _NEW,
                    'icon' => URL.$service->get('Ressource')->getIcon($infos[$si]),
                    'events' => [
                        'onclick' => '$(\'#tree\').jstree(\'create\', null, \'last\', {
                            \'attr\' : { \'rel\' : \''.$infos[$si][$opt['infotypekey']].'\' }
                        });'
                    ],
                ])
            ]
        ]);
		$addcontentmenucode = '<form><fieldset><legend>'.$opt['addcontenttitle'].'</legend>';
		foreach($addcontentmenu as $v){
			$addcontentmenucode .= $v->render();
		}
		$addcontentmenucode .= '</fieldset></form>';

        $leftcol = [
			new LucidToolBar([
				'width' => '100%',
				'id' => 'treebar',
				'childs' => [
					new LucidButton([
						'id' => 'add',
						'title' => _ADD,
						'icon' => 'plus',
						'events' => [
							'onmouseover' => '$(\'#statusbar\').html(\''.SYSTEM_ADMIN_SECTION_ADDFOLDERTIP.'\');',
							'onmouseout' => '$(\'#statusbar\').html(\'\');',
                            'onclick' => '$(\'#responsepanel\').hide();$(\'#formpanel > div:first-child\').html(\''.addslashes(htmlentities(str_replace(["\n","\r"],'',$addcontentmenucode))).'\');$(\'#formpanel\').show();'
                        ]
					]),
					new LucidButton([
						'id' => 'delete',
						'title' => _DELETE,
						'icon' => 'trash',
						'events' => [
							'onclick' => 'bootbox.confirm(\''.
                                addslashes(_CONFIRMDELETE).'\',
                                function(result){
                                    if(result) $(\'#tree\').jstree(\'remove\');
                                }
                            );',
							'onmouseover' => '$(\'#statusbar\').html(\''.
                                addslashes(_DELETETIP).'\');',
							'onmouseout' => '$(\'#statusbar\').html(\'\');'
						]
					])
				]
			]),
			new LucidTree([
				'id' => 'tree'
			])
		];
		$interface = new LucidInterface([
			'id' => 'contentmanager',
			'name' => 'contentmanager',
			'childs' => [
                new LucidViewport([
					'childs' => [
						new LucidLayout([
							'childs' => [
								new LucidHBox([
									'childs' => [
										new LucidBreadCrumbs([
											'childs' => $this->data['breadcrumbs']
										])
									]
								]),
								new LucidHBox([
									'childs' => [
										new LucidLeftVBox([
											'childs' => $leftcol
										]),
										new LucidMainVBox([
											'childs' => [
												new LucidHBox([
													'childs' => [
														new LucidPanel([
															'id' => 'formpanel',
															'html' =>  $this->data['form']->render()
														])
													]
												]),
												new LucidHBox([
													'childs' => [
														new LucidPanel([
															'id' => 'responsepanel',
															'html' =>  ''
														])
													]
												])
											]
										])
									]
								]),
								new LucidHBox([
									'childs' => [
										new LucidStatusBar([
                                            'id' => 'statusbar',
                                            'html' => 'Status bar ...'
                                        ])
									]
								]),
							]
						])
					]
				])
			]
		]);
		$code = $interface->render();
		$script = $interface->renderJS().'$(function () {
		$(\'#tree\')
			.bind(\'before.jstree\', function (e, data) {
                if(data.func === "select_node") {
                    if ($("#formpanel").length > 0 && $("#formpanel").attr("data-is-saved") != "1" && typeof $("#formpanel").attr("data-is-saved") != \'undefined\' && $("#formpanel").attr("data-is-saved") !== false && $("#formpanel").css("display") != "none") {
                        BootstrapDialog.show({
                            title: "Your changes will be lost !",
                            message: "Are you sure you want to continue without saving ?",
                            buttons: [{
                                    label: "No",
                                    cssClass: "btn-default",
                                    action: function(dialogRef) {
                                        dialogRef.close();
                                    }
                                }, {
                                    label: "Yes",
                                    cssClass: "btn-primary",
                                    action: function(dialogRef) {
                                        $("#formpanel").attr("data-is-saved",1);
                                        data.inst.deselect_all();
                                        data.inst.select_node(data.args[0]);
                                        dialogRef.close();
                                    }
                                }
                            ]
                        });
                        e.stopImmediatePropagation();
                        return false;
                    }
                }
			})
			.jstree({
				\'plugins\' : [
					\'themes\',\'json_data\',\'ui\',\'crrm\',\'cookies\',\'dnd\',\'search\',\'types\',\'contextmenu\'
				],
				\'themes\' : {
					\'theme\' : \'lucid\',
					\'url\' : \''.URL.'lib/jstree/1.0rc3/themes/lucid/style.css\',
					\'dots\' : true,
					\'icons\' : true
		        },
				\'json_data\' : {
					\'ajax\' : {
						\'url\' : \''.$this->data['apiurl'].$map['getchildren'].'\',
						\'data\' : function (n) {
							return {
								\'id\' : n.attr ? n.attr(\'id\').replace(\'node_\',\'\') : 1
							};
						}
					}
				},
				\'search\' : {
					\'ajax\' : {
						\'url\' : \''.$this->data['apiurl'].$map['searchtree'].'\',
						\'data\' : function (str) {
							return {
								\'search_str\' : str
							};
						}
					}
				},
				\'types\' : {
					\'max_depth\' : -2,
					\'max_children\' : -2,
					\'valid_children\' : [ \'drive\' ],
					\'types\' : {
						'.$treecontenttypes.'
					}
				}
			})
			.bind(\'create.jstree\', function (e, data) {
				$.post(
					\''.$this->data['apiurl'].$map['createnode'].'\',
					{
						\'id\' : data.rslt.parent.attr(\'id\').replace(\'node_\',\'\'),
						\'position\' : data.rslt.position,
						\'title\' : data.rslt.name,
						\'nodetype\' : data.rslt.obj.attr(\'rel\')
					},
					function (r) {
						if(r.status) {
							$(data.rslt.obj).attr(\'id\', \'node_\' + r.id);
						}
						else {
							$.jstree.rollback(data.rlbk);
						}
					}
				);
			})
			.bind(\'remove.jstree\', function (e, data) {
				data.rslt.obj.each(function () {
					$.ajax({
						async : false,
						type: \'POST\',
						url : \''.$this->data['apiurl'].$map['removenode'].'\',
						data : {
							\'id\' : this.id.replace(\'node_\',\'\'),
							\'nodetype\' : $(this).attr("rel")
						},
						success : function (r) {
							if(!r.status) {
								data.inst.refresh();
							}
						}
					});
				});
			})
			.bind(\'rename.jstree\', function (e, data) {
				$.post(
					\''.$this->data['apiurl'].$map['renamenode'].'\',
					{
						\'id\' : data.rslt.obj.attr(\'id\').replace(\'node_\',\'\'),
						\'title\' : data.rslt.new_name,
						\'nodetype\' : data.rslt.obj.attr(\'rel\')
					},
					function (r) {
						if(!r.status) {
							$.jstree.rollback(data.rlbk);
						}
					}
				);
			})
			.bind(\'move_node.jstree\', function (e, data) {
				data.rslt.o.each(function (i) {
					$.ajax({
						async : false,
						type: \'POST\',
						url: \''.$this->data['apiurl'].$map['movenode'].'\',
						data : {
							\'operation\' : \'move_node\',
							\'id\' : $(this).attr(\'id\').replace(\'node_\',\'\'),
							\'ref\' : data.rslt.cr === -1 ? 1 : data.rslt.np.attr(\'id\').replace(\'node_\',\'\'),
							\'position\' : data.rslt.cp + i,
							\'title\' : data.rslt.name,
							\'copy\' : data.rslt.cy ? 1 : 0,
							\'nodetype\' : $(this).attr("rel")
						},
						success : function (r) {
							if(!r.status == "1") {
								$.jstree.rollback(data.rlbk);
							}
							else {
								$(data.rslt.oc).attr(\'id\', \'node_\' + r.id);
								if(data.rslt.cy && $(data.rslt.oc).children(\'UL\').length) {
									data.inst.refresh(data.inst._get_parent(data.rslt.oc));
								}
							}

						}
					});
				});
			})
			.bind("select_node.jstree", function (event, data) {
                $.ajax({
                    async : false,
                    type: \'POST\',
                    url: \''.$this->data['apiurl'].$map['edit'].'\',
                    data : {
                        \'id\' : data.rslt.obj.attr("id").replace(\'node_\',\'\'),
                        \'nodetype\' : data.rslt.obj.attr("rel")
                    },
                    success : function (r) {
                        var html = "";
                        if (!r.success)	{
                            $("#formpanel").hide();
                            html = "<div class=\"warning\">";
                            html += r.message;
                            html += "</div>";
                            $("#responsepanel").html(html).show();
                        }else {
                            if ($(window).width() < 768) {
                                $("#tree a").click(function() {
                                    $("html, body").animate({
                                        scrollTop: $("#formpanel").offset().top
                                    }, 100);
                                });
                            }
                            try{for (instance in CKEDITOR.instances) CKEDITOR.instances[instance].destroy();}catch(e){}
                            $("#formpanel > div:first-child").lucidForm(r.form);
                            $("#formpanel").attr("data-is-saved",1);
                            $("#formpanel > div:first-child input, #formpanel > div:first-child select, #formpanel > div:first-child textarea").on("change",function(){
                                console.info("is changed");
                                $("#formpanel").attr("data-is-saved",0);
                            });
                            $("#formpanel form:first-child").ajaxForm({
                                target: "#responsepanel",
                                url : "'.$this->data['apiurl'].$map['save'].'",
                                beforeSubmit : function(data, jqForm, options){
                                    try {
                                        for (instance in CKEDITOR.instances) {
                                            var ckvalue = CKEDITOR.instances[instance].getData();
                                            $(\'#\'+instance).val(ckvalue);
                                            for(i=0;i<data.length;i++) {
                                                if(data[i].name == instance) data[i].value = ckvalue;
                                            }
                                            CKEDITOR.instances[instance].destroy();
                                        }
                                    } catch(err) {}
                                    return true;
                                },
                                success: function(responseText, statusText, xhr, $form){
                                    $("html, body").animate({ scrollTop: 0 }, "fast");
                                    if (responseText.success) {
                                        $("#formpanel").hide();
                                        $("#responsepanel").html("<div class=\"success\">"+responseText.message+"</div>").fadeIn();
                                        setTimeout(function(){
                                            $.get("'.$this->data['apiurl'].$map['getchildren'].'", function () {
                                                $("#tree").jstree("refresh",-1);
                                            });
                                        },1000);
                                    }else {
                                        $("#responsepanel").html("<div class=\"warning\">"+responseText.message+"</div>").fadeIn();
                                    }

                                }
                            });
                            $("#responsepanel").hide();
                            $("#formpanel").show();
                        }
                    }
                });
			});
		});
        $(document).ready(function() {
            $("#formpanel form").ajaxForm({
				target: "#responsepanel",
				url : "'.$this->data['apiurl'].$map['save'].'",
				beforeSubmit : function(data, jqForm, options){
                    try {
                        for (instance in CKEDITOR.instances) {
                            var ckvalue = CKEDITOR.instances[instance].getData();
                            $(\'#\'+instance).val(ckvalue);
                            for(i=0;i<data.length;i++) {
                                if(data[i].name == instance) data[i].value = ckvalue;
                            }
                            CKEDITOR.instances[instance].destroy();
                        }
                    } catch(err) {}
					return true;
				},

				success: function(responseText, statusText, xhr, $form){
					$("html, body").animate({ scrollTop: 0 }, "fast");
					if (responseText.status) {
						$("#formpanel").hide();
						$("#responsepanel").html("<div class=\"success\">"+responseText.message+"</div>").fadeIn();
						setTimeout("$.get(\''.$this->data['apiurl'].$map['getchildren'].'\', function () { $(\'#tree\').jstree(\'refresh\',-1);});",1000);
					}else {
						$("#responsepanel").html("<div class=\"warning\">"+responseText.message+"</div>").fadeIn();
					}

				}
			});
        });

        /**
         * Make sure Bootstrap Modal gets in focus when shown...
         */
		$.fn.modal.Constructor.prototype.enforceFocus = function() {
			modal_this = this
			$(document).on(\'focusin.modal\', function (e) {
			if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
			&& !$(e.target.parentNode).hasClass(\'cke_dialog_ui_input_select\')
			&& !$(e.target.parentNode).hasClass(\'cke_dialog_ui_input_text\')) {
				modal_this.$element.focus()
			}
			})
		};
		';
		$service->get('Ressource')->get('lib/jstree/1.0rc3/lib/cookie');
		$service->get('Ressource')->get('lib/jstree/1.0rc3/jstree');
		$service->get('Ressource')->get('lib/bootbox/4.3.0/bootbox.min');
		$service->get('Ressource')->get('lib/ajaxform');
		/*$service->get('Ressource')->addScript('var CKEDITOR_BASEPATH = \''.URL.'lib/ckeditor/4.5.9/\';CKFINDER_BASEPATH = \''.URL.'lib/ckfinder/2.4.2/\';',true);
		$service->get('Ressource')->get('lib/ckfinder/2.4.2/ckfinder');
		$service->get('Ressource')->get('lib/ckeditor/4.5.9/ckeditor');*/
		$service->get('Ressource')->get('lib/packages/form/form');
		$service->get('Ressource')->get('lib/jquery-ui-core-interactions/1.11.1/jquery-ui.min');
		$service->get('Ressource')->addScript($script);
		return $code;
    }

    private function getInfos(){
        global $service;
        //Get content types and add folder content type
        $o = $this->getVar('options');
        $infos = array();
        $infos = $service->get('Ressource')->getInfos($o['contenttype']);
        $f = false;
        foreach ($infos as $v ) {
            if ($v[$o['infotypekey']] == 'folder') $f = true;
        }
        if (!$f) $infos[] = array(
            $o['infotypekey'] => 'folder',
            'title' => _FOLDER,
            'description' => _FOLDERDESC,
            'component' => 'system',
            'icon' => 'folder'
        );
        return $infos;
    }

    private function getContentIconCode($v) {
        global $service;
        $key = $this->getVar('options')['infotypekey'];
        return "'{$v[$key]}': {
            'valid_children' : 'none',
            'icon' : {
                'image' : '".URL.$service->get('Ressource')->getIcon($v)."'
            }
        }";
    }

    private function getContentButton($v){
        global $service;
        $key = $this->getVar('options')['infotypekey'];
        return new LucidButton([
            'id' => 'addtype_'.$v[$key],
            'title' => $v['title'],
            'icon' => URL.$service->get('Ressource')->getIcon($v),
            'events' => [
                'onclick' => "$('#tree').jstree('create', null, 'last', {
                    'attr' : {
                        'rel' : '{$v[$key]}'
                    }
                });"
            ]
        ]);
    }

    private function getContentMenuItem($v){
        global $service;
        $key = $this->getVar('options')['infotypekey'];
        return new LucidActionPanel([
            'title' => $v['title'],
            'description' => $v['description'],
            'childs' => [
                new LucidPrimaryButton([
                    'title' => _NEW,
                    'icon' => URL.$service->get('Ressource')->getIcon($v),
                    'events' => [
                        'onclick' => '$(\'#tree\').jstree(\'create\', null, \'last\', {
                            \'attr\' : { \'rel\' : \''.$v[$key].'\' }
                        });'
                    ],
                ])
            ]
        ]);
    }
}
?>
