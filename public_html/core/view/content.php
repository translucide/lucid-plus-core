<?php
/**
 * Section view
 *
 * Default view loaded on section page request
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('com/view/default');

class ContentView extends DefaultView{
	public function init(){
	}
	
	public function render(){
		global $url;
		$code = '';
        $code .= '<h1 class="pagetitle'.(($this->data['element']['displaytitle'])?'':' hidden').'">'.$this->data['element']['pagetitle'].'</h1>';
		$code .= '<article class="';
		if ($this->data['class'] != '') $code .= ' '.$this->data['class'];
		if ($this->data['element']['class'] != '') $code .= ' '.$this->data['element']['class'];
		if ($this->data['element']['parent'] != '') $code .= ' section_'.$this->data['element']['parent'];
		if ($this->data['element']['id'] != '') $code .= ' objid-'.$this->data['element']['objid'];
		if ($this->data['element']['parent'] != '') $code .= ' parent-'.$this->data['element']['parent'];
		if ($this->data['element']['classpath'] != '') $code .= ' url-'.$this->data['element']['classpath'];
		if ($this->data['layout'] != '') $code .= ' '.$this->data['layout'];
		if ($this->data['element']['type'] != '') $code .= ' '.$this->data['element']['type'];
		$code .= '" id="'.$this->data['element']['type'].'_'.$this->data['element']['id'].'">';
		$code .= '[widgetzone:beforetitle(itemsperline=1)]';
		$code .= '<h1 class="articletitle'.(($this->data['element']['displaytitle'])?'':' hidden').'">'.$this->data['element']['title'].'</h1>';
		$code .= '[widgetzone:aftertitle(itemsperline=1)]';		
		$code .= '<div class="content">'.$this->data['element']['content'].'</div>';
		$code .= '[widgetzone:aftercontent(itemsperline=1)]';				
		$code .= '</article>';
		return $code;
	}
}
?>