<h1 class="httperror error{:$errorcode:} pagetitle">{:$title:}</h1>
<article class="httperror error{:$errorcode:}">
    <div class="content">
        <h1 class="articletitle">{:$title:}</h1>
        <p>{:$message:}</p>
        <p><a href="{:$redirecturl:}">{:$redirecttext:}</a></p>
    </div>
</article>
<script language="javascript" type="text/javascript">
setTimeout(
    function(){
        window.location.replace("{:$redirecturl:}");
    },2000
);
</script>
