<?php
/**
 * Request object
 *
 * Gets all request parameters and optionnaly filters them.
 *
 * Feb 9, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class Request {
	
	/**
	 * Holds request key values pairs
	 *
	 * @access private
	 * @var array $data
	 */
	private $data;
	
	/**
	 * Options
	 *
	 * @access private
	 * @var array $options
	 */
	private $options;
	
	/**
	 * Request PHP5 constructor
	 *
	 * @access public
	 * @return void Loads the current request parameters
	 */
	public function __construct(){
		$this->Request();
	}
	
	/**
	 * Route PHP4 constructor
	 *
	 * @access public
	 * @return void Loads the current route information
	 */
	public function Request($autoload=true){
		$this->data = false;
		$this->options = array(
			'prefix' => '',
			'prefixexceptions' => array('op','id','type','name')
		);
		if ($autoload){
			$this->load();
		}
	}
	
	/**
	 * Gets / sets an option
	 *
	 * @access public
	 *
	 * @param string $var Option name
	 * @param mixed $val Option value
	 * @return mixed Option value or void in getter context
	 */
	public function option($var,$val=null){
		if ($val == null) return $this->options[$var];
		else $this->options[$var] = $val;
	}

	/**
	 * Adds a variable to this request object
	 *
	 * @access public
	 * @param string $var
	 * @param mixed $val
	 * @return void Adds a variable to this request object.
	 */
	public function add($var, $val) {
		$this->data[$var] = $val;
	}
	
	/**
	 * Loads all variables available in the current route and in the $_POST array
	 *
	 * @access public
	 * @return void Fills the data property.
	 */
	public function load(){
		global $service;
		$route = $service->get('Route');
		
		//Find the routes that match the requested one.
        $isSecure = (strpos(URL,"https://") !== false)?true:false;
		$url = (($isSecure == false)?'http://':'https://').$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$url = str_replace(URL,'',$url);
		if (substr($url,0,1) == '/') $url = substr($url,1);
		if (substr($url,-1,1) == '/') $url = substr($url,0,-1);
		$pieces = explode('/',$url);
		
		//Add get first, so any conflicting parameter overrides the GET ones.
		foreach ($_GET as $k => $v) {
			$this->add($k,$v);
		}
		
		//Add the route parameters in second
		$v = $route->get();
		$routepieces = explode('/',$v->getVar('route_url'));
		foreach ($routepieces as $kp => $vp) {
			if (substr($vp,0,1) == '{' && substr($vp,-1,1) == '}') {
				$param = substr($vp,1,-1);
				$val = $pieces[$kp];
				if (strpos($val,'?') !== false) $val = substr($val,0,strpos($val,'?'));
				$this->add($param,$val);
			}
		}
		
		//Add the POST parameters last so they can override any parameter
		foreach ($_POST as $k => $v){
			$this->add($k,$v);
		}
	}
	
	/**
	 * Gets a request variable
	 *
	 * @access public
	 * @var string $param
	 * @return mixed Value of the request variable
	 */
	public function get($param='') {
		$prefix = $this->option('prefix');
		if ($param == '') {
			//Remove unprefixed entries and remove the prefix to prefixed ones
			//if request prefix is set
			$d = $this->data;
			if ($prefix != '') {
				$d2 = array();
				foreach($d as $k => $v) {
					if (substr($k,0,strlen($prefix)) == $prefix) {
						$d2[substr($k,strlen($prefix)+1)] = $v;	
					}
					if (in_array($k,$this->option('prefixexceptions')) !== false) {
						$d2[$k] = $v;
					}
				}
				$d = $d2;
			}
			return $d;
		}else {
			$fname = $param;
			if ($prefix != '' && in_array($param,$this->option('prefixexceptions')) === false) $fname = $prefix.'_'.$fname;
			$val = '';
			if (isset($this->data[$fname])) $val = $this->data[$fname];
			return $val;
		}
		return false;
	}
	
	/**
	 * Triggers operations events
	 *
	 * @access public
	 * @return void Triggers on+POST['op'] events
	 */
	public function triggerEvents(){
		$op = $this->get('op');
		if ($op && strlen($op) > 3) {
			global $service;
			$service->get('EventHandler')->trigger('request.on'.ucfirst($op),$this->get());
		}
	}
}
?>