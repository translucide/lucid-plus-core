<?php
/**
 * Widget classes
 *
 * Defines widget classes (WidgetData, WidgetModel, WidgetStore and Widget)
 *
 * Oct 30, 2012
 *
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license		See docs/license.txt
 */

global $service;
$service->get('ressource')->get('core/user/group');

/**
 * Model class for Widget data (WidgetData) and Widget (Widget)
 *
 * Usage: $obj = new WidgetModel();
 *
 * @version 0.1
 * @since 0.1
 * @class WidgetModel
 * @package	sys
 * @subpackage display
 * @see WidgetModel
 */
class WidgetModel extends Model {

	/**
	 * PHP 5 Constructor
	 * @return void
	 */
	public function __construct() {
		global $service;
		$this->type = 'widget';
		$this->initVar('widget_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('widget_objid',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('widget_parent',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('widget_parenttype',DATATYPE_STRING);
		$this->initVar('widget_component',DATATYPE_STRING);
		$this->initVar('widget_zone',DATATYPE_STRING);
		$this->initVar('widget_position',DATATYPE_INT);
		$this->initVar('widget_title',DATATYPE_STRING);
		$this->initVar('widget_name',DATATYPE_STRING);
		$this->initVar('widget_tag',DATATYPE_STRING);
		$this->initVar('widget_options',DATATYPE_ARRAY);
		$this->initVar('widget_type',DATATYPE_STRING);
		$this->initVar('widget_urltrigger',DATATYPE_STRING);
		$this->initVar('widget_cancelurltrigger',DATATYPE_STRING);
		$this->initVar('widget_requesttrigger',DATATYPE_STRING);
		$this->initVar('widget_sectiontrigger',DATATYPE_INTARRAY);
		$this->initVar('widget_language',DATATYPE_INT);
		$this->initVar('widget_groups',(DB_UUIDS)?DATATYPE_UUIDARRAY:DATATYPE_INTARRAY);
		$this->initVar('widget_site',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('widget_created',DATATYPE_INT);
		$this->initVar('widget_modified',DATATYPE_INT);
		$this->initVar('widget_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
	}
}

class WidgetData extends Data{
	function __construct(){
        parent::__construct('widget');
	}
}

class WidgetStore extends Store{
	public function __construct() {
		parent::__construct('widget');
	}
}

class Widget {

	/**
	 * Stores the widget data
	 *
	 * @private
	 * @var WidgetData $data
	 */
	protected $data;

	/**
	 * Stores widget infos
	 *
	 * @private
	 * @var array $infos
	 */
	protected $infos;

    /**
     * Stores the widget internal config options
     */
    protected $options;

	/**
	 * Widget PHP5 constructor
	 *
	 * @public
	 * @return void Stores widget info
	 */
	public function __construct($widgetData=false){
		$this->setData($widgetData);
		$this->init();
	}

	/**
	 * Gets the widget data
	 *
	 * @public
	 *
	 * @return array The widget data
	 */
	public function getData(){
		return $this->data;
	}

	/**
	 * Sets the widget data
	 *
	 * @public
	 *
	 * @return array The widget data
	 */
	public function setData($data){
		if (is_a($data,'WidgetData')) $this->data = $data;
	}

	/**
	 * Returns an array containing widget informations
	 *
	 * @public
	 * @param string $info The information needed
	 * @return mixed Returns the infos array or just one info if set
	 */
	public function getInfo($info=''){
		if (isset($this->infos[$info])) return $this->infos[$info];
		return $this->infos;
	}

	/**
	 * Sets widget informations
	 *
	 * @protected
	 * @param mixed $info The information
	 * @return void
	 */
	protected function setInfo($info='',$value=''){
		if (is_array($info)) $this->infos = $info;
		else $this->infos[$info] = $value;
	}

	/**
	 * Initialize the widget object mostly by filling the infos array
	 *
	 * @public
	 * @return void
	 */
	public function init(){
		$this->infos = array(
			'type' => '',
			'name' => '',
			'title' => '',
			'description' => '',
			'icon' => '',
			'saveoptions' => array(),
			'copyoptions' => array()
		);
	}

	/**
	 * Renders this widget
	 *
	 * @public
	 * @return array Rendered widget
	 */
	public function render(){}

	/**
	 * Lets the widget file add its own form fields to the widget edit form.
	 *
	 * @public
	 *
	 * @param array $obj One or more objects beeing edited with same objid
	 * @param Form $form The edit form, add form fields & return it back.
	 * @return Form The form to display.
	 */
	public function edit($obj,$form){
		return $form;
	}

	/**
	 * Save handler function called on widget updates
	 *
	 * Most of the time this method will not be implemented as one
	 * should use getOptions and send all widget options to be saved
	 * by the widgetmanager
	 *
	 * @public
	 *
	 * @return bool $success
	 */
	public function save(){
		return true;
	}

	/**
	 * Copy handler function called on object copy
	 *
	 * Functions such as copy/paste in the admin interface will call that method
	 * to recursively copy all of this obj childrens
	 */
	public function copy($obj,$original_id,$new_id){
		return true;
	}

	/**
	 * Returns true or false for apiCall access
	 * @return bool True: has apiCall access
	 */
	public function hasAccess($op){
		return false;
	}
}

class WidgetController extends Controller {
	/**
	 * Hold the data object referring to the current widgets
	 *
	 * @private
	 *
	 * @var Widget $widget
	 */
	private $data;

	/**
	 * Constructor
	 *
	 * @public
	 *
	 * @param int $id
	 * @return void Widget()
	 */
	public function __construct(){

	}

	/**
	 * Loads the current widget
	 *
	 * @public
	 *
	 * @return void load()
	 */
	function load() {
		global $service;
		$widgets = array();
		$widgetStore = new WidgetStore();
		$widgetsCrit = new CriteriaCompo();

		//Load widgets that have to be loaded for the current route(s)
		$selector = new CriteriaCompo();
		$emptyselector = new CriteriaCompo();
		$notselector = new CriteriaCompo();

		$themeselector = new CriteriaCompo();
		$themeselector->add(new Criteria('widget_zone','theme'),'OR');
		$zones = $service->get('Theme')->getInfo()['zones'];
		foreach($zones as $v) {
			$themeselector->add(new Criteria('widget_zone',$v['name']),'OR');
		}

		$route = $service->get('Route')->get();
		if (is_array($route)) {
			$route = $route[0];
		}
		if ($route->getVar('route_controller') != '') {
			if ($route->getVar('route_controller') == 'content') {
				$section = $service->get('Content')->get();
				$selector->add(new Criteria('widget_sectiontrigger','%,'.$section->getVar('content_objid').',%','LIKE'),'OR');
				$sections = explode('/',$section->getVar('content_idpath'));
				foreach($sections as $k => $v) {
					$selector->add(new Criteria('widget_sectiontrigger','%,'.$v.',%','LIKE'),'OR');
				}
			}
		}

		$emptycrit = new CriteriaCompo('widget_sectiontrigger','');
		$emptycrit->add(new Criteria('widget_sectiontrigger',''),'OR');
		$emptycrit->add(new Criteria('widget_sectiontrigger',',,'),'OR');
		$emptyselector->add($emptycrit,'AND');
		$emptyselector->add(new Criteria('widget_urltrigger',""),'AND');

		//Load widgets that should be loaded on current url
		$url = $service->get('Url')->get();
		$url = $url['language'].(($url['path'] != '')?'/'.$url['path']:'');
		if($url != '') {
            $selector->add(new Criteria('widget_urltrigger',"%,".$url.",%",'LIKE'),'OR');
            $selector->add(new Criteria('widget_urltrigger',"%,".substr($url,0,strrpos($url,"/")+1)."*,%",'LIKE'),'OR');
        }

		$notselector->add(new Criteria('widget_cancelurltrigger','%,'.$url.',%','NOT LIKE'),'AND');
		$notselector->add(new Criteria('widget_cancelurltrigger',"%,".substr($url,0,strrpos($url,"/"))."*,%",'NOT LIKE'),'AND');

		$bothcrit = new CriteriaCompo();
		$bothcrit->add($selector,'OR');
		$bothcrit->add($emptyselector,'OR');
		$bothcrit->add($themeselector,'AND');
		$bothcrit->add($notselector,'AND');
		$widgetsCrit->add($bothcrit);

		$widgetsCrit->setOrder('ASC');
		$widgetsCrit->setSort('widget_position');
		$data = $widgetStore->get($widgetsCrit);
		foreach ($data as $k => $v) {
			$widgets[] = $this->toWidget($v);
		}
		return $widgets;
	}

	/**
	 * Converts a WidgetData into its corresponding Widget object
	 *
	 * @public
	 *
	 * @param WidgetData $data
	 * @return Widget The widget object, containing the WidgetData object
	 */
	function toWidget($data) {
		global $service;
		$service->get('Ressource')->get('com/'.strtolower($data->getVar('widget_component')).'/widget/'.strtolower($data->getVar('widget_name')));
		$service->get('Ressource')->get('com/'.strtolower($data->getVar('widget_component')).'/lang/'.$service->get("Language")->getCode().'/'.strtolower($data->getVar('widget_name')));
		$widgetCls = ucfirst($data->getVar('widget_name')).'Widget';
		$widget = new $widgetCls($data);
		return $widget;
	}

	/**
	 * Returns a widget object from name and component information
	 *
	 * @access public
	 * @param String $component Component name to look for widget into.
	 * @param String $name Widget name to look for
	 * @param String $zone (Deprecated) Optional theme zone where the widget will appear
	 */
	function get($component,$name, $zone=''){
		global $service;
		$data = new WidgetData();
		if ($zone) {
			$data->setVar('widget_zone',$zone);
			$service->get('Log')->add("WidgetController::get : The Zone parameter is deprecated. Please update your code accordingly.",__FILE__,__LINE__,E_INFO);
		}
		$service->get('Ressource')->get('com/'.$component.'/widget/'.strtolower($name));
		$service->get('Ressource')->get('com/'.strtolower($component).'/lang/'.$service->get("Language")->getCode().'/'.strtolower($name));
		$widgetCls = ucfirst($name).'Widget';
		if (class_exists($widgetCls)) $widget = new $widgetCls($data);
		else $widget = new Widget($data);
		return $widget;
	}

	/**
	 * Renders a widget
	 *
	 * @public
	 *
	 * @return array The rendered widget
	 */
	public function render($widget){
		global $service;
		$groups = implode('-',$service->get('User')->getGroups());
		$site = $service->get('Site')->getId();
		$lang = $service->get('Language')->getId();
		$data = $widget->getData();

        //Titles should get a span for each word, and also get divs around { and } so we can put text on more than 1 line easily.
        $title = $data->getVar('widget_title');
        if (strpos($title,'{') !== false && strpos($title,'}') !== false) {
            $title = str_replace(array('{','}'),array('<div>','</div>'),$title);
        }
        $title = '<span class="word">'.str_replace(' ','</span> <span class="word">',$title).'</span>';
        $data->setVar('widget_title', $title);

        $opt = $data->getVar('widget_options');
        if (isset($opt['title'])) {
            $title = $opt['title'];
            if (strpos($title,'{') !== false && strpos($title,'}') !== false) {
                $title = str_replace(array('{','}'),array('<div>','</div>'),$title);
            }
            $title = '<span class="word">'.str_replace(' ','</span> <span class="word">',$title).'</span>';
            $opt['title'] = $title;
            $data->setVar('widget_options', $options);
        }

		$info = $widget->getInfo();
		$expires = intval($data->getVar('widget_options')['cacheduration']);
		if (intval($data->getVar('widget_options')['usecache']) != 1) $expires = 0;
		$cachefilename = DATAROOT.'/cache/widget/'.$info['name'].'_'.
			md5($site.'-'.$lang.'-'.$groups.'-'.$data->getVar('widget_objid'));

		//Check if cache is enabled and not expired.
		if ($expires > 0 && file_exists($cachefilename)
			&& filemtime($cachefilename) > (time()-($expires*60))) {
			$code = unserialize(file_get_contents($cachefilename));
			$service->get('Ressource')->import($code['ressources']);
			$code = $code['code'];
		}else {
			$service->get('Ressource')->buffer('renderwidget');
			$code = $widget->render();
			$ressources = $service->get('Ressource')->export([RESSOURCE_CSS,RESSOURCE_JS],'renderwidget');
			if ($expires > 0) {
				file_put_contents($cachefilename,serialize(array('ressources' => $ressources, 'code' => $code)));
			}
		}
		if ($data->getVar('widget_options')['widgetwidth']
			&& $data->getVar('widget_options')['widgetwidth'] > 0) {
			$cls = 'col-sm-'.$data->getVar('widget_options')['widgetwidth'];
            $width = $data->getVar('widget_options')['widgetwidth'];
		}else {
            $cls = '';
            $width = 12;
        }
		if ($data->getVar('widget_options')['widgetclasses'] != '') {
			$cls .= ' '.$data->getVar('widget_options')['widgetclasses'];
		}
		if (isset($info['contenttype'])) {
			$tag = $info['contenttype'];
		}else {
			if ($data->getVar('widget_options')['widgetcontenttype']) {
				$tag = $data->getVar('widget_options')['widgetcontenttype'];
			}else $tag = 'section';
		}
		return array(
			'type' => $info['type'],
			'name' => $info['name'],
            'class' => $cls,
            'width' => $width,
            'id' => $data->getVar('widget_options')['widgetidattr'],
			'src' => $code
		);
	}

}
