<?php
/**
 * Action Manager handler class
 *
 * Handles actions management
 *
 * August 12, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('Ressource')->get('core/helper/dbitemmanager');

class ActionManager{
	private $options;

    function __construct($opprefix='action'){
        $this->option('callbackprefix',$opprefix);
        $this->option('name','actions');
    }

    /**
     * ActionManager
     *
     * Returns an ItemManager form field to manage actions
     *
     * @access public
     *
     * @param string $baseurl Path to parent widget API
     * @param Data $defobj Parent object
     */
    public function getFormfield($baseurl,$defobj) {
        global $service;
		$infos = $service->get('Ressource')->getInfos('widget');
		$types = array();
		foreach ($infos as $k => $v) {
			if ($v['type'] == 'action') $types[]  = array(
				'title' => $v['title'],
				'value' => $v['name']
			);
		}

		$callbackURL = $baseurl;
        $actionsvar = (isset($this->options['name']))?$this->options['name']:'actions';
        $actions = $defobj->getVar('widget_options')[$actionsvar];
        if ($defobj->getType() == 'data') $actions = $defobj->getVar('data_data')[$actionsvar];
		$field = new ItemmanagerFormField($actionsvar,$actions,array(
			'title'=>(isset($this->options['title']))?$this->options['title']:_ACTIONS,
			'tab'=>(isset($this->options['tab']))?$this->options['tab']:'basic',
			'parent' => $defobj->getVar($defobj->getType().'_objid'),
			'editlabel' => _EDIT,
			'deletelabel' => _DELETE,
			'duplicatelabel' => _DUPLICATE,
			'newlabel' => _NEW,
			'list' => $callbackURL.$this->option('callbackprefix').'list',
			'edit' => $callbackURL.$this->option('callbackprefix').'edit',
			'duplicate' => $callbackURL.$this->option('callbackprefix').'duplicate',
			'save' => $callbackURL.$this->option('callbackprefix').'save',
			'delete' => $callbackURL.$this->option('callbackprefix').'delete',
			'move' => $callbackURL.$this->option('callbackprefix').'move',
			'types' => $types
		));
        return $field;
    }

    public function ls(){
        $itemManager = $this->getItemManager();
        return $itemManager->list();
    }

    public function move(){
        $itemManager = $this->getItemManager();
        return $itemManager->move();
    }

    public function delete(){
        $itemManager = $this->getItemManager();
        return $itemManager->delete();
    }
	public function duplicate(){
		return $this->edit();
	}
    public function edit(){
        global $service;
   		$store = new WidgetStore();
		$request = $service->get('Request')->get();
        $itemManager = $this->getItemManager();
        $op = str_replace($this->option('callbackprefix'),'',$request['op']);
        $ret = ($op == 'duplicate')?$itemManager->duplicate():'';
        $data = $itemManager->getEditObjects($ret);
        if ($request['id'] == 0 && $request['selectedtype']) {
            foreach ($data['data'] as $k => $v) {
                $data['data'][$k]->setVar('widget_name',$request['selectedtype']);
            }
            $store->save($data['data']);
        }
        $defobj = $store->getDefaultObj($data['data']);
        $obj = $data['data'];
        $form = $data['form'];
        $form->option('prefix',$this->option('callbackprefix').'list');
        $widget = $service->get('Ressource')->getRessourceObject('widget',$defobj->getVar('widget_name'));
        if ($widget) $form = $widget->edit($obj,$form);
		if ($this->option('parenttype') != '') $form->add(new HiddenFormfield('widget_parenttype',$this->option('parenttype')));
        $form->add(new SelectstyleFormField('stylesheet',$defobj->getVar('widget_options')['stylesheet'],array(
            'title' => _STYLE,
            'width' => 3,
            'component' => $defobj->getVar('widget_component'),
            'type' => 'widget',
            'name' => $defobj->getVar('widget_name')
        )));
        return $itemManager->edit($form);
    }

    public function save(){
        global $service;
		$store = new WidgetStore();
		$service->get('Request')->option('prefix',$this->option('callbackprefix').'list');
		$request = $service->get('Request')->get();
        $itemManager = $this->getItemManager();
		$obj = $store->getByObjId($request['widget_objid']);
    	$widget = $service->get('Ressource')->getRessourceObject('widget',$store->getDefaultObj($obj)->getVar('widget_name'));
        $infos = $widget->getInfo();
        $infos['saveoptions'][] = 'stylesheet';
        $ret = $itemManager->save((is_array($infos['saveoptions']))?$infos['saveoptions']:array());
        $service->get('Request')->option('prefix','');
        return $ret;
    }

	public function render($parentid){
        global $service;
		$content = '';
		$store = new WidgetStore();
		$crit = new CriteriaCompo();
		$crit->add(new criteria('widget_parent',$parentid));
		if ($this->option('parenttype') != '') {
			$crit->add(new criteria('widget_parenttype',$this->option('parenttype')));
		}
		$objs = $store->get($crit);
		foreach($objs as $v) {
			$widget = $service->get('Ressource')->getRessourceObject('widget',$v->getVar('widget_name'));
			$widget->setData($v);
			$content .= $widget->render();
		}
        return $content;
	}

	public function option($name, $val = null) {
		if (is_null($val)) return $this->options[$name];
		else $this->options[$name] = $val;
	}

    private function getItemManager(){
		$store = new WidgetStore();
		$itemManager = new DBItemManager($store,'action');
		$itemManager->map('widget_type','widget_parent','widget_title','widget_position','widget_options');
		$itemManager->option('opprefix',$this->option('callbackprefix'));
        return $itemManager;
    }

	/**
	 * Call function enables the use of action->list method.
	 *
	 * @access public
	 */
    public function __call($func, $args)
    {
        switch ($func)
        {
            case 'list':
                return $this->ls();
            break;
            default:
                trigger_error("Call to undefined method ".__CLASS__."::$func()", E_USER_ERROR);
            die ();
        }
    }

}
