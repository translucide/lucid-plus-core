<?php
/**
 * Item Manager handler class
 *
 * Handles itemmanager form field ajax operations
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('Ressource')->get('core/data');
$service->get('Ressource')->get('core/display/form/converter/requesttoobject');
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('core/display/form/field');

class DBItemManager extends BaseClass{

	/**
	 * Datatype used by this instance
	 *
	 * Datatype is used when storing Data objects into the data table.
	 *
	 * @access public
	 *
	 * @var string $datatype
	 */
	var $datatype;

	/**
	 * The Data store to use
	 *
	 * @access public
	 *
	 * @var DataStore $store
	 */
	var $store;


	/**
	 * Columns names mapping
	 *
	 * @access public
	 *
	 * @var Array $mapping
	 */
	var $mapping;

	/**
	 * Data source, usually  a copy of $service->get('Request')->get()
	 *
	 * @access public
	 *
	 * @var array $source
	 */
	var $source;

	/**
	 * Various options supported by the DBItemManager
	 *
	 * @access public
	 *
	 * @var array $options Options, such as 'opprefix',...
	 */
	var $options;

	/**
	 * Constructor
	 *
	 * @acces public
	 * @return void
	 */
	function __construct($store = '', $type = ''){
		$this->DBItemManager($store, $type);
	}
	function DBItemManager($store = '', $type = ''){
		global $service;
		if ($type != '') $this->datatype = $type;
		if (is_object($store)) $this->store = $store;
		else $this->store = new DataStore();
		$this->store->setOption('ignorelangs',true);
		$this->mapping = array(
			'type' => 'data_type',
			'parent' => 'data_parent',
			'title' => 'data_title',
			'position' => 'data_position',
			'data' => 'data_data'
		);
		$this->source = $service->get('Request')->get();
		$this->options = array();
	}

	/**
	 * Sets a columns mapping
	 *
	 * @access public
	 * @param string $type Type column
	 * @param string $parent Parent column name
	 * @param string $title Title column name
	 * @param string $position Position column name
	 * @param strong $data Data column name
	 * @return void
	 */
	function map($type,$parent,$title,$position,$data) {
		$this->mapping = array(
			'type' => $type,
			'parent' => $parent,
			'title' => $title,
			'position' => $position,
			'data' => $data
		);
	}

	/**
	 * Handles the listing of data items
	 *
	 * @access public
	 *
	 * @return array $ret An API Call ready to return response array
	 */
	public function ls() {
		global $service;
		$request = $service->get('Request')->get();
		$this->store->setOption('ignorelangs',false);
		$crit = $this->getBaseCrit();
		if ($request['parent'] > 0) $crit->add(new Criteria($this->mapping['parent'],$request['parent']));
		$crit->setSort($this->mapping['position']);
		$obj = $this->store->get($crit);
		$response = array();
		foreach($obj as $v) $response[] = array('title'=> $v->getVar($this->mapping['title']),'id' => $v->getVar($v->getType().'_objid'));
		$ret = array('success'=>1,'data'=>$response);
		return $ret;
	}

	/**
	 * Handles moving an item
	 *
	 * @access public
	 *
	 * @return array $ret An API Call ready to return response array
	 */
	public function move(){
		global $service;
		$request = $service->get('Request')->get();
		$obj = $this->store->getByObjId($request['id']);
		$offset = $request['offset'];
		if ($offset != 1 && $offset != -1) {
			$resp = false;
		}else {
			$oldpos = $obj[0]->getVar($this->mapping['position']);
			$newpos = $obj[0]->getVar($this->mapping['position'])+$offset;
			$parent = $obj[0]->getVar($this->mapping['parent']);
			$crit = $this->getBaseCrit();
			$crit->add(new Criteria($this->mapping['parent'],$parent));
			$crit->add(new Criteria($this->mapping['position'],$newpos));
			$obj2 = $this->store->get($crit);
			if (count($obj2) != 0) {
				foreach ($obj as $k => $v){
					$obj[$k]->setVar($this->mapping['position'],$newpos);
				}
				foreach ($obj2 as $k => $v){
					$obj2[$k]->setVar($this->mapping['position'],$oldpos);
				}
				$this->store->save($obj);
				$this->store->save($obj2);
			}
		}
		if ($resp || $resp == array()) $ret = array('success'=>1, 'message' => _SAVED);
		else $ret = array('success' => 0, 'message' => _SAVEERROR);
		return $ret;
	}

	/**
	 * Handles duplicating an item
	 *
	 * @access public
	 *
	 * @return array $ret An API Call ready to return response array
	 */
	public function duplicate(){
		global $service;
		$request = $service->get('Request')->get();
		$resp = false;
		if ($request['id']) {
			$itemsparent = $request['id'];
			$obj = $this->store->getByObjId($request['id']);
			$nextpos = $this->store->getNext($this->mapping['position'],new Criteria($this->mapping['type'],$this->datatype));
			foreach ($obj as $k => $v) {
				$obj[$k]->setVar($this->mapping['parent'],$request['parent']);
				$obj[$k]->setVar($this->mapping['position'],$nextpos);
			}
			$resp = $this->store->createMultilingual($obj);
			$resp = $resp[0]->getVar($resp[0]->getType().'_objid');
		}
		return $resp;
	}

	/**
	 * Returns an edit form to use with the item manager along with data object.
	 *
	 * @access public
	 *
	 * @return array An array containing 2 members: 'form' and 'data'
	 */
	public function getEditObjects($objid = ''){
		global $service;
		$type = $this->datatype;
		$request = $service->get('Request')->get();
		if ($objid == '') $objid = $request['id'];
		$defaultlang = $service->get('Language')->getDefault();
		$form = new Form('Items', ($type == '')?'itemmanagerform':$this->datatype, 'Edit Fields', $method='POST');
		$form->setVar('formtag',false);
		$form->setVar('layout','vertical');
		if ($objid > 0) {
			$obj = $this->store->getByObjId($objid);
		}else {
			$obj = $this->store->createMultilingual();
			$max = $this->store->getNext($this->mapping['position'],$this->getBaseCrit());
			foreach($obj as $k => $v) {
				if ($type) $obj[$k]->setVar($this->mapping['type'],$this->datatype);
				$obj[$k]->setVar($this->mapping['parent'],$request['parent']);
				$obj[$k]->setVar($this->mapping['position'],$max);
			}
			$this->store->save($obj);
		}
		$defobj = $this->store->getDefaultObj($obj);
		$id = $defobj->getVar($defobj->getType().'_objid');
		$title = $defobj->getVar($this->mapping['title']);
		if ($title == '' && $objid == 0) $title = _NEW;
		$form->add(new HiddenFormField($defobj->getType().'_id',$defobj->getVar($defobj->getType().'_id'),array(
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,$defobj->getType().'_id')
		)));
		if ($type) $form->add(new HiddenFormField($this->mapping['type'],$this->datatype));
		$form->add(new HiddenFormField($this->mapping['parent'],$request['parent']));
		$form->add(new HiddenFormField($defobj->getType().'_objid',$id,array(
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,$defobj->getType().'_objid')
		)));
		$form->add(new TextFormField($this->mapping['title'],$defobj->getVar($this->mapping['title']),array(
			'title'=>_TITLE,
			'width' => 12,
			'length'=>255,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,$this->mapping['title'])
		)));
		return array('form' => $form, 'data' => $obj);
	}

	/**
	 * Creates the ajax response for edit op
	 *
	 * @access public
	 *
	 * @return array $ret An API Call ready to return response array
	 */
	public function edit($form){
		$op = 'save';
		if ($this->option('opprefix')) $op = $this->option('opprefix').$op;
		$form->addSaveButton($op,_SAVE);
		return array('success'=>1,'form'=>$form->renderArray());
	}

	/**
	 * Saves an edit request
	 *
	 * @access public
	 *
	 * @param array $options An array containing the names of the fields to save in the data_data array
	 * @return array $ret An API Call ready to return response array
	 */
	public function save($options){
		$saveRequest = new RequestToObject(($this->store->getType())?$this->store->getType():'data');
		$saveRequest->addOptions($options,$this->mapping['data']);
		$resp = $saveRequest->save();
		$ret = array('success' => 0, 'message' => _SAVEERROR);
		if ($resp || $resp == array()) {
			$ret = array('success'=>1, 'message' => _SAVED);
		}
		return $ret;
	}

	/**
	 * Deletes a data object
	 *
	 * @access public
	 *
	 * @return array $ret An API Call ready to return response array
	 */
	public function delete(){
		global $service;
		$request = $service->get('Request')->get();
		if ($request['id']) {
			$resp = $this->store->delete($request['id']);
		}else {
			$resp = false;
		}
		if ($resp || $resp == array()) $ret = array('success'=>1, 'message' => _SAVED);
		else $ret = array('success' => 0, 'message' => _SAVEERROR);
		return $ret;
	}

	/**
	 * Gets or sets an option
	 *
	 * @access public
	 *
	 * @param string $name Option name
	 * @param mixed $val Option value
	 * @return mixed Option value or nothing in setter mode
	 */
	public function option($name,$val = null) {
		if ($val != null) $this->options[$name] = $val;
		else return $this->options[$name];
	}

	/**
	 * Call function enables the use of itemmanager->list method.
	 *
	 * @access public
	 */
    public function __call($func, $args)
    {
        switch ($func)
        {
            case 'list':
                return $this->ls();
            break;
            default:
                trigger_error("Call to undefined method ".__CLASS__."::$func()", E_USER_ERROR);
            die ();
        }
    }

	private function getBaseCrit(){
		return ($this->datatype)?
			new CriteriaCompo(new Criteria($this->mapping['type'],$this->datatype)):
			new CriteriaCompo();
	}
}
