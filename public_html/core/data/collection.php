<?php
/**
 * A data collection manipulation class
 *
 * Feb 12, 2018
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2018 Translucide
 * @license
 * @since 		0.1
 */
class Collection{
    /**
     * Reference to the data collection we wish to manipulate
     *
     * @access private
     * @var Array $data An array or arrays or an array of Data objects
     **/
    private $options;
    private $data;
    private $originalData;

    public function __construct($data=null){
        if(!is_null($data)) $this->data($data);
        $this->options = array(
            'casesensitive' => true
        );
    }

    /**
     * Gets or sets the data to work on
     *
     * @access public
     * @param array $data
     **/
    public function data($data=null) {
        if(!is_null($data)) {
            //Set both original data and current data.
            $this->data = $this->originalData = $data;
            return $this;
        }else {
            //Reset data back to original data when getting data.
            $ret = $this->data;
            $this->data = $this->originalData;
            return $ret;
        }
    }

    /**
     * Returns current data
     *
     * @access public
     **/
    public function get(){
        return $this->data();
    }

    /**
     * Returns current data count
     *
     * @access public
     * @return int $count
     **/
    public function count($keepData=false){
        $count = count($this->data);
        if ($keepdata == false) $this->data();
        return $count;
    }

    /**
     * Sets the current data
     *
     * @access public
     * @param array $data
     **/
    public function set($data=null){
        return $this->data($data);
    }

    /**
     * Sets/gets an option
     */
    public function option($name,$value=null){
        if (!is_null($value)) {
            $this->options[$name] = $value;
        }
        else return $this->option[$name];
    }

    /**
     * Refreshes the originalData cache
     *
     * @access public
     */
    public function refresh(){
        $this->originalData = $this->data;
    }

    /**
     * Checks if current data is a collection
     *
     * @access public
     * @return bool
     **/
    public function isCollection(){
        return (is_array($this->data) && count($this->data) > 0 && (is_array($this->data[0]) || is_object($this->data[0])));
    }

    /**
     * Gets or sets data in a given column, can also create the column if not existing yet.
     *
     * This method is chainable
     *
     * @access public
     * @param string $var Column name or names if array
     * @param mixed $value Column new value of values if multiple columns
     * @return object $this Reference to this object for method chaining. Use get to retrieve data.
     **/
    public function col($vars, $values = null) {
        if (!$this->isCollection()) return $this;
        $objs = $this->data;

        $r = array();
        $isnull = ($values === null)?true:false;
        if (!is_array($vars)) $vars = array($vars);
        if (!is_array($values)) $values = array($values);
        foreach($objs as $k => $v) {
            foreach ($vars as $kv => $vv){
                $var = $vv;
                $value = $values[$kv];
                if (is_callable($value)) {
                    $value = $value($v);
                }
                if ($isnull) {
                    if (is_object($v)) $r[] = $v->getVar($var);
                    else $r[] = $v[$var];
                }else {
                    if (is_object($v)) $objs[$k]->setVar($var,$value);
                    else $objs[$k][$var] = $value;
                }
            }
        }
        $this->data = ($isnull)?$r:$objs;
        return $this;
    }

    /**
     * Keeps objects matching a specific column value(s)
     *
     * @access public
     * @param string $column
     * @param mixed $value Some values to match col against. $value can be a single value, an array of values, or a closure returning a boolean value
     * @return object $this Reference to this object for method chaining. Use get to retrieve data.
     **/
    public function keep($var,$val) {
        if (!$this->isCollection()) return $this;
        $objs = $this->data;

        $matches = array();
        foreach($objs as $v) {
            if (is_array($v)) {
                if (
                    (is_array($val) && in_array($v[$var],$val) !== false) ||
                    (is_callable($val) && $val($v[$var])) ||
                    (!is_array($val) && !is_callable($val) && ($v[$var] == $val || (!$this->options['casesensitive'] && strtoupper($v[$var]) == strtoupper($val))))
                ) {
                    $matches[] = $v;
                }
            }else {
                if (
                    (is_array($val) && in_array($v->getVar($var),$val) !== false) ||
                    (is_callable($val) && $val($v->getVar($var))) ||
                    (!is_array($val) && !is_callable($val) && ($v->getVar($var) == $val || (!$this->options['casesensitive'] && strtoupper($v->getVar($var)) == strtoupper($val))))
                ) {
                    $matches[] = $v;
                }
            }
        }
        $this->data = $matches;
        return $this;
    }

    /**
     * Filters out objects that match a value in a specific column and keeps remaining objects
     *
     * @access public
     * @param string $column
     * @param mixed $value Some values to match col against. $value can be a single value, an array of values, or a closure returning a boolean value
     * @return object $this Reference to this object for method chaining. Use get to retrieve data.
     **/
    public function discard($var,$val) {
        if (!$this->isCollection()) return $this;
        $objs = $this->data;

        foreach($objs as $k =>  $v) {
            if (is_array($v)) {
                if (
                    (is_array($val) && in_array($v[$var],$val) !== false) ||
                    (is_callable($val) && $val($v[$var])) ||
                    (!is_array($val) && !is_callable($val) && ($v[$var] == $val || (!$this->options['casesensitive'] && strtoupper($v[$var]) == strtoupper($val))))
                ) {
                    unset($objs[$k]);
                }
            }
            else{
                if (
                    (is_array($val) && in_array($v->getVar($var),$val) !== false) ||
                    (is_callable($val) && $val($v->getVar($var))) ||
                    (!is_array($val) && !is_callable($val) && ($v->getVar($var) == $val || (!$this->options['casesensitive'] && strtoupper($v->getVar($var)) == strtoupper($val))))
                ) {
                    unset($objs[$k]);
                }
            }
        }
        $this->data = $objs;
        return $this;
    }

    /**
     * Cast object as another object
     *
     * This method will convert arrays to objects if input data are arrays.
     *
     * @access public
     * @param string $type Output object type
     * @return object $this Reference to this object for method chaining. Use get to retrieve data.
     **/
    public function to($type) {
        if (!$this->isCollection()) return $this;
        $objs = $this->data;

        $type = strtolower($type);
        $dataObjCls = ucfirst(strtolower($type)).'Data';
        foreach($objs as $k => $v) {
            $tmp = new $dataObjCls();

            if(is_object($v)) $vars = $v->get();
            else $vars = $v;

            $vars2 = array();
            foreach ($vars as $kv => $vv) {
                //Also cast cache and options fields...
                if (is_array($vv)) {
                    $vv2 = array();
                    foreach ($vv as $kvv => $vvv) {
                        $vv2[str_replace($v->getType().'_',$type.'_',$kvv)] = $vvv;
                    }
                    $vv = $vv2;
                }
                //Clear ID fields when morphing an object to another type to prevent conflicts.
                if ($kv != $v->getType().'_id' && $kv != $v->getType().'_uuid') {
                    $vars2[str_replace($v->getType().'_',$type.'_',$kv)] = $vv;
                }
                if ($kv == $v->getType().'_id') $vars2[str_replace($v->getType().'_',$type.'_',$kv)] = 0;
                if ($kv == $v->getType().'_uuid') $vars2[str_replace($v->getType().'_',$type.'_',$kv)] = '';
            }
            $tmp->set($vars2);
            $objs[$k] = $tmp;
        }
        $this->data = $objs;
        return $this;
    }

    /**
     * Re-keys the objects collection by a specified variable
     *
     * @access public
     * @param string $var
     * @return object $this Reference to this object for method chaining. Use get to retrieve data.
     **/
    public function key($var){
        if (!$this->isCollection()) return $this;
        $objs = $this->data;

        $new = array();
        foreach ($objs as $k => $v) {
			if (is_object($v)) $new[$v->getVar($var)] = $v;
			else $new[$v[$var]] = $v;
        }
        $this->data = $new;
        return $this;
    }

    /**
     * Gets the nth item in collection, 0 based
     *
     * @access public
     * @param int $index
     * @return object $this Reference to this object for method chaining. Use get to retrieve data.
     **/
    public function eq($index){
        if (!$this->isCollection()) return $this;
        $objs = $this->data;
        $this->data = $objs[$index];
        return $this;
    }

    /**
     * Renames columns
     *
     * @access public
     * @param string $var
     * @return object $this Reference to this object for method chaining. Use get to retrieve data.
     **/
    public function map($map){
        if (!$this->isCollection()) return $this;
        $objs = $this->data;

        foreach ($objs as $k => $v) {
			if (is_object($v)) $vars = $v->get(true);
			else $vars = $v;
            foreach($map as $mk => $mv) {
                $vars[$mv] = $vars[$mk];
                unset($vars[$mk]);
            }
            if (is_object($v)) $objs[$k]->set($vars);
			else $objs[$k] = $vars;
        }
        $this->data = $objs;
        return $this;
    }

    /**
     * Keeps selected columns
     *
     * @access public
     * @param array $cols Columns to keep
     * @return object $this Reference to this object for method chaining. Use get to retrieve data.
     **/
    public function keepCol($cols){
        if (!$this->isCollection()) return $this;
        $objs = $this->data;

        foreach ($objs as $k => $v) {
			if (is_object($v)) $vars = $v->get(true);
			else $vars = $v;
            $new = array();
            foreach($cols as $mk => $mv) {
                $new[$mv] = $vars[$mv];
            }
            if (is_object($v)) $objs[$k]->set($new);
			else $objs[$k] = $new;
        }
        $this->data = $objs;
        return $this;
    }

    /**
     * Removes selected columns
     *
     * @access public
     * @param array $cols Columns to keep
     * @return object $this Reference to this object for method chaining. Use get to retrieve data.
     **/
    public function discardCol($cols){
        if (!$this->isCollection()) return $this;
        $objs = $this->data;

        foreach ($objs as $k => $v) {
			if (is_object($v)) $vars = $v->get(true);
			else $vars = $v;
            foreach($cols as $mk => $mv) {
                unset($vars[$mv]);
            }
            if (is_object($v)) $objs[$k]->set($vars);
			else $objs[$k] = $vars;
        }
        $this->data = $objs;
        return $this;
    }

	/**
	 * Return the columns that are constant (i.e. their value is the same across the whole collection)
	 *
	 * NOTE: This method returns an associative array of column > value pairs
	 *
	 * @access public
	 * @return object $this Reference to this object for method chaining. Use get to retrieve data.
	 **/
	public function same() {
        if (!$this->isCollection()) return $this;
        $objs = $this->data;

        if (is_object($objs[0])) $vars = $objs[0]->getVars();
        else $vars = $objs[0];

        foreach ($vars as $k => $v) {
            $data = $this->col($v)->get();

            //Case sensitive
    		if ($this->options['casesensitive'] && count(array_unique($data)) !== 1) {
    			unset($vars[$k]);
    		}

            //Case insensitive
    		if (!$this->options['casesensitive'] && count(array_intersect_key($data,array_unique(array_map("StrToLower",$data)))) !== 1) {
    			unset($vars[$k]);
    		}

            $this->data = $objs;
    	}
        $this->data = $vars;
        return $this;
	}

    /**
     * Converts objects to arrays
     *
     * @return object $this Reference to this object for method chaining. Use get to retrieve data.
     **/
    public function asArray() {
        if (!$this->isCollection()) return $this;
        $objs = $this->data;

        foreach ($objs as $k => $v) {
			if (is_object($v)) $v = $v->get(true);
			$objs[$k] = $v;
        }
        $this->data = $objs;
        return $this;
    }

    /**
     * Extends rows with current dataset values when a match is made
     *
     * @param array $data A dataset
     * @param mixed $match Can be any of the following :
     *      - A string representing a single column name present in both datasets
     *      - A string in the form of Origdatacolumnname:newdatacolumnname indicating both columns should match
     *      - An array with 2 members, 0=> origcolumnname, 1=> newcolumnname
     *      - A closure returning a boolean value: eg. function($origrow,$newrow) { if ... return true;}
     * @return object $this Reference to this object for method chaining. Use get to retrieve data
     */
    public function extend($data,$match) {
        if (!$this->isCollection()) return $this;
        $objs = $this->data;
        $iscallable = is_callable($match);
        $isarray = false;
        if (!$iscallable) {
            $isarray = (strstr($match,':') !== false || is_array($match))?true:false;
            if (strstr($match,':') !== false) {
                $match = explode(':',$match);
            }
        }
        foreach ($objs as $k => $v) {
			if (is_object($v)) $vars = $v->get(true);
			else $vars = $v;

            foreach($data as $dk => $dv) {
                if (is_object($dv)) $dvars = $dv->get(true);
    			else $dvars = $dv;
                if (!$iscallable && !$isarray && isset($dvars[$match]) && isset($vars[$match]) &&
                    ($dvars[$match] == $vars[$match] || (!$this->options['casesensitive'] && strtoupper($dvars[$match]) == strtoupper($vars[$match])))){
                    $vars = array_merge($vars,$dvars);
                }
                if ($isarray &&
                    ($dvars[$match[1]] == $vars[$match[0]] || (!$this->options['casesensitive'] && strtoupper($dvars[$match[1]]) == strtoupper($vars[$match[0]])))) {
                    $vars = array_merge($vars,$dvars);
                }
                if ($iscallable && $match($vars,$dvars)) {
                    $vars = array_merge($vars,$dvars);
                }
            }
            if (is_object($v)) $objs[$k]->set($vars);
			else $objs[$k] = $vars;
        }
        $this->data = $objs;
        return $this;
    }


    /**
     * Expands a rows with current dataset values when a match is made
     * creating a sub-row (or expanded row)
     *
     * @param string $dest A column name to use to extend object when a match is found
     * @param array $data A dataset
     * @param mixed $match Can be any of the following :
     *      - A string representing a single column name present in both datasets
     *      - A string in the form of Origdatacolumnname:newdatacolumnname indicating both columns should match
     *      - An array with 2 members, 0=> origcolumnname, 1=> newcolumnname
     *      - A closure returning a boolean value: eg. function($origrow,$newrow) { if ... return true;}
     * @return object $this Reference to this object for method chaining. Use get to retrieve data
     */
    public function expand($dest, $data, $match) {
        if (!$this->isCollection()) return $this;
        $objs = $this->data;
        $iscallable = is_callable($match);
        $isarray = false;
        if (!$iscallable) {
            $isarray = (strstr($match,':') !== false || is_array($match))?true:false;
            if (strstr($match,':') !== false) {
                $match = explode(':',$match);
            }
        }
        foreach ($objs as $k => $v) {
			if (is_object($v)) $vars = $v->get(true);
			else $vars = $v;

            foreach($data as $dk => $dv) {
                if (is_object($dv)) $dvars = $dv->get(true);
    			else $dvars = $dv;
                if (!$iscallable && !$isarray && isset($dvars[$match]) && isset($vars[$match]) &&
                    ($dvars[$match] == $vars[$match] || (!$this->options['casesensitive'] && strtoupper($dvars[$match]) == strtoupper($vars[$match])))){
                    $vars[$dest][] = $dvars;
                }
                if ($isarray &&
                    ($dvars[$match[1]] == $vars[$match[0]] || (!$this->options['casesensitive'] && strtoupper($dvars[$match[1]]) == strtoupper($vars[$match[0]])))) {
                    $vars[$dest][] = $dvars;
                }
                if ($iscallable && $match($vars,$dvars)) {
                    $vars[$dest][] = $dvars;
                }
            }
            if (is_object($v)) $objs[$k]->set($vars);
			else $objs[$k] = $vars;
        }
        $this->data = $objs;
        return $this;
    }

    /**
     * Executes a closure returning the row vars/values on each row
     *
     * @param $closure
     * @return object $this Reference to this object for method chaining. Use get to retrieve data
     */
    public function each($closure){
        if (!$this->isCollection()) return $this;
        $objs = $this->data;

        foreach ($objs as $k => $v) {
            if (is_object($v)) $vars = $v->get(true);
            else $vars = $v;

            if (is_callable($closure)){
                $vars = $closure($vars);
            }

            if (is_object($v)) $objs[$k]->set($vars);
			else $objs[$k] = $vars;
        }

        $this->data = $objs;
        return $this;
    }
}
