<?php
interface ListInterface{
    public function asObject();
    public function asOptions();
    public function asArray();
}