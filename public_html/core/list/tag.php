<?php
global $service;
$service->get('Ressource')->get('core/list/listinterface');
$service->get('Ressource')->get('core/model/tag');

class TagList implements ListInterface{
	public function asArray(){
		$objs = $this->getObjs();
		$array = array();
		foreach ($objs as $k => $v) {		
			$array[$v->getVar('tag_objid')] = $v->getVar('tag_title');
		}
		return $array;
	}
	
	public function asObject(){
		return $this->getObjs();
	}
	
	public function asOptions(){
		$objs = $this->getObjs();
		$so = array();
		foreach ($objs as $k => $v) {
            $so[] = array(
                'title' => $v->getVar('tag_title'),
                'value' => $v->getVar('tag_objid')
            );
            unset($objs[$k]);
		}
		return $so;
	}
	
	private function getObjs() {
		global $service;
		$store = new TagStore();
		$crit = new CriteriaCompo();
		$crit->setSort('tag_title');
		$crit->setOrder('ASC');
		return $store->get($crit);
	}
}
?>