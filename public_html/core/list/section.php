<?php
global $service;
$service->get('Ressource')->get('core/list/listinterface');

class SectionList implements ListInterface{
	public function asArray(){
		$objs = $this->getObjs();
		$array = array();
		foreach ($objs as $k => $v) {		
			$array[$v->getVar('content_objid')] = $v->getVar('content_title');
		}
		return $array;
	}
	
	public function asObject(){
		return $this->getObjs();
	}
	
	public function asOptions(){
		$objs = $this->getObjs();
		$so = array([
				'title' => _ALL,
				'value' => ''
			 ]);
		foreach ($objs as $k => $v) {
			if ($v->getVar('content_level') == 1) {
				$so[] = array(
					'title' => $v->getVar('content_title'),
					'value' => $v->getVar('content_objid')
				);
				unset($objs[$k]);
				$so = array_merge($so,$this->getChilds($objs,$v->getVar('content_objid')));
				break;
			}
		}
		return $so;
	}
	
	private function getObjs() {
		global $service;
		$store = new ContentStore();
		$crit = new CriteriaCompo();
		$crit->add(new Criteria('content_type','section'));
		$crit->setSort('content_position');
		$crit->setOrder('ASC');
		return $store->get($crit);
	}
	
	private function getChilds($objs,$parent,$ret=array()){
		foreach ($objs as $k => $v) {
			if ($v->getVar('content_parent') == $parent) {
				$ret[] = array(
					'title'=> str_repeat('&nbsp;',($v->getVar('content_level')-1)*3).$v->getVar('content_title'),
					'value' => $v->getVar('content_objid')					
				);
				unset($objs[$k]);
				$ret = $this->getChilds($v->getVar('content_objid'),$objs,$ret);
			}
		}
		return $ret;
	}    
}
?>