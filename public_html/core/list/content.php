<?php
global $service;
$service->get('Ressource')->get('core/list/listinterface');

class ContentList implements ListInterface{
	public function asArray(){
		$objs = $this->getObjs();
		$array = array();
		foreach ($objs as $k => $v) {		
			$array[$v->getVar('content_objid')] = $v->getVar('content_title');
		}
		return $array;
	}
	
	public function asObject(){
		return $this->getObjs();
	}
	
	public function asOptions(){
		$objs = $this->getObjs();
		foreach ($objs as $k => $v) {
            $so[] = array(
                'title' => $v->getVar('content_title'),
                'value' => $v->getVar('content_objid')
            );
            unset($objs[$k]);
		}
		return $so;
	}
	
	private function getObjs() {
		global $service;
		$store = new ContentStore();
		$crit = new CriteriaCompo();
		$crit->add(new Criteria('content_type','section','!='));
		$crit->setSort('content_position');
		$crit->setOrder('ASC');
		return $store->get($crit);
	}    
}
?>