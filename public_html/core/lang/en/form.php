<?php
/**
 * System constants
 *
 * Common form language constants available system-wide :
 * - Tabs titles
 * - Actions/button labels
 * - Field labels
 * - Field options
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */



/**
 * Labels
 */
define("_TITLE","Title");
define("_TYPE","Type");
define("_LINK","Link");
define("_PICTURE","Picture");
define("_IMAGE","Image");
define("_VIDEO","Video");
define("_THUMBNAIL","Thumbnail");
define("_SUBTITLE","Subtitle");
define("_CONTENT","Content");
define("_DRAFT","Draft");
define("_EXERPT","Exerpt");
define("_INTRO","Introduction");
define("_VISIBILITY","Visible for these group(s)");
define("_WIDTH","Width");
define("_HEIGHT","Height");
define("_SIZE","Size");
define("_POSITION","Position");
define("_SPEED","Display speed");
define("_EMAIL","Email");
define("_SUBJECT","Sujet");
define("_MESSAGE","Message");
define("_NAME","Name");
define("_HEADER","Header");
define("_FOOTER","Footer");
define("_BODY","Body");
define("_PAGETITLE","Page title");
define("_DESCRIPTION","Page description");
define("_KEYWORDS","Keywords");
define("_ICON","Icon");
define("_TEXT","Text");
define("_STYLE","Style");
define("_SUBSTYLE","Sub-Style");
define("_VARIANT","Variant");
define("_LIGHT","Light");
define("_DARK","Dark");
define("_DISPLAYTITLE","Display title ?");
define("_CACHE","Cache ?");
define('_CACHEDURATION','Cache duration');
define("_PAGEURL",'Page URL');
define("_OUTRO","Outro");
define("_PERLINE","Items per line");
define("_PERPAGE","Items per page");
define("_LISTITEMS","List items");
define('_ACTIONS','Actions');
define('_FORM','Form');
define('_URL','URL');
define('_BUTTONTEXT','Button text');
define('_SELECTED','Selected?');
define('_BGEFFECT','BG Effect');
define('_ANIMATION','Animation');
define('_LISTTITLE','List title');
define('_ITEMS','Items');
define('_ICONS','Icons');
define('_TITLES','Titles');
define('_HTML','HTML Source');
define('_JS','Javascript source');
define('_CSS','CSS source');
define('_CLASS','Class');
define('_COLOR','Color');
define('_BGCOLOR','BG Color');
define('_REQUIRED','Requied?');
define('_VALIDATE','Validate');
define('_NUMBER','Number');
define('_PHONENUMBER','Phone number');
define('_DISABLED','Disabled?');
define('_DEVICEVISIBILITY','Device visibility');
define('_TAGS','Tags');
define('_TEMPLATE','Template');
define('_SECTION','Section');
define('_ALLOWEDFILETYPES','Allowed file types');
define("_MARGINS","Margins");
define("_TEXTALIGN","Text alignment");
define("_PADDINGS","Padding");
define("_PADDINGSTABLET","Tablet paddings");
define("_PADDINGSMOBILE","Mobile paddings");
define("_GRADIENT","Gradient");
define("_GRADIENTEDITOR","Gradient Editor");
define("_GRADIENT_TIP","Click on the small link icon to open the gradient editor. Then only copy paste the w3c compatible gradient line, without the 'background:' at the beginning.");
define("_GRADIENTTABLET","Tablets Gradient");
define("_GRADIENTMOBILE","Mobile Gradient");
define("_BGIMAGETABLET","Tablet's background image");
define("_BGIMAGEMOBILE","Mobile's background image");
define("_BGPOSTABLET","Tablet's BG position");
define("_BGPOSMOBILE","Mobile's BG position");

/**
 *
 * Options / choices
 */
define("_YES","Yes");
define("_NO","No");

define("_LEFT","Left");
define("_RIGHT","Right");
define("_TOP","Top");
define("_BOTTOM","Bottom");
define("_CENTER","Center");

define("_TINY","Tiny");
define("_SMALL","Small");
define("_MEDIUM","Medium");
define("_LARGE","Large");
define("_ENLARGED","Enlarged");

define("_ALL","All");
define("_CHOOSE","Select an option");

define("_DEFAULT","Default");
define("_NONE","Default");

define('_MOBILEVISIBLE','Mobile');
define('_TABLETVISIBLE','Tablets');
define('_DESKTOPVISIBLE','Desktop');
define('_MOBILEHIDDEN','Mobile hidden');
define('_TABLETHIDDEN','Tablets hidden');
define('_DESKTOPHIDDEN','Desktop hidden');

/**
 * Placeholders
 */
define('_CACHEDURATION_PLACEHOLDER','Duration in minutes');

/**
 * TABS
 **/
define("_BASIC","Basic options");
define("_ADVANCED","Advanced");
define("_HEADERFOOTER","Header/Footer");
define("_BASICOPTIONS","Basic options");
define("_SEOOPTIONS","SEO options");
define("_ADVANCEDOPTIONS","Advanced options");
define("_UNUSED",'Unused');

/**
 * Units
 **/
define('_MINUTES','Minutes');
define('_HOURS','Hours');
define('_SECONDS','Seconds');
define('_MILLISECONDS','Milliseconds');
define('_PIXELS','Pixels');
define('_PERCENTS','Percents');
?>
