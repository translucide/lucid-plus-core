<?php
/**
 * System constants
 *
 * Link target constants
 *
 * June 15, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */
define('_TARGET','Link Target');
define('_TARGET_SELF','Current Window');
define('_TARGET_BLANK','New Window');
define('_TARGET_PARENT','Parent Window');
define('_TARGET_TOP','Top Window');
?>