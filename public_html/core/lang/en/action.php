<?php
/**
 * System constants
 *
 * Common action language constants available site wide
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */
define("_LOGIN","Log in");
define("_LOGOUT","Logout");
define("_CANCEL","Cancel");
define("_SAVE","Save");
define("_DELETE","Delete");
define("_EDIT","Edit");
define("_NEW","New");
define("_ADD","Add");
define("_DUPLICATE","Duplicate");
define("_REFRESH","Refresh");
define("_SEND","Send");
define("_OPEN","Open");
define("_SEARCH","Search");
define("_REPLACE","Replace by");
define("_READMORE","Read more");
define("_SHOWMORE","Show more");
define("_SHOWLESS","Show less");
define("_DETAILS","Details");
define("_OK","Ok");
?>