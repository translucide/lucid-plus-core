<?php
define('HTTPERROR_403_TITLE','Forbidden!');
define('HTTPERROR_403_DESC','Forbidden access!');
define('HTTPERROR_403_LINKTEXT','Click here if redirection does not work');

define('HTTPERROR_401_TITLE','Unauthorized access!');
define('HTTPERROR_401_DESC','You are not authorized to access this location. You will be automatically redirected to the login page');
define('HTTPERROR_401_LINKTEXT','Click here if redirection does not work');

define('HTTPERROR_301_TITLE','This page has moved!');
define('HTTPERROR_301_DESC','You are now being redirected to the new location');
define('HTTPERROR_301_LINKTEXT','Click here if redirection does not work');
 ?>
