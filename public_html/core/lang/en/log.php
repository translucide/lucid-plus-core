<?php
define('LOG_FILE_DATAINTEGRITY_DSC','Data integrity');
define('LOG_FILE_DATABASE_DSC','Database queries');
define('LOG_FILE_STATS_DSC','Statistics');
define('LOG_FILE_SECURITY_DSC','Security');
define('LOG_FILE_SYSTEM_DSC','System');
define('LOG_FILE_HTTPERRORS_DSC','HTTP Errors');

define('LOG_SHUTDOWN_NBQUERIES','Number of queries executed : %s for URL %s');
define('LOG_SHUTDOWN_QUERIESTIMER','Total DB queries execution time : %s seconds for URL %s');
define('LOG_SHUTDOWN_PEAKMEM','Peak memory usage : %s KB for URL %s');
define('LOG_SHUTDOWN_ENDMEM','Final memory usage : %s KB for URL %s');
define('LOG_SHUTDOWN_CPUUSERTIME','CPU : User time used : %s seconds for URL %s');
define('LOG_SHUTDOWN_SYSTEMTIME','CPU : System time used : %s seconds for URL %s');
define('LOG_SHUTDOWN_CONTENTTIME','Content zone loading took %s seconds for URL %s');
define('LOG_SHUTDOWN_WIDGETTIME','Widgets loading took %s seconds for URL %s');
define('LOG_SHUTDOWN_LOADTIME','Total load time took %s seconds for URL %s');
define('LOG_SHUTDOWN_BOOTTIME','LucidCMS initialization took %s seconds for URL %s');
define('LOG_SHUTDOWN_OUTPUTRENDERING','Output rendering took %s seconds for URL %s');
define('LOG_SHUTDOWN_DEFAULT','Timer named \'%s\' took %s seconds for URL %s');

define('LOG_DBQUERYTIME','Query executed in %s seconds : %s');
?>