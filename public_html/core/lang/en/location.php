<?php
/**
 * System constants
 *
 * Location constants, available system-wide
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

define("_LOCATION_HOME","Home");
define("_LOCATION_ADMINISTRATION","Administration");
define("_LOCATION_CONTENT","Pages");
define("_LOCATION_MENUS","Menus");
define("_LOCATION_SETTINGS","Settings");
define("_LOCATION_WIDGETS","Widgets");
define("_LOCATION_PROFILE","My Profile");
?>