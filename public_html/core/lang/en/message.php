<?php
/**
 * System constants
 *
 * Common message language constants available system-wide
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */
define('_SAVED','Changes successfully saved.');
define('_SAVERROR','An error occured while saving changes.');
define('_CONFIRMDELETE','Are you sure you want to delete the selected items and it\'s child content (if applicable)? Once deleted those item(s) cannot be recovered.');
define('_INVALIDREQUEST','Invalid request');
define('_FORBIDDENREQUEST','Forbidden request!');
define('_CLICKTOEDIT','No element selected. Click on an item to edit it.');
define('_ACTIVATEJS','ERROR: Form can\'t be shown because you have javascript disabled.');
define('_SITECLOSED','Site closed');
?>