<?php
define('SYSTEM_LOCALE_DATE','Y-m-d');
define('SYSTEM_LOCALE_TIME','H:i:S');
define('SYSTEM_LOCALE_DATE_TEXT','F j, Y');
define('SYSTEM_LOCALE_DATETIME_TEXT','l jS \of F Y h:i:s A');
define('SYSTEM_LOCALE_DATETIME','Y-m-d H:i:S');
define('SYSTEM_LOCALE_MONTHS',serialize(array('January','February','March','April','May','June','July','August','September','October','November','December')));
define('SYSTEM_LOCALE_DAYS',serialize(array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday')));

define('_SECOND','second');
define('_SECONDS','seconds');
define('_MINUTE','minute');
define('_MINUTES','minutes');
define('_HOUR','hour');
define('_HOURS','hours');
define('_DAY','day');
define('_DAYS','days');
define('_WEEK','week');
define('_WEEKS','weeks');
define('_MONTH','month');
define('_MONTHS','months');
define('_YEAR','year');
define('_YEARS','years');
define('_TIMEAGO','%s ago');
define('_NOW','just now');
define('_NEVER','never');
?>