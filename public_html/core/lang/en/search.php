<?php
/**
 * Search string
 */
define('SEARCH_SEARCH','Search');
define('SEARCH_SEARCHAGAIN','Search for');
define('SEARCH_RESULTS','Search results');
define('SEARCH_RESULTS_MATCHING','Search results matching');
define('SEARCH_MATCHS','Found %s results. Showing results %s to %s.');
?>