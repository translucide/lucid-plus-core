<?php
define('ERROR404_TITLE','Not found !');
define('ERROR404_MSG','The page you are looking for was not found. We are sorry for the inconvenience.');
define('ERROR404_GETBACK','<a href="javascript:history.go(-1);">Click here to get back where you were.</a>');
?>