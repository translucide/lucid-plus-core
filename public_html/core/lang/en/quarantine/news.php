<?php
define('NEWS_ADMIN_DASHBOARDTITLE','News');

define('NEWS_ADMIN_DASHBOARD_NEWS','Articles');
define('NEWS_ADMIN_DASHBOARD_ADDNEWS','Create article');
define('NEWS_ADMIN_DASHBOARD_STATS','Statistics');
define('NEWS_ADMIN_DASHBOARD_PREFERENCES','Preferences');
?>