<?php
define('PRODUCT_CONTENT_TYPE','Product');
define('PRODUCT_CONTENT_TYPE_DESC','Adds a product to the current section');
define('PRODUCT_MAINPICTURE','Main picture');
define('PRODUCT_PICTURE_2','Photo #2');
define('PRODUCT_PICTURE_3','Photo #3');
define('PRODUCT_PICTURE_4','Photo #4');
define('PRODUCT_SUMMARY','Summary');
define('PRODUCT_MODELS','Models');
define('PRODUCT_MODEL','Model');
define('PRODUCT_MODELITEMS','Model items');
define('PRODUCT_MODELITEM','Model item');
define('PRODUCT_MODEL_MAINPIC','Main model picture');
define('PRODUCT_MODEL_OTHERPICS','Other model pictures');
define('PRODUCT_MODEL_TECHSPECS','Technical specifications PDF');
define('PRODUCT_MODELITEM_TITLE','Item title');
define('PRODUCT_MODELITEM_CONTENT','Item content');
define('PRODUCT_MODEL_ISADDON','Display this model as a suggested product');
define('PRODUCT_MODELITEM_TECHSPECS','Option\'s technical specifications');
define('PRODUCT_SELECTMODEL','Select a model');
define('PRODUCT_TECHSPECS','Technical specifications');
define('PRODUCT_TECHSPECSDOWNLOAD','Download %s technical specifications');
define('PRODUCT_ADDONSELECT','Complete your assembly');
?>
