<?php
define('SHARER_SHARETHISPAGE','Share this page');
define('SHARER_SHARETHISPAGEVIAEMAIL','Share this page via email');
define('SHARER_SHARETHISPAGEVIAFACEBOOK','Share this page via email');
define('SHARER_SHARETHISPAGEVIAEMAIL_EMAIL','Your friend\'s email address');
define('SHARER_SHARETHISPAGEVIAEMAIL_NAME','Your name');
define('SHARER_SHARETHISPAGEVIAEMAIL_SUBJECT','Mail subject');
define('SHARER_SHARETHISPAGEVIAEMAIL_MESSAGE','Message');
define('SHARER_SHARETHISPAGEVIAEMAIL_SENT','Message sent !');
define('SHARER_SHARETHISPAGEVIAEMAIL_NOTSENT','An error occured while sending the message.');
define('SHARER_SHARETHISPAGEVIAEMAIL_FROM','You have a message from');

?>