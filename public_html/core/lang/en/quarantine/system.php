<?php
/**
 * System generic constants
 */

define('_DELETETIP','Delete selected element');
define('_CLICKTOEDIT','Select an item to edit it.');
define('_DEFAULT','Default');

define('_FORM_VALIDATE_NOTEMPTY','This field is required.');
define('_FORM_VALIDATE_EMAIL','Valid email address required.');
define('_FORM_VALIDATE_NUMBER','Number required.');
define('_FORM_VALIDATE_BETWEEN','Number between %s and %s required.');
define('_FORM_VALIDATE_PATTERN','Valid value required.');

/**
 * Admin dashboard constants
 */
define('SYSTEM_DASHBOARD','Tableau de bord');
define('SYSTEM_DASHBOARD_WIDGETS','Widgets');
define('SYSTEM_DASHBOARD_CONTENT','Content');
define('SYSTEM_DASHBOARD_CONTENTPAGES','Pages');
define('SYSTEM_DASHBOARD_COMMENTS','Comments');
define('SYSTEM_DASHBOARD_PREFERENCES','Preferences');
define('SYSTEM_DASHBOARD_MODULES','Modules');
define('SYSTEM_DASHBOARD_GROUPS','Groups');
define('SYSTEM_DASHBOARD_USERS','Users');





/**
 * System widgets section
 */
define('SYSTEM_WIDGETS','Widgets Manager');
define('SYSTEM_WIDGETS_NEWWIDGET','Create a new widget');
define('SYSTEM_WIDGETS_MANAGE','Manage Widgets');
define('SYSTEM_WIDGETS_SELECTWIDGETTYPE','Choose the type of widget you want to create');
define('SYSTEM_WIDGETS_EDIT','Edit widget');
define('SYSTEM_WIDGETS_CANTEDITZONES','Only widgets can be edited. Widget zones (folders) as you selected are not editable.');
define('SYSTEM_WIDGETS_CANCELURLTRIGGER','Hide on URLs');
define('SYSTEM_WIDGETS_SECTIONTRIGGER','Display on section loaded');
define('SYSTEM_WIDGETS_URLTRIGGER','Display on page url');

define('_CACHEDURATION_PLACEHOLDER','In minutes');
define('SYSTEM_WIDGETS_TAG','Tag');



/**
 * Bloc types definitions
 */
define('SYSTEM_CUSTOMWIDGET_TITLE','Custom content');
define('SYSTEM_CUSTOMWIDGET_LINKTITLE','Title link');
define('SYSTEM_CUSTOMWIDGET_DESCRIPTION','Generic content type. Uses an HTML editor to let you put content in place.');
define('SYSTEM_USERMENUWIDGET_TITLE','User menu');
define('SYSTEM_USERMENUWIDGET_DESCRIPTION','User menu that adapts itself to the user login status.');
define('SYSTEM_MENUWIDGET_TITLE','Generic menu');
define('SYSTEM_MENUWIDGET_DESCRIPTION','A generic menu widget. Lets you build a custom menu.');
define('SYSTEM_ADMINMENUWIDGET_TITLE','Administration area menu');
define('SYSTEM_ADMINMENUWIDGET_DESCRIPTION','An administrative area menu widget.');
define('SYSTEM_MAINMENUWIDGET_TITLE','Main site menu');
define('SYSTEM_MAINMENUWIDGET_DESCRIPTION','Adds a main menu to the website pages.');

/**
 * System content manager
 */
define('_LOCATION_CONTENT','Content Manager');
define('SYSTEM_ADMIN_CONTENT_NOTFOUND','No content handler found for this type of content');
define('SYSTEM_CONTENT_TYPE_CONTENT','Content');
define('SYSTEM_CONTENT_TYPE_CONTENT_DESC','Generic content type. Uses an HTML editor to let you put content in place.');
define('SYSTEM_CONTENT_TYPE_CUSTOM','Custom HTML Content');
define('SYSTEM_CONTENT_TYPE_CUSTOM_DESC','Custom content lets you create a custom form for every editable part in custom HTML & CSS content');

/*
 * System Sections
 */
define('SYSTEM_ADMIN_SECTION_ADDFOLDERTIP','Add new section or new content to the tree.');
define('SYSTEM_ADMIN_SECTION_EDITTIP','Change selected section or content title');
define('SYSTEM_ADMIN_SECTION_REFRESHTIP','Reload tree items');
define('_PAGEURL','Url');
define('_PAGEURLPATH','Url Path');
define('SYSTEM_ADMIN_SECTION_WIDGETS','Widgets');
define('SYSTEM_ADMIN_SECTION_HASLEVELS','Display sub sections on this section page ?');
define('SYSTEM_ADMIN_SECTION_HASLEVELS_INLINE','Show sub sections as part of this page');
define('SYSTEM_ADMIN_SECTION_HASLEVELS_LINKS','Only link to sub sections');
define('SYSTEM_ADMIN_SECTION_HASLEVELS_NOSUB','Do not load and display sub sections');
define('SYSTEM_ADMIN_SECTION_CONTENTMODE','Content display mode');
define('SYSTEM_ADMIN_SECTION_CONTENTMODE_0','Only show links to content');
define('SYSTEM_ADMIN_SECTION_CONTENTMODE_1','Only show links to content with content exerpt');
define('SYSTEM_ADMIN_SECTION_CONTENTMODE_2','Render content in this section page (inline mode)');

/**
 * User groups
 */
define('GROUP_ADMIN','Administrators');
define('GROUP_REGISTERED','Registered users');
define('GROUP_ANONYMOUS','Anonymous');



/**
 * Search string
 */
define('SEARCH_SEARCH','Search');
define('SEARCH_SEARCHAGAIN','Search for');
define('SEARCH_RESULTS','Search results');
define('SEARCH_RESULTS_MATCHING','Search results matching');
define('SEARCH_MATCHS','Found %s results. Showing results %s to %s.');

/**
 * Locale settings
 */
define('SYSTEM_LOCALE','en_CA');
define('SYSTEM_LOCALE_CHARTSET','UTF-8');
d
/**
 * Forms constants
 */
define('SYSTEM_FORM_ACTIVATEJS','You need to activate javascript to use this form.');


define('SYSTEM_SITECLOSED','Site closed');
?>
