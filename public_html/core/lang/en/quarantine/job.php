<?php
define('JOB_CONTENT_TYPE','Job');
define('JOB_CONTENT_TYPE_DESC','Adds a job posting');
define('JOB_CONTENT_JOBTYPE','Job type');
define('JOB_CONTENT_TYPE_PERMANENT','Permanent');
define('JOB_CONTENT_TYPE_TEMPORARY','Temporary');
define('JOB_CONTENT_TYPE_SEASONAL','Seasonal');
define('JOB_CONTENT_TYPE_MANDATE','Fixed duration mandate');
define('JOB_CONTENT_SCHEDULETYPE','Type of schedule');
define('JOB_CONTENT_SCHEDULETYPE_FULLTIME','Full time');
define('JOB_CONTENT_SCHEDULETYPE_PARTTIME','Part time');
define('JOB_CONTENT_SCHEDULETYPE_ONCALL','On call');
define('JOB_CONTENT_DESCRIPTION','Job description');
define('JOB_CONTENT_RESPONSIBILITIES','Job description');
define('JOB_CONTENT_PREREQUISITES','Pre-requisites');
define('JOB_CONTENT_START','Job start date');
define('JOB_CONTENT_END','Job end date');