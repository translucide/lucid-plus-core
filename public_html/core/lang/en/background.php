<?php
define('_BACKGROUNDS','Background');
define('_BACKGROUND_IMAGE','Image');
define('_BACKGROUND_SIZE','Size');
define('_BACKGROUND_REPEAT','Repeat');
define('_BACKGROUND_POSITION','Position');

//Deprecated:
define("_BGIMAGE","Background image");
define("_BGPOS","Background position");
