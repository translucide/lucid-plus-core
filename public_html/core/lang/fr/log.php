<?php
define('LOG_FILE_DATAINTEGRITY_DSC','Intégrité des données');
define('LOG_FILE_DATABASE_DSC','Requêtes en Base de Données');
define('LOG_FILE_STATS_DSC','Statistiques');
define('LOG_FILE_SECURITY_DSC','Sécurité');
define('LOG_FILE_SYSTEM_DSC','Système');
define('LOG_FILE_HTTPERRORS_DSC','Erreurs HTTP');

define('LOG_SHUTDOWN_NBQUERIES','Nombre de requêtes exécutées sur cette page : %s pour l\'URL %s');
define('LOG_SHUTDOWN_QUERIESTIMER','Temps total d\'exécution des requêtes en DB : %s secondes pour l\'URL %s');
define('LOG_SHUTDOWN_PEAKMEM','Usage maximal de la mémoire : %s KB pour l\'URL %s');
define('LOG_SHUTDOWN_ENDMEM','Usage final de la mémoire : %s KB pour l\'URL %s');
define('LOG_SHUTDOWN_CPUUSERTIME','CPU : Temps utilisateur utilisé : %s secondes pour l\'URL %s');
define('LOG_SHUTDOWN_SYSTEMTIME','CPU : Temps système utilisé : %s secondes pour l\'URL %s');
define('LOG_SHUTDOWN_CONTENTTIME','Chargement de la zone de contenu : %s secondes pour \'URL %s');
define('LOG_SHUTDOWN_WIDGETTIME','Chargement des blocs : %s secondes pour l\'URL %s');
define('LOG_SHUTDOWN_LOADTIME','Temps total de chargement : %s secondes pour l\'URL %s');
define('LOG_SHUTDOWN_BOOTTIME','Initialisation de LucidCMS : %s secondes pour l\'URL %s');
define('LOG_SHUTDOWN_OUTPUTRENDERING','Temps de génération de l\'output : %s secondes pour l\'URL %s');
define('LOG_SHUTDOWN_DEFAULT','Le compteur nommé \'%s\' à pris %s seconds à compléter sa tâche pour l\'URL %s');

define('LOG_DBQUERYTIME','Requête exécutée en %s secondes : %s');
?>