<?php
define('ERROR404_TITLE','Introuvable !');
define('ERROR404_MSG','La page demandée n\'a pu être trouvée. Nous nous excusons du désagrément.');
define('ERROR404_GETBACK','<a href="javascript:history.go(-1);">Cliquez ici pour retourner à la page précédente.</a>');
?>