<?php
define('SYSTEM_LOCALE_DATE','%Y-%m-%d');
define('SYSTEM_LOCALE_TIME','%H:%M:%S');
define('SYSTEM_LOCALE_TEXTDATE','%A, %B %e %Y');
define('SYSTEM_LOCALE_MONTHS',serialize(array('Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Ao&ucirc;t','Septembre','Octobre','Novembre','Décembre')));
define('SYSTEM_LOCALE_DAYS',serialize(array('Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi')));

define('_SECOND','seconde');
define('_SECONDS','secondes');
define('_MINUTE','minute');
define('_MINUTES','minutes');
define('_HOUR','heure');
define('_HOURS','heures');
define('_DAY','jour');
define('_DAYS','jours');
define('_WEEK','semaine');
define('_WEEKS','semaines');
define('_MONTH','mois');
define('_MONTHS','mois');
define('_YEAR','an');
define('_YEARS','ans');
define('_TIMEAGO','Il y a %s');
define('_NOW','maintenant');
define('_NEVER','jamais');
?>