<?php
/**
 * System constants
 *
 * Common action language constants available site wide
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */
define("_LOGOUT","Connexion");
define("_LOGOUT","Déconnexion");
define("_CANCEL","Annuler");
define("_SAVE","Enregistrer");
define("_DELETE","Supprimer");
define("_EDIT","Modifier");
define("_NEW","Nouveau");
define("_ADD","Ajouter");
define("_DUPLICATE","Dupliquer");
define("_REFRESH","Actualiser");
define("_SEND","Envoyer");
define("_OPEN","Ouvrir");
define("_SEARCH","Rechercher");
define("_REPLACE","Remplacer par");
define("_READMORE","Lire la suite");
define("_DETAILS","Détails");
define("_OK","Ok");
