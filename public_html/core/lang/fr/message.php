<?php
/**
 * System constants
 *
 * Common message language constants available system-wide
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */
define('_SAVED','Changements sauvegardés.');
define('_SAVERROR','Une erreur est survenue lors de la sauvegarde des données.');
define('_CONFIRMDELETE','Êtes-vous certain de vouloir supprimer le(s) item(s) sélectionné(s) ainsi que leur contenu (si applicable)? Cette opération est irréversible.');
define('_INVALIDREQUEST','Requête invalide ou permissions insuffisantes');
define('_FORBIDDENREQUEST','Requête non permise.');
define('_CLICKTOEDIT','Aucun élément sélectionné. Cliquez sur un élément afin de le sélectionner.');
define('_ACTIVATEJS','ERREUR: Le formulaire n\'a pu être affiché car Javascript est désactivé.');
define('_SITECLOSED','Site fermé');
?>