<?php
/**
 * Search string
 */
define('SEARCH_SEARCH','Rechercher');
define('SEARCH_SEARCHAGAIN','Recherche de');
define('SEARCH_RESULTS','Résultats de la recherche');
define('SEARCH_RESULTS_MATCHING','Résultats de recherche correspondants à');
define('SEARCH_MATCHS','%s résultats trouvés. Affichage du résultat %s à %s.');
?>