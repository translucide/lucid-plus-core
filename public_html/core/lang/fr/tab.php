<?php
/**
 * System constants
 *
 * Common tab titles language constants available site wide
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */
define("_BASICOPTIONS","Général");
define("_SEOOPTIONS","Référencement");
define("_ADVANCEDOPTIONS","Avancé");
?>