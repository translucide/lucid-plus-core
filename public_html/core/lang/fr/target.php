<?php
/**
 * System constants
 *
 * Link target constants
 *
 * June 15, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */
define('_TARGET','Cible du lien');
define('_TARGET_SELF','Fenêtre actuelle');
define('_TARGET_BLANK','Nouvelle fenêtre');
define('_TARGET_PARENT','Fenêtre maître');
?>