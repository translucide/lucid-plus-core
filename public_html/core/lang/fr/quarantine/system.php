<?php
/**
 * System constants
 *
 * Common language constants available site wide
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

/**
 * Common field labels
 */
define("_TITLE","Titre");
define("_SUBTITLE","Sous-titre");
define("_EXERPT","Sommaire");
define("_SUBJECT","Sujet");
define("_MESSAGE","Message");
define("_LINK","Lien");
define("_PICTURE","Image");
define("_THUMBNAIL","Miniature");
define("_CONTENT","Contenu");
define("_DRAFT","Brouillon");
define("_WIDTH","Width");
define("_HEIGHT","Height");
define("_SIZE","Size");
define("_POSITION","Position");
define("_SPEED","Vitesse d'affichage");
define("_EMAIL","Courriel");
define("_NAME","Nom");
define("_HEADER","Entête");
define("_PAGETITLE","Titre de la page");
define("_DESCRIPTION","Description de la page");
define("_KEYWORDS","Mot-clefs");
define("_ICON","Icône");

/**
 * Common tabs titles
 */
define("_BASICOPTIONS","Options de base");
define("_SEOOPTIONS","Référencement");
define("_ADVANCEDOPTIONS","Options avancées");

/**
 * Common choices and options
 */
define("_YES","Oui");
define("_NO","Non");
define("_OK","Ok");

/**
 * Common messages
 */
define("_SAVED","Les modifications ont été sauvegardées avec succès.");
define("_SAVERROR","Une erreur s'est produite lors de l'enregistrement des données.");
define("_CONFIRMDELETE","Êtes-vous certain de vouloir supprimer l'élément sélectionné (ainsi que tout ses sous-éléments si applicable)? Une fois cet élément supprimé, il vous sera impossible de le récupérer.");



define("_DELETETIP","Supprime l'élément sélectionné.");
define("_VISIBILITY","Visible pour ces groupes");
define("_CLICKTOEDIT","Sélectionnez un item pour modifier ce dernier.");
define("_CHOOSE","Sélectionnez une option");
define("_ALL","Tout");
define("_INVALIDREQUEST","Requête invalide");
define("_LEFT","Gauche");
define("_RIGHT","Droite");
define("_TOP","Haut");
define("_BOTTOM","Bas");
define("_CENTER","Centre");
define("_SECOND","seconde");
define("_MINUTE","minute");
define("_HOUR","heure");
define("_DAY","jour");
define("_WEEK","semaine");
define("_MONTH","mois");
define("_YEAR","années");
define("_TIMEAGO","Il y a %s");
define("_NOW","maintenant");
define("_PLURAL","s");
define("_TINY","Miniature");
define("_SMALL","Petit");
define("_MEDIUM","Moyen");
define("_LARGE","Large");
define("_ENLARGED","Agrandissement");
define("_BGIMAGE","Background image");
define("_BGPOS","Background position");
define("_TEXT","Texte");
define("_STYLE","Style");
define("_DEFAULT","Par défaut");

define("_FORM_VALIDATE_NOTEMPTY","Ce champ est requis.");
define("_FORM_VALIDATE_EMAIL","Adresse courriel valide requise.");
define("_FORM_VALIDATE_NUMBER","Nombre requis.");
define("_FORM_VALIDATE_BETWEEN","Nombre entre %s et %s requis.");
define("_FORM_VALIDATE_PATTERN","Valeur valide requise.");
/**
 * Admin dashboard constants
 */
define("SYSTEM_DASHBOARD","Tableau de bord");
define("SYSTEM_DASHBOARD_WIDGETS","Blocs");
define("SYSTEM_DASHBOARD_CONTENT","Contenu");
define("SYSTEM_DASHBOARD_CONTENTPAGES","Pages");
define("SYSTEM_DASHBOARD_COMMENTS","Commentaires");
define("SYSTEM_DASHBOARD_PREFERENCES","Préférences");
define("SYSTEM_DASHBOARD_MODULES","Modules");
define("SYSTEM_DASHBOARD_GROUPS","Groupes");
define("SYSTEM_DASHBOARD_USERS","Utilisateurs");

define("SYSTEM_SETTINGS_CATEGORY_USER","Préférences utilisateur");

/**
 * System widgets section
 */
define("SYSTEM_WIDGETS","Gestionnaire de Blocs");
define("SYSTEM_WIDGETS_MANAGE","Gérer les blocs");
define("SYSTEM_WIDGETS_EDIT","Modifier le bloc");
define("SYSTEM_WIDGETS_NEWWIDGET","Créer un nouveau bloc");
define("SYSTEM_WIDGETS_SELECTWIDGETTYPE","Sélectionnez le type de bloc que vous désirez créer");
define("SYSTEM_WIDGETS_CANTEDITZONES","Seuls les blocs peuvent être modifiés. Les zones de blocs (dossiers) tel que sélectionné ne peuvent être modifiées.");
define("SYSTEM_WIDGETS_CONTROLLERTRIGGER","Afficher selon ce controlleur");
define("SYSTEM_WIDGETS_SECTIONTRIGGER","Afficher au chargement de ces sections");
define("SYSTEM_WIDGETS_URLTRIGGER","Afficher selon l'URL de la page");
define("_DISPLAYTITLE","Afficher le titre du bloc ?");
define("_CACHE","Activer la mise en cache?");
define("_CACHEDURATION","Fréquence de mise à jour du bloc");
define("_CACHEDURATION_PLACEHOLDER","En minutes");
define("SYSTEM_WIDGETS_TAG","Balise");


/**
 * Bloc types definitions
 */
define("SYSTEM_CUSTOMWIDGET_TITLE","Contenu générique");
define("SYSTEM_CUSTOMWIDGET_LINKTITLE","Lien sur le titre");
define("SYSTEM_CUSTOMWIDGET_DESCRIPTION","Contenu inséré manuellement. Ce bloc utilise un éditeur HTML pour vous permettre créer votre contenu.");
define("SYSTEM_USERMENUWIDGET_TITLE","Menu utilisateur");
define("SYSTEM_USERMENUWIDGET_DESCRIPTION","Menu utilisateur qui s'adapte au contexte de connexion de l'usager.");
define("SYSTEM_MENUWIDGET_TITLE","Menu générique");
define("SYSTEM_MENUWIDGET_DESCRIPTION","Permet de créer un menu.");
define("SYSTEM_ADMINMENUWIDGET_TITLE","Menu d'administration du site");
define("SYSTEM_ADMINMENUWIDGET_DESCRIPTION","Un menu pour l'interace d'administration du site.");
define("SYSTEM_MAINMENUWIDGET_TITLE","Main site menu");
define("SYSTEM_MAINMENUWIDGET_DESCRIPTION","Ajoute un menu principal aux pages du site.");

/*
define("SYSTEM_WIDGETS_CLICKTOEDIT","Cliquez sur un item à gauche pour le modifier.");
define("SYSTEM_ADMIN_EDIT_WIDGETS","Edit widget");
define("SYSTEM_ADMIN_EDITTIP_WIDGETS","Modify this widget");
define("SYSTEM_ADMIN_DELETETIP_WIDGETS","Delete this widget");
define("SYSTEM_ADMIN_NEW_WIDGETS","Add widget");
define("SYSTEM_ADMIN_PATH_SYSTEM","System");
define("SYSTEM_ADMIN_PATH_ADMIN","Administration");
define("SYSTEM_ADMIN_PATH_WIDGETS","Manage Widgets");
define("SYSTEM_ADMIN_WIDGETS_COL1","Title");
define("SYSTEM_ADMIN_WIDGETS_COL2","Type");
define("SYSTEM_ADMIN_WIDGETS_COL3","Internal name");
define("SYSTEM_ADMIN_WIDGETS_COL4","Date added");
define("SYSTEM_ADMIN_WIDGETS_COL5","Visible?");
define("SYSTEM_ADMIN_WIDGETS_CONFIRMDELETE","Are you sure you want to delete the selected widget? Once deleted the widget cannot be recovered.");
define("SYSTEM_ADMIN_WIDGETS_DELETETIP","Supprimer le bloc sélectionné");
define("SYSTEM_ADMIN_WIDGETS_CONFIRMDELETE_YES","Yes");
define("SYSTEM_ADMIN_WIDGETS_CONFIRMDELETE_CANCEL","Cancel");
define("SYSTEM_ADMIN_WIDGETS_OPTIONS","Settings");
define("SYSTEM_ADMIN_WIDGETS_OPTIONS_MANAGE","Manage bloc settings");
*/

/**
 * System content manager
 */
define("_LOCATION_CONTENT","Gestionnaire de contenu");
define("SYSTEM_ADMIN_CONTENT_NOTFOUND","Aucun gestionnaire de contenu trouvé pour ce type de contenu!");
define("SYSTEM_CONTENT_TYPE_CONTENT","Contenu");
define("SYSTEM_CONTENT_TYPE_CONTENT_DESC","Contenu générique. Mets à votre disposition un éditeur HTML évolué vous laissant placer le contenu de votre choix de la façon qu'il vous plaît.");
define("SYSTEM_CONTENT_TYPE_CUSTOM","Contenu personnalisé");
define("SYSTEM_CONTENT_TYPE_CUSTOM_DESC","Custom content lets you create a custom form for every editable part in custom HTML & CSS content");

/**
 * System Sections
 */
define("SYSTEM_ADMIN_SECTION_ADDFOLDERTIP","Add new section or new content to the tree.");
define("SYSTEM_ADMIN_SECTION_EDITTIP","Change selected section or content title");
define("SYSTEM_ADMIN_SECTION_REFRESHTIP","Reload tree items");
define("_PAGEURL","Url");
define("_PAGEURLPATH","Url Path");
define("SYSTEM_ADMIN_SECTION_WIDGETS","Widgets");
define("SYSTEM_ADMIN_SECTION_HASLEVELS","Afficher les sous-sections sur cette page ?");
define("SYSTEM_ADMIN_SECTION_HASLEVELS_INLINE","Afficher les sous-sections sur la page de cette section");
define("SYSTEM_ADMIN_SECTION_HASLEVELS_LINKS","N'afficher qu'un lien vers les sous-sections");
define("SYSTEM_ADMIN_SECTION_HASLEVELS_NOSUB","Ne pas afficher les sous-sections de cette section");
define("SYSTEM_ADMIN_SECTION_CONTENTMODE","Mode d'affichage du contenu");
define("SYSTEM_ADMIN_SECTION_CONTENTMODE_0","Liens vers les pages de contenu seulement");
define("SYSTEM_ADMIN_SECTION_CONTENTMODE_1","Liens et sommaire seulement");
define("SYSTEM_ADMIN_SECTION_CONTENTMODE_2","Afficher le rendu complet du contenu dans cette page (mode "insertion")");

/**
 * User groups
 */
define("GROUP_ADMIN","Administrateurs");
define("GROUP_REGISTERED","Utilisateurs");
define("GROUP_ANONYMOUS","Anonymes");

/**
 * User settings
 */
define("SYSTEM_SETTINGS_USER_REGISTRATIONMETHOD","User registration method");
define("SYSTEM_SETTINGS_USER_REGISTRATIONMETHOD_DESC","Choose the way users can register on your site.");
define("SYSTEM_SETTINGS_USER_ALLOWREGISTRATION","Permettre les nouveaux enregistrements ?");
define("SYSTEM_SETTINGS_USER_ALLOWREGISTRATION_DESC","Permettre aux visiteurs de s'enregistrer sur le site en créant un nouveau compte");
/**
 * Search string
 */
define("SEARCH_SEARCH","Recherche");
define("SEARCH_SEARCHAGAIN","Rechercher les termes suivants");
define("SEARCH_RESULTS","Résultats de la recherche");
define("SEARCH_RESULTS_MATCHING","Résultats correspondants &agrave;");
define("SEARCH_MATCHS","%s élément(s) trouvé(s). Éléments %s à %s affichés.");

/**
 * Locale settings
 */
define("SYSTEM_LOCALE","fr_CA");
define("SYSTEM_LOCALE_CHARTSET","UTF-8");
define("SYSTEM_LOCALE_DATE","%Y-%m-%d");
define("SYSTEM_LOCALE_TIME","%H:%M:%S");
define("SYSTEM_LOCALE_TEXTDATE","%A %e %B %Y");
define("SYSTEM_LOCALE_MONTHS",serialize(array("Janvier","F&eacute;vrier","Mars","Avril","Mai","Juin","Juillet","Ao&ucirc;t","Septembre","Octobre","Novembre","D&eacute;cembre")));
define("SYSTEM_LOCALE_DAYS",serialize(array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi")));

/**
 * Forms constants
 */
define("SYSTEM_FORM_ACTIVATEJS","Vous devez activer Javascript afin d'utiliser ce formulaire.");


define("SYSTEM_SITECLOSED","Site fermé");
?>