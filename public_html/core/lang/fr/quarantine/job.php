<?php
define('JOB_CONTENT_TYPE','Emploi');
define('JOB_CONTENT_TYPE_DESC','Ajoute une offre d\'emploi');
define('JOB_CONTENT_JOBTYPE','Type de poste');
define('JOB_CONTENT_TYPE_PERMANENT','Permanent');
define('JOB_CONTENT_TYPE_TEMPORARY','Temporaire');
define('JOB_CONTENT_TYPE_SEASONAL','Saisonnier');
define('JOB_CONTENT_TYPE_MANDATE','Mandat à durée déterminée');
define('JOB_CONTENT_SCHEDULETYPE','Type d\'horaire');
define('JOB_CONTENT_SCHEDULETYPE_FULLTIME','Temps plein');
define('JOB_CONTENT_SCHEDULETYPE_PARTTIME','Temps partiel');
define('JOB_CONTENT_SCHEDULETYPE_ONCALL','Sur appel');
define('JOB_CONTENT_DESCRIPTION','Description du poste');
define('JOB_CONTENT_RESPONSIBILITIES','Responsabilités');
define('JOB_CONTENT_PREREQUISITES','Pré-requis');
define('JOB_CONTENT_START','Date d\'entrée en vigueur');
define('JOB_CONTENT_END','Date de terminaison de l\'emploi');