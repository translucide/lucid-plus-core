<?php
define('SHARER_SHARETHISPAGE','Partager cette page');
define('SHARER_SHARETHISPAGEVIAEMAIL','Partager cette page via courriel');
define('SHARER_SHARETHISPAGEVIAFACEBOOK','Share this page via email');
define('SHARER_SHARETHISPAGEVIAEMAIL_EMAIL','Courriel de votre ami');
define('SHARER_SHARETHISPAGEVIAEMAIL_NAME','Votre nom');
define('SHARER_SHARETHISPAGEVIAEMAIL_SUBJECT','Sujet du courriel');
define('SHARER_SHARETHISPAGEVIAEMAIL_MESSAGE','Message à envoyer');
define('SHARER_SHARETHISPAGEVIAEMAIL_SENT','Message envoyé !');
define('SHARER_SHARETHISPAGEVIAEMAIL_NOTSENT','Erreur lors de l\'envoi du message.');
define('SHARER_SHARETHISPAGEVIAEMAIL_FROM','De la part de');

?>