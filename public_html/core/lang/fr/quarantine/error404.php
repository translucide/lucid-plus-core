<?php
$p = 'ERROR404_';
define($p.'TITLE','Not found !');
define($p.'MSG','The page you are looking for was not found. We are sorry for the inconvenience.');
define($p.'GETBACK','<a href="javascript:history.go(-1);">Click here to get back where you were.</a>');

?>