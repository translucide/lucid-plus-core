<?php
/**
 * System constants
 *
 * Common form language constants available system-wide :
 * - Tabs titles
 * - Actions/button labels
 * - Field labels
 * - Field options
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

/**
 * Labels
 */
define("_TITLE","Titre");
define("_LINK","Lien");
define("_PICTURE","Photo");
define("_IMAGE","Image");
define("_THUMBNAIL","Miniature");
define("_SUBTITLE","Sous-titre");
define("_CONTENT","Contenu");
define("_DRAFT","Brouillon");
define("_EXERPT","Sommaire");
define("_INTRO","Introduction");
define("_VISIBILITY","Visible pour ces groupe(s)");
define("_WIDTH","Largeur");
define("_HEIGHT","Hauteur");
define("_SIZE","Taille");
define("_POSITION","Position");
define("_SPEED","Vitesse");
define("_EMAIL","Adresse courriel");
define("_SUBJECT","Sujet");
define("_MESSAGE","Message");
define("_NAME","Nom");
define("_HEADER","Entête");
define("_FOOTER","Pied de page");
define("_PAGETITLE","Titre de la page");
define("_DESCRIPTION","Description");
define("_KEYWORDS","Mot-clés");
define("_ICON","Icône");
define("_BGIMAGE","Image de fond");
define("_BGPOS","Position de l'image de fond");
define("_TEXT","Texte");
define("_STYLE","Style");
define("_DISPLAYTITLE","Titre visible ?");
define("_CACHE","Cache ?");
define('_CACHEDURATION','Durée du cache');
define("_PAGEURL",'URL de la page');
define("_OUTRO","Outro");
define("_PERLINE","Items par ligne");
define("_PERPAGE","Items par page");
define("_LISTITEMS","Items");
define('_ACTIONS','Actions');
define('_FORM','Formulaire');
define('_URL','URL');
define('_BUTTONTEXT','Texte du bouton');
define('_SELECTED','Sélectionné ?');
define('_BGEFFECT','Effet d\'arrière plan');
define('_ANIMATION','Animation');
define('_LISTTITLE','Titre de l\'élément');
define('_ITEMS','Items');
define('_ICONS','Icônes');
define('_TITLES','Titres');
define('_HTML','Source HTML');
define('_JS','Source Javascript');
define('_CSS','Source CSS');
define('_CLASS','Class');
define('_COLOR','Couleur');
define('_BGCOLOR','Couleur de fond');
define('_REQUIRED','Requis?');
define('_VALIDATE','Valider');
define('_NUMBER','Nombre');
define('_PHONENUMBER','Numéro de téléphone');
define('_DISABLED','Désactivé?');
define('_DEVICEVISIBILITY','Visibilité par appareil');
define('_TAGS','Étiquettes');
define('_TEMPLATE','Modèle');
define('_SECTION','Section');
define('_ALLOWEDFILETYPES','Types de fichiers acceptés');
define("_MARGINS","Marges");
define("_TEXTALIGN","Alignment du texte");
define("_PADDINGS","Padding");
define("_PADDINGSTABLET","Padding sur tablettes");
define("_PADDINGSMOBILE","Padding sur mobile");
define("_GRADIENT","Dégradé");
define("_GRADIENTEDITOR","Éditeur de Dégradé");
define("_GRADIENT_TIP","Cliquez sur la petite icône de lien pour ouvrir l'éditeur de dégradé. Ensuite, copier-coller la ligne compatible W3C, en omettant la partie 'background:' située au début de la ligne.");
define("_GRADIENTTABLET","Tablets Gradient");
define("_GRADIENTMOBILE","Mobile Gradient");
define("_BGIMAGETABLET","Tablet's background image");
define("_BGIMAGEMOBILE","Mobile's background image");
define("_BGPOSTABLET","Tablet: Position BG");
define("_BGPOSMOBILE","Mobile: Position BG");

/**
 *
 * Options / choices
 */
define("_YES","Oui");
define("_NO","Non");

define("_LEFT","Gauche");
define("_RIGHT","Droite");
define("_TOP","Haut");
define("_BOTTOM","Bas");
define("_CENTER","Centre");

define("_TINY","Miniature");
define("_SMALL","Petit");
define("_MEDIUM","Moyen");
define("_LARGE","Large");
define("_ENLARGED","Très grand");

define("_ALL","Tout");
define("_CHOOSE","Choisir...");

define("_DEFAULT","Défaut");
define("_NONE","Aucun");

define('_MOBILEVISIBLE','Cellulaires');
define('_TABLETVISIBLE','Tablettes');
define('_DESKTOPVISIBLE','Ordinateurs');
define('_MOBILEHIDDEN','Cellulaires (non-visible)');
define('_TABLETHIDDEN','Tablettes (non-visible)');
define('_DESKTOPHIDDEN','Ordinateurs (non-visibles)');

/**
 * Placeholders
 */
define('_CACHEDURATION_PLACEHOLDER','Durée du cache (minutes)');

/**
 * TABS
 **/
define("_BASIC","Options de base");
define("_ADVANCED","Options avancées");
define("_HEADERFOOTER","Ent&ecirc;'te/Pied de page");
define("_BASICOPTIONS","Options de base");
define("_SEOOPTIONS","Optimisation SEO");
define("_ADVANCEDOPTIONS","Options avancées");
define("_UNUSED",'Unused');

/**
 * Units
 **/
define('_MINUTES','Minutes');
define('_HOURS','Hours');
define('_SECONDS','Seconds');
define('_MILLISECONDS','Milliseconds');
define('_PIXELS','Pixels');
define('_PERCENTS','Percents');
?>
