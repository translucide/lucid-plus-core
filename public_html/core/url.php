<?php
/**
 * URL Handling routines
 *
 * Utility class to parse and handle URL's
 *
 * Feb 15, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class Url {
	/**
	 *	Holds last parsed URL pieces
	 *
	 *	@access private
	 *
	 *	@var array Holds parsed URL data
	 */
	private $data;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 */
	public function __construct(){
		$this->Url();
	}
	
	/**
	 * Url Constructor
	 *
	 * @access public
	 *
	 * @return void Url Constructor
	 */
	public function Url(){
		$this->data = false;
	}
	
	/**
	 * Parse supplied URL or current URL if none given
	 *
	 * @access private
	 * @param string $url URL to parse
	 * @return array Parsed URL
	 */
	public function parse($url = ''){
		if (!is_array($this->data)) {
			$this->data = $this->parseUrl($url);
		}
		return $this->data;
	}
	
	/**
	 * Parse supplied URL or current page URL if none given. This routine only parses
	 * current site urls, not any URL. For that, use the parseExternalUrl() method
	 *
	 * Returns an array containing :
	 * ['route'] = the full path relative to site root including language, and admin parameters
	 * ['path'] = the Url path part relative to site root, without language and admin parameters.
	 * ['language'] = the language parameter value
	 * ['admin'] = true or false if it seems to be an admin url.
	 * 
	 * @access public
	 * @param string $url URL to parse
	 * @return array Parsed URL or False for external URLs, malformed or unsupported URL types.
	 */
	public function parseUrl($url = ''){
		global $LANGUAGE_PREFIXES;
		$data = false;
		
		//Remove trailing slash in URL for comparison purpose.
		$baseurl = substr(URL,0,-1);
		if ($url == '') {
			$url = $this->find();
		}
		
		//Make $url an http URL if its an https URL, so http and
		//https points to the same ressources in the CMS
		$url = str_replace('https://','http://',$url);
		if (substr($url,0,2) == '//') $url = 'http:'.$url;
		
		$baseurl = str_replace('https://','http://',$baseurl);
		if (substr($baseurl,0,2) == '//') $baseurl = 'http:'.$baseurl;
		
		//Ditch malformed URLs, external URLs and different protocols than
		//the site defined one. Remove directory traversal (../) for security reasons
		if (strstr($url,$baseurl) === false) return false;
		$route = str_replace(array($baseurl,'../'),'',$url);
		if (substr($route,0,1) == '/') $route = substr($route,1);
		if (substr($route,-1,1) == '/') $route = substr($route,0,-1);

		$path = $route;
		$pathpieces = explode('/',$path);
		$language = '';
		$admin = false;
		$langPieces = explode(',',(defined('LANG_AVAILABLE'))?LANG_AVAILABLE:LANG_DEFAULT);

		if(count($pathpieces) > 0) {
			if (in_array($pathpieces[0],$langPieces) !== false) {
				$language = $pathpieces[0];
				unset($pathpieces[0]);
			}
			//Remove language (index 0) and possible "virtual locations" such as /admin
			if (isset($pathpieces[1]) && $pathpieces[1] == 'admin') {
				$admin = true;
				unset($pathpieces[1]);
			}
		}
		$path = implode('/',$pathpieces);
		$ext = (strrpos($path,'.') === false)?'':substr($path,strrpos($path,'.'));
		$data = array(
			'route' => $route,
			'path' => $path,
			'ext' => $ext,
			'language' => $language,
			'admin' => $admin		
        );
        
        //Prevent empty language from being returned.
        if ($data['language'] == '') $data['language'] = (defined('LANG_DEFAULT'))?LANG_DEFAULT:'en';
        
		return $data;
	}
	
	/**
	 * Parses an external URL, not linked to the current site.
	 */
	public function parseExternalUrl($url = '') {
		/**
		 * @todo Implement url::parseExternalUrl($url) method
		 */
	}
	
	
	/**
	 * Finds current URL if none supplied, removes query string.
	 * 
	 * @access public
	 * @return string $url
	 */
	public function find(){
		$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || ($_SERVER['SERVER_PORT'] == 443)) ? "https://" : "http://";
		$url = $protocol.str_replace(array('////','///','//'),'/',$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		if (strpos($url,'?') > 0) $url = substr($url,0,strpos($url,'?'));
		return $url;
	}
	
	/**
	 * An alias for parse function, for current URL only
	 *
	 * @access public
	 *
	 * @return array Url information
	 */
	public function get(){
		return $this->parse();
	}
	
	/**
	 * Returns the output mode this URL should be generating
	 *
	 * @access public
	 *
	 * @return string Output mode constant corresponding to the detected one.
	 */
	public function outputMode(){
		if (!$this->data) $this->parse();
		$realpath = strtolower($this->data['route']);
		if (strpos($realpath,'.js') !== false || strpos($realpath,'/js/') !== false || substr($realpath,-3,3) == '/js') {
			return OUTPUT_MODE_JS;
		}
		if (strpos($realpath,'.css') !== false || strpos($realpath,'/css/') !== false || substr($realpath,-4,4) == '/css') {
			return OUTPUT_MODE_CSS;
		}
		if (strpos($realpath,'.xml') !== false || strpos($realpath,'/xml/') !== false || substr($realpath,-4,4) == '/xml') {
			return OUTPUT_MODE_XML;
		}
		if (strpos($realpath,'.txt') !== false || strpos($realpath,'/txt/') !== false || substr($realpath,-4,4) == '/txt') {
			return OUTPUT_MODE_TEXT;
		}
		if (strpos($realpath,'.json') !== false || strpos($realpath,'/json/') !== false || substr($realpath,-4,4) == '/json') {
			return OUTPUT_MODE_JSON;
		}
		if (strpos($realpath,'.ajax') !== false || strpos($realpath,'/ajax/') !== false || substr($realpath,-4,4) == '/ajax') {
			return OUTPUT_MODE_JSON;
		}
		if (strpos($realpath,'.ajax') !== false || strpos($realpath,'/api/') !== false || substr($realpath,-4,4) == '/api') {
			return OUTPUT_MODE_JSON;
		}
		
		//Current URL did not match any other language, so
		//it should be HTML;
		return OUTPUT_MODE_HTML;
	}
	
	/**
	 * Checks whether the current URL is themable
	 *
	 * For an URL to be themable it has to be set to OUTPUT_LANAGUAGE = html
	 *
	 * @access public
	 *
	 * @return bool Whether the current URL must load the theme around it.
	 */
	public function hasTheme(){
		if ($this->outputMode() == OUTPUT_MODE_HTML) {
			//@Todo : Find a better way to implement this feature.
			if (strpos($this->data['path'],'/notheme/') === false){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks whether the current URL is part of the admin area
	 *
	 * @access public
	 *
	 * @return bool Whether the current URL is part of the admin area.
	 */
	public function isAdmin(){
		if (!$this->data) $this->parse();
		$url = $this->data['route'];
		if (substr($url,0,1) == '/') $url = substr($url,1);
		$pathpieces = explode('/',$url);
		if (in_array('admin',$pathpieces)){
			return true;
		}
		return false;
	}
	
	/**
	 * Converts a string to a url
	 *
	 * @access public
	 *
	 * @param string $str
	 * @return string $url
	 */
	public function toUrl($str){
		$url = strtolower(trim($str));
		
		//Replace all possible accentuated chars by their letter equivalent
		$a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ'); 
		$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'); 
		$url = str_replace($a, $b,$url);
		
		//Change HTML entites to their text equivalent (&raquo; to < symbol for example)
		$url = html_entity_decode($url,ENT_QUOTES,'UTF8');
		
		//Replace spaces & ampersand, newlines and returns chars
		$find = array(' ', '&', '\r\n', '\n', '+',',');
		$url = str_replace ($find, '-', $url);
		
		//Remove the remaining special chars.
		$find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
		$repl = array('', '-', '');
		$url = preg_replace ($find, $repl, $url);
		
		return $url;
	}
}