<?php
/**
 * Event class
 *
 * Manages events
 *
 * June 23, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

interface EventInterface {
	public function set($name='',$function='');
	public function getEvent();
	public function execute();
}

class Event extends BaseClass implements EventInterface{
	/**
	 * Event name
	 *
	 * @access private
	 *
	 * @var string $event
	 */
	private $event;

	/**
	 * Function to call on event trigger
	 *
	 * @access private
	 *
	 * @var object $target
	 */
	private $function;

	/**
	 * Tells whether the event is permanent or not
	 *
	 * @access private
	 *
	 * @var bool $isPermanent;
	 */
	private $isPermanent;

	/**
	 * Event constructor
	 *
	 * @access public
	 *
	 * @param string $name
	 * @param function $function
	 * @param mixed $params
	 * @return void event()
	 */
	public function __construct($name='',$function=''){
		$this->set($name,$function);
		$this->isPermanent = true;
	}

	/**
	 * Sets the event parameters
	 *
	 * @access public
	 *
	 * @param string $name
	 * @param function $function
	 * @param mixed $params
	 * @return void event()
	 */
	public function set($name='',$function=''){
		global $service;
		$log =& $service->get('log');

		if ($name != '') $this->event = $name;
		if (is_callable($function)) $this->function = $function;
		else $log->add('event::set: function parameter is not a lambda function',__FILE__,__LINE__,LOG_ERR);
	}

	/**
	 * Gets the event name
	 *
	 * @access public
	 *
	 * @return string $event Event name
	 */
	public function getEvent() {
		return $this->event;
	}

	/**
	 * Runs the event on event trigger
	 *
	 * @access public
	 *
	 * @return void execute()
	 */
	public function execute($params=null){
		global $service;
		$log =& $service->get('log');
		$fnc = $this->function;
		if (is_callable($fnc)) {
			$ret = $fnc($this->event,$params);
			unset($fnc);
			return $ret;
		}
		else {
			unset($fnc);
			$log->add("Error in eval code passed to event handler for event of type $event. Function could not be executed.", __FILE__,__LINE__,E_WARNING);
			return false;
		}
	}

	/**
	 * Sets the event as permanent
	 *
	 * @access public
	 *
	 * @param bool $bool
	 * @return void setPermanent()
	 */
	public function setPermanent($bool) {
		$this->isPermanent = $bool;
	}

	/**
	 * Whether event is permanent or not
	 *
	 * @access public
	 *
	 * @return bool Is Permanent event
	 */
	public function isPermanent(){
		return $this->isPermanent;
	}
}

/**
 * An event handler class for PHP
 */
class EventHandler extends BaseClass{
	/**
	 * Events stack
	 *
	 * @access private
	 *
	 * @var array $events
	 */
	private $events;

	/**
	 * Event handler constructor
	 *
	 * @access public
	 *
	 * @return void events()
	 */
	public function event(){
		$this->events = array();
	}

	/**
	 * Triggers an event
	 *
	 * Triggers an event. Return true on success
	 *
	 * @access public
	 *
	 * @param string $event Event name
	 * @param mixed $params
	 * @param bool $chain Cumulative mode: Use return value as $params for next event in line
	 * @return bool trigger()
	 */
	public function trigger($event, $params = null, $chain = true){
		$event = strtolower($event);
		global $service;
		$log =& $service->get('log');
		if ($chain == false) $ret = array();
		else $ret = $params;

        //Load events
		$components = $service->get('Ressource')->find('com');
		foreach($components as $k => $v) {
			$service->get('Ressource')->get('com/'.$v.'/event/'.str_replace('.','/',$event));
		}
		if (isset($this->events[$event]) && is_array($this->events[$event])) {
			$log->add("Event $event was triggered : ".count($this->events[$event]).' listeners will be called.', __FILE__,__LINE__,E_INFO);
			$passedfirstevent = false;

            //Sort events by priority, lowest first.
            uasort($this->events[$event], function($a, $b) {
                if ($a['priority'] == $b['priority']) return 0;
                return ($a['priority'] < $b['priority'])?-1:1;
            });

			foreach ($this->events[$event] as $k => $v) {
				$startTime = microtime(true);
				if ($chain == false) $ret[] = $v['event']->execute($params);
				else {
					if ($passedfirstevent) $ret = $v['event']->execute($ret);
					else $ret = $v['event']->execute($params);
				}
				//Be careful with non-permanent events: non-permanent events will only fire once !
				if (!$v['event']->isPermanent()) unset($this->events[$event][$k]);
				$passedfirstevent = true;
				$elapsed = microtime(true)-$startTime;

				//Log slow events to system log...
				if ($elapsed > 0.1) {
					$service->get('Log')->add('EventHandler::trigger: '.$event.' has a poor performance event listener (execution time > 100ms). Source: '.$v['source'],__FILE__,__LINE__,E_USER_WARNING);
				}
			}
		}
		unset($k,$v,$passedfirstevent,$params,$chain,$event);

		//Get registered events in the DB too...
		//global $db;
		/**
		 * @TODO : Load events from DB
		 */
		return $ret;
	}

	/**
	 * Registers an event and its callback code to be executed.
	 *
	 * @access public
	 *
	 * @param string $name
	 * @param function $function
	 * @param array $params
	 * @return void on()
	 */
	public function on($name,$function,$params=array()){
		$name = strtolower($name);
		if (!isset($this->events[$name])) $this->events[$name] = array();
		$priority = (isset($params['priority']))?$params['priority']:100;
		$source = (isset($params['source']))?$params['source']:'';
		unset($params['priority']);
		unset($params['source']);
		$this->events[$name][] = array(
			'event' => new Event($name,$function),
			'priority' => $priority,
			'source' => $source,
			'params' => $params
		);
	}
}
?>
