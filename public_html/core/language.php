<?php
/**
 * Language service
 *
 * Parses current language and offer language routines
 *
 * Feb 9, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
class Language extends BaseClass{

	/**
	 * Holds language specific data.
	 *
	 * @var mixed $data
	 */
	var $data;

	/**
	 * Class constructor
	 *
	 * @return void Language service constructor
	 */
	public function __construct() {
		$this->data = array();
		$this->loadLanguages();
		return $this->get();
	}

	/**
	 * Detects current language from the URL data
	 *
	 * @access public
	 *
	 * @return int language ID
	 */
	public function get(){
		global $service;
		if (isset($this->data['language'])) {
			return $this->data['language'];
		}else {
			$url = $service->get('Url')->get();
			$languages = array();
			foreach($this->data['languages'] as $k => $v){
				$languages[] = $k;
			}
			$lang = '';
			if ($url['language']) {
				$lang = $url['language'];
			}
			if ($lang == '' && isset($_COOKIE['lang'])) {
				$lang = $_COOKIE['lang'];
			}
			if(in_array($lang,$languages) !== false) {
				$this->data['language'] = $this->data['languages'][$lang];
			}
			else {
				$this->data['language'] = $this->getDefault();
			}
            $isSecure = (strpos(URL,"https://") !== false)?true:false;
			setcookie('lang','',0,"/","",$isSecure);
			setcookie('lang',$this->data['language']['code'],time()+60*60,"/","",$isSecure);
			return $this->data['language'];
		}
	}

	/**
	 * Loads languages IDs we are concerned about
	 *
	 * @access public
	 *
	 * @return void Loads languages we care about
	 * on this site and put them in $this->data['languages']
	 */
	function loadLanguages(){
		global $service;
		if (!isset($this->data['languages'])) {
			$langs = array();
			if (!defined('LANG_AVAILABLE')) {
				$service->get('Log')->add('Site profile must define languages to use!',E_ERROR,LOG_FILE_SYSTEM,__FILE__,__LINE__);
				die;
			}else {
				$l = explode(',',LANG_AVAILABLE);
				foreach ($l as $k => $v){
					if (defined('LANG_'.strtoupper($v))) {
						$langs[strtolower($v)] = array(
							'id' => constant('LANG_'.strtoupper($v)),
							'code' => strtolower($v),
							'title' => constant('LANGTITLE_'.strtoupper($v))
						);
					}else {
						$service->get('Log')->add('Language \''.strtoupper($v).'\' is active but never defined in site config. Please define LANG_'.strtoupper($v).' constant in your site config file.',E_ERROR,LOG_FILE_SYSTEM,__FILE__,__LINE__);
					}
				}
			}
			$this->data['languages'] = $langs;
		}
	}

	/**
	 *
	 * @access public
	 *
	 * @return int language number
	 */
	public function getId(){
		global $service;
		if (!isset($this->data['language']['id'])) {
			$this->getDefault();
		}
		return $this->data['language']['id'];
	}

	/**
	 *
	 * @access public
	 *
	 * @return string current language code
	 */
	public function getCode(){
		global $service;
		if (!isset($this->data['language']['code'])) {
			$this->getDefault();
		}
		return $this->data['language']['code'];
	}

	/**
	 *
	 * @access public
	 *
	 * @return string current language code
	 */
	public function getLocale(){
		global $service;
		if (!isset($this->data['language']['locale'])) {
			$this->getDefault();
		}
		return $this->data['language']['locale'];
	}

	/**
	 *
	 * @access public
	 *
	 * @return string current language code
	 */
	public function getCodeForId($id){
		global $service;
		foreach ($this->data['languages'] as $k => $v) {
			if ($v['id'] == $id) {
				return $v['code'];
			}
		}
		return $this->data['language']['code'];
	}

	/**
	 * Returns the default language
	 *
	 * @access public
	 *
	 * @return array Array(id=>, code=>)
	 */
	public function getDefault(){
		global $service;
		if (defined('LANG_DEFAULT') && defined('LANG_'.strtoupper(LANG_DEFAULT))) {
			if (LANG_DEFAULT != LANG_AUTO) {
				return array(
					'id' => constant('LANG_'.strtoupper(LANG_DEFAULT)),
					'code' => strtolower(LANG_DEFAULT)
				);
			}else {
				return $this->detect();
			}
		}else {
			return $this->detect();
		}
	}

	/**
	 * Returns all languages (content of the lanugages DB table)
	 *
	 * @access public
	 *
	 * @return array All languages
	 */
	public function getAllLanguages(){
		return $this->data['languages'];
	}

	/**
	 * Returns associative array of id => lang codes, the first one beeing the default language
	 *
	 * @access public
	 *
	 * @return array $langs
	 */
	public function getCodes(){
		$langs = array();
		$default = $this->getDefault();
		$langs[$default['id']] = $default['code'];
		foreach ($this->data['languages'] as $k => $v) {
			if ($v['id'] != $default['id']) {
				$langs[$v['id']] = $v['code'];
			}
		}
		return $langs;
	}

	/**
	 * Tries to autodetect the user browser language
	 *
	 * @access public
	 *
	 * @return array A language specified as array('id' => ... , 'code' => '..')
	 */
	public function detect() {
		//Hard coded default language. Should never be used if
		//LANG_DEFAULT constant is defined in active site config
		$lang = array(
			'id' => 1,
			'code' => 'en'
		);
		$locale = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
		if (defined('LANG_'.strtoupper($locale))) {
            $userlocale = locale_accept_from_http( $_SERVER['HTTP_ACCEPT_LANGUAGE'] );
            if ($userlocale == null) $userlocale = 'en';
			$lang = array(
				'id' => constant('LANG_'.strtoupper($locale)),
				'code' => strtolower($locale),
                'locale' => $userlocale
			);
		}
		return $lang;
	}
}
