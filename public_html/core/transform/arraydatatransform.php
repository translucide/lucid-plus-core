<?php
/**
 * Transform base class
 * 
 * Sept 15, 2015
 *
 * @author 		Translucide
 * @copyright 	copyright (c) 2015 Translucide
 * @license		See docs/license.txt
 */

class ArrayTransform extends Transform {

    /**
     * Rename an array key
     *
     * Does not preserve key order.
     * Supports following options :
     * 'recursive', 'dimension', 'overwrite', 'createmissingkeys', 'missingkeysvalue', 'chainable'
     *
     * @access public
     *
     * @param array $map An key->value array, keys for old key names, values for new key names
     * @param array $data Data to perform rename on. If non provided, data passed to constructor will be used instead.
     * @param bool $forcereturn Forces returning data, even if chainable is set to true (used internally for recursive calls)
     *
     * @return array The renamed array, if option chainable is set, $this if not
     */
    public function renameKey($map,$data=null, $forcereturn = false){
        if ($data == null) $data = $this->data;
        foreach ($data as $k => $v) {
            if (is_array($v) && $this->options['recursive']) {
                $this->dimension++;
                $data[$k] = $this->renameKey($map,$v,true);
                $this->dimension--;
            }else { 
                if ($this->dimension >= $this->options['dimension']){
                    if (isset($map[$k])) {
                        if ((isset($data[$map[$k]]) && $this->options['overwrite']) || !isset($data[$map[$k]])) {
                            $data[$map[$k]] = $v;
                            unset($data[$k]);
                        }
                    }else {
                        if ($this->options['createmissingkeys']) {
                            $data[$map[$k]] = $this->options['missingkeysvalue'];
                        }             
                    }                    
                }
            }
        }
        if (!$this->options['chainable'] || $forcereturn) return $data;
        else {
            $this->data = $data;
            return $this;
        }
    }
    
    /**
     * Adds a key to an array
     *
     * Supports following options :
     * 'recursive', 'dimension', 'overwrite', 'chainable'
     *
     * @access public
     *
     * @param string $key
     * @param mixed $val
     * @param mixed $data
     * @param bool $forcereturn
     */
    public function addKey($key,$val,$data=null,$forcereturn=false){
        if ($data == null) $data = $this->data;
        foreach ($data as $k => $v) {
            if (is_array($v) && $this->options['recursive']) {
                $this->dimension++;
                $data[$k] = $this->addKey($key,$val,$v,true);
                $this->dimension--;
            }
        }
        if (($this->dimension >= $this->options['dimension']) &&
            (isset($data[$key]) && $this->options['overwrite'] || !isset($data[$key]))) {
            $data[$key] = $val;
        }
        if (!$this->options['chainable'] || $forcereturn) return $data;
        else {
            $this->data = $data;
            return $this;
        }
    }
    
    public function selectKey($key, $data=null, $forcereturn=false){
        if ($data == null) $data = $this->data;
        foreach ($data as $k => $v) {
            if (is_array($v) && $this->options['recursive']) {
                $this->dimension++;
                $data[$k] = $this->keyValues($key,$v,true);
                $this->dimension--;
            }else {
                if ($this->dimension >= $this->options['dimension'] && $k != $key) {
                    unset($data[$k]);
                }
            }   
        }
        if (!$this->options['chainable'] || $forcereturn) return $data;
        else {
            $this->data = $data;
            return $this;
        }
    }
}
?>