<?php
/**
 * Transform base class
 * 
 * Sept 15, 2015
 *
 * @author 		Translucide
 * @copyright 	copyright (c) 2015 Translucide
 * @license		See docs/license.txt
 */

class Transform {
    private $data;
    private $dimension;
    private $options;
    
    public function __construct($data) {
        $this->data = $data;
        $this->option('recursive',true);
        $this->option('overwrite',true);
        $this->option('chainable',true);
        $this->option('createmissingkeys',true);
        $this->option('missingkeysvalue','');
        $this->option('dimension',1);
        $this->dimension = 0;
    }
    /**
     * Sets an option
     *
     * Currently supported options are:
     * - bool recursive
     * - bool chainable
     * - bool overwrite
     * - bool createmissingkeys
     * - mixed missingkeysvalue
     * - dimension
     *
     * @access public
     * @param string $opt
     * @param mixed $val
     * @return mixed Option value in getter context, $this in setter context.
     */
    public function option($opt,$val=null) {
        if (isset($val) && $val != null) {
            $this->options[$opt] = $val;
            return $this;
        }
        else return $this->options[$opt];
    }
    
    public function get() {
        return $this->data;
    }
}
?>