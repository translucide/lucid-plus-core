<?php
/**
 * Generic Content class
 *
 * Defines content class (ContentData, ContentModel, ContentStore and Content)
 *
 * Oct 30, 2012
 *
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class ContentModel extends Model {
	function __construct(){
		$this->type = 'content';
		$this->initVars(array(
			'content_id' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'content_objid' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'content_component' => array(DATATYPE_STRING,array('length'=>60)),
			'content_parent' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'content_position' => array(DATATYPE_INT),
			'content_type' => array(DATATYPE_STRING,array('length'=>100)),
			'content_level' => array(DATATYPE_INT),
			'content_layout' => array(DATATYPE_STRING,array('length'=>100)),
			'content_url' => array(DATATYPE_STRING,array('length'=>100)),
			'content_idpath' => array(DATATYPE_STRING,array('length'=>255)),
			'content_urlpath' => array(DATATYPE_STRING,array('length'=>255)),
			'content_titlepath' => array(DATATYPE_STRING,array('length'=>255)),
			'content_pagetitle' => array(DATATYPE_STRING,array('length'=>255)),
			'content_description' => array(DATATYPE_STRING,array('length'=>255)),
			'content_keywords' => array(DATATYPE_STRING,array('length'=>255)),
			'content_title' => array(DATATYPE_STRING,array('length'=>255)),
			'content_thumb' => array(DATATYPE_STRING,array('length'=>255)),
			'content_exerpt' => array(DATATYPE_STRING),
			'content_content' => array(DATATYPE_STRING),
			'content_options' => array(DATATYPE_ARRAY),
			'content_draft' => array(DATATYPE_INT),
			'content_groups' => array((DB_UUIDS)?DATATYPE_UUIDARRAY:DATATYPE_INTARRAY),
			'content_tags' => array(DATATYPE_STRING),
			'content_language' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'content_site' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT),
			'content_created' => array(DATATYPE_INT),
			'content_modified'	=> array(DATATYPE_INT),
			'content_modifiedby' => array((DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT)
		));
		$this->setOption('usecache',false);
	}
}

class ContentData extends Data{
}

class ContentStore extends Store{
	public function __construct(){
		parent::__construct('content');
	}

	/**
	 * Determines the ID path from root (ID #1) to the current object
	 *
	 * @public
	 *
	 * @param ContentData $obj
	 * @return string $idpath The path, in the form 1/2/3/4 (all numbers beeing parents objids)
	 */
	public function generateIdPath($obj){
		global $service;
		$db =& $service->get('Db');
		if (!is_object($obj)) return false;
		$parents = array($obj->getVar('content_objid'));

		$parent = $this->getByObjId($obj->getVar('content_parent'));
		while(is_array($parent) && is_object($parent[0])) {
			$parents[] = $parent[0]->getVar('content_objid');
			$parent = $this->getByObjId($parent[0]->getVar('content_parent'));
		}
		return implode('/',array_reverse($parents));
	}

	/**
	 * Determines the complete url path of a section
	 *
	 * @public
	 *
	 * @param SectionData $obj
	 * @return string $urlPath
	 */
	public function generateUrlPath($obj){
		global $service;
		$db =& $service->get('Db');
		if (!is_object($obj)) return false;
		$parents = array($obj->getVar('content_url'));
		$crit = new CriteriaCompo();
		$crit->add(new Criteria('content_objid',$obj->getVar('content_parent')));
		$crit->add(new Criteria('content_language',$obj->getVar('content_language')));
		$parent = $this->get($crit);
		while(is_array($parent) && is_object($parent[0])) {
			$parents[] = $parent[0]->getVar('content_url');
			$crit = new CriteriaCompo();
			$crit->add(new Criteria('content_objid',$parent[0]->getVar('content_parent')));
			$crit->add(new Criteria('content_language',$parent[0]->getVar('content_language')));
			$parent = $this->get($crit);
		}
		//Add language as the root url path
		$langs = $service->get('Language')->getCodes();
		$parents[] = $langs[$obj->getVar('content_language')];
		$ret = implode('/',array_reverse($parents));
		if (substr($ret,-1,1) == '/') $ret = substr($ret,0,-1);
		if (substr($ret,0,1) == '/') $ret = substr($ret,1);
		return str_replace('//','/',$ret);
	}

	/**
	 * Determines the complete titles path of a content (used for breadbrumbs)
	 *
	 * @public
	 *
	 * @param ContentData $obj
	 * @return string $titlePath
	 */
	public function generateTitlePath($obj){
		global $service;
		$db =& $service->get('Db');
		if (!is_object($obj)) return false;
		$parents = array($obj->getVar('content_title'));
		$crit = new CriteriaCompo();
		$crit->add(new Criteria('content_objid',$obj->getVar('content_parent')));
		$crit->add(new Criteria('content_language',$obj->getVar('content_language')));
		$parent = $this->get($crit);
		while(is_array($parent) && is_object($parent[0])) {
			$parents[] = $parent[0]->getVar('content_title');
		$crit = new CriteriaCompo();
		$crit->add(new Criteria('content_objid',$parent[0]->getVar('content_parent')));
		$crit->add(new Criteria('content_language',$parent[0]->getVar('content_language')));
		$parent = $this->get($crit);
		}
		return implode('/',array_reverse($parents));
	}

}

class ContentHandler extends BaseClass {

	/**
	 * Stores the Content data
	 *
	 * @private
	 * @var ContentData $data
	 */
	private $data;

	/**
	 * Content PHP5 constructor
	 *
	 * @public
	 * @return void Stores content info
	 */
	public function __construct($contentData=array()){
		$this->data = $contentData;
	}

	/**
	 * Parses URL and load the corresponding page
	 *
	 * @public
	 *
	 * @return void load()
	 */
	function load($url = '') {
		if (isset($this->data) && is_a($this->data,'ContentData')) {
			return $this->data;
		}else {
			global $service;
			if ($url == '') {
				$url = $service->get('Url')->get();
				$url = $url['path'];
			}

			$contentStore = new ContentStore();
			$contentStore->setOption('ignorelangs',1);

			$crit = new CriteriaCompo(new Criteria('content_urlpath',$url),'OR');
			$crit = $contentStore->addGroupCriteria($crit);

			$crit2 = new CriteriaCompo();
			$crit2->add(new Criteria('content_draft',0),'OR');
			$crit2 = $contentStore->addGroupCriteria($crit2,array($service->get('group')->id('admin')),'OR');
			$crit->add($crit2);

			//Get /somedir/somesection before /somedir
			$crit->setOrder('DESC');
			$crit->setSort('content_urlpath');

			$object = $contentStore->get($crit);
			if ((is_array($object) && count($object) == 0) || $object == false) return false;

			//Multiple pages have the same URL: Activate A/B testing feature.
			if (count($object) > 1) {
				$i = rand(0,count($object)-1);
				$object = array($object[$i]);
			}

			$this->data = $object[0];
			if (isset($this->data) && is_a($this->data,'ContentData')) {
				return $this->data;
			}
		}
		return false;
	}

	/**
	 * Returns current section if loaded, or loads section if not.
	 *
	 * @public
	 *
	 * @return SectionData $section Returns the current section
	 */
	public function get(){
		return $this->load();
	}

	/**
	 * Returns the current section Id
	 *
	 * @public
	 *
	 * @return int current section object id
	 */
	public function getId(){
		if (is_object($this->data)) {
			return $this->data->getVar('content_objid');
		}
		return false;
	}

	/**
	 * Returns an option value
	 *
	 * @public
	 *
	 * @param string $optionname
	 * @return mixed getOption($optionname)
	 */
	public function getOption($optionname){
		$options = $this->data->getVar('content_options');
		if (isset($options[$optionname])) {
			return $options[$optionname];
		}
		unset($options);
		return false;
	}

	/**
	 * Returns a route object representing the current content object
	 *
	 * @access public
	 *
	 * @param ContentData $content
	 * @return RouteData The current route to this content
	 */
	public function toRoute($content){
		$routeObj = new RouteData('route');
		$routeObj->setVar('route_id',0);
		$routeObj->setVar('route_url',$content->getVar('content_urlpath'));
		$routeObj->setVar('route_controller','Content');
		$routeObj->setContent($content);
		return $routeObj;
	}

	/**
	 * Creates a content object of specified type and returns the object.
	 *
	 * @access public
	 *
	 * @param string $name Content object name to create
	 * @param ContentData $data Object representing the data associated with this content object.
	 */
	public function getContentObject($name = false,$data = false){
		global $service;
		if ($name == false) {
			$request = $service->get('Request')->get();
			$name = $request['type'];
		}
		$ctCls = ucfirst($name).'Content';
		$path = $service->get('Ressource')->findRessource('content',strtolower($name));
		$service->get('Ressource')->get($path);
		$langpath = substr(substr($path,0,strrpos($path,'/')-1),0,strrpos($path,'/')-1).'/lang/'.$service->get("Language")->getCode().'/'.strtolower($name);
		$service->get('Ressource')->get($langpath);
		if (class_exists($ctCls) && $name != '') $obj = new $ctCls($data);
		else $obj = false;
		return $obj;
	}
}

class Content {

	/**
	 * Stores the content data
	 *
	 * @private
	 * @var ContentData $data
	 */
	protected $data;

	/**
	 * Stores content infos
	 *
	 * @private
	 * @var array $infos
	 */
	protected $infos;

	/**
	 * Content PHP5 constructor
	 *
	 * @public
	 * @return void Stores content info
	 */
	public function __construct($data=false){
		$this->Content($data);
	}

	/**
	 * Widget PHP4 constructor
	 *
	 * @public
	 * @return void Stores Content info
	 */
	public function Content($data=false){
		$this->setData($data);
		$this->init();
	}

	/**
	 * Gets the content data
	 *
	 * @public
	 *
	 * @return array The content data
	 */
	public function getData(){
		return $this->data;
	}

	/**
	 * Sets the content data
	 *
	 * @public
	 *
	 * @return array The content data
	 */
	public function setData($data){
		if ($data !== false && is_a($data,'ContentData')) {
			$this->data = $data;
		}
	}

	/**
	 * Returns an array containing content informations
	 *
	 * @public
	 * @param string $info The information needed
	 * @return mixed Returns the infos array or just one info if set
	 */
	public function getInfo($info=''){
		if (isset($this->infos[$info])) return $this->infos[$info];
		return $this->infos;
	}

	/**
	 * Sets content informations
	 *
	 * @protected
	 * @param mixed $info The information
	 * @return void
	 */
	protected function setInfo($info='',$value=''){
		if (is_array($info)) $this->infos = $info;
		else $this->infos[$info] = $value;
	}

	/**
	 * Initialize the content object mostly by filling the infos array
	 *
	 * @public
	 * @return void
	 */
	public function init(){
		$this->infos = array(
			'type' => '',
			'title' => '',
			'description' => '',
			'icon' => '',
			'saveoptions' => array()
		);
	}

	/**
	 * Renders this content
	 *
	 * @public
	 * @return string Rendered content
	 */
	public function render(){
		return '';
	}

	/**
	 * Renders this content as Exerpt
	 *
	 * @public
	 * @return string Rendered content exerpt
	 */
	public function renderExerpt(){
		return '';
	}

	/**
	 * Lets the content file add its own form fields to the content edit form.
	 *
	 * @public
	 *
	 * @param array $obj The content object, one per language, being edited.
	 * @param Form The edit form, add form fields & return it back.
	 * @return Form The form to display.
	 */
	public function edit($obj,$form){
		return $form;
	}

	/**
	 * Save handler function called on content updates
	 *
	 * Most of the time this method will not be implemented as one
	 * should use getOptions and send all content options to be saved
	 * by the contentmanager
	 *
	 * @public
	 *
	 * @return bool $success
	 */
	public function save(){
		return true;
	}

	/**
	 * Copy handler function called on object copy
	 *
	 * Functions such as copy/paste in the admin interface will call that method
	 * to recursively copy all of this obj childrens
	 */
	public function copy($obj,$original_id,$new_id){
		return true;
	}

	/**
	 * Get content IDs matching the query.
	 *
	 * Should only be implemented when the content type in use does not
	 * store all its data into the content table. The content table is
	 * already searched for matches, so returning any match from that table
	 * will cause duplicated search results.
	 *
	 * @param string $query
	 * @return array Array of ints (content IDs) matching query.
	 */
	public function search($query){
		return array();
	}

	/**
	 * Called when an object is deleted.
	 *
	 * Most content objects will not need to implement this method. However
	 * if you use other tables to store data, you might want to delete that
	 * data upon deletion of the associated content object.
	 *
	 * @return Nothing
	 */
	public function delete(){
		return true;
	}

	/**
	 * Called on an ajax call pointing to this content type.
	 *
	 * To make an ajax call to this function, use :
	 * {url}/{lang}/ajax/admin/content/{type}/{op}
	 *
	 * @var string $op The operation you wish this function performs
	 */
	public function ajax($op=''){
	}
}
