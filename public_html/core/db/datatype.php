<?php
/**
 * Datatype interface
 *
 * Ensure all data type handlers have the following methods :
 * - validate
 * - store
 * - retrieve
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

interface DataType {
	
	/**
	 * Method validate
	 * 
	 * @method bool validate() validates $value using $options
	 */
	function validate($value, $options);
	
	/**
	 * Method store
	 *
	 * @method mixed store() prepares a value for storage
	 */
	function store($value,$options);
	
	/**
	 * Method retrieve
	 *
	 * @method mixed retrieve() retrieve a value from a storage representation
	 */
	function retrieve($value,$options);
}
?>