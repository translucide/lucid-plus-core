<?php
/**
 * Base class to represent a data object.
 *
 * Defines a data object
 *
 * June 23, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class Data{

	/**
	 * The object type, i.e. the database table this object
	 * refers to whitout prefix and plural marks
	 *
	 * @access protected
	 * @var string $type Data type identifier, table name without prefix
	 */
	protected $type;

	/**
	 * Modification tracking variable
	 *
	 * @access protected
	 * @var bool $modified
	 */
	protected $modified;

	/**
	 * This object variables as array keys, and values as values.
	 *
	 * @access protected
	 * @var array $data
	 */
	protected $data;

	/**
	 * Hold this object childs data
	 *
	 * @access protected
	 * @var array $childs
	 */
	protected $childs;

	/**
	 * PHP 5 constructor
	 *
	 * @public
	 *
	 * @param string $type Object type
	 * @param array $data Data, in array format
	 * @return void Data()
	 */
	public function __construct($type){
		$this->setType($type);
        $this->modified = true;
	}

	/**
	 * Export this object data in appropriate format
	 *
	 * @TODO: Default should be to export as array, no need to export as object
	 *
	 * @access public
	 * @return mixed Object or array of data
	 */
	public function get($asarray=true){
		if ($asarray) return $this->data;
        else return $this;
	}

	/**
	 * Returns this object data as array
	 *
	 * @public
	 *
	 * @return array Object data as array
	 */
	public function getVars() {
		return $this->get();
	}

	/**
	 * Gets a variable's value in the appropriate format
	 *
	 * @public
	 *
	 * @param string $var Variable's name
	 * @return mixed Variable's value
	 */
	public function getVar($var){
		if (isset($this->data[$var])) {
			return $this->data[$var];
		}else {
			global $service;
			$service->get('Log')->add(get_called_class()."::getVar: Trying to get an undefined variable in this data set : $var.",__FILE__,__LINE__,E_USER_WARNING);
		}
	}

	/**
	 * Sets this object's data
	 *
	 * @public
	 *
	 * @param array $data Data to use for this data object
	 * @return void set()
	 */
	public function set($data){
        if ($data != $this->data) $this->modified = true;
		$this->data = $data;
	}

	/**
	 * Sets a variable's value
	 *
	 * @public
	 *
	 * @param string $var
	 * @param mixed $value
	 * @return void setVar()
	 */
	public function setVar($var,$value){
        if ($value != $this->data[$var]) $this->modified = true;
		$this->data[$var] = $value;
	}

	/**
	 * Gets the objet type
	 *
	 * @public
	 *
	 * @return string getType()
	 */
	public function getType(){
		return $this->type;
	}

	/**
	 * Sets the objet type
	 *
	 * @public
	 *
	 * @return string getType()
	 */
	public function setType($type){
        if ($type != $this->type) $this->modified = true;
		$this->type = $type;
	}

	/**
	 * Gets the childs data
	 *
	 * @public
	 *
	 * @return array $childs
	 */
	public function getChilds(){
		return $this->childs;
	}

	/**
	 * Adds a child data
	 *
	 * @public
	 *
	 * @param string $type Child type
	 * @param array $childs
	 */
	public function addChilds($type,$child){
        if ($this->childs[$type] != $child) $this->modified = true;
		$this->childs[$type] = $child;
	}

    /**
     * Sets the modified flag
     */
    public function setModified($modified=true){
        $this->modified = $modified;
    }

    /**
     * Gets the modified flag
     */
    public function isModified(){
        return $this->modified;
    }
}
?>
