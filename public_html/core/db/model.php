<?php
/**
 * Base class to represent a data model.
 *
 * Defines a data model and ensures data integrity and validation
 *
 * June 23, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('Ressource')->get('core/db/datatype');

class Model {

	/**
	 * The object this model defines
	 *
	 * @protected
	 *
	 * @var string $object
	 */
	protected $type;

	/**
	 * Model vars definition
	 *
	 * @protected
	 *
	 * @var array $vars
	 */
	protected $vars;

	/**
	 * Childs data relationships definition
	 *
	 * Childs with the loadchilds option of a store object allow loading complex objects
	 * with only one SQL query per child relationship for all parent records
	 *
	 * @protected
	 *
	 * @var array $childs
	 */
	protected $childs;

	/**
	 * Parents data relationships definition
	 *
	 * Parents with the loadparents option of a store object allow loading complex objects
	 * with only one SQL query per parent relationship, no matter how many childs there are
	 *
	 * @protected
	 *
	 * @var array $parents
	 */
	protected $parents;

	/**
	 * Model wide options
	 *
	 * @protected
	 *
	 * @var array $options
	 */
	protected $options;

	/**
	 * This model PHP 5 constructor
	 *
	 * @public
	 *
	 * @return void __construct
	 */
	public function __construct(){
		$this->vars = array();
		$this->options = array();
	}

	/**
	 * Get cache key
	 *
	 * Returns the cache key variable name if found or the object ID field var name
	 *
	 * @public
	 *
	 * @return string Cache key variable name
	 */
	public function getCacheKey(){
		if ($this->getOption('cachekey') != '') return $this->getOption('cachekey');
		else {
			foreach ($this->vars as $k => $v) {
				if (isset($v['options']['cachekey']) && $v['options']['cachekey']) {
					return $k;
				}
			}
		}
		return $this->getId();
	}

	/**
	 * Get model object type or variable type if var is specified
	 *
	 * @param string $var Optional variable name
	 * @return string Model object type, or variable type
	 */
	public function getType($var=''){
		if ($var == '') return $this->type;
        else return $this->vars[$var]['type'];
	}

	/**
	 * Sets model object type or variable type if var is specified
	 *
	 * @return void Sets model type
	 */
	public function setType($type,$var=''){
		if ($var == '') $this->type = $type;
        else return $this->vars[$var]['type'] = $type;
	}

	/**
	 * Returns all variables this model supports
	 *
	 * @public
	 *
	 * @return array getVars()
	 */
	public function getVars($fullDefinition=false) {
		if ($fullDefinition) return $this->vars;
		else return array_keys($this->vars);
	}

	/**
	 * Returns the ID field name
	 *
	 * Returns either _id or _uuid var. Defaults to ID
	 *
	 * @public
	 *
	 * @return string ID field name
	 */
	public function getId(){
	return (($this->isVar($this->type.'_id'))?$this->type.'_id':''/*$this->type.'_uuid'*/);
	}

	/**
	 * Adds a child model
	 *
	 * @access public
	 *
	 * @param string Model type. Will instantiate an object of class ucfirst($modeltype).'Model' when fetching childs data
	 * @param string $parentVar The column name in this model that must match the $childVar value
	 * @param string $childVar The column name in the child Model that must match the $parentVar value
	 */
	public function addChild($modeltype,$parentVar,$childVar) {
		$this->childs[$modeltype] = array(
			'model' => $modeltype,
			'parent' => $parentVar,
			'child' => $childVar
		);
	}

	/**
	 * Removes a child model
	 *
	 * @access public
	 *
	 * @param string $type Model type to remove
	 * @return bool True if found, false if not found.
	 */
	public function removeChild($type) {
		if (isset($this->childs[$type])) {
			unset($this->childs[$type]);
			return true;
		}
		return false;
	}

	/**
	 * Returns the child definitions
	 *
	 * @access public
	 *
	 * @return array An array of childs definitions, each key named by the child type. See Model::addChild method.
	 */
	public function getChilds(){
		return $this->childs;
	}

	/**
	 * Adds a parent model
	 *
	 * @access public
	 *
	 * @param Model A parent model object
	 * @param string $parentVar The column name in the parent model that must match the $childVar (this model) value
	 * @param string $childVar The variable name in this model that must match the $parentVar value
	 */
	public function addParent($modeltype,$parentVar,$childVar) {
		$this->parents[$modeltype] = array(
			'model' => $modeltype,
			'parent' => $parentVar,
			'child' => $childVar
		);
	}

	/**
	 * Removes a parent model
	 *
	 * @access public
	 *
	 * @param string $type Model type to remove
	 * @return bool True if found, false if not found.
	 */
	public function removeParent($type) {
		if (isset($this->parents[$type])) {
			unset($this->parents[$type]);
			return true;
		}
		return false;
	}

	/**
	 * Returns the parents definitions
	 *
	 * @access public
	 *
	 * @return array An array of parents definitions, each key named by the parents type. See Model::addParent method.
	 */
	public function getParents(){
		return $this->parents;
	}

	/**
	 * Get option value
	 *
	 * @param string Option name
	 * @return mixed Option value
	 */
	public function getOption($name){
		if (isset($this->options[$name])) return $this->options[$name];
		return false;
	}

	/**
	 * Set model option
	 *
	 * The following model options are honored :
	 * - usecache : Tells whether this model's data can be cached in DB
	 * - cachetime : The number of hours before the cache expires
	 * - cachekey : The variable name to use its value as data keys
	 * - loadchilds : Set to true to auto-load childs data
	 *
	 * @param string Option name
	 * @param mixed Option value
	 * @return void setOption()
	 */
	public function setOption($name,$value){
		global $service;
		if ($name == 'usecache' && is_object($service) && $service->isLoaded('setting')) {
			if ($service->get('Setting')->get('activatedbcache') == 0) {
				return false;
			}
		}
		$this->options[$name] = $value;
	}

	/**
	 * Add and defines multiple variables at once.
	 *
	 * @public
	 *
	 * @param array $vars
	 * @return void initVars()
	 */
	public function initVars($vars) {
		foreach ($vars as $k => $v) {
			$this->initVar($k,$v[0],(isset($v[1]) && is_array($v[1]))?$v[1]:false);
		}
	}

	/**
	 * Adds and define a variable to this model
	 *
	 * @method bool initVar()
	 *
	 * @param string $var Variable name
	 * @param string $type Variable type
	 * @param array $options An array of possible values for this field
	 * @return void initVar()
	 */
	public function initVar($var, $type, $options=false){
		$type = strtolower($type);
		$this->vars[$var] = array(
			'type' => $type,
			'options' => $options
		);
	}

	/**
	 * Checks if a variable exists within current model
	 *
	 * @public
	 *
	 * @param string $varName
	 * @return bool True if variable exists
	 */
	public function isVar($varName) {
		return isset($this->vars[$varName]);
	}

	/**
	 * Returns variables with default values
	 *
	 * @public
	 *
	 * @return bool True if variable exists
	 */
	public function getDefaults() {
		$array = array();
		foreach($this->vars as $k => $v) {
			if (isset($v['options']['default'])) {
				$array[$k] = $v['options']['default'];
			}else {
				switch($v['type']) {
					case DATATYPE_INT: $array[$k] = 0; break;
					case DATATYPE_TIMESTAMP: $array[$k] = 0; break;
					case DATATYPE_FLOAT: $array[$k] = 0; break;
					case DATATYPE_BOOL: $array[$k] = 0; break;
					case DATATYPE_STRING: $array[$k] = ''; break;
					case DATATYPE_INTARRAY: $array[$k] = ''; break;
					case DATATYPE_BOOLARRAY: $array[$k] = 0; break;
					case DATATYPE_FLOATARRAY: $array[$k] = 0; break;
					case DATATYPE_EMAIL: $array[$k] = ''; break;
					case DATATYPE_COLOR: $array[$k] = ''; break;
					case DATATYPE_IMAGE: $array[$k] = ''; break;
					case DATATYPE_IP: $array[$k] = '0.0.0.0'; break;
					case DATATYPE_DOMAINNAME: $array[$k] = ''; break;
					case DATATYPE_UUID: $array[$k] = ''; break;
                    default: $array[$k] = ''; break;
				}
			}
		}
		return $array;
	}

	/**
	 * Validates an object's data
	 *
	 * @public
	 *
	 * @param array $data A key value array in which the key represents the var name
	 * @return array validate()
	 */
	public function validate($data){
		$ret = array();
		foreach($this->vars as $k => $v) {
			$ret[$k] = $this->validateVar($k,$data[$k]);
		}
		return $ret;
	}

	/**
	 * Validate a variable's value
	 *
	 * @public
	 *
	 * @param string $var
	 * @param mixed value
	 * @return mixed validateVar()
	 */
	public function validateVar($var,$value){
		$ret = null;
		$v = $this->vars[$var];
		$type = $v['type'];
		$options = $v['options'];
		$obj = $this->getValidator($type);
		if (!is_object($obj)) $ret = $value;
		else {
			if ($obj->validate($value,$options)) $ret = $value;
			else {
				$ret = null;
				global $service;
				$log =& $service->get('log');
				$log->add("Model::validateVar (".get_called_class().") Variable {$this->type}.$k of type $type failed validity check.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			}
		}
		return $ret;
	}

	/**
	 * Retrieve a stored variable and unpacks it if necessary
	 *
	 * @public
	 *
	 * @param array $data
	 * @return array Data, unserialized, unpacked
	 */
	public function retrieve($data) {
		$ret = array();
		foreach($this->vars as $k => $v) {
			$type = $v['type'];
			$options = $v['options'];
			$value = $data[$k];
			$obj = $this->getValidator($type);
			if (!is_object($obj)) $ret[$k] = $value;
			else $ret[$k] = $obj->retrieve($value,$options);
		}
		return $ret;
	}

	/**
	 * Generate a storable representation of variables
	 *
	 * @public
	 *
	 * @param array $data
	 * @return array Storable representation of all vars in $data
	 */
	public function store($data) {
		$ret = array();
		foreach($this->vars as $k => $v) {
			$type = $v['type'];
			$options = $v['options'];
			$value = $data[$k];
			$obj = $this->getValidator($type);
			if (!is_object($obj)) {
                $ret[$k] = $value;
            }
			else {
                $ret[$k] = $obj->store($value,$options);
            }
		}
		return $ret;
	}

	/**
	 * Returns a datatype object
	 *
	 * @private
	 *
	 * @param string $type
	 * @return DataType $object
	 */
	private function getValidator($type){
		$className = 'DataType'.ucfirst($type);
		$object = false;
		if (!class_exists($className)) {
			global $service;
			$log =& $service->get('log');
			$log->add("Model::getValidator (".get_called_class().") No data handler found for variables of type '$type'. Create a handler file named $type.php in the /core/db/datatype folder. In the mean time validation will pass for variables of this type.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
		}else {
			$object = new $className();
		}
		return $object;
	}
}
?>
