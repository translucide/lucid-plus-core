<?php
/**
 * System DB cache manager
 *
 * Manages system DB cache
 *
 * November 13, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class DBCache extends BaseClass {

	/**
	 * The model to use to generate the DB cache
	 */
	private $model;

	/**
	 * Cache options
	 *
	 * Currently supported options :
	 * - ignorelangs : Boolean value, whether to add language selection criteria to all
	 *   DB queries performed by this store object
	 * - ignoresites : Boolean value, whether to add site selection criteria to all
	 *   DB queries performed by this store object.
	 *
	 * @access private
	 *
	 * @var array Cache-wide options
	 */
	private $options;

	/**
	 * PHP5 constructor
	 *
	 * @access public
	 *
	 * @param Model Model object to use in this cache instance
	 * @return bool True : model is a valid Model object
	 */
	public function __construct($model) {
        if (!is_dir(DATAROOT.'cache/db')) {
            mkdir(DATAROOT.'cache/db', 0777, true);
        }
		return $this->setModel($model);
	}

	/**
	 * Gets an option value
	 *
	 * @access public
	 *
	 * @param string $optionname
	 * @return mixed $option value
	 */
	public function getOption($optionname){
		if (isset($this->options[$optionname])) {
			return $this->options[$optionname];
		}
		global $service;
		$log =& $service->get('log');
		$log->add(
            LOG_FILE_SYSTEM,"DBCache::getOption (".get_called_class().") : Option '$optionname' was accessed without beeing set first!",
            __FILE__, __LINE__, E_WARNING
        );
		return false;
	}

	/**
	 * Sets an option value
	 *
	 * @access public
	 *
	 * @param string $optionname
	 * @param mixed $optionvalue
	 * @return mixed $option value
	 */
	public function setOption($optionname,$optionvalue){
		$this->options[$optionname] = $optionvalue;
	}

	/**
	 * Return cache file path
	 *
	 * @access public
	 *
	 * @return string Cache file path
	 */
	public function getPath(){
		return DATAROOT.'cache/db/'.strtolower($this->model->getType()).'.php';
	}

	/**
	 * Sets the model to use for this cache instance
	 *
	 * @access public
	 *
	 * @param Model $model Model object to use in this cache instance
	 * @return bool Success or failure to use model
	 */
	public function setModel($model){
		if (is_a($model,'Model')) {
			$this->model = $model;
			return true;
		}else{
			global $service;
			$log =& $service->get('log');
			$log->add("DBCache::setModel : The object given as parameter is not a Model object.",__FILE__,__LINE__,E_ERROR,LOG_FILE_DATABASE);
			return false;
		}
	}

	/**
	 * Checks if cache data is valid or expired
	 *
	 * @access public
	 *
	 * @param int $cachetimestamp Cache time stamp to validate
	 * @return bool True if cache is still valid
	 */
	public function isValid($cachetimestamp) {
		if ($cachetimestamp > time()) return true;
		return false;
	}

	/**
	 * Determines if the cache exists for this model
	 *
	 * @access public
	 *
	 * @return bool True if the cache exists
	 */
	public function exists() {
		//@TODO : find another way to keep php stat cache in sync
		//clearstatcache is too slow to be used here...
		//clearstatcache();
		$file = $this->getPath();
		return file_exists($file);
	}

	/**
	 * Flush cache for a model
	 *
	 * @return void flush()
	 */
	public function flush(){
		if ($this->exists()) {
			unlink($this->getPath());
		}
	}

	/**
	 * Gets one or more objects in the cache
	 *
	 * @param mixed $selection Object ID or criteria or empty/false for all objects
	 * @return array Returns an array of arrays
	 */
	public function get($selection=false){
		global $cacheitem;
		unset($cacheitem);
		$cache = $this->load();
		if (is_array($cache)) {
			if ($selection == false) {
				return $cache['data'];
			}
			else {
				global $service;
				$service->get('Time')->startTimer('db.query');
				$ret = array();
				$crit = $selection->renderPHP();
				foreach ($cache['data'] as $k => $v) {
					if (is_object($selection)) {
						unset($cacheitem);
						global $cacheitem;
						$cacheitem = $v;
						if (eval(str_replace('{%s}','cacheitem','return ('.$crit.')?true:false;'))){
							$ret[] = $v;
						}
					}else{
						if ($selection == $v[$this->model->getId()]) {
							$ret[] = $v;
						}
					}
				}
				//Apply group by settings on returned result set...
				//@TODO : Implement GROUP BY feature for cached elements

				//Apply sorting settings on returned result set if any
				$orderStr = '';
				$sortcols = $selection->getSort(true);
				$sortorders = $selection->getOrder(true);
				$order = SORT_ASC;
				if (count($selection->getSort(true))) {
					$orderStr = ' ORDER BY '.$sortcols[0].' '.$sortorders[0];
					if ($sortorders[0] == 'DESC') $order = SORT_DESC;
					usort($ret, $this->createSortFunction(array($sortcols[0],$order)));
				}

				//Apply limit settings on returned result set if any
				$limitStr = '';
				if ($selection->getStart() != '' || $selection->getStart() > 0) {
					$limitStr = ' LIMIT '.$selection->getStart().','.$selection->getLimit();
					$ret = array_slice($ret,$selection->getStart(),$selection->getLimit(),true);
				}
				$timespent = $service->get('Time')->stopTimer('db.query');

				$log =& $service->get('log');
				$log->add("DBCache::get - Query executed in $timespent seconds : ".$this->model->getType()." (".count($ret)." results found) : ".str_replace('{%s}','cacheitem',$crit).$orderStr.$limitStr,__FILE__,__LINE__,E_INFO, LOG_FILE_DATABASE);
				return $ret;
			}
		}else {
			return false;
		}
	}

	protected function createSortFunction() {
		// Normalize criteria up front so that the comparer finds everything tidy
		$criteria = func_get_args();
		foreach ($criteria as $index => $criterion) {
			$criteria[$index] = is_array($criterion)
				? array_pad($criterion, 3, null)
				: array($criterion, SORT_ASC, null);
		}

		 $comparer = function($first, $second) use ($criteria) {
			while(!empty($criteria)) {
				// How will we compare this round?
				list($criterion, $sortOrder, $projection) = array_shift($criteria);
				$sortOrder = $sortOrder === SORT_DESC ? -1 : 1;

				// If a projection was defined project the values now
				if($projection) {
					$lhs = call_user_func($projection, $first[$criterion]);
					$rhs = call_user_func($projection, $second[$criterion]);
				}
				else {
					$lhs = $first[$criterion];
					$rhs = $second[$criterion];
				}

				// Do the actual comparison; do not return if equal
				if ($lhs < $rhs) {
					return -1 * $sortOrder;
				}
				else if ($lhs > $rhs) {
					return 1 * $sortOrder;
				}

			}

			// Nothing more to compare with, so $first == $second
			return 0;
		};
		return $comparer;
	}

	/**
	 * Loads the cache and returns the cache variable defined in the cache file
	 *
	 * @access public
	 *
	 * @return mixed Array: The cache content False on failure to load cache.
	 */
	public function load(){
		global $cache_object;
		if ($this->exists() !== false) {
			include($this->getPath());
			if ($this->isValid($cache_object['expires'])) return $cache_object;
			else return false;
		}else {
			global $service;
			$log =& $service->get('log');
			$log->add("DBCache::load : Could not load cache file ".$this->getPath().". File was not found.",__FILE__,__LINE__,E_NOTICE, LOG_FILE_DATABASE);
			return false;
		}
	}

	/**
	 * Writes cache
	 *
	 * @access public
	 *
	 * @param array $objects Array of Data objects
	 * @return bool Success or failure.
	 */
	public function write($objects){
		$key = $this->model->getCacheKey();
		$vars = $this->model->getVars();

		//Get time at which this cache expires. Defaults to 24h from now.
		if ($this->model->getOption('cachetime') > 0) $cacheexpires = $this->model->getOption('cachetime') + time();
		else $cacheexpires = 24*60*60 + time(); //24 hours cache

		$filecontent = "<?php\n//Autogenerated LucidCMS php cache file. Delete file to force cache refresh.\n";
		$filecontent .= '$cache_object = array(\'expires\' => '.$cacheexpires.', \'textualexpires\' => \''.date('Y-m-d H:i:s').'\', \'data\' => array(';

		foreach ($objects as $k => $v) {
			$objects[$k] = $v->getVars();
		}
		$pastfirstobj = false;
		foreach ($objects as $k => $v) {
			if ($pastfirstobj) $filecontent .= ",";
			$data = $v; //$this->model->store($v);
			if ($key != '') $filecontent .= "\n'".$data[$key]."' => array(";
			$pastfirstvar = false;
			foreach ($vars as $kv => $vv) {
				if ($pastfirstvar) $filecontent .= ', ';
				if (!is_array($data[$vv])) $filecontent .= "'$vv' => '".str_replace('\'','\\\'',$data[$vv])."'";
				else {
					$filecontent .= "'$vv' => array(";
					$pastfirstitem = false;
					foreach($data[$vv] as $kdata => $vdata) {
						if ($pastfirstitem) $filecontent .= ',';
						$filecontent .= "'$kdata' => '".str_replace('\'','\\\'',$vdata)."'";
						$pastfirstitem = true;
					}
					$filecontent .= ")";
				}
				$pastfirstvar = true;
			}
			$filecontent .= ")";
			$pastfirstobj = true;
		}
		$filecontent .= "\n));\n?>";

		$byteswritten = file_put_contents($this->getPath(),$filecontent);
		if ($byteswritten > 0) {
			return true;
		}
		else {
			global $service;
			$log =& $service->get('log');
			$log->add("Cache::write : Could not write cache file to ".$this->getPath(),__FILE__,__LINE__,E_ERROR, LOG_FILE_DATABASE);
			return false;
		}
	}

}
?>
