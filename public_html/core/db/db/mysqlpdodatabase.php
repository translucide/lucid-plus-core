<?php
/**
 * PDO database handler class.
 *
 * Handles queries to a pdo database
 *
 * June 23, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
class MysqlpdoDatabase extends BaseClass{

	/**
	 * The last error message encountered
	 *
	 * @var string $error;
	 */
	private $error;

	/**
	 * The last error code encountered
	 *
	 * @var int $errno;
	 */
	private $errno;

	/**
	 * The host to which to connect to
	 *
	 * @var string $host
	 */
	private $host;

	/**
	 * The user name used to establish the database connection
	 *
	 * @var string $user
	 */
	private $user;

	/**
	 * The password to use to connect to the DB
	 *
	 * @var string $pass
	 */
	private $pass;

	/**
	 * The database name to which we are about to connect
	 *
	 * @var string $db
	 */
	private $db;

	/**
	 * PDO Object
	 *
	 * @var $dbobj;
	 */
	private $dbobj;

	/**
	 * mysqlDatabase constructor
	 *
	 * @var string $host
	 * @var string $user
	 * @var string $pass
	 * @var string $db
	 */
	public function __construct($host, $user, $pass, $db){
		$this->host = $host;
		$this->user = $user;
		$this->pass = $pass;
		$this->db = $db;
	}

	/**
	 * Establish connection to a mysql database
	 *
	 * @method bool connect()
	 *
	 * @var string $host
	 * @var string $user
	 * @var string $pass
	 * @var string $db
	 */
	public function connect($host, $user, $pass, $db, $noretry = false) {
		global $service;
		try {
			$this->dbobj = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
		}
		catch(PDOException $e) {
			$msg = $e->getMessage();
			$ret = false;

			/**
             * Try to recuperate from error by retrying again in 0.5seconds only for this error :
             * SQLSTATE[HY000] [2002] Resource temporarily unavailable. Could not connect to DB.
             */
            if ($noretry === false && strpos($msg,'SQLSTATE[HY000]') !== false && strpos($msg, '[2002]') !== false) {
				sleep(0.5);
                $ret = $this->connect($host,$user,$pass,$db,true);
            }
			if (!$ret) {
				$service->get('Log')->add($msg,__FILE__,__LINE__,E_ERROR, LOG_FILE_DATABASE);
	            if (strpos($msg,'SQLSTATE[HY000]') !== false && strpos($msg, '[2002]') !== false) {
	                @http_response_code(429);
					echo 'ERROR : Could not connect to DB. Please try again!';
	                die;
	            }
			}
		}
		if (is_a($this->dbobj,'PDO')) {
			$this->dbobj->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return true;
		}else {
			return false;
		}
	}

	/**
	 * Perform a query on the database
	 *
	 * @method mixed query() Returns the results as an array
	 * or the last inserted ID when doing an insert.
	 *
	 * @var string $query
	 */
	public function query($query){
		$this->error = '';
		$this->errno = 0;
		try {
			if (is_array($query) && count($query['params']) > 0) {
				$inserting = (stripos($query['query'],'INSERT') ===0)?true:false;
				$updating = (stripos($query['query'],'UPDATE') ===0)?true:false;
				$deleting = (stripos($query['query'],'DELETE') ===0)?true:false;
				$stmt = $this->dbobj->prepare($query['query']);
				foreach ($query['params'] as $k => $v) {
					$type = '';
					if (isset($v['type']) && $v['type'] != '') {
						switch($v['type']) {
							case DATATYPE_STRING : {
								if ($v['length'] > 255) $type = PDO::PARAM_STR;
								else $type = PDO::PARAM_LOB;
							}break;
							case DATATYPE_INT : $type = PDO::PARAM_INT; break;
							case DATATYPE_BOOL : $type = PDO::PARAM_BOOL; break;
						}
						$stmt->bindValue($v['paramname'], $v['value'], $type);
					}else {
						$stmt->bindValue($v['paramname'], $v['value']);
					}
				}
				$ret = $stmt->execute();
				if (!$inserting && !$updating && !$deleting) $result = $stmt->fetchAll();
				else {
					if ($inserting) $result = $this->dbobj->lastInsertId();
					else $result = true;
				}
			}
			else {
				$inserting = (stripos($query,'INSERT') ===0)?true:false;
				$updating = (stripos($query,'UPDATE') ===0)?true:false;
				$deleting = (stripos($query,'DELETE') ===0)?true:false;
				if (is_array($query)) {
					$query = $query['query'];
				}

				$stmt = $this->dbobj->prepare($query);
				$result = $stmt->execute();
				if (!$inserting && !$updating && !$deleting) $result = $stmt->fetchAll();
				else {
					if ($inserting) $result = $this->dbobj->lastInsertId();
					else $result = true;
				}
			}
		}
		catch(PDOException $e) {
			global $service;
			$this->error = $e->getMessage();
			$this->errno = $e->getCode();
			if (defined('DEBUG') && DEBUG == true) {
                echo 'DB ERROR: '.$this->errno.': '.$this->error;
                print_r($query);
            }
			return false;
		}
		return $result;
	}

	/**
	 * Gets the last insert id
	 *
	 * @access public
	 * @return int last insert id
	 */
	public function getLastInsertId(){
		return $this->dbobj->lastInsertId();
	}

	/**
	 * Returns the last error code
	 *
	 * @acces public
	 * @return int Last error code
	 */
	public function getErrorCode() {
		return $this->errno;
	}

	/**
	 * Returns the last error message
	 *
	 * @acces public
	 * @return int Last error code
	 */
	public function getErrorMessage() {
		return $this->error;
	}
}
?>
