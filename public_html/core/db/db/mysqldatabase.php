<?php
/**
 * Mysql database handler class.
 *
 * Handles queries to a mysql database
 *
 * June 23, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
class MysqlDatabase extends BaseClass{

	/**
	 * The last error message encountered
	 *
	 * @var string $error;
	 */
	private $error;

	/**
	 * The last error code encountered
	 *
	 * @var int $errno;
	 */
	private $errno;

	/**
	 * The host to which to connect to
	 *
	 * @var string $host
	 */
	private $host;

	/**
	 * The user name used to establish the database connection
	 *
	 * @var string $user
	 */
	private $user;

	/**
	 * The password to use to connect to the DB
	 *
	 * @var string $pass
	 */
	private $pass;

	/**
	 * The database name to which we are about to connect
	 *
	 * @var string $db
	 */
	private $db;

	/**
	 * mysqlDatabase constructor
	 *
	 * @var string $host
	 * @var string $user
	 * @var string $pass
	 * @var string $db
	 */
	public function MysqlDatabase($host, $user, $pass, $db){
		$this->host = $host;
		$this->user = $user;
		$this->pass = $pass;
		$this->db = $db;
	}

	/**
	 * Establish connection to a mysql database
	 *
	 * @method bool connect()
	 *
	 * @var string $host
	 * @var string $user
	 * @var string $pass
	 * @var string $db
	 */
	public function connect($host, $user, $pass, $db) {
		global $service;
		if (!mysql_connect($host,$user,$pass)) {
			$service->get('Log')->add("Cannot connect to database : ".mysql_errno().' : '.mysql_error(),__FILE__,__LINE__,E_ERROR, LOG_FILE_DATABASE);
		}else {
			if (!mysql_selectdb($db)) {
				$service->get('Log')->add("Cannot select database named $db : ".mysql_errno().' : '.mysql_error(),__FILE__,__LINE__,E_ERROR, LOG_FILE_DATABASE);
			}
		}
	}

	/**
	 * Perform a query on the database
	 *
	 * @param string $query
	 * @return mixed query() Returns the results as an array
	 */
	public function query($query){
		$res = mysql_query($query);
		if (!$res) {
			global $service;
			$service->get('Log')->add("DB Query error : ".$query." - Error returned : ".mysql_error(),__FILE__,__LINE__,E_ERROR, LOG_FILE_DATABASE);
			return false;
		}
		$data = array();
		while ($d = mysql_fetch_array($res)) {
			$data[] = $d;
		}
		unset($d);
		return $data;
	}

	/**
	 * Gets the last insert id
	 *
	 * @access public
	 * @return int last insert id
	 */
	public function getLastInsertId(){
		return mysql_insert_id();
	}

	/**
	 * Returns the last error code
	 *
	 * @acces public
	 * @return int Last error code
	 */
	public function getErrorCode() {
		return $this->errno;
	}

	/**
	 * Returns the last error message
	 *
	 * @acces public
	 * @return int Last error code
	 */
	public function getErrorMessage() {
		return $this->error;
	}
}
?>
