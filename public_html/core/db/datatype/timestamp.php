<?php
/**
 * Timestamp data handler
 *
 * Validates a timestamp value. Checks if value is a timestamp
 *
 * Gives a DB representation of a variable of this type for storing purpose
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class DataTypeTimestamp implements DataType{
	
	function validate($value,$options){
		global $service;
		$log =& $service->get('log');
		if (intval($value) != $value) {
            if (!strtotime($value)) {
                $log->add("DataTypeTimestamp::validate: '$value' is not in defined options set.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
                return false;						
            }else {
                $log->add("DataTypeTimestamp::validate: $value is not an integer.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
                return false;                
            }
		}
		return true;
	}
	
	function retrieve($value,$options){
		return intval($value);
	}	

	function store($value,$options){
        if (!is_numeric($value)) {
            $value = strtotime($value);
        }
		return intval($value);
	}
}
