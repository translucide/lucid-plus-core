<?php
/**
 * Integer data handler
 *
 * Validates an array of integer value. Checks if value is an array made
 * uniquely of integers and if its length is not greather than its maximum
 * length and if values are all present in defined options
 *
 * Gives a DB representation of a variable of this type for storing purpose
 *
 * Gives an array representation from a DB representation of values of this type
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class DataTypeInteger implements DataType{
	
	function validate($value,$options){
		global $service;
		$log =& $service->get('log');
		if (intval($v) != $v) {
			$log->add("DataTypeInteger::validate: $v is not an integer.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;
		}
		
		if ($options['length'] != false && strlen($value) > $options['length']) {
			$log->add("DataTypeInteger::validate: length of $value is greater than {$options['length']}.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;	
		}
		
		if (is_array($options['options']) && !in_array($v,$options['options'])) {
			$log->add("DataTypeInteger::validate: '$v' is not in defined options set.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;						
		}

		if (isset($options['min']) && $value < $options['min']) {
			$log->add("DataTypeInteger::validate: $value is smaller than allowed minimum which is {$options['min']}.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;
		}
		
		if (isset($options['max']) && $value > $options['max']) {
			$log->add("DataTypeInteger::validate: $value is greather than allowed maximum which is {$options['max']}.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;
		}
		
		return true;
	}
	
	function retrieve($value,$options){
		return intval($value);
	}
    
	function store($value,$options){
        $value = str_replace([' ',','],'',$value);
		return intval($value);
	}	
}
