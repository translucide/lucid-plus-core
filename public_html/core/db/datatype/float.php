<?php
/**
 * Floating point data handler
 *
 * Validates an float value. Gives db and php representations of floats.
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class DataTypeFloat implements DataType{
	
	function validate($value, $options){
		global $service;
		$log =& $service->get('log');
		if (is_array($value) || is_object($value)) {
			$log->add("DataTypeFloat::validate: value an object or an array and should be a string.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;
		}
		$maxlength = $options['length'];
		if (strlen(str_replace(['.',',',' '],'',$value)) > $maxlength) {
			$log->add("DataTypeFloat::validate: length of float '$value' is greater than allowed length.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;	
		}
		return true;
	}

    
	function retrieve($value,$options){
        if (isset($options['onretrieve'])) {
            $value = $options['onretrieve']($value);
        }
		return $value;
	}	

	function store($value,$options){
        if (isset($options['onstore'])) {
            $value = $options['onstore']($value);
        }
        else $value = str_replace([',',' '],'',$value);
        if (!is_numeric($value)) $value = 0;
		return $value;
	}	
}
