<?php
/**
 * IP data handler
 *
 * Validates an IP address value.
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class DataTypeIp implements DataType{
	
	function validate($value,$options){
		global $service;
		$log =& $service->get('log');
		
		//IPv4
		if (strpos($value,'.')) {
			$ip = explode('.',$value);
			if (is_array($ip) && count($ip) == 4) {
				$valid = true;
				foreach($ip as $v) {
					if (!(is_numeric($v) && $v < 255 && $v > 0)){
						$log->add("DataTypeIp::validate: $value is not a valid IP address.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
						$valid = false;
					}
				}
				return $valid;
			}
		}
		//Ipv6
		else {
			$ip = explode(':',$value);
			if (is_array($ip) && count($ip) == 8) {
				$valid = true;
				foreach($ip as $v) {
					if (!(is_numeric('0x'.$v) && $v <= 0xFFFF && $v >= 0)){
						$log->add("DataTypeIp::validate: $value is not a valid IP address.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
						$valid = false;
					}
				}
				return $valid;
			}
		}		
		return false;
	}
	
	function retrieve($value,$options){
		return $value;
	}	

	function store($value,$options){
		return $value;
	}	
}
