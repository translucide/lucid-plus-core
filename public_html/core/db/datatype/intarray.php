<?php
/**
 * Array of integers data handler
 *
 * Validates an array of integer value. Checks if value is an array made
 * uniquely of integers and if its length is not greather than its maximum
 * length and if values are all present in defined options
 *
 * Gives a DB representation of a variable of this type for storing purpose
 *
 * Gives an array representation from a DB representation of values of this type
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class DataTypeIntarray implements DataType{
	
	function validate($value, $options){
		global $service;
		$log =& $service->get('log');
		if (!is_array($value)) {
			$log->add("DataTypeIntarray::validate: $value is not an array.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;
		}
		foreach ($value as $v){
			if (intval($v) != $v) {
				$log->add("DataTypeIntarray::validate: $v is not an integer.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
				return false;
			}
			if (isset($options['min']) && $v < $options['min']) {
				$log->add("DataTypeIntarray::validate: $v is smaller than allowed minimum which is {$options['min']}.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
				return false;
			}
			if (isset($options['max']) && $v > $options['max']) {
				$log->add("DataTypeIntarray::validate: $v is greather than allowed maximum which is {$options['max']}.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
				return false;
			}
		}
		if ($options['length'] && count($value) > $options['length']) {
			$log->add("DataTypeIntarray::validate: length of array is greater than {$options['length']}.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;	
		}
		if (is_array($options['options'])) {
			foreach ($value as $k => $v) {
				if (!in_array($v,$options['options'])) {
					$log->add("DataTypeIntarray::validate: '$v' is not in defined options set.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
					return false;						
				}
			}
		}
		return true;
	}
	
	function retrieve($value,$options){
		$array = explode(',',substr($value,1,-1));
		foreach ($array as $k => $v) {
			if (!is_numeric($v)) {
				unset($array[$k]);
			}
		}
		return $array;
	}	

	function store($value,$options){
		if (is_array($value)) {
			if (count($value) == 0) return '';
			foreach ($value as $k => $v) {
				if (!is_numeric($v)) unset($value[$k]);
			}
		}
		return ','.implode(',',$value).',';
	}	
}
