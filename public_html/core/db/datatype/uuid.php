<?php
/**
 * UUID data handler
 *
 * Validates a uuid value, uses a short base64 encoded representation of a UUID
 *
 * Gives a DB representation of a variable of this type for storing purpose
 *
 * Gives a php representation from a DB representation of values of this type
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
class DataTypeUuid implements DataType{

	function validate($value,$options){
		global $service;
		$log =& $service->get('log');
		if (strlen($value) != 22 && strlen($value) != 1) {
			$log->add("DataTypeUuid::validate: $value is not a valid UUID.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;
		}
		return true;
	}

	function retrieve($value,$options){
		return $value;
	}

	function store($value,$options){
		return $value;
	}
}
