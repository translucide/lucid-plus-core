<?php
/**
 * Array of uuids data handler
 *
 * Validates an array of uuid value. Checks if value is an array made
 * uniquely of uuids and if its length is not greather than its maximum
 * length and if values are all present in defined options
 *
 * Gives a DB representation of a variable of this type for storing purpose
 *
 * Gives an array representation from a DB representation of values of this type
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class DataTypeUuidarray implements DataType{

	function validate($value, $options){
		global $service;
		$log =& $service->get('log');
		if (!is_array($value)) {
			$log->add("DataTypeUuidarray::validate: $value is not an array.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;
		}
		foreach ($value as $v){
			if (strlen($v) != 22 && strlen($v) != 1) {
				$log->add("DataTypeUuidarray::validate: $v is not 22 chars long or 1 char long.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
				return false;
			}
			if (strspn($v, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_") != 22  && strlen($v) != 1){
				$log->add("DataTypeUuidarray::validate: \"$v\" uses non allowed chars. It should contain only [A-Za-z0-9-_] chars.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
				return false;
			}
		}
		if ($options['length'] && count($value) > $options['length']) {
			$log->add("DataTypeUuidarray::validate: length of array is greater than {$options['length']}.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;
		}
		return true;
	}

	function retrieve($value,$options){
		$array = explode(',',substr($value,1,-1));
		return $array;
	}

	function store($value,$options){
		if (is_array($value)) {
			if (count($value) == 0) return '';
		}
		return ','.implode(',',$value).',';
	}
}
