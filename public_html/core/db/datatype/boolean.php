<?php
/**
 * Boolean data handler
 *
 * Validates a boolean value. Checks if value is either 1 or 0
 *
 * Gives a DB representation of a variable of this type for storing purpose
 *
 * Gives a php representation from a DB representation of values of this type
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class DataTypeBoolean implements DataType{

	function validate($value,$options){
		global $service;
		$log =& $service->get('log');
		if (intval($v) != $v) {
			$log->add("DataTypeBoolean::validate: $v is not an integer.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;
		}
		if (!($v == 0 || $v == 1)) {
			$log->add("DataTypeBoolean::validate: $v is not a boolean value (0 or 1).",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;
		}
		return true;
	}

	function retrieve($value,$options){
		return (intval($value)> 0)?1:0;
	}

	function store($value,$options){
		return $value;
	}
}
