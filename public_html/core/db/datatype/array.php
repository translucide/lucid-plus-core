<?php
/**
 * Array data handler
 *
 * Validates an array value. Checks if value is an array and if its length
 * is not greather than its maximum length and if values are all present
 * in defined options
 *
 * Gives a DB representation of a variable of this type for storing purpose
 *
 * Gives an array representation from a DB representation of values of this type
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class DataTypeArray implements DataType{

	function validate($value, $options){
		global $service;
		if (!is_array($value)) {
			$service->get('log')->add("DataTypeArray::validate: $value is not an array.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;
		}
		
		if ($options['length'] && count($value) > $options['length']) {
			$service->get('log')->add("DataTypeArray::validate: length of array is greater than {$options['length']}.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;	
		}
		
		if (is_array($options['options'])) {
			foreach ($value as $v){
				if (!in_array($v,$options['options'])) {
					$service->get('log')->add("DataTypeArray::validate: '$v' is not in defined options set.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
					return false;
				}
			}
		}
		return true;
	}

	function retrieve($value,$options){
        if (isset($options['onretrieve'])) {
            return $options['onretrieve']($value);
        }
        $val = unserialize($value);
        if ($val === false) $val = json_decode($value,true);            
		return $val;
	}

	function store($value,$options){
        if (isset($options['onstore'])) {
            return $options['onstore']($value);
        }
        else return json_encode($value);
	}
}
