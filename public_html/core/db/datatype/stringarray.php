<?php
/**
 * Array of strings data handler
 *
 * Validates an array of strings value. Checks if value is an array made
 * uniquely of strings and if its length is not greather than its maximum
 * length and if values are all present in defined options
 *
 * Gives a DB representation of a variable of this type for storing purpose
 *
 * Gives an array representation from a DB representation of values of this type
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class DataTypeStringarray implements DataType{
	
	function validate($value, $options){
		global $service;
		$log =& $service->get('log');
		echo 'checking data';
		if (!is_array($value)) {
			$log->add("DataTypeStringarray::validate: $value is not an array.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;
		}
		foreach ($value as $v){
			if (strpos($v,',') !== false) {
				$log->add("DataTypeStringarray::validate: $v cannot contain ',' as this is a separator caracter.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
				return false;
			}
        }
		if ($options['length'] && count($value) > $options['length']) {
			$log->add("DataTypeStringarray::validate: length of array is greater than {$options['length']}.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;	
		}
		if (is_array($options['options'])) {
			foreach ($value as $k => $v) {
				if (!in_array($v,$options['options'])) {
					$log->add("DataTypeStringarray::validate: '$v' is not in defined options set.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
					return false;						
				}
			}
		}
		echo 'return true;';
		return true;
	}
	
	function retrieve($value,$options){
		$array = explode(',',substr($value,1,-1));
		return $array;
	}	

	function store($value,$options){
		if (is_array($value)) {
			if (count($value) == 0) return '';
		}
		return ','.implode(',',$value).',';
	}	
}
