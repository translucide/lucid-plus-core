<?php
/**
 * Strings data handler
 *
 * Validates an string value. Gives db and php representations of strings.
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class DataTypeString implements DataType{
	
	function validate($value, $options){
		global $service;
		$log =& $service->get('log');
		if (is_array($value) || is_object($value)) {
			$log->add("DataTypeString::validate: value an object or an array and should be a string.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;
		}
		if ($options['length'] && strlen($value) > $options['length']) {
			$log->add("DataTypeString::validate: length of string '$value' is greater than {$options['length']}.",__FILE__,__LINE__,LOG_ERR, LOG_FILE_DATAINTEGRITY);
			return false;	
		}
		return true;
	}
	
	function retrieve($value,$options){
		return $value;
	}	

	function store($value,$options){
		return $value;
	}	
}
