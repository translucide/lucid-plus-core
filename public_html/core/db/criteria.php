<?php
/**
 You may not change or alter any portion of this comment or credits
 of supporting developers from this source code or any supporting source code
 which is considered copyrighted (c) material of the original comment or credit authors.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/**
 * Xoops Editor usage uuide
 *
 * @copyright       The XOOPS Project http://sourceforge.net/projects/xoops/
 * @license         http://www.fsf.org/copyleft/gpl.html GNU public license
 * @package         kernel
 * @subpackage      class
 * @since           2.0.0
 * @author          Kazumi Ono <onokazu@xoops.org>
 * @author          Nathan Dial
 * @author          Taiwen Jiang <phppp@users.sourceforge.net>
 * @author          John Neill <catzwolf@xoops.org>
 * @version         criteria.php 2071 2008-09-12 07:12:34Z phppp $
 */

/**
 * A criteria (grammar?) for a database query.
 *
 * Abstract base class should never be instantiated directly.
 *
 * @abstract
 * @package kernel
 * @subpackage database
 * @author Kazumi Ono <onokazu@xoops.org>
 * @copyright copyright (c) 2000-2003 XOOPS.org
 */
if (!defined('SYS_CORE_DB_CRITERIA_LOCK')) {
    global $SYS_CORE_DB_CRITERIA_INDEX;
    $SYS_CORE_DB_CRITERIA_INDEX = 0;
    define('SYS_CORE_DB_CRITERIA_LOCK',1);
}

class CriteriaElement extends BaseClass
{
    /**
     * Columns to select
     *
     * @var array $selection
     */
    var $selection = array();

    /**
     * Sort order
     *
     * @var array
     */
    var $order = array();

    /**
     *
     * @var array
     */
    var $sort = array();

    /**
     * Number of records to retrieve
     *
     * @var int
     */
    var $limit = 0;

    /**
     * Offset of first record
     *
     * @var int
     */
    var $start = 0;

    /**
     *
     * @var string
     */
    var $groupby = '';

    /**
     * Left join setup
     *
     * @var array
     **/
    var $join = array();

    /**
     * Constructor
     */
    function __construct()
    {
    }

    /**
     * Render the criteria element
     */
    function render()
    {
    }

    /**
     *
     * @param string $sort
     * @param string $order
     * @param mixed $position Numerical or string: first, last
     */
    function setSort($sort,$order = 'ASC',$position='last')
    {
        $order = strtoupper($order);
        if ($position == 'last') {
           $this->sort[] = $sort;
           $this->order[] = ($order == 'ASC' || $order == 'DESC') ? strtoupper($order) : 'ASC';
        }
        if ($position == 'first') {
           array_unshift($this->sort,$sort);
           array_unshift($this->order,($order == 'ASC' || $order == 'DESC') ? strtoupper($order) : 'ASC');
        }
    }

    /**
     *
     * @return string
     */
    function getSort($asArray=false)
    {
        if ($asArray) return $this->sort;
        $sortstr = '';
        foreach ($this->sort as $k => $v) {
            if ($k > 0) $sortstr .= ', ';
            $sortstr .= $this->sort[$k].' '.$this->order[$k];
        }
        return $sortstr;
    }

    /**
     * @param string $order
     */
    function setOrder($order)
    {
        $order = strtoupper($order);
        if ($order == 'ASC' || $order == 'DESC') $this->order[count($this->order)-1] = $order;
    }

    /**
     * @deprecated since 0.2
     * @return string
     */
    function getOrder($asArray = false)
    {
        return ($asArray)?$this->order:'';
    }

    /**
     *
     * @param int $limit
     */
    function setLimit($limit = 0)
    {
        $this->limit = intval($limit);
    }

    /**
     *
     * @return int
     */
    function getLimit()
    {
        return $this->limit;
    }

    /**
     *
     * @param int $start
     */
    function setStart($start = 0)
    {
        $this->start = intval($start);
    }

    /**
     *
     * @return int
     */
    function getStart()
    {
        return $this->start;
    }

    /**
     *
     * @param string $group
     */
    function setGroupby($group)
    {
        $this->groupby = $group;
    }

    /**
     *
     * @return string
     */
    function getGroupby()
    {
        return $this->groupby ? " GROUP BY {$this->groupby}" : "";
    }

    /**
     *
     * @param array $selection
     */
    function setSelection($selection) {
        $this->selection = $selection;
    }
    function select($selection){
        $this->setSelection($selection);
    }
    /**
     *
     * @param string $selection
     */
    function getSelection() {
        $selection = '*';
        if (is_array($this->selection)){
            if (count($this->selection) > 0) {
                $selection = '';
                foreach ($this->selection as $k => $v) {
                    if ($k > 0) $selection .= ', ';
                    if (strpos($v,'`') !== false) {
                        $selection .= $v;
                    }
                    if (strpos($v,'`') === false && strpos($v,':') !== false) {
                        $v = explode(':',$v);
                        if (strpos($v[0],'(') !== false && strpos($v,')') !== false) {
                            $selection .= $v[0];
                        }else $selection .= '`'.$v[0].'`';
                        $selection .= ' AS `'.$v[1].'`';
                    }
                    if (strpos($v,'`') === false && strpos($v,':') === false) {
                        if (strpos($v,'(') !== false && strpos($v,')') !== false) $selection .= $v;
                        else $selection .= '`'.$v.'`';
                    }
                }
            }
        }
        else {
            if (strlen($this->selection) > 0) $selection = $this->selection;
        }
        return $selection;
    }

    /**
     * Setup LEFT JOIN
     *
     * Usage:
     * ->join('tablename:t2','col1 = col2');
     * ->join('tablename','col1 = col2');
     * ->join('tablename','col1','col2');
     *
     * @access public
     * @param string $table
     * @param string $source
     * @param string $destination
     **/
    function join($table,$condition){
        global $service;
        if (is_array($condition)) {
            $condition = '`'.$condition[0].'` = `'.$condition[1].'`';
        }
        $this->join[] = array(
            'table' => $table,
            'condition' => $condition
        );
    }

/**
 * *#@-
 */
}

/**
 * Collection of multiple {@link CriteriaElement}s
 *
 * @package kernel
 * @subpackage database
 * @author Kazumi Ono <onokazu@xoops.org>
 * @copyright copyright (c) 2000-2003 XOOPS.org
 */
class CriteriaCompo extends CriteriaElement
{
    /**
     * The elements of the collection
     *
     * @var array Array of {@link CriteriaElement} objects
     */
    var $criteriaElements = array();

    /**
     * Conditions
     *
     * @var array
     */
    var $conditions = array();

    /**
     * Constructor
     *
     * @param object $ele
     * @param string $condition
     */
    function __construct($ele = null, $condition = 'AND')
    {
        if (isset($ele)) {
            $this->add($ele, $condition);
        }
    }

    /**
     * Adds a selection to the criteria
     *
     * @param string $column
     * @param mixed $selection An int, an int array, false or a Criteria object
     **/
    function &in($column,$sel){
        if ($sel == false) return;
        if (!is_object($sel)) {
            if (!is_array($sel)) $sel = array($sel);
            $this->add(new Criteria($column,'(\''.implode('\',\'',$sel).'\')','IN'));
        }
        else $this->add($sel);
        return $this;
    }

    /**
     * Add an element
     *
     * Usage examples :
     * $criteriaCompo->add('col1','value','>','AND');
     * $criteriaCompo->add(new Criteria('col1','value','>'),'AND');
     * $criteriaCompo->add(['col1','value','>'],'AND');
     *
     * @param object $criteriaElement Criteria Element Object, Array or column name
     * @param string $condition Condition or value
     * @param string $operator Operator, only in use when $criteriaElement parameter is a string.
     * @param string $optcondition Optional condition, used when $criteriaElement is a string.
     * @return object reference to this collection
     */
    function &add($criteriaElement, $condition = 'AND', $operator='=', $optcondition='AND')
    {
        if (!is_array($criteriaElement)  && !is_object($criteriaElement)) {
            $criteriaElement = new Criteria($criteriaElement,$condition,$operator);
            $condition = $optcondition;
        }
        if (is_array($criteriaElement)) {
            if (isset($criteriaElement['column'])) {
                $criteriaElement = new Criteria(
                    $criteriaElement['column'],
                    $criteriaElement['value'],
                    $criteriaElement['operator'],
                    $criteriaElement['prefix'],
                    $criteriaElement['function']
                );
            }
            else {
                $criteriaElement = new Criteria(
                    $criteriaElement[0],
                    $criteriaElement[1],
                    $criteriaElement[2],
                    $criteriaElement[3],
                    $criteriaElement[4]
                );
            }
        }
        if (is_object($criteriaElement)) {
            $this->criteriaElements[] =& $criteriaElement;
            $this->conditions[] = $condition;
        }
        return $this;
    }

    /**
     * Make the criteria into a query string
     *
     * @return string
     */
    function render()
    {
        $ret = '';
        $count = count($this->criteriaElements);
        if ($count > 0) {
            $render_string = $this->criteriaElements[0]->render();
            for($i = 1; $i < $count; $i++) {
                if (!$render = $this->criteriaElements[$i]->render())
                    continue;
                $render_string .= (empty($render_string) ? "" : ' ' . $this->conditions[$i] . ' ') . $render;
            }
            $ret = empty($render_string) ? '' : "({$render_string})";
        }
        return $ret;
    }

    /**
     * Make the criteria into a query string
     *
     * @return string
     */
    function renderPDO()
    {
        $ret = '';
		$params = array();
        $count = count($this->criteriaElements);
        if ($count > 0) {
            $renderedCrit = $this->criteriaElements[0]->renderPDO();
			$render_string = $renderedCrit['query'];
			$params = array_merge($params,$renderedCrit['params']);
            for($i = 1; $i < $count; $i++) {
                if (!$render = $this->criteriaElements[$i]->renderPDO())
                    continue;
                $render_string .= (empty($render_string) ? "" : ' ' . $this->conditions[$i] . ' ') . $render['query'];
				$params = array_merge($params,$render['params']);
            }
            $ret = empty($render_string) ? '' : "({$render_string})";
        }
        return array('query' => $ret, 'params'=>$params);
    }

    /**
     * Make the criteria into a SQL "WHERE" clause
     *
     * @return string
     */
    function renderWhere()
    {
		$return = '';
		switch(DB_TYPE) {
			case 'mysqlpdo' : $ret = $this->renderPDO(); break;
			case 'mysql' :
			default : $ret = $this->render(); break;
		}
		if (is_array($ret)) {
			$return = ($ret['query'] != '') ? 'WHERE ' . $ret['query'] : $ret['query'];
		}else $return = ($ret != '') ? 'WHERE ' . $ret : $ret;

        $join = $this->renderJoin();
        $return = ($join != '')?$join.' '.$return:$return;

        if (is_array($ret)){
			$return = array('query' => $return, 'params' => $ret['params']);
		}
		return $return;
    }

    /**
     * Generate an LDAP filter from criteria
     *
     * @return string
     * @author Nathan Dial ndial@trillion21.com
     */
    function renderLdap()
    {
        $retval = '';
        $count = count($this->criteriaElements);
        if ($count > 0) {
            $retval = $this->criteriaElements[0]->renderLdap();
            for($i = 1; $i < $count; $i++) {
                $cond = strtoupper($this->conditions[$i]);
                $op = ($cond == "OR") ? "|" : "&";
                $retval = "({$op}{$retval}" . $this->criteriaElements[$i]->renderLdap() . ")";
            }
        }
        return $retval;
    }

    function renderPHP(){
        $ret = '';
        $count = count($this->criteriaElements);
        if ($count > 0) {
            $render_string = $this->criteriaElements[0]->renderPHP();
            for($i = 1; $i < $count; $i++) {
                if (!$render = $this->criteriaElements[$i]->renderPHP())
                    continue;
                $condition = (strtoupper($this->conditions[$i]) == 'OR')?'||' : '&&';
                $render_string .= (empty($render_string) ? "" : ' ' . $condition . ' ') . $render;
            }
            $ret = empty($render_string) ? '' : "({$render_string})";
        }
        return $ret;
    }

    function count(){
        return count($this->criteriaElements);
    }

    function renderSelect(){
        return 'SELECT '.$this->getSelection();
    }

    function renderJoin(){
        global $service;
        $join = '';
        foreach ($this->join as $j){
            $tbl = $j['table'];
            if (strpos($tbl,':') !== false && substr_count($tbl,':') == 1) {
                $tmp = explode(':',$tbl);
                $tbl = '`'.$service->get('Db')->prefix($tmp[0]).'` `'.$tmp[1].'`';
            }
            if (strpos($tbl,'`') === false) {
                $tbl = '`'.$service->get('Db')->prefix($tbl).'`';
            }
            if ($j != array()) {
                $join .= 'LEFT JOIN '.$tbl.' ON '.$j['condition'];
            }
        }
        return $join;
    }
}

/**
 * A single criteria
 *
 * @package kernel
 * @subpackage database
 * @author Kazumi Ono <onokazu@xoops.org>
 * @copyright copyright (c) 2000-2003 XOOPS.org
 */
class Criteria extends CriteriaElement
{
    /**
     * Prefix to use for all column names. Prefix + _ will be used.
     *
     * @access private
     *
     * @var string
     */
    private $prefix;

    /**
     * Function to use
     *
     * @access private
     *
     * @var string $function
     */
    private $function;

    /**
     * The column on which we are basing this criteria
     *
     * @access private
     *
     * @var string $column
     */
    private $column;

    /**
     * Comparison operator to use
     *
     * @access private
     *
     * @var string $operator
     */
    private $operator;

    /**
     * Value to compare to
     *
     * @access private
     *
     * @var $value
     */
    private $value;

	/**
	 * Value type
	 *
	 * @access private
	 *
	 * @var $value
	 */
	private $type;

	/**
	 * Value length
	 *
	 * @access private
	 *
	 * @var $length
	 */
	private $length;

	/**
	 * Parameter name (for PDO like queries)
	 *
	 * @access private
	 *
	 * @var $paramname
	 */
	private $paramname;

    /**
     * Constructor
     *
     * @access public
     *
     * @param string $column
     * @param string $value
     * @param string $operator
     * @param string $prefix
     * @param string $function
     * @return void Criteria()
     */
    public function __construct($column, $value = '', $operator = '=', $prefix = '', $function = '')
    {
        global $SYS_CORE_DB_CRITERIA_INDEX;
        $this->prefix = $prefix;
        $this->function = $function;
        $this->column = $column;
        $this->value = $value;
		$this->paramname = 'p_'.$SYS_CORE_DB_CRITERIA_INDEX;
        $this->operator = $operator;
        $SYS_CORE_DB_CRITERIA_INDEX++;
    }

	/**
	 * Sets the value type for PDO prepared queries
	 *
	 * @access public
	 * @param string $type
	 * @param int $length
	 * @return void setType()
	 */
	public function setType($type,$length){
		$this->type = $type;
		$this->length = $length;
	}

    /**
     * Generates an sql condition string
     *
     * @access public
     *
     * @return string
     */
    public function render()
    {
        $clause = (!empty($this->prefix) ? "{$this->prefix}_" : "") . $this->column;
        if (!empty($this->function)) {
            $clause = sprintf($this->function, $clause);
        }
        if (in_array(strtoupper($this->operator), array('IS NULL' , 'IS NOT NULL'))) {
            $clause .= ' ' . $this->operator;
        } else {
            if ('' === ($value = trim($this->value))) {
                //return '';
            }
            if (!in_array(strtoupper($this->operator), array('IN' , 'NOT IN'))) {
                if ((substr($value, 0, 1) != '`') && (substr($value, - 1) != '`')) {
                    $value = "'{$value}'";
                } else if (!preg_match('/^[a-zA-Z0-9_\.\-`]*$/', $value)) {
                    $value = '``';
                }
            }
            $clause .= " {$this->operator} {$value}";
        }
        return $clause;
    }

    /**
     * Generates an sql condition string
     *
     * @access public
     *
     * @return string
     */
    public function renderPDO()
    {
		$paramname = $this->paramname;
		$clause = (!empty($this->prefix) ? "{$this->prefix}_" : "") . $this->column;
        if (!empty($this->function)) {
            $clause = sprintf($this->function, $clause);
        }
        if (in_array(strtoupper($this->operator), array('IS NULL' , 'IS NOT NULL'))) {
            $clause .= ' ' . $this->operator;
        } else {
			$isnotin = false;
            $value = trim($this->value);
            /*if ('' === ($value = trim($this->value))) {
                return '';
            }*/
            if (!in_array(strtoupper($this->operator), array('IN' , 'NOT IN', 'LIKE'))) {
                /*if (!preg_match('/^[a-zA-Z0-9_\.\-`]*$/', $value)) {
                    $value = '';
                }*/
            }
			if (in_array(strtoupper($this->operator), array('IN' , 'NOT IN'))) {
				$isnotin = true;
				//IN / NOT IN clause
				if (substr($value,0,1) == '(' && substr($value,-1,1) == ')') {
					$value = substr($value,1,-1);
				}
				/*if (substr($value,0,1) == '\'' && substr($value,-1,1) == '\'') {
					$value = substr($value,1,-1);
				}*/
				$values = explode(',',$value);
				$params = array();
				$paramsstr = '';
				$i=0;
				foreach($values as $k => $v){
                    if (substr($v,0,1) == '\'' && substr($v,-1,1) == '\'') {
                    	$v = substr($v,1,-1);
                    }
					if ($i > 0) $paramsstr .= ', :';
					$params[] = array(
						'value' => $v,
						'paramname' => ':'.$paramname.'_'.$k,
						'type' => $this->type,
						'length' => $this->length
					);
					$paramsstr.= $paramname.'_'.$k;
					$i++;
				}
				$paramname = $paramsstr;
			}
            if ($isnotin) $clause .= " {$this->operator} (:{$paramname})";
			else $clause .= " {$this->operator} :{$paramname}";
        }
		if (!isset($params)) {
			$params = array(0 => array('value' => $value, 'paramname' => ':'.$paramname, 'type' => $this->type, 'length' => $this->length));
		}
        return array('query' => $clause, 'params' => $params);
    }

    /**
     * Returns an LDAP filter from criteria
     *
     * @access public
     *
     * @return string
     */
    public function renderLdap()
    {
        if ($this->operator == '>') {
            $this->operator = '>=';
        }
        if ($this->operator == '<') {
            $this->operator = '<=';
        }

        if ($this->operator == '!=' || $this->operator == '<>') {
            $operator = '=';
            $clause = "(!(" . $this->column . $operator . $this->value . "))";
        } else {
            if ($this->operator == 'IN') {
                $newvalue = str_replace(array('(' , ')'), '', $this->value);
                $tab = explode(',', $newvalue);
                foreach ($tab as $uid) {
                    $clause .= "({$this->column}={$uid})";
                }
                $clause = '(|' . $clause . ')';
            } else {
                $clause = "(" . $this->column . $this->operator . $this->value . ")";
            }
        }
        return $clause;
    }

    /**
     * Renders this criteria to a PHP language string that can be evald
     *
     * @access public
     *
     * @return string The rendered criteria to a PHP string for eval
     */
    public function renderPHP(){
        $clause = '';
        $operator = $this->operator;

        switch($operator) {
            case 'IS NULL' : $operator = '== NULL';break;
            case 'NOT IS NULL' : $operator = '!= NULL';break;
            case '=' : $operator = '==';break;
            case '<>' : $operator = '!=';break;
        }
        if (!in_array(strtoupper($operator), array('IN' , 'NOT IN','LIKE','NOT LIKE'))) {
            $clause = '${%s}[\''.(!empty($this->prefix) ? "{$this->prefix}_" : "") . $this->column.'\']';
        }
        /*if (!empty($this->function)) {
            $clause = sprintf($this->function, $clause); //@TODO : function support in criteria?
        }*/
		$isnotin = false;
        if (in_array($operator, array('== NULL' , '!= NULL'))) {
            $clause .= ' '.strtolower($operator);
        } else {
            if ('' === ($value = trim($this->value))) {
               // return '';
            }
            if (!in_array(strtoupper($operator), array('IN' , 'NOT IN','LIKE','NOT LIKE'))) {
                if ((substr($value, 0, 1) != '`') && (substr($value, - 1) != '`')) {
                    $value = "'{$value}'";
                } else {
                    $value = substr($value,1,-1);
                }
            }else {
				$isnotin = true;
                if (in_array(strtoupper($operator), array('IN' , 'NOT IN'))) {
                    $op = strtoupper($operator);
                    //@TODO : bettter logic needed here for IN / NOT IN clause
                    if (substr($value,0,1) == '\'' && substr($value,-1,1) == '\'') {
                        $value = substr($value,1,-1);
                    }
                    if (substr($value,0,1) != '(' && substr($value,-1,1) != ')') {
                        $value = '('.$value.')';
                    }
                    $operator = '';
                    if (strtoupper($op) == 'NOT IN') $operator = '!';
                    $operator .= 'in_array(${%s}[\''.(!empty($this->prefix) ? "{$this->prefix}_" : "").$this->column.'\'],array'.$value.')';
                }
                if (in_array(strtoupper($operator), array('LIKE' , 'NOT LIKE'))) {
                    $op = strtoupper($operator);
                    //@TODO : bettter logic needed here for IN / NOT IN clause
                    if (substr($value,0,1) == '\'' && substr($value,-1,1) == '\'') {
                        $value = substr($value,1,-1);
                    }
                    if (substr($value,0,1) == '%' && substr($value,-1,1) == '%') {
                        $value = substr($value,1,-1);
                    }
                    if (substr($value,0,1) == ',' && substr($value,-1,1) == ',') {
                        $value = substr($value,1,-1);
                    }
                    $operator = '';
                    $operator .= 'stripos(${%s}[\''.(!empty($this->prefix) ? "{$this->prefix}_" : "").$this->column.'\'],\''.addslashes($value).'\')';
                    if (strtoupper($op) == 'NOT LIKE') $operator .= ' === false';
                }
				$value = '';
            }
			//if ($operator == '=') $operator = '==';
            $clause .= " {$operator} {$value} ";
        }
        return $clause;
    }

    /**
     * Return a SQL "WHERE" clause
     *
     * @access public
     *
     * @return string
     */
    public function renderWhere()
    {
		switch(DB_TYPE) {
			case 'mysqlpdo' : $cond = $this->renderPDO(); break;
			case 'mysql' :
			default : $cond = $this->render(); break;
		}

        $join = $this->renderJoin();
        if (is_array($cond)) $cond['query'] = ($join != '')?$join.' '.$cond['query']:$cond['query'];
        else $cond = ($join != '')?$join.' '.$cond:$cond;

		if (is_array($cond)) {
			$cond['query'] = empty($cond['query']) ? '' : "WHERE ".$cond['query'];
		}else $cond = empty($cond) ? '' : "WHERE ".$cond;
		return $cond;
    }

    function renderSelect(){
        return 'SELECT '.$this->getSelection();
    }


    function renderJoin(){
        global $service;
        $join = '';
        foreach ($this->join as $j) {
            $tbl = $j['table'];
            if (strpos($tbl,':') !== false && substr_count($tbl,':') == 1) {
                $tmp = explode(':',$tbl);
                $tbl = '`'.$service->get('Db')->prefix($tmp[0]).'` `'.$tmp[1].'`';
            }
            if (strpos($tbl,'`') === false) {
                $tbl = '`'.$service->get('Db')->prefix($tbl).'`';
            }
            if ($j != array()) {
                $join .= 'LEFT JOIN '.$tbl.' ON '.$j['condition'];
            }
        }
        return $join;
    }
}
?>
