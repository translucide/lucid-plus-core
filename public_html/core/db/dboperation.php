<?php
/**
 * DBOperation
 *
 * Covers some basic database operations such as :
 * - Duplicating a record and its childrens
 * 
 * January 26, 2016
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */
class DBOperation {
    
    /**
     * @access private
     * @var array $obj The objects beeing worked on
     */
    private $objs;
    
    /**
     * @access private
     * @var array $infos The associated object infos (if any)
     */
    private $infos;
    
    /**
     * DBOperation constructor.
     *
     * @param array $obj The object to work on/from
     */
    public function __construct($objs = array(),$infos = array()){
        $this->objs = $objs;
        $this->infos = $infos;
    }

	/**
	 * Returns an array containing widget informations
	 *
	 * @public
	 * @param string $info The information needed
	 * @return mixed Returns the infos array or just one info if set
	 */
	public function getInfo($info=''){
		if (isset($this->infos[$info])) return $this->infos[$info];
		return $this->infos;
	}
	
	/**
	 * Sets widget informations
	 *
	 * @protected
	 * @param mixed $info The information
	 * @return void
	 */
	protected function setInfo($info='',$value=''){
		if (is_array($info)) $this->infos = $info;
		else $this->infos[$info] = $value;
	}

    /**
     * Sets the objects to work on/from
     *
     * @access public
     * @param array $objs The objects to work on
     */
    public function setObjects($objs){
        $this->objs = $objs;
    }
	
	/**
	 * Copy objects to a new ID/OBJID
	 *
	 * Change their ID/OBJID to a new one
	 * @return array An array containing a new ObjID for each input obj.
	 */
	public function copy() {
		//1. Find obj types and create new store object
		$store = false;
		$result = true;
		if (is_object($this->objs[0])) {
			$storeName = ucfirst(strtolower($this->objs[0]->getType())).'Store';
			$store = new $storeName();
			$store->setOption('ignorelangs',true);
		}
		else return false;
		
		$objidfield = $store->getObjIdField();
		$objs = $this->objs;
		$ids = array();
		while(count($objs) > 0) {
			//1. Group objs by objid
			$thisObj = array();
			foreach ($objs as $dispatchk => $dispatchv) {
				$thisObj[] = $dispatchv;
				$ids[$dispatchk] = 0;
				unset($objs[$dispatchk]);
				break;
			}
			foreach ($objs as $ko => $vo) {
				if ($vo->getVar($objidfield) == $thisObj[0]->getVar($objidfield)){
					$thisObj[] = $vo;
					$ids[$ko] = 0;
					unset($objs[$ko]);
				}
			}
			
			//2. Change objid and ID, update other fields as required...
			$newobjid = $store->getNextId();
			foreach($thisObj as $vk => $vv) {
				$thisObj[$vk]->setVar($objidfield,$newobjid);
				$thisObj[$vk]->setVar($store->getIdField(),0);
			}
			foreach ($ids as $k => $v){
				if ($v == 0) $ids[$k] = $newobjid;
			}
			$result = $result && $store->save($thisObj);
		}
		return ($result)?$ids:false;
	}
	/**
	 * Copy children handler function called on object copy
	 *
	 * Functions such as copy/paste in the admin interface will call that method
	 * to recursively copy all of this obj childrens
	 *
	 * @param int $original_id The original parent ObjID
	 * @param int $new_id The new parent ObjID
	 */
	public function copyChildren($original_id,$new_id){
		$result = true;
		$type = $this->getInfo('type');
		$name = $this->getInfo('name');
		foreach ($this->getInfo('copyoptions')['data'] as $k => $v) {
			if (isset($v['table'])) {
				$storeName = ucfirst(strtolower($v['table'])).'Store';
			}else $storeName = ucfirst(strtolower($k)).'Store';
			if (class_exists($storeName)) {
				$store = new $storeName();
				$store->setOption('ignorelangs',true);
				$criteria = new CriteriaCompo();
				foreach ($v['select'] as $vk => $vv) {
					$criteria->add(new Criteria($vk,str_replace(
						['{parentobjid}','{parenttype}','{parentname}'],
						[$original_id,$type,$name],
						$vv)));
				}
				$objs = $store->get($criteria);
				$objidfield = $store->getObjIdField();
				while(count($objs) > 0) {
					//1. Group objs by objid
					$thisObj = array();
					foreach ($objs as $dispatchk => $dispatchv) {
						$thisObj[] = $dispatchv;
						unset($objs[$dispatchk]);
						break;
					}
					foreach ($objs as $ko => $vo) {
						if ($vo->getVar($objidfield) == $thisObj[0]->getVar($objidfield)){
							$thisObj[] = $vo;
							unset($objs[$ko]);
						}
					}
					
					//2. Change objid and ID, update other fields as required...
					$newobjid = $store->getNextId();
					foreach($thisObj as $vk => $vv) {
						$thisObj[$vk]->setVar($objidfield,$newobjid);
						$thisObj[$vk]->setVar($store->getIdField(),0);
						foreach ($v['update'] as $ku => $vu) {
							$thisObj[$vk]->setVar($ku,str_replace(
								['{parentobjid}','{parenttype}','{parentname}'],
								[$new_id,$type,$name],
								$vu));
						}
					}
					$result = $result && $store->save($thisObj);
				}
			}
		}
		return $result;
	}

}
?>