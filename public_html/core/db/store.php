<?php

class Store{

	/**
	 * Store type
	 *
	 * @protected
	 *
	 * @var string $type
	 */
	protected $type;

	/**
	 * Store objects ID field name
	 *
	 * @protected
	 *
	 * @var string $id
	 */
	protected $id;

	/**
	 * Model object reference
	 *
	 * @protected
	 *
	 * @var model $model
	 */
	protected $model;

    /**
     * Stores the last inserted ID for UUID based systems
     *
     * @access protected
     * @var mixed $lastinsertid
     */
    protected $lastinsertid;

	/**
	 * Store options
	 *
	 * Currently supported options :
	 * - ignorelangs : Boolean value, whether to add language selection criteria to all
	 *   DB queries performed by this store object
	 * - ignoresites : Boolean value, whether to add site selection criteria to all
	 *   DB queries performed by this store object.
	 *
	 * @protected
	 *
	 * @var array Store-wide options
	 */
	protected $options;

	/**
	 * Data Store constructor
	 *
	 * @public
	 *
	 * @param string $type The object type
	 * @return void Store()
	 */
    public function __construct($type = ''){
		$this->type = $type;
		$this->id = $type.'_id';
		if (class_exists(ucfirst($type).'Model')) {
			$modelCls = ucfirst($type).'Model';
			$this->model = new $modelCls();
            $this->id = $this->model->getId();
			unset($modelCls);
		}else {
			global $service;
			$log =& $service->get('log');
			$log->add("Store::Store (".get_called_class().") : Model class not defined for this store type (".ucfirst($type).'Model'.")",__FILE__,__LINE__,LOG_INFO);
		}
		//Set default store options
		$this->options = array(
			'ignorelangs' => false,
			'ignoresites' => false,
			'ignoretenant' => false,
			'ignoredeleted' => false,
			'regeneratingcache' => false,
			'loadparents' => false,
			'loadchilds' => false,
            'asarray' => false,
			'returncollection' => false,
			'maxchildslevels' => 1,
            'uuidtype' => 'uuid',
            'singlerow' => false,
			'urlreplace' => true
		);
	}

	/**
	 *	Sets the model this store is associated with
	 *
	 *	@public
	 *	@return void
	 */
	public function setModel($model){
		if (is_a($model,'Model')) $this->model = $model;
	}

	/**
	 * Returns the ID field name of data records
	 *
	 * @public
	 *
	 * @return int ID field name
	 */
	public function getIdField(){
		return $this->id;
	}

	/**
	 * Returns the object type
	 *
	 * @access public
	 * @return string Object type
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Returns the ObjID field name of data records
	 *
	 * @public
	 *
	 * @return int ID field name
	 */
	public function getObjIdField(){
		return $this->type.'_objid';
	}

	/**
	 * Get objects by specifying its ID or IDs
	 *
	 * @public
	 *
	 * @param mixed $selection Object, IDs array or integer for only one ID
	 * @param Criteria $crit Optional criteria to use as base criteria
	 * @return array Returns an array of Data objects
	 */
	public function getById($selection,$crit=false) {
		if (!is_object($selection)) {
			if (!is_array($selection)) $selection = array($selection);
			if (!$crit) $crit = new Criteria($this->getIdField(),$this->inClause($selection),'IN');
			else $crit->add(new Criteria($this->getIdField(),$this->inClause($selection),'IN'));
		}
		return $this->get($crit);
	}

	/**
	 * Get objects by specifying its ID or obj IDs (for multipart objects)
	 *
	 * @public
	 *
	 * @param mixed $selection Object IDs array or integer for only one ID
	 * @return array Returns an array of Data objects
	 */
	public function getByObjId($selection) {
		if (!is_object($selection)) {
			if (!is_array($selection)) $selection = new Criteria($this->getObjIdField(),$selection);
			else $selection = new Criteria($this->getObjIdField(),$this->inClause($selection),'IN');
		}
		return $this->get($selection);
	}

	/**
	 * Get next available number for a manual autoincrement column
	 *
	 * @public
	 *
	 * @param string $column Column name of type INT
	 * @return int Returns max+1
	 */
	public function getNextId($column = '') {
		return $this->getNext($column);
	}

	/**
	 * Get next available number for a  column
	 *
	 * @public
	 *
	 * @param string $column Column name of type INT
	 * @param Criteria $crit Optionnal criteria
	 * @return int Returns max+1
	 */
	public function getNext($column = '',$crit=false) {
		global $service;
		$db =& $service->get('Db');
		if ($column == '') $column = $this->getObjIdField();
        if ($this->model->getType($column) == DATATYPE_UUID) {
            $max = $this->newUuid();
        }
        else {
            $query = 'SELECT MAX(`'.$column.'`) AS `max` FROM `'.$db->prefix($this->type).'`';
    		if (is_object($crit)) {
    			if ($this->getOption('ignorelangs') == false) $crit = $this->addLanguageCriteria($crit);
    			if ($this->getOption('ignoresites') == false) $crit = $this->addSiteCriteria($crit);
    			if ($this->getOption('ignoretenant') == false) $crit = $this->addTenantCriteria($crit);
    			if ($this->getOption('ignoredeleted') == false) $crit = $this->addDeletedCriteria($crit);
    			$whereclause = $crit->renderWhere();
    			if (is_array($whereclause)) {
    				$query .= ' '.$whereclause['query'];
    				$whereclause['query'] = $query;
    				$query = $whereclause;
    			}
    			else $query = ' '.$whereclause;
    		}
    		$result = $db->query($query);
    		$max = intval($result[0]['max']) +1;
        }
		return $max;
	}

	/**
	 * Gets an option value
	 *
	 * @public
	 *
	 * @param string $optionname
	 * @return mixed $option value
	 */
	public function getOption($optionname){
		if (isset($this->options[$optionname])) {
			return $this->options[$optionname];
		}
		global $service;
		$log =& $service->get('log');
		$log->add("Store::getOption (".get_called_class().") : Option '$optionname' was accessed without beeing set first!",__FILE__,__LINE__,E_WARNING);
		return false;
	}


	/**
	 * Sets an option value
	 *
	 * @public
	 *
	 * @param string $optionname
	 * @param mixed $value
	 * @return mixed $option value
	 */
	public function setOption($optionname,$value){
		$this->options[$optionname] = $value;
	}

	/**
	 * Get an object by specifying its ID or a criteria object.
	 *
	 * @public
	 *
	 * @param mixed $selection Object ID or criteria Object
	 * @param array $options Options on result return
	 * @return array Returns an array of Data objects
	 */
	public function get($selection=false,$options=array()) {
        if (!is_array($options)) $options = array();
        if (!isset($options['asarray'])) $options['asarray'] = $this->getOption('asarray'); //Return results as arrays
        if (!isset($options['returncollection'])) $options['returncollection'] = $this->getOption('returncollection'); //Return results as collections
        if (!isset($options['loadchilds'])) $options['loadchilds'] = $this->getOption('loadchilds'); //Load child records
        if (!isset($options['singlerow'])) $options['singlerow'] = $this->getOption('singlerow'); //Return a single result, not an array of results

		global $service;
		$db =& $service->get('Db');
		$results = false;
		$forceretrieve = true;
		if ($selection == false) {
			$selection = new Criteria($this->model->getId(),'0','!=');
		}
		if (!is_object($selection)) {
			$selection = new criteria($this->getIdField(),$selection);
		}
		if ($this->getOption('ignorelangs') == false) $selection = $this->addLanguageCriteria($selection);
		if ($this->getOption('ignoresites') == false) $selection = $this->addSiteCriteria($selection);
		if ($this->getOption('ignoretenant') == false) $selection = $this->addTenantCriteria($selection);
		if ($this->getOption('ignoredeleted') == false) $selection = $this->addDeletedCriteria($selection);
		if ($this->model->getOption('usecache') && $this->getOption('regeneratingcache') == false) {
			$cache = new DBCache($this->model);
			$results = $cache->get($selection);
			$forceretrieve = false;
			if ($results == false) {
				//Cache does not exists yet, generate it.
				$this->regenerateCache();
			}
		}
		if (!is_array($results)) {
			if ($this->model->getOption('usecache')) $forceretrieve = true;
			$whereclause = $selection->renderWhere();
            $select = $selection->renderSelect();
			if (is_array($whereclause)) {
				$query = $select.' FROM `'.$db->prefix($this->type).'` '.$whereclause['query'];
			}
			else {
				$query = $select.' FROM `'.$db->prefix($this->type).'` '.$whereclause;
			}

			$query .= $selection->getGroupby();
			if ($selection->getSort()) {
				$query .= ' ORDER BY '.$selection->getSort();
			}
			if ($selection->limit > 0 || $selection->start > 0) {
				$query .= ' LIMIT '.$selection->getStart().', '.$selection->getLimit();
			}
			if (is_array($whereclause)) {
				$whereclause['query'] = $query;
				$query = $whereclause;
			}
			$results = $db->query($query);
		}

		if (!is_array($results)) {
			return false;
		}
        if (strpos($selection->getSelection(),'*') !== false) {
            if ($options['asarray']) $results = $this->toArray($results,$forceretrieve);
            else $results = $this->toObjects($results,$forceretrieve);
        }else {
            //We got a partial object here,
            //return basic array instead of object to make sure program knows its not a full object.
            //@TODO : Partials should be returned the same way as full objects, we need to only save fields that are set.
            foreach ($results as $key => $val) {
                foreach ($val as $k => $v) if (is_numeric($k)) unset($results[$key][$k]);
            }
        }
		if ($options['loadchilds']) {
			$results = $this->getChilds($results);
		}

        //Return only the first results row
        if ($options['singlerow']) {
            if (count($results) > 0) $results = $results[0];
            else $results = false;
        }
		if ($options['returncollection'] && $results !== false) {
			$results = $this->toCollection($results);
		}

        //Log query errors
        if ($results === false || $db->getError()) {
        	$log =& $service->get('log');
            foreach ($query['params'] as $k => $v) {
                $query['query'] = str_replace($v['paramname'],$v['value'],$query['query']);
            }
            $log->add(get_called_class()."::get: ".$db->getError()." QUERY: ".$query['query'],__FILE__,__LINE__,E_WARNING);
        }
		return $results;
	}


	/**
	 * Returns how many objects match the criteria
	 *
	 * @public
	 *
	 * @param mixed $selection Object ID or criteria Object
	 * @return int Returns the objects count
	 */
	public function count($selection=false) {
		global $service;
		$db =& $service->get('Db');
		$results = false;
		$forceretrieve = true;
		if ($selection == false) {
			$selection = new Criteria($this->model->getId(),0,'!=');
		}
		if (!is_object($selection)) {
			$selection = new Criteria($this->getIdField(),$selection);
		}
		if ($this->getOption('ignorelangs') == false) $selection = $this->addLanguageCriteria($selection);
		if ($this->getOption('ignoresites') == false) $selection = $this->addSiteCriteria($selection);
		if ($this->getOption('ignoretenant') == false) $selection = $this->addTenantCriteria($selection);
		if ($this->getOption('ignoredeleted') == false) $selection = $this->addDeletedCriteria($selection);
		/*
		 * TODO : support count in cache
		 */
		/*if ($this->model->getOption('usecache') && $this->getOption('regeneratingcache') == false) {
			$cache = new DBCache($this->model);
			$results = $cache->get($selection);
			$forceretrieve = false;
			if ($results == false) {
				//Cache does not exists yet, generate it.
				$this->regenerateCache();
			}
		}*/
		if (!is_array($results)) {
			if ($this->model->getOption('usecache')) $forceretrieve = true;
			$whereclause = $selection->renderWhere();
			if (is_array($whereclause)) {
				$query = 'SELECT COUNT('.$this->model->getId().') as `count` FROM `'.$db->prefix($this->type).'` '.$whereclause['query'];
			}
			else {
				$query = 'SELECT COUNT('.$this->model->getId().') as `count` FROM `'.$db->prefix($this->type).'` '.$whereclause;
			}

			$query .= $selection->getGroupby();
			/**
			 * Does not matter, so no ordering...
			 *
			if ($selection->getSort()) {
				$query .= ' ORDER BY '.$selection->getSort();
			}
			*/
			/*
			 * Do not apply limits on count queries....
			if ($selection->limit > 0 || $selection->start > 0) {
				$query .= ' LIMIT '.$selection->getStart().', '.$selection->getLimit();
			}
			*/
			if (is_array($whereclause)) {
				$whereclause['query'] = $query;
				$query = $whereclause;
			}
			$results = $db->query($query);
		}
		if (!is_array($results)) {
			return false;
		}
		return $results[0]['count'];
	}

    /**
     * Replaces URLs by dynamic tags, so content is site agnostic.
     *
     * @access protected
     * @param $objects
     * @return array@Data Array of data objects
     **/
    protected function autoReplaceUrls($objects){
		//Replace URL tags by current site's URLs before inserting in DB.
		$s = array(UPLOADURL,THEMEURL,URL);
		$r = array('{uploadurl}','{themeurl}','{url}');
		foreach ($objects as $k => $v) {
            $vars = (is_object($v))?$v->getVars():$v;
            if (is_array($vars)) {
                foreach($vars as $krep=> $vrep) {
                    $vars[$krep] = str_replace($s,$r,$vrep);
                }
            }
			if (is_object($v)) $objects[$k]->set($vars);
			else $objects[$k] = $vars;
        }
        return $objects;
    }

	/**
	 * Saves one or more objects in DB
	 *
	 * @public
	 *
	 * @param mixed $objects An array of Data objects or just a Data object
	 * @return bool Success: true
	 */
	public function save($objects){
		global $service;
		$db =& $service->get('Db');
		if (is_object($objects) && is_a($objects,'Collection')) {
			$objects = $objects->get();
		}
		if (is_object($objects)) {
			$objects = array($objects);
		}
        $objects = $this->autoReplaceUrls($objects);
        $result = true;
        $insertObjects = array();

        //Perform all updates first, then insert new records.
        foreach ($objects as $k => $v) {
            //Update known fields with up to date values
            if ($this->model->isVar($this->type.'_modified')) {
                $v->setVar($this->type.'_modified', time());
            }
            //Update modified by
            if ($this->model->isVar($this->type.'_modifiedby')) {
                $v->setVar($this->type.'_modifiedby', $service->get('User')->getUid());
            }
            $params = array();
            $lastinsertid =$v->getVar($this->getIdField());

            //Perform an update query here....
            if ($v->getVar($this->getIdField()) && (
                $this->model->isVar($this->type.'_created') == false || $v->getVar($this->type.'_created')
            )) {
                $vars = $v->getVars();
                $data = $this->model->store($vars);
				unset($data[$this->getIdField()]);
                $query = 'UPDATE `'.$db->prefix($this->type).'` SET ';
                $i=0;
                foreach($data as $kv => $vv) {
                    if ($i > 0) $query .= ', ';
                    switch(DB_TYPE) {
                        case 'mysqlpdo' : {
                            $query .= '`'.$kv.'` = :'.$kv;
                            $params[] = array('paramname' => $kv, 'value' => $vv);
                        }break;
                        case 'mysql' :
                        default : {
                            $query .= '`'.$kv.'` = \''.str_replace("'","\\'",$vv).'\'';
                        }break;
                    }
                    $i++;
                }
                switch(DB_TYPE) {
                    case 'mysqlpdo' : {
                        $query .= ' WHERE `'.$this->getIdField().'` = :pkid LIMIT 1';
                        $params[] = array('paramname' => 'pkid', 'value' => $v->getVar($this->getIdField()));
                    }break;
                    case 'mysql' :
                    default : {
                        $query .= ' WHERE `'.$this->getIdField().'` = '.$v->getVar($this->getIdField()).' LIMIT 1';
                    }break;
                }

                if (DB_TYPE == 'mysqlpdo') $query = array('query'=>$query, 'params' => $params);
                if (!$db->query($query)) {
                    $result = false;
                }else {
                    $this->lastinsertid = $lastinsertid;
                }
            }
            else {
                $insertObjects[] = $v;
            }
        }

        //Insert new objects
        if (count($insertObjects)) {
            $res = $this->insertBatch($objects);
            if (!$res) $result = false;
        }

        $this->regenerateCache();

        //Log query errors
        if ($result === false) {
            $log =& $service->get('log');
            $log->add(get_called_class()."::save: ".$db->getError()." QUERY: ".$query['query']." (params:".implode("/",$query['params']).")",__FILE__,__LINE__,E_WARNING);
        }
		return $result;
	}

	/**
	 * Inserts many objects in DB in 1 query
	 *
	 * @public
	 *
	 * @param mixed $objects An array of Data objects or just a Data object
	 * @return bool Success: true
	 */
	public function insertBatch($objects){
		global $service;
		$db =& $service->get('Db');
		if (is_object($objects) && is_a($objects,'Collection')) {
			$objects = $objects->get();
		}
		if (is_object($objects)) {
			$objects = array($objects);
		}
        $objects = $this->autoReplaceUrls($objects);
        $result = true;

        $query = 'INSERT INTO `'.$db->prefix($this->type).'` (';
        $objects = array_values($objects);
        if (count($objects) == 0) return true;

        $vars = $objects[0]->getVars();
        $data = $this->model->store($vars);

        if ($this->model->getType($this->getIdField()) != DATATYPE_UUID) {
            unset($data[$this->getIdField()]);
        }
        $i = 0;
        foreach($data as $kv => $vv) {
            if ($i > 0) $query .= ', ';
            $query .= $kv;
            $i++;
        }
        $query .= ') VALUES ';
		$params = array();
        $i = 0;
		foreach ($objects as $k => $v) {
            if ($this->model->getType($this->getIdField()) == DATATYPE_UUID) {
                if (!$v->getVar($this->getIdField())) {
                    $lastinsertid = $this->newUuid();
                    $v->setVar($this->getIdField(), $lastinsertid);
                }
            }
            $lastinsertid = $v->getVar($this->getIdField());

			//Update known fields with up to date values
			if ($this->model->isVar($this->type.'_modified')) {
				$v->setVar($this->type.'_modified', time());
			}
			if ($this->model->isVar($this->type.'_modifiedby')) {
				$v->setVar($this->type.'_modifiedby', $service->get('User')->getUid());
			}
            //Set created field with current unix timestamp
            if ($this->model->isVar($this->type.'_created')) {
                $v->setVar($this->type.'_created', time());
            }
            //Set createdby field
            if ($this->model->isVar($this->type.'_createdby')) {
                $v->setVar($this->type.'_createdby', $service->get('User')->getUid());
            }

            $vars = $v->getVars();
            $data = $this->model->store($vars);
            if ($this->model->getType($this->getIdField()) != DATATYPE_UUID) {
                unset($data[$this->getIdField()]);
            }
            if ($k > 0) $query .= ',';
            $query .= '(';
            $j = 0;
            foreach($data as $kv => $vv) {
                if ($j > 0) $query .= ',';
                switch(DB_TYPE) {
                    case 'mysqlpdo' : {
                        $query .= ':'.$kv.$i;
                        $params[] = array('paramname' => ':'.$kv.$i, 'value' => $vv);
                    }break;
                    case 'mysql' :
                    default : $query .= '\''.$vv.'\''; break;
                }
                $i++;
                $j++;
            }
            $query .= ")";
		}
        if (DB_TYPE == 'mysqlpdo') $query = array('query'=>$query, 'params' => $params);
        if (!$db->query($query)) {
            $result = false;
        }else {
            $this->lastinsertid = $lastinsertid;
        }

        //Log query errors
        if ($result === false) {
        	$log =& $service->get('log');
            $log->add(get_called_class()."::insertBatch: ".$db->getError()." QUERY: ".$query['query']." (params:".implode("/",$query['params']).")",__FILE__,__LINE__,E_WARNING);
        }
		return $result;
	}


	/**
	 * Updates one or more columns
	 *
	 * @public
	 *
	 * @param mixed $values An associative array of column => value to apply to the selection
	 * @param Criteria $selection False to apply to all objects in table, integer to apply to an ID, or a Criteria object.
	 * @param Array $options
	 * @return bool Success: true
	 */
	public function update($values,$selection=false,$options=array()){
		global $service;
		$db =& $service->get('Db');
        $values = $this->autoReplaceUrls($values);
        $result = true;
		$query = 'UPDATE `'.$db->prefix($this->type).'` SET ';
		$data = $this->model->store($values);
		$i=0;

		//Perform an update query here....
		foreach($data as $k => $v) {
			if (isset($values[$k])) {
				if ($i > 0) $query .= ', ';
				switch(DB_TYPE) {
					case 'mysqlpdo' : {
						$query .= '`'.$k.'` = :'.$k;
						$params[] = array('paramname' => ':'.$k, 'value' => $v);
					}break;
					case 'mysql' :
					default : {
						$query .= '`'.$k.'` = \''.str_replace("'","\\'",$v).'\'';
					}break;
				}
				$i++;
			}
		}
		$forceretrieve = true;
		if ($selection == false) {
			$selection = new Criteria($this->model->getId(),0,'<>');
		}
		if (!is_object($selection)) {
			$selection = new criteria($this->getIdField(),$selection);
		}
		if ($this->getOption('ignorelangs') == false) $selection = $this->addLanguageCriteria($selection);
		if ($this->getOption('ignoresites') == false) $selection = $this->addSiteCriteria($selection);
		if ($this->getOption('ignoretenant') == false) $selection = $this->addTenantCriteria($selection);
		if ($this->getOption('ignoredeleted') == false) $selection = $this->addDeletedCriteria($selection);

		$whereclause = $selection->renderWhere();
		if (is_array($whereclause)) {
			$query .= ' '.$whereclause['query'];
			$params = array_merge($params,$whereclause['params']);
		}
		else {
			$query .= ' '.$whereclause;
		}
		$query .= $selection->getGroupby();
		if ($selection->getSort()) {
			$query .= ' ORDER BY '.$selection->getSort();
		}
		if ($selection->limit > 0 || $selection->start > 0) {
			$query .= ' LIMIT '.$selection->getStart().', '.$selection->getLimit();
		}
		if (DB_TYPE == 'mysqlpdo') $query = array('query'=>$query, 'params' => $params);
		$result = $db->query($query);
        $this->regenerateCache();

        //Log query errors
        if ($result === false) {
        	$log =& $service->get('log');
            foreach ($query['params'] as $k => $v) {
                $query['query'] = str_replace($v['paramname'],$v['value'],$query['query']);
            }
            $log->add(get_called_class()."::update: ".$db->getError()." QUERY: ".$query['query'],__FILE__,__LINE__,E_WARNING);
        }
		return $result;
	}

	/**
	 * Saves options in an array column
	 *
	 * @public
	 * @param string $target The field to save options in.
	 * @param array $optionslist The options field names to save.
	 * @return bool Success or failure
	 */
	public function saveOptions($target,$optionslist) {
		global $service;
		$request = $service->get('Request')->get();
		if (isset($request[$this->type.'_objid'])) {
			$objid = $request[$this->type.'_objid'];
			$languages = $service->get('Language')->getCodes();
			$defaultlang = $service->get('Language')->getDefault();
			$options = array();
			foreach($languages as $kl => $vl) {
				foreach ($optionslist as $k=>$v) {
					if (isset($request[$v.'_'.$vl]) || isset($request[$v])) {
						$val = (isset($request[$v.'_'.$vl]))?$request[$v.'_'.$vl]:$request[$v];
						$options[$kl][$v] = $val;
					}
				}
			}
			$objs = $this->getByObjId($objid);
			foreach ($objs as $k => $v) {
				$objs[$k]->setVar($target,$options[$v->getVar($this->type.'_language')]);
			}
			$ret = $this->save($objs);
		}
		$this->regenerateCache();
		if ($ret || $ret == array()) return true;
		return false;
	}

	/**
	 * Deletes object(s) from DB
	 *
	 * @public
	 *
	 * @param mixed $selection Object(s) ID or Criteria Object
	 * @return bool True on success
	 */
	public function delete($selection){
		global $service;
		$db =& $service->get('Db');
		if (is_numeric($selection)) $selection = array($selection);
		if (is_array($selection)) {
            if (!is_numeric($selection[0])) {
                if ($this->model->isVar($this->type.'_objid')) {
					foreach($selection as $k => $v) {
						$selection[$k] = (is_object($selection[$k]))?$selection[$k]->getVar($this->type.'_objid'):$selection[$k][$this->type.'_objid'];
					}
				}
				elseif ($this->model->isVar($this->type.'_id')) {
					foreach($selection as $k => $v) {
						$selection[$k] = (is_object($selection[$k]))?$selection[$k]->getVar($this->type.'_id'):$selection[$k][$this->type.'_id'];
					}
				}
            }
            if ($this->model->isVar($this->type.'_objid')) $selection = new Criteria($this->type.'_objid',$this->inClause($selection),'IN');
            elseif ($this->model->isVar($this->type.'_id')) $selection = new Criteria($this->type.'_id',$this->inClause($selection),'IN');
        }
		$whereclause = $selection->renderWhere();
		if (is_array($whereclause)) {
			$query = 'DELETE FROM `'.$db->prefix($this->type).'` '.$whereclause['query'];
			$whereclause['query'] = $query;
			$query = $whereclause;
		}
		else $query = 'DELETE FROM `'.$db->prefix($this->type).'` '.$whereclause;
		if (!$db->query($query)) {
			$result = false;
		}else $result = true;
		$this->regenerateCache();
		return $result;
	}

	/**
	 * Deletes all object(s) from DB
	 *
	 * @public
	 *
	 * @return bool True on success
	 */
	public function deleteAll(){
		$ret = $this->delete(new Criteria($this->getIdField(),'0','<>'));
		$this->regenerateCache();
		return $ret;
	}

	/**
	 * Create new object in DB or clones an existing one if provided
	 *
	 * @public
	 * @param Data $obj Optional data object to use to create new object from
	 * @return Object $obj
	 */
	public function create($obj=null){
		global $service;
		$db =& $service->get('Db');
		if ($obj == null) {
			$cls = ucfirst($this->type).'Data';
			$obj = new $cls();
			$obj->set($this->model->getDefaults());
			if ($this->model->isVar($this->type.'_language')) {
				$obj->setVar($this->type.'_language',$service->get('language')->getId());
			}
			if ($this->model->isVar($this->type.'_site')) {
				$obj->setVar($this->type.'_site',$service->get('Site')->getId());
			}
			if ($this->model->isVar($this->type.'_tenant')) {
				$obj->setVar($this->type.'_tenant',$_SESSION['tenant']);
			}
		}
		if (is_array($obj)) $obj = $obj[0];
        if ($this->model->getType($this->getIdField()) == DATATYPE_UUID) {
            $lastinsertid = $this->newUuid();
            $obj->setVar($this->getIdField(),$lastinsertid);
        }else $obj->setVar($this->getIdField(),0);
		if ($this->model->isVar($this->type.'_objid')) {
			$obj->setVar($this->getObjIdField(),$this->getNextId($this->getObjIdField()));
		}
		$success = $this->save(array($obj));
        if ($success) {
            if ($this->model->isVar($this->type.'_id') && $this->model->getId() == $this->type.'_id') {
                if ($this->model->getType($this->getIdField()) != DATATYPE_UUID) return $this->getById($db->getLastInsertId())[0];
                else return $obj;
            }
            else {
                return $obj;
            }
        }
        else return false;
	}

	/**
	 * Create new object in DB in all languages
	 *
	 * Use $store->setOption('ignorelangs',true); before calling this method
	 * to get objects back for all langs.
	 *
	 * @public
	 *
	 * @return bool True on success
	 */
	public function createMultilingual($oobj=null){
		global $service;
		$db =& $service->get('Db');
		$objid = $this->getNextId($this->getObjIdField());
		$objs = array();
		$langs = $service->get('Language')->getCodes();
		$cls = ucfirst($this->type).'Data';
		foreach ($langs as $k => $v) {
			$obj = false;
			if ($oobj != null) {
				foreach ($oobj as $ko => $vo) {
					if ($k == $vo->getVar($this->type.'_language')){
						$obj = $vo;
					}
				}
			}
			if ($oobj == null || $obj == false) {
				$obj = new $cls($this->type);
				$obj->set($this->model->getDefaults());
				$obj->setVar($this->type.'_language',$k);
				if ($this->model->isVar($this->type.'_site')) {
					$obj->setVar($this->type.'_site',$service->get('Site')->getId());
				}
				if ($this->model->isVar($this->type.'_tenant')) {
					$obj->setVar($this->type.'_tenant',$_SESSION['tenant']);
				}
			}
			$obj->setVar($this->getIdField(),0);
			$obj->setVar($this->getObjIdField(),$objid);
            $obj->setModified(true);
			$objs[]= $obj;
		}
		$this->save($objs);
		return $this->getByObjId($objid);
	}

	/**
	 * Creates new empty object and returns it
	 *
	 * @public
	 *
	 * @return bool True on success
	 */
	public function getNew(){
		global $service;
		$cls = ucfirst($this->type).'Data';
		$obj = new $cls();
		$obj->set($this->model->getDefaults());
        $obj = $this->setDefaultValues($obj);
        $obj->setModified(true);
		return $obj;
	}

    /**
     * Sets the default values all objects should get
     *
     * @public
     * @return Data $obj
     **/
	protected function setDefaultValues($obj){
        global $service;
        if ($this->model->isVar($this->type.'_deleted')) {
            $obj->setVar($this->type.'_deleted',0);
        }
        if ($this->model->isVar($this->type.'_deletedby')) {
            $obj->setVar($this->type.'_deletedby',0);
        }
        if ($this->model->isVar($this->type.'_created')) {
            $obj->setVar($this->type.'_created',time());
        }
        if ($this->model->isVar($this->type.'_createdby')) {
            $obj->setVar($this->type.'_createdby',$service->get('User')->getUid());
        }
        if ($this->model->isVar($this->type.'_modified')) {
            $obj->setVar($this->type.'_modified',time());
        }
        if ($this->model->isVar($this->type.'_modifiedby')) {
            $obj->setVar($this->type.'_modifiedby',$service->get('User')->getUid());
        }
        if ($this->model->isVar($this->type.'_uid')) {
            $obj->setVar($this->type.'_uid',$service->get('User')->getUid());
        }
        if ($this->model->isVar($this->type.'_language')) {
            if ($this->getOption('ignorelangs') == false) {
                $obj->setVar($this->type.'_language',$service->get('language')->getId());
            }else {
                $obj->setVar($this->type.'_language',0);
            }
        }
        if ($this->model->isVar($this->type.'_site')) {
            $obj->setVar($this->type.'_site',$service->get('Site')->getId());
        }
        if ($this->model->isVar($this->type.'_tenant')) {
            $obj->setVar($this->type.'_tenant',$_SESSION['tenant']);
        }
		if ($this->model->isVar($this->type.'_language') && $this->getOption('ignorelangs') == false) {
			$obj->setVar($this->type.'_language',$service->get('language')->getId());
		}
        return $obj;
    }

	/**
	 * Inserts or update an object in DB
	 *
	 * @param object $object
	 */
	function set($obj) {
		$this->save($obj);
	}

	/**
	 * Gets the default object (for the default language) in a
	 * collection of objects that have all the same ObjectID
	 *
	 * @public
	 *
	 * @param array@Data $obj The collection of objects
	 * @return Data Default object found.
	 */
	public function getDefaultObj($obj){
		global $service;
		if (count($obj) == 1) return $obj[0];
		$defaultlang = $service->get('Language')->getDefault();
		$defobj = false;
		foreach($obj as $k => $v) {
			if ($v->getVar($this->type.'_language') == $defaultlang['id'] || $v->getVar($this->type.'_language') == 0) {
				$defobj = $obj[$k];
			}
		}
		return $defobj;
	}


	/**
	 * Loads the childs objects as defined by current store model
	 *
	 * @access public
	 *
	 * @param array $data A result set to load childs objects associated with this model
	 * @return array Fetches childs data elements and return them
	 */
	public function getChilds($data) {
		$childsDef = $this->model->getChilds();
		$singleTypes = array(DATATYPE_INT, DATATYPE_FLOAT, DATATYPE_BOOL, DATATYPE_STRING, DATATYPE_EMAIL, DATATYPE_COLOR, DATATYPE_IMAGE, DATATYPE_IP, DATATYPE_DOMAINNAME);
		$manyTypes = array(DATATYPE_INTARRAY, DATATYPE_BOOLARRAY, DATATYPE_FLOATARRAY);
		foreach ($childsDef as $k => $v){
			$crit = new CriteriaCompo();
			$parentType = $this->model->getVars(true)[$v['parent']]['type'];
			$childModel = ucfirst($v['model']).'Model';
			$childStore = ucfirst($v['model']).'Store';
			if (class_exists($childModel) && class_exists($childStore)) {
				$childModel = new $childModel();
				$childStore = new $childStore();
				$relType = 0;

				//Apply max levels setting to child store.
				$childLevels = $this->getOption('maxchildslevels');
				if ($childLevels > 0) {
					$childStore->setOption($childLevels-1);
					if ($childLevels-1 == 0) $childStore->setOption('loadchilds',false);
				}

				$childType = $childModel->getVars(true)[$v['child']]['type'];
				$values = array();
				foreach ($data as $kd => $vd) $values = $vd->getVar($v['parent']);

				//One to one relationship
				if (in_array($childType,$singleTypes) !== false && in_array($parentType,$singleTypes) !== false) {
					foreach ($values as $kv => $vv) $values[$kv] = str_replace("'","\'",$vv);
					$crit = new Criteria($v['child'],'(\''.array_implode('\',\'',$values).'\')','IN');
					$relType = 1;
				}
				//Many to one relationship
				elseif (in_array($childType,$singleTypes) !== false && in_array($parentType,$manyTypes) !== false) {
					foreach ($values as $kv => $vv) {
						foreach ($vv as $kvv => $vvv) {
							$values[$kv][$kvv] = str_replace("'","\'",$vvv);
						}
						$values[$kv] = array_implode('\',\'',$vv);
					}
					$crit = new Criteria($v['child'],'(\''.array_implode('\',\'',$values).'\')','IN');
					$relType = 2;
				}
				//Many to many relationship
				elseif (in_array($childType,$manyTypes) !== false && in_array($parentType,$manyTypes) !== false) {
					foreach ($values as $kv => $vv) {
						foreach ($vv as $kvv => $vvv) {
							$crit->add(new Criteria($v['child'],'%,'.$vvv.',%','LIKE'),'OR');
						}
					}
					$relType = 3;
				}
				//One to many relationship
				elseif (in_array($childType,$manyTypes) !== false && in_array($parentType,$singleTypes) !== false) {
					foreach ($values as $kv => $vv) {
							$crit->add(new Criteria($v['child'],'%,'.$vv.',%','LIKE'),'OR');
					}
					$relType = 4;
				}

				//Append childs to current data
				$childs = $childStore->get($crit);
				foreach ($data as $kd => $vd) {
					foreach($childs as $kdc => $vdc) {
						switch($relType) {
							case 1: if ($vd->getVar($v['parent']) == $vdc->getVar($v['child'])) $data[$kdc]->addChild($v['model'],$vdc); break;
							case 2: if (in_array($vdc->getVar($v['child']),$vd->getVar($v['parent'])) !== false) $data[$kdc]->addChild($v['model'],$vdc); break;
							case 3: {
								$found = false;
								$val = $vdc->getVar($v['child']);
								foreach ($val as $kval => $vval) {
									if (in_array($vval,$vd->getVar($v['parent'])) !== false) {
										$found = true;
										break;
									}
								}
								if ($found) $data[$kdc]->addChild($v['model'],$vdc);
							} break;
							case 4: if (in_array($vd->getVar($v['parent']),$vdc->getVar($v['child'])) !== false) $data[$kdc]->addChild($v['model'],$vdc); break;
						}
                        $data[$kdc]->setModified(false);
					}
				}
			}
		}
		return $data;
	}

	/**
	 * Replace URLs in object by their real values
	 *
	 * @access protected
	 * @param array $data Data to loop over and replace urls
	 * @return array $data
	 */
	 protected function replaceUrls($data){
		 if ($this->getOption('urlreplace')) {
			 $s = array('http:{uploadurl}','http:{themeurl}','http:{url}','https:{uploadurl}','https:{themeurl}','https:{url}','{uploadurl}','{themeurl}','{url}');
			 $r = array(UPLOADURL,THEMEURL,URL,UPLOADURL,THEMEURL,URL,UPLOADURL,THEMEURL,URL);
			 foreach($data as $k=> $v) {
				 $data[$k] = str_replace($s,$r,$v);
			 }
		 }
		 return $data;
	 }

	/**
	 * Convert a result set to objects
	 *
	 * @private
	 *
	 * @param array $results
	 * @param bool $forceretrieve Forces a retrieve operation on data
	 * @return array toObjects()
	 */
	private function toObjects($results,$forceretrieve=false) {
		$objCls = $this->type.'Data';
		foreach ($results as $row => $data) {
            $obj = new $objCls($this->type);
            if ($this->model->getOption('usecache') == false || $forceretrieve == true) {
                //$data = array_merge($data,$this->model->retrieve($data));
                $data = $this->model->retrieve($data);
            }
			$data = $this->replaceUrls($data);
            $obj->set($data);
            $obj->setModified(false);
			$results[$row] = $obj;
		}
		return $results;
	}

	/**
	 * Convert a result set to array
	 *
	 * @private
	 *
	 * @param array $results
	 * @param bool $forceretrieve Forces a retrieve operation on data
	 * @return array toArray()
	 */
	private function toArray($results,$forceretrieve=false) {
        if (count($results > 0)) {
            $keys = $results[0]; // Use first row as model;
            foreach ($results as $key => $data) {
                if ($this->model->getOption('usecache') == false || $forceretrieve == true) {
                    //$d = array_merge($data,$this->model->retrieve($data));
                    $d = array_intersect_key($this->model->retrieve($data),$keys);
                }
                foreach ($keys as $k => $v) {
                    if (!is_numeric($k) && !isset($d[$k])) $d[$k] = $data[$k];
                }
				$data = $this->replaceUrls($data);
                $results[$key] = $data;
            }
        }
		return $results;
	}

	/**
	 * Convert a result set to a collection
	 *
	 * @private
	 *
	 * @param array $results
	 * @return Collection $collection
	 */
	private function toCollection($results) {
		$col = new Collection();
		$col->set($results);
		return $col;
	}


	/**
	 * Adds language selection criteria to an existing criteria.
	 *
	 * Searches for the current language ID or 0 for all languages
	 *
	 * @private
	 *
	 * @param Criteria $selection The base selection criteria
	 * @return CriteriaCompo A selection criteria containing language selection criteria
	 */
	private function addLanguageCriteria($selection){
		global $service;
		if ($this->model->isVar($this->type.'_language')) {
			$criteria = $selection;
			if (!is_a($selection,'CriteriaCompo')) $criteria = $this->toCriteriaCompo($selection);
			$id = $service->get('Language')->getId();
			if ($id > 0) $criteria->add(new Criteria($this->type.'_language',$this->inClause([$service->get('Language')->getId(),'0']),'IN'),'AND');
			return $criteria;
		}
		else {
			return $selection;
		}
		return $selection;
	}

	/**
	 * Adds tenant selection criteria to an existing criteria.
	 *
	 * Searches for the current tenant ID or 0 for all tenants
	 *
	 * @private
	 *
	 * @param Criteria $selection The base selection criteria
	 * @return CriteriaCompo A selection criteria containing tenant selection criteria
	 */
	private function addTenantCriteria($selection){
		global $service;
		if ($this->model->isVar($this->type.'_tenant')) {
			$criteria = $selection;
			if (!is_a($selection,'CriteriaCompo')) $criteria = $this->toCriteriaCompo($selection);
			$id = (isset($_SESSION['tenant']))?$_SESSION['tenant']:0;

            //Support single tenant records and multiple tenants records (users are multi-tenant).
            $tenantVarType = $this->model->getType($this->type.'_tenant');
            if ($tenantVarType == DATATYPE_UUIDARRAY || $tenantVarType == DATATYPE_INTARRAY) {
                $tenantCrit = new CriteriaCompo();
                $tenantCrit->add(new Criteria($this->type.'_tenant','%,0,%','LIKE'),'OR');
                $tenantCrit->add(new Criteria($this->type.'_tenant','%,'.$id.',%','LIKE'),'OR');
                $criteria->add($tenantCrit,'AND');
            }
			else $criteria->add(new Criteria($this->type.'_tenant',$this->inClause([$id,0]),'IN'),'AND');

			return $criteria;
		}
		else {
			return $selection;
		}
		return $selection;
	}

	/**
	 * Adds site selection criteria to an existing criteria.
	 *
	 * Searches for the current site ID or 0 for all sites
	 *
	 * @private
	 *
	 * @param Criteria $selection The base selection criteria
	 * @return CriteriaCompo A selection criteria containing website selection criteria
	 */
	private function addSiteCriteria($selection){
		global $service;
		if ($this->model->isVar($this->type.'_site')) {
			$criteria = $selection;
			if (!is_a($selection,'CriteriaCompo')) $criteria = $this->toCriteriaCompo($selection);
			if ($this->model->isVar($this->type.'_site')) {
				$criteria->add(new Criteria($this->type.'_site',$this->inClause([$service->get('Site')->getId(),0]),'IN'),'AND');
				return $criteria;
			}
			else {
				return $selection;
			}
		}
		return $selection;
	}

	/**
	 * Adds groups selection criteria to an existing criteria.
	 *
	 * Adds a criteria that checks if user has enough access rights to view content
	 *
	 * @private
	 *
	 * @param Criteria $selection The base selection criteria
	 * @return CriteriaCompo A selection criteria containing user groups selection criteria
	 */
	public function addGroupCriteria($selection,$groups=null,$mode='AND'){
		global $service;
		if ($this->model->isVar($this->type.'_groups')) {
			$criteria = $selection;
			if (!is_a($selection,'CriteriaCompo')) $criteria = $this->toCriteriaCompo($selection);

			//Check user groups and display content available to his user groups
			if ($groups == null) $groups = $service->get('User')->getGroups();
			$groupsCrit = new CriteriaCompo();
			foreach($groups as $k=>$v){
				$groupsCrit->add(new Criteria($this->type.'_groups','%,'.$v.',%','LIKE'),'OR');
			}
			$criteria->add($groupsCrit,$mode);
			return $criteria;
		}
		return $selection;
	}

	/**
	 * Adds deleted selection criteria to an existing criteria.
	 *
	 * Adds a criteria that prevents deleted elements from showing up
	 *
	 * @private
	 *
	 * @param Criteria $selection The base selection criteria
	 * @return CriteriaCompo A selection criteria containing deleted=0 selection criteria
	 */
	public function addDeletedCriteria($selection){
		global $service;
		if ($this->model->isVar($this->type.'_deleted')) {
			if (!is_a($selection,'CriteriaCompo')) $selection = $this->toCriteriaCompo($selection);
			$selection->add($this->type.'_deleted',0);
			return $selection;
		}
		return $selection;
	}

	private function regenerateCache($objects=null){
		global $service;
		//Flush cache on write to cached data table.
		if ($this->model->getOption('usecache')) {
			$this->setOption('regeneratingcache',true);
			$cache = new DBCache($this->model);

            //Allow all languages temporarily while regenerating cache
			$resetlang = false;
			if ($this->getOption('ignorelangs') == false) {
				$resetlang = true;
				$this->setOption('ignorelangs',true);
			}

			//Regenerate cache on table
			if (isset($objects) && $objects != null) $cache->write($objects);
			else $cache->write($this->get());
			if ($resetlang) $this->setOption('ignorelangs',false);
			$this->setOption('regeneratingcache',false);
		}
	}

	private function toCriteriaCompo($selection){
		$criteria = new CriteriaCompo($selection);
		$sortcols = $selection->getSort(true);
		$sortorders = $selection->getOrder(true);
		foreach ($sortcols as $k => $v ){
			$criteria->setSort($sortcols[$k],$sortorders[$k]);
		}
		$criteria->setOrder($selection->getOrder());
		$criteria->setStart($selection->getStart());
		$criteria->setLimit($selection->getLimit());
		$criteria->setGroupBy($selection->getGroupBy());
		$criteria->setGroupBy($selection->getGroupBy());
		$criteria->setSelection($selection->getSelection());
		return $criteria;
	}

    public function newUuid() {
        global $service;
        $service->get('Ressource')->get('core/security/hash/uuid');
        return UUID::shorten(UUID::generate());
    }

    /**
     * Returns the last insert ID
     *
     * @access public
     * @return int $id
     **/
    public function getLastId() {
        global $service;
        return ($this->model->getType($this->getIdField()) == DATATYPE_UUID)?$this->lastinsertid:$service->get('Db')->getLastInsertId();
    }

    /**
     * Turns a result array column into a IN clause criteria
     *
     * @access public
     * @param array $array
     * @param string $func Function name, currently only IN is supported
     * @return string The column values as a string for SQL IN clause
     **/
    public function inClause($array) {
        return '(\''.implode('\',\'',$array).'\')';
    }
}
?>
