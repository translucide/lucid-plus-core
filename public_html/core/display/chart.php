<?php
/**
 * Chart class
 *
 * Handles chart generation
 * Jan 15, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2015 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('Ressource')->get('lib/chart.js/1.0.1/Chart.min');

class Chart extends BaseClass{
    private $type;
    private $labels;
    private $datasets;
    private $options;
    private $footer;

    public function __construct($type = '',$labels='',$opt=array()){
        $this->Chart($type,$labels,$opt);
    }
    public function Chart($type = '',$labels='',$opt=array()) {
        $this->type = '';
        $this->labels = array();
        $this->datasets = array();
        $this->footer = '';
        $this->options =  array('responsive' => true,'maintainAspectRatio' => false);
        if ($type != '') $this->setType($type);
        if ($labels != '') $this->setLabels($labels);
        if ($opt != array()) $this->setOptions($opt);
    }

    public function setType($type){
        $this->type = $type;
    }

    public function setLabels($labels) {
        $this->labels = $labels;
    }

    public function addDataset($data, $label = 'My Dataset', $color = '220,220,220') {
        $this->datasets[] = array(
            'label' => $label,
            'fillColor' => "rgba($color,0.2)",
            'strokeColor' => "rgba($color,1)",
            'pointColor' => "rgba($color,1)",
            'pointStrokeColor' => "#fff",
            'pointHighlightFill' => "#fff",
            'pointHighlightStroke' => "rgba($color,1)",
            'data' => $data
        );
    }

    public function setOptions($opt){
        $this->options = array_merge($this->options,$opt);
    }

    public function setFooter($footer) {
        $this->footer = $footer;
    }

    function render($ajax=false){
        global $service;
        $id = uniqid('chart-');
        $data = '{
            labels: ["'.implode('","',$this->labels).'"],
            datasets:
                '.json_encode($this->datasets).'
        }';

        $code = '<canvas id="'.$id.'" height="400"></canvas>';
        $code .= '<div class="chartlegend">';
        foreach ($this->datasets as $v) {
            $code .= '<div class="col-sm-4"><span style="background-color:'.$v['strokeColor'].';width:12px;height:12px;display:inline-block;"></span>&nbsp;'.$v['label'].'</div>';
        }
        $code .= '</div>';
        $code .= $this->footer;

        if ($ajax) {
            $code .= '
                var ctx = $("#'.$id.'").get(0).getContext("2d");
                var myNewChart = new Chart(ctx);
                myNewChart.'.ucfirst($this->type).'('.$data.', '.json_encode($this->options).');
            ';
        }else {
            $script = '$(document).ready(function(){
                var ctx = $("#'.$id.'").get(0).getContext("2d");
                var myNewChart = new Chart(ctx);
                myNewChart.'.ucfirst($this->type).'('.$data.', '.json_encode($this->options).');
            });';
            $service->get('Ressource')->addScript($script);
        }
        return $code;
    }
}
?>
