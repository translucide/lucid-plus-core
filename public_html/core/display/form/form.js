/**
 * lucidForm jQuery Plugin
 *
 * Creates a form using the given parameters. As of now it supports the
 * following features :
 *
 * - Generates any form field type needed, even complex ones as ajax image upload.
 * - Multiple layout styles
 * - Form sections
 * - Form tabs
 * - Intro text
 * - Footer text
 * - Render part of a form to insert in a container (sub-form loading)
 *
 * Usage example :
 *
 * $('.myformdiv').lucidForm({
 * 		name: 'formname',
 * 		action : 'index.php',
 * 		title : 'My form title',
 * 		method : 'POST',
 * 		class : 'myform',
 * 		intro : 'Please fill in this form in order to contact us.',
 * 		elements : [
 * 			{type: 'text', value: 'info@text.com', name: 'inputname', placeholder : 'Email'},
 * 			{type: 'radio', value: '1', name: 'inputname', options : [{title:'Radio button text',value:1},{title:'Radio button text',value:1}]}
 * 		]
 * });
 *
 * Oct 30, 2012
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

/**
 * Do not remove this function as it is used to validate form before ajaxForm plugin submits it.
 **/
if (!$.fn.bindFirst)
$.fn.bindFirst = function(name, fn) {
	this.bind(name, fn);
	var handlers = this.data('events')[name.split('.')[0]];
	var handler = handlers.pop();
	handlers.splice(0, 0, handler);
};

/**
 * LucidForm
 *
 * v0.1
 */
(function( $ ) {

	var methods = {
		/**
		 * Initialize the form.
		 *
		 * Renders the form HTML code to the targeted element.
		 */
		init : function( options ) {
			/**
			 * Plugin default settings
			 */
			var $this = $(this);
			var opt = $.extend({
			name         	: 'form',
			action 			: 'index.php',
			title 			: '',
			method 			: 'post',
			'class'			: '',
			layout			: 'horizontal',
			intro			: '',
			footer			: '',
			sections		: [],
			elements 		: [],
			tabs			: [],
			groups			: [],
			lang			: 'en',
			scripts			: '',
			validated		: false,
			formtag			: true,
            gridsize        : 12,
            colsize         : 'sm',
            ajaxform        : false
			}, options);
			$this.data('options', opt);
			if(opt.layout != 'horizontal' && opt.layout != 'vertical') {
				opt.layout = 'vertical';
				$this.data('options',opt);
			}

			var innerHTML = '';
			if (opt.formtag && opt.formtag == '1') {
				innerHTML += '<form name="'+opt.name+'" id="'+opt.name+'" action="'+opt.action+'" method="'+opt.method+'"';
				if (opt['class'] || opt.layout) {
					innerHTML += ' class="'+opt['class']+' form-'+opt.layout+'"';
				}
				innerHTML += '>';
			}
			innerHTML += methods.render.apply(this);
			if (opt.formtag && opt.formtag == '1') innerHTML += '</form>';

            /* @NOTE: The scripts were included before the previous line. Loading script after HTML markup might break something! */
			$(this).html(innerHTML);
			$('body').append(opt.scripts);

			if (opt.formtag && opt.formtag == '1') {
				$this.find('form#'+opt.name).on("submit",function(){
					var valid = $this.lucidForm("validate");
					if (valid) $this.append("<div class=\"sendformmsg\"><span class=\"loading\"></span><div class=\"message\"></div></div>");
					return valid;
				});
			}else {
				/*
				 * If this is a sub-form, try to bind submit event to parent form...
				 * There is no guarantee there is one however...
				 */
				$this.parents('form#'+opt.name).on("submit",function(){
					var valid = $this.lucidForm("validate");
					if (valid) $this.append("<div class=\"sendformmsg\"><span class=\"loading\"></span><div class=\"message\"></div></div>");
					return valid;
				});
			}
            if (opt.ajaxform && (opt.ajaxform === true || opt.ajaxform == 1 || opt.ajaxform === 'true')) {
                $('form#'+opt.name).ajaxForm({
                    success: function(responseText, statusText, xhr, $form){
                        console.info(responseText.success);
                        console.info('---');
                        if (responseText.success) {
                            $("#"+opt.name+"_div").trigger("submitSuccess");
                        }else {
                            $("#"+opt.name+"_div").parent().children(".alert-error").remove();
                            $("#"+opt.name+"_div").parent().append("<div class=\"alert alert-error\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a> "+responseText.message+"</div>");
                            $("#"+opt.name+"_div").trigger("submitError");
                        }
                    }
                });
            }
			$(function () {
				$('[rel=tooltip]').tooltip();
			});
			return this;
		},

		/**
		 * Runs validation control on the form
		 */
		validate : function() {
			var $this = $(this);
            var isValid = true;
            try{
                for (instance in CKEDITOR.instances) {
                    $("#"+instance).val(CKEDITOR.instances[instance].getData());
                    for(i=0;i<data.length;i++) if(data[i].name == instance) data[i].value = CKEDITOR.instances[instance].getData();
                }
            }catch(e){}
            console.info("lucidForm: Validating form...");
            $this.find("button[type=submit],input[type=submit]").attr("disabled","1");
			var settings = $this.data('options');
			if (settings.elements.length > 0) {
				elements = settings.elements;
				for(var ei=0; ei<elements.length; ei++){
					el = elements[ei];
					if (el && el.validate && el.type != 'hidden' && el.type != 'button' && el.type != 'submit') {
						for (var vi = 0; vi < el.validate.length; vi++) {
							var formel = $this.find('[name='+el.name+']');
							var val = formel.val();
							var valid = true;
                            if (val === '' && el.validate[vi].allowempty) {
                                valid = true;
                            }else {
                                switch(el.validate[vi].method) {
                                    case 'notempty' :{
                                        if (val == '') valid = false;
                                    }break;
                                    case 'number' : {
                                        var pattern =/^-?\d*[\.]?\d+$/;
                                        if (!pattern.test(val)) valid = false;
                                    }break;
                                    case 'phonenumber' : {
                                        var str = formel.val();
                                        str = str.replace('(','').replace(')','').replace(' ','-');
                                        formel.val(str);
                                        var pattern =/^\+?[0-9\-]+\*?$/;
                                        if (!pattern.test(str)) valid = false;
                                    }break;
									case 'range' :
                                    case 'between' : {
                                        if (el.validate[vi].min && parseInt(val) < el.validate[vi].min) valid = false;
                                        if (el.validate[vi].max && parseInt(val) > el.validate[vi].max) valid = false;
                                    }break;
                                    case 'length' : {
                                        if (el.validate[vi].min && val.length <= el.validate[vi].min) valid = false;
                                        if (el.validate[vi].max && val.length >= el.validate[vi].max) valid = false;
                                    }break;
                                    case 'email' : {
                                        var pattern =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                        if (!pattern.test(val)) valid = false;
                                    }break;
                                    case 'pattern' : {
                                        if (el.validate[vi].pattern) {
                                            var pattern = el.validate[vi].pattern;
                                            if (!pattern.test(val)) valid = false;
                                        }
                                    }break;
                                    case 'custom': {
                                        if (el.validate[vi].handler) {
                                            eval("var customHandler =" + el.validate[vi].handler);
                                            if (customHandler(val) !== true) valid = false;
                                        }else {
                                            console.info("lucidForm: No handler found")
                                            valid = false;
                                        }
                                    }break;
                                }
                            }
							if (el.validate[vi].invert && (el.validate[vi].invert === true || el.validate[vi].invert === 1)) {
								valid = (valid)?false:true;
							}
							if (valid) $this.lucidForm("validationSuccess",formel);
							else {
								$this.lucidForm("validationError",formel,((el.validate[vi].message)?el.validate[vi].message:''));
								isValid = false;
							}
						}

					}
				}
			}
			if (isValid === false) {
                console.info("lucidForm: Form validation failed.");
                $this.find("button[type=submit],input[type=submit]").removeAttr("disabled","1");
            }
            else console.info("lucidForm: Form validation passed.");

			return isValid;
		},

		validationError: function(el,msg) {
			var $this = $(this);
			if (msg == '') msg = 'You have an error here.';
			el.removeClass('validation-success').addClass('validation-error');
			el.parent().parent().find('.validation-error-message').remove();
			el.parent().parent().append('<div class="validation-error-message">'+msg+'</div>');
		},

		validationSuccess: function(el) {
			var $this = $(this);
			el.removeClass('validation-error').addClass('validation-success');
			el.parent().parent().find('.validation-error-message').remove();
		},

		/**
		 * Called on submitting the form.
		 */
		submit : function(settings) {
			return this;
		},

		/**
		 * Renders the form
		 */
		render: function(){
			var $this = $(this);
			var settings = $this.data('options');
			innerHTML = '';
			innerHTML += '<fieldset>';
			if (settings.title) innerHTML += '<legend>';
			if (settings.icon) {
				if (settings.icon.indexOf('http://') !== -1 || settings.icon.indexOf('.jpg') !== -1 || settings.icon.indexOf('.png') !== -1) {
					innerHTML += '<img src="'+settings.icon+'">';
				}else innerHTML += '<span class="glyphicon glyphicon-'+settings.icon+'"></span>';
			}
			if (settings.title) innerHTML += settings.title+'</legend>';
			if (settings.error && settings.error.msg !== '') innerHTML += '<p class="msg '+settings.error.type+'">'+settings.error.msg+'</p>';
			if (settings.intro && settings.intro !== '' && settings.intro != 'null') innerHTML += '<p>'+settings.intro+'</p>';
			innerHTML += methods.renderTabs.apply(this);
			if (settings.footer && settings.footer !== '' && settings.footer != 'null') innerHTML += '<p>'+settings.footer+'</p>';
			innerHTML += '</fieldset>';
			return innerHTML;
		},

		/**
		 * Renders all tabs
		 */
		renderTabs: function(){
			var $this = $(this);
			var settings = $this.data('options');
			innerHTML = '';
			if (settings.tabs.length > 0) {
				innerHTML += '<div class="tabbable"><ul class="nav nav-tabs">';
				var activetab = 0;
				for(var i=0; i<settings.tabs.length;i++){
					if (settings.tabs[i].active && settings.tabs[i].active == 1) {
						activetab = i;
					}
				}
				for(var i=0; i<settings.tabs.length;i++){
					var tab = settings.tabs[i];
					innerHTML += '<li';
					if (i == activetab) innerHTML += ' class="active"';
					innerHTML += '><a data-toggle="tab" href="#';
					if (tab.id) innerHTML += tab.id + '_' + settings.tabssuffix;
					else innerHTML += 'tab'+i + '_' + settings.tabssuffix;;
					innerHTML += '">';
					if (tab.title) innerHTML += tab.title;
					innerHTML += '</a></li>';
				}
				innerHTML += '</ul>';
				innerHTML += '<div class="tab-content">';
				for(var i=0; i<settings.tabs.length;i++){
					var tab = settings.tabs[i];
					innerHTML += '<div id="';
					if (tab.id) innerHTML += tab.id + '_' + settings.tabssuffix;
					else innerHTML += 'tab'+i + '_' + settings.tabssuffix;
					innerHTML += '" class="tab-pane';
					if (i == activetab) innerHTML += ' active';
					innerHTML += '">';
					innerHTML += '<fieldset>';
					/*if (tab.title) innerHTML += '<legend>'+tab.title+'</legend>';*/
					if (tab.intro && tab.footer != 'null') innerHTML += '<p>'+tab.intro+'</p>';
					innerHTML += methods.renderTab.apply(this,[tab]);
					if (tab.footer && tab.footer != 'null') innerHTML += '<p>'+tab.footer+'</p>';
					innerHTML += '</fieldset>';
					innerHTML += '</div>';
				}
				innerHTML += '</div></div>';
				innerHTML += methods.renderTab.apply(this,['']);
			}else innerHTML += methods.renderTab.apply(this,['']);
			return innerHTML;
		},

		/**
		 * Renders all sections
		 */
		renderSections: function(tab){
			var $this = $(this);
			var settings = $this.data('options');
			innerHTML = '';
			if (settings.sections.length > 0) {
				for(var i=0; i<settings.sections.length;i++){
					innerHTML += '<fieldset>';
					if (settings.sections[i].title) innerHTML += '<legend>'+settings.sections[i].title+'</legend>';
					if (settings.sections[i].intro) innerHTML += '<p>'+settings.sections[i].intro+'</p>';
					innerHTML += methods.renderSection.apply(this,[tab,settings.sections[i]]);
					if (settings.sections[i].footer) innerHTML += '<p>'+settings.sections[i].footer+'</p>';
					innerHTML += '</fieldset>';
				}
			}else innerHTML += methods.renderSection.apply(this,[tab,'']);
			return innerHTML;
		},

		/**
		 * Renders one tab
		 */
		renderTab: function(tab){
			var $this = $(this);
			var settings = $this.data('options');
			innerHTML = '';
			innerHTML += methods.renderSections.apply(this,[tab]);
			return innerHTML;
		},

		/**
		 * Renders one section
		 */
		renderSection: function(tab,section){
			var $this = $(this);
			var settings = $this.data('options');
			var currentwidth = 0;
			innerHTML = '';
			if ($.isArray(section)) var sectionId = section.id;
			else sectionId = section;
			if (tab && tab.id) var tabId = tab.id;
			else tabId = tab;
			if (settings.elements.length > 0) {
				elements = settings.elements;
				for(var ei=0; ei<elements.length;ei++){
					el = elements[ei];
					if (el)
					if (((el.section && el.section == sectionId) || sectionId == '') && (el.tab && el.tab == tabId) || (tab == '' && !el.tab)) {
						if (settings.layout == 'vertical' && el.type != 'hidden') {
							if (currentwidth == 0) {
								innerHTML += '<div class="row grid-'+settings.gridsize+'">';
							}
						}
						innerHTML += methods.renderElement.apply(this,[el]);
						if (settings.layout == 'vertical' && el.type != 'hidden') {
							currentwidth += parseInt(el.width);
							if (currentwidth >= settings.gridsize) {
								innerHTML += '</div>';
								currentwidth = 0;
							}
						}
					}
				}
				if (currentwidth > 0 && settings.layout == 'vertical') innerHTML += '</div>';
			}
			return innerHTML;
		},
		renderElement: function(el){
			var $this = $(this);
			var settings = $this.data('options');

			innerHTML = '';
			if (el.type != 'hidden') {
				if (settings.layout == 'horizontal') {
					innerHTML += '<div class="form-group row">';
				}
				if (settings.layout == 'vertical') {
					if (el.width > 0) innerHTML += '<div class="col-'+settings.colsize+'-'+el.width+'"><div class="form-group">';
					else innerHTML += '<div class="col-'+settings.colsize+'-'+settings.gridsize+'"><div class="form-group">';
				}
				innerHTML += methods.renderElementLabel.apply(this,[el]);
				innerHTML += methods.renderElementInputHeader.apply(this,[el]);
				innerHTML += methods.renderElementInput.apply(this,[el,false]);
				innerHTML += methods.renderTranslations.apply(this,[el]);
				innerHTML += methods.renderElementInputFooter.apply(this,[el]);
				innerHTML += '</div>';
				if (settings.layout == 'vertical') {
					innerHTML += '</div>';
				}
			}else {
				innerHTML += methods.renderElementInput.apply(this,[el]);
				innerHTML += methods.renderTranslations.apply(this,[el]);
			}
			return innerHTML;
		},

		renderElementLabel : function(el) {
			var $this = $(this);
			var settings = $this.data('options');
			innerHTML = '';
			name = '';
			if (el.type == 'hidden') return '';
			if (el.name) name = el.name;

			var typesWithNoTitle = ['button','buttonbar','btn','reset','buttons','submit','tab','hidden'];
			var length = typesWithNoTitle.length;
			var noTitle = false;
			for(var j = 0; j < typesWithNoTitle.length; j++) {
				if(typesWithNoTitle[j] == el.type) noTitle = true;
			}
			if (el.title && noTitle == false) {
				innerHTML += '<label for="'+name+'"';
				if (settings.layout == 'horizontal') {
					innerHTML += ' class="control-label col-'+settings.colsize+'-';
					if (el.width) innerHTML += (settings.gridsize-el.width)+'"';
					else innerHTML += '2"';
				}
				innerHTML += '>';
				if (el.icontip) {
                    innerHTML += '<a href="#" rel="tooltip" title="'+el.icontip+'" alt="'+el.icontip+'" data-original-title="'+el.icontip+'">';
                    if (el.icon) innerHTML += '<span class="glyphicon glyphicon-'+el.icon+'"></span> ';
                    else innerHTML += '<span class="glyphicon glyphicon-question-sign"></span> ';
                    innerHTML += '</a>';
                }
				innerHTML += el.title;
				if (typeof el.validate == 'object' && el.validate.length > 0)  innerHTML += '*';
				innerHTML +='</label>';
			}
			return innerHTML;
		},
		renderElementInputHeader: function (el) {
			var $this = $(this);
			var settings = $this.data('options');
			innerHTML = '';
			if (settings.layout == 'horizontal') {
				if (el.width != undefined) innerHTML += '<div class="col-'+settings.colsize+'-'+el.width+'">';
				else innerHTML += '<div class="col-'+settings.colsize+'-10">';
			}
			return innerHTML;
		},
		renderElementInputFooter: function (el) {
			var $this = $(this);
			var settings = $this.data('options');
			innerHTML = '';
			if (settings.layout == 'horizontal') innerHTML += '</div>';
			return innerHTML;
		},
		renderElementInput : function (el,isTranslation) {
			var $this = $(this);
			var settings = $this.data('options');
			innerHTML = '';
			outerHTML = '';
			if (el.value == 'null') el.value = '';
			if (el.type == 'hidden') {
				innerHTML += '<input type="'+el.type+'" name="'+el.name+'" value="'+el.value+'"';
                if (el.id) innerHTML += 'id="'+el.id+'"';
                else innerHTML += 'id="'+el.name+'"';
                innerHTML += '>';
				return innerHTML;
			}else{
				innerHTML += '<div';
				if (el.translations || el.help || el.type == 'size' || el.type=='imagesize' /*|| el.type == 'datepicker'*/ || el.inputgroup) {
                    innerHTML += ' class="input-group"';
                }
                if (el.type == 'datepicker') {
                    innerHTML += ' class="date datepicker"';
                }
				innerHTML += '>';
			}
            if (el.prepend) innerHTML += el.prepend;
			if (el.translations) {
				innerHTML += '<a class="input-group-addon" onclick="$(\'#translations_'+el.name+'\').slideToggle(200);">'+el.lang+'</a>';
			}

			id = '';
			if (el.id) id = ' id="'+el.id+'"';
			else id = 'id="'+el.name+'"';

			name = '';
			if (el.name) name = ' name="'+el.name+'"';

			if (el['class'] != undefined) {
				el['class'] = 'form-control '+el['class'];
			}else el['class'] = 'form-control';

			cls = '';
			if (el['class']) cls = ' class="'+el['class']+'"';

			type = '';
			if (el.type) type = ' type="'+el.type+'"';

			value = '';
			if (el.value) {
				str = new String(el.value);
				value = ' value="'+str.replace(/"/g,'&quot;')+'"';
			}

			disabled = '';
			if (el.disabled && el.disabled == true) disabled = ' readonly="readonly"';

			help = '';
			helptype = '';
			if (el.helptype && el.helptype == 'inline') helptype = 'inline';
			else helptype = 'block';
			if (el.help) help = '<a class="input-group-addon glyphicon glyphicon-question-sign" rel="tooltip" data-original-title="'+el.help+'"></a>';

			var onclick = '';
			if (el.onclick) onclick = ' onclick="'+el.onclick+'"';
			var onchange = '';
			if (el.onchange) onchange = ' onchange="'+el.onchange+'"';
			var onfocus = '';
			if (el.onfocus) onfocus = ' onfocus="'+el.onfocus+'"';

			switch (el.type) {
				case 'datepicker' : {
					if (el.value == '1970-01-01') el.value = '';
					if (!el.value || typeof el.value == null || el.value == 0 || el.value == ' ' || el.value == '0' || typeof el.value == undefined) el.value = '';
					innerHTML += '<input type="text"';
					if (el['class']) innerHTML += ' class="'+el['class']+" init\" form-control from-control-inline\"";
					innerHTML += '" '+id+' data-date="';
					if (el.value && el.value != '') innerHTML += el.value;
					innerHTML += '" data-date-format="';
					if (el.format) innerHTML += el.format;
					else innerHTML += 'yyyy-mm-dd';
					innerHTML += '" size="16" '+value+name+onfocus+onchange+onclick;
					if (el.placeholder) innerHTML += ' placeholder="'+el.placeholder+'"';
					innerHTML += disabled+'>';
					//innerHTML += '<a class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></a>';
					innerHTML += help;
					innerHTML += '<script language="javascript" type="text/javascript">';
					innerHTML += "\n"+'$("#';
                    if (el.id && el.id != '') innerHTML += el.id;
					else innerHTML += el.name;
					innerHTML += '.init").datepicker({autoclose: true}).on(\'click focus\',function(e){if ($(e.target).prop("readonly")) {$(this).datepicker(\'hide\');e.preventDefault;e.stopPropagation();return false;}}).on(\'show\',function(ev){$(ev.target).trigger(\'keydown\');}).on(\'changeDate\', function (ev) {$(ev.target).trigger(\'change\').trigger(\'blur\');}).removeClass("init");';
					innerHTML += "\n</script>";
				}break;
				case 'text' : {
					innerHTML += '<input'+cls+id+name+type+value+disabled+onfocus+onchange+onclick;
                    if (el.autocomplete) innerHTML += ' autocomplete="'+el.autocomplete+'"';
                    if (el.autocapitalize) innerHTML += ' autocapitalize="'+el.autocapitalize+'"';
                    if (el.autocorrect) innerHTML += ' autocorrect="'+el.autocorrect+'"';
                    if (el.spellcheck) innerHTML += ' spellcheck="'+el.spellcheck+'"';
					if (el.placeholder) innerHTML += ' placeholder="'+el.placeholder+'"';
					if (el.size) innerHTML += ' size="'+el.size+'"';
					if (el.maxlength) innerHTML += ' maxlength="'+el.maxlength+'"';
					innerHTML += '>'+help;
				}break;
				case 'color' : {
					if (el.id =='' || el.id == undefined) el.id = el.name+'_input';
					innerHTML += '<input'+cls+id+name+type+value+disabled+onfocus+onchange+onclick;
					if (el.placeholder) innerHTML += ' placeholder="'+el.placeholder+'"';
					innerHTML += '>'+help;
					innerHTML += '<script language="javascript" type="text/javascript">';
					innerHTML += 'setTimeout(function(){$("#'+el.id+'").spectrum({color: "#ECC", showInput: true, className: "full-spectrum", showInitial: true, showPalette: true, showSelectionPalette: true, maxPaletteSize:10, preferredFormat: "hex", localStorageKey: "spectrum.demo", move: function (color) { }, show: function () { }, beforeShow: function () {  }, hide: function () { }, change: function() { }, palette: [["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)","rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)","rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)","rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)","rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)","rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]]});},100);'
					innerHTML += '</script>';
				}break;
				case 'subform' : {
					outerHTML += el.form;
					innerHTML += "<div"+cls+name.replace('name=','id=')+name+onfocus+onchange+onclick+">";
					innerHTML += '<script language="javascript" type="text/javascript">'+"\n";
					innerHTML += "function subform_init_"+el.name+"(){";
					innerHTML += "    $('#"+el.name+" div.subform_master').hide();";
					innerHTML += "};";
					innerHTML += "function subform_edit_"+el.name+"(item){";
					innerHTML += "    if (item === 0) {$('#"+el.name+" div.subform_edit').html($('#"+el.name+" div.subform_master').html()).show();}";
					innerHTML += "    else {";
					innerHTML += "        $('#"+el.name+" div.subform_edit').html($('#"+el.name+" div.subform_master').html());";
					innerHTML += "        $(item).parent('li').find('span.values input').each(function(i,e){name = $(this).attr('name');name = name.substring(0,name.length-2);console.info(name);$('#"+el.name+" div.subform_edit input[name=\"subformfield_'+name+'\"]').val($(this).val());});";
					innerHTML += "        $('#"+el.name+" div.subform_edit').show();";
					innerHTML += "        $(item).parent('li').remove();";
					innerHTML += "    }";
					innerHTML += "};";
					innerHTML += "function subform_move_"+el.name+"(item,pos){";
					innerHTML += "    if (pos == -1) $(item).closest('li').insertBefore($(item).closest('li').prev());";
					innerHTML += "    if (pos == 1) $(item).closest('li').insertAfter($(item).closest('li').next());";
					innerHTML += "};";
					innerHTML += "function subform_delete_"+el.name+"(item){";
					innerHTML += "    $(item).closest('li').remove();";
					innerHTML += "};";
					innerHTML += "function subform_save_"+el.name+"(){";
					innerHTML += "    $('#"+el.name+" div.subform_edit').hide();";
					innerHTML += "    try{";
					innerHTML += "        if (CKEDITOR != undefined && CKEDITOR.instances != undefined) {for (instance in CKEDITOR.instances) {";
					innerHTML += "            $('#'+instance).val(CKEDITOR.instances[instance].getData());";
					innerHTML += "            ckdata = $('#"+el.name+" div.subform_edit > textarea');";
					innerHTML += "            for(i=0;i<ckdata.length;i++) if(ckdata[i].attr('name') == instance) ckdata[i].val(CKEDITOR.instances[instance].getData());";
					innerHTML += "        }}}catch(err){}";
					innerHTML += "    }";
					innerHTML += "    fields = $('#"+el.name+" div.subform_edit input, #"+el.name+" div.subform_edit textarea, #"+el.name+" div.subform_edit select').serializeArray();"
					innerHTML += "    str = '';";
					innerHTML += "    label = '';";
					innerHTML += "    for(j=0;j<fields.length;j++) {"
					innerHTML += "        val = fields[j];"
					innerHTML += "        if (val.name == 'subformfield_"+el.labelfield+"') label = val.value;";
					innerHTML += "        str += '<input type=\"hidden\" name=\"'+val.name.replace(/subformfield_/g,'')+'[]\" value=\"'+val.value+'\">';";
					innerHTML += "    }";
					innerHTML += "    $('#"+el.name+" ul.subform_list').append('<li>'+label+'<a class=\"btn btn-mini delete pull-right\" onclick=\"subform_delete_"+el.name+"(this);\"><span class=\"glyphicon glyphicon-remove\"></span> "+el.deletelabel+"</a><a class=\"btn btn-mini edit pull-right\" onclick=\"subform_edit_"+el.name+"(this);\"><span class=\"glyphicon glyphicon-edit\"></span> "+el.editlabel+"</a><a class=\"btn btn-mini movedown pull-right\" onclick=\"subform_move_"+el.name+"(this,1);\"><span class=\"glyphicon glyphicon-arrow-down\"></span></a><a class=\"btn btn-mini moveup pull-right\" onclick=\"subform_move_"+el.name+"(this,-1);\"><span class=\"glyphicon glyphicon-arrow-up\"></span></a><span class=\"values\" style=\"display:none;\">'+str+'</span></li>');";
					innerHTML += "}subform_init_"+el.name+"();";
					innerHTML += "\n</script>";
					innerHTML += '<ul class="subform_list">';
					count = 0;
					if (el.value && el.value[el.labelfield] && el.value[el.labelfield].length > 0) {
						count = el.value[el.labelfield].length;
						for(j=0;j<count;j++) {
							innerHTML += "<li>";
							str = '';
							label = '';
							for (p in el.value) {
								if (el.value[p][j]) {
									str += "<input type=\"hidden\" name=\""+p+"[]\" value=\""+el.value[p][j]+"\">";
								}
								if (p == el.labelfield) label = el.value[p][j];
							}
							innerHTML += ""+label+"<a class=\"btn btn-mini delete pull-right\" onclick=\"subform_delete_"+el.name+"(this);\"><span class=\"glyphicon glyphicon-remove\"></span> "+el.deletelabel+"</a><a class=\"btn btn-mini edit pull-right\" onclick=\"subform_edit_"+el.name+"(this);\"><span class=\"glyphicon glyphicon-edit\"></span> "+el.editlabel+"</a><a class=\"btn btn-mini movedown pull-right\" onclick=\"subform_move_"+el.name+"(this,1);\"><span class=\"glyphicon glyphicon-arrow-down\"></span></a><a class=\"btn btn-mini moveup pull-right\" onclick=\"subform_move_"+el.name+"(this,-1);\"><span class=\"glyphicon glyphicon-arrow-up\"></span></a><span class=\"values\" style=\"display:none;\">"+str+"</span></li>";
						}
					}
					innerHTML += '</ul>';
					innerHTML += '<div class="subform_create">';
					innerHTML += '<a class="btn" onclick="subform_edit_'+el.name+'(0);"><span class="glyphicon glyphicon-file"></span> '+el.newlabel+'</a>';
					innerHTML += '</div>';
					innerHTML += '<div class="subform_status"></div>';
					innerHTML += '<div class="subform_edit"></div>';
					innerHTML += '<div class="subform_master"></div>';
					innerHTML += '</div>';
				}break;
				case 'imageupload' : {
					if (el.multiple && el.multiple == true) {
						innerHTML += '<span id="imageupload_preview'+el.name+'" class="imageupload_preview">';
						if (el.value && el.value != null && el.value != '') {
							files = el.value.split(",");
							for(j=0;j<files.length;j++) {
								if (files[j] != '') {
									innerHTML += "<div id=\"image_"+j+el.name+"\"><img src=\""+el.uploadsfolder+'/'+files[j]+"\" style=\"max-width:150px;\"><p><a style=\"cursor:pointer;\" onclick=\"deletePicElement('"+el.uploadsfolder+"/"+files[j]+"','image_"+j+el.name+"');\"><span class=\"glyphicon glyphicon-remove\"></span></a></p></div>";
								}
							}
						}
						innerHTML += '</span>';
						innerHTML += '<span id="imageupload_progress'+el.name+'">';
						innerHTML += '</span>';
						innerHTML += '<span id="'+el.name+'_uploader-basic" class="btn btn-primary btn-mini"'+onfocus+onchange+onclick+'>Upload file</span>'+help;
						innerHTML += '<input type="hidden" id="'+el.name+'" name="'+el.name+'" value="'+((el.value && el.value != null)?el.value:'')+'">';
						innerHTML += "<script language=\"javascript\" type=\"text/javascript\">\n";
						innerHTML += "    function deletePicElement(url,el) {";
						innerHTML += "        newstr=str=$('#"+el.name+"').val().split(',');";
						innerHTML += "        for(i=0;i<str.length;i++) {";
						innerHTML += "            if (str[i] == '') newstr.splice(i,1); if (str[i] == url.replace('"+el.url+"/','')) newstr.splice(i,1);";
						innerHTML += "        }";
						innerHTML += "        $('#"+el.name+"').val(newstr.join(','));";
						innerHTML += "        $('#imageupload_preview"+el.name+" #'+el).remove();";
						innerHTML += "    }";
						innerHTML += "    ";
						innerHTML += "        $fub = $('#"+el.name+"_uploader-basic');";
						innerHTML += "        var uploader = new qq.FineUploaderBasic({";
						innerHTML += "            button: $fub[0],";
						innerHTML += "            request: {";
						innerHTML += "                endpoint: '"+el.uploadurl+"'";
						innerHTML += "            },";
						innerHTML += "            validation: {";
						innerHTML += "                allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],";
						innerHTML += "                sizeLimit: 20004800";
						innerHTML += "            },";
						innerHTML += "            callbacks: {";
						innerHTML += "                onSubmit: function(id, fileName) {";
						innerHTML += "                    $('#imageupload_progress"+el.name+"').append('<div id=\"file-' + id + '"+el.name+"\" class=\"alert\"></div>');";
						innerHTML += "                },";
						innerHTML += "                onUpload: function(id, fileName) {";
						innerHTML += "                    $('#imageupload_progress"+el.name+" #file-' + id + '"+el.name+"').addClass('alert-info').html('<img src=\""+el.libfolder+"/loading.gif\" alt=\"Initializing. Please hold.\"> Initializing '+'\"' + fileName + '\"');";
						innerHTML += "                },";
						innerHTML += "                onProgress: function(id, fileName, loaded, total) {";
						innerHTML += "                    if (loaded < total) {";
						innerHTML += "                        progress = Math.round(loaded / total * 100) + '% of ' + Math.round(total / 1024) + ' kB';";
						innerHTML += "                        $('#imageupload_progress"+el.name+" #file-' + id + '"+el.name+"').removeClass('alert-info').html('<img src=\""+el.libfolder+"/loading.gif\" alt=\"In progress. Please hold.\"> Uploading \"' + fileName + '\" ' +progress);";
						innerHTML += "                    } else {";
						innerHTML += "                        $('#imageupload_progress"+el.name+" #file-' + id + '"+el.name+"').addClass('alert-info').html('<img src=\""+el.libfolder+"/loading.gif\" alt=\"Saving. Please hold.\"> Saving '+'\"'+fileName+'\"');";
						innerHTML += "                    }";
						innerHTML += "                },";
						innerHTML += "                onComplete: function(id, fileName, responseJSON) {";
						innerHTML += "                    if (responseJSON.success) {";
						innerHTML += "                        $('#imageupload_progress"+el.name+" #file-' + id + '"+el.name+"').removeClass('alert-info').addClass('alert-success').html('<span class=\"glyphicon glyphicon-ok\"></span> ' + 'Successfully saved ' +  '\"' + fileName + '\"');"
						innerHTML += "                        var url = '"+el.uploadsfolder+"/'+responseJSON.uploadName;";
						innerHTML += "                        element = $('#"+el.name+"');";
						innerHTML += "                        element.val(element.val()+','+url.replace('"+el.url+"/',''));";
						innerHTML += "                        $('#imageupload_preview"+el.name+"').append('<div id=\"image-' + id + '"+el.name+"\"><img src=\""+el.uploadsfolder+"/'+ responseJSON.uploadName+'\" style=\"max-width:150px;\"><p><a style=\"cursor:pointer;\"><span class=\"glyphicon glyphicon-remove\"></span></a></p></div>');";
						innerHTML += "                        var id2 = id;";
						innerHTML += "						  $('#imageupload_preview"+el.name+" #image-' + id + '"+el.name+" p  a').click(function(){";
						innerHTML += "                            newstr=str=$('#"+el.name+"').val().split(',');";
						innerHTML += "                            for(i=0;i<str.length;i++) {";
						innerHTML += "                                if (str[i] == '') newstr.splice(i,1); if (str[i] == url.replace('"+el.url+"/','')) newstr.splice(i,1);";
						innerHTML += "                            }";
						innerHTML += "                            $('#"+el.name+"').val(newstr.join(','));";
						innerHTML += "                            $('#image-' + id2 + '"+el.name+"').remove();";
						innerHTML += "                        });";
						innerHTML += "                    } else {";
						innerHTML += "                        $('#imageupload_progress"+el.name+" #file-' + id + '"+el.name+"').removeClass('alert-info').addClass('alert-error').html('<span class=\"glyphicon glyphicon-exclamation-sign\"></span> Error with \"' + fileName + '\": ' +responseJSON.error);";
						innerHTML += "                    }";
						innerHTML += "                }";
						innerHTML += "            }";
						innerHTML += "       };";
						innerHTML += "    ";
						innerHTML += "\n</script>";
					}else {
						innerHTML += '<span id="imageupload_preview';
						if (el.name) innerHTML += '_'+el.name;
						innerHTML += '" class="imageupload_preview">';
						if (el.value && el.value != null && el.value != '') {
							innerHTML += '<img src="'+el.uploadsfolder+'/'+el.value+'" style="max-width:150px;">';
							innerHTML += '<p><a style="cursor:pointer;" onclick="$(\'#'+el.name+'\').val(\'\');$(this).parent().parent().children(\'img, p\').remove();"><span class="glyphicon glyphicon-remove"></span></a></p>';
						}
						innerHTML += '</span><span id="';
						if (el.name) innerHTML += el.name;
						innerHTML += '_uploader-basic" class="btn btn-primary btn-mini"'+onfocus+onchange+onclick+'>Upload file</span>';
						innerHTML += ''+help;
						innerHTML += '<input type="hidden" id="'+el.name+'" name="'+el.name+'" value="'+((el.value && el.value != null)?el.value:'')+'">';
						innerHTML += '<script>'+"\n"+'';
						innerHTML += '$fub = $(\'#'+((el.name)?el.name:'')+'_uploader-basic\');';
						innerHTML += '$messages = $(\'#imageupload_preview'+((el.name)?el.name:'')+'\');';
						innerHTML += 'var uploader = new qq.FineUploaderBasic({button: $fub[0],request: {endpoint: \''+el.uploadurl+'\'},validation: {allowedExtensions: [\'jpeg\', \'jpg\', \'gif\', \'png\'],sizeLimit: 20000000},callbacks: {onSubmit: function(id, fileName) {$messages.append(\'\');},onUpload: function(id, fileName) {$(\'#imageupload_preview';
						if (el.name) innerHTML += '_'+el.name;
						innerHTML += '\').addClass(\'alert-info\').html(\'<img src="'+el.libfolder+'/loading.gif" alt="Initializing. Please hold."> \' +\'Initializing \' +\'"\' + fileName + \'"\');},onProgress: function(id, fileName, loaded, total) {if (loaded < total) {progress = Math.round(loaded / total * 100) + \'% of \' + Math.round(total / 1024) + \' kB\';$(\'#imageupload_preview';
						if (el.name) innerHTML += '_'+el.name;
						innerHTML += '\').removeClass(\'alert-info\').html(\'<img src="'+el.libfolder+'/loading.gif" alt="In progress. Please hold."> \' +\'Uploading \' +\'"\' + fileName + \'" \' +progress);} else {$(\'#imageupload_preview';
						if (el.name) innerHTML += '_'+el.name;
						innerHTML += '\').addClass(\'alert-info\').html(\'<img src="'+el.libfolder+'/loading.gif" alt="Saving. Please hold."> \' +\'Saving \' +\'"\' + fileName + \'"\');}},';
						innerHTML += 'onComplete: function(id, fileName, responseJSON) {if (responseJSON.success) {url = \''+el.uploadsfolder+'/\'+responseJSON.uploadName;$(\'#'+el.name+'\').val(url.replace(\''+el.uploadsfolder+'/\',\'\'));';
						innerHTML += '$(\'#imageupload_preview';
						if (el.name) innerHTML += '_'+el.name;
						innerHTML += '\').removeClass(\'alert-info\').removeClass(\'alert-error\').html(\'<img src="'+el.uploadsfolder+'/\'+ responseJSON.uploadName+\'" style="max-width:150px;"><p><a style="cursor:pointer;" onclick="$(\\\'#'+el.name+'\\\').val(\\\'\\\');$(this).parent().parent().children(\\\'img, p\\\').remove();"><span class="glyphicon glyphicon-remove"></span></a></p>\');';
						innerHTML += '} else {console.info("upload error");$(\'#imageupload_preview';
						if (el.name) innerHTML += '_'+el.name;
						innerHTML += '\').removeClass(\'alert-info\').addClass(\'alert-error\').html(\'<span class="glyphicon glyphicon-exclamation-sign"></span> \' +\'Error with \' +\'"\' + fileName + \'": \' +responseJSON.error);}}}});'+"\n</script>";
					}
				}break;
				case 'fileupload' : {
					innerHTML += '<span id="fileupload_preview';
					if (el.name) innerHTML += '_'+el.name;
					innerHTML += '" class="fileupload_preview">';
					if (el.value != null) innerHTML += '<a href="{uploadurl}/'+el.value+'" target="_blank">'+el.value+'</a>';
					innerHTML += '</span>';
					innerHTML += '<span id="';
					if (el.name) innerHTML += el.name;
					innerHTML += '_uploader-basic" class="btn btn-primary btn-xs"'+onfocus+onchange+onclick+'>'+((el.buttonicon)?'<span class="'+el.buttonicon+'"></span> ':'')+'Upload file</span>';
					innerHTML += ''+help;
                    var ext = '';
					if (el.extensions) {
						for(i=0;i<el.extensions;i++) {
							ext += ((i > 0)?',':'')+'\''+el.extensions[i];
						}
					}
                    else ext += '\'doc\', \'pdf\', \'txt\', \'rtf\', \'avi\', \'mp4\', \'mp3\', \'mp3\'';
					innerHTML += '<input type="hidden" id="'+el.name+'" name="'+el.name+'" value="'+((el.value && el.value != null)?el.value:'')+'"><script>'+"\n"+'$fub = $(\'#';
					if (el.name) innerHTML += el.name;
					innerHTML += '_uploader-basic\');$messages = $(\'#fileupload_preview';
					if (el.name) innerHTML += '_'+el.name;
					innerHTML += '\');var uploader = new qq.FineUploaderBasic({button: $fub[0],request: {endpoint: \''+el.uploadurl+'\'},validation: {allowedExtensions: [';
                    innerHTML += ext;
					innerHTML += '],sizeLimit: 20000000},callbacks: {onSubmit: function(id, fileName) {$messages.append(\'\');},onUpload: function(id, fileName) {$(\'#fileupload_preview';
					if (el.name) innerHTML += '_'+el.name;
					innerHTML += '\').addClass(\'alert-info\').html(\'<img src="'+el.libfolder+'/loading.gif" alt="Initializing. Please hold."> \' +\'Initializing \' +\'"\' + fileName + \'"\');},onProgress: function(id, fileName, loaded, total) {if (loaded < total) {progress = Math.round(loaded / total * 100) + \'% of \' + Math.round(total / 1024) + \' kB\';$(\'#fileupload_preview';
					if (el.name) innerHTML += '_'+el.name;
					innerHTML += '\').removeClass(\'alert-info\').html(\'<img src="'+el.libfolder+'/loading.gif" alt="In progress. Please hold."> \' +\'Uploading \' +\'"\' + fileName + \'" \' +progress);} else {$(\'#fileupload_preview';
					if (el.name) innerHTML += '_'+el.name;
					innerHTML += '\').addClass(\'alert-info\').html(\'<img src="'+el.libfolder+'/loading.gif" alt="Saving. Please hold."> \' +\'Saving \' +\'"\' + fileName + \'"\');}},';
					innerHTML += 'onComplete: function(id, fileName, responseJSON) {if (responseJSON.success) {url = \''+el.uploadsfolder+'/\'+responseJSON.uploadName;$(\'#'+el.name+'\').val(url.replace(\''+el.url+'/\',\'\'));';
					innerHTML += '$(\'#fileupload_preview';
					if (el.name) innerHTML += '_'+el.name;
					innerHTML += '\').removeClass(\'alert-info\').removeClass(\'alert-error\').html(\'<a href="'+el.uploadsfolder+'/\'+responseJSON.uploadName+\'">\'+responseJSON.uploadName+\'</a>\');';
					innerHTML += '$(\'#';
					if (el.name) innerHTML += el.name;
					innerHTML += '_uploader-basic\').removeClass(\'qq-upload-button-hover\');';
					innerHTML += '} else {$(\'#fileupload_preview';
					if (el.name) innerHTML += '_'+el.name;
					innerHTML += '\').removeClass(\'alert-info\').addClass(\'alert-error\').html(\'<span class="glyphicon glyphicon-exclamation-sign"></span> \' +\'Error with \' +\'"\' + fileName + \'": \' +responseJSON.error);}}}});'+"\n</script>";
				}break;
				case 'dragndropuploader' :{
					innerHTML += '<div id="dragndropuploader';
					if (el.name) innerHTML += '_'+el.name;
					innerHTML += '" class="dropzone dropzone-previews">';
					if (el.files) {
						for(var i = 0; i < el.files.length; i++) {
							innerHTML += '<div class="dz-preview dz-processing dz-image-preview dz-success"><div class="dz-details"><div class="dz-filename"><span data-dz-name="">'+el.files[i]+'</span></div><div data-dz-size="" class="dz-size"></div><img data-dz-thumbnail="" alt="'+el.files[i]+'" src="'+el.uploadurl+((el.folder)?el.folder+'/':'')+el.files[i]+'"></div><div class="dz-progress"><span data-dz-uploadprogress="" class="dz-upload" style="width: 100%;"></span></div><div class="dz-success-mark"><span>✔</span></div><div class="dz-error-mark"><span>✘</span></div><div class="dz-error-message"><span data-dz-errormessage=""></span></div><a data-dz-remove="" onclick="javascript:dz_removeFile(\''+el.uploadurl+((el.folder)?el.folder+'/':'')+el.files[i]+'\',this);" class="dz-remove">Remove file</a></div>';
						}
					}
					innerHTML += '</div><script type="text/javascript" language="javascript">'+"\n"+
						'function dz_removeFile(file,el){'+
							'$.post( "{url}'+el.removeurl+'", { file: file }, function(data){'+
								'if (data.success && el) $(el).parent(".dz-preview").remove();'+
							'});'+
						'}'+
						'$("div#dragndropuploader'+((el.name)?'_'+el.name:'')+'").dropzone({'+
							'url: "'+el.posturl+'", '+
							'paramName: "qqfile", '+
							'addRemoveLinks: true, '+
							'removedfile : function(file){'+
								'dz_removeFile("'+el.uploadurl+((el.folder)?el.folder+'/':'')+'"+file.name);'+
								'var _ref;return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;'+
							'}'+
						'});';
					if (el.files && el.files.length) innerHTML += '$("div#dragndropuploader'+((el.name)?'_'+el.name:'')+' .dz-message").css("display","none");';
					innerHTML += "\n</script>";
				}break;
				case 'imagesize' : {
					if (el.value && el.value != null) val = el.value.split(",");
					else val = new Array(0,0);
					innerHTML += '<input type="hidden" name="'+el.name+'" id="'+el.name+'" value="'+el.value+'">';
					if (!el.onchange) el.onchange = '';
					innerHTML += '<input type="text" size="4" name="'+el.name+'_x" id="'+el.name+'_x" value="'+val[0]+'" onkeyup="val = $(\'#'+el.name+'_x\').val(); if (parseInt(val) == 0 || parseInt(val) == \'NaN\') $(\'#'+el.name+'_x\').attr(\'value\',val.substring(0, val.length - 1)); else $(\'#'+el.name+'\').val(parseInt($(\'#'+el.name+'_x\').val())+\',\'+parseInt($(\'#'+el.name+'_y\').val()));"'+onchange+onclick+onfocus+cls+'><span class="input-group-addon">x</span><input type="text" size="4" name="'+el.name+'_y" id="'+el.name+'_y" value="'+val[1]+'" onkeyup="val = $(\'#'+el.name+'_y\').val(); if (parseInt(val) == 0 || parseInt(val) == \'NaN\') $(\'#'+el.name+'_y\').attr(\'value\',val.substring(0, val.length - 1)); else $(\'#'+el.name+'\').val(parseInt($(\'#'+el.name+'_x\').val())+\',\'+parseInt($(\'#'+el.name+'_y\').val()));"'+onchange+onclick+onfocus+cls+'>'
				}break;
				case 'tile' : {
					innerHTML += '<div class="tileformfield '+el['class']+'" id="tileformfield_'+el.id+'" '+onfocus+cls+'>';
					innerHTML += '<textarea class="hidden" name="'+el.name+'" id="'+el.id+'" '+onchange+'>'+el.value+'</textarea>';
					innerHTML += '<style type="text/css">#tileformfield_'+el.id+' .tile{';
					if (el.minwidth) innerHTML += 'min-width:'+el.minwidth+';';
					if (el.maxwidth) innerHTML += 'max-width:'+el.maxwidth+';';
					innerHTML += '}</style><div class="tiles">';
					for(var i = 0; i < el.options.length; i++) {
						innerHTML += '<div class="tile '+((el.value == el.options[i].value)?'selected':'')+'" '+onclick+'><textarea class="hidden value">'+el.options[i].value+'</textarea>'+el.options[i].title+'</div>';
					}
					innerHTML += '</div></div>';
					innerHTML += '<script language="javascript" type="text/javascript">$(\'#tileformfield_'+el.id+' .tile\').click(function(e){'+
							'$("#tileformfield_'+el.id+'").children(".tiles").children(".selected").removeClass("selected");'+
							'$(this).addClass("selected");'+
							'$("#'+el.id+'").html($(this).children("textarea.hidden.value").val()).trigger("change");'+
						'});</script>';
				}break;
				case 'size' : {
					var nbboxes = el.dimensions.length;
					if (el.value && el.value != null) val = el.value.split(",");
					else {
						val = new Array();
						for(var i = 0; i < nbboxes; i++) val[i] = 0;
					}
					if (!el.onchange) el.onchange = '';
					var script = '';
					innerHTML += '<a class="input-group-addon" onclick="copyvalue_'+el.name+'();">+</a>';
					for(var i = 0; i < nbboxes; i++) {
						innerHTML += '<input type="text" size="'+el.size+'" name="'+el.name+'_'+i+'" id="'+el.name+'_'+i+'" value="'+val[i]+'"'+onchange+onclick+onfocus+cls+'><a class="input-group-addon">'+el.dimensions[i]+'</a>';
						script += '$("#'+el.name+'_'+i+'").on("keyup",function(){updatevalue_'+el.name+'();});';
					}
					if (el.units) {
						innerHTML += '<select class="form-control" name="'+el.name+'_units" id="'+el.name+'_units" onchange="updatevalue_'+el.name+'();">'
						for(var i = 0; i < el.units.length; i++){
							innerHTML += '<option value="'+el.units[i]+'">'+el.units[i]+'</option>';
						}
						innerHTML += '</select>';
					}
					innerHTML += '<input type="hidden" name="'+el.name+'" id="'+el.name+'" value="'+el.value+'">';
					var updatescript = '<script>'+
						'function getvalue_'+el.name+'(nb){'+
							'var selector = "#'+el.name+'_"+nb; var val = $(selector).val();'+
							'if (parseInt(val) == 0 || parseInt(val) == \'NaN\') $(selector).attr(\'value\',val.substring(0, val.length - 1));'+
							'return val;'+
						'}'+
						'function copyvalue_'+el.name+'(){'+
							'var value = getvalue_'+el.name+'(0);'+
							'for (var i = 1; i < '+nbboxes+'; i++) {'+
								'$("#'+el.name+'_"+i).attr("value",value);'+
							'}'+
							'updatevalue_'+el.name+'();'+
						'}'+
						'function updatevalue_'+el.name+'(){'+
							'var value = "";'+
							'for(var i = 0; i < '+nbboxes+'; i++) {'+
								'value += ((i>0)?",":"")+getvalue_'+el.name+'(i);'+
							'}';
							if (el.units) {
								updatescript += 'value += ","+$("#'+el.name+'_units").val();';
							}
						updatescript +=
							'$("#'+el.name+'").attr("value",value);'+
						'}'+
						script+
						'</script>';
						innerHTML += updatescript;
				}break;
				case 'tree' : {
					innerHTML += '<input type="hidden" name="'+el.name+'" id="'+el.name+'" value="'+el.value+'"><div class="formtree" id="'+el.name+'_tree"></div>';
					innerHTML += "<script language=\"javascript\" type=\"text/javascript\">\n";
					innerHTML += "$('#"+el.name+"_tree').bind('before.jstree', function (e, data){$('#alog').append(data.func + '<br />');})";
					innerHTML += ".jstree({'plugins':['themes','json_data','ui','crrm','cookies','dnd','types'],";
					innerHTML += "'themes':{'theme': 'lucid', 'url' : '{url}/lib/jstree/1.0rc3/themes/lucid/style.css', 'dots': true, 'icons': true},";
					innerHTML += "'json_data': {'ajax': {'url': '"+el.dataurl+"', 'data': function (n) {return { 'operation' : 'get_children', 'id' : n.attr ? n.attr('id').replace('node_',''):1};}}}";
					if (el.contenttypes) innerHTML += ",'types' : { 'max_depth' : -2,'max_children' : -2,'valid_children' : [ 'drive' ], 'types' : {"+el.contenttypes+"}}";
					innerHTML += "})";
					innerHTML += ".bind('select_node.jstree', function (event, data) {$('#"+el.name+"').val(data.rslt.obj.attr('id').replace('node_',''));});";
					innerHTML += "\n</script>";
				}break;
				case 'label' : {
					innerHTML += '<span '+cls+id+name+type+value+disabled+onfocus+onchange+onclick+'>'+el.value+'</span>';
				}break;
				case 'password' : {
					innerHTML += '<input'+cls+id+name+type+value+disabled+onfocus+onchange+onclick;
					if (el.placeholder) innerHTML += ' placeholder="'+el.placeholder+'"';
					innerHTML += '>'+help;
				}break;
				case 'radio' : {
					if (el.options) {
						for(var j=0;j<el.options.length;j++) {
							innerHTML += '<div class="radio">';
							var inline = '';
							if (el.inline) inline = ' inline';
							innerHTML += '<label class="radio'+inline+'"><input'+cls.replace('form-control','')+id+name+type+disabled+onfocus+onchange+onclick+' value="'+el.options[j].value+'"';
							if (el.options[j].value == el.value) innerHTML += ' checked';
							innerHTML += '>';
							if (el.options[j].title) innerHTML += '&nbsp;'+el.options[j].title;
							innerHTML += '</label>';
							innerHTML += '</div>';
						}
					}
					innerHTML += help;
				}break;
				case 'yesno' : {
                    var toggleID = el.name+"_toggle_"+Math.floor((Math.random() * 1000000) + 1);
					innerHTML += '<div class="onoffswitch">';
					innerHTML += '<input type="hidden" name="'+el.name+'" id="'+el.name+'" value="'+el.value+'"'+((el['class'])?' class="'+el['class']+'"':'')+'">';
                    innerHTML += '<input type="checkbox" name="'+toggleID+'_c" value="1" class="onoffswitch-checkbox" id="'+toggleID+'_c_myonoffswitch"';
					if (el.value == 1) innerHTML += ' checked';
					innerHTML += ' style="display:none;" onchange="$(this).prev(\'input\').val(($(this).prop(\'checked\'))?1:0).trigger(\'change\').trigger(\'blur\');">';
                    innerHTML += '<label class="onoffswitch-label" for="'+toggleID+'_c_myonoffswitch" onclick="$(this).prev(\'input\').prev(\'input\').trigger(\'change\').trigger(\'blur\');">';
                    innerHTML += '<div class="onoffswitch-inner"><div class="onoffswitch-active"><div class="onoffswitch-switch">YES</div></div><div class="onoffswitch-inactive"><div class="onoffswitch-switch">NO</div></div>';
					innerHTML += '</div></label></div>';
				}break;
				case 'checkbox' : {
					inline = '';
					if (el.inline) inline = ' inline';
					if (el.uncheckedval) uncheck = el.uncheckedval;
					else uncheck = '';
					if (el.options) {
						for(var j=0;j<el.options.length;j++) {
							innerHTML += '<div class="radio">';
							checked = false;
							if (typeof(el.value)=='object'&&(el.value instanceof Array)) {
								for(k=0;k<el.value.length;k++) {
									if (el.value[k] == el.options[j].value) checked = true;
								}
							}else if (el.options[j].value == el.value) checked = true;
							innerHTML += '<label class="checkbox'+inline+'">';
							innerHTML += '<input type="hidden" id="'+el.name+'_'+j+'" name="'+el.name;
							if (el.options.length>1) innerHTML += '[]';
							innerHTML += '" value="';
							if (checked) innerHTML += el.options[j].value;
							else innerHTML += uncheck;
							innerHTML +='"><input'+cls.replace('form-control','')+id+type+disabled+onfocus+onchange;
							innerHTML += ' onclick="';
							if (el.showhelponclick) innerHTML += '$(\'#'+el.name+'_'+j+'_help\').toggle();';
							innerHTML += 'if (this.checked) {$(\'#'+el.name+'_'+j+'\').attr(\'value\',this.value);}else{$(\'#'+el.name+'_'+j+'\').attr(\'value\',\''+uncheck+'\');}';
							if (el.onclick) innerHTML += el.onclick;
							innerHTML += '" name="'+el.name+'_chk';
							if (el.options.length>1) innerHTML += '[]';
							innerHTML += '" value="'+el.options[j].value+'"';
							if (checked) innerHTML += ' checked';
							innerHTML += '>';
							if (el.options[j].title) innerHTML += el.options[j].title;
							if (el.options[j].help && el.options[j].help != '') {
								innerHTML += '<div id="'+el.name+'_'+j+'_help" style="display:none;">'+el.options[j].help+'</div>';
							}
							innerHTML += '</label>';
							innerHTML += '</div>';
						}
					}
					innerHTML += help;

				}break;
				case 'custom' : {
					innerHTML += '<div'+cls+name.replace('name=','id=')+name+onfocus+onchange+onclick+'>'+el.html+'</div>';
				}break;
				case 'textarea' :
				case 'htmleditor' : {
					innerHTML += '<textarea class="';
					if (el['class']) innerHTML += el['class'];
					if (el.type == 'htmleditor') {
						if (el['class']) innerHTML += ' ';
						innerHTML += el.provider;
					}
					innerHTML += '"';
					if (el.id && el.type != 'htmleditor') {
						innerHTML += ' id="'+el.id+'"';
					}else {
						innerHTML += ' id="'+el.name+'"';
					}
					innerHTML += name+disabled;
					if (el.rows) innerHTML += ' rows="'+el.rows+'"';
					if (el.cols) innerHTML += ' cols="'+el.cols+'"';
                    if (el.autocomplete) innerHTML += ' autocomplete="'+el.autocomplete+'"';
                    if (el.autocapitalize) innerHTML += ' autocapitalize="'+el.autocapitalize+'"';
                    if (el.autocorrect) innerHTML += ' autocorrect="'+el.autocorrect+'"';
                    if (el.spellcheck) innerHTML += ' spellcheck="'+el.spellcheck+'"';
					innerHTML += '>'+el.value+'</textarea>'+help;
					if (el.type == 'htmleditor') {
						if (el.provider == 'ckeditor') {
							if (typeof CKEDITOR == "undefined" && $this.data('options').scripts.indexOf('/ckeditor.js') == -1) {
								$this.data('options').scripts += '<script language="javascript" type="text/javascript">'+"\n"+'CKEDITOR_BASEPATH = \'{url}lib/ckeditor/4.5.9/\';CKFINDER_BASEPATH = \'{url}lib/ckfinder/2.4.2/\';'+"\n"+'</script>';
								$this.data('options').scripts += '<script language="javascript" type="text/javascript" src="{url}com/system/editor/ckeditor/4.5.9/'+((el.configfile)?el.configfile:'config')+'.js"></script>';
								$this.data('options').scripts += '<script language="javascript" type="text/javascript" src="{url}lib/ckfinder/2.4.2/ckfinder.js"></script>';
								$this.data('options').scripts += '<script language="javascript" type="text/javascript" src="{url}lib/ckeditor/4.5.9/ckeditor.js"></script>';
							}
							outerHTML += '<script language="javascript" type="text/javascript">'+"\n"+
                                'try{'+
                                '   if (CKEDITOR != undefined && CKEDITOR.instances != undefined) {'+
                                '       CKEDITOR.instances.'+el.name+'.destroy(true);'+
                                '       delete CKEDITOR.instances.'+el.name+';'+
                                '   }'+
                                '}'+
                                'catch(err){}'+
                                'try{'+
                                '   CKEDITOR_BASEPATH = \'{url}lib/ckeditor/4.5.9/\';'+
                                '   CKFINDER_BASEPATH = \'{url}lib/ckfinder/2.4.2/\';'+
                                '   CKEDITOR.config.forcePasteAsPlainText = true;'+
                                '   CKEDITOR.config.customConfig = "{url}com/system/editor/ckeditor/4.5.9/'+((el.configfile)?el.configfile:'config')+'.js";'+
                                '   CKEDITOR.replace("'+el.name+'",{'+
                                '      language: "'+settings.lang+'",'+
                                '      filebrowserBrowseUrl: "{url}lib/ckfinder/2.4.2/ckfinder.html",'+
                                '      filebrowserImageBrowseUrl: "{url}lib/ckfinder/2.4.2/ckfinder.html?Type=Images",'+
                                '      filebrowserFlashBrowseUrl: "{url}lib/ckfinder/2.4.2/ckfinder.html?Type=Flash",'+
                                '      filebrowserUploadUrl: "{url}lib/ckfinder/2.4.2/core/connector/php/connector.php?command=QuickUpload&type=Files",'+
                                '      filebrowserImageUploadUrl: "{url}lib/ckfinder/2.4.2/core/connector/php/connector.php?command=QuickUpload&type=Images",'+
                                '      filebrowserFlashUploadUrl: "{url}lib/ckfinder/2.4.2/core/connector/php/connector.php?command=QuickUpload&type=Flash"'+
                                '   });'+
                                '}catch(e){}'+
                                "\n"+'</script>';
						}
					}
				}break;
				case 'select' : {
					multiple = '';
					if (el.multiple) {
						multiple = ' multiple="multiple" size="6"';
						name = ' name="'+el.name+'[]"';
					}
					innerHTML += '<select'+cls+id+name+multiple+disabled+onfocus+onchange+onclick+'>';
					if (el.options) {
						for(var j=0;j<el.options.length;j++){
							innerHTML += '<option value="'+el.options[j].value+'"';
							if ($.isArray(el.value)) {
								for(var key = 0; key < el.value.length; key++) {
									if (el.value[key] == el.options[j].value) innerHTML += ' selected';
								}
							}else if (el.value == el.options[j].value) innerHTML += ' selected';
							innerHTML += '>'+el.options[j].title+'</option>';
						}
					}
					innerHTML += '</select>'+help;
					innerHTML += '<script language="javascript" type="text/javascript">'+"\n";
                    var disable_search_threshold = (el.disable_search_threshold)?el.disable_search_threshold:10;
                    var search_contains = (el.search_contains)?el.search_contains:'true';
                    var create_option = (el.create_option)?el.create_option:'false';
                    var persistent_create_option = (el.persistent_create_option)?el.persistent_create_option:'false';
                    var skip_no_results = (el.skip_no_results)?el.skip_no_results:'false';
					if (el.id == undefined) el.id = el.name;
					innerHTML += 'try{$("#'+el.id+'").chosen({disable_search_threshold: '+disable_search_threshold+',search_contains:'+search_contains+',width: "100%", create_option: '+create_option+', persistent_create_option: '+persistent_create_option+', skip_no_results:'+skip_no_results+'});}catch(err){}';
					innerHTML += "\n"+'</script>';
				}break;
				case 'tagselector': {
					id = 'id="'+el.name+'"';
					if (value == 'value') value = '';
					if (value == ',,') value = '';
					innerHTML += '<input'+cls+id+name+type+value+disabled+onfocus+onchange+onclick;
					if (el.placeholder) innerHTML += ' placeholder="'+el.placeholder+'"';
					innerHTML += '>'+help;
					innerHTML += '<script language="javascript" type="text/javascript">'+"\n";
					innerHTML += "$('#"+el.name+"').tokens({cssClasses : {'token-list' : 'tokens-token-list form-control','list-input-holder' : 'tokens-list-input-holder','list-token-holder' : 'tokens-list-token-holder', 'input-text' : 'tokens-input-text','delete-anchor' : 'tokens-delete-token', 'suggestion-selector' : 'tokens-suggestion-selector',	'suggestions-list-element' : 'tokens-suggestions-list-element',	'highlighted-suggestion' : 'tokens-highlighted-suggestion'},source:[";
					for (var i = 0; i < el.options.length; i++) {
						if (i > 0) innerHTML += ",";
						innerHTML += "'" + el.options[i].title + "'";
					}
					innerHTML += "],initValue:[";
					var vals = el.value.slice(1,-1).split(",");
					var firstval = false;
					for(var i = 0; i < vals.length; i++) {
						if (vals[i].length > 0) {
							if (firstval) innerHTML += ",";
							innerHTML += "'"+vals[i]+"'";
							firstval = true;
						}
					}
					innerHTML += "]});";
					innerHTML += '</script>';
				}break;
				case 'readonly' :{
					cls = ' class="';
					if (el['class']) cls += el['class'];
					cls += ' uneditable-input"';
					innerHTML += '<span'+cls+id+name+onfocus+onchange+onclick+'>'+el.value+'</span>';
				}break;
				case 'placeholder' : {
					innerHTML += '<div'+cls+name.replace('name=','id=')+name+onfocus+onchange+onclick+'></div>';
				}break;
				case 'itemmanager' : {
					cls = ' class="'+el['class']+' itemmanager_form_field"';
					innerHTML += '<div'+cls+name.replace('name=','id=')+name+onfocus+onchange+onclick+'>';
					innerHTML += '<script language="javascript" type="text/javascript">'+"\n";
					innerHTML += 'try{$("#itemmanager_duplicatesrc_'+el.name+'").chosen({disable_search_threshold: 10,width: "100%"});$("#itemmanager_typeselector_'+el.name+'").chosen({disable_search_threshold: 10,width: "100%"});}catch(err){}';
					innerHTML += 'function itemmanager_edit_'+el.name+"(id){var data = {'id':id,'parent':'"+el.parent+"'};";
					if (el.types && el.types.length > 0) innerHTML += "data['selectedtype'] = $(\'#itemmanager_typeselector_"+el.name+"\').val();";
					innerHTML += "if (/*data['selectedtype']*/1) $.ajax({async:false,type:'POST',url:'"+el.edit+"',data,success:";
					innerHTML += "function(r){if(!r.success == '1'){$('#"+el.name+" > .itemmanager_status').html('<div class=\\\'error\\\'>"+el.message+"</div>');}else{$('#"+el.name+" > .itemmanager_status').html('');$('#"+el.name+" > div.itemmanager_edit').lucidForm(r.form);$('#"+el.name+" > div.itemmanager_edit input[type=submit], #"+el.name+" > div.itemmanager_edit button[type=submit]').bind('click',function(e){e.preventDefault();itemmanager_save_"+el.name+"();});}}";
					innerHTML += "});};";
					innerHTML += 'function itemmanager_duplicate_'+el.name+"(id){$.ajax({async:false,type:'POST',url:'"+el.duplicate+"',data:{'id':id,'parent':'"+el.parent+"'},success:";
					innerHTML += "function(r){if(!r.success == '1'){$('#"+el.name+" > .itemmanager_status').html('<div class=\\\'error\\\'>"+el.message+"</div>');}else{$('#"+el.name+" > .itemmanager_status').html('');$('#"+el.name+" > div.itemmanager_edit').lucidForm(r.form);$('#"+el.name+" > div.itemmanager_edit input[type=submit], #"+el.name+" > div.itemmanager_edit button[type=submit]').bind('click',function(e){e.preventDefault();itemmanager_save_"+el.name+"();});}}";
					innerHTML += "});};";
					innerHTML += 'function itemmanager_delete_'+el.name+"(id){$.ajax({async:false,type:'POST',url:'"+el['delete']+"',data:{'id':id,'parent':'"+el.parent+"'},success:function(r){if(!r.success == '1'){$('#"+el.name+" > .itemmanager_status').html('<div class=\"error\">'+r.message+'</div>');}else{$('#"+el.name+" > .itemmanager_status').html('<div class=\"success\">'+r.message+'</div>');$('#"+el.name+" > div.itemmanager_edit').html('');}}});}";
					innerHTML += "function itemmanager_list_"+el.name+"(){$.ajax({type:'POST',url:'"+el.list+"',data:{'parent':'"+el.parent+"'},success:function(data){$('#"+el.name+" > div.itemmanager_list').html();str = '';if (!data) return; else for(items=0;items<data.data.length;items++){str+='<div class=\"col-"+settings.colsize+"-"+settings.gridsize+"\">'+data.data[items].title+'<a class=\"btn btn-mini delete pull-right\" onclick=\"itemmanager_delete_"+el.name+"('+data.data[items].id+');itemmanager_list_"+el.name+"();\"><span class=\"glyphicon glyphicon-remove\"></span> "+el.deletelabel+"</a><a class=\"btn btn-mini edit pull-right\" onclick=\"itemmanager_edit_"+el.name+"('+data.data[items].id+');\"><span class=\"glyphicon glyphicon-edit\"></span> "+el.editlabel+"</a><a class=\"btn btn-mini movedown pull-right\" onclick=\"itemmanager_move_"+el.name+"('+data.data[items].id+',1);\"><span class=\"glyphicon glyphicon-arrow-down\"></span></a><a class=\"btn btn-mini moveup pull-right\" onclick=\"itemmanager_move_"+el.name+"('+data.data[items].id+',-1);\"><span class=\"glyphicon glyphicon-arrow-up\"></span></a></div>';}$('#"+el.name+" > div.itemmanager_list').html(str);}});}";
					innerHTML += "function itemmanager_populateselect_"+el.name+"(){$.ajax({type:'POST',url:'"+el.list+"',data:{'parent':0},success:function(data){$('#"+el.name+" select#itemmanager_duplicatesrc_"+el.name+"').html();if (!data) return; else for(items=0;items<data.data.length;items++){$('#"+el.name+" select#itemmanager_duplicatesrc_"+el.name+"').append('<option value=\"'+data.data[items].id+'\">'+data.data[items].title+'</options>');}try{$('#"+el.name+" select#itemmanager_duplicatesrc_"+el.name+"').chosen({inherit_select_classes:true,disable_search_threshold: 10,width: '100%'}).trigger('chosen:updated');}catch(err){};}});}";
					innerHTML += "function itemmanager_save_"+el.name+"(){try{if (CKEDITOR != undefined && CKEDITOR.instances) {for (instance in CKEDITOR.instances) {$('#'+instance).val(CKEDITOR.instances[instance].getData());ckdata = $('#"+el.name+" > textarea');for(i=0;i<ckdata.length;i++) if(ckdata[i].attr('name') == instance) ckdata[i].val(CKEDITOR.instances[instance].getData());}}}catch(e){}$.ajax({type:'POST',url:'"+el.save+"',data: $('#"+el.name+" > div.itemmanager_edit input, #"+el.name+" > div.itemmanager_edit textarea, #"+el.name+" > div.itemmanager_edit select').serialize()+'&parent="+el.parent+"',success:function(data){$('#"+el.name+" > div.itemmanager_status').html('<div class=\\\'success\\\'>'+data.message+'</div>');$('#"+el.name+" > div.itemmanager_edit').html('');itemmanager_list_"+el.name+"();}});}";
					innerHTML += "function itemmanager_move_"+el.name+"(id,offset){$.ajax({type:'POST',url:'"+el.move+"',data: {'id':id,'offset':offset},success:function(data){itemmanager_list_"+el.name+"();}});}";
					innerHTML += "itemmanager_list_"+el.name+"();itemmanager_populateselect_"+el.name+"();\n</script>";
					innerHTML += '<div class="itemmanager_list row grid-'+settings.gridsize+'"></div>';
					innerHTML += '<div class="itemmanager_create row grid-'+settings.gridsize+'">';
					innerHTML += '<div class="col-'+settings.colsize+'-'+(settings.gridsize/2)+'">';
					if (el.types && el.types.length > 0) {
						innerHTML += '<div class="form-group"><div class="input-group"><select name="itemmanager_typeselector_'+el.name+'" id="itemmanager_typeselector_'+el.name+'">';
						for (var i = 0; i < el.types.length; i++){
							innerHTML += '<option value="'+el.types[i].value+'">'+methods.escapeString.apply(this,[String(el.types[i].title).replace(/&/g,'&amp;')])+'</option>';
						}
						innerHTML += '</select>';
					}
					innerHTML += '<a class="'+((el.types && el.types.length)?'input-group-addon ':' ')+'btn btn-primary" onclick="itemmanager_edit_'+el.name+'(0);"><span class="glyphicon glyphicon-file"></span> '+el.newlabel+'</a>&nbsp;';
					if (el.types && el.types.length) innerHTML += '</div></div>'
					innerHTML += '</div><div class="col-'+settings.colsize+'-'+(settings.gridsize/2)+'"><div class="form-group"><div class="input-group">';
					innerHTML += '<select id="itemmanager_duplicatesrc_'+el.name+'" name="itemmanager_duplicatesrc_'+el.name+'"></select>';
					innerHTML += '<a class="input-group-addon btn btn-primary" onclick="itemmanager_duplicate_'+el.name+'($(\'#'+el.name+' select#itemmanager_duplicatesrc_'+el.name+'\').val());"><span class="glyphicon glyphicon-plus"></span> '+el.duplicatelabel+'</a>';
					innerHTML += '</div></div></div></div>';
					innerHTML += '<div class="itemmanager_status"></div>';
					innerHTML += '<div class="itemmanager_edit"></div>';
					innerHTML += '</div>';
				}break;
				case 'spacer' : {
					innerHTML += '<p></p>';
				}break;
				case 'button' :
				case 'buttons' :
				case 'submit' :
				case 'reset' :
				case 'btn' : {
					primary = ' btn-primary';
					if (String(el['class']).indexOf('btn-') !== -1) primary = el['class'];
					else primary = 'btn'+primary;
					var icon = '';
                    if (el.disabled && el.disabled == true) disabled = ' disabled';
					if (typeof(input)=='object'&&(input instanceof Array)) {
						innerHTML += '<div>';
						for(var j=0;j<el.items; j++) {
							if (j>0) primary = '';
							icon = (el.items[j].icon)?'<span class="glyphicon glyphicon-'+el.items[j].icon+'"></span>':'';
							innerHTML += '<button type="'+el.items[j].type+'" class="'+primary+'"'+id+name+onfocus+onchange+onclick+disabled+'>'+icon+el.items[j].title+'</button>';
						}
						innerHTML += '</div>'+help;
					}else {
						icon = (el.icon)?'<span class="glyphicon glyphicon-'+el.icon+'"></span>':'';
						innerHTML += '<div><button type="'+el.type+'" class="'+primary+'"'+id+name+onfocus+onchange+onclick+disabled+'>'+icon+el.title+'</button></div>'+help;
					}
				}break;
				case 'lucideditor' : {
					if ($('.lucideditor').length < 1) {
						$('head').append('<link rel="stylesheet" type="text/css" href="{themeurl}css/lucideditor.css">');
					}
					var ress=[];
					if (typeof(el.ressources) == 'array' || typeof(el.ressources) == 'object') {
						ress = el.ressources[el.lang];
					}
					innerHTML += '<div class="lucideditor_'+el.name+'"></div><script>	try{CKEDITOR.instances.'+el.name+'.destroy(true);delete CKEDITOR.instances.'+el.name+';}catch(err){} setTimeout( function(){try{$(".lucideditor_'+el.name+'").lucidEditor("destroy");}catch(e){}$(".lucideditor_'+el.name+'").lucidEditor({name: "'+el.name+'",value: "'+((el.value != 'null' && el.value != null)?methods.escapeString.apply(this,[el.value]).replace(/script/g,"scri\"+\"pt"):'')+'",url:"'+el.url+'",saveurl:"'+el.saveurl+'",renderurl:"'+el.renderurl+'",removeurl:"'+el.removeurl+'",lang:"'+el.lang+'",filebrowserBrowseUrl:"'+el.filebrowserBrowseUrl+'",filebrowserImageBrowseUrl:"'+el.filebrowserImageBrowseUrl+'",filebrowserUploadUrl:"'+el.filebrowserUploadUrl+'",filebrowserImageUploadUrl:"'+el.filebrowserImageUploadUrl+'",ressources:'+ JSON.stringify(ress) +'})},50);</script>';
				}break;
			}

            if (el.events) {
                outerHTML += "<script language=\"javascript\" type=\"text/javascript\">";
                for(var eventName in el.events) {
                    var code = el.events[eventName];
                    var i = el.id;
                    if (typeof el.id == 'undefined') {
                        i = el.name;
                    }
                    if (eventName != 'ready') outerHTML += "$('#"+i+"').on('"+eventName+"',"+el.events[eventName]+");";
                    else outerHTML += 'var '+i+'_events = {ready: '+el.events[eventName]+'};'+i+'_events.ready();';
                }
                outerHTML += "</script>";
            }

            if (el.translations && isTranslation == false) {
				if (el.translations.length > 0) {
					if (el.type != 'hidden') {
						innerHTML += '<a class="input-group-addon" id="translationstoggle_'+el.name+'" onclick="$(\'#translations_'+el.name+'\').slideToggle(200);"><span class="glyphicon glyphicon-flag"></span></a>';
					}
				}
			}

            if (isTranslation === false && el.append) innerHTML += el.append;
			innerHTML += '</div>';

			return innerHTML+outerHTML;
		},

		renderTranslations: function(el) {
			var $this = $(this);
			var settings = $this.data('options');

			innerHTML = '';
			if (el.translations) {
				if (el.translations.length > 0) {
					if (el.type != 'hidden') {
						innerHTML += '<div class="translations" id="translations_'+el.name+'" style="display:none;">';
					}
					for(var ti=0; ti<el.translations.length; ti++) {
						innerHTML += methods.renderTranslation.apply(this,[el,el.translations[ti]]);
					}
					if (el.type != 'hidden') innerHTML += '</div>';
				}
			}
			return innerHTML;
		},

		renderTranslation: function(el,translation){
			var $this = $(this);
			var settings = $this.data('options');

			innerHTML = '';
			el.value = translation.value;
			if (translation.options) {
				el.options = translation.options;
			}
			el.name = el.name+'_'+translation.lang;
			el.lang = translation.lang;
			innerHTML += methods.renderElementInput.apply(this,[el,true]);
			return innerHTML;
		},

		escapeString: function (string) {
			return ('' + string).replace(/["'\\\n\r\u2028\u2029]/g, function (character) {
				switch (character) {
					case '"':
					case "'":
					case '\\':
						return '\\' + character
					case '\n':
						return '\\n'
					case '\r':
						return '\\r'
					case '\u2028':
						return '\\u2028'
					case '\u2029':
						return '\\u2029'
				}
			})
		}
	};

	$.fn.lucidForm = function(method) {
		if (methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.lucidForm' );
		}
	};

})(jQuery);
