<?php
/**
 * DatepickerFormField class
 *
 * Defines a datepicker field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class DatepickerFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->DatepickerFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function DatepickerFormField($name,$value='',$params=array()){
		$this->type = 'datepicker';
        
        //Format datepicker as a date only field by default (Y-m-d styl, depending on current locale)
        if (!isset($params['dateformat'])) $params['dateformat'] = SYSTEM_LOCALE_DATE;
        
        //If value is a timestamp, convert to a valid datepicker value
        if (is_numeric($value) && intval($value) == $value && isset($params['dateformat'])) {
            if ($value == 0) $value = '';
            else $value = date($params['dateformat'],$value);
        }
        
        //Convert PHP date Y-m-d format to yyyy-mm-dd datepicker format
        if (!isset($params['format']) && isset($params['dateformat'])) {
            $format = $params['dateformat'];
            $format = str_replace(
                ['j','d','D','l','n','m','y','Y'],
                ['d','dd','D','DD','m','mm','yy','yyyy'],
                $format
            );
        }
        
		$this->FormField($name,$value,$params);
	}
	
}