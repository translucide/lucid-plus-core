<?php
/**
 * Button class
 *
 * Defines a button
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class ButtonFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->ButtonFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function ButtonFormField($name,$value='',$params=array()){
		$this->type = 'button';
        if (!isset($params['class'])) $params['class'] = 'btn btn-default';
		$this->FormField($name,$value,$params);
	}
	
}