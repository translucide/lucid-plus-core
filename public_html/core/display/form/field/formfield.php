<?php
/**
 * FormField base class
 *
 * Defines base form field class
 *
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

interface FormFieldInterface {
	public function render();
	public function export();
}

class FormField extends BaseClass implements FormFieldInterface {

	/**
	 * The field type
	 *
	 * @access private
	 * @var string $type
	 */
	protected $type;

	/**
	 * The field name
	 *
	 * @access private
	 * @var string $name
	 */
	protected $name;

	/**
	 * The field value
	 *
	 * @access private
	 * @var mixed $value;
	 */
	protected $value;

	/**
	 * The field parameters
	 *
	 * @access private
	 * @var array $params
	 */
	protected $params;

	public function __construct($name,$value='',$params=array()){
		$this->FormField($name,$value,$params);
	}

	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function FormField($name,$value='',$params=array()){
		$this->name = $name;
		$this->value = $value;

		//Add default validation message if none provided...
		if (isset($params['validate'])) {
			foreach ($params['validate'] as $k => $v) {
				if (!isset($v['message'])) {
					$v['message'] = constant('_FORM_VALIDATE_'.strtoupper($v['method']));
					$params['validate'][$k] = $v;
				}
			}
		}
		$this->params = $params;
	}

	/**
	 * Imports previoulsly exported formfield data
	 *
	 * @access public
	 *
	 * @return void
	 */
	public function import($data){
		$this->type = $data['type'];
		$this->name = $data['name'];
		$this->value = $data['value'];
		$this->params = $data['params'];
	}

	/**
	 * Exports the field data for saving in DB
	 *
	 * @access public
	 *
	 * @return array Field data
	 */
	public function export(){
		$export = array();
		$export['type'] = $this->type;
		$export['name'] = $this->name;
		$export['value'] = $this->value;
		$export['params'] = $this->params;
		return $export;
	}

	public function render(){
		$array = array_merge(array('type'=>$this->type,'value'=>$this->value,'name'=>$this->name),$this->params);
		return json_encode($array);
	}

	public function renderArray(){
		return array_merge(array('type'=>$this->type,'value'=>$this->value,'name'=>$this->name),$this->params);
	}
}
?>
