<?php
/**
 * SelectthemeFormField class
 *
 * Defines a theme selector
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class SelectthemeFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->SelectthemeFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function SelectthemeFormField($name,$value='',$params=array()){
		$this->type = 'select';
		$themefolder = THEMEROOT;
		$options = array();
		$options[] = array('title'=>_CHOOSE,'value'=>'');
		if (is_dir($themefolder)) {
			if ($handle = opendir($themefolder)) {
				while (false !== ($entry = readdir($handle))) {
					$lowerentry = strtolower($entry);
					if ($lowerentry != '.' && $lowerentry != '..') {
						$options[] = array('title'=>ucfirst($entry),'value'=>$entry);
					}
				}
			}
		}
		$params['options'] = $options;
		$this->FormField($name,$value,$params);
	}
	
}