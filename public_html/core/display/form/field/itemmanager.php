<?php
/**
 * ItemmanagerFormField class
 *
 * Defines a sub-items manager form field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class ItemmanagerFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->ItemmanagerFormField($name,$value,$params);
	}

	/**
	 * ItemmanagerFormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function ItemmanagerFormField ($name,$value='',$params=array()){
		$this->type = 'itemmanager';
		$this->FormField($name,$value,$params);
	}
}