<?php
/**
 * SelectmultipleFormField class
 *
 * Defines a select
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class SelectmultipleFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->SelectmultipleFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function SelectmultipleFormField($name,$value='',$params=array()){
		$this->type = 'select';
		$params['multiple'] = 1;
		if (!is_array($value)) {
			$value = explode(',',substr($value,1,-1));
		}
		
		global $service;
		$service->get('Ressource')->get('lib/chosen/chosen-min');		
		$service->get('Ressource')->get('lib/chosen/chosen-jquery-min');
		
		$this->FormField($name,$value,$params);
	}
	
}