<?php
/**
 * YesnoFormField class
 *
 * Defines a yes-no form field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class YesnoFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->YesnoFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function YesnoFormField($name,$value='',$params=array()){
		$this->type = 'yesno';
		/*$params['options'] = array(
			array('title' => FORM_YESNO_YES,'value'=> 1),
			array('title' => FORM_YESNO_NO,'value'=> 0)
		);*/
		$this->FormField($name,$value,$params);
	}
	
}