<?php
/**
 * Gridmanager class
 *
 * Defines an html grid editor field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class LucideditorFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->LucideditorFormField($name,$value,$params);
	}

	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function LucideditorFormField ($name,$value='',$params=array()){
		global $service;
		$this->type = 'lucideditor';
		if (!isset($params['url'])) $params['url'] = URL.$service->get('Language')->getCode().'/api/system/editor/lucideditor/loaddialog';
		if (!isset($params['renderurl'])) $params['renderurl'] = URL.'{lang}/api/system/editor/lucideditor/render';
		if (!isset($params['saveurl'])) $params['saveurl'] = URL.$service->get('Language')->getCode().'/api/system/editor/lucideditor/save';
		if (!isset($params['removeurl'])) $params['removeurl'] = URL.$service->get('Language')->getCode().'/api/system/editor/lucideditor/remove';
		if (!isset($params['stylesheet'])) $params['stylesheet'] = $service->get('Ressource')->getThemeUrl().'css/gridmanager';
		$params['filebrowserBrowseUrl'] = URL.'lib/ckfinder/2.4.2/ckfinder.html';
		$params['filebrowserImageBrowseUrl'] = URL.'lib/ckfinder/2.4.2/ckfinder.html?Type=Images';
		$params['filebrowserUploadUrl'] = URL.'lib/ckfinder/2.4.2/core/connector/php/connector.php?command=QuickUpload&type=Files';
		$params['filebrowserImageUploadUrl'] = URL.'lib/ckfinder/2.4.2/core/connector/php/connector.php?command=QuickUpload&type=Images';
		//$service->get('lib/html5sortable/0.3.0/html.sortable.min');
		$this->FormField($name,$value,$params);
	}
}