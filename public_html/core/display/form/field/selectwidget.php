<?php
/**
 * SelectwidgetFormField class
 *
 * Lists available widgets for item
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class SelectwidgetFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->SelectwidgetFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function SelectwidgetFormField($name,$value='',$params=array()){
		global $service;
		$infos = $service->get('Ressource')->getInfos('widget');
        $options = array();
		foreach($infos as $k => $v) {
			$options[] = ['title' => $v['title'],'value'=> $v['name']];
		}
		$this->type = 'select';
		unset($params['name']);
		unset($params['type']);
		unset($params['component']);
		$params['options'] = $options;
		$this->FormField($name,$value,$params);
	}
	
}