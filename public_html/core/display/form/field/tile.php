<?php
/**
 * TileFormField class
 *
 * Defines an html content tile selector
 * 
 * Aug 18, 2015

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class TileFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->TileFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function TileFormField($name,$value='',$params=array()){
		$this->type = 'tile';
		$this->FormField($name,$value,$params);
	}
	
}