<?php
/**
 * PasswordFormField class
 *
 * Defines a password field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class PasswordFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->PasswordFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function PasswordFormField($name,$value='',$params=array()){
		$this->type = 'password';
		$this->FormField($name,$value,$params);

	}
	
}