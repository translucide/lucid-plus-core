<?php
/**
 * ColorFormField class
 *
 * Defines a color input
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class ColorFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->ColorFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function ColorFormField($name,$value='',$params=array()){
		$this->type = 'color';
		$this->FormField($name,$value,$params);

	}
	
}