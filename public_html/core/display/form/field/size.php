<?php
/**
 * TextFormField class
 *
 * Defines a text
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class SizeFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->SizeFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function SizeFormField($name,$value='',$params=array()){
		$this->type = 'size';
		if (!isset($params['dimensions'])) {
			$params['dimensions'][] = _WIDTH;
			$params['dimensions'][] = _HEIGHT;
		}
		if (!isset($params['size'])) $params['size'] = 2;
		$this->FormField($name,$value,$params);

	}
	
}