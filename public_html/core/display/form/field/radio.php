<?php
/**
 * RadioFormField class
 *
 * Defines a radio button
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class RadioFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->RadioFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function RadioFormField($name,$value='',$params=array()){
		$this->type = 'radio';
		$this->FormField($name,$value,$params);
	}
	
}