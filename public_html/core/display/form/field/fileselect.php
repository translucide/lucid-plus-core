<?php
/**
 * FileselectFormField class
 *
 * Defines a file select
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class FileselectFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->FileselectFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function FileselectFormField($name,$value='',$params=array()){
		$this->type = 'select';
		if (isset($params['dir'])) {
			$dir = $params['dir'];
			$ressources = array();
			if (is_dir($dir) && $handle = opendir($dir)) {
				while (false !== ($entry = readdir($handle))) {
					$lowerentry = strtolower($entry);
					if ($lowerentry != '.' && $lowerentry != '..' && strstr($lowerentry,'.backup')) {
						$ressources[] = $lowerentry;
					}
				}
			}
			unset($params['dir']);
			foreach ($ressources as $v ){
				$params['options'][] = array('title'=>$v,'value' => $v);
			}
		}
		$this->FormField($name,$value,$params);
	}
	
}