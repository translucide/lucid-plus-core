<?php
/**
 * Htmleditor class
 *
 * Defines an html editor field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class HtmleditorFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->HtmleditorFormField($name,$value,$params);
	}

	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function HtmleditorFormField($name,$value='',$params=array()){
		global $service;
		$this->type = 'htmleditor';
		if (!isset($params['provider'])) $params['provider'] ='ckeditor';
		if ($params['provider'] == 'ckeditor'){
			$service->get('Ressource')->addScript('var CKEDITOR_BASEPATH = \''.URL.'lib/ckeditor/4.5.9/\';CKFINDER_BASEPATH = \''.URL.'lib/ckfinder/2.4.2/\';',true);
			$service->get('Ressource')->get('lib/ckfinder/2.4.2/ckfinder');
			$service->get('Ressource')->get('lib/ckeditor/4.5.9/ckeditor');			
		}
		//$search = array("'","\n","\r");
		//$replace = array('\\\'',"\\n","\\r");
		//str_replace($search,$replace,$value)
		$this->FormField($name,$value,$params);
	}
}