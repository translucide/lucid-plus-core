<?php
/**
 * SelectcategoryFormField class
 *
 * Defines a select category input
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class SelectcategoryFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->SelectcategoryFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function SelectcategoryFormField($name,$value='',$params=array()){
		$this->type = 'select';
		global $service;
		/*$langStore = new LanguageStore();
		$langs = $langStore->get();
		foreach ($langs as $k => $v) {
			$params['options'][] = array('title'=>$v->getVar('language_title'), 'value'=>$v->getVar('language_id'));
		}*/
		$params['options'][] = array('title' => 'Category 1','value' => 1);
		$this->FormField($name,$value,$params);
	}
	
}