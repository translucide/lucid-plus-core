<?php
/**
 * CustomFormField class
 *
 * Defines a custom HTML field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class CustomFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->CustomFormField($name,$value,$params);
	}

	/**
	 * PlaceholderFormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function CustomFormField ($name,$value='',$params=array()){
		$this->type = 'custom';
		$this->FormField($name,$value,$params);
	}
}