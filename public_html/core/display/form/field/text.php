<?php
/**
 * TextFormField class
 *
 * Defines a text
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class TextFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->TextFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function TextFormField($name,$value='',$params=array()){
		$this->type = 'text';
		$this->FormField($name,$value,$params);

	}
	
}