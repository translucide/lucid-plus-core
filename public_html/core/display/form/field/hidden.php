<?php
/**
 * HiddenFormField class
 *
 * Defines a hidden field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class HiddenFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->HiddenFormField($name,$value,$params);
	}

	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function HiddenFormField($name,$value='',$params=array()){
		$this->type = 'hidden';
		$this->FormField($name,$value,$params);
	}
}