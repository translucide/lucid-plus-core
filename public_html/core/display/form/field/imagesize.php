<?php
/**
 * ImagesizeFormField class
 *
 * Defines an image size field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class ImagesizeFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->ImagesizeFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function ImagesizeFormField($name,$value='',$params=array()){
		$this->type = 'imagesize';
		$this->FormField($name,$value,$params);

	}
	
}