<?php
/**
 * SelectcellwidthFormField class
 *
 * Defines a select
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class SelectcellwidthFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->SelectcellwidthFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function SelectcellwidthFormField($name,$value='',$params=array()){
		global $service;
		$service->get('Ressource')->get('lib/chosen/chosen-min');		
		$service->get('Ressource')->get('lib/chosen/chosen-jquery-min');

		$this->type = 'select';
		$options = array();
		for ($i = 0; $i <= 12; $i++) {
			$options[] = array('title' => (round($i/12,2)*100).'% ('.$i.'/12)','value' => ($i == 0)?12:$i);
		}
		$params['options'] = $options;		
		$this->FormField($name,$value,$params);
	}
	
}
?>