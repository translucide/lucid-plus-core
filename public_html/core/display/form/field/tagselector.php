<?php
/**
 * TagselectorFormField class
 *
 * Defines a tag selector
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class TagselectorFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->TagselectorFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function TagselectorFormField($name,$value='',$params=array()){
		$this->type = 'tagselector';
		$this->FormField($name,$value,$params);
	}
	
}