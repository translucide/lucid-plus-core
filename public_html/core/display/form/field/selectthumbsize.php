<?php
/**
 * SelectthumbsizeFormField class
 *
 * Defines a thumbnail size selector
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

global $service;
$service->get('Ressource')->get('core/user/group');

class SelectthumbsizeFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->SelectthumbsizeFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function SelectthumbsizeFormField($name,$value='',$params=array()){
		$this->type = 'select';
		$options = array();
		$options[] = array('title'=>_CHOOSE,'value'=>'');
		$options[] = array('title'=>_TINY,'value'=>'tiny');
		$options[] = array('title'=>_SMALL,'value'=>'small');
		$options[] = array('title'=>_MEDIUM,'value'=>'medium');
		$options[] = array('title'=>_LARGE,'value'=>'large');
		$options[] = array('title'=>_ENLARGED,'value'=>'enlarged');
		$params['options'] = $options;
		$this->FormField($name,$value,$params);
	}
	
}