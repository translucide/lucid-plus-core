<?php
/**
 * CheckboxFormField class
 *
 * Defines a checkbox
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class CheckboxFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->CheckboxFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function CheckboxFormField($name,$value='',$params=array()){
		$this->type = 'checkbox';
		$this->FormField($name,$value,$params);
	}
	
}