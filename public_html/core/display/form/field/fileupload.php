<?php
/**
 * FileuploadFormField class
 *
 * Defines an image upload field
 *
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class FileuploadFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->FileuploadFormField($name,$value,$params);
	}

	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function FileuploadFormField($name,$value='',$params=array()){
		global $service;
		$this->type = 'fileupload';
		$params['uploadurl'] = URL.$service->get('Language')->getCode().'/json/fileupload';
		$params['uploadsfolder'] = UPLOADURL;
		$params['libfolder'] = URL.'lib/fineuploader';
		$params['buttonicon'] = (isset($params['buttonicon']))?$params['buttonicon']:'glyphicon glyphicon-send';
		if (!isset($params['extensions'])) {
			$params['extensions'] = explode(',',$service->get('Setting')->get('allowedfiletypes'));
		}
		$params['icontip'] = $params['icon'].' '._ALLOWEDFILETYPES.': '.implode(', ',$params['extensions']);
		$params['icon'] = 'question-sign';
		$params['url'] = URL;
		$this->FormField($name,$value,$params);
		$service->get('Ressource')->get('lib/fineuploader/jquery-fineuploader-3-3-0-min');
	}
}
