<?php
/**
 * TextareaFormField class
 *
 * Defines a textarea
 *
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class TextareaFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->TextareaFormField($name,$value,$params);
	}

	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function TextareaFormField($name,$value='',$params=array()){
		$this->type = 'textarea';
		if (trim($value) == 'null' || is_null($value)) $value = '';
		$this->FormField($name,$value,$params);
	}
}
