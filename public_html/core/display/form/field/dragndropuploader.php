<?php
/**
 * DragndropuploaderFormField class
 *
 * Defines an image upload field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class DragndropuploaderFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->DragndropuploaderFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function DragndropuploaderFormField($name,$value='',$params=array()){
		global $service;
		$this->type = 'dragndropuploader';
		$params['uploadurl'] = UPLOADURL;
		if (!isset($params['posturl'])) $params['posturl'] = URL.'/'.$service->get('Language')->getCode().'/json/imageupload';
		if (isset($params['folder'])) $params['posturl'] .= '?folder='.urlencode($params['folder']);
		if (isset($params['files'])) {
			//Reorder array keys, since js script does not like missing keys..
			$params['files'] = array_values($params['files']);
		}
		$this->FormField($name,$value,$params);
		$service->get('Ressource')->get('lib/dropzone/3.10.2/dropzone.min');
		$service->get('Ressource')->get('lib/dropzone/3.10.2/css/dropzone');
	}
}