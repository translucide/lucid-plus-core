<?php
/**
 * TreeFormField class
 *
 * Defines a tree selector
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class TreeFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->TreeFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function TreeFormField($name,$value='',$params=array()){
		$this->type = 'tree';
		$this->FormField($name,$value,$params);

	}
	
}