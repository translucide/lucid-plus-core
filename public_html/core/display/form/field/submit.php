<?php
/**
 * Submit Button class
 *
 * Defines a button
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class SubmitFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->SubmitFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function SubmitFormField($name,$value='',$params=array()){
		$this->type = 'submit';
		if (!isset($params['class'])) $params['class'] = 'btn  btn-primary';
		$this->FormField($name,$value,$params);
	}
	
}