<?php
/**
 * ImageuploadFormField class
 *
 * Defines an image upload field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class ImageuploadFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->ImageuploadFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function ImageuploadFormField($name,$value='',$params=array()){
		global $service;
		$this->type = 'imageupload';
		$params['uploadurl'] = URL.'/'.$service->get('Language')->getCode().'/json/imageupload';
		$params['uploadsfolder'] = UPLOADURL;
		$params['libfolder'] = URL.'lib/fineuploader';
		$params['url'] = URL;
		$this->FormField($name,$value,$params);
		$service->get('Ressource')->get('lib/fineuploader/jquery-fineuploader-3-3-0-min');
	}
}