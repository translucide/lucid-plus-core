<?php
/**
 * SubformFormField class
 *
 * Defines a sub form field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class SubformFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->SubformFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the Subform Form Field
	 */
	public function SubformFormField($name,$value='',$params=array()){
		$this->type = 'subform';
		if (isset($params['form']) && is_object($params['form'])){
			$params['form']->setUseCase('ajax');
			$params['form']->setVar('rendertarget',$name.' .subform_master');
			$params['form']->setVar('jsressources',0);
			$params['form']->setVar('layout','horizontal');
			
			$fd = $params['form']->getFormData();
			foreach ($fd['elements'] as $k => $v ){
				$f = $v->export();
				$f['name'] = 'subformfield_'.$f['name'];
				$v->import($f);
				$fd['elements'][$k] = $v;
			}
			
			$params['form']->setFormData($fd);
			$params['form']->add(new ButtonFormField('save','',array(
				'onclick' => 'subform_save_'.$name.'();',
				'title' => _SAVE
			)));
			$params['form'] = $params['form']->render();
		}
		$this->FormField($name,$value,$params);

	}
	
}