<?php
/**
 * SpacerFormField class
 *
 * Defines a spacer field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class SpacerFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->SpacerFormField($name,$value,$params);
	}

	/**
	 * SpacerFormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function SpacerFormField ($name,$value='',$params=array()){
		$this->type = 'spacer';
		$this->FormField($name,$value,$params);
	}
}