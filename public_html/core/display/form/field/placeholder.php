<?php
/**
 * PlaceholderFormField class
 *
 * Defines a placeholder field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class PlaceholderFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->PlaceholderFormField($name,$value,$params);
	}

	/**
	 * PlaceholderFormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function PlaceholderFormField ($name,$value='',$params=array()){
		$this->type = 'placeholder';
		$this->FormField($name,$value,$params);
	}
}