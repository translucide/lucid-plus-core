<?php
/**
 * SelectstyleFormField class
 *
 * Lists available styles for item
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class SelectstyleFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->SelectstyleFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function SelectstyleFormField($name,$value='',$params=array()){
		global $service;
		$th = $service->get('Ressource')->getThemePath();
		$styles = $service->get('Ressource')->find(
			$th.'css/'.$params['component'].'/'.$params['type'].'/'.$params['name'],
			false
		);
		$com = $service->get('Ressource')->listDir('com/'.$params['component'].'/ressource/'.$params['type'].'/'.$params['name']);
		foreach($com as $k => $v) {
			if (substr($v,-4,4) == '.css') $styles[] = $v;
		}
		$this->type = 'select';
		$options = array();
		$options[] = array('title'=>_DEFAULT,'value'=>'default');
		foreach ($styles as $k => $v) {
			$v = str_replace('.css','',$v);
			$options[] = array('title'=>ucfirst(str_replace(['_','-'],' ',$v)),'value'=>$v);
		}
		unset($params['name']);
		unset($params['type']);
		unset($params['component']);
		$params['options'] = $options;
		$this->FormField($name,$value,$params);
	}
	
}