<?php
/**
 * SelectusergroupFormField class
 *
 * Defines a user group selector
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

global $service;
$service->get('Ressource')->get('core/user/group');

class SelectusergroupFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->SelectusergroupFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function SelectusergroupFormField($name,$value='',$params=array()){
		$this->type = 'select';
		$store = new GroupStore();
		$groups = $store->get();
		$options = array();
		$options[] = array('title'=>_CHOOSE,'value'=>'');
		foreach ($groups as $v) {
			$title = $v->getVar('group_title');
			if (defined($title)) $title = constant($title);
			$options[] = array('title'=>ucfirst($title),'value'=>$v->getVar('group_id'));
		}
		$params['options'] = $options;
		$this->FormField($name,$value,$params);
	}
	
}