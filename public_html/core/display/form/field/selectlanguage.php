<?php
/**
 * SelectlanguageFormField class
 *
 * Defines a select language input
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class SelectlanguageFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->SelectlanguageFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function SelectlanguageFormField($name,$value='',$params=array()){
		$this->type = 'select';
		global $service;
		$langs = $service->get('Language')->getAllLanguages();
		foreach ($langs as $k => $v) {
			$params['options'][] = array('title'=>$v['title'], 'value'=>$v['id']);
		}
		$this->FormField($name,$value,$params);
	}
	
}