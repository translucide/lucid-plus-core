<?php
/**
 * LabelFormField class
 *
 * Defines a text label as a field
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

class LabelFormField extends FormField {
	public function __construct($name,$value='',$params=array()){
		$this->LabelFormField($name,$value,$params);
	}
	
	/**
	 * FormField constructor
	 *
	 * @access public
	 *
	 * @return void Initializes the FormField
	 */
	public function LabelFormField($name,$value='',$params=array()){
		$this->type = 'label';
		$this->FormField($name,$value,$params);

	}
	
}