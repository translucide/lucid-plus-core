<?php
/**
 * Form base class
 *
 * Defines base form class
 *
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/display/form/field/formfield');
$service->get('Ressource')->get('core/display/form/field');

interface FormInterface {

	/**
	 * Sets the form title
	 *
	 * @access public
	 * @return void setTitle()
	 */
	public function setTitle($title);

	/**
	 * Sets the name of the form
	 *
	 * @access public
	 * @return void setName()
	 */
	public function setName($name);

	/**
	 * Sets the form send method, POST or GET
	 *
	 * @access public
	 * @return void setMethod()
	 */
	public function setMethod($method);

	/**
	 * Add an element to the form
	 *
	 * @access public
	 * @return bool True on success
	 */
	public function add($element);

	/**
	 * Alias for add method
	 *
	 * @access public
	 * @return bool true on success
	 */
	public function addElement($element);

	/**
	 * Load a form from a datasource
	 *
	 * @access public
	 * @return bool True on success
	 */
	public function load($name);

	/**
	 * Saves a form to DB
	 *
	 * @access public
	 * @return bool True on success
	 */
	public function save();

	/**
	 * Renders a form to JS
	 *
	 * @access public
	 * @return string The form JS code
	 */
	public function render();

	/**
	 * Exports the form data for saving the form
	 *
	 * @access public
	 * @return array Form data
	 */
	public function export();
}

class FormData extends Data{

	public function __construct(){
		$this->type = 'form';
	}

}

class FormModel extends Model {
	function __construct(){
		$this->initVar('');
	}
}

class Form extends BaseClass implements FormInterface {

	/**
	 * A form data object which holds the form data
	 *
	 * @access private
	 *
	 * @var FormData The form data
	 */
	private $data;

	/**
	 * The form elements
	 *
	 * @var array $elements
	 */
	private $elements;

	/**
	 * Form PHP 5 constructor
	 *
	 * @method __construct
	 */
	public function __construct($action='', $name='form', $title='', $method='POST'){
		$this->Form($action, $name, $title, $method);
	}

	/**
	 * Form PHP 4 constructor
	 *
	 * @access public
	 *
	 * @param string $name Defaults to 'form'
	 * @param string $method Either 'post' or 'get'. Defaults to POST
	 * @param string $title Defaults to empty string
	 * @return void Form()
	 */
	public function Form($action='', $name='form', $title='', $method='POST'){
		$this->data['layout'] = 'horizontal';
		$this->data['method'] = 'POST';
		$this->data['class'] = '';
		$this->data['tabs'] = '';
		$this->data['tabssuffix'] = uniqid();
		$this->data['prefix'] = '';
		$this->data['prefixexceptions'] = array('id','op','name','type');

		$this->setAction($action);
		$this->setName($name);
		$this->setTitle($title);
		$this->setMethod($method);
	}

	/**
	 * Get / set an option
	 *
	 * @access public
	 *
	 * @param string $var Option name
	 * @param mixed $val Option value
	 * @return mixed Option value or void in setter context
	 */
	public function option($var,$val=null){
		if ($val == null) return $this->data[$var];
		else $this->data[$var] = $val;
	}

	/*
	 * Sets the form title
	 *
	 * @access public
	 *
	 * @param string $title
	 * @return void setTitle()
	 */
	public function setTitle($title){
		$this->setVar('title', $title);
	}

	/**
	 * Sets the form name
	 *
	 * @access public
	 *
	 * @param string $name The form name
	 * @return void setName()
	 */
	public function setName($name){
		$this->setVar('name', $name);
	}

	/**
	 * Sets the form action
	 *
	 * @access public
	 *
	 * @param string $name The form action
	 * @return void setName()
	 */
	public function setAction($action){
		$this->setVar('action', $action);
	}

	/**
	 * Sets the form method, either GET or POST
	 *
	 * @access public
	 *
	 * @param string $method Either GET or POST
	 * @return void setMethod()
	 */
	public function setMethod($method){
		$this->setVar('method', (strtoupper($method) == 'GET' || strtoupper($method) == 'POST')?$method:'POST');
	}

	/**
	 * Sets a variable: DEPRECATED
	 *
	 * @access public
	 *
	 * @return void Sets a variable in the data property.
	 */
	public function setVar($var,$val){
		//@TODO: Remove setVar and references to it.
		global $service;
		$service->get('log')->add("Form::setVar: setVar method is deprecated. Use Form::option instead.",__FILE__,__LINE__,LOG_ERR);
		$this->data[$var] = $val;
	}

	/**
	 * Gets a variable: DEPRECATED
	 *
	 * @access public
	 *
	 * @return mixed Gets a variable in the data property.
	 */
	public function getVar($var){
		//@TODO: Remove setVar and references to it.
		global $service;
		$service->get('log')->add("Form::getVar: getVar method is deprecated. Use Form::option instead.",__FILE__,__LINE__,LOG_ERR);
		return $this->data[$var];
	}

	/**
	 * Sets form data at once
	 *
	 * @access public
	 * @param array $data
	 * @return void setFormData
	 */
	public function setFormData($data) {
		if (isset($data['elements'])) {
			$this->elements = $data['elements'];
			unset($data['elements']);
		}
		$this->data = $data;
	}

	/**
	 * Setups the form for common use cases.
	 *
	 * @access public
	 *
	 * @param string $scenario Supports html ajax
	 * @return bool $success;
	 */
	public function setUseCase($scenario='html'){
		switch($scenario) {
			case 'html' : {
				return true;
				$this->setVar('formtag',1);
				$this->setVar('renderscriptonly',0);
				$this->setVar('returncode',0);
				$this->setVar('addscripttags',0);
				$this->setVar('jsressources',1);
				$this->setVar('loadonready',1);
			}break;
			case 'ajax' : {
				$this->setVar('formtag',0);
				$this->setVar('renderscriptonly',1);
				$this->setVar('returncode',1);
				$this->setVar('addscripttags',1);
				$this->setVar('jsressources',1);
				$this->setVar('layout','');
				$this->setVar('loadonready',0);
				return true;
			}break;
			case 'popupdialog' : {
                $this->setVar('formtag',0);
                $this->setVar('renderscriptonly',0);
                $this->setVar('returncode',1);
                $this->setVar('addscripttags',1);
                $this->setVar('jsressources',1);
                $this->setVar('loadonready',0);
				return true;
            }break;
		}
	}

	/**
	 * Sets the render target div
	 *
	 * @access public
	 *
	 * @param string $renderTarget
	 * @return void
	 */
	public function renderTarget($renderTarget){
		$this->setVar('rendertarget',$renderTarget);
	}

	/**
	 * Gets forms data
	 *
	 * @access public
	 * @return array getFormData()
	 */
	public function getFormData() {
		$data = $this->data;
		$data['elements'] = $this->elements;
		return $data;
	}

	/**
	 * Adds a form element to the form
	 *
	 * @access public
	 *
	 * @param FormElement $element
	 * @return bool True if element was added to the form correctly
	 */
	public function add($element){
		if (is_a($element,'FormField')) {
			$this->elements[] = $element;
		}else {
			global $service;
			$service->get('log')->add("Form::add: element is not a FormField and therefore could not be added to the form.",__FILE__,__LINE__,LOG_ERR);
			return false;
		}
		return true;
	}

	/**
	 * Alias of add method : adds an element to the form
	 *
	 * @access public
	 *
	 * @param FormElement $element
	 * @return bool True if element was added correctly
	 */
	public function addElement($element) {
		$this->add($element);
	}

	/**
	 * Sets form tabs
	 *
	 * @access public
	 *
	 * @param array $tabs
	 * @return void
	 */
	public function setTabs($tabs) {
		$this->setVar('tabs',$tabs);
	}

	/**
	 * Adds a tab to the current form
	 *
	 * @access public
	 *
	 * @param string $id
	 * @param string $title
	 * @return void
	 */
	public function addTab($id,$title){
		if (!is_array($this->data['tabs'])) {
			$this->data['tabs'] = array();
		}
		$this->data['tabs'][] = array('id'=>$id,'title'=>$title);
	}

	/**
	 * Retrieve a form from DB: not implemented yet.
	 *
	 * @access public
	 *
	 * @var int $id
	 * @return bool True on success
	 */
	function load($name) {
		global $db;
	}

	/**
	 * Save a form to DB: not implemented yet.
	 *
	 * @access public
	 *
	 * @return bool True on success
	 */
	public function save(){
		global $db;
	}

	public function addSavebutton($op,$title) {
		$this->add(new HiddenFormField('op',$op));
		$this->add(new SubmitFormField('submit','',array('title' => $title,'width'=>2)));
	}
	/**
	 * Exports this form's data
	 *
	 * @access public
	 *
	 * @return array The form data
	 */
	public function export(){
		$form = $this->data;
		foreach ($this->elements as $k => $v) {
			$form['elements'][$k] = $v->export();
		}
		return $form;
	}

	/**
	 * Adds dynamically added form fields to this form
	 *
	 * @access public
	 * @return void Modifies the current form by adding plugin fields
	 */
	public function triggerEvents(){
		global $service;
		//Get other plugins that may want to display things here.
		$this->setFormData($service->get('EventHandler')->trigger('form.on'.ucfirst($this->data['name']).'load',$this->getFormData(),true));
	}
	/**
	 * Renders the form to the appropriate output type
	 *
	 * @access public
	 *
	 * @return string The form code representing this form
	 */
	public function render(){
		global $service;
		$this->applyPrefix();
		$this->data['lang'] = $service->get('Language')->getCode();
		$rendertarget = $this->data['name']."_div";
		if (isset($this->data['rendertarget']) && $this->data['rendertarget'] != '') {
			$rendertarget = $this->data['rendertarget'];
		}
		$loadonready = (isset($this->data['loadonready']) && $this->data['loadonready'])?true:false;
		$renderscriptonly = (isset($this->data['renderscriptonly']) && $this->data['renderscriptonly'])?true:false;
		$returncode = (isset($this->data['returncode']) && $this->data['returncode'])?true:false;
		$addscripttags = (isset($this->data['addscripttags']) && $this->data['addscripttags'])?true:false;
		$jsressources = (isset($this->data['jsressources']) && $this->data['jsressources'])?true:false;
		$jscode = '';

		if ($jsressources) {
			//Render ressources scripts as well so all is in place to render the form part.
			$ressources = $service->get('Ressource')->getUrls(OUTPUT_LANGUAGE_JS);
			foreach($ressources as $k => $v){
				if ($v['type'] == 'inline') $jscode .= '<script language="javascript" type="text/javascript">';
				else $jscode .= '<script language="javascript" type="text/javascript" src="';
				$jscode .= $v['src'];
				if ($v['type'] == 'inline') $jscode .= '</script>';
				else $jscode .= '"></script>';
			}
		}

		$this->triggerEvents();
		$code = '';
		if (isset($this->data['timeout'])) {
			$code .= 'setTimeout(';
		}
		$code .= "$(\"#".$rendertarget."\").lucidForm({";
		$firstpassed = false;
		foreach ($this->data as $k => $v) {
			if($k != 'rendertarget' && $k != 'renderscriptonly' && $k != 'returncode') {
				if ($firstpassed) $code .= ",";
				if (is_array($v)) {
					$code .= "$k:".json_encode($v);
				}else {
					if ($k == 'intro') $code .= "'$k':'".addslashes(str_replace(array("\n","\r","\t"),'',$v))."'";
					else $code .= "'$k':'".addslashes($v)."'";
				}
				$firstpassed = true;
			}
		}
		$code .= ",elements : [";
		$firstpassed = false;
		if (is_array($this->elements))
		foreach ($this->elements as $k => $v) {
			if ($firstpassed) $code .= ",";
			$code .= $v->render();
			$firstpassed = true;
		}
		$code .= "]})";
		if (isset($this->data['timeout'])) {
			$code .= ','.$this->data['timeout'].');';
		}else $code .= ';';

		if ($loadonready) $code = '$(document).ready(function() {'.$code.'});';
		if ($returncode) {
			if ($addscripttags) $code = '<script language="javascript" type="text/javascript">'."\n".$code."\n</script>";
			if ($jsressources) $code = $jscode.$code;
			return $code;
		} else {
			$service->get('Ressource')->addScript("".$code."");
			return "<div id=\"".$rendertarget."\"><div class=\"warning\"><noscript>"._ACTIVATEJS."</noscript></div></div>";
		}
	}

	public function renderArray(){
		$this->applyPrefix();
		$this->triggerEvents();
		$els = array();
		foreach ($this->elements as $v) {
			$els[] = $v->renderArray();
		}
		$array = array_merge($this->data,array('elements'=>$els));
		return $array;
	}

	/**
	 * Adds prefix to form field names
	 *
	 * @access private
	 *
	 * @return void
	 */
	private function applyPrefix() {
		if ($this->data['prefix'] != '') {
			foreach ($this->elements as $k => $v) {
				$e = $v->export();
				if (in_array($e['name'],$this->option('prefixexceptions')) === false) {
					$e['name'] = $this->data['prefix'].'_'.$e['name'];
				}
				$this->elements[$k]->import($e);
			}
		}
	}

	/**
	 * Returns translations for any formfield based
	 * on a collection of objects of the same ObjectID
	 *
	 * This method finds the default language and returns an
	 * array in the form : []['lang' => 'fr', 'value' => 'fieldvalue...']
	 * where 'fr' is a secondary language.
	 *
	 * @access public
	 *
	 * @param Array@Data $obj The obj with translations
	 * @param string $fieldname The field name for which we generate translations
	 * @param string $key The optionnal field key(s) if field value is an array, use array type for multiple fields to be joined in an array
	 * @param string or array $defaultvalue The default field value, only if not empty string
	 * @return array Translations array to use in form fields
	 */
	public function getTranslations($obj,$fieldname,$key='',$defaultvalue='') {
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$langs = $service->get('Language')->getCodes();
		$tr = array();
		foreach ($langs as $k=> $v) {
			$value = $obj[0]->getVar($fieldname);

			//Check if we should apply default value
			if ($defaultvalue != '' && ($value == '' || $value == array())) {
				if (is_array($defaultvalue) && isset($defaultvalue[$v])) $value = $defaultvalue[$v];
				else $value = $defaultvalue;
			}

			if ($key != '' && is_array($value)) {
				if (is_array($key)){
					foreach ($key as $kk => $vv) $value[$kk] = $vv;
				}
				else $value = $value[$key];
			}
			if ($fieldname == $obj[0]->getType().'_id') $value = 0; // Default to non-created object
			foreach ($obj as $ko => $vo) {
				if ($k == $vo->getVar($vo->getType().'_language') && $vo->getVar($vo->getType().'_language') != $defaultlang['id'] && $vo->getVar($vo->getType().'_language') != 0) {
					$value = $vo->getVar($fieldname);
					if ($key != '' && is_array($value)) {
						if (is_array($key)){
							foreach ($key as $kk => $vv) $value[$kk] = $vv;
						}
						else $value = $value[$key];
					}
				}
			}
			if ($k != $defaultlang['id']) $tr[] = array('lang' => $v,'value' => $value);
		}
		return $tr;
	}

	/**
	 * Returns an array of values for each language from a field, and optional key.
	 *
	 * @access public
	 * @params Array@Data $obj The objects to extract data from
	 * @param string $field
	 * @param mixed $key String or array
	 * @return Array $ret
	 */
	public function getByLang($obj,$field,$key = ''){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$langs = $service->get('Language')->getCodes();
		$ret = array();
		foreach ($obj as $k => $v) {
			$val = $v->getVar($field);
			if ($key != '') $val = $val[$key];
			$ret[$langs[$v->getVar($v->getType().'_language')]] = $val;
		}
		return $ret;
	}
}
?>
