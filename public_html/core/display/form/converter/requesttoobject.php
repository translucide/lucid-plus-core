<?php
/**
 * Request To Object base class
 *
 * Defines a method for converting request parameters back to data objects
 *
 * Oct 30, 2012
 *
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class RequestToObject{

	/**
	 * Data holder variable
	 *
	 * @access private;
	 * @var array $data;
	 */
	private $data;

	/**
	 * Constructor
	 *
	 * @access public
	 * @param string $objtype
	 * @return void __construct()
	 */
	public function __construct($objtype=''){
		$this->RequestToObject($objtype);
	}

	public function RequestToObject($objtype='') {
		if ($objtype != '') $this->data['type'] = $objtype;
		$this->load();
	}

	/**
	 *	setModel
	 *
	 *	Sets the model to use to generate form
	 *
	 *	@access public
	 *	@param Model $model
	 *	@return void setModel
	 */
	public function setModel($model) {
		if (is_a($model,'Model')) {
			$this->data['model'] = $model;
		}
	}

	/**
	 *	setStore
	 *
	 *	Sets the store to use to generate form
	 *
	 *	@access public
	 *	@param Store $store
	 *	@return void setStore
	 */
	public function setStore($store) {
		if (is_a($store,'Store')) {
			$this->data['store'] = $store;
		}
	}

	/**
	 *	setRequest
	 *
	 *	Sets the request to operate with. Allows to tweak request
	 *	parameters before converting them to objects.
	 *
	 *	@access public
	 *	@param array $request
	 *	@return void setRequest
	 */
	public function setRequest($request) {
		if (is_array($request)) {
			$this->data['request'] = $request;
		}
	}

	/**
	 * load()
	 *
	 * Loads the needed objects if any $objid specified in constructor
	 *
	 * @access public
	 * @return void load()
	 */
	public function load(){
		global $service;
		if (isset($this->data['type'])) {
			$service->get('Ressource')->get('com/model/'.$this->data['type']);
			if (class_exists(ucfirst($this->data['type']).'Model')) {
				$cls = ucfirst($this->data['type']).'Model';
				$this->data['model'] = new $cls();
			}
			if (class_exists(ucfirst($this->data['type']).'Store')) {
				$cls = ucfirst($this->data['type']).'Store';
				$obj = new $cls();
				$obj->setOption('ignorelangs',true);
				$this->data['store'] = $obj;
			}
			$this->loadObjects();
		}
	}

	/**
	 * Checks if we are converting many objects or just one (optionnaly multilanguage) object
	 *
	 * @access public
	 * @return void isMultiObject()
	 */
	public function isMultiObject(){
		global $service;
		if (isset($this->data['request']) && is_array($this->data['request'])) $request = $this->data['request'];
		else $request = $service->get('Request')->get();
		if (isset($request['mo_0_'.$this->data['store']->getIdField()])) {
			return true;
		}
		if (isset($request[$this->data['store']->getIdField()])) {
			return false;
		}
	}

	/**
	 * Load targeted objects
	 *
	 * @access public
	 * @return void loadObjects()
	 */
	public function loadObjects(){
		global $service;
		if (!isset($this->data['store']) || !isset($this->data['model'])) {
			$this->data['objects'] = array();
			return false;
		}
		if (isset($this->data['request']) && is_array($this->data['request'])) $request = $this->data['request'];
		else $request = $service->get('Request')->get();

		$multi = $this->isMultiObject();
		$langs = $this->getLanguages();
		$toLoad = array();
		$objects = array();
		$formattedData = $this->data['objects'] = array();
		$i = 0;
		if ($multi) $prefix = 'mo_'.$i.'_';
		else $prefix = '';
		$modeldata = $this->data['model']->getVars();//export();

		//1. Load all existing objects into $formattedData first.
		while(isset($request[$prefix.$this->data['store']->getIdField()])) {
			if (isset($request[$prefix.$this->data['store']->getIdField()])) {
				if ($request[$prefix.$this->data['store']->getIdField()]) {
					$toLoad[] = $request[$prefix.$this->data['store']->getIdField()];
				}
				foreach($langs as $kl => $vl) {
					if (isset($request[$prefix.$this->data['store']->getIdField().'_'.$service->get('Language')->getCodeForId($vl)]) &&
						$request[$prefix.$this->data['store']->getIdField().'_'.$service->get('Language')->getCodeForId($vl)]) {
						$toLoad[] = $request[$prefix.$this->data['store']->getIdField().'_'.$service->get('Language')->getCodeForId($vl)];
					}
				}
			}
			$i++;
			$prefix = 'mo_'.$i.'_';
		}
		$objects = $this->data['store']->getById($toLoad);
		foreach ($objects as $k => $v) {
			$formattedData[$v->getVar($this->data['store']->getIdField())] = $v->get();
		}
		$i = 0;
		if ($multi) $prefix = 'mo_'.$i.'_';
		else $prefix = '';
		//2. Overwrite existing objects data  with the one defined in the request...
		while(isset($request[$prefix.$this->data['store']->getIdField()])) {
			//3. Fill formatted data with the master data found...
			if (isset($request[$prefix.$this->data['store']->getIdField()])) {
				if ($request[$prefix.$this->data['store']->getIdField()] == '0') {
					//4. Create master object if it does not exists yet...
					$obj = $this->create();
					$formattedData[$obj->getVar($this->data['store']->getIdField())] = $obj->getVars();
					$request[$prefix.$this->data['store']->getIdField()] = $obj->getVar($this->data['store']->getIdField());
					$request[$prefix.$this->data['store']->getObjIdField()] = $obj->getVar($this->data['store']->getObjIdField());
				}
				//5. Add request data to the object, overwriting existing data if some was loaded before.
				foreach($modeldata as $k => $v) {
					if (isset($request[$prefix.$v])) {
						$formattedData[$request[$prefix.$this->data['store']->getIdField()]][$v] = $request[$prefix.$v];
					}
				}
				//6. Gather translation obj data and overwrite master data with it in a new obj.
				$masterdata = $formattedData[$request[$prefix.$this->data['store']->getIdField()]];
				foreach($langs as $kl => $vl) {
					$clc = $service->get('Language')->getCodeForId($vl);
					if (isset($request[$prefix.$this->data['store']->getIdField().'_'.$clc])) {
						$trid = $request[$prefix.$this->data['store']->getIdField().'_'.$clc];
						//We have a translation... gather all obj fields values
						if ($trid == '0') {
							//Create translation object if id=0
							$obj = $this->create();
							$trid = $obj->getVar($this->data['store']->getIdField());
							$formattedData[$trid][$this->data['store']->getIdField()] = $trid;
							$request[$prefix.$this->data['store']->getIdField().'_'.$clc] = $trid;
						}
						foreach($modeldata as $k => $v) {
							if (!isset($request[$prefix.$v.'_'.$clc])) {
								//Field is not multilingual, take the master obj value.
								$formattedData[$trid][$v] = $masterdata[$v];
							}else {
								$formattedData[$trid][$v] = $request[$prefix.$v.'_'.$clc];
							}
						}
						//Adjust special field names based on the data we have.
						if ($this->data['model']->isVar($this->data['type'].'_language')) {
							$formattedData[$trid][$this->data['type'].'_language'] = $vl;
						}
						if ($this->data['model']->isVar($this->data['type'].'_objid')) {
							$formattedData[$trid][$this->data['type'].'_objid'] = $masterdata[$this->data['type'].'_objid'];
						}
					}
				}
			}
			$i++;
			//Will exit the while loop if in single object mode but continue it in multiobject mode.
			$prefix = 'mo_'.$i.'_';
		}

		//Convert array back to objects
		$cls = ucfirst($this->data['type']).'Data';
		foreach($formattedData as $k => $v) {
			$obj = new $cls($this->data['type']);
			$obj->set($v);
			$this->data['objects'][$k] = $obj;
		}
	}

	/**
	 * Returns the site languages
	 *
	 * @access public
	 * @return array $languages
	 */
	public function getLanguages(){
		global $service;
		$langs = $service->get('Setting')->get('sitelanguages');
		return explode(',',substr($langs,1,-1));
	}

	/**
	 * Creates an object and returns it
	 *
	 * @access public
	 * @return Data $obj
	 */
	public function create(){
		$obj = $this->data['store']->create();
		return $obj;
	}

	/**
	 * Returns the objects to save
	 *
	 * @access public
	 * @return array@Data $objects
	 */
	public function get(){
		return $this->data['objects'];
	}

	/**
	 * Save to DB the objects to save
	 *
	 * @access public
	 * @return bool $success
	 */
	public function save(){
		return $this->data['store']->save($this->data['objects']);
	}


	/**
	 * Used to save the object options column after an edit request
	 *
	 * @access public
	 * @param array $optionslist The options field names to save.
	 * @param string $field The array field name in which you save options
	 * @param string $subfield The subfield in which to save options in. Leave blank for none
	 * @return bool Success or failure
	 */
	public function addOptions($optionslist,$field = '',$subfield='') {
		global $service;
		if (isset($this->data['request']) && is_array($this->data['request'])) $request = $this->data['request'];
		else $request = $service->get('Request')->get();
		$languages = $service->get('Language')->getCodes();
		$defaultlang = $service->get('Language')->getDefault();
		$options = array();
		if ($field == '') $field = $this->data['type'].'_options';

		foreach($languages as $kl => $vl) {
			foreach ($optionslist as $k=>$v) {
				if (isset($request[$v.'_'.$vl]) || isset($request[$v])) {
					$val = (isset($request[$v.'_'.$vl]))?$request[$v.'_'.$vl]:$request[$v];
					$options[$kl][$v] = $val;
				}
			}
		}
		$objs = $this->data['objects'];
		foreach ($objs as $k => $v) {
			$opt = array();
			$curopt = (array)$v->getVar($field);
			$key = $defaultlang['id'];
			if ($this->data['model']->isVar($this->data['type'].'_language')) {
				$key = $v->getVar($this->data['type'].'_language');
			}
			$newopt = (array)$options[$key];
			if ($subfield != '') {
				$curopt[$subfield] = array_merge((array)$curopt[$subfield],$newopt);
				$opt = $curopt;
			}
			else $opt = array_merge($curopt,$newopt);
			$objs[$k]->setVar($field,$opt);
		}
		$this->data['objects'] = $objs;
		return true;
	}
}
?>
