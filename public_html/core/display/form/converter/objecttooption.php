<?php
/**
 * Object to option converter base class
 *
 * Defines a method for converting one or more objects
 * to select field options list
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */
class ObjectToOption{
	public function __construct($objs,$valuefied,$titlefield) {
		return $this->ObjectToOption($objs,$valuefied,$titlefield);
	}
	public function ObjectToOption($objs,$valuefied,$titlefield){
		return $this->convert($objs,$valuefield,$titlefield);
	}
	
	public function convert($objs,$valuefied,$titlefield){
		if (!is_array($objs) && is_object($objs)) {
			$objs = array($objs);
		}
		$result = array();
		foreach($objs as $k => $v) {
			$result[] = array(
				'value' => $v->getVar($valuefield),
				'title' => $v->getVar($titlefield)
			);
		}
		return $result;
	}
}