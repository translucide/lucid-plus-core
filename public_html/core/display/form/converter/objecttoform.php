<?php
/**
 * Object to form converter base class
 *
 * Defines a method for converting one or more objects
 * to a multiobject and multilanguage form
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */
class ObjectToForm {

	/**
	 * Data holder variable
	 *
	 * @access private;
	 * @var array $data;
	 */
	private $data;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @param string $objtype
	 * @param string $objid
	 * @return void __construct()
	 */
	public function __construct($objects,$multilingual=false){
		$this->ObjectToForm($objects,$multilingual);
	}
	
	public function ObjectToForm($objects,$multilingual=false) {
		$this->isMultilingual($multilingual);
		$this->setObjects($objects);
	}
	
	/**
	 * Get or set the multilingual option
	 *
	 * @access public
	 * @return bool $multilingual
	 */
	public function isMultilingual($multilingual = null) {
		if ($multilingual == null) return $this->getOption('multilingual');
		else $this->setOption('multilingual',$multilingual);
	}
	
	/**
	 * Sets the objects to form-ulize
	 *
	 * It will group all given sub-objects into a
	 * common array structure to be parsed later. The array structure
	 * will keep the objects order given and so the form will too.
	 * Sub objects will be placed in a ['childs'] under its parent object array.
	 *
	 * @access public
	 * @return void setObjects
	 */
	public function setObjects($objects){
		global $service;
		$deflang = $service->get('Setting')->get('defaultlanguage');
		
		if (is_object($objects)) {
			$objects = array($objects);
		}
		if (is_array($objects)) {
			//Buil array structure
			foreach ($objects as $k=> $v) {
				$model = ucfirst($v->getType()).'Model';
				$model = new $model();
				if (isset($objects[$k])) {
					$thisObj = array();
					if (!$this->isMultilingual() || $this->isMaster($v)) {
						//Master object found
						$thisObj['master'] = $v;
						if ($this->isMultilingual()) {
							$thisObj['lang'] = $deflang;
							if ($model->isVar($model->getType().'_objid')) {
								$thisObj['childs'] = $this->getChilds($v,$objects);
								$objects = $this->removeChilds($thisObj['childs'],$objects);
							}
						}
					}else {
						//Child object found first in a multilingual context.
						//Retain position and find master obj
						$childs = $this->getChilds($v,$objects);
						$childs[$v->getVar($v->getType().'_language')] = $v;
						foreach ($childs as $k2 => $v2) {
							if ($this->isMaster($v2)) {
								$thisObj['master'] = $v2;
								$thisObj['lang'] = $v2->getvar($v->getType().'_language');
								unset($childs[$k2]);
							}
						}
						if (!isset($thisObj['master'])) {
							$thisObj['master'] = null;
							$thisObj['lang'] = $deflang;
						}
					}
					$this->data['objects'][] = $thisObj;
				}
			}
		}
		else {
			//$objects must be an arrray message ?
		}
	}
	
	public function getForm($form=null){
		global $service;
		
		if ($form == null) $form = new Form();
		$i = 0;
		$langs = $this->getLanguages();
		foreach ($this->data['objects'] as $k => $v) {
			$model = ucfirst($v['master']->getType()).'Model';
			$store = ucfirst($v['master']->getType()).'Store';
			$model = new $model();
			$store = new $store();
			$vars = array();
			if (is_object($v['master'])) $vars = $v['master']->getVars();
			else {
				//Request a new object of the kind.
				$type = '';
				foreach ($v['childs'] as $kt => $vt) {
					$type = $vt->getType();
				}
				$v['master'] = $store->getNew();
				$vars = $v['master']->getVars();
			}
			$modeldata = $model->getVars(true);
			$prefix = 'mo_'.$i.'_';
			foreach($modeldata as $km => $vm) {
				if (isset($vm['options']['formtype'])) {
					$fieldname = $prefix.$km;
					$fieldValue = $vars[$km];
					$fieldCls = ucfirst($vm['options']['formtype']).'FormField';
					$params = array();
					if (isset($vm['options']['options'])) $params['options'] = $vm['options']['options'];
					if (isset($vm['options']['onclick'])) $params['onclick'] = $vm['options']['onclick'];
					if (isset($vm['options']['onchange'])) $params['onchange'] = $vm['options']['onchange'];
					if (isset($vm['options']['onfocus'])) $params['onfocus'] = $vm['options']['onfocus'];
					if (isset($vm['options']['title'])) {
						$params['title'] = (defined($vm['options']['title']))?constant($vm['options']['title']):$vm['options']['title'];	
					}
					if ($this->isMultilingual() && isset($v['childs']) && $vm['options']['multilingual']) {
						$params['lang'] = $service->get('Language')->getCodeForId($v['lang']);
						foreach ($langs as $kc => $vc) {
							if ($vc != $v['lang']) {
								if (isset($v['childs'][$vc])) {
									$params['translations'][$vc] = array(
										'value' => $v['childs'][$vc]->getVar($km),
										'lang' => $service->get('Language')->getCodeForId($vc),
									);
								}else {
									$new = $store->getNew();
									$params['translations'][$vc] = array(
										'value' => $new->getVar($km),
										'lang' => $service->get('Language')->getCodeForId($vc),
									);
								}
							}
						}
					}
					$form->add(new $fieldCls($fieldname, $fieldValue, $params));
				}
			}
			$i++;
		}
		return $form;
	}

	private function getChilds($obj,$objects){
		$model = ucfirst($obj->getType()).'Model';
		$model = new $model();
		$childs = array();
		foreach ($objects as $k => $v) {
			if ($obj->getType() == $v->getType()) {
				if ($model->isVar($v->getType().'_objid') && $model->isVar($v->getType().'_language') &&
					$v->getVar($v->getType().'_objid') == $obj->getVar($v->getType().'_objid') &&
					$v->getVar($v->getType().'_language') != $obj->getVar($v->getType().'_language')) {
					//Child obj found.
					$childs[$v->getVar($v->getType().'_language')] = $v;
				}
			}
		}
		return $childs;
	}
	
	private function removeChilds($toremove,$collection) {
		foreach ($collection as $k => $v) {
			if (in_array($v,$toremove)) unset($collection[$k]);
		}
		return $collection;
	}
	
	private function isMaster($obj) {
		global $service;
		$deflang = $service->get('Setting')->get('defaultlanguage');
		$model = ucfirst($obj->getType()).'Model';
		$model = new $model();
		if (($model->isVar($model->getType().'_language') && $obj->getVar($model->getType().'_language') == $deflang) ||
			$model->isVar($model->getType().'_language')){
			return true;
		}return false;
		
	}
	
	/**
	 * Returns the site languages
	 *
	 * @access public
	 * @return array $languages
	 */
	private function getLanguages(){
		global $service;
		$langs = $service->get('Setting')->get('sitelanguages');
		return explode(',',substr($langs,1,-1));;
	}

	/**
	 * Sets an option
	 *
	 * @access private
	 * @return void setOption()
	 */	
	private function setOption($name,$value){
		$this->data['options'][strtolower($name)] = $value;
	}

	/**
	 * Returns an option value
	 *
	 * @access private
	 * @return void setOption()
	 */		
	private function getOption($name) {
		if(isset($this->data['options'][strtolower($name)])) {
			return $this->data['options'][strtolower($name)];
		}
	}
}
?>