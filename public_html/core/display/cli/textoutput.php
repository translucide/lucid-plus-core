<?php
/**
 * TextOutput class
 *
 * Manages text output formatting, usually for CLI mode
 *
 * May 25, 2014

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
class TextOutput {
   
   public $n;
   public $h;
   public $l;
   public $i;
   public $t;
   
   public $width;
   public $tabwith;
   
   public function __construct(){
      $this->TextOutput();
   }
   public function TextOutput(){
      $this->setFormat();
      $this->tabwidth = 8; //@TODO: Find system real tab width
   }
   
   public function setFormat($w=80,$h="#",$l="-",$i=" ",$t="\t",$n="\r\n") {
      $this->width = $w;
      $this->n = $n;
      $this->h = str_repeat($h,80); //@TODO Get terminal width automatically ?
      $this->l = str_repeat($l,80);
		$this->i = str_repeat($i,3);
		$this->t = $t;
   }
   
   /**
    * Wraps texts to prevent line overflow
    *
    * @access public
    * @param String $text The text to wrap
    * @param Integer $indent The number of indentation caracters to add at line beginning
    * @return String The wraped text
    */
   public function wrap($text,$indent=0){
      $text = str_replace("\t",str_repeat("_",$this->tabwidth),$text);
      $indent = intval($indent);
      $text = explode(' ',$text);
      $r = array(str_repeat($this->i,$indent));
      $i = 0;
      
      //Preprocess all text words to locate words longer than lines.
      //@TODO: Make preprocessing of longer words than line work
      /*$max = ($this->width-($indent*$this->tabwidth+3));
      foreach($text as $k => $v) {
         if (strlen($v) > $max){
            $c = 0;
            $text[$k] = '';
            while(strlen($v) > $c*$max) {
               $text[$k] .= substr($v,$c*$max,$max).' ';
               $c++;
            }
         }
      }
      $text = explode(' ',implode(' ',$text));
      */
      foreach ($text as $k => $v) {
         if (strlen($r[$i]) + strlen($v) + 1 >= $this->width) {
            $i++;
            $r[$i] = str_repeat($this->i,$indent+1);
         }
         if (strlen($r[$i]) + strlen($v) + 1 < $this->width) {
            $r[$i] .= $v.' ';
         }
      }
      $text = implode($this->n,$r);
      $text = str_replace(str_repeat("_",$this->tabwidth),"\t",$text);
      return $text;
   }

   /**
    * Returns an header
    *
    * @access public
    *
    * @param String $text The header text.
    * @return String The header formatted string
    */
   public function header($text) {
		return $this->n.$this->h.$this->n.$this->wrap($text).$this->n.$this->h.$this->n;
   }
   public function h($text) {
      return $this->header($text);
   }   

   /**
    * Returns a Title
    *
    * @access public
    *
    * @param String $text The text of the title section
    * @return String The title formatted text.
    */
   public function title($text) {
		return $this->n.$this->l.$this->n.$this->wrap(strtoupper($text)).$this->n.$this->l.$this->n;
   }
   public function t($text) {
      return $this->title($text);
   }

   /**
    * Returns a Sub Title
    *
    * @access public
    *
    * @param String $text The text of the Subtitle section
    * @return String The sub title formatted text.
    */
   public function subtitle($text) {
		return $this->n.$this->n.$this->wrap(strtoupper($text)).$this->n;
   }
   public function s($text) {
      return $this->subtitle($text);
   }
   
   /**
    * Returns a paragraph properly formatted for CLI output
    *
    * @access public
    *
    * @param String $text The paragraph text to output
    * @return String The paragraph formatted text
    */
   public function paragraph($text) {
		return $this->n.$this->wrap($text).$this->n;
   }
   public function p($text) {
      return $this->paragraph($text);
   }   
   
   /**
    * Returns a line properly formatted for CLI output
    *
    * @access public
    *
    * @param String $text The text line to output
    * @return String The line formatted text
    */
   public function line($text,$indent=0) {
		return $this->wrap($text,$indent).$this->n;
   }
   public function l($text,$indent=0) {
      return $this->line($text,$indent=0);
   }   
   
   /**
    * Returns an option line
    *
    * @access public
    *
    * @param String $option The option text
    * @param String $text The option descriptive text
    * @param String $otherlines The option description text in multi-line format.
    * @return String The formatted option
    */
   public function option($option, $text, $otherlines="", $indent=1){
      $nbtabs = ceil(((4*$this->tabwidth)-strlen(str_repeat($this->i,$indent).$option))/$this->tabwidth);
      if ($otherlines != "") {
         $otherlines = $this->n.$this->wrap($otherlines,2);
         //$otherlines = $this->n.$this->wrap($otherlines,ceil((strlen($option)+$nbtabs*$this->tabwidth)/strlen($this->i)));
      }
		return
         $this->n.$this->wrap($option.str_repeat($this->t,$nbtabs).$text,$indent).
         $otherlines;
   }
   public function o($option,$text, $otherlines="", $indent=1) {
      return $this->option($option, $text, $otherlines, $indent);
   }   
}