<?php
/**
 * Theme class
 *
 * Defines common points to all themes.
 *
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('Ressource')->get('core/display/template');
$service->get('Ressource')->get('core/constants');

interface ThemeInterface{
	/**
	 * Setups the theme
	 *
	 * @public
	 *
	 * @return string render()
	 */
	public function setup();

	/**
	 * Renders the theme to appropriate output mode
	 *
	 * @public
	 *
	 * @return string render()
	 */
	public function render();

	/**
	 * Sets widgets to use in the output
	 *
	 * @public
	 *
	 * @param array $widgets An array of widgets objects
	 * @return bool True on success
	 */
	public function setWidgets($widget);

	/**
	 * Adds a widget
	 *
	 * @public
	 *
	 * @param Widget $widget
	 * @return bool True on success
	 */
	public function addWidget($widget);

	/**
	 * Sets the page title
	 *
	 * @public
	 *
	 * @param string $title
	 * @return void setTitle()
	 */
	public function setTitle($title);

	/**
	 * Sets the page description
	 *
	 * @public
	 *
	 * @param string $description
	 * @return void setDescription()
	 */
	public function setDescription($description);

	/**
	 * Sets the page keywords
	 *
	 * @public
	 *
	 * @param mixed $keywords String (coma separated) or array containing keywords.
	 * @return void setKeywords()
	 */
	public function setKeywords($keywords);

	/**
	 * Sets page meta tags
	 *
	 * @public
	 *
	 * @param array $metas In tihs format : array[] = array( name => '', value=> '');
	 * @return void setMetas()
	 */
	public function setMetas($metas);

	/**
	 * Adds a meta tag
	 *
	 * @param string $metaname
	 * @param string $metavalue
	 * @return void addMeta()
	 */
	public function addMeta($meta);

	/**
	 * Sets the template to use for this theme
	 *
	 * @public
	 *
	 * @return void setTemplate();
	 */
	public function setTemplate($file);

	/**
	 * Sets the current layout
	 *
	 * @public
	 *
	 * @param array Layout
	 * @return void setLayout()
	 */
	public function setLayout($layout);

	/**
	 * Returns information about the current theme
	 *
	 * @public
	 * @return array Information about the current theme
	 */
	public function getInfo();
}

class Theme implements ThemeInterface {

	/**
	 * Theme options
	 *
	 * @protected;
	 *
	 * @var array $options
	 */
	protected $options;

	/**
	 * Theme content widgets
	 *
	 * @protected;
	 *
	 * @var array $widgets
	 */
	protected $widgets;

	/**
	 * Theme content
	 *
	 * @protected;
	 *
	 * @var array $content
	 */
	protected $content;

	/**
	 * Theme main template location
	 *
	 * @protected
	 *
	 * @var $template
	 */
	protected $template;


	/**
	 * Theme main email template location
	 *
	 * @protected
	 *
	 * @var $template
	 */
	protected $emailtemplate;

	/**
	 * The theme layout in use
	 *
	 * @private
	 *
	 * @var $layout
	 */
	protected $layout;

	/**
	 * Theme constructor
	 *
	 * @public
	 *
	 * @return void Theme()
	 */
	public function __construct(){
        $this->setup();
	}

    /**
     * Basic theme setup :
     *
     * Load template class, modernizr, jquery, bootstrap, lucidcore
     *
     * @access public
     **/
    public function setup(){
		global $service;
		$service->get('Ressource')->get('core/display/template');
		$service->get('Ressource')->get('lib/modernizr/2.6.2/modernizr',RESSOURCE_FRAMEWORK);

		//Prefer local version in development mode: allows working without internet connection.
		if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' || strpos($_SERVER['REMOTE_ADDR'],'192.168.') !== false) {
			$service->get('Ressource')->get('lib/jquery/2.1.4/jquery-2.1.4.min.js',RESSOURCE_FRAMEWORK);
			$service->get('Ressource')->get('lib/bootstrap/3.3.5/css/bootstrap.min.css',RESSOURCE_FRAMEWORK);
			$service->get('Ressource')->get('lib/bootstrap/3.3.5/css/bootstrap-theme.min.css',RESSOURCE_FRAMEWORK);
			$service->get('Ressource')->get('lib/bootstrap/3.3.5/js/bootstrap.min.js',RESSOURCE_FRAMEWORK);
		}else {
			$service->get('Ressource')->get('//code.jquery.com/jquery-2.1.4.min.js',RESSOURCE_FRAMEWORK);
			$service->get('Ressource')->get('//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css',RESSOURCE_FRAMEWORK);
			$service->get('Ressource')->get('//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css',RESSOURCE_FRAMEWORK);
			$service->get('Ressource')->get('//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js',RESSOURCE_FRAMEWORK);
		}
		//Forward system constants to JS...
		$service->get('Ressource')->addScript('var LucidConfig = {
			\'connected\': \''.$service->get('User')->isLogged().'\',
			\'logoutUrl\': \''.URL.$service->get('Language')->getCode().'/user/logout'.'\',
			\'userID\': \''.$service->get('User')->getUid().'\',
			\'lang\': \''.$service->get('Language')->getCode().'\',
			\'url\': \''.URL.'\',
			\'uploadUrl\': \''.UPLOADURL.'\',
			\'themeUrl\': \''.THEMEURL.'\'
		};');
        $service->get('Ressource')->get('lib/lucidcore/0.1/lucidcore',RESSOURCE_PLUGIN);
		$this->options = array();
    }

	/**
	 * Renders the themed emails
	 *
	 * @public
	 *
	 * @return string render()
	 */
	public function renderEmail(){
		global $service;

		if (!is_object($this->emailtemplate)) {
			$log =& $service->get('log');
			$log->add('Theme::render ('.get_called_class().') Template to render has not been defined. Use $this->setTemplate(new Template(\'absolute_path_to_template_file\',\'smarty\')); in the current theme constructor.',__FILE__,__LINE__,E_ERROR);
			return;
		}
		$this->assignVars();

		//Render template
		return $this->template->render();
	}

	/**
	 * Renders the theme to appropriate output mode
	 *
	 * @public
	 *
	 * @return string render()
	 */
	public function render(){
		global $service;

		if (!is_object($this->template)) {
			$log =& $service->get('log');
			$log->add('Theme::render ('.get_called_class().') Template to render has not been defined. Use $this->setTemplate(new Template(\'absolute_path_to_template_file\',\'smarty\')); in the current theme constructor.',__FILE__,__LINE__,E_ERROR);
			return;
		}
		$this->assignVars();

		//Render template
		return $this->template->render();
	}

	/**
	 * Assigns variables to the template
	 *
	 * @public
	 *
	 * @return void Assigns the theme variables to the template.
	 */
	protected function assignVars(){
		global $service;
		$title = $service->get('Setting')->get('sitename');
		if ($service->get('Setting')->get('siteslogan') != '') $title .= ' - '.$service->get('Setting')->get('siteslogan');
		if (!isset($this->options['title'])) $this->setTitle($title);
		if (!isset($this->options['description'])) $this->setDescription($title);
		if (!isset($this->options['keywords'])) $this->setKeywords($service->get('Setting')->get('sitekeywords'));

		if (!isset($this->options['metas']) || !is_array($this->options['metas'])) {
			$this->options['metas'] = array();
		}
		if (!isset($this->options['objid'])) $this->options['objid'] = 0;

		$this->assignWidgets();

		if (is_array($this->options)) {
			foreach($this->options as $k => $v) {
				$this->template->assign($k,$v);
			}
			unset($k,$v);
		}

		$this->template->assign("hreflang",$this->getLanguageAnnotations());

		//Set layouts
		$this->template->assign('layout',$this->layout);
		$this->template->assign('siteclosedmsg',$service->get('Setting')->get('siteclosedmsg'));

		//Assign widgets
		$this->template->assign('content',$this->content);
		$this->template->assign('js', $service->get('Ressource')->getUrls(OUTPUT_MODE_JS));
		$this->template->assign('css', $service->get('Ressource')->getUrls(OUTPUT_MODE_CSS));
	}

	public function assignWidgets() {
		$controller = new WidgetController();
		$unassigned = $this->widgets;
		$widgets = array();
		$tags = array();
		$info = $this->getInfo();
		$zones = $info['zones'];
		$zones['theme'] = [
			'name' => 'theme',
			'title' => 'Theme',
			'orientation' => 'v',
			'allow' => '*',
			'restrict' => ''
		];
		foreach($zones as $zk => $zv) {
			foreach($unassigned as $k => $v) {
				$data = $v->getData();
				$type = $v->getInfo('type');
				$allow = explode(',',$zv['allow']);
				$restrict = explode(',',$zv['restrict']);
				if ($zv['name'] == $data->getVar('widget_zone') &&
					(in_array('*',$allow) || in_array($type,$allow)) &&
					($zv['restrict'] == '' || !in_array($type,$restrict))){
					$render = $controller->render($v);
					$widgets[$zv['name']][] = $render;
					if ($data->getVar('widget_tag') != '') $tags[$data->getVar('widget_tag')] = $render;
					unset($unassigned[$k]);
				}
			}
		}

		//[widgetzone:somezonename(width=3)]
        $output = $this->content;
		$start = strpos($output,'[widgetzone:');
		$inlinezones = array();
		while ($start !== false) {
			$zone = array();
			$end = strpos($output,']',$start);
			$tag = substr(substr($output,$start,$end-$start),strlen('[widgetzone:'));
			$zone['name'] = substr($tag,0,strpos($tag,'('));
			if (strpos($tag,'(') !== false) {
                $params = substr($tag,strpos($tag,'(')+1,-1);
                $params = explode(':',$params);
                $params2 = array();
                foreach ($params as $k => $v) {
                    $tmp = explode('=',$v);
                    unset($params[$k]);
                    $params2[$tmp[0]] = $tmp[1];
                }
                $params = $params2;
            }
            else $params = array();
			$zone['params'] = $params;
			$zone['placeholder'] = '['.uniqid().']';
			$output = str_replace('[widgetzone:'.$tag.']',$zone['placeholder'],$output);
            $inlinezones[] = $zone;
			$start = strpos($output,'[widgetzone:');
		}
		foreach ($inlinezones as $k => $z) {
            $cols = 0;
            $content = '<div class="widgetzone '.$z['name'].'">';
			foreach ($unassigned as $kw => $w){
                $data = $w->getData();
				if ($data->getVar('widget_zone') == $z['name']) {
                    $width = $data->getVar('widget_options')['widgetwidth'];
                    if ($width == 0) $width = 12;
                    if ($cols + $width > 12) {
                        $content .= '</div><div class="row">';
                        $cols -= 12;
                    }
					$render = $controller->render($w);
                    $content .= '<div class="col-sm-'.(($width > 0)?$width:12).'">'.$render['src'].'</div>';
                    unset($unassigned[$kw]);
				}
			}
            $content .= '</div>';
            $output = str_replace($z['placeholder'],$content,$output);
		}

        $this->content = $output;

		//Add unassigned widgets to the default zone.
		foreach($unassigned as $k => $v) {
			$data = $v->getData();
			$render = $controller->render($v);
			$widgets['default'][] = $render;
			if ($data->getVar('widget_tag') != '') $tags[$data->getVar('widget_tag')] = $render;
		}

		//Add zonecount parameter for each zones
		foreach($zones as $k => $v) {
			$widgets[$v['name'].'count'] = (isset($widgets[$v['name']]))?count($widgets[$v['name']]):0;
		}
		$this->template->assign('widgets',$widgets);
		foreach ($tags as $k => $v) {
			$this->template->assign($k,$v);
		}
	}

	/**
	 * Sets widgets to use in the output
	 *
	 * @public
	 *
	 * @param array $widgets An array of widgets objects
	 * @return bool True on success
	 */
	public function setWidgets($widgets){
		if (!is_array($this->widgets)) $this->widgets = array();
		$this->widgets = array_merge($this->widgets,$widgets);
	}

	/**
	 * Adds a widget
	 *
	 * @public
	 *
	 * @param widget $widget
	 * @return bool True on success
	 */
	public function addWidget($widget){
		$this->widgets[] = $widget;
	}

	/**
	 * Sets the page title
	 *
	 * @public
	 *
	 * @param string $title
	 * @return void setTitle()
	 */
	public function setTitle($title){
		$this->options['title'] = $title;
	}

	/**
	 * Sets the page description
	 *
	 * @public
	 *
	 * @param string $description
	 * @return void setDescription()
	 */
	public function setDescription($description){
		$this->options['description'] = $description;
	}

	/**
	 * Sets the page keywords
	 *
	 * @public
	 *
	 * @param mixed $keywords String (coma separated) or array containing keywords.
	 * @return void setKeywords()
	 */
	public function setKeywords($keywords){
		$this->options['keywords'] = $keywords;
	}

	/**
	 * Sets page meta tags
	 *
	 * @public
	 *
	 * @param array $metas In tihs format : array[] = array( name => '', value=> '');
	 * @return void setMetas()
	 */
	public function setMetas($metas){
		$this->options['metas'] = $metas;
	}

	/**
	 * Adds a meta tag
	 *
	 * @param string $meta
	 * @return void addMeta()
	 */
	public function addMeta($meta){
		$this->options['metas'][] = $meta;
	}

	/**
	 * Sets the template to use for this theme
	 *
	 * @public
	 *
	 * @param Template $templateObj
	 * @return void setTemplate();
	 */
	public function setTemplate($templateObj) {
		if (is_object($templateObj)) $this->template =  $templateObj;
	}

	/**
	 * Sets the email template to use for this theme
	 *
	 * @public
	 *
	 * @param Template $templateObj
	 * @return void setTemplate();
	 */
	public function setEmailTemplate($templateObj) {
		global $service;
		if (is_object($templateObj)) {
			$this->emailtemplate =  $templateObj;
			$this->emailtemplate->assign('url',URL);
			$this->emailtemplate->assign('themeurl',THEMEURL.strtolower($service->get('Setting')->get('theme')));
			$this->emailtemplate->assign('langid',$service->get('Language')->getId());
			$this->emailtemplate->assign('lang',$service->get('Language')->getCode());
			$this->emailtemplate->assign('sitename',$service->get('Setting')->get('sitename'));
		}
	}

	/**
	 * Gets the email template to use for this theme
	 *
	 * @public
	 *
	 * @param Template $templateObj
	 * @return void setTemplate();
	 */
	public function getEmailTemplate() {
		return $this->emailtemplate;
	}

	/**
	 * Sets the current layout
	 *
	 * @public
	 *
	 * @param array Layout
	 * @return void setLayout()
	 */
	public function setLayout($layout){
		$this->layout = $layout;
	}

	/**
	 * Sets the main page content
	 *
	 * @public
	 *
	 * @param string $content
	 * @return void
	 */
	public function setContent($content){
		$this->content = $content;
	}

	/**
	 * Sets a variable to be assigned to the template
	 *
	 * @public
	 *
	 * @param string $var Variable name
	 * @param mixed $value Variable value
	 * @return void Sets a theme variable
	 */
	public function setVar($var,$value){
		$this->options[$var] = $value;
	}

	/**
	 * Returns information about the current theme.
	 *
	 * @public
	 * @return array $info
	 */
	public function getInfo(){
		return array();
	}

	/**
	 * Adds hreflang tags to current page.
	 *
	 * @public
	 **/
	public function getLanguageAnnotations(){
		global $service;
		$url = $service->get('Url')->get();
		$url = $url['path'];
		$src = '';
		$lang = $service->get('Language')->getId();
		$langs = $service->get('Language')->getCodes();
		$alllangs = $service->get('Language')->getAllLanguages();
		$obj = $service->get('Route')->getController();
		foreach ($langs as $k => $v) {
			$langTitle = $v;
			$ret = $obj->translate($k,0,$url);
			if ($ret == '') $ret = $v;
			$src .= '<link rel="alternate" hreflang="'.$langTitle.'" href="'.URL.$ret.'" />';
		}
		return $src;
	}
}
?>
