<?php
/**
 * LucidCMS Admin Interface classes
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class LucidElement {
	/**
	 * Holds data objects to render/sub-render and settings for this interface.
	 */
	protected $data;
	
    public function __construct($data){
        $this->setup($data);
    }
	public function setup($data) {
		$this->data = $data;
	}

	public function render(){
		if (!isset($this->data['type'])) $this->data['type'] = '';
		if (isset($this->data['rendermode'])) {
			$tag = $this->data['rendermode'];
		}else $tag = 'div';
		
		$code = '<'.$tag.' class="'.$this->data['type'].'" '.((isset($this->data['id']))?'id="'.$this->data['id'].'"':'').' '.((isset($this->data['name']))?'name="'.$this->data['name'].'"':'');
		if (isset($this->data['events']) && count($this->data['events']) > 0) {
			foreach($this->data['events'] as $k => $v) {
				$code .= ' '.$k.'="'.$v.'"';
			}
		}
		$code .= $this->getStyleAttr();
		$code .= '>';
		if ($this->data['type'] == 'vbox' || $this->data['type'] == 'vbox leftvbox') {
			$code .= '<div class="dragbar"></div>';
		}
		if ($tag == 'table') $code .= '<tr>';
		if (isset($this->data['title'])) {
			$code .= '<div class="title">';
			if (isset($this->data['icon'])) {
				if (strstr($this->data['icon'],'http://') || strstr($this->data['icon'],'.jpg')  || strstr($this->data['icon'],'.png')) {
					$code .= '<span class="icon-custom"><img src="'.$this->data['icon'].'"></span>';
				}else $code .= '<span class="glyphicon glyphicon-'.$this->data['icon'].'"></span>';
			}
			$code .= $this->data['title'].'</div>';
		}
		//$code .= '<div class="'.$this->data['type'].'_wrapper">';
		if (isset($this->data['childs']) && is_array($this->data['childs'])) 
		foreach($this->data['childs'] as $k => $v) {
			if ($this->data['type'] != 'hbox') {
				$code .= '<';
				if ($tag == 'table') $code .= 'td';
				else $code .= 'div';
				$code .= ' class="'.$this->data['type'].'_child">';
			}
			$code .= $v->render();
			if ($this->data['type'] != 'hbox') {
				if ($tag == 'table') $code .= '</td>';
				else $code .= '</div>';
			}
		}
		if (isset($this->data['html']) && $tag != 'table') {
			$code .= $this->data['html'];
		}
		//$code .= '</div>';

		if ($tag == 'table') $code .= '</tr></table>';
		else $code .= '</'.$tag.'>';
		return $code;
	}
	
	public function renderJS(){
		global $service;
		$code = '';
		if (isset($this->data['childs']) && is_array($this->data['childs'])) 
		foreach($this->data['childs'] as $k => $v) {
			if (is_object($v)) $code .= $v->renderJS();
		}
		return $code;
	}
	
	public function getStyleAttr(){
		$styles = '';
		if (isset($this->data['width'])) $styles = 'width:'.$this->data['width'].';';
		if (isset($this->data['height'])) $styles = 'height:'.$this->data['height'].';';
		if (isset($this->data['margin'])) $styles = 'margin:'.$this->data['margin'].';';
		if (isset($this->data['margin-right'])) $styles = 'margin-right:'.$this->data['margin-right'].';';
		if (isset($this->data['margin-left'])) $styles = 'margin-left:'.$this->data['margin-left'].';';
		if (isset($this->data['margin-top'])) $styles = 'margin-top:'.$this->data['margin-top'].';';
		if (isset($this->data['margin-bottom'])) $styles = 'margin-bottom:'.$this->data['margin-bottom'].';';
		if (isset($this->data['padding'])) $styles = 'padding:'.$this->data['padding'].';';
		if (isset($this->data['padding-left'])) $styles = 'padding-left:'.$this->data['padding-left'].';';
		if (isset($this->data['padding-right'])) $styles = 'padding-right:'.$this->data['padding-right'].';';
		if (isset($this->data['padding-top'])) $styles = 'padding-top:'.$this->data['padding-top'].';';
		if (isset($this->data['padding-bottom'])) $styles = 'padding-bottom:'.$this->data['padding-bottom'].';';
		if ($styles == '') return '';
		return 'style="'.$styles.'"';
	}
}

class LucidInterface extends LucidElement{
	public function __construct($data){
		$data['type'] = 'lucidinterface';
		$this->setup($data);
	}
	
	public function renderJS(){
		global $service;
		$code = '
		$(".lucidinterface .dragbar").each(function(){
			var el = $(this);
			$(this).mousedown(function(e){
				e.preventDefault();
				$(document).mousemove(function(e){
					$(el).parent().css("width",e.pageX+2);
					$(el).parent().next().css("margin-left",e.pageX+2);
				})
			});
			
		});
		setInterval(function(){adjustDragbarHeight();},1000);
		function adjustDragbarHeight(){
			$(".lucidinterface .dragbar").each(function(){		
				h1 = $(this).parent(".vbox").height();
				h2 = $(this).parent().next(".vbox").height();
				height = Math.max(h1,h2);
				$(this).height(height);
			});
		}
		$(document).mouseup(function(e){
			$(document).unbind("mousemove");
		});
	   
		function getAvailableViewportHeight() {
			var height = $(window).height();
			height -= parseInt($("body").css("marginTop").replace("px","").replace("%","").replace("em",""));
			height -= parseInt($("body").css("paddingTop").replace("px","").replace("%","").replace("em",""));
			height -= parseInt($("body").css("paddingBottom").replace("px","").replace("%","").replace("em",""));
			height -= parseInt($("body").css("marginBottom").replace("px","").replace("%","").replace("em",""));
			nav = $(".navbar");
			if (nav) {
				height -= nav.height();
			}
			return height;
		}
		';
		if (is_array($this->data['childs'])) 
		foreach($this->data['childs'] as $k => $v) {
			if (is_object($v)) $code .= $v->renderJS();
		}
		return $code;
	}	
}

class LucidViewport extends LucidElement{
	public function __construct($data){
		$data['type'] = 'viewport';
		$this->setup($data);
	}
	
	public function renderJS(){
		global $service;
		$code = '$(document).ready(function() {var height = getAvailableViewportHeight();$(".lucidinterface .viewport").css("min-height",height+"px");});';
		if (isset($this->data['childs']) && is_array($this->data['childs'])) 
		foreach($this->data['childs'] as $k => $v) {
			$code .= $v->renderJS();
		}
		return $code;
	}
}

class LucidLayout extends LucidElement{
	public function __construct($data){
		$data['type'] = 'layout';
		$this->setup($data);
	}
}

class LucidPanel extends LucidElement{
	public function __construct($data){
		$data['type'] = 'panel';
		$this->setup($data);
	}
}

class LucidHBox extends LucidElement{
	public function __construct($data){
		$data['type'] = 'hbox';
		//$data['rendermode'] = 'table';
		$this->setup($data);
	}
}

class LucidVBox extends LucidElement{
	public function __construct($data){
		$data['type'] = 'vbox';
		$this->setup($data);
	}
}
class LucidLeftVBox extends LucidElement{
	public function __construct($data){
		$data['type'] = 'vbox leftvbox';
		$this->setup($data);
	}
}
class LucidMainVBox extends LucidElement{
	public function __construct($data){
		$data['type'] = 'vbox mainvbox';
		$this->setup($data);
	}
}

class LucidToolBar extends LucidElement{
	public function __construct($data){
		$data['type'] = 'tbar';
		$this->setup($data);
	}
}

class LucidButton extends LucidElement{
	public function __construct($data){
		$data['type'] = 'btn';
		$this->setup($data);
	}
}
class LucidPrimaryButton extends LucidElement{
	public function __construct($data){
		$data['type'] = 'btn btn-primary';
		$this->setup($data);
	}
}
class LucidLink extends LucidElement{
	public function __construct($data){
		$data['type'] = 'a';
		$data['rendermode'] = 'a';
		$this->setup($data);
	}
}

class LucidButtonDropdown extends LucidElement{
	public function __construct($data){
		$data['type'] = 'btn-group';
		$this->setup($data);
	}
    
	public function render(){
		$code = '<div class="'.$this->data['type'].'" '.((isset($this->data['id']))?'id="'.$this->data['id'].'"':'').' '.((isset($this->data['name']))?'name="'.$this->data['name'].'"':'');
		if (isset($this->data['events']) && count($this->data['events']) > 0) {
			foreach($this->data['events'] as $k => $v) {
				$code .= ' '.$k.'="'.$v.'"';
			}
		}
		$code .= $this->getStyleAttr();
		$code .= '><a class="btn dropdown-toggle" data-toggle="dropdown" href="#">';
		if (isset($this->data['title'])) {
			$code .= '<div class="title">';
			if (isset($this->data['icon'])) {
				if (strstr($this->data['icon'],'http://') || strstr($this->data['icon'],'.jpg')  || strstr($this->data['icon'],'.png')) {
					$code .= '<span class="icon-custom"><img src="'.$this->data['icon'].'"></span>';
				}else $code .= '<span class="glyphicon glyphicon-'.$this->data['icon'].'"></span>';
			}
			$code .= $this->data['title'].'<span class="caret"></span></div>';
		}
		$code .= '</a><ul class="dropdown-menu">';
		if (isset($this->data['childs']) && is_array($this->data['childs'])) 
		foreach($this->data['childs'] as $k => $v) {
			$code .= '<li class="'.$this->data['type'].'_child">'.$v->render().'</li>';
		}
		$code .= '</ul>';
		if (isset($this->data['html'])) {
			$code .= $this->data['html'];
		}
		$code .= '</div>';
		return $code;
	}
}

class LucidMenuItem extends LucidElement{
	public function __construct($data){
		$data['type'] = '';
		$this->setup($data);
	}
	public function render(){
		$code = '<a ';
		if (isset($this->data['events']) && count($this->data['events']) > 0) {
			foreach($this->data['events'] as $k => $v) {
				$code .= ' '.$k.'="'.$v.'"';
			}
		}
		$code .= $this->getStyleAttr();
		$code .= '>';
		if (isset($this->data['icon'])) {
			if (strstr($this->data['icon'],'http://') || strstr($this->data['icon'],'.jpg')  || strstr($this->data['icon'],'.png')) {
					$code .= '<span class="icon-custom"><img src="'.$this->data['icon'].'"></span>';
			}else $code .= '<span class="glyphicon glyphicon-'.$this->data['icon'].'"></span>';
		}
		if (isset($this->data['title'])) $code .= $this->data['title'];
		$code .= '</a>';
		return $code;
	}
	
}

class LucidStatusBar extends LucidElement{
	public function __construct($data){
		$data['type'] = 'sbar';
		$this->setup($data);
	}
}

class LucidBreadCrumbs extends LucidElement{
	public function __construct($data){
		$data['type'] = 'breadcrumbs';
		$this->setup($data);
	}
	
	public function render(){
		$code = '';
		if (isset($this->data['childs'])) {
			$code = '<ul class="breadcrumb"><li><a href="'.URL.'"><span class="glyphicon glyphicon-home"></span></a></li>';
			$passedfirst = false;
			$count = 0;
			$countb = count($this->data['childs']);
			foreach ($this->data['childs'] as $k => $v) {
				if ($passedfirst) $code .= '</li>';
				$code .= '<li';
				if ($count == $countb-1) $code .= ' class="active"'; 
				$code .= '>';
				if ($count < $countb-1) $code .= '<a href="'.$v['link'].'">';
				if(isset($v['icon']) && $v['icon'] != '') $code .= '<span class="'.$v['icon'].'"></span>';
				$code .= $v['title'];
				if ($count < $countb-1) $code .= '</a> ';
				$passedfirst = true;
				$count++;
			}
			$code .= '</li></ul>';
		}
		return $code;		
	}
}

class LucidTree extends LucidElement{
	public function __construct($data){
		$data['type'] = 'tree';
		$this->setup($data);
	}
}

class LucidList extends LucidElement{
	public function __construct($data){
		$data['type'] = 'list';
		$this->setup($data);
	}
	
	public function render(){
		$code = '';
		$code .= '<ul class="list">';
		if (isset($this->data['title'])) {
			$code .= '<li class="title">'.$this->data['title'].'</li>';
		}

		if (isset($this->data['childs'])) {
			foreach ($this->data['childs'] as $k => $v) {
				$code .= '<li>'.$v->render().'</li>';
			}
		}
		$code .= '</ul>';
		return $code;		
	}
	
}
class LucidAccordion extends LucidElement{
	public function __construct($data){
		$data['type'] = 'accordion';
		$this->setup($data);
	}
	
	public function render(){
		$code = '';		
		$code .= '<div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">';

		if (isset($this->data['childs'])) {
			foreach ($this->data['childs'] as $k => $v) {
				$code .= '<div class="panel panel-default">'.$v->render().'</div>';
			}
		}
		$code .= '</div>';
		return $code;		
	}
}
class LucidAccordionSection extends LucidElement{
	public function __construct($data){
		$data['type'] = 'accordionsection';
		$this->setup($data);
	}
	
	public function render(){
		$id = uniqid();
		$code = '
		<div class="panel-heading" role="tab" id="heading_'.$id.'">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapse_'.$id.'" aria-expanded="false" aria-controls="collapse_'.$id.'">';
		if (isset($this->data['title'])) {
			$code .= '<div class="title">'.$this->data['title'].'</div>';
		}
		$code .= '</a></h4></div>';
		
		$code .= '<div id="collapse_'.$id.'" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="heading_'.$id.'">
		<div class="panel-body">';

		if (isset($this->data['childs'])) {
			foreach ($this->data['childs'] as $k => $v) {
				$code .= $v->render();
			}
		}
		
		$code .= '</div></div>';
		return $code;		
	}
}

class LucidActionPanel extends LucidElement{
	public function __construct($data){
		$data['type'] = 'actionpanel';
		$data['html'] = '<div class="description">'.$data['description'].'</div>';
		$this->setup($data);
	}	
}
?>