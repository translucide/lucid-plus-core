<?php
/**
 * Table base class
 *
 * Defines base table class
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

global $service;
$service->get('Ressource')->get('core/display/table');

interface TableInterface {
	
	/**
	 * Sets the table title
	 *
	 * @public
	 * @return void setTitle()
	 */
	public function setTitle($title);
	
	/**
	 * Sets the name of the table
	 *
	 * @public
	 * @return void setName()
	 */
	public function setName($name);
	
	/**
	 * Sets the class of the table
	 *
	 * @public
	 * @return void setId()
	 */
	public function setClass($class);
	
	/**
	 * Sets the column parameters of the table
	 *
	 * @public
	 * @return void setId()
	 */
	public function setColumns($columns);
	
	/**
	 * Add a row element to the table
	 *
	 * @public
	 * @return bool True on success
	 */
	public function add($element);
	
	/**
	 * Alias for add method
	 *
	 * @public
	 * @return bool true on success
	 */
	public function addElement($element);

	/**
	 * Renders a form to JS
	 *
	 * @public
	 * @return string The form JS code
	 */
	public function render();
	
	/**
	 * Exports the form data for saving the form
	 *
	 * @public
	 * @return array Form data
	 */
	public function export();
	
	/**
	 * Triggers events on table load
	 *
	 * @public
	 * @return void
	 */
	public function triggerEvents();
} 

class Table implements TableInterface {
	
	/**
	 * A table data array which holds the table data
	 *
	 * @private
	 *
	 * @var TableData The table data
	 */
	private $data;

	/**
	 * Table PHP 5 constructor
	 * 
	 * @method __construct
	 */
	public function __construct($title='', $name=''){
		$this->Table($title, $name);
	}
	
	/**
	 * Table PHP 4 constructor
	 *
	 * @public
	 *
	 * @param string $title Defaults to empty string
	 * @param string $id Defaults to empty string
	 * @return void Table()
	 */	
	public function Table($title='', $name=''){
		$this->data['type'] = 'datatables';
		$this->data['title'] = $title;
		if ($name == '') $name = 'table';
		$this->data['name'] = $name;
	}
	
	/*
	 * Sets the table title
	 *
	 * @public
	 *
	 * @param string $title
	 * @return void setTitle()
	 */
	public function setTitle($title){
		$this->setVar('title', $title);
	}
	
	/*
	 * Sets the table name
	 *
	 * @public
	 *
	 * @param string $name
	 * @return void setName()
	 */
	public function setName($name){
		$this->setVar('name', $name);
	}

	/**
	 * Sets the class of the table
	 *
	 * @public
	 * @return void setClass()
	 */
	public function setClass($class){
		$this->setVar('class', $class);	
	}
	
	/*
	 * Sets the table columns all at once
	 *
	 * @public
	 *
	 * @param array $columns
	 * @return void setColumns()
	 */
	public function setColumns($cols){
		foreach ($cols as $k => $v) {
			if (!is_a($v,'TableColumn')) {
				unset($cols[$k]);
				global $service;
				$log =& $service->get('log');
				$log->add("Table::setColumns: a column is not a TableColumn object.",__FILE__,__LINE__,LOG_ERR);
				return false;				
			}else {
				$cols[$k] = $v;
			}
		}
		$this->setVar('columns', $cols);
	}

	/**
	 * Sets a variable
	 *
	 * @public
	 *
	 * @return void Sets a variable in the data property.
	 */
	public function setVar($var,$val){
		$this->data[$var] = $val;
	}
	
	/**
	 * Sets table data at once
	 *
	 * @public
	 * @param array $data
	 * @return void setTableData
	 */
	public function setTableData($data) {
		$this->data = $data;
	}
	
	/**
	 * Gets table data
	 *
	 * @public
	 * @return array getTableData()
	 */
	public function getTableData() {
		return $this->data;
	}
	
	/**
	 * Adds a row element to the table
	 *
	 * @public
	 * 
	 * @param array $element
	 * @return bool True if element was added to the form correctly
	 */
	public function add($element){
		if (count($this->data['columns']) == count($element)) {
			$this->data['rows'][] = $element;
		}else {
			global $service;
			$log =& $service->get('log');
			$log->add("Table::add: element does not have the right number of columns and therefore could not be added to the table.",__FILE__,__LINE__,LOG_ERR);
			return false;
		}
		return true;
	}
	
	/**
	 * Alias of add method : adds an element to the table
	 *
	 * @public
	 *
	 * @param array $element
	 * @return bool True if element was added correctly
	 */
	public function addElement($element) {
		$this->add($element);
	}
	
	/**
	 * Exports this table's data
	 *
	 * @public
	 *
	 * @return array The table data
	 */
	public function export(){
		return $this->data;
	}
	
	/**
	 * Adds dynamically added table fields to this table
	 *
	 * @public
	 * @return void Modifies the current form by adding plugin fields
	 */
	public function triggerEvents(){
		global $service;
		//Get other plugins that may want to display things here.
		$this->setTableData($service->get('EventHandler')->trigger('table.on'.ucfirst($this->data['name']).'load',$this->getTableData(),true));
	}
	
	/**
	 * Renders the form to the appropriate output type
	 * 
	 * @public
	 *
	 * @return string The form code representing this form
	 */
	public function render(){
		$this->triggerEvents();
		foreach($this->data['rows'] as $k => $v) {
			foreach($this->data['rows'][$k] as $k1 => $v1) $rows[$k][$k1] = $this->data['columns'][$k]->renderCell($v1);
		}
		foreach($this->data['columns'] as $k => $v) {
			$this->data['columns'][$k] = $v->export();
		}
		$code = "$(\"#".$this->data['name']."_div\").lucidTable(";
		$code .= json_encode($this->data);
		$code .= ');';
		
		global $service;
		$service->get('Ressource')->addScript($code);
		return "<div id=\"".$this->data['name']."_div\"></div>";
	}
}
?>