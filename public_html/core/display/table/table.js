/**
 * lucidTable jQuery Plugin
 *
 * Writes tables in JS.
 *
 * Oct 30, 2012
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

(function( $ ) {
	
	var methods = {
		settings : {
			type: 'datatables',
			title: '',
			columns: [],
			name: '',
			contenturl: '',
			rows : [],
			displayactions: 0
		},
		/**
		 * Initialize the form.
		 *
		 * Renders the form HTML code to the targeted element.
		 */
		init : function( options ) {
			/**
			 * Plugin default settings
			 */
			methods.settings = settings = $.extend(methods.settings, options);
			
			var $this = $(this);
			data = $this.data('lucidTable');
			$this.data('lucidTable', {
				target : $this,
				settings : methods.settings
			});
			innerHTML = '';
			if (settings.title != '') {
				innerHTML += '<h2>'+settings.title+'</h2>';
			}
			innerHTML += '<table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="'+settings.name+'_table"><thead><tr>';
			for(i=0;i<settings.columns.length;i++) {
				innerHTML += '<th';
				if (settings.columns[i].titleid) innerHTML += ' id="'+settings.columns[i].titleid+'"';
				if (settings.columns[i].titleclass) innerHTML += ' class="'+settings.columns[i].titleclass+'"';
				if (settings.columns[i].titleid) innerHTML += ' id="'+settings.columns[i].titleid+'"';
				if (settings.columns[i].titleid) innerHTML += ' id="'+settings.columns[i].titleid+'"';
				innerHTML += '>';
				if (settings.columns[i].titlewrapper) {
					if ($.isArray(settings.columns[i].titlewrapper)) innerHTML += settings.columns[i].titlewrapper[0];
					else innerHTML += '<'+settings.columns[i].titlewrapper+'>';
				}
				innerHTML += settings.columns[i].title;
				if (settings.columns[i].titlewrapper) {
					if ($.isArray(settings.columns[i].titlewrapper)) innerHTML += settings.columns[i].titlewrapper[1];
					else innerHTML += '</'+settings.columns[i].titlewrapper+'>';
				}
				innerHTML += '</th>';
			}
			innerHTML += '</tr></thead><tbody>';
			if (settings.rows) {
				for(i=0;i<settings.rows.length;i++){
					innerHTML += '<tr>';
					for(j=0; j< settings.rows[i].length;j++){
						if (settings.columns[j].type) {
							switch(settings.columns[j].type) {
								case 'yesno' : {
									innerHTML += '<td><img src="{url}/com/system/asset/icon/16x16/';
									if (settings.rows[i][j] == 0) innerHTML += 'no.png';
									else innerHTML += 'yes.png';
									innerHTML += '"></td>';
								}break;
								case 'datetime' : {
									innerHTML += '<td>';
									var a = new Date(settings.rows[i][j]*1000);
									var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
									var year = a.getFullYear();
									var month = months[a.getMonth()];
									var date = a.getDate();
									var hour = a.getHours();
									var min = a.getMinutes();
									var sec = a.getSeconds();
									innerHTML += date+', '+month+' '+year+' '+hour+':'+min+':'+sec;
									innerHTML += '</td>';
								}break;
								case 'actions' : {
									innerHTML += '<td>';
									if (settings.columns[j].editurl) innerHTML += '<a href="{url}/'+settings.columns[j].editurl.replace('{value}',settings.rows[i][j])+'"';
									if (settings.columns[j].edittip) innerHTML += ' class="tooltiptrigger" title="'+settings.columns[j].edittip+'" alt="'+settings.columns[j].edittip+'"';
									if (settings.columns[j].onedit) {
										settings.columns[j].onedit.replace('{value}',settings.rows[i][j]);
										for(counter=0;counter<10;counter++){
											if (settings.rows[i][counter]) settings.columns[j].onedit.replace('{col'+counter+'}',settings.rows[i][counter]);
										}
										innerHTML += ' onclick="'+settings.columns[j].onedit+'"';
									}									
									if (settings.columns[j].editurl) innerHTML += '><span class="icon-edit"></span></a>';
									if (settings.columns[j].deleteurl || settings.columns[j].ondelete) innerHTML += '<a';
									if (settings.columns[j].deleteurl) innerHTML += ' href="{url}/'+settings.columns[j].deleteurl.replace('{value}',settings.rows[i][j])+'"';
									if (settings.columns[j].deletetip) innerHTML += ' class="tooltiptrigger" title="'+settings.columns[j].deletetip+'" alt="'+settings.columns[j].deletetip+'"';
									if (settings.columns[j].ondelete) {
										var replacement = settings.columns[j].ondelete.replace('{value}',settings.rows[i][j]);
										for(counter=0;counter<10;counter++){
											if (settings.rows[i][counter]) {
												replacement = replacement.replace('{col'+counter+'}',settings.rows[i][counter]);
												console.info('{col'+counter+'}');
											}
										}
										innerHTML += ' onclick="'+replacement+'"';
									}
									if (settings.columns[j].deleteurl || settings.columns[j].ondelete) innerHTML += '><span class="icon-trash"></span></a>';
									innerHTML += '</td>';
								}break;
								case 'image' : {
									innerHTML += '<td><img src="{url}/'+settings.rows[i][j]+'"';
									if (settings.columns[j].width) innerHTML += ' width="'+settings.columns[j].width+'"></td>';
									if (settings.columns[j].height) innerHTML += ' height="'+settings.columns[j].height+'"></td>';
								}break;
								case '' :
								default : {
									innerHTML += '<td>'+settings.rows[i][j]+'</td>';
								}break;
							}
						}
						else innerHTML += '<td>'+settings.rows[i][j]+'</td>';
					}
					innerHTML += '</tr>';
				}
			}
			innerHTML += '</tbody>';
			innerHTML += '</tr></thead></table>';
			document.write('<link rel="stylesheet" href="{url}/lib/vendor/datatables/datatables.css" />');
			document.write('<script language="javascript" type="text/javascript" src="{url}/lib/vendor/datatables/datatables-plugin.js"></script>');
			document.write('<script language="javascript" type="text/javascript" src="{url}/lib/vendor/datatables/datatables-bootstrap.js"></script>');
			$(this).html(innerHTML);			
			tableObj = new Object();
			if (settings.contenturl != '') {
				tableObj.bProcessing = true;
				tableObj.sAjaxSource = '{url}/'+settings.contenturl;
			}
			tableObj.sDom = "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>";	
			tableObj.sPaginationType = "bootstrap";
			tableObj.sPaginationType = "bootstrap";
			tableObj.oLanguage = {"sLengthMenu": "_MENU_ records per page"};
			$(function () {
				$("a.tooltiptrigger").tooltip();
			});
			$(document).ready(function() {
				$('#'+settings.name+'_table').dataTable(tableObj);
			} );
			return this;
		},

	};

	$.fn.lucidTable = function(method) { 
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.lucidTable' );
		}    
	};	

})(jQuery);