<?php
/**
 * Table column base class
 *
 * Defines a table column class
 *
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class TableColumn extends BaseClass{
	/**
	 * A data array which holds the table column data
	 *
	 * @private
	 *
	 * @var array The table column data
	 */
	private $data;

	/**
	 * Table PHP 5 constructor
	 *
	 * @method __construct
	 */
	public function __construct($title='', $params=array()){
		$this->TableColumn($title, $params);
	}

	/**
	 * Table PHP 4 constructor
	 *
	 * @public
	 *
	 * @param string $title Defaults to empty string
	 * @param array $params
	 * @return void Table()
	 */
	public function TableColumn($title='', $params=array()){
		$this->data = array();
		$params['title'] = $title;
		$this->data = $params;
	}

	/**
	 * Sets the title id
	 *
	 * @param string $id
	 * @return void setTitleId();
	 */
	public function setTitleId($id){
		$this->data['titleid'] = $id;
	}

	/**
	 * Sets the column title class
	 *
	 * @param string $cls
	 * @return void setTitleClass();
	 */
	public function setTitleClass($cls){
		$this->data['titleclass'] = $cls;
	}

	/**
	 * Sets the column class
	 *
	 * @param string $cls
	 * @return void setclass();
	 */
	public function setClass($cls){
		$this->data['class'] = $cls;
	}

	/**
	 * Sets the id
	 *
	 * @param string $id
	 * @return void setId();
	 */
	public function setId($id){
		$this->data['id'] = $id;
	}

	/**
	 * Sets the column title wrapper tag
	 *
	 * @param string $cls
	 * @return void setclass();
	 */
	public function setTitleWrapper($tag){
		$this->data['titlewrapper'] = $tag;
	}

	/**
	 * Sets the column wrapper tag
	 *
	 * @param string $cls
	 * @return void setclass();
	 */
	public function setWrapper($tag){
		$this->data['wrapper'] = $tag;
	}

	/**
	 * Sets the column priority for mobile table display
	 *
	 * @param int $p
	 * @return void setclass();
	 */
	public function setPriority($p){
		if (is_numeric($p)) $this->data['priority'] = $p;
	}

	/**
	 * Sets the column datatype
	 *
	 * @param string $datatype
	 * @return void setType();
	 */
	public function setType($datatype){
		$this->data['type'] = $datatype;
	}

	/**
	 * Export the current object as Array
	 *
	 * @return array Object Data for export
	 */
	public function export(){
		return $this->data;
	}

	/**
	 * Renders cell
	 *
	 * @public
	 * @return string Rendered cell
	 */
	public function renderCell($data){
		/*if (isset($this->data['type']))
		switch($this->data['type']) {
			case 'yesno' : {
				if ($data == 1) $data = 'Yes';
				else $data = 'No';
			}break;
			default:{

			}break;
		}*/
		return $data;
	}
}
?>
