<?php
/**
 * Pager class
 *
 * Handles pager code generation
 * Jan 15, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2015 Translucide
 * @license
 * @since 		0.1
 */

class Pager extends BaseClass{
	public function render($total,$perpage=10,$page,$url='',$items=5) {
		$code = '';
		$nbshown = 5;
		if (!$perpage || $perpage == 0) $perpage=10;
		$eachside = floor(($items-1)/2);
		$start = $page - $eachside;
		if ($start < 1) $start = 1;
		if ($total > $perpage) {
			$code = '<div class="row"><div class="col-sm-12"><ul class="pagination">';
			if ($page >= 1) {
				$code .= '<li><a ';
				if ($url != '') $code .= 'href="'.str_replace('{page}',$page-1,$url).'"';
				$code .= ' data-page="'.($page-1).'">&laquo;</a></li>';
			}
			else $code .= '<li class="disabled"><a  class="disabled" onclick="return false;">&laquo;</a></li>';

			for($i = $start; $i < $start + $nbshown; $i++){
				if (ceil($total / $perpage) >= $i) $code .= '<li class="'.(($page == $i-1)?"active":"").'"><a '.(($url != '')?'href="'.str_replace('{page}',$i-1,$url).'"':'').' data-page="'.($i-1).'">'.$i.'</a></li>';
			}

			if ($page < floor($total/$perpage)) $code .= '<li><a '.(($url != '')?'href="'.str_replace('{page}',$page+1,$url).'"':'').' data-page="'.($page+1).'">&raquo;</a></li>';
			else $code .= '<li class="disabled"><a class="disabled" onclick="return false;">&raquo;</a></li>';
			$code .= "</ul></div></div>";
		}else $code = '';
		return $code;
	}
}
