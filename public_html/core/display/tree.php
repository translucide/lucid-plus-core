<?php
/**
 * Tree handler class
 *
 * Handles tree operations
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class Tree extends BaseClass{

	/**
	 * Store data about the current tree
	 *
	 * @public
	 *
	 * @var $data
	 */
	private $data;

	/**
	 * Sets the parameters to use to save tree structure (folders) to DB
	 *
	 * @public
	 *
	 * @param string $table
	 * @param string $idfield The id field name of records
	 * @param string $parentfield The parent field name of records
	 * @param string $positionfield The position field name of records
	 * @param string $titlefield The title field name of records
	 * @return void folderSettings()
	 */
	public function folderSettings($table, $idfield, $parentfield, $positionfield, $typefield, $titlefield){
		$this->data['folder']['table'] = $table;
		$this->data['folder']['id'] = $idfield;
		$this->data['folder']['parent'] = $parentfield;
		$this->data['folder']['position'] = $positionfield;
		$this->data['folder']['title'] = $titlefield;
		$this->data['folder']['type'] = $typefield;
	}

	/**
	 * Sets folders types (for a tree where leafs & folders share the same table)
	 *
	 * @public
	 *
	 * @param array $types
	 * @return void folderTypes()
	 */
	public function folderTypes($types){
		$this->data['folder']['types'] = $types;
	}

	/**
	 * Sets the parameters to use to save tree structure (leafs) to DB
	 *
	 * @public
	 *
	 * @param string $table
	 * @param string $idfield The id field name of records
	 * @param string $parentfield The parent field name of records
	 * @param string $positionfield The position field name of records
	 * @param string $titlefield The title field name of records
	 * @return void leafSettings()
	 */
	public function leafSettings($table, $idfield, $parentfield, $positionfield, $typefield, $titlefield){
		$this->data['leaf']['table'] = $table;
		$this->data['leaf']['id'] = $idfield;
		$this->data['leaf']['parent'] = $parentfield;
		$this->data['leaf']['position'] = $positionfield;
		$this->data['leaf']['title'] = $titlefield;
		$this->data['leaf']['type'] = $typefield;
	}

	/**
	 * Update position of a node
	 *
	 * @public
	 *
	 * @return bool true or false depending on the operation success
	 */
	public function order($id,$parent,$newposition,$type='folder'){
		global $service;
		$id = addslashes($id);
		$parent = addslashes($parent);
		$newposition = intval($newposition);
		$db =& $service->get('Db');
		$success = false;
		if (isset($this->data[$type])) {
			$src = $this->data[$type];
			//Update items positions past the current item new position...
			$ret = $db->query(
				'UPDATE '.$db->prefix($src['table']).' '.
				'SET `'.$src['position'].'` = `'.$src['position'].'` + 1 '.
				'WHERE `'.$src['position'].'` >= '.$newposition.' AND `'.$src['parent'].'` = \''.$parent.'\''.$this->addSiteCriteria($src['table']));
			$success = ($ret || $ret == array())?true:false;
			//Set item new position
			$ret = $db->query(
				'UPDATE '.$db->prefix($src['table']).' '.
				'SET `'.$src['position'].'` = '.$newposition.' '.
				'WHERE `'.$src['id'].'` = \''.$id.'\' '.$this->addSiteCriteria($src['table']));
			$success = $success && ($ret || $ret == array())?true:false;

		}
		return $success;
	}

	/**
	 * Reorder nodes in a section/category by removing empty spaces between positions
	 *
	 * @public
	 *
	 * @return bool true or false depending on the operation success
	 */
	public function reorder($parent,$type='folder'){
		global $service;
		$db =& $service->get('Db');
		$success = false;
		if ($type == 'folder' && $this->data['folder']['table'] != $this->data['leaf']['table']) {
			$src = $this->data['folder'];
		}else {
			if ($type == 'folder' || $type == 'leaf') {
				$src = $this->data['leaf'];
			}
		}
		$ret = $db->query('SELECT `'.$src['id'].'`,`'.$src['position'].'` FROM '.$db->prefix($src['table']).
			' WHERE `'.$src['parent'].'` = \''.$parent.'\' GROUP BY `'.$src['id'].'` ORDER BY '.$src['position'].' ASC');
		$i = 0;
		$success = ($ret)?true:false;
		foreach($ret as $k => $v) {
			$ret2 = $db->query(
				'UPDATE '.$db->prefix($src['table']).' '.
				'SET `'.$src['position'].'` = \''.$i.'\' '.
				'WHERE `'.$src['id'].'` = \''.$v[$src['id']].'\' AND `'.$src['parent'].'` = \''.$parent.'\''.$this->addSiteCriteria($src['table']));
			$success = ($ret2 && $success)?true:false;
			$i++;
		}
		return $success;
	}

	/**
	 * Moves a node or leaf
	 *
	 * @public
	 *
	 * @return bool $success
	 */
	public function move($id,$newparent,$position,$type) {
		global $service;
		$db =& $service->get('Db');
		$id = addslashes($id);
		$newparent = addslashes($newparent);
		$success = $this->reorder($newparent,$type);
		if (isset($this->data[$type])) {
			$success = true;
			$src = $this->data[$type];
			$ret = $db->query(
				'UPDATE `'.$db->prefix($src['table']).'` '.
				'SET `'.$src['parent'].'` = \''.$newparent.'\' '.
				'WHERE `'.$src['id'].'` = \''.$id.'\' '.$this->addSiteCriteria($src['table']));
			$this->order($id,$newparent,$position,$type);
			if ($ret == false && !$ret == array());
		}
	}

	/**
	 * Get children nodes / leafs
	 *
	 * @public
	 *
	 * @return array $return
	 */
	public function getChildren($parent) {
		global $service;
		$db =& $service->get('Db');
		$lang = $service->get('Language')->getDefault();
		$success = true;
		$foldertypes = '';
		$parent = addslashes($parent);
		//1. Get all folders in parent ID.
		if (isset($this->data['folder'])) {
			$src = $this->data['folder'];
			if (isset($src['types']) && is_array($src['types']) && $src['type'] != '') {
				$foldertypes = ' AND `'.$src['type'].'` IN(\''.implode('\',\'',$src['types']).'\')';
			}
			$data = $db->query(
				'SELECT `'.$src['id'].'`, `'.$src['parent'].'`,`'.$src['position'].'`,`'.$src['title'].'` '.
				'FROM `'.$db->prefix($src['table']).'` '.
				'WHERE `'.$src['parent'].'` = \''.$parent.'\' AND `'.$src['table'].'_language` = '.$lang['id'].' '.$this->addSiteCriteria($src['table']).$foldertypes.' '.
				'ORDER BY `'.$src['position'].'` ASC');
			if ($data) {
				foreach ($data as $k => $v) {
					$data[$k] = array(
						'id' => $v[$src['id']],
						'parent' => $v[$src['parent']],
						'position' => $v[$src['position']],
						'title' => $v[$src['title']],
						'type' => 'folder'
					);
				}
			}
			//Change foldertypes crit for leaf selection if any to prevent duplication of items.
			if (isset($src['types']) && is_array($src['types']) && $src['type'] != '') {
				$foldertypes = ' AND `'.$src['type'].'` NOT IN(\''.implode('\',\'',$src['types']).'\')';
			}
		}

		//2. Get all leafs under this parent.
		if (isset($this->data['leaf'])) {
			$src = $this->data['leaf'];
			$data2 = $db->query(
				'SELECT `'.$src['id'].'`, `'.$src['parent'].'`,`'.$src['position'].'`,`'.$src['title'].'` ,`'.$src['type'].'` '.
				'FROM `'.$db->prefix($src['table']).'` '.
				'WHERE `'.$src['parent'].'` = \''.$parent.'\' AND `'.$src['table'].'_language` = '.$lang['id'].' '.$this->addSiteCriteria($src['table']).' '.$foldertypes.' '.
				'ORDER BY `'.$src['position'].'` ASC');
			if ($data2) {
				foreach ($data2 as $k => $v) {
					$data2[$k] = array(
						'id' => $v[$src['id']],
						'parent' => $v[$src['parent']],
						'position' => $v[$src['position']],
						'title' => $v[$src['title']],
						'type' => ($v[$src['type']] == '')?'default':$v[$src['type']]
					);
				}
			}
		}
		if (!is_array($data)) $data = array();
		if (!is_array($data2)) $data2 = array();
		$ret = array_merge($data,$data2);

		//Same table, reorder children with mixed folders and leafs in the natural order they should be
		if ($this->data['leaf']['table'] == $this->data['folder']['table']) {
			$ret2 = array();
			$i = 0;
			while(count($ret) > 0) {
				foreach ($ret as $k => $v) {
					if ($v['position'] == $i) {
						$ret2[] = $v;
						unset($ret[$k]);
					}
				}
				$i++;
			}
			$ret = $ret2;
		}
		return $ret;
	}


	/**
	 * Create an item in the tree
	 *
	 * @public
	 *
	 * @return bool $success
	 */
	public function create($parent,$position,$title,$type='folder',$dtype){
		global $service;
		$parent = addslashes($parent);
		$position = intval($position);
		$db =& $service->get('Db');
		$success = $this->reorder($parent,$type);
		if (isset($this->data[$type])) {
			$src = $this->data[$type];
			$success = true;

			//1. Get next objid available
			if (DB_UUIDS) {
				$service->get('Ressource')->get('core/security/hash/uuid');
				$max = UUID::shorten(UUID::generate());
			}else {
				$max = $db->query('SELECT MAX(`'.$src['id'].'`) AS `max` FROM `'.$db->prefix($src['table']).'` '.
					'WHERE `'.$src['id'].'` > 0 '.$this->addSiteCriteria($src['table']));
				$max = $max[0]['max']+1;

				//Start at least at 2 because jstree assumes root is 1 and main folders should be 2+
				if ($max < 2) $max = 2;
			}

			//2. Create the object in all languages on current site only
			$langs = $service->get('Language')->getCodes();
			if ($dtype && $src['type'] != '') $datatype = array(',`'.$src['type'].'`',',\''.$dtype.'\'');
			else $datatype = array('','');

			foreach ($langs as $k => $v) {
				$ret = $db->query('INSERT INTO `'.$db->prefix($src['table']).'` '.
					'(`'.$src['id'].'`,`'.$src['parent'].'`,`'.$src['position'].'`,`'.$src['title'].'`'.$datatype[0].',`'.$src['table'].'_language`,`'.$src['table'].'_site`) '.
					'VALUES(\''.$max.'\',\''.$parent.'\',\''.$position.'\',\''.addslashes($title).'\''.$datatype[1].',\''.$k.'\''.$this->addSiteCriteria($src['table'],'INSERT').')');
				if ($ret == false && $ret != array()) $success = false;
			}

			//3. Reorder objects below current obj position
			$ret = $this->order($max,$parent,$position,$type);
			if ($ret == false && $ret != array()) $success = false;

			//4. Update current obj position
			$ret = $db->query('UPDATE `'.$db->prefix($src['table']).'` '.
				'SET `'.$src['position'].'` = '.$position.' '.
				'WHERE `'.$src['id'].'` = \''.$max.'\' '.$this->addSiteCriteria($src['table']));
			if ($ret == false && $ret != array()) $success = false;

		}

		if ($success) $success = $max;
		return $success;
	}

	/**
	 * Rename a node or leaf
	 *
	 * @public
	 *
	 * @return bool $success
	 */
	public function rename($id,$title,$type='folder') {
		global $service;
		$id = addslashes($id);
		$lang = $service->get('Language')->getId();
		$db =& $service->get('Db');
		$success = false;
		if (isset($this->data[$type])) {
			$src = $this->data[$type];

			//1. Change title for item in current language only
			$success = $db->query('UPDATE `'.$db->prefix($src['table']).'` '.
				'SET `'.$src['title'].'` = \''.addslashes($title).'\' '.
				'WHERE `'.$src['id'].'` = \''.$id.'\' AND `'.$src['table'].'_language` = '.$lang.$this->addSiteCriteria($src['table']));
		}
		return $success;
	}

	/**
	 * Deletes a tree node and sub nodes
	 *
	 * @public
	 *
	 * @return $success
	 */
	function delete($id,$type='folder') {
		global $service;
		$parent = addslashes($parent);
		$id = addslashes($id);
		$position = intval($position);
		$db =& $service->get('Db');
		$success = true;
		$ids = array($id);
		if ($type == 'folder' && isset($this->data['folder'])) {
			if ($id < 2) return false;
			$success = true;
			$src = $this->data['folder'];

			//Check if this is the only top-level folder left...deny deletion if it is.
			$folder = $db->query('SELECT * FROM `'.$db->prefix($src['table']).'` WHERE `'.$src['id'].'`=\''.$id.'\'');
			if (is_array($folder)) {
				$folder = $folder[0];
				if ($folder[$src['parent']] == 1) {
					//Top level folder found.
					$lang = $service->get('Language')->getId();
					$count = $db->query('SELECT count(`'.$src['id'].'`) AS count FROM `'.$db->prefix($src['table']).'` '.
					'WHERE `'.$src['parent'].'`= \'1\' AND `'.$src['table'].'_language` = '.$lang);
					$count = $count[0]['count'];
					if ($count <= 1) return false;
				}
			}

			//Get all child ids in lower levels until there is no more child levels
			$idpool = array($id);
			while($ids = $this->getChildrenIds($ids,$type)) {
				if ($ids && is_array($ids)) $idpool = array_merge($idpool,$ids);
			}
			//Erase all child folders
			$ret = $db->query('DELETE FROM `'.$db->prefix($src['table']).'` WHERE `'.$src['id'].'` '.
				'IN (\''.implode('\',\'',$idpool).'\')'.$this->addSiteCriteria($src['table']));
			if ($ret == false && $ret != array()) $success = false;
		}
		//Erase child leafs too if their parent is the deleted node or a child of it
		if (isset($this->data['leaf'])) {
			$src = $this->data['leaf'];
			if (isset($idpool)) {
				$ret = $db->query('DELETE FROM `'.$db->prefix($src['table']).'` WHERE `'.$src['parent'].'` '.
					'IN (\''.implode('\',\'',$idpool).'\')'.$this->addSiteCriteria($src['table'])) && $success;
			}else {
				$ret = $db->query('DELETE FROM `'.$db->prefix($src['table']).'` WHERE `'.$src['id'].'` = \''.$id.'\''.
					$this->addSiteCriteria($src['table'])) && $success;
			}
			if ($ret == false && $ret != array()) $success = false;
		}

		return $success;
	}

	/**
	 * Get all child ids of one or more parents
	 *
	 * @public
	 *
	 * @return array IDs of childs
	 */
	function getChildrenIds($parents,$type) {
		global $service;
		if (!is_array($parents)) $parents = array($parents);
		foreach($parents as $k => $v) $parents[$k] = addslashes($v);
		$db =& $service->get('Db');
		$ids = array();
		if (isset($this->data[$type])) {
			$src = $this->data[$type];
			$ids = $db->query('SELECT `'.$src['id'].'` FROM `'.$db->prefix($src['table']).'` '.
				'WHERE `'.$src['parent'].'` IN (\''.implode('\',\'',$parents).'\')'.$this->addSiteCriteria($src['table']));
			if ($ids){
				foreach($ids as $k => $v){
					$ids[$k] = $v[$src['id']];
				}
				$ids = array_unique($ids);
			}

		}
		if (count($ids) == 0) $ids = false;
		return $ids;
	}

	/**
	 * Adds a site condition at the end of all requests
	 */
	public function addSiteCriteria($table,$context='SELECT') {
		global $service;
		switch (strtoupper($context)) {
			case 'SELECT' : {
				return ' AND `'.$table.'_site` IN(\'0\',\''.$service->get('Site')->getId().'\')';
			}break;
			case 'INSERT' : {
				return ',\''.$service->get('Site')->getId().'\'';
			}break;
		}
	}

	/**
	 * Builds an allowed content types string for an instance of jstree
	 *
	 * @access public
	 *
	 * @return string Allowed content types
	 */
	public function getContentTypes($type){
		global $service;
		$infos = $service->get('Ressource')->getInfos($type);
		$treecontenttypes = '';
		$sectionIndex = 0;
		foreach ($infos as $k => $v) {
			if ($v['type'] != 'folder' && $v['type'] != 'section') {
				$treecontenttypes .= '\''.$v['type'].'\' : {\'valid_children\' : \'none\',\'icon\' : {\'image\' : \''.URL.$service->get('Ressource')->getIcon($v).'\'}},';
			}else {
				$sectionIndex = $k;
			}
		}
		$treecontenttypes.= '\'folder\' : {\'valid_children\' : ['.$validchilds.'\'folder\'],\'icon\' : {\'image\' : \''.URL.$service->get('Ressource')->getIcon($infos[$sectionIndex]).'\'}}';
		return $treecontenttypes;
	}
}
