<?php
/**
 * Image Effect
 *
 * Helper class for common CSS image effects
 *
 * May 6, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */
class ImageEffect {
    private $img;
    
    public function ImageEffect($img = ''){
        $this->img = $img;
    }

    public function __construct($img = '') {
        $this->ImageEffect($img);
    }
    
    public function options(){
        return array(
            ['value' => 'none', 'title' => _NONE],
            ['value' => 'dotted', 'title' => _EFFECT_DOTTED],
            ['value' => 'darken', 'title' => _EFFECT_DARKEN],
            ['value' => 'darken-80percent', 'title' => _EFFECT_DARKEN.' (80%)'],
            ['value' => 'darken-70percent', 'title' => _EFFECT_DARKEN.' (70%)'],
            ['value' => 'darken-60percent', 'title' => _EFFECT_DARKEN.' (60%)'],
            ['value' => 'darken-50percent', 'title' => _EFFECT_DARKEN.' (50%)'],
            ['value' => 'darken-40percent', 'title' => _EFFECT_DARKEN.' (40%)']
        );
    }
    
    public function bg($effect) {
        $bg = 'url("'.UPLOADURL.$this->img.'");';
        switch($effect) {
			case 'dotted' : {
				$bg = 'background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAADCAYAAABWKLW/AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAJklEQVQI1yXBsRUAEBAFsHwKnc10JjXePYVCEixfNQyZBz3YuKgHPLQEYXmc15IAAAAASUVORK5CYII=), '.$bgi;
			} break;
			case 'darken' : {
				$bg = 'background-image: linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), '.$bg;
			} break;
			case 'darken-80percent' : {
				$bg = 'background-image: linear-gradient(rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0.8)), '.$bg;
			} break;
			case 'darken-70percent' : {
				$bg = 'background-image: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), '.$bg;
			} break;
			case 'darken-60percent' : {
				$bg = 'background-image: linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), '.$bg;
			} break;
			case 'darken-50percent' : {
				$bg = 'background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), '.$bg;
			} break;
			case 'darken-40percent' : {
				$bg = 'background-image: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), '.$bg;
			} break;
            case 'normal' : 
            default: {
                $bg = 'background-image: '.$bg;
            }break;
		}
        return $bg;
    }
}