<?php
/**
 * Menu class
 *
 * Defines site menu classes (MenuData, MenuModel, MenuStore and Menu)
 *
 * Oct 30, 2012
 *
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('ressource')->get('core/db/model');

class MenuModel extends Model {
	public function __construct() {
		$this->MenuModel();
	}

	public function MenuModel(){
		global $service;
		$this->type = 'menu';
		$this->initVar('menu_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('menu_objid',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('menu_component',DATATYPE_STRING);
		$this->initVar('menu_parent',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('menu_position',DATATYPE_INT);
		$this->initVar('menu_type',DATATYPE_STRING);
		$this->initVar('menu_title',DATATYPE_STRING);
		$this->initVar('menu_link',DATATYPE_STRING);
		$this->initVar('menu_image',DATATYPE_STRING);
		$this->initVar('menu_options',DATATYPE_ARRAY);
		$this->initVar('menu_groups',(DB_UUIDS)?DATATYPE_UUIDARRAY:DATATYPE_INTARRAY);
		$this->initVar('menu_site',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('menu_language',DATATYPE_INT);
		$this->initVar('menu_created',DATATYPE_INT);
		$this->initVar('menu_modified',DATATYPE_INT);
		$this->initVar('menu_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->setOption('usecache',true);
	}
}

class MenuData extends Data{
	function __construct(){
        parent::__construct('menu');
	}
}

class MenuStore extends Store{
	public function __construct(){
		parent::__construct('menu');
	}
}

class Menu extends BaseClass {

	/**
	 * Stores the menu data
	 *
	 * @private
	 * @var MenuData $data
	 */
	protected $data;

	/**
	 * Stores widget infos
	 *
	 * @private
	 * @var array $infos
	 */
	protected $infos;

	/**
	 * Menu PHP5 constructor
	 *
	 * @public
	 * @return void Stores menu info
	 */
	public function __construct($menuData=array()){
		$this->Menu($menuData);
	}

	/**
	 * Menu PHP4 constructor
	 *
	 * @public
	 * @return void Stores Menu info
	 */
	public function Menu($menuData=false){
		$this->setData($menuData);
		$this->init();
	}

	/**
	 * Gets the menu data
	 *
	 * @public
	 *
	 * @return array The menu data
	 */
	public function getData(){
		return $this->data;
	}

	/**
	 * Sets the menu data
	 *
	 * @public
	 *
	 * @return array The widget data
	 */
	public function setData($data){
		if (is_a($data,'MenuData')) $this->data = $data;
	}

	/**
	 * Returns an array containing menu informations
	 *
	 * @public
	 * @param string $info The information needed
	 * @return mixed Returns the infos array or just one info if set
	 */
	public function getInfo($info=''){
		if (isset($this->infos[$info])) return $this->infos[$info];
		return $this->infos;
	}

	/**
	 * Sets men informations
	 *
	 * @protected
	 * @param mixed $info The information
	 * @return void
	 */
	protected function setInfo($info='',$value=''){
		if (is_array($info)) $this->infos = $info;
		else $this->infos[$info] = $value;
	}

	/**
	 * Initialize the menu object mostly by filling the infos array
	 *
	 * @public
	 * @return void
	 */
	public function init(){
		$this->infos = array(
			'type' => '',
			'title' => '',
			'description' => '',
			'icon' => '',
			'saveoptions' => array()
		);
	}

	/**
	 * Renders this menu
	 *
	 * @public
	 * @return array Rendered menu
	 */
	public function render(){}

	/**
	 * Lets the menu file add its own form fields to the menu edit form.
	 *
	 * @public
	 *
	 * @param array $obj One or more objects beeing edited with same objid
	 * @param Form $form The edit form, add form fields & return it back.
	 * @return Form The form to display.
	 */
	public function edit($obj,$form){
		return $form;
	}

	/**
	 * Save handler function called on menu updates
	 *
	 * Most of the time this method will not be implemented as one
	 * should use getOptions and send all widget options to be saved
	 * by the widgetmanager
	 *
	 * @public
	 *
	 * @return bool $success
	 */
	public function save(){
		return true;
	}
}
?>
