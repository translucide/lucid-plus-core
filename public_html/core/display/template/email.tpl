<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>{:$title:}</title>
	<style type="text/css">
		#outlook a {padding:0;}
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
		.ExternalClass {width:100%;} 
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		table td {}
	</style>
</head>
<body align="center" bgcolor="#FFFFFF">
<style type="text/css">
a, a:active, a:visited, a:hover {text-decoration:none;}
</style>
<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable"><tr><td>
<table cellspacing="0" cellpadding="0" border="0" width="650" style="width:650px; mso-table-lspace:0pt; mso-table-rspace:0pt;" align="center">
	{:if $onlinelink:}
	<tr valign="top">
		<td align="center">
			<font color="#565656" size="1" face="Arial">
			{:$onlinelink:}
		</td>
	</tr>
	{:/if:}
	<tr valign="top">
		<td align="center" width="650">
		</td>
	</tr>
	<tr valign="top">
		<td>
			<table cellpadding="10" cellspacing="20" border="0" id="text" width="100%">
				<tr valign="top">
					<td>
						<font color="#000" size="4" face="Arial">
							<b>{:$title:}</b>
						</font><br><br>
						<font color="#333" size="2" face="Arial">
							{:$content:}
						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	{:if $unsubscribe:}
	<tr>
		<td align="center"><font color="#565656" size="1" face="Arial"><br>
		{:$unsubscribe:}
	</tr>
	{:/if:}
</table></td></tr></table>
</body>
</html>