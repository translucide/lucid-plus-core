<?php
/**
 * Template class to represent a template.
 *
 * This class is a front end to supported templating engines in Lucid+
 *
 * Defines a template
 *
 * June 23, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
class Template extends BaseClass {

	/**
	 * Template type in use
	 *
	 * @public
	 *
	 * @var string $type
	 */
	private $type;

	/**
	 * Hold a reference to a smarty template
	 *
	 * @private
	 *
	 * @var Smarty $smarty
	 */
	private $handler;

	/**
	 * Holds the template file location
	 *
	 * @private
	 *
	 * @var string $template
	 */
	private $template;

	/**
	 * Template constructor
	 *
	 * @public
	 *
	 * @param string $file
	 * @param string $type
	 * @return void Template()
	 */
	public function __construct($file='',$type='smarty'){
		$this->type = $type;
		switch(strtolower($this->type)) {
			case 'smarty' : {
				global $service;
				$service->get('Ressource')->get('lib/smarty/3.1.12/Smarty.class');
				$this->handler = new Smarty();
				$this->handler->left_delimiter = '{:';
				$this->handler->right_delimiter = ':}';
				$this->handler->compile_check = true; //Check tpl for modifs ?

				//This forces Smarty to (re)compile templates on every invocation.
				if ($service->get('Setting')->get('developmentmode')) {
					$this->handler->force_compile = true; //Check tpl for modifs ?
				}

				$this->handler->setCompileDir(DATAROOT.'cache/smarty/compile');
				$this->handler->setCacheDir(DATAROOT.'cache/smarty/cache');
				$this->handler->setConfigDir(DATAROOT.'cache/smarty/config');
				if ($file != '') $this->setTemplate($file);
			}break;
		}
		$this->assignSystemVars();
	}

	/**
	 * Sets the template to use.
	 *
	 * By default we assume the template is a file. You can define
	 * The template source by prepending src:, db: or file: to the template path.
	 *
	 * @access public
	 *
	 * @param string $tpl
	 * @return void
	 */
	public function setTemplate($tpl){
		if (substr($tpl,0,4) == 'src:' || substr($tpl,0,3) == 'db:' || substr($tpl,0,5) == 'file:'){
			$this->Template = $tpl;
		}else {
			$this->template = 'file:'.$tpl;
		}
	}

	/**
	 * Renders a given template
	 *
	 * @public
	 *
	 * @return string Rendered template output.
	 */
	public function render($file='') {
		global $service;
		if ($file != '') $this->setTemplate($file);
		$file = $this->template;
		$tplswitch = explode(':',$file);
        if (substr($tplswitch[1],0,1) != '/') {
            $file = $tplswitch[0].':'.ROOT.$tplswitch[1];
        }

        $tploverride = THEMEROOT.$service->get('Setting')->get('theme').'/templates/'.str_replace(ROOT,'',$tplswitch[1]);
        if (file_exists($tploverride) && !is_dir($tploverride)) {
            $file = $tplswitch[0].':'.$tploverride;
        }

        if (is_object($this->handler)) {
			switch(strtolower($this->type)) {
				case 'smarty' : {
					return $this->handler->fetch($file);
				}break;
			}
		}
		else {
			global $service;
			$log =& $service->get('log');
			$log->add('Template::render ('.get_called_class().') Unknown template type '.$this->type.'. Please use a template type that is known to the system.',__FILE__,__LINE__,LOG_ERR);
		}
        echo 'render=ok'."\n";

	}

	/**
	 * Assigns a template variable
	 *
	 * @public
	 *
	 * @param string $var
	 * @param mixed $value
	 * @return void assign()
	 */
	public function assign($var,$value) {
		if (is_object($this->handler)) {
			switch(strtolower($this->type)) {
				case 'smarty' : {
					$this->handler->assign($var,$value);
				}break;
			}
		}
		else {
			global $service;
			$log =& $service->get('log');
			$log->add('Template::render ('.get_called_class().') Unknown template type '.$this->type.'. Please use a template type that is known to the system.',__FILE__,__LINE__,LOG_ERR);
		}
	}

	/**
	 * Alias of assign method
	 *
	 * @public
	 *
	 * @param string $var
	 * @param mixed $value
	 * @return void assign()
	 */
	public function setVar($var,$value) {
		$this->assign($var,$value);
	}

	private function assignSystemVars(){
		global $service;
		$this->assign('url',URL);
		$this->assign('root',ROOT);
		$this->assign('uploadurl',UPLOADURL);
		$this->assign('uploadroot',UPLOADROOT);
        $this->assign('themeurl',$service->get('Ressource')->getThemeUrl($service->get('Url')->isAdmin()));
        $this->assign('themeroot',THEMEROOT);
		$this->assign('userid',(($service->get('User')->isAnonymous())?0:$service->get('User')->getUid()));
		$this->assign('islogged',$service->get('User')->isLogged());
		$this->assign('isadmin',$service->get('User')->isAdmin());
		$this->assign('useremail',$service->get('User')->get('email'));
		$this->assign('username',$service->get('User')->get('name'));
		$this->assign('langid',$service->get('Language')->getId());
		$this->assign('lang',$service->get('Language')->getCode());
		$this->assign('sitename',$service->get('Setting')->get('sitename'));

		$url = $service->get('Url')->get();
		if (substr($url['path'],0,1) == '/') $url['path'] = substr($url['path'],1);
		$this->assign('referer',str_replace('/','+',$url['path']));
		$this->assign('pageurl',$url['path']);
	}
}

/**
 * Template model
 *
 * Template model for storing templates in DB
 */
class TemplateModel extends Model {
	function __construct(){
		$this->initVar('template_id',DATATYPE_INT);
		$this->initVar('template_objid',DATATYPE_INT);
		$this->initVar('template_type',DATATYPE_STRING,array('length' => 20));
		$this->initVar('template_name',DATATYPE_STRING,array('length' => 100));
		$this->initVar('template_src',DATATYPE_STRING);
		$this->initVar('template_site',DATATYPE_INT);
		$this->initVar('template_created',DATATYPE_INT);
		$this->initVar('template_modified',DATATYPE_INT);
		$this->initVar('template_modifiedby',DATATYPE_INT);
		$this->type = 'template';
	}
}
class TemplateData extends Data {
	function __construct(){
		parent::__construct('template');
	}
}
class TemplateStore extends Store {
	function __construct(){
		parent::__construct('template');
	}
}
?>
