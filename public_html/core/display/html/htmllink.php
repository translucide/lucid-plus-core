<?php
/**
 * Link class
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */
global $service;
$service->get('Ressource')->get('core/display/html/htmlobject');
class HtmlLink extends HtmlObject{
    public function open(){
        $link = $this->attr('href');
        if ($link == '') return '';
        
        //Find a target
        $target = $this->attr('target');
        if ($this->attr('target') == '') {
            $target = "_blank";
            if (strpos($link,URL) !== false ||
                (substr($link,0,2) != '//' && substr($link,0,7) != 'http://' && substr($link,0,8) != 'https://')) $target =  "_self";            
        }
        
        //Find a title
        $title = $this->attr('title');
        if ($title == '') $title = ucfirst(strtolower(str_replace(array('-','_'),' ',substr($link,strrpos($link,'/')))));
        
        return '<a href="'.$this->attr('href').'" target="'.$target.'" title="'.$title.'" class="'.$this->attr('class').'">';
    }
    
    public function close(){
        return ($this->attr('href') == '')?'':'</a>';
    }}
?>