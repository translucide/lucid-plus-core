<?php
/**
 * HtmlObject base class
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

class HtmlObject{
    private $attr;
    private $options;
    private $innerHtml;
    
    public function __construct(){
        $this->HtmlObject();
    }
    public function HtmlObject() {
        $this->attr = $this->options = array();
        $this->innerHtml = '';
    }
    
    /**
     * Gets/Sets an attribute
     *
     * @access public
     * @param string $name Attribute name
     * @param mixed $value Attribute value
     * @return mixed Attribute value (getter context) or void (setter context)
     */
    public function attr($name,$value=null){
        if ($value === null) return $this->attr[$name];
        else {
            $this->attr[$name] = $value;
        }
        return $this;
    }
    
    /**
     * Gets/Sets an option
     *
     * @param string $name Option name
     * @param mixed $value Option value
     * @return mixed Option value or void.
     */
    public function options($name,$value=null){
        if ($value === null) return $this->attr[$name];
        else {
            $this->attr[$name] = $value;
        }
        return $this;
    }
    
    public function render(){
        return $this->open().$this->innerHtml.$this->close();
    }
    
    public function open(){
        return '';
    }
    
    public function close(){
        return '';
    }
}
?>