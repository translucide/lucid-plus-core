<?php
/**
 * Link class
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */
global $service;
$service->get('Ressource')->get('core/display/html/htmlobject');
class HtmlImage extends HtmlObject{
    public function open(){
        $link = $this->attr('src');
        if ($link == '') return '';
        
        if (substr($link,0,2) != '//' && substr($link,0,7) != 'http://' && substr($link,0,8) != 'https://') {
            $link = UPLOADURL.$link;
        }
        
        //Find an alternate text for image
        $alt = $this->attr('alt');
        if ($alt == '') {
            $fname = substr($link,strrpos($link,'/')+1);
            $fname = substr($fname,0,strrpos($fname,'.')-1);
            $title = ucfirst(strtolower(str_replace(array('-','_'),' ',$fname)));
        }
        
        //Find an alternate text for image
        $title = $this->attr('title');
        
        return '<img src="'.str_replace(' ','%20',$link).'" alt="'.$alt.'" title="'.$title.'" class="'.$this->attr('class').'">';
    }
    
    public function close(){
        return '';
    }}
?>