<?php
/**
 * Thumbnail class
 *
 * Handles thumbnails creation and substitution of image src
 * for any thumbnail size available
 *
 * Oct 30, 2012
 *
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class Thumbnail {
	public function generateThumbnails($source){
        global $service;
        $service->get('Ressource')->get('lib/phpthumb/1.7.13/phpthumb.class');
		$phpThumb = new phpThumb();
		$phpThumb->config_disable_debug = true;
		$sizes = array(
			explode(',',$service->get("Setting")->get("tinypicturesize")),
			explode(',',$service->get("Setting")->get("smallpicturesize")),
			explode(',',$service->get("Setting")->get("mediumpicturesize")),
			explode(',',$service->get("Setting")->get("largepicturesize")),
			explode(',',$service->get("Setting")->get("enlargedpicturesize"))
		);
		$sizesnames = array(
			'tiny',
			'small',
			'medium',
			'large',
			'enlarged'
		);
		$path = `which convert`;
		$dir = substr(UPLOADROOT.$source,0,strrpos(UPLOADROOT.$source,'/')).'/thumb';
		foreach ($sizes as $k => $size) {
			$phpThumb->resetObject();
			$phpThumb->setSourceFilename(UPLOADROOT.$source);
			$phpThumb->setParameter('config_document_root', UPLOADROOT);
			$phpThumb->setParameter('config_cache_directory', $dir);
			$phpThumb->setParameter('w', $size[0]);
			$phpThumb->setParameter('h', $size[1]);
			if ($sizesnames[$k] != 'enlarged') $phpThumb->setParameter('zc', 'C');
			//$phpThumb->setParameter('config_output_format', 'jpeg');
			$phpThumb->setParameter('config_imagemagick_path', ($path)?$path:'usr/local/bin/convert');

			//Generate & output thumbnail
			$ext = substr($source,strrpos($source,'.'));
			$output_filename = dirname(UPLOADROOT.$source).'/thumb/'.substr(basename($source),0,strlen($ext)*-1).'_'.$sizesnames[$k].$ext;
			if ($phpThumb->GenerateThumbnail()) {
				$phpThumb->RenderToFile($output_filename);
				$phpThumb->purgeTempFiles();
			}
		}
		if (!$phpThumb->config_disable_debug) {
			$debug = $phpThumb->phpThumbDebug();
			if (is_array($debug)) $debug = print_r($debug,true);
			$service->get('Log')->log($debug,__FILE__,__LINE__);
		}
	}

    /**
     * Converts a file to any size.
     * @param  string $file [description]
     * @param  string $size [description]
     */
    public function toSize($file, $size='small') {
        $f = $file;
        $exploded = explode('/',$file);
        $exploded[] = $exploded[count($exploded)-1];
        $exploded[count($exploded)-2] = 'thumb';
        $path = implode('/',$exploded);
        $ext = substr($path,strrpos($path,'.'));
        $path = substr($path,0,strlen($ext)*-1).'_'.$size.$ext;
        if (file_exists(UPLOADROOT.$path)){
            return $path;
        }
        return $f;
    }

    public function changeSize($dir, $files, $size='small') {
        if (file_exists(UPLOADROOT.$dir.'/thumb')){
            foreach ($files as $k=> $v){
                $ext = substr($v,strrpos($v,'.'));
                $files[$k] = 'thumb/'.substr($v,0,strlen($ext)*-1).'_'.$size.$ext;
            }
        }
        return $files;
    }
}
