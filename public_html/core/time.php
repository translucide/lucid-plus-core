<?php
/**
 * Base class to handle time and dates.
 *
 * Handles time, timers and dates
 *
 * June 23, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class Time extends BaseClass {

	/**
	 * Timers
	 *
	 * @access private
	 *
	 * @var array $timers
	 */
	private $timers;

	/**
	 * Class constructor
	 *
	 * @access public
	 *
	 * @return void Time()
	 */
	public function __construct(){
		$this->timers = array();
	}
	/**
	 * Starts a timer
	 *
	 * @access public
	 *
	 * @var string Timer name
	 * @return void startTimer()
	 */
	function startTimer($timerName,$timerValue=0) {
		if ($timerValue == 0) $timerValue = microtime(true);
		$this->timers[$timerName] = $timerValue;
	}

	/**
	 * Stop a timer, gets the time elapsed
	 *
	 * @access public
	 *
	 * @var string Timer name to stop
	 * @return int Time elapsed in microseconds
	 */
	function stopTimer($timerName){
		global $service;
		$log =& $service->get('log');
		$timerValue = round(microtime(true) - $this->timers[$timerName],5);
		$timerTitle = $timerName;
		switch($timerName) {
			case 'sys.content' : $log->add(sprintf(LOG_SHUTDOWN_CONTENTTIME,$timerValue,$_SERVER['REQUEST_URI']),__FILE__,__LINE__,E_INFO, LOG_FILE_STATS);break;
			case 'sys.widget' : $log->add(sprintf(LOG_SHUTDOWN_WIDGETTIME,$timerValue,$_SERVER['REQUEST_URI']),__FILE__,__LINE__,E_INFO, LOG_FILE_STATS);break;
			case 'sys.all' : $log->add(sprintf(LOG_SHUTDOWN_LOADTIME,$timerValue,$_SERVER['REQUEST_URI']),__FILE__,__LINE__,E_INFO, LOG_FILE_STATS);break;
			case 'sys.boot' : $log->add(sprintf(LOG_SHUTDOWN_BOOTTIME,$timerValue,$_SERVER['REQUEST_URI']),__FILE__,__LINE__,E_INFO, LOG_FILE_STATS);break;
			case 'sys.output' : $log->add(sprintf(LOG_SHUTDOWN_OUTPUTRENDERING,$timerValue,$_SERVER['REQUEST_URI']),__FILE__,__LINE__,E_INFO, LOG_FILE_STATS);break;
			default: if ($timerName != 'db.query') $log->add(sprintf(LOG_SHUTDOWN_DEFAULT,$timerName,$timerValue,$_SERVER['REQUEST_URI']),__FILE__,__LINE__,E_INFO, LOG_FILE_STATS);break;
		}
		unset($this->timers[$timerName]);
		return $timerValue;
	}

	/**
	 * Converts a unix GMT timestamp to the user time
	 *
	 * @access public
	 *
	 * @var int The GMT timestamp to convert to user time.
	 * @return int The timestamp converted to the user time.
	 */
	function toUserTime($timestamp) {
		global $service;
		$tz = $service->get('User')->get('timezone');
		if ($tz) {
			$usertz = new DateTimeZone($tz);
			$gmttz = new DateTimeZone("UTC");
			$usertime = new DateTime('now', $usertz);
			$gmttime = new DateTime('now', $gmttz);
			$timeOffset = $usertz->getOffset($gmttime);
			$timestamp += $timeOffset;
		}
		return $timestamp;
	}

	/**
	 * Converts a unix GMT timestamp to the server time
	 *
	 * @access public
	 *
	 * @var int The user timestamp to convert to server time.
	 * @return int The timestamp converted to the user time.
	 */
	function toServerTime($timestamp) {
		global $service;
		$tz = (SERVERTIMEZONE != '')?'UTC':'';
		if ($tz) {
			$usertz = new DateTimeZone($tz);
			$gmttz = new DateTimeZone("UTC");
			$usertime = new DateTime('now', $usertz);
			$gmttime = new DateTime('now', $gmttz);
			$timeOffset = $usertz->getOffset($gmttime);
			$timestamp = $timestamp + $timeOffset;
		}
		return $timestamp;
	}

	/**
	 * Converts a unix GMT timestamp to a time elapsed string
	 *
	 * @access public
	 *
	 * @var int $time The user Time stamp
	 * @var int $precision The number of units to display: 2 means hours and minutes, or for an old item, years and months for example
	 * @return string A string that represents the time elapsed.
	 */
	function toElapsedTimeString($time,$precision=1) {
		if ($time == 0) return _NEVER;

		$now = new DateTime;
		$ago = new DateTime(date("Y-m-d H:i:s",$time));
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$string = array(
			'y' => _YEAR,
			'm' => _MONTH,
			'w' => _WEEK,
			'd' => _DAY,
			'h' => _HOUR,
			'i' => _MINUTE,
			's' => _SECOND,
		);
		$strings = array(
			'y' => _YEARS,
			'm' => _MONTHS,
			'w' => _WEEKS,
			'd' => _DAYS,
			'h' => _HOURS,
			'i' => _MINUTES,
			's' => _SECONDS,
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . ($diff->$k > 1 ? $strings[$k] : $v);
			} else {
				unset($string[$k]);
			}
		}

		$string = array_slice($string, 0, $precision);
		if ($string) {
			return sprintf(_TIMEAGO,implode(', ', $string));
		}
		return _NOW;
	}

}
?>
