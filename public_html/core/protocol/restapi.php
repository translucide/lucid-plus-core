<?php
/**
 * Generic REST Api base class
 *
 * June 15, 2017
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2017 Translucide
 * @license
 * @since 		0.1
 */
  
class RestAPI {
    /**
     * Last operation error code
     *
     * @access protected
     * @var int $error
     */
    protected $error = null;
    
    /**
     * Last operation error message
     *
     * @access protected
     * @var string $message
     */
    protected $message = null;

    /**
     * API Endpoint
     *
     * @access protected
     * @var $endpoint
     */
    protected $endPoint = null;
    
    /**
     * API Key
     *
     * @access protected;
     * @var string $apiKey
     */
    protected $apiKey = null;

    /**
     * Sets the post/put/patch/delete data encoding scheme:
     * either 'querystring' or 'json'
     *
     * @access protected
     * @var string $encoding;
     */
    protected $encoding = null;
    
    /**
     * API constructor
     *
     * @access public
     * @param string $apiKey API Key for access
     * @param string $endPoint API Endpoint
     * @param string $encoding Data encoding for POST/PUT/DELETE/PATCH Http verbs.
     */
    public function __construct($endPoint=null, $apiKey=null,$encoding='json'){
        $this->apiKey = $apiKey;
        $this->endPoint = $endPoint;
        $this->encoding = $encoding;
    }
    
    /**
     * Checks if last request sent returned an error
     *
     * Returns TRUE when an error occured, false otherwise
     *
     * @access public
     * @return bool $error
     */
    public function isSuccessful() {
        return is_null($this->error);
    }
    
    /**
     * Returns the last request error code
     *
     * @access public
     * @return int Error code
     */
    public function getError(){
        return $this->error;
    }
    
    /**
     * Returns the last request error message
     *
     * @access public
     * @return string Error message
     */
    public function getMessage(){
        return $this->message;
    }

    /**
     * Resets errors from previous calls
     *
     * @access protected
     * @return void
     */
    protected function resetError(){
        $this->error = $this->message = null;
    }
    
    /**
     * Sets encoding of data sent to remote API
     *
     * @access public
     * @param string $e Either 'querystring' or 'json'
     * @return void
     */
    public function setEncoding($e = 'querystring') {
        $this->encoding = ($e == 'querystring' || $e == 'json')?$e:'json';
    }
    
    /**
     * Sends the request
     *
     * @access protected
     *
     * @param string $method GET,POST,PUT,DELETE,...
     * @param string $call URL to which to send the call.
     * @param array $input Input variables
     * @param array $headers Extra headers. Accept: and ContentType: headers are already set.
     * @return array $result
     */
    protected function sendRequest($method,$call,$input=array(),$headers=array()) {
        $this->resetError();
        $curl = curl_init();
        $url = $this->endPoint.$call;
        $method = strtoupper($method);

        //Allow child class to modify input parameters
        $this->onInit($curl,$method,$call,$input,$headers);
        
        if (count($headers)) curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $encodedInput = ($this->encoding == 'json')?json_encode($input):http_build_query($input);
        switch($method) {
            case 'POST' : {
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS,$encodedInput);
            }break;
            case'GET' : {
                $url .= ((strrpos($url,'?') !== false)?'&':'?').http_build_query($input);
            }break;
            case 'PATCH':{
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($curl, CURLOPT_POSTFIELDS,$encodedInput);
            }break;
            case 'PUT':{
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($curl, CURLOPT_POSTFIELDS,$encodedInput);
            }break;
            case 'DELETE':{
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
                curl_setopt($curl, CURLOPT_POSTFIELDS,$encodedInput);
            }break;
            default:{
            }break;
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        $response = curl_exec($curl);
        $this->onResponse($response,$curl);
        curl_close($curl);
        return $response;
    }
    
    /**
     * Add/modify/remove inputs parameters and headers before API requests
     *
     * @access protected
     * @param resource $curl Curl Request about to be sent* 
     * @param string $method Http verb to use: GET, POST, PUT, etc.
     * @param string $call Call URL, without the endpoint base url.
     * @param array $input The input array
     * @param array $headers The headers array
     */
    protected function onInit(&$curl,&$method,&$call,&$input,&$headers){
        curl_setopt($curl, CURLOPT_PORT, 443);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);        
    }
    
    /**
     * Act on response received
     *
     * @access protected
     * @param string $response
     * @param resource $curl Curl Request about to be sent
     */
    protected function onResponse(&$response,&$curl){
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($httpcode != 200 && $httpcode != 201) {
            $this->error = $httpcode;
            $response = null;
        }
        else {
            if ($this->encoding == 'json') $response = json_decode($response,true);
            else $response = parse_str($response);
        }
        return $response;
    }
}
