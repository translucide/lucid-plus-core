<?php
/**
 * Class Sendmail
 *
 * Email class handler
 *
 * November 11, 2012
 *
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class SendmailEmailMethod implements EmailMethodInterface {
	function __construct(){}
	private $error;
	
	function send($options){
		$headers = 'from: ';
		$from = '';
		if ($options['from']['name'] != '') $from .= $options['from']['name'];
		else $from .= $options['from']['email'];
		$from .= ' <'.$options['from']['email'].'>';
		$headers .= $from."\n";
		
		if (!$options['viewrecipients']) $headers .= 'bcc: ';
		else $headers .= 'to: ';
		$i = 0;
		foreach ($options['to'] as $v) {
			if ($i > 0) $headers .= ', ';
			$headers .= (($v['name']!= '')?$v['name']:$v['email']).' <'.$v['email'].'>';
			$i++;
		}
		$headers .= "\n";
		if ($options['headers']) $headers .= "\n".implode("\n",$options['headers']);
		
		$to = $options['to']['email'];
		if ($options['to']['name'] != '') $to = $options['to']['name'].' <'.$to.'>';
		
		if ($options['viewrecipients']) $mail = mail($to,$options['subject'],$options['body'],$headers);
		else $mail = mail($from,$options['subject'],$options['body'],$headers);
		if (!$mail) $this->error = $mail;
		return $mail;
	}
	
	public function error(){
		return $this->error;
	}
}
