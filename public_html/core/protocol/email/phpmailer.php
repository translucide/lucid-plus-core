<?php
/**
 * Class PHPMailer
 *
 * Email class handler
 *
 * November 11, 2012
 *
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class PHPMailerEmailMethod implements EmailMethodInterface {
	function __construct(){}
	
	private $error;
	
	function send($options){
		global $service;
		$service->get('Ressource')->get('lib/phpmailer/class.phpmailer');
        $service->get('Ressource')->get('lib/phpmailer/class.smtp');            
		$error = "An email could not be sent for the following reason(s):\n";
		
		//1. Setup mail object
		$mail = new PHPMailer(true);
		if (defined('EMAIL_HOST')) {
            $mail->IsSMTP();
            $mail->SMTPAuth   = true;
            if (EMAIL_DEBUG) $mail->SMTPDebug  = 1;
            if (EMAIL_MODE != '') $mail->SMTPSecure = EMAIL_MODE;
            $mail->Host       = EMAIL_HOST;
            $mail->Port       = EMAIL_PORT;
            $mail->Username   = EMAIL_USER;
            $mail->Password   = EMAIL_PASS;
            if (EMAIL_SELFSIGNED) {
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );
            }
        }else {
            $mail->Mailer = 'sendmail';            
        }

        //Setup DKIM signed emails, if configured.
        if (EMAIL_DKIM) {
            $mail->DKIM_domain = EMAIL_DKIM_DOMAIN;
            $mail->DKIM_private = EMAIL_DKIM_PRIVATEKEY;
            $mail->DKIM_selector = EMAIL_DKIM_SELECTOR;
            $mail->DKIM_passphrase = EMAIL_DKIM_PASSPHRASE;
            $mail->DKIM_identity = (EMAIL_DKIM_FROM != '')?EMAIL_DKIM_FROM:$options['from']['email'];
        }
        
		try{
			$mail->CharSet = SYSTEM_LOCALE_CHARTSET;
			if ($options['html']) $mail->IsHTML(true);
			
			//2. Set to and from...
            //@note: removed setFrom to check if it makes emails get flagged as spam.
            if (filter_var($options['from']['email'], FILTER_VALIDATE_EMAIL)) {
                $mail->addReplyTo($options['from']['email'],$options['from']['name']);
            }
            if (defined('EMAIL_FROM')) $mail->SetFrom(EMAIL_FROM, EMAIL_NAME);
            else $mail->SetFrom($options['from']['email'], $options['from']['name']);
            
			foreach ($options['to'] as $k => $v) {
                if (filter_var($v['email'], FILTER_VALIDATE_EMAIL)) {
                    if (!$options['viewrecipients']) {
                        if ($k == 0) $mail->AddAddress($v['email'],$v['name']);
                        else $mail->AddAddress($v['email'],$v['name']);
                    }
                    else $mail->AddAddress($v['email'],$v['name']);
                }
			}

			if ($options['headers']) {
				foreach ($options['headers'] as $k => $v) $mail->AddCustomHeader($options['headers'][$k]);
			}
	
			$mail->Subject = $options['subject'];
			$mail->MsgHTML($options['body']);
			foreach($options['attachments'] as $v) {
				$mail->AddAttachment($v, substr($v,strrpos($v,'/')+1), 'base64', '', $disposition = 'attachment');	
			}
			$ret = $mail->Send();
		}catch (phpmailerException $e) {
			$error .= $e->errorMessage(); //Pretty error messages from PHPMailer
		}
		catch (Exception $e) {
			$error .= $e->getMessage(); //Boring error messages from anything else!
		}
		$error .= "\n".print_r(error_get_last()."\n\n".$mail->ErrorInfo, true);
		if ($ret == false) $this->error = $error;
		return $ret;
	}
	
	public function error() {
		return $this->error;
	}
}
