<?php
/**
 * Class Email
 *
 * Email class
 *
 * November 11, 2012
 *
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

interface EmailMethodInterface {
	function __construct();
	function send($options);
}

class Email {

	/**
	 * The email handler to use
	 *
	 * @private
	 * @var EmailMethod $handler
	 */
	private $handler;

	/**
	 * The email options
	 *
	 * @private
	 * @var $options
	 */
	private $options;

	private $error;

	/**
	 * The email constructor
	 *
	 * @public
	 * @return void Email()
	 */
	public function __construct($method='phpmailer') {
		$this->Email($method);
	}

	public function Email($method='phpmailer'){
		$this->options['viewrecipients'] = false;
		$this->setMethod($method);
	}

	/**
	 * Sets the TO field
	 *
	 * @public
	 * @param string $to
	 * @return void to()
	 */
	public function to($to,$name='') {
		$this->options['to'][] = array('email'=>$to,'name'=>$name);
	}

	/**
	 * Sets the FROM field
	 *
	 * @public
	 * @param string $from
	 * @return void from()
	 */
	public function from($from,$name='') {
		$this->options['from'] = array('email'=>$from,'name'=>$name);
	}

	/**
	 * Sets the subject
	 *
	 * @public
	 * @param string $subject
	 * @return void subject()
	 */
	function subject($subject) {
		$this->options['subject'] = $subject;
	}

	/**
	 * Sets the body
	 *
	 * @public
	 * @param string $body
	 * @return void body();
	 */
	function body($body) {
		$this->options['body'] = $body;
	}

	/**
	 * Whether to send email in HTML or text form
	 *
	 * @public
	 * @param bool $html
	 * @return void html()
	 */
	public function html($html= false){
		$this->options['html'] = $html;
	}

	/**
	 * Whether to allow recipients to view each others
	 * Send mail as BCC (false) or as normal TO (true)
	 *
	 * @public
	 * @param bool $view
	 * @return void ()
	 */
	public function viewRecipients($view=false) {
		$this->options['viewrecipients'] = $view;
	}

	/**
	 * Adds a mail header
	 *
	 * @public
	 * @param string $value
	 * @return void header()
	 */
	public function header($value){
		$this->options['headers'][] = $value;
	}

	/**
	 * Sets the email sending method to use
	 *
	 * @public
	 * @param string $method The email medhod : sendmail, smtp, etc.
	 * @return void setMethod
	 */
	public function setMethod($method){
        global $service;
        $service->get('Ressource')->get('core/protocol/email/'.$method);
		$this->options['method'] = $method;
		if (!class_exists(ucfirst($method).'EmailMethod')) {
			trigger_error('Email method named '.ucfirst($method).' should be defined in a class named '.ucfirst($method).'EmailMethod but that class was not found !');
		}else {
			$clsName = ucfirst($method).'EmailMethod';
			$this->handler = new $clsName;
		}
	}

	/**
	 * Sends the current mail
	 *
	 * @public
	 * @return bool $success
	 */
	public function send(){
		$ret = $this->handler->send($this->options);
		if (!$ret) $this->error = $this->handler->error();
		return $ret;
	}

	/**
	 * Adds an attachment to the mail
	 *
	 * @access public
	 *
	 * @param string $file Path to file
	 * @return void
	 */
	public function attach($file){
		if (file_exists($file)) {
			$this->options['attachments'][] = $file;
		}
	}

    /**
     * Queues the mail for sending later on
     *
     * @access public
     *
     * @return bool $success
     */
    public function queue(){
        global $service;
        $service->get('Ressource')->get('core/model/queue');
        $qStore = new QueueStore();
        $qItem = $qStore->create();
        $qItem->setVar('queue_component','');
        $qItem->setVar('queue_type','');
        $qItem->setVar('queue_name','');
        $qItem->setVar('queue_action','sendemail');
        $qItem->setVar('queue_status','0');
        $qItem->setVar('queue_data',$this->options['to']);
        $qItem->setVar('queue_options',$this->options);
        return $qStore->save($qItem);
    }

	public function error(){
		return $this->error;
	}

    public function setOptions($opt){
        $this->options = $opt;
    }
}
?>
