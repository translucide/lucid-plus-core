<?php
/**
 * Cache service.
 *
 * Manages cache: sets, gets and clears cache
 *
 * June 23, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
class Cache{
    /**
     * Cache location
     *
     * @access protected
     * @var string $cacheDir
     **/
    protected $cacheDir = DATAROOT.'cache/data/';
    
    /**
     * Initializes the Cache service
     *
     * @access public
     **/
    public function __construct(){
        if (!is_dir($this->cacheDir)) {
            mkdir($this->cacheDir,0777,true);
            if (!is_dir($this->cacheDir)) die("ERROR : Cannot create data cache folder at \"".$this->cacheDir."\"!");
        }
    }
    
    /**
     * Gets cached data
     *
     * @access public
     * @param string $key Cache key
     * @return mixed $data Cached data Null on error
     **/
    public function get($key){
        $file = $this->getLocation($key);
        $data = null;
        if (file_exists($file)) {
            $data = file_get_contents($file);
            $data = json_decode($data,true);
            if (is_array($data) && $data['expires'] < time()) {
                $data = null;
            }
            else $data = $data['data'];
        }
        return $data;
    }
    
    /**
     * Caches data, data will expire 1h from now
     *
     * @access public
     * @param string $key
     * @param mixed $data
     * @param int $expires
     **/
    public function set($key,$data,$expires=0){
        if ($expires == 0) $expires = time()+3600;
        $val = json_encode(array(
            'data' => $data,
            'expires' => $expires
        ));
        return file_put_contents($this->getLocation($key),$val);
    }
    
    /**
     * Clears the cache
     *
     * @access public
     * @param string $key cache key to clear
     * @return bool $success    
     **/
    public function clear($key=null){
        if ($key !== null) {
            $res = $res2 = unlink($this->getLocation($key));
        }else {
            $res = rmdir($this->cacheLoc);
            $res2 = mkdir($this->cacheLoc);
        }
        return ($res && $res2)?true:false;
    }
    
    /**
     * Returns the cache file location for $key
     *
     * @access protected
     * @return string $file Cache file location
     **/
    protected function getLocation($key) {
        global $service;
        $comp = $service->get('Route')->get();
        $com = $comp->getVar('route_component');
        if ($com == '') $com = 'core';
        else $com = 'com-'.$com;
        return $this->cacheDir.$com.'-'.$key.'.json';
    }
}