<?php
/**
 * Ressource manager class
 *
 * Manages ressources (php, js and css) paths, inclusion and more
 *
 * October 28, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

//Define ressource levels
define('RESSOURCE_FRAMEWORK',1);
define('RESSOURCE_PLUGIN',2);
define('RESSOURCE_SCRIPT',3);

define('RESSOURCE_PHP',0);
define('RESSOURCE_JS',1);
define('RESSOURCE_CSS',2);

class Ressource extends BaseClass{

	/**
	 * Files and code snippets to be included
	 *
	 * @access private
	 * @var array $ressources
	 */
	private $ressources;

	/**
	 * Ressource types cache
	 *
	 * @access private
	 * @var array $types
	 */
	private $types;

	/**
	 * Ressource priority cache
	 *
	 * @access private
	 * @var array $priority
	 */
	private $priority;

	/**
	 * Ressource options cache
	 *
	 * @access private
	 * @var array $options
	 */
	private $options;

	/**
	 * Ressources buffers
	 *
	 * @access private
	 * @var array $buffers
	 */
	private $buffer;

	public function __construct(){
		$this->ressources = array();
		$this->types = array();
		$this->priority = array();
		$this->options = array();
		$this->buffer = array();
	}

	/**
	 * Includes a file.
	 *
	 * Currently supports PHP, JS and CSS files
	 *
	 * @access public
	 *
	 * @param string $file
	 * @param mixed $params Int for priority only, otherwise an array: [
	 * 		'priority' => ...
	 * 		'integrity' => '',
	 * 		'crossorigin' => 'anonymous'
	 * 		]
	 * return void get($file);
	 */
	public function get($file, $params = RESSOURCE_PLUGIN){
		if (is_array($params)) {
			$params['priority'] = $priority = (isset($params['priority']))?$params['priority']:RESSOURCE_PLUGIN;
			$params['crossorigin'] = (isset($params['crossorigin']))?$params['crossorigin']:'anonymous';
		}
		else {
			$priority = $params;
			$params = array();
		}
		$processed = false;
		$types = array();
		$priorities = array();
		$basedir = '';
		$root = substr($file,0,6);
		if ($root == '__SR__') $file = SITEROOT.substr($file,6);
		if ($root == '__DR__') $file = DATAROOT.substr($file,6);
		if ($root == '__TR__') $file = THEMEROOT.substr($file,6);
		if (substr($file,0,1) != '/') $basedir = ROOT;
		$ext = strtolower(substr($file,strrpos($file,'.')));
		if (strpos($ext,"?") !== false) {
            $ext = substr($ext,0,strpos($ext,"?"));
        }
		if (strpos($ext,"#") !== false) {
            $ext = substr($ext,0,strpos($ext,"#"));
        }

		//1. External ressource: JS or CSS only
		if (substr($file,0,2) == '//' || substr($file,0,7) == 'http://' || substr($file,0,8) == 'https://') {
			if (in_array($file,$this->ressources) === false)  {
				if ($ext == '.js' || $ext == '.css') {
					$this->ressources[] = $file;
					$this->options[] = $params;
					$this->priority[] = $priority;
				}
				if ($ext == '.js') $this->types[] = RESSOURCE_JS;
				if ($ext == '.css') $this->types[] = RESSOURCE_CSS;
			}
			return;
		}

		//2. Single file with extension: JS, CSS, and PHP
		if (is_file($basedir.$file)) {
			if ($ext == '.php') {
				include_once($basedir.$file);
			}
			if ((in_array($basedir.$file,$this->ressources) === false) && ($ext == '.js' || $ext == '.css'))  {
				$this->ressources[] = $basedir.$file;
				$this->options[] = $params;
				$this->priority[] = $priority;
				$this->types[] = ($ext == '.js')?RESSOURCE_JS:RESSOURCE_CSS;
			}
			return;
		}

		//3. File without extension
		$isPHP = is_file($basedir.$file.'.php');
		$isJS = is_file($basedir.$file.'.js');
		$isCSS = is_file($basedir.$file.'.css');
		if (!$processed && ($isPHP || $isCSS || $isJS)) {
			if ($isJS && in_array($basedir.$file.'.js',$this->ressources) === false) {
				$this->types[] = RESSOURCE_JS;
				$this->priority[] = $priority;
				$this->options[] = $params;
				$this->ressources[] = $basedir.$file.'.js';
			}
			if ($isCSS && in_array($basedir.$file.'.css',$this->ressources) === false) {
				$this->types[] = RESSOURCE_CSS;
				$this->priority[] = $priority;
				$this->options[] = $params;
				$this->ressources[] = $basedir.$file.'.css';
			}
			if ($isPHP) {
				include_once($basedir.$file.'.php');
			}
		}

		//4. Folder content
		if (!$processed && is_dir($basedir.$file)) {
			$files = $this->find($basedir.$file);
			foreach($files as $k => $v) {
				$v = $basedir.$file.((substr($file,-1,1) == '/')?'':'/').$v;
				if (in_array($v,$this->ressources) === false) {
					if (strtolower(substr($v,-3,3)) == '.js') {
						$this->types[] = RESSOURCE_JS;
						$this->priority[] = $priority;
						$this->options[] = $params;
						$this->ressources[] = $v;
					}
					if (strtolower(substr($v,-4,4)) == '.css') {
						$this->types[] = RESSOURCE_CSS;
						$this->priority[] = $priority;
						$this->options[] = $params;
						$this->ressources[] = $v;
					}
				}
				if (strtolower(substr($v,-4,4)) == '.php') {
					include_once($v);
				}
			}
		}
	}

	/**
	 * Includes an inline script
	 *
	 * @access public
	 *
	 * @param string $script JS inline script
	 * @param bool $head Whether to include this JS before any other JS in the page.
	 * @return void Adds an inline script to the web page.
	 */
	function addScript($script, $head=false){
		if (in_array($script,$this->ressources) === false) {
			$this->ressources[] = $script;
			$this->priority[] = RESSOURCE_SCRIPT;
			$this->options[] = array();
			$this->types[] = RESSOURCE_JS;
		}
	}


	/**
	 * Includes an inline stylesheet
	 *
	 * @access public
	 *
	 * @param string $style CSS inline styles
	 * @return void Adds an inline stylesheet to the web page.
	 */
	function addStyle($style){
		if (in_array($style,$this->ressources) === false) {
			$this->ressources[] = $style;
			$this->priority[] = RESSOURCE_SCRIPT;
			$this->options[] = array();
			$this->types[] = RESSOURCE_CSS;
		}
	}

	function getStyle($com,$type,$name,$style) {
		if ($style != '' && file_exists($this->getThemePath()."css/$com/$type/$name/$style.css")) {
			$this->get($this->getThemePath()."css/$com/$type/$name/$style");
			$this->get($this->getThemePath()."js/$com/$type/$name/$style");
		}else {
			$this->get('com/'.$com.'/ressource/'.$type.'/'.$name);
			$this->get('com/'.$com.'/ressource/'.$type.'/'.$name.'/'.$style);
		}
	}

	/**
	 * Generate urls for HTML included ressources such as JS and CSS.
	 *
	 * @access public
	 *
	 * @return array Array of strings to include in the theme header.
	 */
	public function getUrls($outputlanguage){
		global $service;
        $packAllFiles = false;
		$inlines = '';
		$liburls = array();
		$dynamicurls = array();
        $lastmods = array();
        $defaultmoddate = mktime(0,0,0,date('m'),date('d'),date('Y'));
		//Sort ressources: separate files and files to concatenate of current type needed (JS or CSS)
		foreach ($this->ressources as $k => $v) {
			if (($this->types[$k] == RESSOURCE_CSS && $outputlanguage == OUTPUT_MODE_CSS) ||
				($this->types[$k] == RESSOURCE_JS && $outputlanguage == OUTPUT_MODE_JS)) {
				if ($this->priority[$k] != RESSOURCE_SCRIPT && strpos($v,'lib/') === false && substr($v,0,2) != '//' && substr($v,0,7) != 'http://' && substr($v,0,8) != 'https://') {
					//Do not allow SITEROOTs urls, but allow DATAROOT and THEMEROOT as well as ROOT based urls.
                    $file = str_replace(array(URL,/*SITESROOT,*/DATAROOT,THEMEROOT,ROOT),array(''/*,'__SR__'*/,'__DR__','__TR__',''),$v);
                    $f2 = ROOT.$file;
                    if (strpos($file,'__TR__') !== false) $f2 = str_replace('__TR__',THEMEROOT,$file);
                    if (strpos($file,'__DR__') !== false) $f2 = str_replace('__DR__',THEMEROOT,$file);
                    if ($packAllFiles) {
                        $dynamicurls[$this->priority[$k]] .= 'f[]='.urlencode($file).'&';
                        if (!isset($lastmods[$this->priority[$k]])) $lastmods[$this->priority[$k]] = $defaultmoddate;
                        $lastmods[$this->priority[$k]] = max($lastmods[$this->priority[$k]],filemtime($f2));
                    }
                    else {
                        $cur = count($dynamicurls[$this->priority[$k]]);
                        $dynamicurls[$this->priority[$k]][$cur] = urlencode($file);
                        if (!isset($lastmods[$this->priority[$k]][$cur])) $lastmods[$this->priority[$k]][$cur] = $defaultmoddate;
                        $lastmods[$this->priority[$k]][$cur] = max($lastmods[$this->priority[$k]][$cur],filemtime($f2));
                    }
				}else {
					if ($this->priority[$k] != RESSOURCE_SCRIPT) {
						$liburls[$this->priority[$k]][] = array_merge(
							$this->options[$k],
							array('file' =>str_replace(ROOT,URL,$v))
						);
					}
					else $inlines .= $v;
				}
			}
		}

		//Apply appropriate treatment to frameworks, plugins and scripts
		foreach($dynamicurls as $k => $v) {
			if ((!is_array($dynamicurls[$k]) && strlen($dynamicurls[$k]) > 0) || is_array($dynamicurls[$k])) {
                if ($packAllFiles) $liburls[$k][] = (($outputlanguage == OUTPUT_MODE_JS)?URL.'js.js?v='.($lastmods[$k]+1).'&':URL.'style.css?v='.($lastmods[$k]+1).'&').substr($dynamicurls[$k],0,-1);
                else  {
                    foreach($dynamicurls[$k] as $kd => $d) {
                        $liburls[$k][] = (($outputlanguage == OUTPUT_MODE_JS)?URL.'js.js?v='.($lastmods[$k][$kd]+1).'&f[]=':URL.'style.css?v='.($lastmods[$k][$kd]+1).'&f[]=').$dynamicurls[$k][$kd];
                    }
                }
            }
		}

		foreach ($liburls as $ku => $vu) {
			foreach($liburls[$ku] as $k => $v) {
				$data = array('type'=>'file');
				if (is_array($v)) {
					$data['src'] = $v['file'];
					if(isset($v['integrity'])) $data['integrity'] = $v['integrity'];
					if(isset($v['crossorigin'])) $data['crossorigin'] = $v['crossorigin'];
				}
				else $data['src'] = $v;
				$liburls[$ku][$k] = $data;
			}
		}
		if (!is_array($liburls[RESSOURCE_FRAMEWORK])) $liburls[RESSOURCE_FRAMEWORK] = array();
		if (!is_array($liburls[RESSOURCE_PLUGIN])) $liburls[RESSOURCE_PLUGIN] = array();
		if ($inlines != '') $inlines = array(array('type'=>'inline','src'=>$inlines));
		else $inlines = array();
		$urls = array_merge($liburls[RESSOURCE_FRAMEWORK],$liburls[RESSOURCE_PLUGIN],$inlines);
		return $urls;
	}

	/**
	 * Clears included ressources
	 *
	 * @access public
	 *
	 * @return void
	 */
	public function clear(){
		$this->ressources = array();
		$this->types = array();
		$this->priority = array();
		$this->options = array();
	}

	/**
	 * Creates a ressource buffer
	 *
	 * @access public
	 *
	 * @return void
	 */
	public function buffer($name = 'buffer'){
		$this->buffer[$name] = count($this->ressources);
	}

	/**
	 * Returns all ressources in an array, each line having 'type', 'priority', 'ressource' members.
	 *
	 * @access public
	 *
	 * @param mixed $type An integer or an array of integers representing ressources types to export.
	 * @param string $buffer The buffer name to use, empty for none.
	 * @return Array Ressources added so far
	 */
	public function export($type=null,$buffer='') {
		$ret = array();
		foreach ($this->ressources as $k => $v){
			if (
				($buffer == '' || $k >= $this->buffer[$buffer])
				&& (
				$type == null
				|| (is_numeric($type) && $type == $this->types[$k])
				|| (is_array($type) && in_array($this->types[$k],$type) !== false)
				)
				){
                //Only replace URL and Paths in files ressources as it will be handled via the CSS and JS controllers,
                //Do not replace URLs&Paths in inline scripts as this causes problems with files that are not
                //served by PHP, i.e. for BG images in CSS for example.
                //To prevent compatibility issues, we will keep the URL replacements active for JS scripts
                //as this was active and never caused any problem yet.... one should not hardcode any URL in JS scripts anyways
                //and should use {url} and similar JS tags if needed instead.
                if ($this->priority[$k] != RESSOURCE_SCRIPT && $this->types[$k] == RESSOURCE_CSS) $ress = str_replace(
                    array(URL,SITESROOT,DATAROOT,THEMEROOT,ROOT),
                    array('','__SR__','__DR__','__TR__',''),$this->ressources[$k]
                );
                else $ress = $this->ressources[$k];
				$ret[] = array(
					'type' => $this->types[$k],
					'ressource' => $ress,
					'priority' => $this->priority[$k],
					'options' => $this->options[$k]
				);
			}
		}
		return $ret;
	}

	/**
	 * Imports ressources previously exported.
	 *
	 * @access public
	 *
	 * @param Array $ressources
	 */
	public function import($ressources) {
		foreach ($ressources as $k => $v){
			$res = $v['ressource'];
			if (substr($res,0,1) != '/' && $v['priority'] != RESSOURCE_SCRIPT) {
				if (substr($res,0,4) != 'http') $res = ROOT.$res;
			}
			if (in_array($$this->ressources) == false) {
				$this->ressources[] = $res;
				$this->types[] = $v['type'];
				$this->options[] = $v['options'];
				$this->priority[] = $v['priority'];
			}
		}
	}

	public function listDir($dir,$appendroot=true){
		if ($appendroot && substr($dir,0,1) != '/') $dir = ROOT.$dir;
		$ressources = array();
		if (is_dir($dir) && $handle = opendir($dir)) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != '.' && $entry != '..') {
					$ressources[] = $entry;
				}
			}
		}
		return $ressources;
	}

	public function find($dir) {
		$ressources = $this->listDir($dir);
		foreach($ressources as $k => $entry) {
			$lowerentry = strtolower($entry);
			if (strstr($lowerentry,'.php') === false && strstr($lowerentry,'.js') === false && strstr($lowerentry,'.css') === false && is_dir(ROOT.$dir.'/'.$lowerentry) == false) {
				unset($ressources[$k]);
			}
		}
		return $ressources;
	}

	/**
	 * Finds a ressource
	 *
	 * @param string $type The ressource type : widget, content, view, controller
	 * @param string $name Ressource name
	 * @return string Ressource path if found, false otherwise
	 */
	public function findRessource($type,$name) {
		$r = glob(ROOT.'com/*/'.strtolower($type).'/'.strtolower($name).'.php');
		foreach ($r as $v) {
			return str_replace([ROOT,'.php'],'',$v);
		}
		return false;
	}

	public function getRessourceObject($type,$name) {
		$this->get($this->findRessource($type,$name));
		$clsName = ucfirst(strtolower($name)).ucfirst(strtolower($type));
		if (class_exists($clsName)) return new $clsName();
		else return false;
	}

	/**
	 * Finds the component in which specified ressource exists.
	 *
	 * @access public
	 *
	 * @param string $type Ressource type to look for
	 * @param string $name Ressource name to look for
	 * @return string Component folder name where ressource was found.
	 */
	public function findRessourceComponent($type,$name) {
		$path = $this->findRessource($type,$name);
		if ($path) {
			$path = explode('/',$path);
			return $path[1];
		}
		return false;
	}

	/**
	 * Clears all caches or a specific one
	 *
	 * @access public
	 * @return bool $success
	 */
	public function clearCaches($type='',$name=''){
		$pattern = '*/*';
		if ($type != '') $pattern = $type.'/'.(($name == '')?'*':$name);
		$files = glob(str_replace('//','/',DATAROOT.'/cache/').$pattern); //get all file names
		$success = true;
		foreach($files as $file){
			if(is_file($file)) {
				$res = unlink($file); //delete file
				if (!$res) $success = $res;
			}
		}
		return $success;
	}

	/**
	 * Alias for clearCaches
	 *
	 * @access public
	 * @return bool $success
	 */
	public function clearCache($type='',$name=''){
		$this->clearCaches($type,$name);
	}

    /**
     * Returns current theme path, including current theme folder.
     *
     * Will return default theme path on failure to find theme.
     *
     * @access public
     * @param bool $forAdminTheme Whether to set path for the admin theme or user theme
     * @return string $themepath Theme folder path
     **/
	public function getThemePath($forAdminTheme = false) {
		global $service;
		$theme = ($forAdminTheme)?$service->get('Setting')->get('admintheme'):$service->get('Setting')->get('theme');
		if ($theme == '') $theme = ($forAdminTheme)?'admin':'default';
		$tpath = '';
		if (file_exists(THEMEROOT.strtolower($theme))) {
			$tpath = THEMEROOT.strtolower($theme).'/';
		} else {
			$tpath = THEMEROOT.'default/';
		}
		return $tpath;
	}

    /**
     * Gets current theme url, including theme folder name.
     *
     * @access public
     * @param bool $forAdminTheme Whether to return path to admin theme or user theme
     * @return string $themeurl
     **/
	public function getThemeUrl($forAdminTheme = false){
		return str_replace(THEMEROOT, THEMEURL, $this->getThemePath($forAdminTheme));
	}

	/**
	 * Builds the current theme object
	 *
	 * @access public
	 *
	 * @param bool $forAdminTheme Returns the path of the admin theme instead of the user side theme path
	 * @return Theme $themeObject
	 */
	public function getTheme($forAdminTheme = false) {
		global $service;
		$theme = $this->getThemePath($forAdminTheme);
		$service->get('Ressource')->get($theme);
		$theme = substr($theme,0,-1);
		$theme = substr($theme,strrpos($theme,'/')+1);
		$themeCls = ucfirst($theme).'Theme';
		if (class_exists($themeCls)) $theme = new $themeCls();
        else{
            //Hard error. End process here.
            global $service;
            $service->get('Log')->add(ucfirst($theme)." Theme not found ! Aborting.",__FILE__,__LINE__,E_ERROR,LOG_FILE_DATABASE);
            echo ucfirst($theme)." Theme not found ! Aborting.";
            die;
        }
		return $theme;
	}

	/**
	 * Returns icon path
	 *
	 * @access public
	 *
	 * @param Array $info An info array returned by getInfo() method
	 * @param int $size
	 * @return String Icon file path relative to ROOT
	 */
	public function getIcon($info, $size=32){
		return 'com/'.$info['component'].'/asset/icon/'.intval($size).'x'.intval($size).'/'.$info['icon'].
			((isset($info['iconformat']))?".".$info['iconformat']:'.png');
	}

	/**
	 * Gets all paths to ressources of the same type
	 *
	 * @access public
	 * @param string $type
	 * @return array $ressourcePaths
	 */
	public function getType($type) {
		$r = glob(ROOT.'com/*/'.strtolower($type).'/*.php');
		foreach ($r as $k => $v){
			$file = str_replace([ROOT,'.php'],'',$v);
			$r[substr($file,strrpos($file,'/')+1)] = $file;
		}
		return $r;
	}

	/**
	 * Returns informations about available content type members
	 *
	 * @access public
	 *
	 * @param String $type
	 * $return Array Informations for each plugin found
	 */
	public function getInfos($type='content') {
		if ($type == '') return array();
		$ressources = glob(ROOT.'com/*/'.strtolower($type).'/*.php');
		$infos = array();
		foreach($ressources as $v) {
			$v = str_replace([ROOT,'.php'],'',$v);
			$this->get($v);
			$widgetCls = ucfirst(strtolower(substr($v,strrpos($v,'/')+1))).ucfirst(strtolower($type));
			if (class_exists($widgetCls)) {
				$obj = new $widgetCls();
				$i = $obj->getInfo();
				if (is_array($i)) $infos[] = $obj->getInfo();
			}
		}
		return $infos;
	}

	/**
	 * listComponents
	 *
	 * @return array Components array
	 */
	public function listComponents() {
		$r = glob(ROOT.'com/*/info.php');
		foreach ($r as $k => $v) {
			$r[$k] = str_replace([ROOT.'com/','/info.php'],'',$v);
		}
		return $r;
	}

}
?>
