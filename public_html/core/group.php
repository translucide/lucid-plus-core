<?php
/**
 * User Groups
 *
 * Manages users groups
 *
 * October 28, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class GroupModel extends Model {
	function __construct(){
		$this->initVar('group_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('group_nameid',DATATYPE_STRING,array('length'=>100));
		$this->initVar('group_title',DATATYPE_STRING,array('length'=>100));
		$this->initVar('group_site',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('group_created',DATATYPE_INT);
		$this->initVar('group_modified',DATATYPE_INT);
		$this->initVar('group_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->type = 'group';
	}
}

class GroupData extends Data {
	function __construct(){
        parent::__construct('group');
	}
}

class GroupStore extends Store {
	function __construct(){
		parent::__construct('group');
	}
}

class Group {
	/**
	 * Holds groups objects
	 *
	 * @access private
	 */
	private $data;

	/**
	 * Group constructor
	 *
	 * @access public
	 *
	 * @return void User()
	 */
	public function __construct(){
		$this->data['loaded'] = false;
		$this->load();
	}

	/**
	 * Loads the groups into $data
	 *
	 * @access public
	 * @return bool True or false depending if current groups was found.
	 */
	public function load(){
		//Prevent repetitive user loading queries for anonymous users.
		if ($this->data['loaded']) {
			return (isset($data['groups']) && is_object($data['groups']))?true:false;
		}
		$store = new GroupStore();
		$groups = $store->get();
		$this->data['loaded'] = true;
		if ($groups == false || !is_array($groups) || count($groups) == 0) {
			$this->data['groups'] = array();
			return false;
		}
		else $this->data['groups'] = $groups;
		return true;
	}

	/**
	 * Returns the group ID for the specified name ID.
	 * @param  [string] $name NameID for group
	 * @return [Guid/ID] The GUID/ID of name ID or $name when not found.
	 */
	public function id($name){
		if (!$this->data['loaded']) $this->load();
		foreach( $this->data['groups'] as $v) {
			if ($v->getVar('group_nameid') == $name) return $v->getVar('group_id');
		}
		return $name;
	}
}
