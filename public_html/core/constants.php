<?php
define('CORE_VERSION','0.2.3');

include_once(DIRNAME(__FILE__).'/constant/language.php');
include_once(DIRNAME(__FILE__).'/constant/outputmode.php');
include_once(DIRNAME(__FILE__).'/constant/datatype.php');

//Boot modes
define('BOOTMODE_WEB','WEB');
define('BOOTMODE_CLI','CLI');

define('SANITIZER_FORBIDDEN_HTML_TAGS','script,style,iframe,table,basefont,center,dir,embed,font,isindex,menu,noembed,s,strike,u,input,textarea,head,object');
define('SANITIZER_FORBIDDEN_USER_NAMES','admin,supervisor,su,root');
define('SANITIZER_RESTRICTED_USER_NAMES_PARTS','fuck,bitch,asshole');
?>
