<?php
/**
 * Default menu controller
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('obj/controller');
  
class MenuController extends Controller{
    public function __construct(){
        parent::__construct();
    }
    
	public function execute(){
		global $service;
		$items = $service->get('EventHandler')->trigger('menu.onLoad');
		$view = $this->getView();
		$view->setVar('items',$items);
		return $view;
	}
}
?>