<?php
/**
 * Default route controller
 *
 * Default controller loaded on home page request or unknown page requests
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('obj/controller');
  
class Error404Controller extends Controller{
    public function __construct(){
        parent::__construct();
    }
    
	public function execute(){
		return $this->getView();
	}
}
?>