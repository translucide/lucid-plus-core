<?php
/**
 * CSS ressource handler class
 *
 * Handles CSS output
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

global $service;
$service->get('Ressource')->get('core/controller/asset');
$service->get('Ressource')->get('core/view/css');
  
class CssController extends AssetController{
	
	/**
	 * Css class constructor
	 *
	 * @public
	 *
	 * @return void CssController()
	 */
	public function __construct(){
		parent::__construct();
	}
	
	/**
	 * Renders the CSS to stdout
	 *
	 * @public
	 *
	 * @return string CSS code to output
	 */
	public function execute(){
        global $service;
        $cachefile = $this->cacheFileName().'.css';
        $iscached = file_exists(DATAROOT.'cache/css/'.$cachefile);
        if (!$iscached || $this->devmode) {
            $code = $this->getOutput($this->files);
            $code = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '' ,str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $code));

            /**
             * Replace theme styles vars
             **/
			$service->get('Ressource')->get('core/display/theme');
			$theme = $service->get('Ressource')->getTheme($service->get('Url')->isAdmin());
            $info = $theme->getInfo();
            if (isset($info['styles'])) {
                foreach ($info['styles'] as $k => $v ){
                    foreach ($v as $kv => $vv) {
                        $code = str_replace('{'.$k.'.'.$kv.'}',$vv,$code);
                    }
                }                
            }
            
            $res = file_put_contents(DATAROOT.'cache/css/'.$cachefile,$code);
            if ($res === false) {
                mkdir(DATAROOT.'cache/css/',0744,true);
                $res = file_put_contents(DATAROOT.'cache/css/'.$cachefile,$code);
                if ($res === false) {
                    global $service;
                    $log = $service->get('Log');
                    $log->add("CssController::execute (".get_called_class().") : Could not create CSS cache directory in \"".DATAROOT."cache/css/\". Make sure directory is writable!",__FILE__,__LINE__,E_WARNING);
                    die;
                }
            }
        }else {
            $res = true;
            $code = file_get_contents(DATAROOT.'cache/css/'.$cachefile);
        }
        header('Content-type: text/css',true);
        $this->cacheHeaders($this->mostRecent($this->files));
        $this->getView()->set(array('code' => $code));
		return $this->getView();
    }
}
?>