<?php
/**
 * JS ressource handler class
 *
 * Handles JS output
 * 
 * Oct 30, 2012

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1 
 */

global $service;
$service->get('Ressource')->get('core/controller/asset');
$service->get('Ressource')->get('core/view/js');
  
class JsController extends AssetController{
	
	/**
	 * JS class constructor
	 *
	 * @public
	 *
	 * @return void JsController()
	 */
	public function __construct(){
		parent::__construct();
	}
	
	/**
	 * Renders the JS to stdout
	 *
	 * @public
	 *
	 * @return string JS code to output
	 */
	public function execute(){
        $cachefile = $this->cacheFileName().'.js';
        $iscached = file_exists(DATAROOT.'cache/js/'.$cachefile);
        if (!$iscached || $this->devmode) {
            $code = $this->getOutput($this->files);
            $res = file_put_contents(DATAROOT.'cache/js/'.$cachefile,$code);
            if ($res === false) {
                mkdir(DATAROOT.'cache/js/',0744,true);
                $res = file_put_contents(DATAROOT.'cache/js/'.$cachefile,$code);
                if ($res === false) {
                    global $service;
                    $log = $service->get('Log');
                    $log->add("JsController::execute (".get_called_class().") : Could not create JS cache directory in \"".DATAROOT."cache/js/\". Make sure directory is writable!",__FILE__,__LINE__,E_WARNING);
                    die;
                }
            }
        }else {
            $res = true;
            $code = file_get_contents(DATAROOT.'cache/js/'.$cachefile);
        }
        header('Content-type: text/javascript',true);
        $this->cacheHeaders($this->mostRecent($this->files));
        $this->getView()->set(array('code' => $code));
		return $this->getView();
    }
}
?>