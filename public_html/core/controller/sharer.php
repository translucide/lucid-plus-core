<?php
/**
 * Sharer controller
 *
 * Handles sharer requests
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('lib/communication/email');
$service->get('Ressource')->get('obj/controller');

class SharerController extends Controller{
    public function __construct(){
        $this->component = 'core';        
    }
    
	public function execute(){
		global $service;
		$url = $service->get('Url')->get();
		$request = $service->get('Request')->get();		
		switch($url['realpath']) {
			case '/'.$service->get('Language')->getCode().'/json/sharer/sendmail' : {
				header('Content-type: application/json');
				$mail = new Email();
				$mail->to($request['email']);
				$mail->from($service->get('Setting')->get('adminmail'));
				$mail->subject($request['subject']);
				$mail->body(SHARER_SHARETHISPAGEVIAEMAIL_FROM.$request['name']."\n\n".$request['message']);
				$mail->send();
				$ret['success'] = 1;
				echo json_encode($ret);
				die();
			}break;
		}
		return $view;
	}
}
?>