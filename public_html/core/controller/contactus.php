<?php
/**
 * Contact us plugin controller
 *
 * Handles contact us requests
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('obj/controller');
$service->get('Ressource')->get('lib/display/form');
$service->get('Ressource')->get('lib/display/form/field');
$service->get('Ressource')->get('com/lang/'.$service->get('Language')->getCode().'/contactus');

class ContactusController extends Controller{
    public function __construct(){
        parent::__construct();
    }
    
	public function execute(){
		global $service;
		$view = $this->getView();
		$request = $service->get('Request')->get();
		
		//Set page & form title title...
		if ($request['title'] != '') $title = urldecode($request['title']);
		else $title = CONTACTUS_TITLE;
		$service->get('Theme')->setTitle($title);
		
		$obj = array();
		$obj['name'] = $request['name'];
		$obj['email'] = $request['email'];
		if ($service->get('User')->isLogged() && $obj['email'] == '') $obj['email'] = $request['email'];
		$obj['phone'] = $request['phone'];
		$obj['subject'] = $request['subject'];
		$obj['body'] = ($request['body'] != null & $request['body'] != false)?$request['body'] : '';
		$op = $request['op'];
		
		if ($service->get('Language')->getCode() == 'fr') $form = new Form(URL.'/fr/contactez-nous.html','contactform',$title,'POST');
		else $form = new Form(URL.'/en/contact-us.html','contactform',$title,'POST');

		$valid = false;
		//Validate inputs...
		if (strlen($obj['name']) > 2 && strlen($obj['email']) > 5  && strlen($obj['phone']) >= 8  &&
			strlen($obj['subject']) >= 2  && strlen($obj['body']) >= 2 && $op=='contactus')
			$valid = true;
			
		if ($valid) {
			$service->get('Ressource')->get('lib/communication/email');
			$email = new Email();
			$email->from($obj['name'].' <'.$obj['email'].'>');
			$to = $service->get('Setting')->get('contactus_email');
			if ($to == '') $to = $service->get('Setting')->get('adminmail');
			$email->to($to);
			$email->subject($obj['subject']);
			
			$body = '';
			$exist = explode('|',$request['fields']);
			foreach ($exist as $k => $v) {
				//Form field found.
				$title = '';
				$val = '';
				if (isset($request['t_'.$k])) $title = $request['t_'.$k];
				else {
					$title = $v;
					if ($v == 'name') $title = CONTACTUS_NAME;
					if ($v == 'address') $title = CONTACTUS_ADDRESS;
					if ($v == 'city') $title = CONTACTUS_CITY;
					if ($v == 'pcode') $title = CONTACTUS_PCODE;
					if ($v == 'email') $title = CONTACTUS_EMAIL;
					if ($v == 'phone') $title = CONTACTUS_PHONENUMBER;
					if ($v == 'body') $title = CONTACTUS_BODY;
				}
				
				$val = $request[$v];
				if (is_array($val)) $val = "\n".implode("\n",$val);
				else {
					if (strlen($val > 40)) $val = "\n\n".$val."\n\n";
					else $val = $val."\n";
				}
				$body .= $title.': '.$val;
			}
			
			$body .= CONTACTUS_CLIENTIP.': '.$_SERVER['REMOTE_ADDR']."\n";
			$body .= CONTACTUS_CLIENTLANG.': '.$service->get('Language')->getCode()."\n";
			$email->body($body);
			$res = $email->send();
			echo nl2br($body);
			die();
			if ($res) $form->setVar('intro',CONTACTUS_SENDCONFIRM);
			else $form->setVar(CONTACTUS_SENDFAILED);
		}
		else {
			if ($op != 'contactus') $form->setVar('intro',CONTACTUS_INTRO);
			else $form->setVar('intro',CONTACTUS_NOTVALID);
			
			$form->add(new HiddenFormField('op','contactus'));
			
			if (!isset($request['h_name'])) $form->add(new TextFormField('name',$obj['name'],array('title' => (isset($request['t_name']))?$request['t_name']:CONTACTUS_NAME)));	
			else $form->add(new HiddenFormField('name',$obj['name'],array('title' => (isset($request['t_name']))?$request['t_name']:CONTACTUS_NAME)));	
			
			if (!isset($request['h_address'])) $form->add(new TextFormField('address',$obj['address'],array('title' => (isset($request['t_address']))?$request['t_address']:CONTACTUS_ADDRESS)));
			else $form->add(new HiddenFormField('address',$obj['address'],array('title' => (isset($request['t_address']))?$request['t_address']:CONTACTUS_ADDRESS)));
			
			if (!isset($request['h_city'])) $form->add(new TextFormField('city',$obj['city'],array('title' => (isset($request['t_city']))?$request['t_city']:CONTACTUS_CITY)));
			else $form->add(new HiddenFormField('city',$obj['city'],array('title' => (isset($request['t_city']))?$request['t_city']:CONTACTUS_CITY)));
			
			if (!isset($request['h_pcode'])) $form->add(new TextFormField('pcode',$obj['pcode'],array('title' => (isset($request['t_pcode']))?$request['t_pcode']:CONTACTUS_PCODE)));
			else $form->add(new HiddenFormField('pcode',$obj['pcode'],array('title' => (isset($request['t_pcode']))?$request['t_pcode']:CONTACTUS_PCODE)));
			
			if (!isset($request['h_email'])) $form->add(new TextFormField('email',$obj['email'],array('title' => (isset($request['t_email']))?$request['t_email']:CONTACTUS_EMAIL)));
			else $form->add(new HiddenFormField('email',$obj['email'],array('title' => (isset($request['t_email']))?$request['t_email']:CONTACTUS_EMAIL)));
			
			if (!isset($request['h_phone'])) $form->add(new TextFormField('phone',$obj['phone'],array('title' => (isset($request['t_phone']))?$request['t_phone']:CONTACTUS_PHONENUMBER)));
			else $form->add(new HiddenFormField('phone',$obj['phone'],array('title' => (isset($request['t_phone']))?$request['t_phone']:CONTACTUS_PHONENUMBER)));
			
			if (!isset($request['h_subject'])) $form->add(new TextFormField('subject',$obj['subject'],array('title' => (isset($request['t_subject']))?$request['t_subject']:CONTACTUS_SUBJECT)));
			else $form->add(new HiddenFormField('subject',$obj['subject'],array('title' => (isset($request['t_subject']))?$request['t_subject']:CONTACTUS_SUBJECT)));
			
			if (!isset($request['h_body'])) $form->add(new TextareaFormField('body',$obj['body'],array('title' => (isset($request['t_body']))?$request['t_body']:CONTACTUS_BODY,'cols'=>80,'rows'=>10)));
			else $form->add(new HiddenFormField('body',$obj['body'],array('title' => (isset($request['t_body']))?$request['t_body']:CONTACTUS_BODY,'cols'=>80,'rows'=>10)));
			
			//Add hidden & title mentions for existing fields
			//And add new fields at the same time
			$exist = array('name','address','city','pcode','email','phone','subject','body');
			foreach ($request as $k => $v) {
				if ((substr($k,0,2) == 'h_' || substr($k,0,2) == 't_' || substr($k,0,2) == 'n_' || substr($k,0,2) == 'f_' || substr($k,0,2) == 'o_')) {
					if (!is_array($v)) $form->add(new HiddenFormField($k,$v));
					else {
						foreach ($v as $vk => $vo) {
							$form->add(new HiddenFormField($k.'[]',$vo));
						}
					}
				}
				if (substr($k,0,2) != 'h_' && substr($k,0,2) != 'o_' && substr($k,0,2) != 'f_' && substr($k,0,2) != 't_' && substr($k,0,2) != 'n_' && !in_array($k,$exist)) {
					$exist[] = $k;
					//New field...
					if (isset($request['f_'.$k])) {
						$type = ucfirst($request['f_'.$k]).'FormField';
						$name = (isset($request['n_'.$k]))?$request['n_'.$k]:$request[$k];
						$title = (isset($request['t_'.$k]))?$request['t_'.$k]:$request[$k];
						$options = array();
						foreach($request['o_'.$k] as $v) {
							$options[] = array('title' => $v, 'value' => $v);
						}
						$form->add(new $type($name,'',array('title'=>$title,'options'=>$options)));
					}
				}
			}
			$form->add(new HiddenFormField('fields',implode('|',$exist)));
			$form->add(new SubmitFormField('submit','',array('title' => CONTACTUS_SEND)));
		}
		$view->setVar('form',$form->render());
		return $view;
	}
	
	/**
	 * Returns the corresponding URL in the language ID given
	 * or simply returns the home page url if none found
	 *
	 * @public
	 * @param string $url The url for which we need the translation
	 * @param int $langid The language ID to find the URL for
	 * @return string The translated URL in the language we asked for or false if not found.
	 */
	public function translate($langid, $objid = 0, $url=''){
		if ($url == 'en/contact-us.html' || $url == 'contact-us.html' || $url == 'fr/contactez-nous.html') {
			switch($langid) {
				case 0: return 'contact-us.html';break;
				case 1: return 'en/contact-us.html';break;
				case 2: return 'fr/contactez-nous.html';break;
			}
		}
		return false;
	}
}
?>