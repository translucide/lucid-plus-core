<?php
/**
 * Assets ressource base handler class
 *
 * Handles JS/CSS assets output
 * 
 * Aug 21, 2016

 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2016 Translucide
 * @license
 * @since 		0.1 
 */

global $service;
$service->get('Ressource')->get('obj/controller');

class AssetController extends Controller{
	
    var $theme;
    var $lang;
    var $files;
    var $mostrecent;
    var $devmode;
    
	/**
	 * Asset class constructor
	 *
	 * @access public
	 *
	 * @return void AssetController()
	 */
	public function __construct(){
        global $service;
        parent::__construct();
        $this->lang = $service->get('Language')->getCode();
        $this->theme = $service->get('Ressource')->getThemePath(($service->get('Url')->isAdmin())?true:false);		
        $this->files = $this->getAssets();
        $this->mostrecent = $this->mostRecent($this->files);
        $this->devmode = $service->get('Setting')->get('developmentmode');
        if (!$this->devmode && defined('DEBUG') && DEBUG == true) {
            $this->devmode = true;
        }
	}
	
	/**
	 * Renders the Assets to stdout
	 *
	 * @access public
	 *
	 * @return string assets code to output
	 */
	public function execute(){
        return '';
	}

    /**
     * Builds a unique cache file name based on assets to load and tags values to replace
     *
     * @access protected
     * 
     * @return string An md5 string representing the cachable assets.
     */
    protected function cacheFileName(){
        $f = $this->files;
        sort($f);
        return md5('MODIFIED='.$this->mostrecent.'|DATAROOT='.DATAROOT.'|URL='.URL.'|THEME='.$this->theme.'|UPLOADURL='.UPLOADURL.'|LANG='.$this->lang.'+++'.implode('|',$f));
    }
    
    /**
     * Sets assets headers: force 1 year cache
     *
     * @access protected
     *
     * @param int $mostrecent;
     */
    protected function cacheHeaders($lastmodified){
        if ($this->devmode) {
            header('Cache-Control: must-revalidate',true);            
        }else {
            header('Cache-Control: public',true);
            header("Pragma: cache");
            header("Last-Modified: " . gmdate("D, d M Y H:i:s",$lastmodified) . " GMT",true);
            header('Expires: '.gmdate("D, d M Y H:i:s",time()+31556926).' GMT',true);                    
        }
    }
    
    /**
     * Finds assets to load.
     *
     * @access protected
     *
     * @return array $files An array of assets to load.
     */
    protected function getAssets(){
		global $service;
		$request = $service->get('Request')->get();
        $url = $service->get('Url')->get();
        $urlpath = (substr($url['path'],0,1) == '/')?substr($url['path'],1):$url['path'];
        $files = array();
        if (!isset($request['f'])) $request['f'] = array($urlpath);
        //sort($request['f']);
        foreach($request['f'] as $k => $v) {
            $files[$k] = $this->toFilePath(urldecode($v));
        }
        return $files;        
    }
    
    /**
     * Returns the most recently modified file within a file list.
     *
     * @access protected
     *
     * @param array $files An array of file paths
     * @return int $mostrecent Timestamp of most recent modified file.
     */
    protected function mostRecent($files){
        $mostrecent = 0;
        foreach($files as $k => $v) {
            $mostrecent = max(filemtime($v),$mostrecent);
        }
        return $mostrecent;
    }
    
    /**
     * Turns an asset URL to an absolute file path
     *
     * @access protected
     *
     * @param string $url An asset file URL to convert to file path.
     * @return string $str Absolute file path.
     */
	protected function toFilePath($url){
		$bpath = substr($url,0,6);
		if ($bpath == '__SR__' || $bpath == '__DR__' || $bpath == '__TR__') {
			if ($bpath == '__SR__') $url = str_replace('__SR__',SITESROOT,$url);
			if ($bpath == '__DR__') $url = str_replace('__DR__',DATAROOT,$url);
			if ($bpath == '__TR__') $url = str_replace('__TR__',THEMEROOT,$url);
			$str = $url;
		} else {
			$str = ROOT.$url;
		}
		return $str;
	}
    
    /**
     * Retrieve all specified files in $files.
     *
     * Replaces the following tags: {themeurl}, {uploadurl}, {lang}, {url}
     *
     * @access protected
     * @param array $files Input files
     * @return string $code All files content as a concatenated string
     */
    protected function getOutput($files) {
        $tmp = '';
        foreach ($files as $k => $v) {
            $tmp .= "\r\n".file_get_contents($v)."\r\n";
        }     
        $code = str_replace('{themeurl}',str_replace(THEMEROOT,THEMEURL,$this->theme),$tmp);
        $code = str_replace('{uploadurl}',UPLOADURL,$code);
        $code = str_replace('{lang}',$this->lang,$code);
        $code = str_replace('{url}',URL,$code);
        return $code;
    }
}
?>