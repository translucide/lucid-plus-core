<?php
/**
 * Default banner controller
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
@ini_set('post_max_size','15M');
@ini_set('upload_max_filesize','15M');

global $service;
$service->get('Ressource')->get('core/controller');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/setting');
$service->get('Ressource')->get('lib/fineuploader/qqfileuploader');

class ImageuploadController extends Controller{
    public function __construct(){
        parent::__construct();
    }

	public function execute(){
		global $service;
		$url = $service->get("Url")->get();
		$request = $service->get('Request')->get();

		$uploader = new qqFileUploader();

		if ($url['path'] == 'json/fileupload') {
			//Only allow file types set in site settings.
			$uploader->allowedExtensions = explode(",",$service->get("Setting")->get("allowedfiletypes"));
		}else {
			//Only allow image types since we are uploading an image...
			$uploader->allowedExtensions = array("JPEG","JPG","jpg","jpeg","PNG","png","gif","GIF");
		}
		/**
		 * SizeLimit must not exceed server's POST_MAX _SIZE
		 * and UPLOAD_MAX_FILESIZE values.
		 *
		 * An error will be visible to users if this value is higher than server's values.
		 */
		$uploader->sizeLimit = 10 * 1024 * 1024; //10MB

		// Specify the input name set in the javascript.
		$uploader->inputName = 'qqfile';

		// If you want to use resume feature for uploader, specify the folder to save parts.
		$uploader->chunksFolder = DATAROOT.'cache/chunks';

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		//$result = $uploader->handleUpload(ROOT.'/uploads');

		// To save the upload with a specified name, set the second parameter.
		$uploadsfolder = UPLOADROOT;
		if (isset($request['folder'])) {
			//Do not allow ../ for security reasons
			$request['folder'] = str_replace(['../','..\/'],'',$request['folder']);
			$uploadsfolder .= $request['folder'];
		}
		if (!is_dir($uploadsfolder)) mkdir($uploadsfolder,0777,true);
		if (!is_dir($uploadsfolder.'/thumb')) mkdir($uploadsfolder.'/thumb',0777,true);
		$result = $uploader->handleUpload($uploadsfolder, preg_replace("/[^a-zA-Z0-9.]/", '',  $uploader->getName()));

		// To return a name used for uploaded file you can use the following line.
		$result['uploadName'] = ((isset($request['folder']))?$request['folder'].'/':'').$uploader->getUploadName();

        if (@getimagesize(UPLOADROOT.$result['uploadName']) !== false) {
            $service->get('Ressource')->get('core/display/thumbnail');
            $thumb = new Thumbnail();
            $thumb->generateThumbnails($result['uploadName']);
        }

		header("Content-Type: text/plain");
		echo json_encode($result);
		exit();
	}

}
?>
