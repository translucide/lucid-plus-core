<?php
/**
 * System Sections controller
 *
 * Handles system sections display
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/controller');
$service->get('Ressource')->get('display/widget');

class ContentController extends Controller{

	/**
	 * Constructor
	 *
	 * @public
	 *
	 * @param int $id
	 * @return void Content()
	 */
	public function __construct(){
        parent::__construct();
	}

	public function execute(){
		global $service;
		$view = $this->getView();
		$data = $service->get('Content')->get();
		$view->setVar('contentid',$data->getVar('content_objid'));
		//We need only one content...
		$content = $service->get('Content')->getContentObject($data->getVar('content_type'),$data);
		if (is_object($content)) {
			$content->setData($data);
			$el = $content->render();
			$id = ($data->getVar('content_options')['contentidattr'] != '')?$data->getVar('content_options')['contentidattr']:$data->getVar('content_type').'_'.$data->getVar('content_objid');
            $opt = $data->getVar('content_options');
            $displaytitle = (isset($opt['displaytitle']))?$opt['displaytitle']:false;
            $contentclasses = (isset($opt['contentclasses']))?$opt['contentclasses']:'';
            $extracss = (isset($opt['extracss']))?$opt['extracss']:'';
			$view->setVar('element',array(
				'title'=>$data->getVar('content_title'),
				'pagetitle'=>$data->getVar('content_pagetitle'),
				'displaytitle' => $displaytitle,
				'type' => $data->getVar('content_type'),
				'class' => $contentclasses,
				'id' => $id,
				'objid' => $data->getVar('content_objid'),
				'parent' => $data->getVar('content_parent'),
				'classpath' => $service->get('Url')->toUrl(str_replace(array('-','/'),array('','-'),$data->getVar('content_urlpath'))),
				'content' => $el.'<style type="text/css">'.str_replace("{id}",$id,$extracss).'</style>'
			));

			//Page metas for SEO.
			$title = $data->getVar('content_pagetitle');
			if (strlen(trim($title)) == 0) {
				$title = $data->getVar('content_title');
				if (strlen($title) + strlen($service->get('Setting')->get('sitename') + 3) < 65) {
					$title = $title.' - '.$service->get('Setting')->get('sitename');
				}
			}
			
			$service->get('Theme')->setTitle($title);
			$desc = $data->getVar('content_description');
			if ($desc == '') $desc = $data->getVar('content_exerpt');
			$service->get('Theme')->setDescription($desc);
			$service->get('Theme')->setKeywords($data->getVar('content_keywords'));
			$view->setVar('layout',$data->getVar('content_layout'));
		}
		return $view;
	}

	public function translate($langid,$objid=0,$url='') {
		global $service;
		$urlpath = $service->get('Url')->get();
		$store = new ContentStore();
		$store->setOption('ignorelangs',true);
		$section = $store->get(new Criteria('content_urlpath',$urlpath['language'].'/'.$url));
		if (count($section) == 1) {
			$section = $store->get(new Criteria('content_objid',$section[0]->getVar('content_objid')));
		}
		$returl  = '';
		foreach ($section as $k => $v) {
			if ($v->getVar('content_language') == $langid) {
				$returl = $v->getVar('content_urlpath');
			}
		}
		return $returl;
	}
}
?>
