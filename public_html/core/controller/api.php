<?php
/**
 * Ajax controller
 *
 * Handles ajax requests and dispatch them
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/controller');

class ApiController extends Controller{
    public function __construct(){
        parent::__construct();
    }

	public function execute(){
		global $service;
		if ($service->get('User')->isAdmin() == false && $service->get('Url')->isAdmin()) die();
		$url = $service->get('Url')->get();
		$request = $service->get('Request')->get();
		$service->get('Ressource')->clear();
		$obj = $this->getAjaxObject($request['component'],$request['type'],$request['name']);
		if ($obj && method_exists($obj,'apiCall') && method_exists($obj,'hasAccess') && $obj->hasAccess($request['op'])) {
			$ret = $obj->apiCall($request['op']);
			$ressources = $service->get('Ressource')->export([RESSOURCE_CSS,RESSOURCE_JS]);
			if (count($ressources) && isset($ret['ressources']) && $ret['ressources']) {
				$ret['ressources'] = array(
					'files' => $ressources,
					'urls' => array(
						'js' => $service->get('Ressource')->getUrls(OUTPUT_MODE_JS),
						'css' => $service->get('Ressource')->getUrls(OUTPUT_MODE_CSS)
					)
				);
			}
			else {
				if (isset($ret['ressources'])) unset($ret['ressources']);
			}
		}else {
			$ret = array('success' => false, 'message' => _INVALIDREQUEST);
		}
		header('Content-Type: application/json');
		echo json_encode($ret);
		die();
	}

	public function getAjaxObject($component,$type,$class){
		global $service;
		$obj = false;
		$clsName = ucfirst($class).ucfirst($type);
		$service->get('Ressource')->get('com/'.$component.'/'.$type.'/'.$class);
		if (class_exists($clsName)) $obj = new $clsName();
		return $obj;
	}

}
