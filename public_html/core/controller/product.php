<?php
/**
 * Product controller
 *
 * Handles product requests
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Section');
$service->get('Ressource')->get('io/data');
$service->get('Ressource')->get('io/converter/requesttoobject');
$service->get('Ressource')->get('obj/controller');
$service->get('Ressource')->get('lib/display/form');
$service->get('Ressource')->get('lib/display/form/field');
$service->get('Ressource')->get('com/lang/'.$service->get('Language')->getCode().'/content');

class ProductController extends Controller{
    public function __construct(){
        parent::__construct();
    }

	public function execute(){
		global $service;
		//$view = $this->getView();
		$url = $service->get('Url')->get();
		$store = new DataStore();
		$store->setOption('ignorelangs',true);
		$request = $service->get('Request')->get();
		switch($url['realpath']) {
			/**
			 * Models managing
			 */
			case '/'.$service->get('Language')->getCode().'/json/admin/product/model/list' : {
				header('Content-type: application/json');
				$defaultlang = $service->get('Language')->getDefault();
				$store->setOption('ignorelangs',false);
				$crit = new CriteriaCompo(new Criteria('data_type','productmodel'));
				if ($request['parent']) $crit->add(new Criteria('data_parent',$request['parent']));
				$crit->setOrder('ASC');
				$crit->setSort('data_position');
				$obj = $store->get($crit);

				//Reorder products on list OP in order to be sure they can be ordered properly later on.
				$i = 1;
				foreach($obj as $k => $v) {
					$service->get('Db')->query('UPDATE `data` SET `data_position` = '.$i.' WHERE `data_type` = \'productmodel\' AND `data_objid` = \''.$v->getVar('data_objid').'\' AND `data_parent` = \''.$request['parent'].'\'');
					$i++;
				}

				$response = array();
				foreach($obj as $v) {
					$response[] = array('title'=> $v->getVar('data_title'),'id' => $v->getVar('data_objid'));
				}
				echo json_encode(array('success'=>1,'data'=>$response));
				die();
			}break;
			case '/'.$service->get('Language')->getCode().'/json/admin/product/model/move' : {
				header('Content-type: application/json');
				$defaultlang = $service->get('Language')->getDefault();
				$store->setOption('ignorelangs',true);
				$obj = $store->getByObjId($request['id']);
				$offset = intval($request['offset']);
				if ($offset != 1 && $offset != -1) {
					$resp = false;
				}else {
					$oldpos = $obj[0]->getVar('data_position');
					$newpos = $obj[0]->getVar('data_position')+$offset;
					$parent = $obj[0]->getVar('data_parent');
					$crit = new CriteriaCompo(new Criteria('data_type','productmodel'));
					$crit->add(new Criteria('data_parent',$parent));
					$crit->add(new Criteria('data_position',$newpos));
					$obj2 = $store->get($crit);
					if (count($obj2) != 0) {
						foreach ($obj as $k => $v){
							$obj[$k]->setVar('data_position',$newpos);
						}
						foreach ($obj2 as $k => $v){
							$obj2[$k]->setVar('data_position',$oldpos);
						}
						$store->save($obj);
						$store->save($obj2);
					}
				}
				if ($resp || $resp == array()) echo json_encode(array('success'=>1, 'message' => _SAVED));
				else echo json_encode(array('success' => 0, 'message' => _SAVEERROR));
				die();
			}break;
			case '/'.$service->get('Language')->getCode().'/json/admin/product/model/duplicate' : {
				if ($request['id']) {
					$itemsparent = $request['id'];
					$obj = $store->getByObjId($request['id']);
					$next = $store->getNext();
					$nextpos = $store->getNext('data_position',new Criteria('data_type','productmodel'));
					foreach ($obj as $k => $v) {
						$obj[$k]->setVar('data_id',0);
						$obj[$k]->setVar('data_parent',$request['parent']);
						$obj[$k]->setVar('data_objid',$next);
						$obj[$k]->setVar('data_position',$nextpos);
					}
					$store->save($obj);
					$request['id'] = $next;

					//Duplicate all product model items too..
					$crit = new CriteriaCompo(new Criteria('data_parent',$itemsparent));
					$crit->add(new Criteria('data_type','productmodelitem'));
					$crit->setOrder('ASC');
					$crit->setSort('data_position');
					$obj = $store->get($crit);
					$objids = array();
					foreach ($obj as $k => $v){
						$obj[$k]->setVar('data_id',0);
						$obj[$k]->setVar('data_parent',$next);
						$objids[] = $v->getVar('data_objid');
					}
					$next++;
					foreach ($objids as $ko => $vo) {
						foreach($obj as $k => $v ){
							if ($v->getVar('data_objid') == $vo) {
								$obj[$k]->setVar('data_objid',$next);
							}
						}
						$next++;
					}
					$store->save($obj);
				}
			}
			case '/'.$service->get('Language')->getCode().'/json/admin/product/model/edit' : {
				@header('Content-type: application/json');
				$defaultlang = $service->get('Language')->getDefault();
				$langs = $service->get('Language')->getCodes();
				$form = new Form('', 'modelform', 'Edit model', $method='POST');
				$form->setVar('formtag',false);
				if ($request['id']) {
					$obj = $store->getByObjId($request['id']);
				}else {
					$obj = $store->createMultilingual();
					$max = $store->getNext('data_position',new Criteria('data_type','productmodel'));
					foreach($obj as $k => $v) {
						$obj[$k]->setVar('data_type','productmodel');
						$obj[$k]->setVar('data_parent',$request['parent']);
						$obj[$k]->setVar('data_position',$max);
					}
					$store->save($obj);
				}
				$defobj = $store->getDefaultObj($obj);
				$id = $defobj->getVar('data_objid');
				$title = $defobj->getVar('data_title');
				if ($title == '' && $request['id'] == 0) $title = _NEW.' '.PRODUCT_MODEL;
				$data = $defobj->getVar('data_data');
				$form->add(new HiddenFormField('data_id',$defobj->getVar('data_id'),array(
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_id')
				)));
				$form->add(new HiddenFormField('data_type','productmodel'));
				$form->add(new HiddenFormField('data_parent',$request['parent']));
				$form->add(new HiddenFormField('data_objid',$id));
				$form->add(new TextFormField('data_title',$defobj->getVar('data_title'),array(
					'title'=>_TITLE,
					'length'=>255,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_title')
				)));
				$form->add(new ImageuploadFormField('model_mainpic',$defobj->getVar('data_data')['model_mainpic'],array(
					'title'=>PRODUCT_MODEL_MAINPIC,
					'length'=>255,
				)));
				$form->add(new ImageuploadFormField('model_otherpics',$defobj->getVar('data_data')['model_otherpics'],array(
					'title'=>PRODUCT_MODEL_OTHERPICS,
					'length'=>255,
					'multiple' => true
				)));
				$form->add(new FileuploadFormField('model_specs',$defobj->getVar('data_data')['model_specs'],array(
					'title'=>PRODUCT_MODEL_TECHSPECS,
					'length'=>255,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','model_specs')
				)));
				$form->add(new ItemmanagerFormField('model_items','',array(
					'title'=>PRODUCT_MODELITEMS,
					'length'=>255,
					'parent' => $defobj->getVar('data_objid'),
					'editlabel' => _EDIT,
					'deletelabel' => _DELETE,
					'duplicatelabel' => _DUPLICATE,
					'newlabel' => _NEW.' '.PRODUCT_MODELITEM,
					'list' => URL.$service->get('Language')->getCode().'/json/admin/product/modelitem/list',
					'edit' => URL.$service->get('Language')->getCode().'/json/admin/product/modelitem/edit',
					'duplicate' => URL.$service->get('Language')->getCode().'/json/admin/product/modelitem/duplicate',
					'save' => URL.$service->get('Language')->getCode().'/json/admin/product/modelitem/save',
					'delete' => URL.$service->get('Language')->getCode().'/json/admin/product/modelitem/delete',
					'move' => URL.$service->get('Language')->getCode().'/json/admin/product/modelitem/move',
				)));
				$form->add(new CheckboxFormField('model_isaddon',$defobj->getVar('data_data')['model_isaddon'],array(
					'title'=>PRODUCT_MODEL_ISADDON,
					'length'=>255,
					'options' => array(array('title'=>'','value'=>1))
				)));
				$form->addSaveButton('save',_SAVE.' '.PRODUCT_MODEL);
				$form->setVar('title',$title);
				echo json_encode(array('success'=>1,'form'=>$form->renderArray()));
				die();
			}break;
			case '/'.$service->get('Language')->getCode().'/json/admin/product/model/save' : {
				header('Content-type: application/json');
				$saveRequest = new RequestToObject('data');
				$resp = $saveRequest->save();
				$resp = $resp && $store->saveOptions('data_data',array(
					'model_mainpic','model_isaddon','model_otherpics', 'model_specs'
				));
				if ($resp || $resp == array()) echo json_encode(array('success'=>1, 'message' => _SAVED));
				else echo json_encode(array('success' => 0, 'message' => _SAVEERROR));
				die();
			}break;
			case '/'.$service->get('Language')->getCode().'/json/admin/product/model/delete' : {
				header('Content-type: application/json');
				$defaultlang = $service->get('Language')->getDefault();
				$langs = $service->get('Language')->getCodes();
				if ($request['id']) {
					$resp = $store->delete($request['id']);
				}else {
					$resp = false;
				}
				if ($resp || $resp == array()) echo json_encode(array('success'=>1, 'message' => _SAVED));
				else echo json_encode(array('success' => 0, 'message' => _SAVEERROR));
				die();
			}break;

			/**
			 * Model item managing
			 */
			case '/'.$service->get('Language')->getCode().'/json/admin/product/modelitem/list' : {
				header('Content-type: application/json');
				$defaultlang = $service->get('Language')->getDefault();
				$store->setOption('ignorelangs',false);
				$crit = new CriteriaCompo(new Criteria('data_type','productmodelitem'));
				if ($request['parent']) $crit->add(new Criteria('data_parent',$request['parent']));
				$crit->setOrder('ASC');
				$crit->setSort('data_position');
				$obj = $store->get($crit);
				$response = array();
				foreach($obj as $v) {
					$data = $v->getVar('data_data');
					$response[] = array('title'=> $data['itemtitle'],'id' => $v->getVar('data_objid'));
				}
				echo json_encode(array('success'=>1,'data'=>$response));
				die();
			}break;
			case '/'.$service->get('Language')->getCode().'/json/admin/product/modelitem/move' : {
				header('Content-type: application/json');
				$defaultlang = $service->get('Language')->getDefault();
				$store->setOption('ignorelangs',true);
				$obj = $store->getByObjId($request['id']);
				$offset = $request['offset'];
				if ($offset != 1 && $offset != -1) {
					$resp = false;
				}else {
					$oldpos = $obj[0]->getVar('data_position');
					$newpos = $obj[0]->getVar('data_position')+$offset;
					$parent = $obj[0]->getVar('data_parent');
					$crit = new CriteriaCompo(new Criteria('data_type','productmodelitem'));
					$crit->add(new Criteria('data_parent',$parent));
					$crit->add(new Criteria('data_position',$newpos));
					$obj2 = $store->get($crit);
					if (count($obj2) != 0) {
						foreach ($obj as $k => $v){
							$obj[$k]->setVar('data_position',$newpos);
						}
						foreach ($obj2 as $k => $v){
							$obj2[$k]->setVar('data_position',$oldpos);
						}
						$store->save($obj);
						$store->save($obj2);
					}
				}
				if ($resp || $resp == array()) echo json_encode(array('success'=>1, 'message' => _SAVED));
				else echo json_encode(array('success' => 0, 'message' => _SAVEERROR));
				die();
			}break;
			case '/'.$service->get('Language')->getCode().'/json/admin/product/modelitem/duplicate' : {
				if ($request['id']) {
					$itemsparent = $request['id'];
					$obj = $store->getByObjId($request['id']);
					$next = $store->getNext();
					$nextpos = $store->getNext('data_position',new Criteria('data_type','productmodelitem'));
					foreach ($obj as $k => $v) {
						$obj[$k]->setVar('data_id',0);
						$obj[$k]->setVar('data_objid',$next);
						$obj[$k]->setVar('data_position',$nextpos);
						$obj[$k]->setVar('data_parent',$request['parent']);
					}
					$store->save($obj);
					$request['id'] = $next;
				}
			}
			case '/'.$service->get('Language')->getCode().'/json/admin/product/modelitem/edit' : {
				header('Content-type: application/json');
				$defaultlang = $service->get('Language')->getDefault();
				$langs = $service->get('Language')->getCodes();
				$form = new Form('', 'modelitemform', 'Edit model item', $method='POST');
				$form->setVar('formtag',false);
				if ($request['id']) {
					$obj = $store->getByObjId($request['id']);
				}else {
					$obj = $store->createMultilingual();
					$max = $store->getNext('data_position',new Criteria('data_type','productmodelitem'));
					foreach($obj as $k => $v) {
						$obj[$k]->setVar('data_type','productmodelitem');
						$obj[$k]->setVar('data_parent',$request['parent']);
						$obj[$k]->setVar('data_position',$max);
						$obj[$k]->setVar('data_data',array('itemtitle'=>'','itemcontent'=>''));
					}
					$store->save($obj);
				}
				$defobj = $store->getDefaultObj($obj);
				$id = $defobj->getVar('data_objid');
				$title = $defobj->getVar('data_title');
				if ($title == '' && $request['id'] == 0) $title = _NEW.' '.PRODUCT_MODEL;
				$data = $defobj->getVar('data_data');
				$form->add(new HiddenFormField('data_id',$defobj->getVar('data_id'),array(
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_id')
				)));
				$form->add(new HiddenFormField('data_type','productmodelitem'));
				$form->add(new HiddenFormField('data_parent',$request['parent']));
				$form->add(new HiddenFormField('data_objid',$id));
				$form->add(new TextFormField('itemtitle',$defobj->getVar('data_data')['itemtitle'],array(
					'title'=>PRODUCT_MODELITEM_TITLE,
					'length'=>255,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','itemtitle')
				)));
				$form->add(new HtmleditorFormField('itemcontent',$defobj->getVar('data_data')['itemcontent'],array(
					'title'=>PRODUCT_MODELITEM_CONTENT,
					'length'=>100,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','itemcontent')
				)));

				$subform = new Form('','','');
				$subform->add(new TextFormField('itemfiletitle',PRODUCT_MODELITEM_TECHSPECS,array(
					'title'=>'Titre du lien',
					'length'=>255,
					'lang' => $defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','doesnotexists','Technical specifications for option')
				)));
				$subform->add(new FileuploadFormField('itemfile','',array(
					'title'=>'Fiche technique',
					'length'=>255,
					'lang' => $defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','doesnotexists')
				)));
				$subformval = array();
				foreach ($obj as $k => $v) {
					if ($langs[$v->getVar('data_language')] != $defaultlang['code']) {
						$subformval = array_merge($subformval,array('itemfile_'.$langs[$v->getVar('data_language')] => $v->getVar('data_data')['itemfile'],'itemfiletitle_'.$langs[$v->getVar('data_language')] => $v->getVar('data_data')['itemfiletitle']));
					}
					else $subformval = array_merge($subformval,array('itemfile' => $v->getVar('data_data')['itemfile'],'itemfiletitle' => $v->getVar('data_data')['itemfiletitle']));
				}
				$form->add(new SubformFormField('itemfiles',$subformval,array(
					'form' => $subform,
					'title' => 'Fiches techniques',
					'labelfield' => 'itemfiletitle',
					'deletelabel' => _DELETE,
					'editlabel' => _EDIT,
					'newlabel' => _NEW,
				)));

				$form->addSaveButton('save',_SAVE.' '.PRODUCT_MODELITEM);
				$form->setVar('title',$title);
				echo json_encode(array('success'=>1,'form'=>$form->renderArray()));
				die();
			}break;
			case '/'.$service->get('Language')->getCode().'/json/admin/product/modelitem/save' : {
				header('Content-type: application/json');
				$saveRequest = new RequestToObject('data');
				$resp = $saveRequest->save();
				$resp = $store->saveOptions('data_data',array(
					'itemcontent','itemtitle','itemfiletitle','itemfile'
				));
				if ($resp || $resp == array()) echo json_encode(array('success'=>1, 'message' => _SAVED));
				else echo json_encode(array('success' => 0, 'message' => _SAVEERROR));
				die();
			}break;
			case '/'.$service->get('Language')->getCode().'/json/admin/product/modelitem/delete' : {
				header('Content-type: application/json');
				$defaultlang = $service->get('Language')->getDefault();
				$langs = $service->get('Language')->getCodes();
				if ($request['id']) {
					$resp = $store->delete($request['id']);
				}else {
					$resp = false;
				}
				if ($resp || $resp == array()) echo json_encode(array('success'=>1, 'message' => _SAVED));
				else echo json_encode(array('success' => 0, 'message' => _SAVEERROR));
				die();
			}break;




			default : break;
		}
	}

	public function getContentObject($name = false){
		global $service;
		if ($name == false) {
			$request = $service->get('Request')->get();
			$name = $request['type'];
		}
		$ctCls = ucfirst($name).'Content';
		$service->get('Ressource')->get('com/content/'.strtolower($name));
		if (class_exists($ctCls) && $name != '') $obj = new $ctCls();
		else $obj = false;
		return $obj;
	}
}
?>
