<?php
/**
 * Base class to handle databases.
 *
 * Handles queries to the database of the choosen type
 *
 * June 23, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

class Db extends BaseClass {

	/**
	 * The database handler for the choosen database type
	 *
	 * @var object $hander;
	 */
	var $handler;

	/**
	 * The host to which to connect to
	 *
	 * @var string $host
	 */
	var $host;

	/**
	 * The user name used to establish the database connection
	 *
	 * @var string $user
	 */
	var $user;

	/**
	 * The password to use to connect to the DB
	 *
	 * @var string $pass
	 */
	var $pass;

	/**
	 * The database name to which we are about to connect
	 *
	 * @var string $db
	 */
	var $db;

	/**
	 * The query count
	 *
	 * @access private
	 *
	 * @var int $queryCount
	 */
	private $queryCount;

	/**
	 * Sum of all queries execution time
	 *
	 * @access private
	 *
	 * @var int $totalTime;
	 */
	private $totalTime;

	function __construct(){
		global $service;
		$log =& $service->get('log');
		$this->host = DB_HOST;
		$this->user = DB_USER;
		$this->pass = DB_PASS;
		$this->db = DB_NAME;
		$this->queryCount = 0;
		$this->totalTime = 0;

		if (file_exists(dirname(__FILE__).'/db/db/'.strtolower(DB_TYPE).'database.php')) {
			include_once(dirname(__FILE__).'/db/db/'.strtolower(DB_TYPE).'database.php');
			$clsName = strtolower(DB_TYPE).'Database';
			$this->handler = new $clsName(DB_HOST,DB_USER,DB_PASS,DB_NAME);
			unset($clsName);
		}else {
			$log->add("Database of type $type are not supported. In order to support this kind of database, create a database class named $type.php in the folder /core/db.",__FILE__,__LINE__,E_ERROR,LOG_FILE_DATABASE);
		}
		if (!$this->connect($this->host,$this->user,$this->pass, $this->db)){
			$log->add("Could not connect to DB. Check DB connection settings in config.php. Aborting in file core/db.php.",__FILE__,__LINE__,E_ERROR,LOG_FILE_DATABASE);
			echo '<br>Could not connect to DB. Check logs for details.';
			die;
		}
	}

	/**
	 * Establish connection to a mysql database
	 *
	 * @method bool connect()
	 *
	 * @var string $host
	 * @var string $user
	 * @var string $pass
	 * @var string $db
	 */
	function connect($host, $user, $pass, $db) {
		if (is_object($this->handler)) return $this->handler->connect($host, $user, $pass, $db);
		else return false;
	}

	/**
	 * Query a database and return result set
	 *
	 * @method array query()
	 *
	 * @var string $query
	 */
	function query($query,$params = array()){
		global $service;
		$this->queryCount++;
		$service->get('Time')->startTimer('db.query');
        foreach ($query['params'] as $k => $v) {
            if (!isset($v['paramname']) && !isset($v['value'])) {
                 $query['params'][$k] = array('paramname' => $k, 'value' => $v);
            }
        }
		$ret = $this->handler->query($query);
		$timespent = $service->get('Time')->stopTimer('db.query');
		$this->totalTime += $timespent;
		if (is_array($query)) {
			$tmp = $query['query'].' ';
			if (isset($query['params']) && is_array($query['params'])) {
				foreach($query['params'] as $k => $v) {
					$tmp = str_replace($v['paramname'].' ','\''.$v['value'].'\' ',$tmp);
					$tmp = str_replace($v['paramname'].')','\''.$v['value'].'\')',$tmp);
					$tmp = str_replace($v['paramname'].',','\''.$v['value'].'\',',$tmp);
				}
			}
			$query = $tmp;
		}
		$msg = "Query executed in $timespent seconds : $query";
		if ($this->handler->getErrorCode() != 0){
			$msg .= "\n".'ERROR: ['.$this->handler->getErrorCode().'] '.$this->handler->getErrorMessage();
		}
		//echo $msg.'<br>';
		$service->get('Log')->add($msg,__FILE__,__LINE__,E_INFO,LOG_FILE_DATABASE);
		return $ret;
	}

	/**
	 * Adds the DB prefix to a table name
	 *
	 * @method string prefix()
	 *
	 * @var string $str
	 */
	function prefix($str=''){
		if (DB_PREFIX != '') return DB_PREFIX.'_'.$str;
		return $str;
	}

	/**
	 * Gets the number of queries executed since this obj initialization
	 *
	 * @access public
	 *
	 * @return int Number of queries executed since this DB obj initialization
	 */
	public function getQueryCount(){
		return $this->queryCount;
	}

	/**
	 * Gets the total number of seconds that all queries took together to run
	 *
	 * @access public
	 *
	 * @return int Number of seconds that all queries took together to run.
	 */
	public function getTotalTime(){
		return $this->totalTime;
	}

	/**
	 * Gets the last insert ID
	 *
	 * @return int lastid
	 */
	public function getLastInsertId(){
		return $this->handler->getLastInsertId();
	}

	/**
	 * Gets the last error message
	 *
	 * @return string Error message
	 */
	public function getError(){
		return $this->handler->getErrorMessage();
	}

	/**
	 * Gets the last error code
	 *
	 * @return string Error code
	 */
	public function getErrorCode(){
		return $this->handler->getErrorCode();
	}
}
?>
