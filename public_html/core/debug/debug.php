<?php
/**
 * This file is loaded when DEBUG constant is defined, enabling the debug mode.
 *
 * All debug mode related code should be in here.
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//@TODO : Find a way to type a simple function that prints out a nice debug output where you want it
//In this case get_defined_vars works, but it needs to be called directly in same scope as where you want it....
function debug(){
    print_r(get_defined_vars());die;
}
?>
