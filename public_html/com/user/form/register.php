<?php
/**
 * Registration form
 *
 * Handles registration form
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('core/display/form/field');
$service->get('Ressource')->get('com/user/lang/'.$service->get('Language')->getCode().'/user');

class RegisterForm extends Form{
    var $values;

	public function buildForm(){
        global $service;
		$this->data['layout'] = 'vertical';
		$this->data['method'] = 'POST';
		$this->data['class'] = 'userregisterform';
		$this->data['tabs'] = '';
		$this->data['tabssuffix'] = uniqid();
		$this->data['prefix'] = '';
		$this->data['prefixexceptions'] = array('id','op','name','type');
		$this->setAction(URL.$service->get('Language')->getCode().'/user/register');
		$this->setName('userregisterform');
		$this->setTitle(LOGIN_USERREGISTER_TITLE);
		$this->setMethod($method);
		$this->add(new HiddenFormfield('op','register'));
		$this->add(new TextFormField('name',$this->getValue('name'), array(
			'title'=>USER_FORMFIELD_DISPLAYNAME,
			'help' => sprintf(USER_HELP_DISPLAYNAME,3,20),
			'validate' => [
				['method' => 'length', 'min' => 3, 'max' => 20, 'message' => sprintf(USER_VALIDATE_DISPLAYNAME_ERROR,3,20)]
			]
		)));
		$this->add(new TextFormField('email',$this->getValue('email'), array(
			'title'=>USER_FORMFIELD_EMAIL,
			'help' => USER_HELP_EMAIL,
			'validate' => [
				['method' => 'email', 'message' => USER_VALIDATE_EMAIL_ERROR]
			]
		)));
		$this->add(new PasswordFormField('pass',$this->getValue('pass'),array(
			'title'=>USER_FORMFIELD_PASS,
			'help'=>USER_HELP_PASS,
			'validate' => [
				['method' => 'length', 'min' => 0, 'max' => 8, 'invert' => 1, 'message' => LOGIN_PASSHELP]
			]
		)));
		$this->add(new PasswordFormField('pass2',$this->getValue('pass2'),array(
			'title'=>USER_FORMFIELD_PASS2,
			'help'=>USER_HELP_PASS2,
			'validate' => [
				['method' => 'length', 'min' => 0, 'max' => 8, 'invert' => 1, 'message' => LOGIN_PASSHELP]
			]
		)));
		$this->add(new SubmitFormField('submit','',array('title' => USER_REGISTER_ACTION,'width'=>2)));
	}

    public function setValue($field,$val){
        $this->values[$field] = $val;
    }
    public function getValue($field){
        return isset($this->values[$field])?$this->values[$field]:'';
    }
}
?>
