<?php
global $service;
$service->get('Ressource')->get('com/user/lang/'.$service->get('Language')->getCode().'/user');
$service->get('EventHandler')->on('login.onPageload',
	function($e,$p){
		global $service;
		$authentified = $service->get('User')->isLogged();
		if (!$authentified) return '<section class="register"><div class="ct"><h1>'.REGISTER_TITLE.'</h1><p>'.sprintf(REGISTER_LINK,'<a href="'.URL.'en/user/register">','</a>').'</div></section>';
		return '';
	}
);
?>