<?php
global $service;
$service->get('Ressource')->get('com/user/lang/'.$service->get('Language')->getCode().'/user');
$service->get('Ressource')->get('core/display/template/template');
$service->get('Ressource')->get('core/protocol/email');

$service->get('Event')->on('user.register',
	function($e,$p){
		global $service;
		$request = $service->get('Request')->get();
		$pass = '';
		if (isset($request['pass'])) $pass = $request['pass'];
		if (isset($request['user_pass'])) $pass = $request['user_pass'];
		if ($pass == '********' || $pass == '') return $p;

		$tpl = new Template('com/user/templates/emailinvite.tpl');
		$tpl->assign('user',$p->getVars());
		$tpl->assign('sitename',$service->get('Setting')->get('sitename'));
		$tpl->assign('textpassword',$pass);

		$from = $service->get('Setting')->get('adminmail');
		$to = $p->getVar('user_email');
		$subject = 'Invitation to '.$service->get('Setting')->get('sitename');
		$body = $tpl->render();

		$email = new Email();
		$email->html(true);
		$email->from($from);
		$email->to($to);
		$email->subject($subject);
		$email->body($body);
		$res = $email->send();
		return $p;
	}
);
?>
