<style type="text/css">
.chosen-container-single .chosen-single{
	border-top-right-radius: 0!important;
	border-bottom-right-radius: 0!important;
	height:34px!important;
	padding-top:4px!important;
}
.chosen-container-single .chosen-single div b{
	background-position:0px 5px;
}
</style>

{:if $isdialog:}
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title">{:if $record.user_id > 0:}Edit User{:else:}Add User{:/if:}</h4>
</div>
<div class="modal-body">
{:else:}
<ul class="breadcrumb">
	<li><a href="{:$url:}">Home</a></li>
	<li><a href="#" onclick="loadScreen('users_list');">Users</a></li>
	<li class="active">{:if $record.user_id > 0:}Edit{:else:}Add{:/if:} User</li>
</ul>
<div class="panel panel-default">
    <div class="panel-heading"><h1 class="panel-title">{:if $record.user_id > 0:}Edit{:else:}Add{:/if:} User</h1></div>
	<div class="panel-body">
{:/if:}

<form class="form-vertical" id="userform" action="{:$url:}en/api/user/controller/usermanager/saveuser">
	<input type="hidden" name="user_id" id="user_id" value="{:$record.user_id:}">
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="user_name">User name</label>
				<input type="text" class="form-control" id="user_name" name="user_name" placeholder="User name" value="{:$record.user_name:}" tabindex="1">
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label for="user_email">Email</label>
				<input type="text" class="form-control" id="user_email" name="user_email" placeholder="Email address" value="{:$record.user_email:}" tabindex="1">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="user_pass">Pasword</label>
				<input type="password" class="form-control" id="user_pass" name="user_pass" placeholder="Enter password" value="{:if $record.user_id > 0:}********{:/if:}" tabindex="1">
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label for="user_pass2">Pasword (re-type)</label>
				<input type="password" class="form-control" id="user_pass2" name="user_pass2" placeholder="Re-type password" value="{:if $record.user_id > 0:}********{:/if:}" tabindex="1">
			</div>
		</div>
	</div>
	<div class="row">
		{:if $isadmin:}
		<div class="col-sm-6">
			<div class="form-group">
				<label for="user_groups">User privileges</label>
				<select class="form-control" name="user_groups" id="user_groups">
					<option value=",2,3,"{:if $record.usertype == 'user':} selected{:/if:}>Regular user</option>
					<option value=",1,2,3,"{:if $record.usertype == 'admin':} selected{:/if:}>Administrator</option>
				</select>
			</div>
		</div>
		{:/if:}
		<div class="col-sm-{:if $isadmin:}6{:else:}12{:/if:}">
			<div class="form-group">
				<label for="user_timezone">User Timezone</label>
				<select name="user_timezone" class="form-control" id="user_timezone">
					{:foreach item=tz from=$timezones:}
					<option value="{:$tz:}"{:if $record.user_timezone == $tz:} selected{:/if:}>{:$tz:}</option>
					{:/foreach:}
				</select>
			</div>
		</div>
	</div>
{:if $isdialog:}
	</div>
	<div class="modal-footer">
		<button type="cancel" class="btn btn-default btn-secondary" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
		<button type="submit" class="btn btn-default btn-primary" onclick="sendForm('userform','{:if $isadmin:}users_list{:/if:}');"><span class="glyphicon glyphicon-floppy-open"></span> Save</button>
	</div>
{:else:}
	<div class="panel-footer clearfix">
		<div class="pull-right">
			<button type="cancel" class="btn btn-default btn-secondary"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
			<button type="submit" class="btn btn-default btn-primary" onclick="sendForm('userform','{:if $isadmin:}users_list{:/if:}');"><span class="glyphicon glyphicon-floppy-open"></span> Save</button>
		</div>
	</div>
</div>
</div>
{:/if:}

</form>
<script language="javascript" type="text/javascript">
	//$('#user_timezone').chosen();
	$('#brokernotes_dialog').on('hidden.bs.modal', function (e) {
		$(e.target).removeData("bs.modal").find(".modal-content").empty();
	});
    $('#userform').bootstrapValidator({
        message: 'Value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            user_name: {
                trigger: "focus blur change submit",
                validators: {
                    notEmpty: {
                    },
                    stringLength: {
                        min: 3,
                        max: 100,
						message: 'Username must be 4 to 12 caracters'
                    },
       				remote: {
						url: '{:$url:}en/api/user/controller/usermanager/validateusername',
						data: function(validator) {
							return {
                                v: $('#user_name').val(),
                                id: $('#user_id').val(),
							};
						},
						message: 'Username not available'
					}
				}
            },
            user_email: {
                trigger: "focus blur change",
                validators: {
                    notEmpty: {
                    },
                    email: {
                    },
					remote: {
                        url: '{:$url:}en/api/user/controller/usermanager/validateuseremail',
                        data: function(validator) {
                            return {
                                v: $('#user_email').val(),
                                id: $('#user_id').val(),
                            };
                        },
                        message: 'Email address is in use'
                    }
                }
            },
            user_pass: {
                trigger: "focus blur change",
                validators: {
                    notEmpty: {
                    },
                    stringLength: {
                        min: 8,
                        max: 100,
						message: 'Username must be at least 8 caracters'
                    },
                    identical: {
                        field: 'user_pass2',
                        message: 'Both passwords fields must be the same'
                    }
                }
            },
            user_pass2: {
                trigger: "focus blur change",
                validators: {
                    notEmpty: {
                    },
                    stringLength: {
                        min: 8,
                        max: 100,
						message: 'Username must be at least 8 caracters'
                    },
                    identical: {
                        field: 'user_pass',
                        message: 'Both passwords fields must be the same'
                    }
                }
            }
        }
    });
</script>
