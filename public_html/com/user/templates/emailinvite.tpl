<p>Dear {:$user.user_name:},</p>

<p>You are invited to join the community at {:$url:}</p>
<p>Here is your login information :</p>
<p>Login (email address): {:$user.user_email:}</p>
<p>Password: {:$textpassword:}</p>

<p></p>
<p>Click here to join: <a href="{:$url:}" target="_blank">{:$url:}</a></p>