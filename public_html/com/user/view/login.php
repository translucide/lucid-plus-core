<?php
/**
 * Login route view
 *
 * Default view loaded on login page request
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/view');

class LoginView extends DefaultView{
	public function init(){
	}

	public function render(){
		$code .= '<article class="user login user-login'.$this->data['element']['class'].'">';
		$code .= '<h1>'.LOGIN_TITLE.'</h1>';
		$code .= '<section class="loginform"><div class="ct">';
		$code .= $this->data['form'];
		$code .= '</div></section>';
		if (isset($this->data['addons'])) $code .= implode('',$this->data['addons']);
		$code .= '</article>';
		return $code;
	}
}
?>
