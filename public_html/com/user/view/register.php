<?php
/**
 * User registration route view
 *
 * Default view loaded on user registration page request
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('com/view/default');

class RegisterView extends DefaultView{
	public function init(){
	}
	
	public function render(){
		$code .= '<article class="user register user-register'.$this->data['element']['class'].'">';
		$code .= '<h1>'.REGISTER_TITLE.'</h1>';
		$code .= '<section class="registerform"><div class="ct">';
		$code .= $this->data['form'];
		$code .= '</div></section>';
		$code .= implode('',$this->data['addons']);
		$code .= '</article>';
		return $code;
	}
}
?>