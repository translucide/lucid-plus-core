<?php
define('USER_USERPROFILE_PAGETITLE','Paramêtres du compte');
define('USER_USERPROFILE_DISPLAYNAME','Nom affiché');
define('USER_USERPROFILE_EMAIL','Adresse courriel');
define('USER_USERPROFILE_PASS','Mot de passe');
define('USER_USERPROFILE_PASS_HELP','Seuls les mots de passe contenant 8 caractères et plus seront acceptés. Laisser vide pour conserver votre mot de passe actuel.');
define('USER_USERPROFILE_PASS2','Confirmer le mot de passe');

define('USER_USERPROFILE_EDIT','Modifier');
define('USER_USERPROFILE_SAVE','Enregistrer');

define('LOGIN_TITLE','Connexion');
define('LOGIN_INTRO','Entrer votre adresse courriel et votre mot de passe pour vous connecter');
define('LOGIN_DISPLAYNAME','Nom affiché');
define('LOGIN_EMAIL','Adresse courriel');
define('LOGIN_PASS','Mot de passe');
define('LOGIN_PASSHELP','Seuls les mots de passe contenant 8 caractères et plus seront acceptés.');
define('LOGIN_PASSAGAIN','Confirmer le mot de passe');
define('LOGIN_SUBMIT','Connexion');
define('LOGIN_LOGOUTTITLE','Fermer la session');
define('LOGIN_LOGOUTINTRO','Vous êtes présentement connecté. Cliquez sur le bouton déconnexion pour fermer la session utilisateur.');
define('LOGIN_LOGOUTSUBMIT','Logout');
define('LOGIN_USERMENUTITLE','Mon compte');
define('LOGIN_USERMENU_SETTINGS','Paramêtres du compte');
define('LOGIN_USERMENU_PROFILE','Mon profil');
define('LOGIN_USERMENU_ADMINSECTION','Administration');
define('LOGIN_USERMENU_ADMINAREA','Interface administrateur');
define('LOGIN_USERACCOUNT_TITLE','Paramêtres du compte');
define('LOGIN_USERACCOUNT_INTRO','Modifiez votre adresse courriel principale, votre mot de passe et votre nom d\'utilisateur à l\'aide du formulaire ci-bas.');
define('LOGIN_USERACCOUNT_INTROSAVE','Changements enregistrés.');
define('LOGIN_USERACCOUNT_SAVE','Enregistrer les paramêtres');
define('LOGIN_USERACCOUNT_ERROR_INUSE','Le nom d\'utilisateur ou l\'adresse courriel spécifiée est en cours d\'utilisation. Veuillez utiliser un autre nom ou adresse courriel.');
define('LOGIN_USERACCOUNT_ERROR_SAVEERROR','Impossible de sauvegarder les informations. Veuillez réessayer plus tard.');
define('LOGIN_USERREGISTER_TITLE','Créer un nouveau compte.');
define('LOGIN_USERREGISTER_INTRO','Veuillez remplir les champs suivants afin de créer un nouveau compte');
define('LOGIN_USERREGISTER_SAVE','Créer mon compte');
define('LOGIN_USERREGISTER_PASSDONOTMATCH','Les mots de passe ne correspondent pas. Veuillez entrer un mot de passe identique dans les deux champs prévus à cet effet.');
define('LOSTPASSWORD_LOGINFORMTEXT','Mot de passe perdu? %sRécupérer mon mot de passe.%s');
define('REGISTER_TITLE','Créer un compte');
define('REGISTER_LINK','Vous n\'avez pas de compte sur notre site? %s Cliquez ici afin de créer un nouveau compte. %s');
define('REGISTER_INTRO','Veuillez remplir le formulaire suivant afin de créer votre compte.');
define('REGISTER_EMAIL','Adresse courriel');
define('REGISTER_PASS','Mot de passe');
define('REGISTER_PASS2','Confirmer le mot de passe');
define('REGISTER_SUBMIT','Créer mon compte');
define('REGISTER_SUCCESS','Compte créé avec succès.');

?>