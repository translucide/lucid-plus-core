<?php
define('USER_USERPROFILE_TITLE','My Account');
define('USER_USERPROFILE_PAGETITLE','Account settings');
define('USER_USERPROFILE_DISPLAYNAME','Display name');
define('USER_USERPROFILE_EMAIL','Email');
define('USER_USERPROFILE_PASS','Password');
define('USER_USERPROFILE_PASS_HELP','Only password containing 8 and more caracters will be accepted. Leave empty to keep your current password.');
define('USER_USERPROFILE_PASS2','Confirm password');

define('USER_USERPROFILE_EDIT','Edit');
define('USER_USERPROFILE_SAVE','Save changes');

define('LOGIN_TITLE','Login');
define('LOGIN_INTRO','Enter your email address and your password to login');
define('LOGIN_DISPLAYNAME','Display name');
define('LOGIN_EMAIL','Email address');
define('LOGIN_PASS','Password');
define('LOGIN_PASSHELP','Only passwords longer than 8 caracters are accepted.');
define('LOGIN_PASSAGAIN','Password (again)');
define('LOGIN_SUBMIT','Login');
define('LOGIN_LOGOUTTITLE','Logout');
define('LOGIN_LOGOUTINTRO','You are currently logged in. Click the logout button below to close your user session.');
define('LOGIN_LOGOUTSUBMIT','Logout');
define('LOGIN_USERMENUTITLE','My Account');
define('LOGIN_USERMENU_SETTINGS','Account settings');
define('LOGIN_USERMENU_PROFILE','My profile');
define('LOGIN_USERMENU_ADMINSECTION','Administration');
define('LOGIN_USERMENU_ADMINAREA','Administrative area');
define('LOGIN_USERACCOUNT_TITLE','Account settings');
define('LOGIN_USERACCOUNT_INTRO','Here you can change your account email address, password and user name.');
define('LOGIN_USERACCOUNT_INTROSAVE','Changes were saved successfully.');
define('LOGIN_USERACCOUNT_SAVE','Save settings');
define('LOGIN_USERACCOUNT_ERROR_INUSE','Username or email already in use by someone else. Try using another username or email address');
define('LOGIN_USERACCOUNT_ERROR_SAVEERROR','Could not save information to the database. Please try agin later.');
define('LOGIN_USERREGISTER_TITLE','Create a new account');
define('LOGIN_USERREGISTER_INTRO','Fill in the following fields to create a new account');

define('LOGIN_USERREGISTER_PASSDONOTMATCH','Both password fields do not match. Please type your password twice in both password fields.');
define('LOSTPASSWORD_LOGINFORMTEXT','Lost your password? %sClick here to change it for a new one.%s');
define('REGISTER_TITLE','Register');
define('REGISTER_LINK','Don\'t have an account on our site yet? %s Click here to create one. %s');
define('REGISTER_INTRO','Fill in this form in order to create your user account.');
define('REGISTER_EMAIL','Email address');
define('REGISTER_PASS','Type a password');
define('REGISTER_PASS2','Retype your passord');
define('REGISTER_SUBMIT','Register');
define('REGISTER_SUCCESS','Account created successfully.');

define('USER_SUCCESS','Modifications applied successfully!');
define('USER_ERROR_DISPLAYNAME','The display name you have choosen is in use by some one else, is reserved to our staff or contains forbidden language. Please try again by choosing another display name.');
define('USER_ERROR_EMAIL','The email address you have choosen is in use by some one else or is invalid.');
define('USER_ERROR_PASS','Both passwords must be the same.  Passwords minimum length is 8 caracters. Leave blank to keep your current password.');
define('USER_ERROR_NOTFOUND','User profile not found ! Please contact the site administrator.');
define('USER_ERROR_CANNOTSAVE','Could not save modifications to DB. Please retry later. If it does not fix the problem, please contact the site administrator.');

/**
 * Fields validation errors across user module
 */
define('USER_VALIDATE_DISPLAYNAME_ERROR','Display name should have between %s and %s caracters');
define('USER_VALIDATE_EMAIL_ERROR','Invalid email address.');
define('USER_VALIDATE_PASSWORD_ERROR','Both passwords must be the same. Leave blank to keep current password. Passwords must have minimum 8 caracters.');

/**
 * Form field names
 */
define('USER_FORMFIELD_DISPLAYNAME','Display name');
define('USER_FORMFIELD_EMAIL','Email address');
define('USER_FORMFIELD_PASS','Password');
define('USER_FORMFIELD_PASS2','Confirm password');

/**
 * Form field help tips
 */
define('USER_HELP_DISPLAYNAME','Display name should have between %s and %s caracters.');
define('USER_HELP_EMAIL','Enter a valid email address.');
define('USER_HELP_PASS','Both passwords must be the same. Leave blank to keep current password. Passwords must have minimum 8 caracters. Password is case sensitive.');
define('USER_HELP_PASS2','Type your password exactly as you typed it in the first password field.');

/**
 * Action buttons
 */
define('USER_REGISTER_ACTION','Create account');
?>
