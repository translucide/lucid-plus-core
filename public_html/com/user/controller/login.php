<?php
/**
 * Login plugin controller
 *
 * Handles login requests
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/controller');
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('com/user/lang/'.$service->get('Language')->getCode().'/user');

class LoginController extends Controller{
	public function init(){
		$this->component = 'user';
	}

	public function execute(){
		global $service;
		$url = $service->get('Url')->get();
		$view = $this->getView();
		$authentified = $service->get('User')->isLogged();
		switch($url['path']) {
			//Logout: set form accordingly and exit.
			case 'user/logout' : {
				if ($authentified) return $this->logout();
				else $this->loginForm();
			}break;
			case 'user/login' : {
				if ($authentified) {
					$this->redirect();
					header('Location: '.URL.$service->get('Language')->getCode().'/user/profile');
					exit;
				}
				else {
					$login = $this->login();
					return $this->loginForm();
				}
			}break;
		}
		return $view;
	}

	/**
	 * Attempts to login user if user & pass are present in request.
	 * If login is successful, it redirects the user at $request['redirect']
	 * location.
	 *
	 * @access private
	 */
	private function login() {
		global $service;
		$request = $service->get('Request')->get();
		if (isset($request['user']) && isset($request['pass'])) {
			$res = $service->get('User')->login($request['user'],$request['pass'],(isset($request['tenant']))?$this->getTenantId($request['tenant']):0);
			if ($res) {
				$this->redirect();
				header('location: '.URL);
				exit;
			}
		}
	}

    private function getTenantId($tenantName= '') {
        if ($tenantName != '') {
            global $service;
            $tenantStore = new TenantStore();
            $res = $tenantStore->getByName($tenantName);
            return (is_object($res))?$res->getVar('tenant_id'):0;
        }
        return 0;
    }

	private function loginForm(){
		global $service;
		$request = $service->get('Request')->get();
		$view = $this->getView();
		$name = 'loginform';
		$service->get('Theme')->setTitle(LOGIN_TITLE);
		$form = new Form(URL.$service->get('Language')->getCode().'/user/login',$name,'','POST');
		$form->option('intro',LOGIN_INTRO);
		$form->add(new HiddenFormfield('op','login'));
		$form->add(new HiddenFormfield('redirect',((isset($request['redirect']))?$request['redirect']:'')));
		$form->add(new TextFormfield('user','',array('title' => LOGIN_EMAIL)));
		$form->add(new PasswordFormfield('pass','',array('title' => LOGIN_PASS)));
		$form->add(new SubmitFormField('submit','',array('title' => LOGIN_SUBMIT,'width'=>2)));
		$form = $form->render();
		$view->setVar('form',$form);
		//Get other plugins that may want to display things here.
		$addons = $service->get('EventHandler')->trigger('user.login');
		$view->setVar('addons',$addons);
		return $view;
	}

	private function logout() {
		global $service;
		$request = $service->get('Request')->get();
		$service->get('User')->logout();
		$this->redirect();
		header('Location: '.URL);
		exit;
	}
	private function logoutForm(){
		global $service;
		$request = $service->get('Request')->get();
		$view = $this->getView();
		$name = 'logoutform';
		$service->get('Theme')->setTitle(LOGIN_LOGOUTTITLE);
		$form = new Form(URL.$service->get('Language')->getCode().'/user/logout',$name,'','POST');
		$form->option('intro',LOGIN_LOGOUTINTRO);
		$form->add(new HiddenFormfield('op','logout'));
		$form->add(new HiddenFormfield('redirect',((isset($request['redirect']))?$request['redirect']:'')));
		$form->add(new SubmitFormField('submit','',array('title' => LOGIN_LOGOUTSUBMIT)));
		$form = $form->render();
		$view->setVar('form',$form);

		//Get other plugins that may want to display things here.
		$addons = $service->get('EventHandler')->trigger('user.logout');
		$view->setVar('addons',$addons);
		return $view;
	}

	private function redirect() {
		global $service;
		$request = $service->get('Request')->get();
		$redirect = str_replace(URL,'',$service->get('Request')->get('redirect'));
		if (isset($request['redirect'])) {
			header('Location: '.URL.$redirect);
			exit;
		}
	}

	/**
	 * Returns the corresponding URL in the language ID given
	 * or simply returns the home page url if none found
	 *
	 * @public
	 * @param string $url The url for which we need the translation
	 * @param int $langid The language ID to find the URL for
	 * @return string The translated URL in the language we asked for or false if not found.
	 */
	public function translate($langid, $objid = 0, $url=''){
		global $service;
		return $service->get('Language')->getCodeForId($langid).'/login';
	}
}
?>
