<?php
/**
 * User account plugin controller
 *
 * Handles login requests
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/controller');
$service->get('Ressource')->get('core/model/profile');
$service->get('Ressource')->get('core/model/profilefield');
$service->get('Ressource')->get('core/model/profilesection');
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('core/display/form/field');
$service->get('Ressource')->get('com/user/lang/'.$service->get('Language')->getCode().'/user');

class ProfileController extends Controller{
	public function init(){
		$this->component = 'user';
	}

	public function execute(){
		global $service;
		$request = $service->get('Request')->get();
		$view = $this->getView();

		//Prevent unlogged people from viewing this page.
		if ($service->get('User')->isLogged() == false) {
			@header('Location: '.URL.$service->get('Language')->getCode().'/user/login');
			exit;
		}

		$service->get('Theme')->setTitle(USER_USERPROFILE_PAGETITLE);
		$form = new Form(URL.$service->get('Language')->getCode().'/user/profile','profile',USER_USERPROFILE_PAGETITLE,'POST');
		$errorfound = false;
		$errormessage = '';

		switch($request['op']) {
			case 'edit':
			case 'save': {
				if (!isset($request['name']) && !isset($request['email'])) {
					$form = $this->getEditForm($form);
                    $form = $service->get('EventHandler')->trigger('user.editprofile',['form' => $form],true);
                    $form = $form['form'];
				}else {
					$messages = array();

					$name = trim($service->get('Request')->get('name'));
					$email = trim($service->get('Request')->get('email'));
					$pass = trim($service->get('Request')->get('pass'));
					$pass2 = trim($service->get('Request')->get('pass2'));

					$userStore = new UserStore();
					$user = $userStore->get(new Criteria('user_id',$service->get('User')->getUid()));
					if (count($user) == 1) {
						$user = $user[0];

						//1. Check if username is in use
						$crit = new CriteriaCompo();
						$crit->add(new Criteria('user_name',$name));
						$crit->add(new Criteria('user_id',$service->get('User')->getUid(),'!='));
						$inuse = $userStore->count($crit);
						$forbidden = explode(',',SANITIZER_FORBIDDEN_USER_NAMES);
						$restricted = explode(',',SANITIZER_RESTRICTED_USER_NAMES_PARTS);
						if (($service->get('User')->isAdmin()) ||
							(!$inuse && in_array($name,$forbidden) === false && str_replace($restricted,'',$name) == $name)
							) {
							$user->setVar('user_name',$name); //cancel name change
						}else {
							$messages[] = USER_ERROR_DISPLAYNAME;
						}

						//2. Check if email is in use
						$crit = new CriteriaCompo();
						$crit->add(new Criteria('user_email',$email));
						$crit->add(new Criteria('user_id',$service->get('User')->getUid(),'!='));
						$inuse = $userStore->count($crit);
						if (!$inuse) {
							$user->setVar('user_email',$email); // Cancel email change.
						}else {
							$messages[] = USER_ERROR_EMAIL;
						}

						//3. Change password
						if (strlen($pass) >= 8 && $pass == $pass2) {
							if ($pass != '********') {
								$pass = $service->get('User')->encryptPassword($pass);
								$user->setVar('user_pass',$pass['password']);
								$user->setVar('user_salt',$pass['salt']);
								$user->setVar('user_encryptionmethod',(defined('USER_CRYPTOALGORITHM'))?USER_CRYPTOALGORITHM:'scrypt');
							}
						}else {
							if ($pass != ''  || $pass2 != '') $messages[] = USER_ERROR_PASS;
						}

						$ok = $userStore->save(array($user));
						if (!$ok) {
							$messages[] = USER_ERROR_CANNOTSAVE;
						}

						//4. Save extra profile fields
						$profileStore = new ProfileStore();
						$profile = $profileStore->getByUser($service->get('User')->getUid());
						foreach ($profile as $k => $v) {
							if (isset($request[$v->getVar('profile_key')])) {
								$profile[$k]->setVar('profile_value',$request[$v->getVar('profile_key')]);
							}
						}
						$ok = $profileStore->save($profile);
						if (!$ok) {
							$messages[] = USER_ERROR_CANNOTSAVE;
						}

                        //5. Reload user data used by the edit form
                        $service->get('User')->reload();

                        //6. Trigger onsave event
                        $service->get('EventHandler')->trigger('user.saveprofile');
					}else {
						$messages[] = USER_ERROR_NOTFOUND;
					}

					if (count($messages) > 0) {
						$form->option('intro','<p class="msg error">'.implode('</p><p class="msg error">',$messages).'</p>');
						$form = $this->getEditForm($form);
					}
					else {
						$form->option('intro','<p class="msg success">'.USER_SUCCESS.'</p>');
						$form = $this->getEditForm($form);
					}
				}
			}break;
			default: {
				$form->add(new HiddenFormfield('op','edit'));
				$form->add(new LabelFormField('name',$service->get('User')->get('name'), array('title'=>USER_USERPROFILE_DISPLAYNAME)));
				$form->add(new LabelFormField('email',$service->get('User')->get('email'), array('title'=>USER_USERPROFILE_EMAIL)));
				//$form->add(new LabelFormField('pass','********', array('title'=>USER_USERPROFILE_PASS)));
				$form = $this->addProfileCustomFields($form,false);
				$form->add(new SubmitFormField('send','',array('title' => '<span class="glyphicon glyphicon-pencil"></span> '.USER_USERPROFILE_EDIT,'width'=> 2)));
				$this->syncProfileFields();
			}break;
		}
		$view->setVar('form',$form->render());

		//Get other plugins that may want to display things here.
		$addons = $service->get('EventHandler')->trigger('user.loadprofile');
		$view->setVar('addons',$addons);
		return $view;
	}

	/**
	 * Returns the corresponding URL in the language ID given
	 * or simply returns the home page url if none found
	 *
	 * @public
	 * @param string $url The url for which we need the translation
	 * @param int $langid The language ID to find the URL for
	 * @return string The translated URL in the language we asked for or false if not found.
	 */
	public function translate($langid, $objid = 0, $url=''){
		global $service;
		return $service->get('Language')->getCodeForId($langid).'/user/profile';
	}

	private function getEditForm($form){
		global $service;
		$form->option('layout','vertical');
		$form->add(new HiddenFormfield('op','save'));
		$form->add(new TextFormField('name',$service->get('User')->get('name'), array(
			'title'=>USER_USERPROFILE_DISPLAYNAME,
			'help' => sprintf(USER_VALIDATE_DISPLAYNAME_ERROR,3,20),
			'section' => 'section_default',
			'validate' => [
				['method' => 'length', 'min' => 3, 'max' => 20, 'message' => sprintf(USER_VALIDATE_DISPLAYNAME_ERROR,3,20)]
			]
		)));
		$form->add(new TextFormField('email',$service->get('User')->get('email'), array(
			'title'=>USER_USERPROFILE_EMAIL,
			'section' => 'section_default',
			'validate' => [
				['method' => 'email', 'message' => USER_VALIDATE_EMAIL_ERROR]
			]
		)));
		$form->add(new PasswordFormField('pass','********',array(
			'title'=>USER_USERPROFILE_PASS,
			'help'=>USER_USERPROFILE_PASS_HELP,
			'section' => 'section_default',
			'validate' => [
				['method' => 'length', 'min' => 0, 'max' => 7, 'invert' => 1, 'message' => USER_VALIDATE_PASSWORD_ERROR]
			]
		)));
		$form->add(new PasswordFormField('pass2','********',array(
			'title'=>USER_USERPROFILE_PASS2,
			'help'=>USER_USERPROFILE_PASS_HELP,
			'section' => 'section_default',
			'validate' => [
				['method' => 'length', 'min' => 0, 'max' => 7, 'invert' => 1, 'message' => USER_VALIDATE_PASSWORD_ERROR]
			]
		)));

		$form = $this->addProfileCustomFields($form);
		$form->add(new SubmitFormField('save','',array(
			'title' => '<span class="glyphicon glyphicon-save"></span> '.USER_USERPROFILE_SAVE,
			'section' => 'section_default',
			'width'=>3
		)));
		return $form;
	}

	private function addProfileCustomFields($form,$editmode=true){
		global $service;
		$uid = $service->get('User')->getUid();
		$db =& $service->get('Db');
		$px = $db->prefix('');
		$res = $db->query("
			SELECT
				`{$px}profile`.`profile_key` as `key`,
				`{$px}profile`.`profile_value` as `value`,
				`{$px}profilefield`.`profilefield_type` as `type`,
				`{$px}profilefield`.`profilefield_title` as `title`,
				`{$px}profilefield`.`profilefield_options` as `options`,
				`{$px}profilefield`.`profilefield_defaultvalue` as `default`,
				`{$px}profilefield`.`profilefield_position` as `position`,
				`{$px}profilesection`.`profilesection_title` as `section`,
				`{$px}profilesection`.`profilesection_id` as `sectionid`
			FROM `{$px}profile`
			LEFT JOIN `{$px}profilefield` ON `{$px}profile`.`profile_key` = `{$px}profilefield`.`profilefield_key`
			LEFT JOIN `{$px}profilesection` ON `{$px}profilefield`.`profilefield_section` = `{$px}profilesection`.`profilesection_objid`
			WHERE `{$px}profile`.`profile_userid` = '$uid'
			ORDER BY position ASC"
		);

		$sections = array('section_default' => [
			'title' => 'Account Settings',
			'id' => 'section_default',
		]);
		$sectionsArray = ['section_default'];
		foreach ($res as $k => $v) {
			if (in_array('section_'.$v['sectionid'],$sectionsArray) === false) {
				$sections['section_'.$v['sectionid']] = [
					'title' => $v['section'],
					'id' => 'section_'.$v['sectionid'],
				];
			}
			$type = ucfirst(strtolower($v['type'])).'FormField';
			$options = array();
			$options['title'] = $v['title'];
			$options['section'] = 'section_'.$v['sectionid'];
			if ($editmode == false) {
				$type = 'LabelFormField';
				switch ($v['type']) {
					case 'imageupload' : {
						$v['value'] = '<img src="'.UPLOADURL.$v['value'].'" width="150">';
					}break;
					case 'text' : {
						$v['value'] = $v['value'];
					}break;
				}
			}
			if (class_exists($type)) {
				$form->add(new $type($v['key'],$v['value'],$options));
			}
		}
		$form->options['sections'] = $sections;
		return $form;
	}

	private function syncProfileFields(){
		global $service;
		$uid = $service->get('User')->getUid();

		$fieldStore = new ProfilefieldStore();
		$fields = $fieldStore->get(new Criteria('profilefield_id',0,'>'));

		$profileStore = new ProfileStore();
		$vars = $profileStore->getByUser($uid);
		foreach ($fields as $kf => $vf) {
			foreach($vars as $kv => $vv) {
				if ($vv->getVar('profile_key') == $vf->getVar('profilefield_key')){
					unset($fields[$kf]);
				}
			}
		}
		if (count ($fields)) {
			foreach ($fields as $k => $v) {
				$obj = $profileStore->create();
				$obj->setVar('profile_key',$v->getVar('profilefield_key'));
				$obj->setVar('profile_value',$v->getVar('profilefield_defaultvalue'));
				$obj->setVar('profile_userid',$uid);
				$profileStore->save($obj);
			}
		}
	}
}
?>
