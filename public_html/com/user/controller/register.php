<?php
/**
 * User account plugin controller
 *
 * Handles login requests
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('obj/controller');
$service->get('Ressource')->get('com/user/form/register');
$service->get('Ressource')->get('com/user/view/register');
$service->get('Ressource')->get('com/user/lang/'.$service->get('Language')->getCode().'/user');

class RegisterController extends Controller{
	public function execute(){
		global $service;

		//1. Get rid of logged in users but not admins
		if ($service->get('User')->isLogged() && $service->get('User')->isAdmin() == false) {
			header('Location: '.URL.$service->get('Language')->getCode().'/user/profile');
			exit;
		}
		//2. Get rid of request if allow user registration is disabled.
		if ($service->get('Setting')->get('allowuserregistration') == false  && $service->get('User')->isAdmin() == false) {
			header('Location: '.URL.$service->get('Language')->getCode().'/user/login');
			exit;
		}

		$view = $this->getView();

		$service->get('Theme')->setTitle(LOGIN_USERACCOUNT_TITLE);
		$errorfound = false;
		$errormessage = '';
		$form = new RegisterForm();
		if ($service->get('Request')->get('op') == 'register') {
			$username = $service->get('Request')->get('name');
			$email = $service->get('Request')->get('email');
			$pass = ($service->get('Request')->get('pass') == $service->get('Request')->get('pass2'))?$service->get('Request')->get('pass'):'';
			if ($service->get('User')->exists($email)) {
				$errorfound = true;
				$errormessage = LOGIN_USERACCOUNT_ERROR_INUSE;
			}
			if ($pass == '') {
				$errorfound = true;
				$errormessage = LOGIN_USERREGISTER_PASSDONOTMATCH;
			}
			if (!$errorfound) {
				$user = new UserData('user');
				$userStore = new UserStore();
				$user->setVar('user_id',0);
				$user->setVar('user_name',$username);
				$user->setVar('user_email',$email);
				if (strlen($pass) >= 8) {
					$pass = $service->get('User')->encryptPassword($pass);
					$user->setVar('user_pass',$pass['password']);
					$user->setVar('user_salt',$pass['salt']);
				}
				$user->setVar('user_encryptionmethod',(defined('USER_CRYPTOALGORITHM'))?USER_CRYPTOALGORITHM:'scrypt');
				$user->setVar('user_groups',array('2','3'));
				$user->setVar('user_lastlogin',0);
				$user->setVar('user_timezone',defined('DEFAULTTIMEZONE')?DEFAULTTIMEZONE:'UTC');
				$user->setVar('user_profileid',0);
				$user->setVar('user_sid','');
				$user->setVar('user_ip',$_SERVER['REMOTE_ADDR']);
				$user->setVar('user_site',$service->get('Site')->getId());
				$user->setVar('user_tenant',0);
				$user->setVar('user_language',$service->get('Language')->getId());
				$save = $userStore->save($user);

				//Fire user register event
				if ($user->getVar('user_id') == 0) {
					$service->get('Event')->trigger('user.register',$user,true);
				}

				//Create users upload folder
				mkdir(UPLOADROOT.'users/'.$service->get('Db')->getLastInsertId(),0777,true);
			}
			if ($errorfound == false) {
				$form->option('intro',REGISTER_SUCCESS);
			}
			else {
				$form->option('intro','<div class="control-group error">'.$errormessage.'</div>');
				$form->setValue('name','');
				$form->setValue('email','');
				$form->setValue('pass',$pass);
				$form->setValue('pass2',$pass);
				$form->buildForm();
			}
		}else{
			$form->option('intro',LOGIN_USERREGISTER_INTRO);
			$form->add(new HiddenFormfield('redirect',(($service->get('Request')->get('redirect') != '')?$service->get('Request')->get('redirect'):'')));
			$form->setValue('name','');
			$form->setValue('email','');
			$form->setValue('pass','');
			$form->setValue('pass2','');
			$form->buildForm();
		}
		$form = $form->render();
		$view->setVar('form',$form);

		//Get other plugins that may want to display things here.
		$addons = $service->get('EventHandler')->trigger('register.onPageload');
		$view->setVar('addons',$addons);
		return $view;
	}

	/**
	 * Returns the corresponding URL in the language ID given
	 * or simply returns the home page url if none found
	 *
	 * @public
	 * @param string $url The url for which we need the translation
	 * @param int $langid The language ID to find the URL for
	 * @return string The translated URL in the language we asked for or false if not found.
	 */
	public function translate($langid, $objid = 0, $url=''){
		global $service;
		return $service->get('Language')->getCodeForId($langid).'/register';
	}
}
?>
