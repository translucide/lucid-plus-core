<?php
/**
 * Collections of tasks to perform on install
 **/
$info = [
    'info' => [
        'title' => 'User profiles plugin',
        'description' => 'Login, register and edit profile functionality',
        'author' => 'Emmanuel Morin',
        'authorurl' => 'https://translucide.ca',
        'company' => 'Translucide',
        'companyurl' => 'https://translucide.ca',
        'url' => '',
        'version' => '0.1'
    ],
    'route' => [
        [
            'url' => '{lang}/user/login',
            'controller' => 'login'
        ],
        [
            'url' => '{lang}/user/logout',
            'controller' => 'login'
        ],
        [
            'url' => '{lang}/user/register',
            'controller' => 'register'
        ],
        [
            'url' => '{lang}/user/resetpassword',
            'controller' => 'recover'
        ],
        [
            'url' => '{lang}/user/profile',
            'controller' => 'profile'
        ]
    ],
    'setting' => [
        [
            'category' => 'user',
            'name' => 'userconfirmemail',
            'value' => '1',
            'type' => 'yesno',
            'options' => '',
            'title' => 'SYSTEM_SETTINGS_USER_REGISTRATIONMETHOD',
            'description' => 'SYSTEM_SETTINGS_USER_REGISTRATIONMETHOD_DESC'
        ],
        [
            'category' => 'cats',
            'name' => 'allowuserregistration',
            'value' => '1',
            'type' => 'yesno',
            'options' => '',
            'title' => 'SYSTEM_SETTINGS_USER_ALLOWREGISTRATION',
            'description' => 'SYSTEM_SETTINGS_USER_ALLOWREGISTRATION_DESC'
        ]
    ],
    'settingcategory' => [
    ]
];
