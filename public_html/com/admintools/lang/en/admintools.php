<?php
define('ADMINTOOLS_TITLE','Admin Tools');
define('ADMINTOOLS_DBTOOLS','Database tools');
define('ADMINTOOLS_DBTOOLS_SEARCHREPLACE','Search and replace');
define('ADMINTOOLS_DBTOOLS_SEARCHREPLACE_DSC','Performs a search and replacce operation on selected database tables');
define('ADMINTOOLS_DBTOOLS_STARTWITH','Replace if value starts with search term?');

define('ADMINTOOLS_TARGET','Target DB tables');

define('ADMINTOOLS_DBTABLES_WIDGETS','Widgets (widget)');
define('ADMINTOOLS_DBTABLES_MENU','Menus (menu)');
define('ADMINTOOLS_DBTABLES_DATA','Public data used mostly by editor plugins (data)');
define('ADMINTOOLS_DBTABLES_CONTENT','Sections & content (content)');
define('ADMINTOOLS_DBTABLES_SETTINGS','Settings & setting categories (setting, settingcategory)');
define('ADMINTOOLS_DBTABLES_USERS','Users, groups and permissions (user, group, permission)');
define('ADMINTOOLS_DBTABLES_SITES','Sites (site)');

?>