<?php
define('ADMINTOOLS_TITLE','Outils  pour webmestres');
define('ADMINTOOLS_DBTOOLS','Base de données');
define('ADMINTOOLS_DBTOOLS_SEARCHREPLACE','Chercher et rmeplacer');
define('ADMINTOOLS_DBTOOLS_SEARCHREPLACE_DSC','Effectue une recherche parmis les tableaux sélectionnés');
define('ADMINTOOLS_DBTOOLS_STARTWITH','Remplacer si les valeurs débutent par le terme recherché?');

define('ADMINTOOLS_TARGET','Tableaux ciblés');

define('ADMINTOOLS_DBTABLES_WIDGETS','Widgets (widget)');
define('ADMINTOOLS_DBTABLES_MENU','Menus (menu)');
define('ADMINTOOLS_DBTABLES_DATA','Données publiques (data)');
define('ADMINTOOLS_DBTABLES_CONTENT','Sections & contenu (content)');
define('ADMINTOOLS_DBTABLES_SETTINGS','Préférences et catégories de préférences (setting, settingcategory)');
define('ADMINTOOLS_DBTABLES_USERS','Utilisateurs, groupes et permissions (user, group, permission)');
define('ADMINTOOLS_DBTABLES_SITES','Sites (site)');

?>