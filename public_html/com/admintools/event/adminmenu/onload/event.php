<?php
global $service;
$service->get('EventHandler')->on('adminmenu.onLoad',
	function($e,$p){
		global $service;
		$langcode = $service->get('Language')->getCode();
		$service->get('Ressource')->get('com/admintools/lang/'.$langcode.'/admintools');
		$p['system']['items'][] = array(
			'type'=>'item',
			'component'=>'admintools',
			'icon'=>'databasetools',
			'glyphicon' => 'wrench',
			'title'=>ADMINTOOLS_TITLE,
			'link' => $langcode.'/admin/admintools'
		);
		return $p;
	}
);
?>