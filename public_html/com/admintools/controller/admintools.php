<?php
/**
 * Admin Tools
 *
 * Dec 29, 2014
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('com/admintools/lang/'.$service->get('Language')->getCode().'/admintools');
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('core/display/form/field');
set_time_limit(0);

class AdmintoolsController extends Controller {
	function init(){
		$this->component = 'admintools';
	}
		
	public function execute(){
		global $service;
		$view = $this->getView();
		$url = $service->get('Url')->get();
		$request = $service->get('Request')->get();
		$breadcrumbs = array(
			array('link' => URL.$service->get('Language')->getCode().'/admin', 'title' => _LOCATION_ADMINISTRATION),
			array('link' => URL.$service->get('Language')->getCode().'/admin/admintools', 'title' => ADMINTOOLS_TITLE),
		);
		$form = new Form('', 'admintoolsform', SYSTEM_WIDGETS_MANAGE, $method='POST');
		$form->setVar('intro',_CLICKTOEDIT);
		$view->setVar('form',$form);
		$view->setVar('breadcrumbs',$breadcrumbs);
		return $view;		
	}

	public function apiCall($op){
		global $service;
		$request = $service->get('Request')->get();
		$service->get('Ressource')->get('core/display/tree');
		$ret = array();
		switch($op) {
			case 'dbsearchandreplace' : {
				$form = new Form('', 'dbsearandreplaceform', ADMINTOOLS_DBTOOLS_SEARCHREPLACE, $method='POST');
				$form->setTitle(ADMINTOOLS_DBTOOLS_SEARCHREPLACE);
				$form->setVar('icon',URL.'com/admintools/asset/icon/32x32/databasetools.png');
				$form->setVar('layout','vertical');
				$form->add(new TextareaFormField('search','',array(
					'title' => _SEARCH,
					'lang'=>$defaultlang['code'],
					'width' => 4
				)));
				$form->add(new TextFormField('replace','',array(
					'title' => _REPLACE,
					'lang'=>$defaultlang['code'],
					'width' => 4
				)));
				$form->add(new YesnoFormField('startswith','',array(
					'title' => ADMINTOOLS_DBTOOLS_STARTWITH,
					'lang'=>$defaultlang['code'],
					'width' => 4
				)));
				$form->add(new CheckboxFormField('target',URL,array(
					'title' => ADMINTOOLS_TARGET,
					'options' => array(
						array('title' => _ALL, 'value' => 'all'),
						array('title' => ADMINTOOLS_DBTABLES_WIDGETS, 'value' => 'widgets'),
						array('title' => ADMINTOOLS_DBTABLES_MENU, 'value' => 'menus'),
						array('title' => ADMINTOOLS_DBTABLES_DATA, 'value' => 'data'),
						array('title' => ADMINTOOLS_DBTABLES_CONTENT, 'value' => 'content'),
						array('title' => ADMINTOOLS_DBTABLES_SETTINGS, 'value' => 'settings'),
						array('title' => ADMINTOOLS_DBTABLES_USERS, 'value' => 'users'),
						array('title' => ADMINTOOLS_DBTABLES_SITES, 'value' => 'sites'),
					)
				)));				
				$form->addSaveButton('dbsearchandreplace_process',_SAVE);
				$ret = array('success'=>1,'form'=>$form->renderArray());
				
			}break;
			case 'dbsearchandreplace_process' :{
				global $service;
				$service->get('Ressource')->get('core/widget');
				$service->get('Ressource')->get('core/display/menu');
				$service->get('Ressource')->get('core/data');
				$service->get('Ressource')->get('core/content');
				$service->get('Ressource')->get('core/setting');
				$service->get('Ressource')->get('core/user');
				$service->get('Ressource')->get('core/user/group');
				$service->get('Ressource')->get('core/user/permission');
				$service->get('Ressource')->get('core/site');
				$exporttables = array(
					'widgets' => array('widget'),
					'menus' => array('menu'),
					'data' => array('data'),
					'content' => array('content'),
					'settings' => array('setting','settingcategory'),
					'users' => array('user','group','permission'),
					'sites' => array('site')
				);
				$request['search'] = str_replace("\r\n", "\n", $request['search']); // windows -> unix
				$request['search'] = str_replace("\r", "\n", $request['search']);   // remaining -> unix
				$search = explode("\n",$request['search']);
				foreach($search as $k => $v){
					$v = trim($v);
				}
				$replace = trim($request['replace']," \t\n\r\0\x0B");
				foreach($exporttables as $kt => $vt) {
					if (in_array($kt,$request['target']) || in_array('all',$request['target']) !== false) {
						foreach($vt as $ktable => $table) {
							$cls = ucfirst($table).'Store';
							$store = new $cls();
							$store->setOption('ignorelangs',true);
							$objects = $store->get(new Criteria($table.'_id',0,'>'));
							foreach ($objects as $k => $v) {
								$data = $v->get(true);
								if ($request['startswith']) {
									array_walk_recursive($data, function(&$item,&$key,$p) {
										foreach ($p[1] as $prefix){
											if (substr($item, 0, strlen($prefix)) == $prefix) {
												$item = $p[0].substr($item, strlen($prefix));
											}
										}
									},array($replace,$search));									
								}else {
									array_walk_recursive($data, function(&$item,&$key,$p) {
										$item = str_replace($p[1],$p[0],$item);
									},array($replace,$search));									
								}
								$objects[$k]->set($data);
							}
							$store->save($objects);
						}
					}
				}
				$ret = array('success'=>1, 'message' => _SUCCESS);
			}break;
		}
		return $ret;
	}
	
	public function hasAccess($op){
		global $service;
		if ($service->get('User')->isAdmin()) return true;
		return true;
	}	
}
?>