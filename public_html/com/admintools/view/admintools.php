<?php
/**
 * Backup manager view
 *
 * Default view loaded on backup management page request
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('com/view/default');

class AdmintoolsView extends DefaultView{
	public function init(){
	}
	
	public function render(){
		global $service;
		$service->get('Ressource')->get('core/display/interface');
		$url = $service->get('Url')->get();
		
		$list = array();
        $list[] = new LucidLink([
            'icon' => URL.$service->get('Ressource')->getIcon(['component'=>'admintools','icon'=>'databasetools']),
            'title' => ADMINTOOLS_DBTOOLS_SEARCHREPLACE,
            'events' => [
                'onclick' => ''
            ]
        ]);
		
		$addcontentmenu[] = new LucidActionPanel([
			'title' => '<img src="'.URL.$service->get('Ressource')->getIcon(['component'=>'admintools','icon'=>'databasetools']).'"> '.ADMINTOOLS_DBTOOLS_SEARCHREPLACE,
			'description' => ADMINTOOLS_DBTOOLS_SEARCHREPLACE_DSC,
			'childs' => [
				new LucidPrimaryButton([
					'title' => _OPEN,
					'icon' => URL.$service->get('Ressource')->getIcon(['component'=>'admintools','icon'=>'databasetools'],16),
					'events' => [
						'onclick' => 'loadmodule(\'dbsearchandreplace\');'
					],
				])
			]
		]);
		
		$addcontentmenucode = '<form><fieldset><legend>'.ADMINTOOLS_TITLE.'</legend>';
		foreach($addcontentmenu as $v){
			$addcontentmenucode .= $v->render();
		}
		$addcontentmenucode .= '</fieldset></form>';
		
        $cols[] = new LucidLeftVBox([
            'id' => 'leftvbox',
            'childs' => [
				new LucidAccordion([
					'id' => 'toolslist',
					'title' => ADMINTOOLS_TITLE,
					'childs' => [
						new LucidAccordionSection([
							'title' => 'Database tools',
							'id' => 'databasetools',
							'childs' =>  $list
						])
					]
				])
            ]
        ]);
		$cols[] = new LucidMainVBox([
			'id' => 'mainvbox',
			'childs' => [
				new LucidHBox([
					'childs' => [
						new LucidPanel([
							'id' => 'formpanel',
							'html' =>  $addcontentmenucode
						])
					]
				])
			]
		]);
		$interface = new LucidInterface([
			'id' => 'admintools',
			'name' => 'admintools',
			'childs' => [
				new LucidViewport([
					'childs' => [
						new LucidLayout([
							'childs' => [
								new LucidHBox([
									'childs' => [										
										new LucidBreadCrumbs([
											'childs' => $this->data['breadcrumbs']
										])
									]
								]),
								new LucidHBox([
									'childs' => $cols
								])					
							]
						])
					]
				])
			]
		]);
		$code = $interface->render();
		$service->get('Ressource')->addScript('var CKEDITOR_BASEPATH = \''.URL.'lib/ckeditor/\';CKFINDER_BASEPATH = \''.URL.'lib/ckfinder/\';',true);
		$service->get('Ressource')->get('lib/ckfinder/ckfinder');
		$service->get('Ressource')->get('lib/ckeditor/ckeditor');
		$service->get('Ressource')->get('lib/packages/form/form');
		$script = $interface->renderJS().'
		function loadmodule(name) {
			$.ajax({
				async : false,
				type: "POST",
				url: "'.URL.$service->get('Language')->getCode().'/api/admintools/controller/admintools/"+name,
				success : function (r) {
					if(r.success) {
						$("#formpanel").lucidForm(r.form);
						$("#formpanel form").ajaxForm({
							target: "#responsepanel",
							url : "'.URL.$service->get('Language')->getCode().'/api/admintools/controller/admintools/"+name+"_process",
							beforeSubmit : function(data, jqForm, options){
								try{
								for (instance in CKEDITOR.instances) {
									$("#"+instance).val(CKEDITOR.instances[instance].getData());
									for(i=0;i<data.length;i++) if(data[i].name == instance) data[i].value = CKEDITOR.instances[instance].getData();
								}
								}catch(e){}
								return true;
							},
							success: function(responseText, statusText, xhr, $form){
								$("html, body").animate({ scrollTop: 0 }, "fast");
								if (responseText.success) {
									$("#formpanel").fadeOut();
									$("#responsepanel").html("<div class=\\"success\\">"+responseText.message+"</div>").fadeIn();
								}else {
									$("#responsepanel").html("<div class=\\"warning\\">"+responseText.message+"</div>").fadeIn();	
								}
								
							}
						});							
						$("#responsepanel").hide();
						$("#formpanel").show();										
					}
				}
			});								
		}';
		$service->get('Ressource')->addScript($script);
		return $code;
	}
}
?>