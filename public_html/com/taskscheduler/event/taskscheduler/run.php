<?php
//Add a cron job like this to your CRON tab file:
//php <pathToLucid+>/index.php --site=<yoursite> --triggerevent=taskscheduler.
global $service;
$service->get('EventHandler')->on('taskscheduler.run',
	function($event,$params){
		global $service;

		return $params;
	}
);
?>