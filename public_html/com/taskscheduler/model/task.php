<?php
/**
 * Scheduled Task classes
 *
 * Defines scheduled tasks classes (TaskData, TaskModel, TaskStore)
 * 
 * August 4, 2016
 *
 * @author 		Translucide
 * @copyright 	copyright (c) 2015 Translucide
 * @license		See docs/license.txt
 */
class TaskModel extends Model {
	
	/**
	 * PHP 5 Constructor
	 * @return void
	 */
	public function __construct() {
		$this->TaskModel();
	}
	
	public function TaskModel(){
		global $service;
		$this->type = 'task';        
		$this->initVar('task_id',DATATYPE_INT);
		$this->initVar('task_uuid',DATATYPE_UUID);
		$this->initVar('task_tenant',DATATYPE_INT);
		$this->initVar('task_type',DATATYPE_STRING,array('length'=> 45)); //Task type: content,widget,...
		$this->initVar('task_name',DATATYPE_STRING,array('length'=> 45)); //Task name: Name of content object
		$this->initVar('task_component',DATATYPE_STRING,array('length'=> 60));
		$this->initVar('task_action',DATATYPE_STRING,array('length'=> 60));
		$this->initVar('task_status',DATATYPE_INT); //Last error code
		$this->initVar('task_user',DATATYPE_INT); //User associated with this task
		$this->initVar('task_maxruns',DATATYPE_INT); //Maximum number of times the task should run
		$this->initVar('task_begin',DATATYPE_TIMESTAMP); //Begin running from timestamp
		$this->initVar('task_end',DATATYPE_TIMESTAMP); //End running at timestamp
		$this->initVar('task_interval',DATATYPE_INT); //Minimum time between each run
		$this->initVar('task_priority',DATATYPE_INT); //Task priority
		$this->initVar('task_active',DATATYPE_INT); //Active: the task is currently running ?
		$this->initVar('task_lastduration',DATATYPE_INT); //Last timestamp when task ran.
		$this->initVar('task_lastrun',DATATYPE_INT); //Last timestamp when task ran.
		$this->initVar('task_data',DATATYPE_ARRAY); //Additional data to pass to task handler
		$this->initVar('task_runcount',DATATYPE_INT); //Number of runs since it was created (or reset manually)
		$this->initVar('task_errorcount',DATATYPE_INT); //Number of errors generated
		$this->initVar('task_logenable',DATATYPE_INT); //Enable logging of this task for monitoring purpose
		$this->initVar('task_site',DATATYPE_INT);
		$this->initVar('task_deleted',DATATYPE_INT);
		$this->initVar('task_deletedby',DATATYPE_INT);
		$this->initVar('task_created',DATATYPE_INT);
		$this->initVar('task_createdby',DATATYPE_INT);
		$this->initVar('task_modified',DATATYPE_INT);
		$this->initVar('task_modifiedby',DATATYPE_INT);
	}
}

class TaskData extends Data{
	function __construct(){
        parent::__construct('task');         
	}
}

class TaskStore extends Store{
	public function __construct() {
		parent::__construct('task');
	}
    
    /**
     * Returns next task to run
     *
     * @access public
     * @param int start
     * @return array $task
     */
    public function next($start=0){
        $crit = new CriteriaCompo();
        
        //Prioritize tasks that never ran first.
        $crit1 = new CriteriaCompo();
        $crit1->add(new Criteria('task_lastrun',0),'OR');
        $crit1->add(new Criteria('task_lastrun',time()-1,'<'),'OR');
        $crit->add($crit1);
        
        //Then select tasks that should have already begin
        $crit2 = new CriteriaCompo();
        $crit2->add(new Criteria('task_begin',time(),'<'),'OR');
        $crit2->add(new Criteria('task_begin',0),'OR');
        $crit->add($crit2);

        //Then remove tasks that should have already ended
        $crit3 = new CriteriaCompo();
        $crit3->add(new Criteria('task_end',time(),'>'),'OR');
        $crit3->add(new Criteria('task_end',0),'OR');
        $crit->add($crit3);
        
        //Then remove tasks that have run enough times
        $crit4 = new CriteriaCompo();
        $crit4->add(new Criteria('task_maxruns',0),'OR');
        $crit4->add(new Criteria('task_maxruns','`task_runcount`','>'),'OR');
        $crit->add($crit4);
        
        //Get only tasks that are not running...
        $crit->add(new Criteria('task_active',0));
        
        //Get only tasks scheduled for running
        $crit->add(new Criteria('`task_maxruns`+`task_interval`',time(),'>'));

        //Then remove tasks that have run enough times
        $crit5 = new CriteriaCompo();
        $crit5->add(new Criteria('`task_runcount`','`task_errorcount`*2','>'),'OR');
        $crit5->add(new Criteria('`task_runcount`',4,'<'),'OR');
        $crit->add($crit5);
        $crit->setOrder('task_priority','DESC');
        $crit->setOrder('task_lastrun','ASC');
        $crit->setLimit($start,1);
        return $this->get($crit);
    }
    
    /**
     * Mark tasks as running
     *
     * @access public
     * @return bool $success
     */
    public function begin($task){
        if (!isset($task[0]) && !is_object($task)) {
            $task = $this->toObjects(array($task));
        }
        if (!is_array($task)) {
            $task = array($task);
        }
        foreach($task as $k => $v) {
            $task[$k]->setVar('task_previousrun',$v->getVar('task_lastrun'));
            $task[$k]->setVar('task_lastrun', time());
            $task[$k]->setVar('task_runcount', $v->getVar('task_runcount')+1);
            $task[$k]->setVar('task_active', 1);
        }
        return $this->save($task);
    }    
    
    /**
     * Mark tasks as finished
     *
     * @access public
     * @return bool $success
     */
    public function end($task){
        if (!isset($task[0]) && !is_object($task)) {
            $task = $this->toObjects(array($task));
        }
        if (!is_array($task)) {
            $task = array($task);
        }
        foreach($task as $k => $v) {
            $task[$k]->setVar('task_lastduration',time()-$v->getVar('task_previousrun'));
            $task[$k]->setVar('task_active', 0);
        }
        return $this->save($task);
    }    
    
    /**
     * Resets active flag when a task is stuck on active for more than twice the interval set for task.
     *
     * @access public
     * @return bool $success
     */
    public function revive() {
        $crit = new CriteriaCompo();
        $crit->add(new Criteria('task_active',1));        
        $crit->add(new Criteria('NOW()','`task_lastrun`+`task_interval`*2','>'));        
        return $this->update([
            'task_active' => 0,
        ],$crit);
    }
}
?>