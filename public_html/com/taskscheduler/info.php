<?php
/**
 * Collections of tasks to perform on install
 **/
$info = [
    'info' => [
        'title' => 'Task Scheduler',
        'description' => 'Near realtime task scheduler. Note: must be run from its own CRON job.',
        'author' => 'Emmanuel Morin',
        'authorurl' => 'https://translucide.ca',
        'company' => 'Translucide',
        'companyurl' => 'https://translucide.ca',
        'url' => '',
        'version' => '0.1'
    ],
    'route' => [
        [
            'url' => '{lang}/taskscheduler/{op}',
            'controller' => 'cats'
        ]        
    ],
    'setting' => [
        [
            'category' => 'taskscheduler',
            'name' => 'taskscheduler',
            'value' => '',
            'type' => 'text',
            'options' => '',
            'title' => 'CATS_SETTING_RESTRICTEDSTATUS'
        ],
        [
            'category' => 'cats',
            'name' => 'restrictedcategory',
            'value' => '',
            'type' => 'text',
            'options' => '',
            'title' => 'CATS_SETTING_RESTRICTEDCATEGORY'
        ],
        [
            'category' => 'cats',
            'name' => 'portalurl',
            'value' => '',
            'type' => 'text',
            'options' => '',
            'title' => 'CATS_WIDGET_PORTALURL'
        ],
        [
            'category' => 'cats',
            'name' => 'portalid',
            'value' => '',
            'type' => 'text',
            'options' => '',
            'title' => 'CATS_WIDGET_PORTALID'
        ],
        [
            'category' => 'cats',
            'name' => 'apikey',
            'value' => '',
            'type' => 'text',
            'options' => '',
            'title' => 'CATS_WIDGET_APIKEY'
        ]
    ],
    'settingcategory' => [
        [
            'name' => 'cats',
            'title' => 'CATS_SETTINGS',
        ]
    ]

];