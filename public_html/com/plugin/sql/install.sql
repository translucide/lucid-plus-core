DROP TABLE IF EXISTS `{DB_PREFIX}_plugin`;
CREATE TABLE `{DB_PREFIX}_plugin` (
    `plugin_pkid` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `plugin_id` BINARY(22) NOT NULL DEFAULT '',
    `plugin_component` VARCHAR(100) NOT NULL DEFAULT '',
    `plugin_title` VARCHAR(100) NOT NULL DEFAULT '',
    `plugin_description` VARCHAR(100) NOT NULL DEFAULT '',
    `plugin_version` VARCHAR(25) NOT NULL DEFAULT '',
    `plugin_url` VARCHAR(255) NOT NULL DEFAULT '',
    `plugin_author` VARCHAR(50) NOT NULL DEFAULT '',
    `plugin_authorurl` VARCHAR(255) NOT NULL DEFAULT '',
    `plugin_company` VARCHAR(50) NOT NULL DEFAULT '',
    `plugin_companyurl` VARCHAR(255) NOT NULL DEFAULT '',
    `plugin_status` INT(10) NOT NULL,
    `plugin_options` TEXT,
    `plugin_tenant` TEXT,
    `plugin_site` BINARY(22) NOT NULL DEFAULT '0',
    `plugin_deleted` INT(10) UNSIGNED DEFAULT '0',
    `plugin_deletedby` BINARY(22) NOT NULL DEFAULT '0',
    `plugin_created` INT(10) UNSIGNED DEFAULT '0',
    `plugin_createdby` BINARY(22) NOT NULL DEFAULT '0',
    `plugin_modified` INT(10) UNSIGNED DEFAULT '0',
    `plugin_modifiedby` BINARY(22) NOT NULL DEFAULT '0',
    PRIMARY KEY (`plugin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `{DB_PREFIX}_route`
    (`route_component`, `route_url`, `route_controller`, `route_language`, `route_site`, `route_created`, `route_modified`, `route_modifiedby`)
    VALUES ('plugin', '{lang}/admin/plugins', 'plugin', '0', '0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0');

ALTER TABLE `{DB_PREFIX}_settingcategory`
ADD COLUMN `settingcategory_title` VARCHAR(50) NOT NULL AFTER `settingcategory_name`;
