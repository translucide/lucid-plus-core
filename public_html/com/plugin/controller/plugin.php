<?php
/**
 * Plugin controller
 *
 * Handles plugins operations
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('controller');
$service->get('Ressource')->get('core/model/list');
$service->get('Ressource')->get('core/data/collection');
$service->get('Ressource')->get('com/plugin/model/plugin');
$service->get('Ressource')->get('com/plugin/locale/'.$service->get('Language')->getCode());

class PluginController extends Controller {
	function init(){
		$this->component = 'plugin';
	}

    public function execute(){
        global $service;
		$breadcrumbs = array(
			array('link' => URL.$service->get('Language')->getCode().'/admin', 'title' => _LOCATION_ADMINISTRATION),
			array('link' => URL.$service->get('Language')->getCode().'/admin/plugins', 'title' => PLUGIN_PLUGINS)
		);
        $view = $this->getView();
        $view->setVar('breadcrumbs',$breadcrumbs);
        return $view;
    }

    /**
     * Executes an API call. Called on every AJAX request.
     *
     * @param string $op Current api call name
     * @access public
     **/
	public function apiCall($op){
		global $service;
		$ret = array();
		$isAdmin = $service->get('User')->isAdmin();
        $ls = new ListStore();
        $request = $service->get('Request')->get();
        $store = new PluginStore();
        $crit = $store->initCrit();

        switch($op) {
            /**
             * Perform plugin installation and output results
             **/
            case "install": {
                if ($request['id']) {
                    $crit->add('plugin_id',$request['id']);
                    $crit->add('plugin_status',0);
                    $obj = $store->get($crit,['singlerow' => true]);
                    $success = true;
                    $message = '';
                    if (is_object($obj)) {
                        $info = [];
                        include(ROOT."com/".$obj->getVar('plugin_component').'/info.php');

                        //1. Insert plugin routes
                        $res = $this->addRoutes($obj->getVar('plugin_component'),$info['route']);
                        if ($res == false) {
                            $success = false;
                            $message .= "<p>Creating Routes : ".$service->get('Db')->getError().'</p>';
                        }

                        //2. Insert plugin setting categories
                        $res = $this->addSettingCategories($obj->getVar('plugin_component'),$info['settingcategory']);
                        if ($res == false) {
                            $success = false;
                            $message .= "<p>Creating Setting Categories : ".$service->get('Db')->getError().'</p>';
                        }

                        //3. Insert plugin settings
                        $res = $this->addSettings($obj->getVar('plugin_component'),$info['setting']);
                        if ($res == false) {
                            $success = false;
                            $message .= "<p>Creating Settings : ".$service->get('Db')->getError().'</p>';
                        }

                        //4. Mark plugin as installed.
                        $obj->setVar('plugin_status',1);
                        $res = $store->save($obj);
                        if ($res == false) {
                            $success = false;
                            $message .= "<p>".$service->get('Db')->getError().'</p>';
                        }
                    }else {
                        $success = false;
                    }
                    $ret = array(
                        'success' => $success,
                        'message' => ($success)?PLUGIN_INSTALLRESULT_MESSAGE_SUCCESS:PLUGIN_INSTALLRESULT_MESSAGE_ERROR.$message
                    );
                }
                else {
                    $ret = array(
                        'success' => false,
                        'message' => "<p>No plugin selected for installation. Please specify a plugin to install.</p>"
                    );
                }
				$service->get('Ressource')->clearCaches();
            }break;

            /**
             * Perform plugin installation and output results
             **/
            case "remove": {
                $crit->add('plugin_id',$request['id']);
                $obj = $store->get($crit,['singlerow' => true]);
                $success = true;
                if (is_object($obj)) {
                    $info = [];
                    include(ROOT."com/".$obj->getVar('plugin_component').'/info.php');

                    //1. Insert plugin routes
                    $res = $this->removeRoutes($obj->getVar('plugin_component'),$info['route']);
                    if ($res == false) {
                        $success = false;
                        $message .= "<p>Removing Routes : ".$service->get('Db')->getError().'</p>';
                    }

                    //2. Insert plugin settings
                    $res = $this->removeSettings($obj->getVar('plugin_component'),$info['setting']);
                    if ($res == false) {
                        $success = false;
                        $message .= "<p>Removing Settings : ".$service->get('Db')->getError().'</p>';
                    }

                    //3. Insert plugin setting categories
                    $res = $this->removeSettingCategories($obj->getVar('plugin_component'),$info['settingcategory']);
                    if ($res == false) {
                        $success = false;
                        $message .= "<p>Removing Setting Categories : ".$service->get('Db')->getError().'</p>';
                    }

                    //4. Mark plugin as installed.
                    $obj->setVar('plugin_status',0);
                    $res = $store->save($obj);
                    if ($res == false) {
                        $success = false;
                        $message .= "<p>".$service->get('Db')->getError().'</p>';
                    }
                }else {
                    $success = false;
                    $message .= "<p>Plugin not found !</p>";
                }
				$service->get('Ressource')->clearCaches();
                $ret = array(
                    'success' => $success,
                    'message' => ($success)?PLUGIN_REMOVERESULT_MESSAGE_SUCCESS:PLUGIN_REMOVERESULT_MESSAGE_ERROR.$message
                );
            }break;

            /**
            * Load all plugins data for plugins_list screen
            *
            * @param string REQUEST/op op must be loadplugins
            * @return json Plugins table data
            */
            case 'loadplugins' : {
                $this->updatePluginsList();
                $crit->add(new Criteria('plugin_id','0','!='));
                $dir = (strtoupper($request['order'][0]['dir']) == 'ASC')?'ASC':'DESC';
                switch($request['order'][0]['column']) {
                    case 0:	$crit->setSort('plugin_title',$dir); break;
                    case 1:	$crit->setSort('plugin_description',$dir); break;
                }
                $crit->setStart($request['start']);
                $count = $store->count($crit);
                if ($request['search']['value'] != '') {
                    $s = $request['search']['value'];
                    $searchcrit = new CriteriaCompo();
                    $searchcrit->add(new Criteria('plugin_title','%'.$s.'%','LIKE'),'OR');
                    $searchcrit->add(new Criteria('plugin_description','%'.$s.'%','LIKE'),'OR');
                    $crit->add($searchcrit);
                }
                $countfiltered = $store->count($crit);
                $crit->setLimit($request['length']);
                $data = $store->get($crit);
                $colsdata = array();
                foreach($data as $k => $v) {
                    $colsdata[] = array(
                        'id' => $v->getVar('plugin_id'),
                        'title' => '<label>Name:</label><strong>'.$v->getVar('plugin_title').'</strong>',
                        'description' => '<label>Description:</label>'.$v->getVar('plugin_description'),
                        'version' => '<label>Version:</label>'.$v->getVar('plugin_version'),
                        'actions' =>
                            (($v->getVar('plugin_status') > 0)?'':'<a class="btn btn-success btn-xs" onclick="installPlugin('.$v->getVar('plugin_id').');"><span class="glyphicon glyphicon-plus-sign"></span> '.PLUGIN_TABLECOL_ACTIONS_INSTALL.'</a> ').
                            (($v->getVar('plugin_status')  < 1)?'':'<a class="btn btn-default btn-xs btn-danger" onclick="removePlugin('.$v->getVar('plugin_id').');"><span class="glyphicon glyphicon-trash"></span> '.PLUGIN_TABLECOL_ACTIONS_REMOVE.'</a>')
                    );
                }
                $ret = array(
                    'draw' => $request['draw'],
                    'recordsTotal' => $count,
                    'recordsFiltered' => $countfiltered,
                    'data' => $colsdata
                );
            }break;

            /**
             * Loads a template into a screen or a dialog.
             */
            case 'loaddialog':
            case 'loadscreen' : {
                $service->get('Ressource')->get('core/display/template');
                $tpl = new Template('com/plugin/templates/'.$request['screen'].'.tpl');
                $id = $request['id'];
                if (file_exists(ROOT.'/com/plugin/screen/'.$request['screen'].'.php')) {
                    include ROOT.'/com/plugin/screen/'.$request['screen'].'.php';
                }
                $tpl->assign('appurl',URL.$service->get('Language')->getCode().'/plugin/app');
                $tpl->assign('url',URL);
                $tpl->assign('lang',$service->get('Language')->getCode());
                $tpl->assign('screen',$request['screen']);
                $tpl->assign('isdialog',($op=='loaddialog')?1:0);
                $tpl->assign('isadmin',$service->get('User')->isAdmin());
                $tpl->assign('request',$request);
                echo $tpl->render();
                exit;
            }break;
        }
		return $ret;
	}

    /**
     * Checks whether current user has access to operation $op
     *
     * @access public
     * @param string $op
     * @return bool $hasaccess
     */
	public function hasAccess($op) {
		global $service;
		$request = $service->get('Request')->get();
		$isAdmin = $service->get('User')->isAdmin();
		if ($isAdmin) return true; // Admin has all rights.
		return false;
	}

    /**
     * Updates plugin table, add new plugins
     *
     * @access public
     **/
    public function updatePluginsList(){
        global $service;
        $store = new PluginStore();
        $crit = $store->initCrit();
        $crit->add('plugin_id','0','!=');
	    $plugins = $store->get($crit);

        //Get local components, and update the registered ones
        $components = $service->get('Ressource')->listComponents();
        foreach ($plugins as $k => $v) {
            foreach($components as $kc => $vc) {
                if ($v->getVar('plugin_component') == $vc) {
                    $info = [];
                    include(ROOT."com/".$vc."/info.php");
                    $plugins[$k]->setVar('plugin_component',$vc);
                    $plugins[$k]->setVar('plugin_title',$info['info']['title']);
                    $plugins[$k]->setVar('plugin_description',$info['info']['description']);
                    $plugins[$k]->setVar('plugin_version',$info['info']['version']);
                    $plugins[$k]->setVar('plugin_url',$info['info']['url']);
                    $plugins[$k]->setVar('plugin_author',$info['info']['author']);
                    $plugins[$k]->setVar('plugin_authorurl',$info['info']['authorurl']);
                    $plugins[$k]->setVar('plugin_company',$info['info']['company']);
                    $plugins[$k]->setVar('plugin_companyurl',$info['info']['companyurl']);
                    unset($components[$kc]);
                }
            }
        }
        $store->save($plugins);

        //Register all new components
        $objs = array();
        foreach($components as $k => $v) {
            $info = [];
            include(ROOT."com/".$v."/info.php");
            $obj = $store->getNew();
            $obj->setVar('plugin_component',$v);
            $obj->setVar('plugin_status',0); //Uninstalled
            $obj->setVar('plugin_title',$info['info']['title']);
            $obj->setVar('plugin_description',$info['info']['description']);
            $obj->setVar('plugin_version',$info['info']['version']);
            $obj->setVar('plugin_url',$info['info']['url']);
            $obj->setVar('plugin_author',$info['info']['author']);
            $obj->setVar('plugin_authorurl',$info['info']['authorurl']);
            $obj->setVar('plugin_company',$info['info']['company']);
            $obj->setVar('plugin_companyurl',$info['info']['companyurl']);
            $objs[] = $obj;
        }
        $store->save($objs);
    }

    /**
     * Adds routes to DB
     *
     * @access public
     * @param string $component
     * @param array $routes
     * @return bool $success
     **/
    public function addRoutes($com,$routes) {
        global $service;
        $routeStore = new RouteStore();
        $routeObjs = array();
        foreach ($routes as $r) {
            $obj = $routeStore->getNew();
            $obj->setVar('route_component',$com);
            $obj->setVar('route_url',$r['url']);
            $obj->setVar('route_controller',$r['controller']);
            $obj->setVar('route_language',(isset($r['language']))?$r['language']:0);
            $routeObjs[] = $obj;
        }
        return $routeStore->save($routeObjs);
    }

    /**
     * Removes a previously added route
     *
     * @access public
     * @param string $component
     * @param array $route
     * @return bool $success
     **/
    public function removeRoutes($com,$routes) {
        global $service;
        $routeStore = new RouteStore();
        $crit = new CriteriaCompo();
        $crit->add(new Criteria('route_component',$com),'AND');
        foreach($routes as $r ) {
            $rc = new CriteriaCompo();
            $rc->add(new Criteria('route_url',$r['url']));
            $rc->add(new Criteria('route_controller',$r['controller']));
            $crit->add($rc,'OR');

        }
        $toRemove = $routeStore->get($crit);
        if (count($toRemove)) return $routeStore->delete($toRemove);
        else return true;
    }

    /**
     * Adds a setting category
     *
     * @access public
     * @param string $component
     * @param array $settingcategories
     * @return bool $success
     **/
    public function addSettingCategories($com,$settingcategories) {
        global $service;
        $settingcategories = $this->prefixNames($com,$settingcategories);
        $store = new SettingCategoryStore();
        $next = $store->getNext('settingcategory_objid');
        $settingCats = array();
        foreach ($settingcategories as $s) {
            $obj = $store->getNew();
            $obj->setVar('settingcategory_objid',$next);
            $obj->setVar('settingcategory_name',$s['name']);
            $obj->setVar('settingcategory_title',$s['title']);
            $obj->setVar('settingcategory_language',(isset($r['language']))?$r['language']:0);
            $settingCats[] = $obj;
            $next++;
        }
        return $store->save($settingCats);
    }

    /**
     * Remove setting category if no more setting found in this category
     *
     * @access public
     * @param string $com
     * @param array $sc
     * @return bool $success
     **/
    public function removeSettingCategories($com,$sc) {
        global $service;
		$col = new Collection();
        $sc = $this->prefixNames($com,$sc);
        //Get category obj ids
        $store = new SettingCategoryStore();
        $crit = new Criteria("settingcategory_name",$store->inClause($col->set($sc)->col("name")->get()),'IN');
        $objs = $store->get($crit);
        if (count($objs) > 0) {
            //Find which cats are still in use in settings table
            $settingStore = new SettingStore();
            $settings = $settingStore->get(new Criteria('setting_cid',$store->inClause($col->set($objs)->col('settingcategory_objid')->get()),'IN'));
            $keep = array_unique($col->set($settings)->col('setting_cid')->get());

            //Delete only categories that are empty.
            if (count($keep) > 0) {
                $del = $col->set($objs)->filter('settingcategory_objid',$keep)->col("settingcategory_objid")->get();
                $crit = new Criteria("settingcategory_objid",$store->inClause($del),'IN');
            }else {
                $crit = new Criteria("settingcategory_objid",$store->inClause($col->set($objs)->col("settingcategory_objid")->get()),'IN');
            }
            return $store->delete($crit);
        }
        return true;
    }

    /**
     * Add setting to DB
     *
     * @access public
     * @param string $com Component
     * @param array $settings
     * @return bool #success
     **/
    public function addSettings($com,$settings) {
        $settings = $this->prefixNames($com,$settings);
		$col = new Collection($settings);
        $store = new SettingStore();
        $catStore = new SettingCategoryStore();

        $names = $col->col("category")->get();
		$systemcategories = ['general','user','contactus'];
        foreach ($names as $k => $v) {
            if (in_array($v,$systemcategories) === false) {
				$names[$k] = $com.'_'.$v;
			}
			else {
				$names[$k] = $v;
			}
        }
        $crit = new Criteria('settingcategory_name',$store->inClause($names),'IN');
        $cats = $catStore->get($crit);
        $settingsObjs = array();
        $next = $store->getNext('setting_objid');
        foreach($settings as $s) {
			$s['category'] = (in_array($v,$systemcategories) === false)?$com.'_'.$s['category']:$s['category'];
            $cat = $col->set($cats)->keep('settingcategory_name',$s['category'])->get();
            $obj = $store->getNew();
            $obj->setVar('setting_objid',$next);
            $obj->setVar('setting_cid',(is_object($cat[0]))?$cat[0]->getVar('settingcategory_objid'):0);
            $obj->setVar('setting_name',$s['name']);
            $obj->setVar('setting_value',$s['value']);
            $obj->setVar('setting_type',$s['type']);
            $obj->setVar('setting_options',$s['options']);
            $obj->setVar('setting_title',$s['title']);
            $obj->setVar('setting_description',$s['description']);
            $obj->setVar('setting_language',(isset($s['language']))?$s['anguage']:0);
            $settingsObjs[] = $obj;
            $next++;
        }
        return $store->save($settingsObjs);
    }

    /**
     * Removes a previously added setting
     *
     * @access public
     * @param string $component
     * @param array $sc
     * @return bool $success
     **/
    public function removeSettings($com,$sc) {
        global $service;
        $sc = $this->prefixNames($com,$sc);
        $store = new SettingStore();
		$col = new Collection();
        $crit = new Criteria("setting_name",$store->inClause($col->set($sc)->col($sc,"name")->get()),'IN');
        return $store->delete($crit);
    }

    /**
     * Prefix names fields with component name
     *
     * Used for settings and setting categories
     *
     * @access public
     * @param string $component
     * @param array $data
     * @return bool $success
     **/
    public function prefixNames($com,$data) {
        foreach($data as $k => $v) {
            if (isset($v['name'])) {
                if (substr($v['name'],0,strlen($com)+1) != $com.'_') {
                    $data[$k]['name'] = $com.'_'.$v['name'];
                }
            }
        }
        return $data;
    }

}
