<?php
global $service;
$service->get('EventHandler')->on('adminmenu.onLoad',
	function($e,$p){
		global $service;
		$langcode = $service->get('Language')->getCode();
        $service->get('Ressource')->get('com/plugin/locale/'.$langcode);
		$p['system']['items'][] = array(
			'type'=>'item',
			'component'=>'plugin',
			'icon'=>'plugin',
			'fa' => 'puzzle-piece',
			'title'=>PLUGIN_PLUGINS,
			'link' => $langcode.'/admin/plugins'
		);
		return $p;
	}
);
?>