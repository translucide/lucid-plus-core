<?php
define('PLUGIN_PLUGINS','Plugins');
define('PLUGIN_TABLECOL_PLUGIN','Plugin');
define('PLUGIN_TABLECOL_DESCRIPTION','Description');
define('PLUGIN_TABLECOL_VERSION','Version');
define('PLUGIN_TABLECOL_ACTIONS','Actions');
define('PLUGIN_TABLECOL_ACTIONS_INSTALL','Install');
define('PLUGIN_TABLECOL_ACTIONS_REMOVE','Remove');

define("PLUGIN_INSTALLPROMPT_TITLE","Install this plugin?");
define("PLUGIN_INSTALLPROMPT_MESSAGE","Are you sure you want to install this plugin ?");
define("PLUGIN_INSTALLRESULT_TITLE_SUCCESS","Plugin installed successfully!");
define("PLUGIN_INSTALLRESULT_TITLE_ERROR","Error installing plugin!");
define("PLUGIN_INSTALLRESULT_MESSAGE_SUCCESS","The plugin was installed successfully. You can now close this window.");
define("PLUGIN_INSTALLRESULT_MESSAGE_ERROR","An error occured while installing this plugin. Try updating the plugin files to the newest version, remove the plugin and try installing again.");

define("PLUGIN_REMOVEPROMPT_TITLE","Remove this plugin?");
define("PLUGIN_REMOVEPROMPT_MESSAGE","This will remove all routes, settings, and setting categories this plugin had installed. Plugin files will not be deleted. Do you still want to remove this plugin ?");
define("PLUGIN_REMOVERESULT_TITLE_SUCCESS","Plugin removed successfully!");
define("PLUGIN_REMOVERESULT_TITLE_ERROR","Error removing plugin!");
define("PLUGIN_REMOVERESULT_MESSAGE_SUCCESS","Plugin removed successfully.");
define("PLUGIN_REMOVERESULT_MESSAGE_ERROR","An error occured when removing this plugin. Some plugin elements might not be uninstalled.");
?>