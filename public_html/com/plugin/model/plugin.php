<?php
/**
 * Plugin model
 *
 * Manages plugin
 *
 * October 28, 2012

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
class PluginModel extends Model {
	function __construct(){
		$this->initVar('plugin_id',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('plugin_component',DATATYPE_STRING,['length' => 100]);
		$this->initVar('plugin_title',DATATYPE_STRING,['length' => 100]);
		$this->initVar('plugin_description',DATATYPE_STRING,['length' => 100]);
		$this->initVar('plugin_version',DATATYPE_STRING,['length' => 25]);
		$this->initVar('plugin_url',DATATYPE_STRING,['length' => 255]);
		$this->initVar('plugin_author',DATATYPE_STRING,['length' => 50]);
		$this->initVar('plugin_authorurl',DATATYPE_STRING,['length' => 255]);
		$this->initVar('plugin_company',DATATYPE_STRING,['length' => 50]);
		$this->initVar('plugin_companyurl',DATATYPE_STRING,['length' => 255]);
		$this->initVar('plugin_status',DATATYPE_INT);
		$this->initVar('plugin_options',DATATYPE_ARRAY);
		$this->initVar('plugin_tenant',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('plugin_site',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('plugin_deleted',DATATYPE_TIMESTAMP);
		$this->initVar('plugin_deletedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('plugin_created',DATATYPE_TIMESTAMP);
		$this->initVar('plugin_createdby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->initVar('plugin_modified',DATATYPE_TIMESTAMP);
		$this->initVar('plugin_modifiedby',(DB_UUIDS)?DATATYPE_UUID:DATATYPE_INT);
		$this->type = 'plugin';
	}
}

class PluginData extends Data {
	function __construct(){
        parent::__construct('plugin');
	}
}

class PluginStore extends Store{
	public function __construct(){
        parent::__construct('plugin');
		$this->setOption('ignorelangs',true);
		$this->setOption('ignoresites',true);
		$this->setOption('usecache',false);
	}

    public function initCrit() {
        $crit = new CriteriaCompo();
        return $crit;
    }
}
