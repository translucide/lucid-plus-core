<h1><i class="fa fa-puzzle-piece"></i>{:$smarty.const.PLUGIN_PLUGINS:}</h1>
<table id="plugins_list" class="table table-striped table-bordered" width="100%" cellspacing="0">
<thead>
    <tr>
        <th>{:$smarty.const.PLUGIN_TABLECOL_PLUGIN:}</th>
        <th>{:$smarty.const.PLUGIN_TABLECOL_DESCRIPTION:}</th>
        <th>{:$smarty.const.PLUGIN_TABLECOL_VERSION:}</th>
        <th>{:$smarty.const.PLUGIN_TABLECOL_ACTIONS:}</th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th>{:$smarty.const.PLUGIN_TABLECOL_PLUGIN:}</th>
        <th>{:$smarty.const.PLUGIN_TABLECOL_DESCRIPTION:}</th>
        <th>{:$smarty.const.PLUGIN_TABLECOL_VERSION:}</th>
        <th>{:$smarty.const.PLUGIN_TABLECOL_ACTIONS:}</th>
    </tr>
</tfoot>
</table>
<script language="javascript" type="text/javascript">
var pluginsListTable = $('#plugins_list').DataTable( {
	"processing": true,
	"serverSide": true,
	"ajax": "{:$url:}{:$lang:}/api/plugin/controller/plugin/loadplugins",
    "columns" : [
        { "data": "title" },
        { "data": "description"},
        { "data": "version" },
        { "data": "actions" }
    ]
}).on('draw',function(){
    $('#plugins_list .DataTables_filter input')
        .unbind('keypress keyup')
        .bind('keypress keyup', function(e) {
        if ($(this).val().length > 0 && $(this).val().length < 3 && e.keyCode != 13) return;
        pluginsListTable.fnFilter($(this).val());
    });    
});
function installPlugin(id) {
    $(".app.lucidinterface").lucidInterface("messageDialog",{
        type: "prompt",
        title: "{:$smarty.const.PLUGIN_INSTALLPROMPT_TITLE:}",
        message: "{:$smarty.const.PLUGIN_INSTALLPROMPT_MESSAGE:}",
        data:{
            'id' : id
        },
        actions: {
            no: true,
            yes: function(e){
                console.info(e);
                $.ajax({
                    method: "POST",
                    url: "{:$url:}{:$lang:}/api/plugin/controller/plugin/install",
                    data: {
                        'id':e.data.id
                    },
                    success: function(d){
                        $(".app.lucidinterface").lucidInterface("messageDialog",{
                            type: ((d.success)?"success":"error"),
                            title: ((d.success)?"{:$smarty.const.PLUGIN_INSTALLRESULT_TITLE_SUCCESS:}":"{:$smarty.const.PLUGIN_INSTALLRESULT_TITLE_ERROR:}"),
                            message: d.message,
                            actions: {
                                ok: function(){
                                    $('#plugins_list').DataTable().draw();
                                }
                            }
                        });
                    }
                });
            }
        }
    });
}
function removePlugin(id) {
    $(".app.lucidinterface").lucidInterface("messageDialog",{
        type: "prompt",
        title: "{:$smarty.const.PLUGIN_REMOVEPROMPT_TITLE:}",
        message: "{:$smarty.const.PLUGIN_REMOVEPROMPT_MESSAGE:}",
        data:{
            'id' : id
        },
        actions: {
            no: true,
            yes: function(e){
                console.info(e);
                $.ajax({
                    method: "POST",
                    url: "{:$url:}{:$lang:}/api/plugin/controller/plugin/remove",
                    data: {
                        'id':e.data.id
                    },
                    success: function(d){
                        $(".app.lucidinterface").lucidInterface("messageDialog",{
                            type: ((d.success)?"success":"error"),
                            title: ((d.success)?"{:$smarty.const.PLUGIN_REMOVERESULT_TITLE_SUCCESS:}":"{:$smarty.const.PLUGIN_REMOVERESULT_TITLE_ERROR:}"),
                            message: d.message,
                            actions: {
                                ok: function(){
                                    $('#plugins_list').DataTable().draw();
                                }
                            }
                        });
                    }
                });
            }
        }
    });
}
</script>