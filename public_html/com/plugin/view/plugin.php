<?php
/**
 * Plugin view
 *
 * Default view loaded on plugins page request
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('sys/core/view');

class PluginView extends DefaultView{
	public function init(){
	}
	
	public function render(){
		global $service;
        $service->get('Ressource')->get('lib/datatables/1.10.15/jquery.dataTables.min');
        $service->get('Ressource')->get('lib/datatables/1.10.15/dataTables.bootstrap');
        $service->get('Ressource')->get('lib/vendor/ajaxform');
        $service->get('Ressource')->get('lib/packages/form/form');
        $service->get('Ressource')->get('lib/jquery-hotkeys/2016-09-18/jquery.hotkeys');
        $service->get('Ressource')->get('lib/lucidinterface/0.1/lucidinterface');
        $service->get('Ressource')->get('com/plugin/asset/js/app');
		$service->get('Ressource')->get('core/display/interface');
		$interface = new LucidInterface([
			'id' => 'plugin',
			'name' => 'plugin',
			'childs' => [
				new LucidViewport([
					'childs' => [
						new LucidLayout([
							'childs' => [
								new LucidHBox([
									'childs' => [
										new LucidBreadCrumbs([
											'childs' => $this->data['breadcrumbs']
										])
									]
								]),
								new LucidHBox([
									'childs' => [
										new LucidPanel([
											'id' => 'formpanel',
											'html' =>  '<div class="app lucidinterface"></div>'
										])
									]
								])
							]
						])
					]
				])
			]
		]);
        
        $ret = $interface->render();        
        $service->get('Ressource')->addSCript("
        $('.app.lucidinterface').lucidInterface({
            'initScreen' : 'plugins_list',
            'initComponent' : 'plugin',
            'initController' : 'plugin',
            'root': '".URL."',
            'lang': '".$service->get('Language')->getCode()."'
        });
        ");
		return $ret;
	}
}
?>