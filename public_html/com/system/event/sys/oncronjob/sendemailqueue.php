<?php
global $service;
$service->get('Ressource')->get('core/model/queue');
$service->get('Ressource')->get('core/protocol/email');
$service->get('EventHandler')->on('sys.onCronJob',
	function($e,$p){
		global $service;
        $service->get('Ressource')->get('sys/display/cli/textoutput');
        $c = new TextOutput();
        ob_start();

        //Select the next 50 emails to send. No more than 50 emails per minute will be sent.
        //WE might want to use the taskscheduler to run more than 50 emails per minute.
        $qStore = new QueueStore();

        $crit = new CriteriaCompo();
        $crit->add(new Criteria('queue_action','sendemail'));
        $crit->add(new Criteria('queue_status','(0,2)','IN')); //2 = failed before. 0 = to send.
        $crit->setSort('queue_status','ASC');
        $crit->setLimit(0,50);
        $emails = $qStore->get($crit);
        $status1 = array();
        $status2 = array();
        foreach($emails as $k => $v) {
            $email = new Email();
            $email->setOptions($v->getVar('queue_options'));
            $res = $email->send();

            //Send an email to admin when a mail fails... but use the mail function this time instead
            //of PHPMAiler
            if ($res == false) {
                $status2[] = $v->getVar('queue_id');
                echo $c->l($email->error()."\n\nOriginal message: ".$v->getVar('queue_options')['subject']."\n\n".$v->getVar('queue_options')['body']);
            }else{
                $status1[] = $v->getVar('queue_id');
            }
        }

        //Sent emails : Update  status in queue, mark as sent
        if (count($status1)) {
            $crit = new CriteriaCompo();
            $crit->add(new Criteria('queue_action','sendemail'));
            $crit->add(new Criteria('queue_id',$qStore->inClause($status1),'IN'));
            $qStore->update(['queue_status'=>1],$crit);
        }

        //Failed emails : Update  status in queue, mark as failed
        if (count($status2)) {
            $crit = new CriteriaCompo();
            $crit->add(new Criteria('queue_action','sendemail'));
            $crit->add(new Criteria('queue_id',$qStore->inClause($status2),'IN'));
            $qStore->update(['queue_status'=>2],$crit);
        }

        $buffer = ob_get_contents();
        ob_end_clean();
        if (strlen($buffer)) {
            echo $c->t('sendemailqueue: sys.onCronJob (com/system/event/sys/oncronjob/sendmailqueue)');
            echo $c->l('Some emails were not sent. We will retry to send those later. You can view mail details below. ');
            echo $buffer;
        }

        return $p;
	}
);
?>
