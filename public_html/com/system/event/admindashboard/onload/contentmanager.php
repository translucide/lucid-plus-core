<?php
global $service;
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/contentmanager');
$service->get('EventHandler')->on('admindashboard.onLoad',
	function($e,$p){
		global $service;
		$langcode = $service->get('Language')->getCode();
		$service->get('Ressource')->get('com/admin/view/admindashboardblock');
		$items = array(
			array(
				'icon'=>$service->get('Ressource')->getIcon(['component'=>'system','icon'=>'content']),
				'glyphicon' => 'file',
				'title'=>_LOCATION_CONTENT,
				'link' => $langcode.'/admin/content'
			)
		);

		$view = new AdmindashboardblockView();
		$view->setVar('priority',100);
		$view->setVar('width',1);
		$view->setVar('title',"<span><img src=\"".URL.$items[0]['icon']."\" width=\"16\"></span> ".CONTENTMANAGER_CONTENTSTATS);
		$view->setVar('type','html');
		$langcode = $service->get('Language')->getCode();
		$code = '';
		$store = new ContentStore();
		$crit = new Criteria('content_objid',0,'>');
		$crit->setSort("content_modified");
		$crit->setOrder('DESC');
		$crit->setLimit('1');
		$crit->setStart('0');
		$obj = $store->get($crit);
		if (is_object($obj[0])) {
			$mostrecent = $obj[0]->getVar("content_modified");
			$code .= '<p>'.CONTENTMANAGER_LASTMODIF.': '.$service->get('Time')->toElapsedTimeString($mostrecent).' '.CONTENTMANAGER_LASTMODIF_ONPAGE.' "'.$obj[0]->getVar("content_title").'"</p>';	
		}

		//Select last year post count
		$crit = new Criteria("content_created",time() - 60*60*24*365,'>');		
		$count = $store->count($crit);
		if ($count/365 < 1) $code .= '<p>'.CONTENTMANAGER_AVERAGEPOSTFREQUENCYYEAR.': '.sprintf(CONTENTMANAGER_EVERYDAY,round(1/($count / 365),1)).'</p>';
		else $code .= '<p>'.CONTENTMANAGER_AVERAGEPOSTFREQUENCYYEAR.': '.round($count / 365,2).' '.CONTENTMANAGER_PERDAY.'</p>';
		
		//Select last 30 days post count
		$crit = new Criteria("content_created",time() - 60*60*24*30,'>');		
		$count = $store->count($crit);
		if ($count/365 < 1) $code .= '<p>'.CONTENTMANAGER_AVERAGEPOSTFREQUENCYMONTH.': '.sprintf(CONTENTMANAGER_EVERYDAY,round(1/($count / 365),1)).'</p>';
		else $code .= '<p>'.CONTENTMANAGER_AVERAGEPOSTFREQUENCYMONTH.': '.round($count / 365,2).' '.CONTENTMANAGER_PERDAY.'</p>';
		
		$count = $store->count();
		$code .= '<p>'.CONTENTMANAGER_CONTENTCOUNT.': '.$count.'</p>';	
		
		$code .= "<a href=\"".URL.$items[0]['link']."\"><span><img src=\"".URL.$items[0]['icon']."\" width=\"16\"></span> {$items[0]['title']}</a>";
		$code .= '';
		$view->setVar('items',$code);
		return $view->render();
	}
);
?>