<?php
global $service;
$service->get('Ressource')->get('core/model/comment');
$service->get('Event')->on('search.query',
	function($e,$p){
		global $service;
		$crit = new Criteria('comment_content','%'.$p.'%','LIKE');
		$store = new CommentStore();
        $crit->setLimit(100);
        $crit->setStart(0);
		$ids = array();
		$punc = array('<','>','.',':','(',')','-','_','+','=',';',',','!','@','#','$','%','*','\\','/','\'','"','?');
        $obj = $store->get($crit);
        $page = 0;
        while(count($obj) > 0) {            
            foreach ($obj as $k => $v){
                $content = ' '.str_replace($punc,' ',$v->getVar('comment_content')).' ';
                if (strpos($content,' '.$p.' ')) $ids[] = $v->getVar('comment_contentobjid');
            }
            $page++;
            $crit->setStart($page*100);            
            $obj = $store->get($crit);
        }
		return $ids;
	}
);
?>