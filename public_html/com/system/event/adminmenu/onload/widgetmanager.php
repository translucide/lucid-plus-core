<?php
global $service;
$service->get('EventHandler')->on('adminmenu.onLoad',
	function($e,$p){
		global $service;
		$service->get('Ressource')->get('com/admin/lang/'.$service->get('Language')->getCode().'/system');
		$langcode = $service->get('Language')->getCode();
		$p['pages']['items'][] = array(
			'type'=>'item',
			'component'=>'system',
			'icon'=>'widget',
			'glyphicon' => 'tasks',
			'title'=>_LOCATION_WIDGETS,
			'link' => $langcode.'/admin/widget'
		);
		return $p;
	}
);
?>