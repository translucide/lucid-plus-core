<?php
global $service;
$service->get('EventHandler')->on('adminmenu.onLoad',
	function($e,$p){
		global $service;
		$langcode = $service->get('Language')->getCode();
		$p['system']['items'][] = array(
			'type'=>'item',
			'component'=>'system',
			'icon'=>'setting',
			'glyphicon' => 'cog',
			'title'=>_LOCATION_SETTINGS,
			'link' => $langcode.'/admin/settings'
		);
		return $p;
	}
);
?>