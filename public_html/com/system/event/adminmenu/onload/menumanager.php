<?php
global $service;
$service->get('EventHandler')->on('adminmenu.onLoad',
	function($e,$p){
		global $service;
		$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/menumanager');
		$langcode = $service->get('Language')->getCode();
		$p['pages']['items'][] = array(
			'type'=>'item',
			'component'=>'system',
			'icon'=>'menu',
			'glyphicon' => 'list',
			'title'=>SYSTEM_MENUMANAGER,
			'link' => $langcode.'/admin/menu');
		return $p;
	}
);
?>