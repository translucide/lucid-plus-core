<?php
/**
 * Source code widget
 *
 * May 6, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/helper/actionmanager');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/sourcecode');

class SourcecodeWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'sourcecode',
			'title' => SYSTEM_WIDGET_SOURCECODE_TITLE,
			'description' => SYSTEM_WIDGET_SOURCECODE_DESC,
			'wireframe' => 'sourcecode',
			'icon' => 'sourcecode',
			'saveoptions' => array('actions','jssource','csssource','htmlsource')
		));
	}
	
	public function render(){
		global $service;
		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','sourcecode',$opt['stylesheet']);
		$id = 'widget_sourcecode_'.$this->data->getVar('widget_objid');
        $content = '<section id="'.$id.'" class="widget html sourcecode '.$opt['stylesheet'].' '.$this->data->getVar('widget_options')['widgetclasses'].'">';
		$content .= '<div class="ct">';
        if (trim($opt['csssource']) != '') {
            $css = str_replace(
                array('{id}','{url}','{dataurl}','{uploadurl}','{themeurl}'),
                array($id,URL,DATAURL,UPLOADURL,THEMEURL),
                $opt['csssource']
            );
			$content .= '<style type="text/css">'."\n".$css."\n".'</style>';
		}
        $content .= $opt['htmlsource'];
        if (trim($opt['jssource']) != '') $service->get('Ressource')->addScript(
            str_replace(
                array('{id}','{url}','{dataurl}','{uploadurl}','{themeurl}'),
                array($id,URL,DATAURL,UPLOADURL,THEMEURL),
                $opt['jssource']
            )
        );
		$action = new ActionManager();
		$content .= '<div class="actions">'.$action->render($this->data->getVar('widget_objid')).'</div>';
		$content .= '</div></section>';
		return $content;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');				
		$form->add(new TextareaFormField('htmlsource',$options['htmlsource'],array(
			'tab'=> 'basic',
			'width' => 4,
            'rows' => '20',
			'title' => _HTML,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','htmlsource')
		)));
		$form->add(new TextareaFormField('csssource',$options['csssource'],array(
			'tab'=> 'basic',
			'width' => 4,
            'rows' => '20',
			'title' => _CSS
		)));
		$form->add(new TextareaFormField('jssource',$options['jssource'],array(
			'tab'=> 'basic',
			'width' => 4,
            'rows' => '20',
			'title' => _JS
		)));		
		$action = new ActionManager();
		$form->add($action->getFormField(URL.$service->get('Language')->getCode().'/api/system/widget/sourcecode/',$defobj));
		return $form;
	}

	public function apiCall($op){
		global $service;
		$action = new ActionManager();
		$op = str_replace('action','',$op);
		switch($op) {
			case 'list' :
			case 'move' :
			case 'delete' :
			case 'duplicate': 
			case 'edit' :
			case 'save' : {
				$ret = $action->$op();
			}break;
			default : break;
		}
		return $ret;
	}
	
	public function hasAccess($op){
		global $service;		
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}	
	
}
?>