<?php
/**
 * Quote widget
 *
 * May 6, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/quote');

class QuoteWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'quote',
			'title' => SYSTEM_WIDGET_QUOTE_TITLE,
			'wireframe' => 'quote',
			'icon' => 'quote',
			'saveoptions' => array('quote','author','authorsummary')
		));
	}
	
	public function render(){
		global $service;
        $opt = $this->data->getVar('widget_options');
        $opt['stylesheet'] = ($opt['stylesheet'] == '')?'default':$opt['stylesheet'];
		$service->get('Ressource')->getStyle('system','widget','quote',$opt['stylesheet']);
		$cls = uniqid('cls_');
        $content = '<section class="widget html quote '.$opt['stylesheet'].' '.$cls.' '.$this->data->getVar('widget_options')['widgetclasses'].'">
			<h1'.(($opt['displaywidgettitle'])?'':' class="hidden"').'>'.$this->data->getVar('widget_title').'</h1>
			<blockquote>';
        if ($opt['quote'] != '') $content .= '<p class="quote">'.$opt['quote'].'</p>';
        if ($opt['author'] != '') $content .= '<p class="author">'.$opt['author'].'</p>';
        if ($opt['authorsummary'] != '') $content .= '<p class="authorsummary">'.$opt['authorsummary'].'</p>';
        $content .= '</blockquote>';
		$content .= '</section>';
        return $content;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
				
		$form->add(new TextareaFormField('quote',$options['quote'],array(
			'tab'=> 'basic',
			'width' => 12,
			'title' => SYSTEM_WIDGET_QUOTE_QUOTE,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','quote')
		)));
		$form->add(new TextFormField('author',$options['author'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_QUOTE_AUTHOR,
			'width' => 4,
            'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','author')            
		)));
		$form->add(new TextFormField('authorsummary',$options['authorsummary'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_QUOTE_AUTHORSUMMARY,
			'width' => 4,
            'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','authorsummary')            
		)));
		return $form;
	}
}
?>