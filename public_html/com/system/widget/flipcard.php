<?php
/**
 * Flipcard widget
 *
 * May 6, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/data');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/flipcard');
$service->get('Ressource')->get('core/helper/actionmanager');

class FlipcardWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'flipcard',
			'title' => SYSTEM_WIDGET_FLIPCARD_TITLE,
			'wireframe' => 'flipcard',
			'icon' => 'flipcard',
			'saveoptions' => array('fronttitle','fronttext','backtitle','backtext','frontimage','bgimageposition','bgimage')
		));
	}
	
	public function render(){
		global $service;
        $opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','flipcard',$opt['stylesheet']);
		$cls = uniqid('cls_');
		$bgimg = '';
		if (isset($opt['bgimage']) && $opt['bgimage'] != '') {
			$bgimg = 'background-image:url(\''.UPLOADURL.$opt['bgimage'].'\');background-repeat:no-repeat;background-position:'.((isset($opt['bgimageposition']) && $opt['bgimageposition'] != '')?$opt['bgimageposition']:'left top').';background-size:cover;';
		}
		$content = '<style type="text/css">.'.$cls.' .front{'.$bgimg.'}</style>';
        $content .= '
		<section id="widget_flipcard_'.$this->data->getVar('widget_objid').'" class="widget html flipcard '.$opt['stylesheet'].' '.$cls.' '.$this->data->getVar('widget_options')['class'].'" ontouchstart="this.classList.toggle(\'hover\');" onclick="this.classList.toggle(\'hover\');">
		<div class="ct">
			<div class="flipper">
				<div class="front face">
					<div class="ct">';
        if ($opt['frontimage'] != '') $content .= '<img src="'.UPLOADURL.$opt['frontimage'].'">';
        if ($opt['fronttitle'] != '') $content .= '<h1>'.$opt['fronttitle'].'</h1>';
        if ($opt['fronttext'] != '') $content .= '<p>'.$opt['fronttext'].'</p>';
        $content .= '
					</div>
				</div>
				<div class="back face">
					<div class="ct">';
        if (trim($opt['backtitle']) != '') $content .= '<h2>'.$opt['backtitle'].'</h2>';
        if (trim($opt['backtext']) != '') $content .= '<p>'.$opt['backtext'].'</p>';
		$action = new ActionManager();
		$content .= '<div class="actions">'.$action->render($this->data->getVar('widget_objid')).'</div>';		
        $content .= '
					</div>
				</div>
			</div>
		</div>
		</section>';
        return $content;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
				
		$form->add(new TextFormField('fronttitle',$options['fronttitle'],array(
			'tab'=> 'basic',
			'width' => 6,
			'title' => SYSTEM_WIDGET_FLIPCARD_FRONTTITLE,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','fronttitle')
		)));
		$form->add(new TextFormField('backtitle',$options['backtitle'],array(
			'tab'=> 'basic',
			'width' => 6,
			'title' => SYSTEM_WIDGET_FLIPCARD_BACKTITLE,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','backtitle')
		)));
		$form->add(new HtmleditorFormField('fronttext',$options['fronttext'],array(
			'tab'=> 'basic',
			'width' => 6,
			'title' => SYSTEM_WIDGET_FLIPCARD_FRONTTEXT,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','fronttext')
		)));
		$form->add(new HtmleditorFormField('backtext',$options['backtext'],array(
			'tab'=> 'basic',
			'width' => 6,
			'title' => SYSTEM_WIDGET_FLIPCARD_BACKTEXT,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','backtext')
		)));
		$form->add(new ImageuploadFormField('frontimage',$options['frontimage'],array(
			'tab'=> 'basic',
			'title' => _PICTURE,
			'width' => 4
		)));
		$form->add(new ImageuploadFormField('bgimage',$options['bgimage'],array(
			'tab'=> 'basic',
			'title' => _BGIMAGE,
			'width' => 4
		)));
		$form->add(new SelectFormField('bgimageposition',$options['bgimageposition'],array(
			'tab'=> 'basic',
			'width' => 4,
			'title' => _BGPOS,
			'options' => array(
				array('title' => _LEFT.' '._TOP, 'value' => 'left top'),
				array('title' => _RIGHT.' '._TOP, 'value' => 'right top'),
				array('title' => _LEFT.' '._CENTER, 'value' => 'left center'),
				array('title' => _RIGHT.' '._CENTER, 'value' => 'right center'),
				array('title' => _LEFT.' '._BOTTOM, 'value' => 'left bottom'),
				array('title' => _RIGHT.' '._BOTTOM, 'value' => 'right bottom'),
				array('title' => _CENTER.' '._CENTER, 'value' => 'center center'),
				array('title' => _CENTER.' '._TOP, 'value' => 'center top'),
				array('title' => _CENTER.' '._BOTTOM, 'value' => 'center bottom')
			)
		)));	
		$action = new ActionManager();
		$form->add($action->getFormField(URL.$service->get('Language')->getCode().'/api/system/widget/flipcard/',$defobj));
		return $form;
	}

	public function apiCall($op){
		global $service;
		$action = new ActionManager();
		switch($op) {
			case 'actionlist' :
			case 'actionmove' :
			case 'actiondelete' :
			case 'actionduplicate': 
			case 'actionedit' :
			case 'actionsave' : {
				$op2 = str_replace('action','',$op);
				$ret = $action->$op2();
			}break;
			default : break;
		}
		return $ret;
	}

	public function hasAccess($op){
		global $service;		
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}		
}
?>