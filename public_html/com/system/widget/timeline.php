<?php
/**
 * Timeline widget
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget/listwidget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/timeline');

class TimelineWidget extends ListWidget{
	private $wn;
	
	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->wn = 'timeline';
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => $this->wn,
			'title' => SYSTEM_WIDGET_TIMELINE_TITLE,
			'description' => SYSTEM_WIDGET_TIMELINE_DESCRIPTION,
			'wireframe' => $this->wn,
			'icon' => $this->wn,
			'saveoptions' => $this->getOptionsNames(),
			'copyoptions' => [
				'data' => [
					'data' => [
						'select' => [
							'data_type' => '{parentname}item',
							'data_parent' => '{parentobjid}'
						],
						'update' => [
							'data_parent' => '{parentobjid}'
						]
					]
				]
			]
		));
	}

	public function render(){
		global $service;
        $service->get('Ressource')->get('lib/timeline/0.1/main');
        $service->get('Ressource')->get('lib/timeline/0.1/style');
		/*$service->get('Ressource')->addScript('
			$("section#widget_timeline_'.$this->data->getVar('widget_objid').' section").matchHeight();
			$("section#widget_timeline_'.$this->data->getVar('widget_objid').' section h1").matchHeight();
			$("section#widget_timeline_'.$this->data->getVar('widget_objid').' section h2").matchHeight();
			$("section#widget_timeline_'.$this->data->getVar('widget_objid').' section p").matchHeight();');*/
        $i = $this->getInfo();
		$w = $this->data;
		$o = $w->getVar('widget_options');
		$service->get('Ressource')->getStyle($i['component'],'widget',$i['name'],$o['stylesheet']);
		if ($o['stylesheet'] == '') $o['stylesheet'] = 'default';
		
		global $service;
		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','titleimage',$opt['stylesheet']);
		
		$cls = uniqid('cls_');
		$bgi = $bgp = '';
		if (isset($opt['bgimage']) && $opt['bgimage'] != '') {
			$bgi = 'background-image: url("'.UPLOADURL.$opt['bgimage'].'");';
			$bgp = 'background-position:'.((isset($opt['bgimageposition']) && $opt['bgimageposition'] != '')?$opt['bgimageposition']:'left top').';';
		}
		switch($opt['stylesheet']) {
			case 'dotted-background-overlay' : {
				$bgi = 'background-image: url("'.URL.'com/system/asset/widget/titleimage/blackdot-pattern.png"), '.
					str_replace('background-image: ','',$bgi);
			} break;
			case 'dark-background-overlay' : {
				$bgi = 'background-image: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), '.
					str_replace('background-image: ','',$bgi);
			} break;
		}
		$service->get('Ressource')->addStyle('.'.$cls.'{'.$bgi.$bgp.'}');     
        $m = $this->getItems();		
		$content .= "<section id='widget_".$i['name']."_".$w->getVar('widget_objid')."' class='widget html ".$i['name']." {$o['stylesheet']} ".$o['widgetclasses']."'>";
		$content .= "<div class='ct'>";
		$content .= "<h1".(($o['displaywidgettitle'])?"":" class='hidden'").">".$w->getVar('widget_title')."</h1>";
		if ($o['toptext']) $content .= '<p>'.$o['toptext'].'</p>';
        $content .= "<section class='cd-horizontal-timeline'><div class='timeline'><div class='events-wrapper'><div class='events'><ol>";
		foreach ($m as $k => $v){
            $selected = ($k == 0)?' class="selected"':'';
			$content .= '<li><a href="#'.$k.'" data-date="'.$this->timelineDate($v->getVar('data_data')['eventdate']).'"'.$selected.'>'.$v->getVar('data_data')['eventdatetext'].'</a></li>';
		}
		$content .= "</ol><span class='filling-line' aria-hidden='true'></span></div></div><ul class=\"cd-timeline-navigation\"><li><a href=\"#0\" class=\"prev inactive\">Prev</a></li><li><a href=\"#0\" class=\"next\">Next</a></li></ul></div> <!-- .timeline --><div class=\"events-content\"><ol>";
		foreach ($m as $k => $v){
            $selected = ($k == 0)?' class="selected"':'';
			$content .= '<li'.$selected.' data-date="'.$this->timelineDate($v->getVar('data_data')['eventdate']).'"><section>'.$this->renderItem($v,$w).'</section></li>';
		}
        $content .= '</ol></div></section>';
		if ($o['bottomtext']) $content .= '<p>'.$o['bottomtext'].'</p>';
		$action = new ActionManager();
		$actions = $action->render($this->data->getVar('widget_objid'));
		if ($actions != '') $content .= '<div class="actions">'.$actions.'</div>';		
		$content .= "</div></section>";
		return $content;
    }
	
	protected function renderItem($v,$w){
		global $service;
		$k = $v->getVar('data_objid');
		if ($v->getVar('data_title')) $content .= "<h1>{$v->getVar('data_title')}</h1>";
		if ($v->getVar('data_data')['subtitle']) $content .= "<h2>{$v->getVar('data_data')['subtitle']}</h2>";
		if ($v->getVar('data_data')['description']) $content .= "<p class='description'>{$v->getVar('data_data')['description']}</p>";
		if ($v->getVar('data_data')['detailslink']) {
            $target = " target=\"_blank\"";
            if (strstr($v->getVar('data_data')['detailslink'],URL) !== false) {
                $target="";
            }
            $content .= "<div class='actions'><a href='".$v->getVar('data_data')['detailslink']."'".$target.">"._DETAILS."</a></div>";
        }
		return $content;
	}

	protected function editItem($data){
		global $service;
		$store = new DataStore();
		$defaultlang = $service->get('Language')->getDefault();		
		$form = $data['form'];
		$form->setTitle(SYSTEM_WIDGET_TIMELINE_TITLE);
		$defobj = $store->getDefaultObj($data['data']);
		$obj = $data['data'];
		$form->add(new TextFormField('subtitle',$defobj->getVar('data_data')['subtitle'],array(
			'title' => _SUBTITLE,
			'width' => 6
		)));
		$form->add(new DatepickerFormField('eventdate',$defobj->getVar('data_data')['eventdate'],array(
			'title' => SYSTEM_WIDGET_TIMELINE_DATE,
			'width' => 3
		)));
		$form->add(new TextFormField('eventdatetext',$defobj->getVar('data_data')['eventdatetext'],array(
			'title' => SYSTEM_WIDGET_TIMELINE_DATETEXT,
			'width' => 3
		)));
		$form->add(new TextareaFormField('description',$defobj->getVar('data_data')['description'],array(
			'title'=>_TEXT,
			'width' => 12,
			'length'=>255,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','text')
		)));
		$form->add(new TextFormField('detailslink',$defobj->getVar('data_data')['detailslink'],array(
			'title' => _LINK,
			'width' => 6
		)));            
		return $form;
	}
	
	protected function saveItem() {
		return array('description','eventdate','subtitle','detailslink','eventdatetext');
	}
    
    protected function timelineDate($date) {
        $time = $this->timelineTime($date);
        return date("m/d/Y",$time);
    }
    
    protected function timelineTime($date) {
        $date = explode("-",$date);
        return mktime(0,0,0,$date[1],$date[2],$date[0]);
    }
}
?>