<?php
/**
 * Popup lead form
 *
 * May 6, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/popupleadform');

class PopupleadformWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$p = 'popupleadform_';
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'action',
			'name' => 'popupleadform',
			'title' => SYSTEM_WIDGET_POPUPLEADFORM_TITLE,
			'description' => SYSTEM_WIDGET_POPUPLEADFORM_DESC,
			'icon' => 'popupleadform',
			'wireframe' => 'popupleadform',
			'saveoptions' => array($p.'form',$p.'url',$p.'btntext',$p.'icon',$p.'disablesubject',$p.'subject')
		));
	}

	public function render(){
		global $service;
        $opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->get('com/system/ressource/action/popupleadform/0.1/popupleadform');
		$service->get('Ressource')->getStyle('system','widget','image',$opt['stylesheet']);
		$extras = '';
		if ($opt['popupleadform_subject'] != '') $extras .= ' data-subject="'.$opt['popupleadform_subject'].'"';
		if ($opt['popupleadform_disablesubject'] != '') $extras .= ' data-subject-disabled="'.$opt['popupleadform_disablesubject'].'"';
        $content = '<button class="widget action popupleadform '.$opt['stylesheet'].'" data-id="'.$this->data->getVar('widget_objid').'" data-form="'.$opt['popupleadform_form'].'"'.$extras.'>';
		$content .= '<span class="'.$opt['popupleadform_icon'].'"></span>';
		$content .= $opt['popupleadform_btntext'];
        $content .= '</button>';
        return $content;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		$form->add(new TextFormField('popupleadform_btntext',$options['popupleadform_btntext'],array(
			'title' => _BUTTONTEXT,
			'width' => 4,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','popupleadform_btntext')
		)));
		$forms = $store->get(new Criteria('widget_name','form'));
		$selopts = array();
		foreach($forms as $k => $v) {
			$selopts[] = ['title' => $v->getVar('widget_title'),'value' => $v->getVar('widget_objid')];
		}
		$form->add(new SelectFormField('popupleadform_form',$options['popupleadform_form'],array(
			'title' => _FORM,
			'width' => 4,
			'options' => $selopts
		)));
		$form->add(new TextFormField('popupleadform_url',$options['popupleadform_url'],array(
			'title' => _URL,
			'width' => 4,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','popupleadform_url')
		)));
		$form->add(new SelectFormField('popupleadform_icon',$options['popupleadform_icon'],array(
			'title' => _ICON,
			'width' => 4,
			'options' => array(
				['value' => 'glyphicon glyphicon-music', 'title' => 'Music'],
				['value' => 'glyphicon glyphicon-film', 'title' => 'Film'],
				['value' => 'glyphicon glyphicon-ok', 'title' => 'OK'],
				['value' => 'glyphicon glyphicon-file', 'title' => 'File'],
				['value' => 'glyphicon glyphicon-download', 'title' => 'Download'],
				['value' => 'glyphicon glyphicon-download-alt', 'title' => 'Download (Alternate)'],
				['value' => 'glyphicon glyphicon-upload', 'title' => 'Upload'],
				['value' => 'glyphicon glyphicon-play', 'title' => 'Play'],
				['value' => 'glyphicon glyphicon-share', 'title' => 'Share'],
				['value' => 'glyphicon glyphicon-plus-sign', 'title' => 'Plus sign'],
				['value' => 'glyphicon glyphicon-ok-sign', 'title' => 'OK sign'],
				['value' => 'glyphicon glyphicon-ok-circle', 'title' => 'OK circle'],
				['value' => 'glyphicon glyphicon-circle-arrow-up', 'title' => 'Circle arrow up'],
				['value' => 'glyphicon glyphicon-circle-arrow-down', 'title' => 'Circle arrow down'],
				['value' => 'glyphicon glyphicon-usd', 'title' => 'Dollar sign']
			)
		)));
		$form->add(new TextFormField('popupleadform_subject',$options['popupleadform_subject'],array(
			'title' => _SUBJECT,
			'width' => 4,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','popupleadform_subject')
		)));
		$form->add(new YesnoFormField('popupleadform_disablesubject',$options['popupleadform_disablesubject'],array(
			'title' => _DISABLED,
			'width' => 4
		)));
		return $form;
	}

	public function apiCall($op){
		global $service;
		$request = $service->get('Request')->get();
		$ret = array('success ' => false);
		switch($op) {
			case 'geturl' : {
				$id = $request['id'];
				if ($id) {
					$store = new WidgetStore();
					$obj = $store->getByObjId($id);
					if ($obj[0]) {
						$url = $obj[0]->getVar('widget_options')['popupleadform_url'];
						if (strpos($url,URL) === false && $url != '') $url = URL.$url;
						$ret = array('success' => true, 'data' => $url);
					}
				}
			}break;
		}
		return $ret;
	}

	public function hasAccess($op){
		global $service;
		switch ($op) {
			case 'geturl' : {
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}
}
?>
