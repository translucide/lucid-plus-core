<?php
/**
 * Hero unit
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/image');
$service->get('Ressource')->get('core/helper/actionmanager');

class ImageWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'image',
			'title' => SYSTEM_WIDGET_IMAGE_TITLE,
			'description' => SYSTEM_WIDGET_IMAGE_DESC,
			'icon' => 'image',
			'wireframe' => 'image',
			'saveoptions' => array('image','bgimageposition','bgimage','bgcolor','text','textalign')
		));
	}
	
	public function render(){
		global $service;
        $opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','image',$opt['stylesheet']);
		$cls = uniqid('cls_');
		$bgimg = '';
		if (isset($opt['bgimage']) && $opt['bgimage'] != '') {
			$bgimg = 'background-image:url(\''.UPLOADURL.$opt['bgimage'].'\');background-repeat:no-repeat;background-position:'.((isset($opt['bgimageposition']) && $opt['bgimageposition'] != '')?$opt['bgimageposition']:'left top').';background-size:cover;';
		}
		if (isset($opt['bgcolor']) && $opt['bgcolor'] != '#000000') $bgimg .= 'background-color:'.$opt['bgcolor'].';';
		$content = '<style type="text/css">.'.$cls.'{'.$bgimg.'}'.str_replace("{cls}",$cls,$opt['extracss']).'</style>';
        $content .= '<section class="widget image '.$opt['stylesheet'].' '.$cls.' '.$this->data->getVar('widget_options')['class'].' '.$this->data->getVar('widget_options')['textalign'].'"><div class="ct">';
		if (strlen($this->data->getVar('widget_title')) && $opt['displaywidgettitle']) {
			$h1 = '<h1>'.$this->data->getVar('widget_title').'</h1>';
		}else $h1 = '';
		if (strlen($opt['text']) && $opt['text']) {
			$p = '<p>'.$opt['text'].'</p>';
		}else $p = '';
		
		if (strlen($opt['image']) > 0) $img = '<img src="'.UPLOADURL.'{src}" alt="{title}" title="{title}">';
		else $img = '';
		$img = str_replace('{src}',$opt['image'],$img);
		$img = str_replace('{title}',str_replace(["&reg;","®"],"",str_replace("\"","&quot;",$this->data->getVar('widget_title'))),$img);

        $content .= $h1.$p.$img;
		$action = new ActionManager();
		$content .= '<div class="actions">'.$action->render($this->data->getVar('widget_objid')).'</div>';
		$content .= '</div></section>';
        return $content;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		$form->add(new HtmleditorFormField('text',$options['text'],array(
			'tab'=> 'basic',
			'title' => _TEXT,
			'width' => 12,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','text')
		)));				
		$form->add(new SelectFormField('textalign',$options['textalign'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_IMAGE_TEXTALIGN,
			'width' => 3,
            'options' => array(
                ['title' => _LEFT, 'value' => 'text-left'],
                ['title' => _RIGHT, 'value' => 'text-right'],
                ['title' => _CENTER, 'value' => 'text-center']
            )
		)));
		$form->add(new ImageuploadFormField('image',$options['image'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_IMAGE_IMAGE,
			'width' => 3
		)));
		$form->add(new ImageuploadFormField('bgimage',$options['bgimage'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_IMAGE_BGIMAGE,
			'width' => 3
		)));
		$form->add(new SelectFormField('bgimageposition',$options['bgimageposition'],array(
			'tab'=> 'basic',
			'width' => 3,
			'title' => SYSTEM_WIDGET_IMAGE_BGPOSITION,
			'options' => array(
				array('title' => _LEFT.' '._TOP, 'value' => 'left top'),
				array('title' => _RIGHT.' '._TOP, 'value' => 'right top'),
				array('title' => _LEFT.' '._CENTER, 'value' => 'left center'),
				array('title' => _RIGHT.' '._CENTER, 'value' => 'right center'),
				array('title' => _LEFT.' '._BOTTOM, 'value' => 'left bottom'),
				array('title' => _RIGHT.' '._BOTTOM, 'value' => 'right bottom'),
				array('title' => _CENTER.' '._CENTER, 'value' => 'center center')
			)
		)));
		$form->add(new ColorFormField('bgcolor',$options['bgcolor'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_IMAGE_BGCOLOR,
			'width' => 3
		)));
		
		$action = new ActionManager();
		$form->add($action->getFormField(URL.$service->get('Language')->getCode().'/api/system/widget/titleimage/',$defobj));
		
		return $form;
	}

	public function apiCall($op){
		global $service;
		$action = new ActionManager();
		$op = str_replace('action','',$op);
		switch($op) {
			case 'list' :
			case 'move' :
			case 'delete' :
			case 'duplicate': 
			case 'edit' :
			case 'save' : {
				$ret = $action->$op();
			}break;
			default : break;
		}
		return $ret;
	}	
}
?>