<?php
/**
 * Customer logos widget
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/customerlogos');

class CustomerlogosWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'customerlogos',
			'title' => SYSTEM_WIDGET_CUSTOMERLOGOS_TITLE,
			'description' => SYSTEM_WIDGET_CUSTOMERLOGOS_DESC,
			'icon' => 'image',
			'wireframe' => 'customerlogos',
			'saveoptions' => array('logos','usefilenameastitle','logoslink')
		));
	}
	
	public function render(){
		global $service;
        $opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->get('core/display/html/htmlimage');
		$service->get('Ressource')->get('core/display/html/htmllink');
		$service->get('Ressource')->getStyle('system','widget','customerlogos',$opt['stylesheet']);
        $content .= '<section class="widget customerlogos '.$opt['stylesheet'].' '.$this->data->getVar('widget_options')['widgetclasses'].'"><div class="ct">';
		$content .= '<h1'.(($opt['displaywidgettitle'])?'':' class="hidden"').'>'.$this->data->getVar('widget_title').'</h1>';
		$content .= '<div class="logos">';
		
		$image = new HtmlImage();
		
		$link = new HtmlLink();
		$link->attr('href',$opt['logoslink'])->attr('title',$this->data->getVar('widget_title'));
		$linkcode = array($link->open(),$link->close());
		
		$filespath = 'system/widget/customerlogos_'.$this->data->getVar('widget_objid');
		$files = $service->get('Ressource')->listDir(UPLOADROOT.$filespath,false);
		foreach ($files as $f) {
			if ($f != 'thumb') {
				$content .= $linkcode[0];
				$title = $this->data->getVar('widget_title');
				if ($opt['usefilenameastitle']) $title = htmlentities(substr($f,0,strrpos($f,'.')));
				$content .= $image->attr('src',$filespath.'/'.$f)->attr('alt',$title)->attr('title',$title)->open();
				$content .= $linkcode[1];
			}
		}
        $content .= '</div></div></section>';
        return $content;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		
        $files = $service->get('Ressource')->listDir(UPLOADROOT.'system/widget/customerlogos_'.$defobj->getVar('widget_objid'),false);
		$key = array_search('thumb',$files);
		if ($key) unset($files[$key]);
		$form->add(new TextFormField('logoslink',$options['logoslink'],array(
			'title'=>SYSTEM_WIDGET_CUSTOMERLOGOS_LINK,
			'width' => 6,
			'tab'=>'basic',
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','logoslink')			
		)));
		$form->add(new YesnoFormField('usefilenameastitle',$options['usefilenameastitle'],array(
			'title'=>SYSTEM_WIDGET_CUSTOMERLOGOS_USEFNAMEASTITLE,
			'width' => 6,
			'tab'=>'basic'
		)));
		$form->add(new DragndropuploaderFormField('logos','',array(
			'title'=>SYSTEM_WIDGET_CUSTOMERLOGOS_DRAGNDROP,
			'files' => $files,
			'folder' => 'system/widget/customerlogos_'.$defobj->getVar('widget_objid'),
			'removeurl' => $service->get('Language')->getCode().'/api/system/widget/customerlogos/removefile',
			'tab'=>'basic'
		)));
		return $form;
	}
    
	public function apiCall($op){
		global $service;
		$url = $service->get('Url')->get();
		$request = $service->get('Request')->get();		
		$ret = array();
		switch($op){
			case 'removefile' : {
				if (isset($request['file']) && strstr($request['file'],'../') === false && substr($request['file'],0,strlen(UPLOADURL)) == UPLOADURL) {
					$file = str_replace(UPLOADURL,UPLOADROOT,$request['file']);
					if (file_exists($file)) {
						unlink($file);
						$ret = array('success' => true);
					}
				}
				if (!isset($ret['success'])) $ret['success'] = false;
			}break;
		}
		return $ret;
	}

	public function hasAccess($op){
		global $service;
		if ($service->get('User')->isAdmin()) return true;
		return true;
	}	    
}
?>