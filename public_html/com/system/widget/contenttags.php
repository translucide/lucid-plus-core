<?php
/**
 * Content Tags widget
 *
 * Sept 16, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/model/tag');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/contenttags');

class ContenttagsWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'contenttags',
			'title' => SYSTEM_WIDGET_CONTENTTAGS,
			'description' => SYSTEM_WIDGET_CONTENTTAGS_DSC,
			'icon' => 'calendar',
			'wireframe' => 'contenttags',
			'saveoptions' => array('nbitems')
		));
	}
	
	public function render(){
		global $service;
        $service->get('Ressource')->get('core/display/html/htmlimage');
		$service->get('Ressource')->get('core/display/html/htmllink');
		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','contenttags',($opt['stylesheet'])?$opt['stylesheet']:'default');
		$classes = array('widget','contenttags',
            ($opt['stylesheet'])?$opt['stylesheet']:'default',
            'section_'.$opt['section'],$opt['widgetclasses']);
        $content = '<section class="'.implode(' ',$classes).'"><div class="ct">';
		$content .= '<h1'.(($opt['displaywidgettitle'])?'':' class="hidden"').'>'.$this->data->getVar('widget_title').'</h1><ul>';
        $tags = $service->get('Content')->get()->getVar('content_tags');
        $tags = explode(',',substr($tags,1,-1));
		$link = new HtmlLink();
		$count = 0;
        foreach ($tags as $k => $v) {
            $url = URL.$service->get('Language')->getCode().'/tag/'.$service->get('Url')->toUrl($v);
            $link->attr('href',$url)->attr('title',$v)->attr('class','tag');
            $content .= '<li>';
            $content .= $link->open().ucfirst($v).$link->close();
            $content .= '</li>';
			$count++;
        }
        $content .= '</ul></div></section>';
        return $content;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		$sl = new SectionList();
		$form->add(new TextFormField('nbitems',$options['nbitems'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_CONTENTTAGS_NBITEMS,
            'width' => 4
		)));
		return $form;
	}
	
	public function hasAccess($op){
		global $service;		
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}		
}
?>