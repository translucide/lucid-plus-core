<?php
/**
 * Tags List widget
 *
 * Sept 16, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/model/tag');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/tagslist');

class TagslistWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'tagslist',
			'title' => SYSTEM_WIDGET_TAGSLIST,
			'description' => SYSTEM_WIDGET_TAGSLIST_DSC,
			'icon' => 'calendar',
			'wireframe' => 'tagslist',
			'saveoptions' => array('nbitems')
		));
	}

	public function render(){
		global $service;
        $service->get('Ressource')->get('core/display/html/htmlimage');
		$service->get('Ressource')->get('core/display/html/htmllink');
		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','tagslist',($opt['stylesheet'])?$opt['stylesheet']:'default');
		$classes = array('widget','tagslist',
            ($opt['stylesheet'])?$opt['stylesheet']:'default',
            'section_'.$opt['section'],$opt['widgetclasses']);
        $content = '<section class="'.implode(' ',$classes).'"><div class="ct">';
		$content .= '<h1'.(($opt['displaywidgettitle'])?'':' class="hidden"').'>'.$this->data->getVar('widget_title').'</h1><ul>';
		$objs = $service->get('Db')->query('
			SELECT `tag_url`, `tag_title`, count(*) as `count`
			FROM `tag`
			LEFT JOIN `content` ON `content_tags` LIKE CONCAT(\'%,\',`tag`.`tag_title`,\',%\')
			WHERE `content_tags` LIKE CONCAT(\'%,\',tag.tag_title,\',%\')
			GROUP BY `tag_title`
			ORDER BY `count` DESC
		');
		$link = new HtmlLink();
		$count = 0;
        foreach ($objs as $k => $v) {
            if ($count == $opt['nbitems'] && intval($opt['nbitems']) > 0) break;
            $url = URL.$service->get('Language')->getCode().'/tag/'.$service->get('Url')->toUrl($v['tag_title']);
            $link->attr('href',$url)->attr('title',$v['tag_title'])->attr('class','tag');
            $content .= '<li><span class="count">'.$v['count'].'</span><h2>';
            $content .= $link->open().$v['tag_title'].$link->close();
            $content .= '</h2></li>';
			$count++;
        }
		if ($count == 0){
			$content .= '<li class="notfound">'.SYSTEM_WIDGET_TAGSLIST_NOCONTENTAVAILABLE.'</li>';
		}
        $content .= '</ul></div></section>';
        return $content;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		$sl = new SectionList();
		$form->add(new TextFormField('nbitems',$options['nbitems'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_TAGSLIST_NBITEMS,
            'width' => 4
		)));
		return $form;
	}

	public function hasAccess($op){
		global $service;
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}
}
?>
