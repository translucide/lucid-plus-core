<?php
/**
 * Change language widget
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/changelanguage');

class ChangelanguageWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'changelanguage',
			'title' => CHANGELANGUAGE_WIDGET_TITLE,
			'description' => CHANGELANGUAGE_WIDGET_DESC,
			'wireframe' => 'changelanguage',
			'icon' => 'file',
			'saveoptions' => array('widgetdisplaytype','displaywidgettitle','displayabbr')
		));
	}
	
	public function render(){
		global $service;
		$url = $service->get('Url')->get();
		$url = $url['path'];
		
		$src = '';
		$lang = $service->get('Language')->getId();
		$langs = $service->get('Language')->getCodes();
		$alllangs = $service->get('Language')->getAllLanguages();
		$obj = $service->get('Route')->getController();
		
		$displaymode = $this->data->getVar('widget_options')['widgetdisplaytype'];
		foreach ($langs as $k => $v) {
			if (($lang != $k && $displaymode == 1) || $displaymode == 2) {
				$langTitle = $v;
				if ($this->data->getVar('widget_options')['displayabbr'] == false) {
					if (defined('LANGTITLE_'.strtoupper($v))) {
						$langTitle = constant('LANGTITLE_'.strtoupper($v));
					}
					else {
						foreach($alllangs as $k2 => $v2) {
							if ($k == $v2->getVar('language_id')) {
								$langTitle = $v2->getVar('language_title');
							}
						}						
					}
				}
				$ret = $obj->translate($k,0,$url);
				if ($ret == '') $ret = $v;
				$src .= '<a href="'.URL.$ret.'">'.$langTitle.'</a>';
			}
		}
		return $src;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		
		$opt = array();
		$i = 1;
		while(defined('CHANGELANGUAGE_WIDGET_TYPE_'.$i)) {
			$opt[] = array('title' => constant('CHANGELANGUAGE_WIDGET_TYPE_'.$i), 'value' => $i);
			$i++;
		}
		$form->add(new YesnoFormField('displayabbr',$options['displayabbr'],array(
			'tab'=> 'basic',
			'title' => CHANGELANGUAGE_WIDGET_DISPLAYABBR
		)));
		$form->add(new RadioFormField('widgetdisplaytype',$options['widgetdisplaytype'],array(
			'tab'=> 'basic',
			'title' => CHANGELANGUAGE_WIDGET_TYPE,
			'options' => $opt
		)));
		return $form;
	}
}
?>