<?php
/**
 * Title Image widget
 *
 * May 6, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/data');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/video');
$service->get('Ressource')->get('core/helper/actionmanager');

class VideoWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'video',
			'title' => SYSTEM_WIDGET_VIDEO_TITLE,
			'wireframe' => 'video',
			'icon' => 'video',
			'saveoptions' => array('text','action','video','stillimage','autoplay')
		));
	}

	public function render(){
		global $service;
		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','video',$opt['stylesheet']);
		$service->get('Ressource')->get('lib/jquery-background-video/1.1.0/jquery.background-video');

		$cls = uniqid('cls_');
		$bgi = $bgp = '';
		if (isset($opt['stillimage']) && $opt['stillimage'] != '') {
			$bgi = 'background-image: url("'.UPLOADURL.$opt['stillimage'].'");';
			$bgp = 'background-position:'.((isset($opt['bgimageposition']) && $opt['bgimageposition'] != '')?$opt['bgimageposition']:'center center').';';
		}
		$content = '<style type="text/css">.'.$cls.'{'.$bgi.$bgp.'}'.'</style>';
		$content .= '<section class="widget html video '.$opt['stylesheet'].' '.$cls.' '.$this->data->getVar('widget_options')['widgetclasses'].'">';
		$content .= '<div class="ct">';
        $content .= '<video class="videotag" loop '.(($opt['autoplay'] == 1)?'autoplay':'').' poster="'.UPLOADURL.$opt['stillimage'].'"><source src="'.$opt['video'].'" type="video/mp4"></video>';
        $content .= "<div class=\"placeholder\" style=\"min-height:80vh\">&nbsp;</div></div></section>";
		$service->get('Ressource')->addScript('
		$(\'.'.$cls.' .videotag\').bgVideo({
			fadeIn: 500,
			pauseAfter: 120,
			fadeOnPause: false,
			fadeOnEnd: true,
			showPausePlay: true,
			pausePlayXPos: \'right\',
			pausePlayYPos: \'top\',
			pausePlayXOffset: \'15px\',
			pausePlayYOffset: \'15px\'
		});
		');
		return $content;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');

		$form->add(new ImageuploadFormField('stillimage',$options['stillimage'],array(
			'tab'=> 'basic',
			'title' => _BGIMAGE,
			'width' => 3
		)));
		$form->add(new FileuploadFormField('video',$options['video'],array(
			'tab'=> 'basic',
			'title' => 'Video',
			'width' => 3
		)));
		$form->add(new YesnoFormField('autoplay',$options['autoplay]'],array(
			'tab'=> 'basic',
            'icontip' => SYSTEM_WIDGET_VIDEO_AUTOPLAY_TIP,
            'icon' => 'question-sign',
			'title' => SYSTEM_WIDGET_VIDEO_AUTOPLAY,
			'width' => 3
		)));
		return $form;
	}

	public function apiCall($op){
		global $service;
		$action = new ActionManager();
		$op = str_replace('action','',$op);
		switch($op) {
			case 'list' :
			case 'move' :
			case 'delete' :
			case 'duplicate':
			case 'edit' :
			case 'save' : {
				$ret = $action->$op();
			}break;
			default : break;
		}
		return $ret;
	}

	public function hasAccess($op){
		global $service;
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}

}
?>
