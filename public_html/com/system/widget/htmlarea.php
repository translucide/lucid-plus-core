<?php
/**
 * HTML Area widget
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/htmlarea');

class HtmlareaWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'htmlarea',
			'title' => SYSTEM_WIDGET_HTMLAREA_HTMLAREA,
			'description' => SYSTEM_WIDGET_HTMLAREA_HTMLAREA_DESC,
			'icon' => 'html',
			'wireframe' => 'htmlarea',
			'saveoptions' => array('htmlcontent','bgimage','bgimageposition')
		));
	}
	
	public function render(){
		global $service;
		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','htmlarea',($opt['stylesheet'])?$opt['stylesheet']:'default');
		$service->get('Ressource')->get('lib/ckeditor/4.5.9/plugins/codesnippet/lib/highlight/styles/xcode');
		$cls = uniqid('cls_');
		$tag = ($opt['widgetcontenttype'])?$opt['widgetcontenttype']:'section';
		$bgimg = '';
		if (isset($opt['bgimage']) && $opt['bgimage'] != '') {
			$bgimg = 'background-image:url(\''.UPLOADURL.$opt['bgimage'].'\');background-repeat:no-repeat;background-position:'.((isset($opt['bgimageposition']) && $opt['bgimageposition'] != '')?$opt['bgimageposition']:'left top').';background-size:cover;';
		}
		$content = '<style type="text/css">.'.$cls.'{'.$bgimg.'}</style>';
        $content .= '<'.$tag.' class="widget htmlarea '.$opt['stylesheet'].' '.$cls.' '.$opt['widgetclasses'].'">';
		$content .= '<div class="ct">';

        $title = '<span class="word">'.str_replace(' ','</span> <span class="word">',trim(strip_tags($this->data->getVar('widget_title')))).'</span>';            
        $title = str_replace(['<span class="word"></span>','<span class="word"> </span>'],'',$title);            
        $title = str_replace(['<span class="word"><div>','</div></span>'],['<div>','</div>'],$title);            
        
		$content .= '<h1'.(($opt['displaywidgettitle'])?'':' class="hidden"').'>'.$title.'</h1>';
		$content .= $opt['htmlcontent'];
		$content .= '</div>';
		$content .= "</".$tag.">";
        return $content;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		$form->add(new HtmleditorFormField('htmlcontent',$options['htmlcontent'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_HTMLAREA_CONTENT,
			'lang'=>$defaultlang['code'],
            'configfile' => 'config-admin',
			'translations' => $form->getTranslations($objs,'widget_options','htmlcontent')
		)));
		$form->add(new ImageuploadFormField('bgimage',$options['bgimage'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_HTMLAREA_BGIMAGE,
			'width' => 3
		)));
		$form->add(new SelectFormField('bgimageposition',$options['bgimageposition'],array(
			'tab'=> 'advanced',
			'width' => 4,
			'title' => SYSTEM_WIDGET_HTMLAREA_BGPOSITION,
			'options' => array(
				array('title' => _LEFT.' '._TOP, 'value' => 'left top'),
				array('title' => _RIGHT.' '._TOP, 'value' => 'right top'),
				array('title' => _LEFT.' '._CENTER, 'value' => 'left center'),
				array('title' => _RIGHT.' '._CENTER, 'value' => 'right center'),
				array('title' => _LEFT.' '._BOTTOM, 'value' => 'left bottom'),
				array('title' => _RIGHT.' '._BOTTOM, 'value' => 'right bottom'),
				array('title' => _CENTER.' '._CENTER, 'value' => 'center center')
			)
		)));
		return $form;
	}
}
?>