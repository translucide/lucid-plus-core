<?php
/**
 * Animated slide widget
 *
 * May 6, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/data');
$service->get('Ressource')->get('core/helper/actionmanager');
$service->get('Ressource')->get('core/display/effect/imageeffect');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/animatedslide');

class AnimatedslideWidget extends Widget{
    private $wn;

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
        $this->wn = 'animatedslide';
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => $this->wn,
			'title' => constant('SYSTEM_WIDGET_'.strtoupper($this->wn).'_TITLE'),
			'description' => constant('SYSTEM_WIDGET_'.strtoupper($this->wn).'_DESCRIPTION'),
			'wireframe' => $this->wn,
			'icon' => $this->wn,
			'saveoptions' => array('subtitle','text','action','bgimageposition','bgimage','image','bgeffect')
		));
	}

	public function render(){
		global $service;
		$service->get('Ressource')->get('lib/animate/3.6.1/animate.min');
		$service->get('Ressource')->get('lib/waypoints/3.1.1/jquery.waypoints.min');
		$service->get('Ressource')->get('lib/waypoints/3.1.1/shortcuts/inview.min');
		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','animatedslide',$opt['stylesheet']);
		$idcls = 'widget_animatedslide_'.$this->data->getVar('widget_objid');
		$bgi = $bgp = '';
		if (isset($opt['bgimage']) && $opt['bgimage'] != '') {
            $imgeffect = new ImageEffect($opt['bgimage']);
            $bgi = $imgeffect->bg($opt['bgeffect']);
			$bgp = 'background-position:'.((isset($opt['bgimageposition']) && $opt['bgimageposition'] != '')?$opt['bgimageposition']:'left top').';';
		}
		$content = '<section id="widget_animatedslide_'.$this->data->getVar('widget_objid').'" class="widget html animatedslide '.$opt['stylesheet'].' '.$cls.' '.$opt['widgetclasses'].'">';
		$content .= '<style type="text/css">#'.$idcls.'{'.$bgi.$bgp.'}'.'</style>';
		$content .= '<div class="ct">';
		$content .= '<h1'.(($opt['displaywidgettitle'])?'':' class="hidden"').'>'.$this->data->getVar('widget_title').'</h1>';
		if ($opt['subtitle'] != '') $content .= '<h2>'.$opt['subtitle'].'</h2>';
        if ($opt['image'] != '') $content .= '<div class="img"><img src="'.UPLOADURL.$opt['image'].'"></div>';
        if ($opt['text'] != '') $content .= '<div class="text">'.$opt['text'].'</div>';
		$action = new ActionManager();
		$content .= '<div class="actions">'.$action->render($this->data->getVar('widget_objid')).'</div>';
		$content .= '</div></section>';
		return $content;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');

		$form->add(new TextFormField('subtitle',$options['subtitle'],array(
			'tab'=> 'basic',
			'width' => 12,
			'title' => _SUBTITLE,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','subtitle')
		)));
		$form->add(new HtmleditorFormField('text',$options['text'],array(
			'tab'=> 'basic',
			'width' => 6,
			'title' => _TEXT,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','text')
		)));
		$form->add(new ImageuploadFormField('bgimage',$options['bgimage'],array(
			'tab'=> 'basic',
			'title' => _BGIMAGE,
			'width' => 3
		)));
		$form->add(new ImageuploadFormField('image',$options['image'],array(
			'tab'=> 'basic',
			'title' => _IMAGE,
			'width' => 3
		)));

        $imgeffect = new ImageEffect();
		$form->add(new SelectFormField('bgeffect',$options['bgeffect'],array(
			'tab'=> 'basic',
			'width' => 6,
			'title' => _BGEFFECT,
            'options' => $imgeffect->options()
		)));
		$form->add(new SelectFormField('bgimageposition',$options['bgimageposition'],array(
			'tab'=> 'basic',
			'width' => 6,
			'title' => _BGPOS,
			'options' => array(
				array('title' => _LEFT.' '._TOP, 'value' => 'left top'),
				array('title' => _RIGHT.' '._TOP, 'value' => 'right top'),
				array('title' => _LEFT.' '._CENTER, 'value' => 'left center'),
				array('title' => _RIGHT.' '._CENTER, 'value' => 'right center'),
				array('title' => _LEFT.' '._BOTTOM, 'value' => 'left bottom'),
				array('title' => _RIGHT.' '._BOTTOM, 'value' => 'right bottom'),
				array('title' => _CENTER.' '._CENTER, 'value' => 'center center'),
				array('title' => _CENTER.' '._TOP, 'value' => 'center top'),
				array('title' => _CENTER.' '._BOTTOM, 'value' => 'center bottom')
			)
		)));
		$action = new ActionManager();
		$form->add($action->getFormField(URL.$service->get('Language')->getCode().'/api/system/widget/animatedslide/',$defobj));
		return $form;
	}

	public function apiCall($op){
		global $service;
		$action = new ActionManager();
		$op = str_replace('action','',$op);
		switch($op) {
			case 'list' :
			case 'move' :
			case 'delete' :
			case 'duplicate':
			case 'edit' :
			case 'save' : {
				$ret = $action->$op();
			}break;
			default : break;
		}
		return $ret;
	}

	public function hasAccess($op){
		global $service;
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}

}
?>
