<?php
/**
 * Search widget
 *
 * Sept 16, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/list/section');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/search');

class SearchWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'search',
			'title' => SYSTEM_WIDGET_SEARCH_TITLE,
			'description' => SYSTEM_WIDGET_SEARCH_TITLE_DSC,
			'icon' => 'search',
			'wireframe' => 'search',
			'saveoptions' => array('section')
		));
	}
	
	public function render(){
		global $service;
        $service->get('Ressource')->get('core/display/html/htmlimage');
		$service->get('Ressource')->get('core/display/html/htmllink');
		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','search',($opt['stylesheet'])?$opt['stylesheet']:'default');
		$classes = array('widget','search',
            ($opt['stylesheet'])?$opt['stylesheet']:'default',
            'section_'.$opt['section'],$opt['widgetclasses']);
        $content = '<section class="'.implode(' ',$classes).'"><div class="ct">';
		$content .= '<h1'.(($opt['displaywidgettitle'])?'':' class="hidden"').'>'.$this->data->getVar('widget_title').'</h1>';
        $content .= '<form class="form-search" method="get" action="'.URL.$service->get('Language')->getCode().'/search">
            <input type="hidden" id="p" name="p" value="">
            <div class="input-group">
				<input type="hidden" name="section" value="'.implode(',',$opt['section']).'">
                <input type="text" class="form-control" placeholder="'.SEARCH_SEARCH.'" name="query" id="query" value="">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
            </form>';
        $content .= '</div></section>';
        return $content;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		$sl = new SectionList();
		$form->add(new SelectFormField('section',$options['section'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_SEARCH_SECTION,
            'width' => 4,
            'multiple' => 1,
            'options' =>$sl->asOptions()
		)));
		return $form;
	}
	
	public function hasAccess($op){
		global $service;		
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}		
}
?>