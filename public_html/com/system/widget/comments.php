<?php
/**
 * Comments widget
 *
 * June 20, 2016
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/model/comment');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/comments');

class CommentsWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'comments',
			'title' => SYSTEM_WIDGET_COMMENTS,
			'description' => SYSTEM_WIDGET_COMMENTS_DESC,
			'icon' => 'user',
			'wireframe' => 'comments',
			'saveoptions' => array('displaymode','allowattachments','commentsperuser','allowanonymous','pagesize','nbattachments','closed','postcommentbuttontext')
		));
	}

	public function render(){
		global $service;
		$request = $service->get('Request')->get();
		$opt = $this->data->getVar('widget_options');
		$inpagecontent = true;
		$service->get('Ressource')->getStyle('system','widget','comments',($opt['stylesheet'])?$opt['stylesheet']:'default');
		$cobjid = intval($service->get('Content')->get()->getVar('content_objid'));
		$parentid = intval($service->get('Content')->get()->getVar('content_parent'));
		$dm = ($opt['displaymode'] == 1)?'flat':'threaded';
		$attachments = ($opt['allowattachments'] == 1)?'allowattachments':'noattachments';
		$classes = array('widget','comments',
            ($opt['stylesheet'])?$opt['stylesheet']:'default',
            'section_'.$opt['section'],$opt['widgetclasses'],$dm,$attachments);

        $content = '<section class="'.implode(' ',$classes).'" data-widgetid="'.$this->data->getVar('widget_id').'" data-ps="'.$opt['pagesize'].'" data-cobjid="'.$cobjid.'" data-parent="'.$parentid.'"><div class="ct">';
		$content .= '<h1'.(($opt['displaywidgettitle'])?'':' class="hidden"').'>'.$this->data->getVar('widget_title').'</h1><ul>';
        $content .= '<div class="comments_list">';
		if ($inpagecontent) $content .= str_replace('{uploadurl}',UPLOADURL,$this->getPage($cobjid,intval($request['comments_page']),$opt['pagesize']));
		$content .= '</div>';
        $content .= '<div class="comments_nav"></div>';
        $content .= '<div id="comments_form" class="comments_form">';
		//if ($inpagecontent) $content .= $this->getForm(0,0,'newcommentform')->render();
		$content .= '</div>';
		$content .= '</div></section>';
		$service->get('Ressource')->get('lib/packages/form');
		$service->get('Ressource')->get('com/system/ressource/js/lucidcomments/0.1/lucidcomments');
        return $content;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		$sl = new SectionList();
		$form->add(new SelectFormField('displaymode',$options['displaymode'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_COMMENTS_DISPLAYMODE,
            'width' => 3,
            'options' => array(
                array('value' => '1', 'title' => SYSTEM_WIDGET_COMMENTS_DISPLAYMODE_FLAT),
                array('value' => '2', 'title' => SYSTEM_WIDGET_COMMENTS_DISPLAYMODE_THREADED),
            )
		)));
		$form->add(new YesnoFormField('allowanonymous',$options['allowanonymous'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_COMMENTS_ALLOWANONYMOUS,
            'width' => 3
		)));
		$form->add(new YesnoFormField('allowattachments',$options['allowattachments'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_COMMENTS_ALLOWATTACHMENTS,
            'width' => 3
		)));
		$form->add(new SelectFormField('nbattachments',$options['nbattachments'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_COMMENTS_NBATTACHMENTS,
            'width' => 3,
			'options' => array(
				array('value' => 1, 'title'=>'1'),
				array('value' => 2, 'title'=>'2'),
				array('value' => 3, 'title'=>'3'),
				array('value' => 4, 'title'=>'4')
			)
		)));
		$form->add(new YesnoFormField('closed',$options['closed'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_COMMENTS_CLOSECOMMENTS,
            'width' => 3
		)));
		$form->add(new TextFormField('pagesize',$options['pagesize'],array(
			'tab'=> 'basic',
			'title' => _PERPAGE,
            'width' => 3
		)));
		$form->add(new TextFormField('commentsperuser',$options['commentsperuser'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_COMMENTS_COMMENTSPERUSER,
			'help' => SYSTEM_WIDGET_COMMENTS_COMMENTSPERUSERHELP,
            'width' => 3
		)));
		$form->add(new TextFormField('postcommentbuttontext',$options['postcommentbuttontext'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_COMMENTS_POSTCOMMENTBUTTONTEXT,
            'width' => 3
		)));
		return $form;
	}

    public function apiCall($op) {
		global $service;
		$request = $service->get('Request')->get();
		$ret = array();
		$store = new CommentStore();
		$id = $request['id'];
		$this->loadWidgetInstance($request['wid']);

		$cobjid = (isset($request['cobjid']))?$request['cobjid']:$request['comment_contentobjid'];
		$page = intval($request['page']);
		$pagesize = intval($request['pagesize']);
		$parent = (isset($request['parent']))?$request['parent']:$request['comment_parent'];
		$displaymode = intval($request['displaymode']);
		switch($op){
			case 'getpage':{
				$ret = array(
					'success' => true,
					'response' => str_replace(
						'{uploadurl}',
						UPLOADURL,
						$this->getPage($cobjid,$page,$pagesize))
				);
			}break;
            case 'getform': {
				$form = '';

				//Check "comments per user" current value.
				$cpu = intval($this->data->getVar('widget_options')['commentsperuser']);
				if ($this->data->getVar('widget_options')['commentsperuser'] > 0) {
					$cpuCrit = new CriteriaCompo();
					$cpuCrit->add(new Criteria('comment_userid',$service->get('User')->getUid()));
					$cpuCrit->add(new Criteria('comment_contentobjid',$cobjid));
					$count = $store->count($cpuCrit);
				}else $count = 0;

				if ($this->data->getVar('widget_options')['closed'] == 0 &&
					(($cpu > 0 && ($count < $cpu || $id > 0)) || $cpu == 0)) {
					if ($id > 0) {
						$obj = $store->get($id);
						if (is_array($obj) && isset($obj[0])) $obj = $obj[0];
						$form = $this->getForm($cobjid,$parent,$request['formname'],$obj)->render();
					}
					else $form = $this->getForm($cobjid,$parent,$request['formname'])->render();
				}
                $ret = array(
                    'success' => true,
                    'response' => $form
                );
            }break;
			case 'delete' : {
				$deletechildposts = true;
				if ($id > 0) {
					$obj = $store->getById($id);
					if (count($obj)){
						$obj = $obj[0];
						if ($service->get('User')->isAdmin() || $service->get('User')->getUid() == $obj->getVar('comment_userid')) {
							$position = $obj->getVar('comment_position');
							$level = $obj->getVar('comment_level');
							$objid = $obj->getVar('comment_contentobjid');
							$store->updatePositions($obj->getVar('comment_contentobjid'),$position,'top');
							$store->delete(new Criteria('comment_id',$id,'='));

							if ($deletechildposts) {
								//Find next post at same level as current deleted comment.
								$crit = new CriteriaCompo();
								$crit->add(new Criteria('comment_position',$position,'>='));
								$crit->add(new Criteria('comment_level',$level,'<='));
								$crit->add(new Criteria('comment_contentobjid',$objid));
								$crit->setSort('comment_position','ASC');
								$crit->setLimit(1);
								$maxpos = $store->get($crit);
								$nb = 0;
								if (is_object($maxpos[0])) {
									$max = $maxpos[0]->getVar('comment_position');
									$crit = new CriteriaCompo();
									$crit->add(new Criteria('comment_position',$position,'>='));
									$crit->add(new Criteria('comment_position',$max,'<'));
									$crit->add(new Criteria('comment_level',$level,'>'));
									$crit->add(new Criteria('comment_contentobjid',$objid));
									$nb = $store->count($crit);
									$store->delete($crit);
								}else {
									//No next comment at same or lower level found...
									//Delete tail
									$crit = new CriteriaCompo();
									$crit->add(new Criteria('comment_position',$position,'>='));
									$crit->add(new Criteria('comment_level',$level,'>'));
									$crit->add(new Criteria('comment_contentobjid',$objid));
									$nb = $store->count($crit);
									$store->delete($crit);
								}
								if ($nb > 0) $store->updatePositions($obj->getVar('comment_contentobjid'),$position,'top',$nb);
							}
							return  $ret = array(
								'success' => true,
								'response' => ''
							);
						}
					}
				}
				$ret = array(
					'success' => false,
					'response' => '',
					'message' => _FORBIDDENREQUEST
				);
			}break;
            case 'save' : {
                $service->get('Ressource')->get('core/security/sanitizer');
                $sanitizer = new Sanitizer();
                if ($request['comment_id']) {
                    $obj = $store->getById($request['comment_id'])[0];
					if ($obj->getVar('comment_userid') != $service->get('User')->getUid() && !$service->get('User')->isAdmin()) {
						return $ret = array('success'=>false,'message' => _FORBIDDENREQUEST);
					}
                }else {
					$parentobj = $store->getParent($cobjid,$parent);
					if (is_array($parentobj)) $parentobj = $parentobj[0];
					$level = (is_object($parentobj))?$parentobj->getVar('comment_level')+1:0;

					if ($level == 0) {
						$crit = new CriteriaCompo();
						$crit->add(new Criteria('comment_contentobjid',$cobjid));
						$crit->add(new Criteria('comment_parent',0));
						$index = $store->getNext('comment_index',$crit);
					}else $index = 0;

					//1. Get new comment position from parent
					$position = 0;
					if (is_object($parentobj)) {
						$crit = new CriteriaCompo();
						$crit->add(new Criteria('comment_contentobjid',$cobjid));
						$crit->add(new Criteria('comment_level',$parentobj->getVar('comment_level'),'<='));
						$crit->add(new Criteria('comment_position',$parentobj->getVar('comment_position'),'>'));
						$crit->setSort('comment_position','ASC');
						$crit->setStart(0);
						$crit->setLimit(1);
						$posobj = $store->get($crit);
						if ($posobj) $position = $posobj[0]->getVar('comment_position');

					}
					if ($position == 0) $position = $store->getNext('comment_position',new Criteria('comment_contentobjid',$cobjid));

					//2. Update positions for all comments after this new one...
					$store->updatePositions($cobjid,$position);

                    $obj = $store->create();
                    $obj->setVar('comment_userid',$service->get('User')->getUid());
                    $obj->setVar('comment_parent',$parent);
                    $obj->setVar('comment_approved',1);
					$obj->setVar('comment_level',$level);
                    $obj->setVar('comment_contentobjid',$cobjid);
                    $obj->setVar('comment_index',$index);
					$obj->setVar('comment_position',$position);
                }
                if ($this->data->getVar('widget_options')['allowattachments']) {
					$attachments = '';
					for($i = 1; $i <= max(1,$this->data->getVar('widget_options')['nbattachments']); $i++) {
						if ($request['comment_attachments'.$i] != '' &&
							UPLOADURL != $request['comment_attachments'.$i] &&
							file_exists(UPLOADROOT.str_replace(UPLOADURL,'',$request['comment_attachments'.$i]))) {
							if ($attachments != '') $attachments .= "\n";
							$attachments .= str_replace(UPLOADURL,'',$request['comment_attachments'.$i]);
						}
					}
	                $obj->setVar('comment_attachments', $attachments);
				}
                //Get and set common variables to edit and add comment operations
                $obj->setVar('comment_content', str_replace('<p>&nbsp;</p>','',$sanitizer->removeForbiddenTags($request['comment_content'])));
                $success = $store->save($obj);
                if ($success) $ret = array('success'=> true, 'message' => 'Comment saved !');
                else $ret = array('success' => false, 'message' => 'Could not save new comment in DB.');
			}
            case 'getcomment': {
				if ($id > 0) {
					$obj = $store->get($id);
					if (is_array($obj) && isset($obj[0])) $obj = $obj[0];
					$this->getSrc($obj);
				}
				if ($obj) $ret = array(
					'success' => true,
					'response' => $this->getSrc($obj)
				);
				else $ret = array(
					'success' => false,
					'response' => '',
					'message' => SYSTEM_WIDGET_COMMENTS_NOTFOUND
				);
            }break;
        }
        return $ret;
    }
	public function hasAccess($op){
		global $service;
		switch ($op) {
			case 'getpage' :
			case 'getform' :
			case 'getcomment' : {
				return true;
			}break;
			case 'save' : {
				if ($service->get('User')->isAnonymous() == false) return true;
			}break;
			case 'delete': {
				if ($service->get('User')->isAnonymous() == false) return true;
			}
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}

	private function getPage($cobjid,$page,$pagesize){
		global $service;
		$service->get('Ressource')->get('core/display/template');
		$service->get('ressource')->get('core/display/pager');
		$store = new CommentStore();
		$comments = array();
		if ($cobjid > 0) {
			$comments = $store->getCommentsForContent($cobjid,$page,$pagesize);
			if ($page > 0 && count($comments) ==0) {
				$page = $page -1;
				$comments = $store->getCommentsForContent($cobjid,$page,$pagesize);
			}
			$comments = $this->formatForOutput($comments);
		}
		$crit = new Criteria('comment_contentobjid',$cobjid);
		$count = $store->count($crit);
		$pager = new Pager();
		$tpl = new Template('com/system/ressource/template/comments.tpl');
		$tpl->assign('count',count($comments));
		$tpl->assign('thumbsize',explode(',',strtolower($service->get('Setting')->get('tinypicturesize'))));
        $tpl->assign('closed',$this->data->getVar('widget_options')['closed']);
		$tpl->assign('comments',$comments);
		$tpl->assign('countstr',sprintf(SYSTEM_WIDGET_COMMENTS_COMMENTSFOUND,$count));
		$tpl->assign('pager', $pager->render($count,$pagesize,$page,URL.$service->get('Content')->get()->getVar('content_urlpath').'?comments_page={page}'));
		return $tpl->render();
	}

    private function getForm($cobjid,$parent,$name='commentform',$obj=false){
        global $service;

		//Set Comment form title
		$title = SYSTEM_WIDGET_COMMENTS_POSTNEWCOMMENT;
		if ($ojb < 1 && $parent > 0) {
			$ustore = new UserStore();
			$store = new CommentStore();
			$parentobj = $store->getParent($cobjid,$parent);
			if (count($parentobj)){
				$userobj = $ustore->get($parentobj[0]->getVar('comment_userid'))[0];
				$username = ((count($userobj)))?$userobj->getVar('user_name'):'comment';
			}
			else $username = 'comment';
			$title = sprintf(SYSTEM_WIDGET_COMMENTS_POSTREPLYCOMMENT,$username);
		}
		if ($obj > 0) {
			$title = SYSTEM_WIDGET_COMMENTS_POSTEDITCOMMENT;
		}
        $form = new Form(URL.$service->get('Language')->getCode().'/api/system/widget/comments/save',$name,$title,'POST');
        $form->setUseCase('ajax');
        $form->setVar('formtag',1);
        $form->setVar('layout','vertical');
        $form->add(new HiddenFormField('wid',$this->data->getVar('widget_id')));
        $form->add(new HiddenFormField('comment_id',($obj)?$obj->getVar('comment_id'):0));
        $form->add(new HiddenFormField('comment_contentobjid',$cobjid));
        $form->add(new HiddenFormField('comment_parent',$parent));
        $form->add(new HtmleditorFormField('comment_content',($obj)?$obj->getVar('comment_content'):'',array(
            'width' => 12,
            'title' => SYSTEM_WIDGET_COMMENTS_COMMENT,
            'autocomplete' => 'off',
            'autocapitalize' => 'off',
            'autocorrect' => 'off',
            'spellcheck' => 'false'
        )));
		if ($this->data->getVar('widget_options')['allowattachments']) {
			$ca = ($obj)?explode("\n",str_replace(UPLOADURL,'',$obj->getVar('comment_attachments'))):array();
			for ($i = 0; $i < $this->data->getVar('widget_options')['nbattachments']; $i++) {
				$form->add(new FileuploadFormField('comment_attachments'.($i+1),($obj && isset($ca[$i]))?$ca[$i]:'',array(
					'width' => 3,
					'title' => SYSTEM_WIDGET_COMMENTS_ATTACHMENT
				)));
			}
		}
		$btntext = $this->data->getVar('widget_options')['postcommentbuttontext'];
        $form->add(new SubmitFormField('submit','',array(
			'title' => (($btntext != '')?$btntext:SYSTEM_WIDGET_COMMENTS_POSTCOMMENT)
		)));
        return $form;
    }

	private function getSrc($obj){
		global $service;
		$service->get('Ressource')->get('core/display/template');
		if (is_object($obj)) $obj = $obj->getVars();

		$us = new UserStore();
		$u = $us->get(new Criteria('user_id',$obj['comment_userid']));
		$obj['user_name'] = $u[0]->getVar('user_name');

		$obj = $this->formatForOutput(array($obj));
        $tpl = new Template('com/system/ressource/template/comment.tpl');
		$tpl->assign('thumbsize',explode(',',strtolower($service->get('Setting')->get('tinypicturesize'))));
        $tpl->assign('closed',$this->data->getVar('widget_options')['closed']);
        $tpl->assign('comment',$obj[0]);
		return $tpl->render();
	}

	private function formatForOutput($comments){
		global $service;

		//1. Get all user avatars
		$service->get('Ressource')->get('core/model/profile');
		if (!is_array($comments)) $comments = array();
		$profileStore =  new ProfileStore();
		$userStore = new UserStore();
		$uids = array();
		foreach ($comments as $v) $uids[] = $v['comment_userid'];
		$avatars = $profileStore->getKey('avatar',array_unique($uids));
		foreach ($comments as $k => $v) {
			$avatar = '';
			foreach ($avatars as $a) {
				if ($a->getVar('profile_userid') == $v['comment_userid']) {
					$avatar = $a->getVar('profile_value');
				}
			}
			$comments[$k]['avatar'] = (file_exists(UPLOADROOT.$avatar)  && $avatar)?UPLOADURL.$avatar:'';
			if ($this->data->getVar('widget_options')['allowattachments']) {
				$files = explode("\n",str_replace(['{uploadurl}',UPLOADURL],'',$v['comment_attachments']));
				foreach ($files as $kf => $vf){
					if ($vf != '') {
						$isimage = false;
						if (stripos($vf,'.png') || stripos($vf,'.jpg') || stripos($vf,'.jpeg') || stripos($vf,'.gif')) {
							$isimage = true;
						}
						$vf = str_replace(UPLOADURL,'',$vf);

						$name = str_replace('//','',substr($vf,strrpos($vf,'/')+1));
						$name = substr($name,0,strrpos($name,'.') - 4).substr($name,strrpos($name,'.'));
						$files[$kf] = array(
							'name' => $name,
							'isimage' => $isimage,
							'url' => UPLOADURL.$vf
						);
					}
				}
				$comments[$k]['comment_attachments'] = $files;
			}else {
				$comments[$k]['comment_attachments'] = '';
			}
			//Adjust timezones
			if ($service->get('User')->get('timezone') != '') {
				$v['comment_created'] = $service->get('Time')->toUserTime($v['comment_created']);
			}
			$comments[$k]['comment_created_text'] = date(SYSTEM_LOCALE_DATETIME_TEXT,$v['comment_created']);
			$comments[$k]['comment_created_machine'] = date(SYSTEM_LOCALE_DATETIME,$v['comment_created']);
		}
		return $comments;
	}

	private function loadWidgetInstance($wid){
		//Load Widget instance associated with apiCall
		$wid = intval($wid);
		if ($wid > 0) {
			$widgetStore = new WidgetStore();
			$this->data = $widgetStore->getById($wid)[0];
		}
		if (!is_object($this->data)) {
			header('Content-Type: application/json');
			echo json_encode(array(
				'success' => false,
				'message' => 'Widget instance #'.$wid.' not found.'
			));
			die;
		}
	}
}
?>
