<?php
/**
 * Sidebysidetext widget
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget/listwidget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/sidebysidetext');

class SidebysidetextWidget extends ListWidget{
	private $wn;

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->wn = 'sidebysidetext';
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => $this->wn,
			'title' => SYSTEM_WIDGET_SIDEBYSIDETEXT_TITLE,
			'wireframe' => $this->wn,
			'icon' => $this->wn,
			'saveoptions' => $this->getOptionsNames(),
			'copyoptions' => [
				'data' => [
					'data' => [
						'select' => [
							'data_type' => '{parentname}item',
							'data_parent' => '{parentobjid}'
						],
						'update' => [
							'data_parent' => '{parentobjid}'
						]
					]
				]
			]
		));
	}

	public function render(){
		global $service;
        $this->options['listwidget']['itemsectionclasses'] = function($v) {
            if ($v->getVar('data_data')['highlighted']) return 'highlighted';
            return '';
        };
		$service->get('Ressource')->get('core/display/html/htmllink');
		$service->get('Ressource')->get('core/display/html/htmlimage');
		$id = $this->data->getVar('widget_objid');
		$service->get('Ressource')->addScript('
			$("section#widget_sidebysidetext_'.$id.' section h1").matchHeight();
			$("section#widget_sidebysidetext_'.$id.' section .body").matchHeight();
			$("section#widget_sidebysidetext_'.$id.' section > img").matchHeight();
			$("section#widget_sidebysidetext_'.$id.' section > a > img").matchHeight();
		');
		return parent::render();
	}

	protected function renderItem($v,$w){
		$link = new HtmlLink();
		$link->attr('href',$v->getVar('data_data')['link'])->attr('title',$v->getVar('data_title'));
		$linkcode = array($link->open(),$link->close());
		$image = new HtmlImage();
		$image->attr('src',$v->getVar('data_data')['image'])->attr('alt',$v->getVar('data_title'));
		if ($v->getVar('data_data')['image']) $content .= $linkcode[0].$image->open().$linkcode[1];
		if ($v->getVar('data_title')) $content .= "<h1".((!$v->getVar('data_data')['displayitemtitle'])?' class="hidden"':'').">".$linkcode[0].$v->getVar('data_title').$linkcode[1]."</h1>";
		if ($v->getVar('data_data')['text']) {
			$content .= '<div class="body">';
			if (substr($v->getVar('data_data')['text'],0,1) == '<') {
				$content .= $v->getVar('data_data')['text'];
			}
			else $content .= "<p>".str_replace("\n",'</p><p>',$v->getVar('data_data')['text'])."</p>";
			$content .= '</div>';
		}
		if ($v->getVar('data_data')['displaybottomlink']) {
			$content .= '<div class="actions">'.$linkcode[0]._DETAILS.$linkcode[1].'</div>';
		}
		return $content;
    }

	protected function editItem($data){
		global $service;
		$store = new DataStore();
		$defaultlang = $service->get('Language')->getDefault();
		$form = $data['form'];
		$form->setTitle(SYSTEM_WIDGET_SIDEBYSIDETEXT_TITLE);
		$defobj = $store->getDefaultObj($data['data']);
		$obj = $data['data'];
		$form->add(new HtmleditorFormField('text',$defobj->getVar('data_data')['text'],array(
			'title'=>_TEXT,
			'width' => 12,
			'length'=>255,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','text')
		)));
		$form->add(new TextFormField('link',$defobj->getVar('data_data')['link'],array(
			'title'=>_LINK,
			'width' => 3,
			'length'=>255,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','link')
		)));
		$form->add(new ImageuploadFormField('image',$defobj->getVar('data_data')['image'],array(
			'title' => _PICTURE,
			'width' => 3
		)));
		$form->add(new YesnoFormField('displayitemtitle',$defobj->getVar('data_data')['displayitemtitle'],array(
			'title' => SYSTEM_WIDGET_SIDEBYSIDETEXT_DISPLAYTITLE,
			'width' => 2
		)));
		$form->add(new YesnoFormField('displaybottomlink',$defobj->getVar('data_data')['displaybottomlink'],array(
			'title' => SYSTEM_WIDGET_SIDEBYSIDETEXT_DISPLAYBOTTOMLINK,
			'width' => 2
		)));
		$form->add(new YesnoFormField('highlighted',$defobj->getVar('data_data')['highlighted'],array(
			'title' => SYSTEM_WIDGET_SIDEBYSIDETEXT_HIGHLIGHTED,
			'width' => 2
		)));
		return $form;
	}

	protected function saveItem() {
		return array('text','image','displayitemtitle','link','displaybottomlink','highlighted');
	}
}
?>
