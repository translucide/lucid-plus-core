<?php
/**
 * Default site menu widget
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/display/menu');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/menumanager');

class SimplemenuWidget extends Widget{
	/**
	 * Returns information about this block type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'menu',
			'name' => 'simplemenu',
			'title' => SYSTEM_MENSYSTEM_SIMPLEMENUWIDGET_TITLE,
			'description' => SYSTEM_SIMPLEMENUWIDGET_DESCRIPTION,
			'icon' => 'menu',
			'saveoptions' => array('menutype','source','logo','addphone','disablemobilemenu'),
			'class' => 'navbar navbar-default',
			'attributes' => 'role="navigation"',
			'contenttype' => 'nav'
		));
	}
	
	public function render(){
		global $service;
		$info = $this->getInfo();
		//Add needed resources in case it is not loaded already.
        $opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','simplemenu',$opt['stylesheet']);
		$code = '<nav id="widget_menu_'.$this->data->getVar('widget_objid').'" '.
			'class="widget simplemenu menu_'.$this->data->getVar('widget_objid').' '.$opt['stylesheet'].'" '.
			'role="navigation">';
		if ($this->data->getVar('widget_options')['displaywidgettitle']) {
			$code .= '<h1>'.$this->data->getVar('widget_title').'</h1>';
		}
		$code .= '<ul>';
		$store = new MenuStore();
		$crit = new Criteria('menu_parent',$this->data->getVar('widget_options')['source']);
		$crit->setOrder('ASC');
		$crit->setSort('menu_position');
		$objs = $store->get($crit);

		foreach ($objs as $k => $v) {
			$menuitem = $service->get('Ressource')->getRessourceObject('menu',$v->getVar('menu_type'));
			if (!is_object($menuitem)) {
				$service->get('Log')->add('MenuWidget::render() : Menu item of type '.$v->getVar('menu_type').' could not be found !',__FILE__,__LINE__);
			}else {
				$menuitem->setData($v);
				$code .= $menuitem->render();				
			}
		}
		$code .= '</ul></nav>';
		return $code;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		
		$service->get('Ressource')->get('core/display/tree');
		$tree = new Tree();
		$form->add(new TreeFormField('source',$options['source'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_MENU_SOURCE,
			'dataurl' => URL.$service->get('Language')->getCode().'/api/system/controller/menumanager/getchildren',
			'contenttypes' => $tree->getContentTypes('menu'),
			'width' => 6
		)));
		$form->add(new RadioFormField('menutype',$options['menutype'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_MENU_TYPE,
			'options' => array(
				array('value' => 1, 'title'=>SYSTEM_MENU_TYPE_1),
				array('value' => 2, 'title'=>SYSTEM_MENU_TYPE_2)
			),
			'width' => 6
		)));
		return $form;
	}
}
?>