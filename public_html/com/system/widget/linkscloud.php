<?php
/**
 * Links Cloud widget
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget/listwidget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/linkscloud');

class LinkscloudWidget extends ListWidget{
	private $wn;
	
	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->wn = 'linkscloud';
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => $this->wn,
			'title' => SYSTEM_WIDGET_LINKSCLOUD_TITLE,
            'description' => SYSTEM_WIDGET_LINKSCLOUD_DSC,
			'wireframe' => $this->wn,
			'icon' => $this->wn,
			'saveoptions' => $this->getOptionsNames(),
			'copyoptions' => [
				'data' => [
					'data' => [
						'select' => [
							'data_type' => '{parentname}item',
							'data_parent' => '{parentobjid}'
						],
						'update' => [
							'data_parent' => '{parentobjid}'
						]
					]
				]
			]
		));
	}
	
	public function render(){
		global $service;
        $i = $this->getInfo();
		$w = $this->data;
		$o = $w->getVar('widget_options');
		$service->get('Ressource')->getStyle($i['component'],'widget',$i['name'],$o['stylesheet']);
		if ($o['stylesheet'] == '') $o['stylesheet'] = 'default';
		$m = $this->getItems();
		$content = "<section id='widget_".$i['name']."_".$w->getVar('widget_objid')."' class='widget html ".$i['name']." {$o['stylesheet']} ".$o['widgetclasses']."'>";
		$content .= "<div class='ct'>";
		$content .= "<h1".(($o['displaywidgettitle'])?"":" class='hidden'").">".$w->getVar('widget_title')."</h1>";
        if ($o['toptext']) $content .= '<p>'.$o['toptext'].'</p>';
        $content .= '<ul class="linkcloud">';
		foreach ($m as $k => $v){
            if ($v->getVar('data_data')['url'] != '') {
                $link = '<li><a href="'.$v->getVar('data_data')['url'].'" target="'.$v->getVar('data_data')['target'].'" title="{title}" alt="{title}">{title}</a></li>';
                $keywords = explode("\n",$v->getVar('data_data')['keywords']);
                foreach ($keywords as $kk => $vk) {
                    $content .=  str_replace('{title}',str_replace('"','&quot;',trim($vk)),$link);
                }
            }
		}
		$content .= "</ul>";
		if ($o['bottomtext']) $content .= '<p>'.$o['bottomtext'].'</p>';
		$action = new ActionManager();
		$actions = $action->render($this->data->getVar('widget_objid'));
		if ($actions != '') $content .= '<div class="actions">'.$actions.'</div>';		
		$content .= "</div></section>";
		return $content;
    }

	protected function editItem($data){
		global $service;
		$store = new DataStore();
		$defaultlang = $service->get('Language')->getDefault();		
		$form = $data['form'];
		$form->setTitle(SYSTEM_WIDGET_LINKSCLOUD_TITLE);
		$defobj = $store->getDefaultObj($data['data']);
		$obj = $data['data'];
		$form->add(new TextFormField('url',$defobj->getVar('data_data')['url'],array(
			'title'=>_LINK,
			'width' => 4,
			'length'=>255,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','url')
		)));
		$form->add(new SelectFormField('target',$options['target'],array(
			'title' => SYSTEM_WIDGET_LINKSCLOUD_TARGET,
			'width' => 6,
			'options' => array(
				['value' => '_self', 'title' => SYSTEM_WIDGET_LINKSCLOUD_TARGET_SELF],
				['value' => '_blank', 'title' => SYSTEM_WIDGET_LINKSCLOUD_TARGET_BLANK],
				['value' => '_parent', 'title' => SYSTEM_WIDGET_LINKSCLOUD_TARGET_PARENT],
				['value' => '_top', 'title' => SYSTEM_WIDGET_LINKSCLOUD_TARGET_TOP]
			),
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','target')
		)));        
		$form->add(new TextareaFormField('keywords',$defobj->getVar('data_data')['keywords'],array(
			'title'=>_KEYWORDS,
			'width' => 12,
			'length'=>255,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','keywords')
		)));
		return $form;
	}
	
	protected function saveItem() {
		return array('url','target','keywords');
	}

	public function hasAccess($op){
		global $service;		
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}	    
}
?>