<?php
/**
 * Hero unit
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/widgetzone');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/widgetzone');
$service->get('Ressource')->get('core/helper/actionmanager');

class WidgetzoneWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'widgetzone',
			'title' => SYSTEM_WIDGET_WIDGETZONE_WIDGETZONE,
			'description' => SYSTEM_WIDGET_WIDGETZONE_WIDGETZONE_DESC,
			'icon' => 'widgetzone',
			'wireframe' => 'widgetzone',
			'saveoptions' => array('zonename','itemsperline')
		));
	}
	
	public function render(){
		global $service;
		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','widgetzone',($opt['stylesheet'])?$opt['stylesheet']:'default');
		$classes = array(
			'widget',
			'widgetzone',
			($opt['stylesheet'])?$opt['stylesheet']:'default',
			$opt['widgetclasses']
        );
        $content .= '<section class="'.implode(' ',$classes).'"><div class="ct"><div class="row">';
        $content .= '[widgetzone:'.$opt['zonename'].'(itemsperline='.$opt['itemsperline'].')]';
        $content .= '</div></div></section>';
        return $content;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');				
		$form->add(new TextFormField('zonename',$options['zonename'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_WIDGETZONE_ZONENAME,
            'width' => 6
		)));
		$form->add(new SelectFormField('itemsperline',$options['itemsperline'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_WIDGETZONE_PERLINE,
            'width' => 6,
            'options' => array(['value'=>1,'title'=>1],['value'=>2,'title'=>2],['value'=>3,'title'=>3],['value'=>4,'title'=>4])
		)));
		return $form;
	}

    public function save() {
        global $service;
        $r = $service->get('Request')->get();
        $zs = new WidgetzoneStore();
        $obj = $zs->get(new Criteria('widgetzone_name',$r['zonename']));
        if (count($obj) < 1) {
            $obj = $zs->create();
        }else $obj = $obj[0];
        $obj->setVar('widgetzone_name',$r['zonename']);
        $obj->setVar('widgetzone_title',$r['widget_title']);
        $obj->setVar('widgetzone_provider','inline');
        $obj->setVar('widgetzone_options',array('itemsperline',$itemsperline));
        $zs->save($obj);
    }
    
	public function hasAccess($op){
		global $service;		
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}		
}
?>