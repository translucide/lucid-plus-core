<?php
/**
 * Link action
 *
 * May 6, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/linkwidget');

class LinkWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$p = 'link_';
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'action',
			'name' => 'link',
			'title' => SYSTEM_WIDGET_LINK_TITLE,
			'description' => SYSTEM_WIDGET_LINK_DESC,
			'icon' => 'link',
			'wireframe' => 'link',
			'saveoptions' => array($p.'url',$p.'target',$p.'icon',$p.'text')
		));
	}
	
	public function render(){
		global $service;
        $opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','link',$opt['stylesheet']);
        $extras = ' href="'.$opt['link_url'].'" target="'.$opt['link_target'].'" title="'.$this->data->getVar('widget_title').'"';
        $content = '<a class="widget action link '.$opt['stylesheet'].'" data-id="'.$this->data->getVar('widget_objid').'"'.$extras.'>';
		$content .= '<span class="'.$opt['link_icon'].'"></span>';
		$content .= $opt['link_text'];
        $content .= '</a>';
        return $content;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		$form->add(new TextFormField('link_text',$options['link_text'],array(
			'title' => _BUTTONTEXT,
			'width' => 6,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','link_text')            
		)));
		$form->add(new TextFormField('link_url',$options['link_url'],array(
			'title' => _URL,
			'width' => 6,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','link_url')            
		)));
		$form->add(new SelectFormField('link_target',$options['link_target'],array(
			'title' => SYSTEM_WIDGET_LINK_TARGET,
			'width' => 6,
			'options' => array(
				['value' => '_self', 'title' => SYSTEM_WIDGET_LINK_TARGET_SELF],
				['value' => '_blank', 'title' => SYSTEM_WIDGET_LINK_TARGET_BLANK],
				['value' => '_parent', 'title' => SYSTEM_WIDGET_LINK_TARGET_PARENT],
				['value' => '_top', 'title' => SYSTEM_WIDGET_LINK_TARGET_TOP]
			)
		)));	
		$form->add(new SelectFormField('link_icon',$options['link_icon'],array(
			'title' => _ICON,
			'width' => 6,
			'options' => array(
				['value' => 'glyphicon glyphicon-music', 'title' => 'Music'],
				['value' => 'glyphicon glyphicon-film', 'title' => 'Film'],
				['value' => 'glyphicon glyphicon-ok', 'title' => 'OK'],
				['value' => 'glyphicon glyphicon-file', 'title' => 'File'],
				['value' => 'glyphicon glyphicon-download', 'title' => 'Download'],
				['value' => 'glyphicon glyphicon-download-alt', 'title' => 'Download (Alternate)'],
				['value' => 'glyphicon glyphicon-upload', 'title' => 'Upload'],
				['value' => 'glyphicon glyphicon-play', 'title' => 'Play'],
				['value' => 'glyphicon glyphicon-share', 'title' => 'Share'],
				['value' => 'glyphicon glyphicon-plus-sign', 'title' => 'Plus sign'],
				['value' => 'glyphicon glyphicon-ok-sign', 'title' => 'OK sign'],
				['value' => 'glyphicon glyphicon-ok-circle', 'title' => 'OK circle'],
				['value' => 'glyphicon glyphicon-circle-arrow-up', 'title' => 'Circle arrow up'],
				['value' => 'glyphicon glyphicon-circle-arrow-down', 'title' => 'Circle arrow down'],
				['value' => 'glyphicon glyphicon-usd', 'title' => 'Dollar sign']
			)
		)));	
		return $form;
	}
	
	public function hasAccess($op){
		global $service;
		switch ($op) {
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}
}
?>