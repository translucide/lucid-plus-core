<?php
/**
 * Textslideup widget
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget/listwidget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/textslideup');

class TextslideupWidget extends ListWidget{
	private $wn;
	
	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->wn = 'textslideup';
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => $this->wn,
			'title' => SYSTEM_WIDGET_TEXTSLIDEUP_TITLE,
			'description' => SYSTEM_WIDGET_TEXTSLIDEUP_DISPLAYTITLE,
			'wireframe' => $this->wn,
			'icon' => $this->wn,
			'saveoptions' => $this->getOptionsNames(),
			'copyoptions' => [
				'data' => [
					'data' => [
						'select' => [
							'data_type' => '{parentname}item',
							'data_parent' => '{parentobjid}'
						],
						'update' => [
							'data_parent' => '{parentobjid}'
						]
					]
				]
			]
		));
	}
	
	public function render(){
		global $service;
		$id = 'widget_textslideup_'.$this->data->getVar('widget_objid');
        $service->get('Ressource')->get('lib/textslideup/0.1/textslideup');
		//$service->get('Ressource')->addScript('$("#'.$id.' section h1").matchHeight()');
		return parent::render();
	}
	
	protected function renderItem($v,$w){
		$content = '<div class="image">';
		if ($v->getVar('data_data')['image']) $content .= "<img src='".UPLOADURL.$v->getVar('data_data')['image']."'>";
		$content .= "</div>";
		$content .= "<h1>".$v->getVar('data_title')."</h1>";
		$content .= "<p>".$v->getVar('data_data')['text']."</p>";
		return $content;
    }

	protected function editItem($data){
		global $service;
		$store = new DataStore();
		$defaultlang = $service->get('Language')->getDefault();		
		$form = $data['form'];
		$form->setTitle(SYSTEM_WIDGET_TEXTSLIDEUP_TITLE);
		$defobj = $store->getDefaultObj($data['data']);
		$obj = $data['data'];
		$form->add(new HtmleditorFormField('text',$defobj->getVar('data_data')['text'],array(
			'title'=>_TEXT,
			'width' => 9,
			'length'=>255,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','text')
		)));
		$form->add(new ImageuploadFormField('image',$defobj->getVar('data_data')['image'],array(
			'title' => _PICTURE,
			'width' => 3
		)));
		return $form;
	}
	
	protected function saveItem() {
		return array('text','image');
	}
}
?>