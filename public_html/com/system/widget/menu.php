<?php
/**
 * Default site menu widget
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/display/menu');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/menumanager');

class MenuWidget extends Widget{
	/**
	 * Returns information about this block type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'menu',
			'name' => 'menu',
			'title' => SYSTEM_MENUWIDGET_TITLE,
			'description' => SYSTEM_MENUWIDGET_DESCRIPTION,
			'icon' => 'menu',
			'saveoptions' => array('menutype','source','logo','addphone','disablemobilemenu'),
			'class' => 'navbar navbar-default',
			'attributes' => 'role="navigation"',
			'contenttype' => 'nav'
		));
	}

	public function render(){
		global $service;
		$info = $this->getInfo();
		//Add needed resources in case it is not loaded already.
        $opt = $this->data->getVar('widget_options');
        $service->get('Ressource')->get('lib/lucidcore/0.1/lucidcore');
		$service->get('Ressource')->getStyle('system','widget','menu',$opt['stylesheet']);
		$service->get('Ressource')->addScript('$( document ).ready(function() {
			$("widget_menu_'.$this->data->getVar('widget_objid').' .col").matchHeight();
			if (Math.max(document.documentElement.clientWidth, window.innerWidth || 0) > 768) {
				$(".dropdown").on("show.bs.dropdown", function(e) {$(this).find(".dropdown-menu").first().stop(true, true).slideDown();});
				$(".dropdown").on("hide.bs.dropdown", function(e) {$(this).find(".dropdown-menu").first().stop(true, true).slideUp();});
			}
		});');
		$code = '<nav id="widget_menu_'.$this->data->getVar('widget_objid').'" '.
			'class="widget menu menu_'.$this->data->getVar('widget_objid').' '.$opt['stylesheet'].' '.$opt['variant'].' navbar navbar-default" '.
			'role="navigation">';

		if ($this->data->getVar('widget_options')['disablemobilemenu'] == false) {
			$code .= '<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-'.$this->data->getVar('widget_objid').'">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand mobile" href="'.URL.'">';
			if ($this->data->getVar('widget_options')['logo']) {
				$code .= '<img src="'.UPLOADURL.$this->data->getVar('widget_options')['logo'].'">';
			}
			$code .= '</a>
				</div>
				<div class="collapse navbar-collapse" id="bs-navbar-collapse-'.$this->data->getVar('widget_objid').'">';
		}else {
			$code .= '<div>';
		}
		$code .= '<ul class="nav navbar-nav">';
		$store = new MenuStore();
		$crit = new Criteria('menu_parent',$this->data->getVar('widget_options')['source']);
		$crit->setOrder('ASC');
		$crit->setSort('menu_position');
		$objs = $store->get($crit);
		$nbitems = count($objs);

		foreach ($objs as $k => $v) {
			$menuitem = $service->get('Ressource')->getRessourceObject('menu',$v->getVar('menu_type'));
			if (!is_object($menuitem)) {
				$service->get('Log')->add('MenuWidget::render() : Menu item of type '.$v->getVar('menu_type').' could not be found !',__FILE__,__LINE__);
			}else {
				$menuitem->setData($v);
				$code .= $menuitem->render([
					'last' => true
				]);
			}
		}
		$code .= '
					</ul>
				</div>';
		if ($this->data->getVar('widget_options')['disablemobilemenu'] == false) {
			$code .= '</div>';
		}
		$code .= '</nav>';
		return $code;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');

		$form->add(new ImageuploadFormField('logo',$options['logo'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_MENU_MOBILELOGO,
			'lang'=> $defaultlang['code'],
			'width' => 6
		)));
		$form->add(new YesnoFormField('addphone',$options['addphone'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_MENU_MOBILEPHONE,
			'help' => SYSTEM_MENU_MOBILEPHONE_DESC,
			'width' => 6
		)));
		$service->get('Ressource')->get('core/display/tree');
		$tree = new Tree();
		$form->add(new TreeFormField('source',$options['source'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_MENU_SOURCE,
			'dataurl' => URL.$service->get('Language')->getCode().'/api/system/controller/menumanager/getchildren',
			'contenttypes' => $tree->getContentTypes('menu'),
			'width' => 6
		)));
		$form->add(new RadioFormField('menutype',$options['menutype'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_MENU_TYPE,
			'options' => array(
				array('value' => 1, 'title'=>SYSTEM_MENU_TYPE_1),
				array('value' => 2, 'title'=>SYSTEM_MENU_TYPE_2)
			),
			'width' => 6
		)));
		$form->add(new YesnoFormField('disablemobilemenu',$options['disablemobilemenu'],array(
			'tab'=> 'advanced',
			'title' => SYSTEM_MENU_DISABLEMOBILEMENU,
			'width' => 4
		)));
		return $form;
	}
}
?>
