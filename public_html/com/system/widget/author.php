<?php
/**
 * Author unit
 *
 * Sept. 24, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/author');

class AuthorWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'author',
			'title' => SYSTEM_WIDGET_AUTHOR_TITLE,
			'description' => SYSTEM_WIDGET_AUTHOR_TITLE_DSC,
			'icon' => 'author',
			'wireframe' => 'author',
			'saveoptions' => array()
		));
	}
	
	public function render(){
		global $service;
        $ctobj = $service->get('Content')->get();
        $userStore = new UserStore();
        $obj = $userStore->get($ctobj->getVar('content_modifiedby'));

		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','author',($opt['stylesheet'])?$opt['stylesheet']:'default');
		$classes = array(
			'widget',
			'author',
			($opt['stylesheet'])?$opt['stylesheet']:'default',
			$opt['widgetclasses']
        );
        $content .= '<header class="'.implode(' ',$classes).'"><div class="ct"><p>';
        $content .= sprintf(SYSTEM_WIDGET_AUTHOR_POSTEDON,$obj[0]->getVar('user_name'),date("Y:m:d H:i:s",$ctobj->getVar('content_created')));
        $content .= '</p></div></header>';
        return $content;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');				
		return $form;
	}

	public function hasAccess($op){
		global $service;		
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}		
}
?>