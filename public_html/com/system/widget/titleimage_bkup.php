<?php
/**
 * Title Image widget
 *
 * May 6, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/data');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/titleimage');
$service->get('Ressource')->get('core/helper/actionmanager');

class TitleimageWidget2 extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'titleimage',
			'title' => SYSTEM_WIDGET_TITLEIMAGE_TITLE,
			'wireframe' => 'titleimage',
			'icon' => 'titleimage',
			'saveoptions' => array('text','action','bgimageposition','bgimage','textalign')
		));
	}
	
	public function render(){
		global $service;
		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','titleimage',$opt['stylesheet']);
		
		$cls = uniqid('cls_');
		$bgi = $bgp = '';
		if (isset($opt['bgimage']) && $opt['bgimage'] != '') {
			$bgi = 'background-image: url("'.UPLOADURL.$opt['bgimage'].'");';
			$bgp = 'background-position:'.((isset($opt['bgimageposition']) && $opt['bgimageposition'] != '')?$opt['bgimageposition']:'left top').';';
		}

		switch($opt['stylesheet']) {
			case 'dotted-background-overlay' : {
				$bgi = 'background-image: url("'.URL.'com/system/asset/widget/titleimage/blackdot-pattern.png"), '.
					str_replace('background-image: ','',$bgi);
			} break;
			case 'dark-background-overlay' : {
				$bgi = 'background-image: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), '.
					str_replace('background-image: ','',$bgi);
			} break;
		}

		$content = '<style type="text/css">.'.$cls.'{'.$bgi.$bgp.'}'.'</style>';
		$content .= '<section class="widget html titleimage '.$opt['stylesheet'].' '.$cls.' '.$this->data->getVar('widget_options')['widgetclasses'].' '.$this->data->getVar('widget_options')['textalign'].'">';
		$content .= '<div class="ct">';
		$content .= '<h1'.(($opt['displaywidgettitle'])?'':' class="hidden"').'>'.$this->data->getVar('widget_title').'</h1>';
		if (trim($opt['text'])) {
			if (substr($opt['text'],0,1) == '<') $content .= $opt['text'];
			else '<p>'.$opt['text'].'</p>';
		}
		
		$action = new ActionManager();
		$content .= '<div class="actions">'.$action->render($this->data->getVar('widget_objid')).'</div>';
		$content .= '</div></section>';
		return $content;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
				
		$form->add(new HtmleditorFormField('text',$options['text'],array(
			'tab'=> 'basic',
			'width' => 6,
			'title' => _TEXT,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','text')
		)));
		$form->add(new ImageuploadFormField('bgimage',$options['bgimage'],array(
			'tab'=> 'basic',
			'title' => _BGIMAGE,
			'width' => 3
		)));
		$form->add(new SelectFormField('bgimageposition',$options['bgimageposition'],array(
			'tab'=> 'basic',
			'width' => 3,
			'title' => _BGPOS,
			'options' => array(
				array('title' => _LEFT.' '._TOP, 'value' => 'left top'),
				array('title' => _RIGHT.' '._TOP, 'value' => 'right top'),
				array('title' => _LEFT.' '._CENTER, 'value' => 'left center'),
				array('title' => _RIGHT.' '._CENTER, 'value' => 'right center'),
				array('title' => _LEFT.' '._BOTTOM, 'value' => 'left bottom'),
				array('title' => _RIGHT.' '._BOTTOM, 'value' => 'right bottom'),
				array('title' => _CENTER.' '._CENTER, 'value' => 'center center'),
				array('title' => _CENTER.' '._TOP, 'value' => 'center top'),
				array('title' => _CENTER.' '._BOTTOM, 'value' => 'center bottom')
			)
		)));
		$form->add(new SelectFormField('textalign',$options['textalign'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_TITLEIMAGE_TEXTALIGN,
			'width' => 3,
            'options' => array(
                ['title' => _LEFT, 'value' => 'text-left'],
                ['title' => _RIGHT, 'value' => 'text-right'],
                ['title' => _CENTER, 'value' => 'text-center']
            )
		)));		
		$action = new ActionManager();
		$form->add($action->getFormField(URL.$service->get('Language')->getCode().'/api/system/widget/titleimage/',$defobj));
		return $form;
	}

	public function apiCall($op){
		global $service;
		$action = new ActionManager();
		$op = str_replace('action','',$op);
		switch($op) {
			case 'list' :
			case 'move' :
			case 'delete' :
			case 'duplicate': 
			case 'edit' :
			case 'save' : {
				$ret = $action->$op();
			}break;
			default : break;
		}
		return $ret;
	}
	
	public function hasAccess($op){
		global $service;		
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}	
	
}
?>