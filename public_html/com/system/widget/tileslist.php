<?php
/**
 * Title Image widget
 *
 * May 6, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/data');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/tileslist');
$service->get('Ressource')->get('core/helper/actionmanager');
$service->get('Ressource')->get('core/display/effect/imageeffect');

class TileslistWidget extends Widget{
    private $wn;

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
        $this->wn = 'tileslist';
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => $this->wn,
			'title' => constant('SYSTEM_WIDGET_'.strtoupper($this->wn).'_TITLE'),
			'description' => constant('SYSTEM_WIDGET_'.strtoupper($this->wn).'_DESC'),
			'wireframe' => $this->wn,
			'icon' => $this->wn,
			'saveoptions' => array('subtitle','intro','listtitle','listtitles','listitems','listicons','action','bgimageposition','bgimage','bgeffect','outro'),
			'copyoptions' => [
				'data' => [
					'data' => [
						'select' => [
							'data_type' => '{parentname}item',
							'data_parent' => '{parentobjid}'
						],
						'update' => [
							'data_parent' => '{parentobjid}'
						]
					]
				]
			]
		));
	}

	public function render(){
		global $service;
		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->get('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
		$service->get('Ressource')->getStyle('system','widget','tileslist',$opt['stylesheet']);

		$cls = uniqid('cls_');
		$bgi = $bgp = '';
		if (isset($opt['bgimage']) && $opt['bgimage'] != '') {
            $imgeffect = new ImageEffect($opt['bgimage']);
            $bgi = $imgeffect->bg($opt['bgeffect']);
			$bgp = 'background-position:'.((isset($opt['bgimageposition']) && $opt['bgimageposition'] != '')?$opt['bgimageposition']:'left top').';';
		}
		$content = '<style type="text/css">.'.$cls.'{'.$bgi.$bgp.'}'.'</style>';
		$content .= '<section class="widget html tileslist '.$opt['stylesheet'].' '.$cls.' '.$this->data->getVar('widget_options')['class'].'">';
		$content .= '<div class="ct">';
		$content .= '<h1'.(($opt['displaywidgettitle'])?'':' class="hidden"').'>'.$this->data->getVar('widget_title').'</h1>';
		if ($opt['subtitle'] != '') $content .= '<h2>'.$opt['subtitle'].'</h2>';
		if ($opt['intro'] != '') $content .= '<p class="intro">'.str_replace("\n\n","</p><p>",$opt['intro']).'</p>';
		if ($opt['listtitle'] != '') $content .= '<p class="listitle"><strong>'.$opt['listtitle'].'</strong></p>';
		if ($opt['listitems'] != '') {
            $content .= '<ul>';
            if ($opt['listitems'] != '') $items = explode("\n",$opt['listitems']);
            else $items = array();
            if ($opt['listicons']) $icons = explode("\n",$opt['listicons']);
            else $icons = array();
            if ($opt['listtitles']) $titles = explode("\n",$opt['listtitles']);
            else $titles = array();
            $longest = 'items';
            $count = count($items);
            if ($count < count($titles)) {
                $longest = 'titles';
                $count = count($titles);
            }
            if ($count < count($icons)) {
                $longest = 'icons';
                $count = count($icons);
            }
            foreach (${$longest} as $k => $v) {
                $i = $t = $p = '';
                if (isset($icons[$k])) {
                    $prefix = substr($icons[$k],0,strpos($icons[$k],'-'));
                    $i = '<span class="'.(($prefix == 'fa' || $prefix == 'glyphicon')?$prefix:'').' '.$icons[$k].'"></span>';
                }
                if (isset($titles[$k])) {
                    $t = '<h2>'.$titles[$k].'</h2>';
                }
                if (isset($items[$k])) {
                    $p = '<p>'.$items[$k].'</p>';
                }
                $content .= "<li>".$i.$t.$p."</li>";
            }
            $content .= '</ul>';
		}

		$action = new ActionManager();
   		if ($opt['outro'] != '') $content .= '<p>'.str_replace("\n\n","</p><p>",$opt['outro']).'</p>';
		$content .= '<div class="actions">'.$action->render($this->data->getVar('widget_objid')).'</div>';
		$content .= '</div></section>';
		return $content;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		$form->add(new TextFormField('subtitle',$options['subtitle'],array(
			'tab'=> 'basic',
			'width' => 4,
			'title' => _SUBTITLE,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','subtitle')
		)));
		$form->add(new TextFormField('listtitle',$options['listtitle'],array(
			'tab'=> 'basic',
			'width' => 4,
			'title' => _LISTTITLE,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','listtitle')
		)));
		$form->add(new TextFormField('intro',$options['intro'],array(
			'tab'=> 'basic',
			'width' => 4,
			'title' => _INTRO,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','intro')
		)));
		$form->add(new TextareaFormField('listicons',$options['listicons'],array(
			'tab'=> 'basic',
			'width' => 4,
			'title' => _ICONS,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','listicons')
		)));
		$form->add(new TextareaFormField('listtitles',$options['listtitles'],array(
			'tab'=> 'basic',
			'width' => 4,
			'title' => _TITLES,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','listtitles')
		)));
		$form->add(new TextareaFormField('listitems',$options['listitems'],array(
			'tab'=> 'basic',
			'width' => 4,
			'title' => _ITEMS,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','listitems')
		)));
		$form->add(new IconFormField('icon','',array(
            'id' => 'icon',
			'tab'=> 'basic',
            'fa' => true,
			'width' => 9,
			'title' => _ICON,
            'onchange'=>"$('#icontext').val($(this).val())"
		)));
		$form->add(new TextFormField('icontext','',array(
			'tab'=> 'basic',
			'width' => 3,
			'title' => 'Copy icon name to icon list'
		)));
		$form->add(new ImageuploadFormField('bgimage',$options['bgimage'],array(
			'tab'=> 'basic',
			'title' => _BGIMAGE,
			'width' => 4
		)));
		$form->add(new SelectFormField('bgimageposition',$options['bgimageposition'],array(
			'tab'=> 'basic',
			'width' => 4,
			'title' => _BGPOS,
			'options' => array(
				array('title' => _LEFT.' '._TOP, 'value' => 'left top'),
				array('title' => _RIGHT.' '._TOP, 'value' => 'right top'),
				array('title' => _LEFT.' '._CENTER, 'value' => 'left center'),
				array('title' => _RIGHT.' '._CENTER, 'value' => 'right center'),
				array('title' => _LEFT.' '._BOTTOM, 'value' => 'left bottom'),
				array('title' => _RIGHT.' '._BOTTOM, 'value' => 'right bottom'),
				array('title' => _CENTER.' '._CENTER, 'value' => 'center center'),
				array('title' => _CENTER.' '._TOP, 'value' => 'center top'),
				array('title' => _CENTER.' '._BOTTOM, 'value' => 'center bottom')
			)
		)));
        $imgeffect = new ImageEffect();
		$form->add(new SelectFormField('bgeffect',$options['bgeffect'],array(
			'tab'=> 'basic',
			'width' => 4,
			'title' => _BGEFFECT,
            'options' => $imgeffect->options()
		)));

		$action = new ActionManager();
		$form->add($action->getFormField(URL.$service->get('Language')->getCode().'/api/system/widget/tileslist/',$defobj));

		$form->add(new TextareaFormField('outro',$options['outro'],array(
			'tab'=> 'basic',
			'width' => 12,
			'title' => _OUTRO,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','outro')
		)));

		return $form;
	}

	public function apiCall($op){
		global $service;
		$action = new ActionManager();
		$op = str_replace('action','',$op);
		switch($op) {
			case 'list' :
			case 'move' :
			case 'delete' :
			case 'duplicate':
			case 'edit' :
			case 'save' : {
				$ret = $action->$op();
			}break;
			default : break;
		}
		return $ret;
	}

	public function hasAccess($op){
		global $service;
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}

}
?>
