<?php
/**
 * Featured Content widget
 *
 * Sept 16, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/list/content');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/featuredcontent');

class FeaturedcontentWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'featuredcontent',
			'title' => SYSTEM_WIDGET_FEATUREDCONTENT_TITLE,
			'description' => SYSTEM_WIDGET_FEATUREDCONTENT_TITLE_DSC,
			'icon' => 'calendar',
			'wireframe' => 'featuredcontent',
			'saveoptions' => array('featureditems','displaydetailslink','displayimage','displayexerpt','imagesize')
		));
	}
	
	public function render(){
		global $service;
        $service->get('Ressource')->get('core/display/html/htmlimage');
		$service->get('Ressource')->get('core/display/html/htmllink');
		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','featuredcontent',($opt['stylesheet'])?$opt['stylesheet']:'default');
		$classes = array('widget','featuredcontent',
            ($opt['stylesheet'])?$opt['stylesheet']:'default',
            'section_'.$opt['section'],$opt['widgetclasses']);
        $content = '<section class="'.implode(' ',$classes).'"><div class="ct">';
		$content .= '<h1'.(($opt['displaywidgettitle'])?'':' class="hidden"').'>'.$this->data->getVar('widget_title').'</h1><ul>';
        $s = new ContentStore();
        $crit = new CriteriaCompo();
        $crit->add(new Criteria('content_objid','('.implode(',',$opt['featureditems']).')','IN'));
        $crit->add(new Criteria('content_draft',0));
        $crit = $s->addGroupCriteria($crit);
        $crit->setSort('content_position','DESC');
        $objs = $s->get($crit);
		$image = new HtmlImage();
		$link = new HtmlLink();
        $count = 0;
        foreach ($objs as $k => $v) {
            $link->attr('href',URL.$v->getVar('content_urlpath'))->attr('title',$v->getVar('content_title'))->attr('class','thumbnail');
            $image->attr('src',$v->getVar('content_thumb'))->attr('title',$v->getVar('content_title'));
            $content .= '<li>';
            if ($opt['displayimage'] && $v->getVar('content_thumb')) $content .= $link->open().$image->open().$link->close();
            $content .= '<h2>'.$link->attr('class','')->open().$v->getVar('content_title').$link->close().'</h2>';
            if ($opt['displayexerpt'] && $v->getVar('content_exerpt') != '') $content .= '<p>'.$link->attr('class','')->open().$v->getVar('content_exerpt').$link->close().'</p>';
            if ($opt['displaydetailslink']) $content .= '<div class="actions">'.$link->attr('class','details')->open()._DETAILS.$link->close().'</div>';
            $content .= '</li>';
			$count++;
        }
		if ($count == 0) {
			$content .= '<li class="notfound">'.SYSTEM_WIDGET_FEATUREDCONTENT_NOCONTENTAVAILABLE.'</li>';
		}
        $content .= '</ul></div></section>';
        return $content;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');
		$cl = new ContentList();
		$form->add(new SelectFormField('featureditems',$options['featureditems'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_FEATUREDCONTENT_FEATURED,
            'width' => 4,
            'multiple' => 1,
            'options' =>$cl->asOptions()
		)));
		$form->add(new SelectthumbsizeFormField('imagesize',$options['imagesize'],array(
			'tab'=> 'basic',
			'title' => _SIZE,
            'width' => 4
		)));
		$form->add(new YesnoFormField('displaydetailslink',$options['displaydetailslink'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_FEATUREDCONTENT_DISPLAYDETAILSLINK,
			'width' => 4
		)));
		$form->add(new YesnoFormField('displayimage',$options['displayimage'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_FEATUREDCONTENT_DISPLAYTHUMBNAILS,
			'width' => 4
		)));
		$form->add(new YesnoFormField('displayexerpt',$options['displayexerpt'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_FEATUREDCONTENT_DISPLAYEXERPT,
			'width' => 4
		)));
		return $form;
	}
	
	public function hasAccess($op){
		global $service;		
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}		
}
?>