<?php
/**
 * Team widget
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/team');
$service->get('Ressource')->get('core/helper/dbitemmanager');

class TeamWidget extends BlockWidget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'team',
			'title' => SYSTEM_WIDGET_TEAM_TITLE,
			'wireframe' => 'team',
			'icon' => 'team',
			'saveoptions' => array_merge($this->getOptionsNames(),array('fields'))
		));
	}

	public function render(){
		global $service;
		$service->get('Ressource')->getStyle('system','widget','team',$opt['stylesheet']);
		$service->get('Ressource')->get('lib/slidestoggle/0.1/slidestoggle');
		/*
		$service->get('Ressource')->addScript('
			$("section#widget_team_'.$this->data->getVar('widget_objid').' section > p").matchHeight();');
		*/
		$w = $this->data;
		$o = $w->getVar('widget_options');
		$dstore = new DataStore();
		$crit = new CriteriaCompo();
		$crit->add(new Criteria('data_parent',$this->data->getVar('widget_objid')));
		$crit->add(new Criteria('data_type','teamitem'));
		$m = $dstore->get($crit);
		$nav = "<nav class='remote-nav'><ul>";
		$content = "<section id='widget_team_".$this->data->getVar('widget_objid')."' class='widget html team {$opt['stylesheet']} lucidslidestoggle ".$o['class']."'><div class='ct'>
		<h1".(($o['displaywidgettitle'])?"":" class='hidden'").">".$w->getVar('widget_title')."</h1><div>";

		foreach ($m as $k => $v){
            $social = "";
            if (trim($v->getVar('data_data')['linkedin']) != '') {
                $social .= '<a href="'.$v->getVar('data_data')['linkedin'].'" class="social linkedin" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>';
            }
            if (trim($v->getVar('data_data')['facebook']) != '') {
                $social .= '<a href="'.$v->getVar('data_data')['facebook'].'" class="social facebook" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>';
            }
            if (trim($v->getVar('data_data')['email']) != '') {
                $social .= '<a href="mailto:'.$v->getVar('data_data')['email'].'" class="social email" target="_blank"><i class="fa fa-envelope-square" aria-hidden="true"></i></a>';
            }
			$content .= "
			<section id='teammember_{$k}' class='slide toggle'>
                <h1>".$v->getVar('data_data')['membername']."</h1>
				<h2>".$v->getVar('data_data')['memberjob']."</h2>
                <p class='pic'><img src='".UPLOADURL.$v->getVar('data_data')['memberpic']."'></p>
                <p class='bio'>".$v->getVar('data_data')['memberdesc']."</p>
                <p class='socialmedias'>".$social."</p>
			</section>";
			$nav .= "<li class='".(($k == 0)?'active':'')."'><a href='#teammember_{$k}' class='remote-toggle'>".$v->getVar('data_data')['membername']."</a></li>";
		}
		$nav .= "</ul></nav>";
		return $content.$nav."</div></div></section>";
    }

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');

		//Form creation code...
		$form->add(new ItemmanagerFormField('fields',$defobj->getVar('widget_options')['fields'],array(
			'title'=>SYSTEM_WIDGET_TEAM_MEMBERS,
			'tab'=>'basic',
			'parent' => $defobj->getVar('widget_objid'),
			'editlabel' => _EDIT,
			'deletelabel' => _DELETE,
			'duplicatelabel' => _DUPLICATE,
			'newlabel' => _NEW,
			'list' => URL.$service->get('Language')->getCode().'/api/system/widget/team/list',
			'edit' => URL.$service->get('Language')->getCode().'/api/system/widget/team/edit',
			'duplicate' => URL.$service->get('Language')->getCode().'/api/system/widget/team/duplicate',
			'save' => URL.$service->get('Language')->getCode().'/api/system/widget/team/save',
			'delete' => URL.$service->get('Language')->getCode().'/api/system/widget/team/delete',
			'move' => URL.$service->get('Language')->getCode().'/api/system/widget/team/move'
		)));
		return parent::edit($objs,$form);
	}

	public function apiCall($op){
		global $service;
		$store = new DataStore();
		$itemManager = new DBItemManager($store,'teamitem');

		switch($op) {
			case 'list' :
			case 'move' :
			case 'delete' : {
				$ret = $itemManager->$op();
			}break;
			case 'duplicate' :
			case 'edit' : {
				$defaultlang = $service->get('Language')->getDefault();
				$ret = '';
				if ($op == 'duplicate') $ret = $itemManager->duplicate();
				$data = $itemManager->getEditObjects($ret);
				$form = $data['form'];
				$form->setTitle(SYSTEM_WIDGET_TEAM_MEMBERDETAILS);
				$defobj = $store->getDefaultObj($data['data']);
				$obj = $data['data'];
				$form->add(new TextFormField('membername',$defobj->getVar('data_data')['membername'],array(
					'title'=>SYSTEM_WIDGET_TEAM_NAME,
					'width' => 6,
					'length'=>255
				)));
				$form->add(new TextFormField('memberjob',$defobj->getVar('data_data')['memberjob'],array(
					'title'=>SYSTEM_WIDGET_TEAM_JOBTITLE,
					'width' => 6,
					'length'=>255,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','memberjob')
				)));
				$form->add(new TextareaFormField('memberdesc',$defobj->getVar('data_data')['memberdesc'],array(
					'title'=>SYSTEM_WIDGET_TEAM_DESC,
					'width' => 6,
					'length'=>255,
					'lang'=>$defaultlang['code'],
					'translations' => $form->getTranslations($obj,'data_data','memberdesc')
				)));
				$form->add(new ImageuploadFormField('memberpic',$defobj->getVar('data_data')['memberpic'],array(
					'title' => SYSTEM_WIDGET_TEAM_PICTURE,
					'width' => 6
				)));
				$form->add(new TextFormField('linkedin',$defobj->getVar('data_data')['linkedin'],array(
					'title' => SYSTEM_WIDGET_TEAM_LINKEDIN_LINK,
					'width' => 6
				)));
				$form->add(new TextFormField('facebook',$defobj->getVar('data_data')['facebook'],array(
					'title' => SYSTEM_WIDGET_TEAM_FACEBOOK_LINK,
					'width' => 6
				)));
				$ret = $itemManager->edit($form);
			}break;
			case 'save' : {
				$ret = $itemManager->save(array('membername','memberjob','memberdesc','memberpic','facebook','linkedin'));
			}break;
			default : break;
		}
		return $ret;
	}

	public function hasAccess($op){
		global $service;
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}
}
?>
