<?php
/**
 * Title Image widget
 *
 * May 6, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/data');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/titleimage');
$service->get('Ressource')->get('core/helper/actionmanager');

class TitleimageWidget extends BlockWidget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->wn = 'titleimage';
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => $this->wn,
			'title' => SYSTEM_WIDGET_TITLEIMAGE_TITLE,
			'description' => SYSTEM_WIDGET_TITLEIMAGE_DISPLAYTITLE,
			'wireframe' => $this->wn,
			'icon' => $this->wn,
			'saveoptions' => array_merge($this->getOptionsNames(),array('text','action','bgimageposition','bgimage','textalign')),
			'copyoptions' => [
				'data' => [
					'data' => [
						'select' => [
							'data_type' => '{parentname}item',
							'data_parent' => '{parentobjid}'
						],
						'update' => [
							'data_parent' => '{parentobjid}'
						]
					]
				]
			]
		));
	}

	public function render(){
		global $service;
		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('system','widget','titleimage',$opt['stylesheet']);
        $content = '';
		if (trim($opt['text'])) {
			if (substr($opt['text'],0,1) == '<') $content .= $opt['text'];
			else '<p>'.$opt['text'].'</p>';
		}
		if (trim($opt['body'])) {
			if (substr($opt['body'],0,1) == '<') $content .= $opt['body'];
			else '<p>'.$opt['body'].'</p>';
		}
        return str_replace('{content}',$content,parent::render());
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');

        //Kepp compatibility with v1 and v2 widgets in current v3 widgets.
        if (isset($options['body']) && $options['body'] != '' && $options['body'] != '<p></p>') $options['text'] = $options['body'];
		$form->add(new HtmleditorFormField('text',(!is_null($options['text']))?$options['text']:'',array(
			'tab'=> 'basic',
			'width' => 12,
			'title' => _TEXT,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','text')
		)));
		return parent::edit($objs,$form);
	}
}
?>
