<?php
/**
 * Accordion widget
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget/listwidget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/accordion');

class AccordionWidget extends ListWidget{
	private $wn;
	
	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->wn = 'accordion';
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => $this->wn,
			'title' => SYSTEM_WIDGET_ACCORDION_TITLE,
            'description' => SYSTEM_WIDGET_ACCORDION_TITLE_DESC,
			'wireframe' => $this->wn,
			'icon' => $this->wn,
			'saveoptions' => $this->getOptionsNames(),
			'copyoptions' => [
				'data' => [
					'data' => [
						'select' => [
							'data_type' => '{parentname}item',
							'data_parent' => '{parentobjid}'
						],
						'update' => [
							'data_parent' => '{parentobjid}'
						]
					]
				]
			]
		));
	}
	
	public function render(){
		global $service;
		$gridSize = 12;
        $i = $this->getInfo();
		$w = $this->data;
		$o = $w->getVar('widget_options');
		$service->get('Ressource')->getStyle($i['component'],'widget',$i['name'],$o['stylesheet']);
		if ($o['stylesheet'] == '') $o['stylesheet'] = 'default';
		$m = $this->getItems();
		$content = "<section id='widget_".$i['name']."_".$w->getVar('widget_objid')."' class='widget html ".$i['name']." {$o['stylesheet']} ".$o['widgetclasses']."'>";
		$content .= "<div class='ct'>";
		$content .= "<h1".(($o['displaywidgettitle'])?"":" class='hidden'").">".$w->getVar('widget_title')."</h1>";
        $content .= '<div class="panel-group" id="widget_'.$i['name']."_".$w->getVar('widget_objid').'_accordion">';
		foreach ($m as $k => $v){
            $linkcode = array('','');
            $endlinkcode = '</a>';
            if ($v->getVar('data_data')['link'] != '') {
                $linkcode[0] = '<a href="'.$v->getVar('data_data')['link'].'" target="'.((strstr($v->getVar('data_data')['link'],URL))?"_self":"_blank").'">';
                $linkcode[1] = '</a>';
            }
            $content .= '<div class="panel panel-default"><div class="panel-heading"><h2 class="panel-title">';
            $content .= '<a data-toggle="collapse" data-parent="#widget_'.$i['name']."_".$w->getVar('widget_objid').'_accordion" href="#widget_'.$i['name']."_".$w->getVar('widget_objid').'_accordion_'.$k.'">';
            $content .= $v->getVar('data_title');
            $content .= '</a></h2></div>';
            $content .= '<div id="widget_'.$i['name']."_".$w->getVar('widget_objid').'_accordion_'.$k.'" class="panel-collapse collapse '.(($k == 0)? 'in' : '').'"><div class="panel-body">';
            $content .= $v->getVar('data_data')['text'];
            if (trim($v->getVar('data_data')['link']) != '') {
                $content .= '<div class="actions"><a href="'.$v->getVar('data_data')['link'].'">'._DETAILS.'</a></div>';
            }
            $content .= '</div></div></div>';
		}
		$content .= "</div>";
		if ($o['bottomtext']) $content .= '<p>'.$o['bottomtext'].'</p>';
		$action = new ActionManager();
		$actions = $action->render($this->data->getVar('widget_objid'));
		if ($actions != '') $content .= '<div class="actions">'.$actions.'</div>';		
		$content .= "</div></section>";
		return $content;
    }

	protected function editItem($data){
		global $service;
		$store = new DataStore();
		$defaultlang = $service->get('Language')->getDefault();		
		$form = $data['form'];
		$form->setTitle(SYSTEM_WIDGET_ACCORDION_TITLE);
		$defobj = $store->getDefaultObj($data['data']);
		$obj = $data['data'];
		$form->add(new HtmleditorFormField('text',$defobj->getVar('data_data')['text'],array(
			'title'=>_TEXT,
			'width' => 12,
			'length'=>255,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','text')
		)));
		$form->add(new TextFormField('link',$defobj->getVar('data_data')['link'],array(
			'title'=>_LINK,
			'width' => 4,
			'length'=>255,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','link')
		)));
		return $form;
	}
	
	protected function saveItem() {
		return array('text','link');
	}
}
?>