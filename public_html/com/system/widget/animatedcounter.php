<?php
/**
 * Animatedcounter widget
 *
 * July 10, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget/listwidget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/animatedcounter');

class AnimatedcounterWidget extends ListWidget{
	private $wn;
	
	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->wn = 'animatedcounter';
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => $this->wn,
			'title' => SYSTEM_WIDGET_ANIMATEDCOUNTER_TITLE,
			'description' => SYSTEM_WIDGET_ANIMATEDCOUNTER_DESCRIPTION,
			'wireframe' => $this->wn,
			'icon' => $this->wn,
			'saveoptions' => $this->getOptionsNames(),
			'copyoptions' => [
				'data' => [
					'data' => [
						'select' => [
							'data_type' => '{parentname}item',
							'data_parent' => '{parentobjid}'
						],
						'update' => [
							'data_parent' => '{parentobjid}'
						]
					]
				]
			]
		));
	}
	
	public function render(){
		global $service;
        $service->get('Ressource')->get('lib/countup/1.5.3/countUp.min');
		$service->get('Ressource')->get('lib/waypoints/3.1.1/jquery.waypoints.min');
		$service->get('Ressource')->get('lib/waypoints/3.1.1/shortcuts/inview.min');
		$service->get('Ressource')->addScript('
			$("section#widget_animatedcounter_'.$this->data->getVar('widget_objid').' section").matchHeight();
			$("section#widget_animatedcounter_'.$this->data->getVar('widget_objid').' section h1").matchHeight();
			$("section#widget_animatedcounter_'.$this->data->getVar('widget_objid').' section h2").matchHeight();
			$("section#widget_animatedcounter_'.$this->data->getVar('widget_objid').' section p").matchHeight();');
		return parent::render();
	}
	
	protected function renderItem($v,$w){
		global $service;
		$k = $v->getVar('data_objid');
		if ($v->getVar('data_title')) $content .= "<h1>{$v->getVar('data_title')}</h1>";
		if ($v->getVar('data_data')['subtitle']) $content .= "<h2>{$v->getVar('data_data')['subtitle']}</h2>";
		$content .= "<div class=\"counter\" id=\"counter_{$w->getVar('widget_objid')}_{$k}_counter\"></div>";
		$service->get('Ressource')->addScript("
			var animatedcounter_options_{$w->getVar('widget_objid')}_{$k} = {
			    useEasing : true,
			    useGrouping : true,
			    separator : ',',
			    decimal : '.',
			    prefix : '".addslashes($v->getVar('data_data')['prefix'])."',
			    suffix : '".addslashes($v->getVar('data_data')['suffix'])."'
			};
            setTimeout(function(){
                var animatedcounter_counter_{$w->getVar('widget_objid')}_{$k} = new CountUp('counter_{$w->getVar('widget_objid')}_{$k}_counter', ".
                    floatval($v->getVar('data_data')['start']).",".
                    floatval($v->getVar('data_data')['end']).",".
                    intval($v->getVar('data_data')['decimals']).",".
                    floatval($v->getVar('data_data')['duration']).",".
                    "animatedcounter_options_{$w->getVar('widget_objid')}_{$k}
                );            
                var inview = new Waypoint.Inview({
                    element: $('#counter_{$w->getVar('widget_objid')}_{$k}_counter')[0],
                    entered: function(direction) {
                        animatedcounter_counter_{$w->getVar('widget_objid')}_{$k}.start();
                    },
                    exited: function(direction) {
                        animatedcounter_counter_{$w->getVar('widget_objid')}_{$k}.reset();
                    }
                });
            },100);
		");
		if ($v->getVar('data_data')['description']) $content .= "<p class='description'>{$v->getVar('data_data')['description']}</p>";
		if ($v->getVar('data_data')['detailslink']) {
            $target = " target=\"_blank\"";
            if (strstr($v->getVar('data_data')['detailslink'],URL) !== false) {
                $target="";
            }
            $content .= "<div class='actions'><a href='".$v->getVar('data_data')['detailslink']."'".$target.">"._DETAILS."</a></div>";
        }
		return $content;
	}

	protected function editItem($data){
		global $service;
		$store = new DataStore();
		$defaultlang = $service->get('Language')->getDefault();		
		$form = $data['form'];
		$form->setTitle(SYSTEM_WIDGET_ANIMATEDCOUNTER_TITLE);
		$defobj = $store->getDefaultObj($data['data']);
		$obj = $data['data'];
		$form->add(new TextFormField('subtitle',$defobj->getVar('data_data')['subtitle'],array(
			'title' => SYSTEM_WIDGET_ANIMATEDCOUNTER_SUBTITLE,
			'width' => 12
		)));
		$form->add(new TextFormField('prefix',$defobj->getVar('data_data')['prefix'],array(
			'title' => SYSTEM_WIDGET_ANIMATEDCOUNTER_PREFIX,
			'width' => 2
		)));
		$form->add(new TextFormField('start',$defobj->getVar('data_data')['start'],array(
			'title' => SYSTEM_WIDGET_ANIMATEDCOUNTER_START,
			'width' => 2
		)));
		$form->add(new TextFormField('end',$defobj->getVar('data_data')['end'],array(
			'title' => SYSTEM_WIDGET_ANIMATEDCOUNTER_END,
			'width' => 2
		)));
		$form->add(new TextFormField('duration',$defobj->getVar('data_data')['duration'],array(
			'title' => SYSTEM_WIDGET_ANIMATEDCOUNTER_DURATION,
			'width' => 2
		)));
		$form->add(new TextFormField('decimals',$defobj->getVar('data_data')['decimals'],array(
			'title' => SYSTEM_WIDGET_ANIMATEDCOUNTER_DECIMALS,
			'width' => 2
		)));
		$form->add(new TextFormField('suffix',$defobj->getVar('data_data')['suffix'],array(
			'title' => SYSTEM_WIDGET_ANIMATEDCOUNTER_SUFFIX,
			'width' => 2
		)));
		$form->add(new TextareaFormField('description',$defobj->getVar('data_data')['description'],array(
			'title'=>_TEXT,
			'width' => 6,
			'length'=>255,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,'data_data','text')
		)));
		$form->add(new TextFormField('detailslink',$defobj->getVar('data_data')['detailslink'],array(
			'title' => SYSTEM_WIDGET_ANIMATEDCOUNTER_DETAILSLINK,
			'width' => 6
		)));        
		return $form;
	}
	
	protected function saveItem() {
		return array('start','end','duration','decimals','prefix','suffix','description','detailslink','subtitle');
	}
}
?>