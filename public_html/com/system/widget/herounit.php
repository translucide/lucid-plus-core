<?php
/**
 * Hero unit
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/herounit');
$service->get('Ressource')->get('core/helper/actionmanager');

class HerounitWidget extends BlockWidget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'block',
			'name' => 'herounit',
			'title' => SYSTEM_WIDGET_HEROUNIT_HEROUNIT,
			'description' => SYSTEM_WIDGET_HEROUNIT_HEROUNIT_DESC,
			'icon' => 'hero',
			'wireframe' => 'herounit',
			'saveoptions' => array_merge($this->getOptionsNames(),array('subtitle','content','contentwidth','image','tabletimage','mobileimage','imageposition','tabletimageposition','mobileimageposition','imagesize',/*'bgimage','bgimageposition',*/'contentorder'))
		));
	}

	public function render(){
		global $service;
        $service->get('Ressource')->getStyle('system','widget','herounit',$opt['stylesheet']);
		$opt = $this->data->getVar('widget_options');
        $this->options['blockwidget']['additionalclasses'] = str_replace(' ','_',$opt['imagealign']);
        $this->options['blockwidget']['disablerenderactions'] = true;
        $this->options['blockwidget']['disablerendertitle'] = true;

        $opt = $this->data->getVar('widget_options');
		$width = max(1,min(12,intval($opt['contentwidth'])));
        $i = $this->getInfo();
        $id = 'widget_'.$i['type'].'_'.$i['name']."_".$this->data->getVar('widget_objid');
        $cls = '#'.$id;
		$css = '';
		if (substr(trim($opt['content']),0,1) != '<') {
			$opt['content'] = '<p>'.str_replace("\n","</p><p>",$opt['content']).'</p>';
		}
		if ($opt['image']) {
            $img = (($opt['mobileimage'])?$opt['mobileimage']:$opt['image']);
            $pos = (($opt['mobileimageposition'])?$opt['mobileimageposition']:$opt['imageposition']);
			$css .= "$cls .cellimg{background-image:url('".UPLOADURL.$img."');background-position:".(($pos)?$pos.';':'left top;').';background-size:'.(($opt['imagesize'])?$opt['imagesize']:'contain').'!important;}';

            $img = (($opt['tabletimage'])?$opt['tabletimage']:$opt['image']);
            $pos = (($opt['tabletimageposition'])?$opt['tabletimageposition']:$opt['imageposition']);
			$css .= "@media(min-width:768px){ $cls .cellimg{background-image:url('".UPLOADURL.$img."');background-position:".(($pos)?$pos.';':'left top;').'}}';

            $img = $opt['image'];
            $pos = $opt['imageposition'];
			$css .= "@media(min-width:1024px){ $cls .cellimg{background-image:url('".UPLOADURL.$img."');background-position:".(($pos)?$pos.';':'left top;').'}}';
		}
		$img = '<div class="col-sm-'.(12-$width).' cell cellimg"></div>';
		$text = '<div class="col-sm-'.$width.' cell cellcontent">';
        $text .= '<h1'.(($opt['displaywidgettitle'])?"":" class='hidden'").'>'.$this->data->getVar('widget_title').'</h1>';
		if ($opt['subtitle']) {
			$text .= '<h2>'.$opt['subtitle'].'</h2>';
		}
		$text .= $opt['content'];
		$action = new ActionManager();
		$text .= '<div class="actions">'.$action->render($this->data->getVar('widget_objid')).'</div>';
		$text .= '</div>';
		if ($opt['contentorder'] == 'right') $content .= $img.$text;
		else $content .= $text.$img;
        $service->get("Ressource")->addStyle($css);
        return str_replace('{content}',$content,parent::render());
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');

		$form->add(new TextareaFormField('subtitle',$options['subtitle'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_HEROUNIT_HEROUNIT_SUBTITLE,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','subtitle')
		)));
		$form->add(new HtmleditorFormField('content',$options['content'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_HEROUNIT_HEROUNIT_CONTENT,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'widget_options','content')
		)));
		$form->add(new ImageuploadFormField('image',$options['image'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMAGE,
			'width' => 2
		)));
		$form->add(new ImageuploadFormField('tabletimage',$options['tabletimage'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMAGE_TABLET,
			'width' => 2
		)));
		$form->add(new ImageuploadFormField('mobileimage',$options['mobileimage'],array(
			'tab'=> 'basic',
			'title' => SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMAGE_MOBILE,
			'width' => 2
		)));
		$form->add(new SelectFormField('imageposition',$options['imageposition'],array(
			'tab'=> 'basic',
			'width' => 2,
			'title' => SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMAGEPOSITION,
			'options' => array(
				array('title' => _LEFT.' '._TOP, 'value' => 'left top'),
				array('title' => _CENTER.' '._TOP, 'value' => 'center top'),
				array('title' => _RIGHT.' '._TOP, 'value' => 'right top'),
				array('title' => _LEFT.' '._CENTER, 'value' => 'left center'),
				array('title' => _CENTER.' '._CENTER, 'value' => 'center center'),
				array('title' => _RIGHT.' '._CENTER, 'value' => 'right center'),
				array('title' => _LEFT.' '._BOTTOM, 'value' => 'left bottom'),
				array('title' => _RIGHT.' '._BOTTOM, 'value' => 'right bottom'),
				array('title' => _CENTER.' '._BOTTOM, 'value' => 'center bottom')
			)
		)));
		$form->add(new SelectFormField('tabletimageposition',$options['tabletimageposition'],array(
			'tab'=> 'basic',
			'width' => 2,
			'title' => SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMAGEPOSITION_TABLET,
			'options' => array(
				array('title' => _LEFT.' '._TOP, 'value' => 'left top'),
				array('title' => _CENTER.' '._TOP, 'value' => 'center top'),
				array('title' => _RIGHT.' '._TOP, 'value' => 'right top'),
				array('title' => _LEFT.' '._CENTER, 'value' => 'left center'),
				array('title' => _CENTER.' '._CENTER, 'value' => 'center center'),
				array('title' => _RIGHT.' '._CENTER, 'value' => 'right center'),
				array('title' => _LEFT.' '._BOTTOM, 'value' => 'left bottom'),
				array('title' => _RIGHT.' '._BOTTOM, 'value' => 'right bottom'),
				array('title' => _CENTER.' '._BOTTOM, 'value' => 'center bottom')
			)
		)));
		$form->add(new SelectFormField('mobileimageposition',$options['mobileimageposition'],array(
			'tab'=> 'basic',
			'width' => 2,
			'title' => SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMAGEPOSITION_MOBILE,
			'options' => array(
				array('title' => _LEFT.' '._TOP, 'value' => 'left top'),
				array('title' => _CENTER.' '._TOP, 'value' => 'center top'),
				array('title' => _RIGHT.' '._TOP, 'value' => 'right top'),
				array('title' => _LEFT.' '._CENTER, 'value' => 'left center'),
				array('title' => _CENTER.' '._CENTER, 'value' => 'center center'),
				array('title' => _RIGHT.' '._CENTER, 'value' => 'right center'),
				array('title' => _LEFT.' '._BOTTOM, 'value' => 'left bottom'),
				array('title' => _RIGHT.' '._BOTTOM, 'value' => 'right bottom'),
				array('title' => _CENTER.' '._BOTTOM, 'value' => 'center bottom')
			)
		)));
		$form->add(new SelectFormField('imagesize',$options['imagesize'],array(
			'tab'=> 'basic',
			'width' => 2,
			'title' => SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMAGESIZE,
			'options' => array(
				array('title' => 'Cover', 'value' => 'cover'),
				array('title' => 'Contain', 'value' => 'contain')
			)
		)));
		$form->add(new SelectcellwidthFormField('contentwidth',($options['contentwidth'] < 1 || $options['contentwidth'] > 12)?6:$options['contentwidth'],array(
			'tab'=>'basic',
			'title' => SYSTEM_WIDGET_HEROUNIT_HEROUNIT_WIDTH,
			'width' => 2
		)));
		$form->add(new SelectFormField('contentorder',$options['contentorder'],array(
			'tab'=> 'basic',
			'width' => 2,
			'title' => SYSTEM_WIDGET_HEROUNIT_HEROUNIT_CONTENTORDER,
			'options' => array(
				array('title' => _LEFT, 'value' => 'left'),
				array('title' => _RIGHT, 'value' => 'right')
			)
		)));
		$action = new ActionManager();
		$form->add($action->getFormField(URL.$service->get('Language')->getCode().'/api/system/widget/herounit/',$defobj));
	    return parent::edit($objs,$form);;
	}

	public function hasAccess($op){
		global $service;
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}
}
?>
