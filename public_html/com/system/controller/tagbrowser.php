<?php
/**
 * Tag browsing controller
 *
 * Handles tag browsing
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/controller');
$service->get('Ressource')->get('core/content');
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/model/tag');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/tagbrowser');

class TagbrowserController extends Controller{
	function init(){
		$this->component = 'system';
	}

	public function execute(){
		global $service;
		$view = $this->getView();
		$request = $service->get('Request')->get();

        $t = new TagStore();
        $c = new ContentStore();
		$results = array();
		$count = 0;
		if (isset($request['query']) && $request['query'] != '') {
			//1. Get text tag.
			$crit = new Criteria('tag_url',$request['query']);
			$tag = $t->get($crit);
			if (is_array($tag)) {
				$tag = $tag[0]->getVar('tag_title');
				//1. Select content that is tagged with the same tag.
				$crit = $this->getContentSelectionCriteria();
				$crit->add(new Criteria('content_tags','%,'.$tag.',%','LIKE'));
				$count = $c->count($crit);
				$page = (isset($request['p']))?intval($request['p']):0;
				$crit->setStart($page*10);
				$results = $c->get($crit);
			}

			$view->setVar('count',$count);
			$view->setVar('results',$results);
		}

		//3. Get all tags with number of items, display only non-empty tags.
		$ret = $service->get('Db')->query('
			SELECT `tag_url`, `tag_title`, count(*) as `count`
			FROM `tag`
			LEFT JOIN `content` ON `content_tags` LIKE CONCAT(\'%,\',`tag`.`tag_title`,\',%\')
			WHERE `content_tags` LIKE CONCAT(\'%,\',tag.tag_title,\',%\')
			GROUP BY `tag_title`
			ORDER BY `count` DESC
			LIMIT 0,100
			');
		$tags = array();
		foreach ($ret as $k => $v) {
			$tags[] = $v['tag_title'];
		}
		$view->setVar('tag',$tag);
		$view->setVar('tags',$tags);
		return $view;
	}

	private function getContentSelectionCriteria(){
		$crit = new CriteriaCompo();
		$c = new ContentStore();

		//1. Prevent unauthorized access to content
		$crit = $c->addGroupCriteria($crit);

		//2. Prevent access to drafts
		$crit->add(new Criteria('content_draft',0));

		$crit->setLimit(10);
		return $crit;
	}
}
