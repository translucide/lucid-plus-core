<?php
/**
 * Session controller
 *
 * MAy 20th, 2018
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/controller');

class SessionController extends Controller {
	function init(){
		$this->component = 'system';
	}

	public function execute(){
		global $service;
        $view = $this->getView();
		return $view;
	}

    public function apiCall($op){
        global $service;
        $request = $service->get('Request')->get();
        if ($op == 'keepalive') {
            //Set last request if it never has been set...
            $_SESSION['time'] = time();

            //Extend session lifetime when set to inifinite.
            if (!isset($_SESSION['lastrequest']) || $service->get('Setting')->get('sessionlifetime')  == 0 || $request['resetlogintimeout']){
                $_SESSION['lastrequest'] = time();
            }
            $sessionDuration = $service->get('Setting')->get('sessionlifetime') * 60;
            if ($sessionDuration == 0) {
                $sessionDuration = 3600*24; //24h
            }
            return array(
                'success' => true,
                'origin' => [
                    'component' => 'system',
                    'type' => 'controller',
                    'name' => 'session',
                    'op' => 'keepalive'
                ],
                'resetlogintimeout' => $request['resetlogintimeout'],
                'connected' => $service->get('User')->isLogged(),
                'remaining' => -1 * ( time() - $_SESSION['lastrequest'] - $sessionDuration),
                'uid' => $service->get('User')->getUid(),
            );
        }
    }

	public function hasAccess($op){
		global $service;
		return true;
		return false;
	}
}
?>
