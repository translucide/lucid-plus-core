<?php
/**
 * System Settings plugin controller
 *
 * Handles system settings editing requests
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/controller');
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('core/display/form/field');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/setting');

class SettingmanagerController extends Controller{
	function init(){
		$this->component = 'system';
	}

	public function execute(){
		global $service;
        $this->getLanguageFiles();
		$settingStore = new SettingStore();
		$settingStore->setOption('ignorelangs',true);
		$settings = $settingStore->get();
		$name = 'settingsmanagerform';
		$title = SYSTEM_SETTINGS_TITLE;
		$service->get('Theme')->setTitle($title);
		$langs = $service->get('Setting')->get('sitelanguages');
		$langs = explode(',',substr($langs,1,-1));
		$view = $this->getView();

		$service->get('EventHandler')->trigger('settingmanager.onLoad');

		$breadcrumbs = array(
			array('link' => URL.$service->get('Language')->getCode().'/admin', 'title' => _LOCATION_ADMINISTRATION),
			array('link' => URL.$service->get('Language')->getCode().'/admin/settings', 'title' => SYSTEM_SETTINGS_TITLE)
		);

		$op = $service->get('Request')->get('op');
		if ($op == 'savesetting') {
			$request = $service->get('Request')->get();
			$toSave = array();
			foreach($settings as $k => $v) {
				if($v->getVar('setting_language') == $service->get('Setting')->get('defaultlanguage') ||$v->getVar('setting_language') == 0){
					if ($v->getVar('setting_type') == 'selectmultiple' && is_array($request[$v->getVar('setting_name')])) {
						$request[$v->getVar('setting_name')] = ','.implode(',',$request[$v->getVar('setting_name')]).',';
					}
					$v->setVar('setting_value',$request[$v->getVar('setting_name')]);
					$toSave[] = $settings[$k];
					if ($v->getVar('setting_language') != 0) {
						$sameObjId = array();
						foreach ($settings as $ks => $vs) {
							if ($v->getVar('setting_objid') == $vs->getVar('setting_objid') &&
								$vs->getVar('setting_language') != $service->get('Setting')->get('defaultlanguage')) {
								$sameObjId[] = $ks;
							}
						}
						foreach ($langs as $kl=> $vl) {
							if ($vl != $v->getVar('setting_language') &&
								isset($request[$v->getVar('setting_name').'_'.$service->get('Language')->getCodeForId($vl)])) {
								$val = $request[$v->getVar('setting_name').'_'.$service->get('Language')->getCodeForId($vl)];
								$data = $v->get();
								$obj = new SettingData();
								$obj->set($data);
								foreach ($sameObjId as $ks => $vs) {
									if ($settings[$vs]->getVar('setting_language') ==  $vl) {
										$data = $settings[$vs]->get();
										$obj = new SettingData();
										$obj->set($data);
									}
								}
								$obj->setVar('setting_language',$vl);
								if ($obj->getVar('setting_id') == $v->getVar('setting_id')) $obj->setVar('setting_id',0);
								$obj->setVar('setting_value',$val);
								$toSave[] = $obj;
							}
						}
					}
				}
			}
			$settingStore->save($toSave);
			$service->get('EventHandler')->trigger('settingmanager.onSave');
			$service->get('Ressource')->clearCaches();
			$form = new Form(URL.$service->get('Language')->getCode().'/admin/system/settings',$name,$title,'POST');
			$form->setVar('intro','<div class="success">'._SAVED.'</div>');
			$view->setVar('form',$form);
		}else {
			$service->get('EventHandler')->trigger('settingmanager.onBeforeLoad');
			$form = new Form(URL.$service->get('Language')->getCode().'/admin/settings',$name,'<span><img src="'.URL.$service->get('Ressource')->getIcon(['icon' => 'setting','component' => 'system']).'"></span>'.$title,'POST');
			$form->setVar('intro','<div class="warning">'.SYSTEM_SETTINGS_INTRO.'</div>');

			//Get all setting categories in an associative array as id => title
			$settingCategory = new SettingCategoryStore();
			$settingCategory->setOption('ignorelangs',false);
			$catsobj = $settingCategory->get();
			$tabs = array();
			foreach($catsobj as $k => $v){
				$ct = $v->getVar('settingcategory_title');
				if (defined($ct)) $ct = constant($ct);
				$tabs[] = array('id' => 'tab'.$v->getVar('settingcategory_objid'), 'title'=>$ct);
			}
			$form->setVar('tabs',$tabs);

			foreach($settings as $k => $v) {
				if ($v->getVar('setting_language') == 0 || $v->getVar('setting_language') == $service->get('Setting')->get('defaultlanguage')) {
					$fieldCls = ucfirst($v->getVar('setting_type')).'FormField';
					$title = $v->getVar('setting_title');
					$help = $v->getVar('setting_description');
					if (defined($help)) $help = constant($help);
					if (defined($title)) $title = constant($title);
					$options = array();
					if ($v->getVar('setting_options') != '') {
						$tmpoptions = explode(',',$v->getVar('setting_options'));
						foreach ($tmpoptions as $k2 => $v2){
							$opt = explode('=',$v2);
							if (defined($opt[1])) $opt[1] = constant($opt[1]);
							$options[$k2] = array(
								'title'=>$opt[1],
								'value'=>$opt[0]
							);
						}
					}
					$translations = array();
					if ($v->getVar('setting_language') > 0) {
						foreach ($settings as $kt => $vt) {
							if ($vt->getVar('setting_language') != 0 &&
								$vt->getVar('setting_language') != $service->get('Setting')->get('defaultlanguage') &&
								$vt->getVar('setting_objid') == $v->getVar('setting_objid')) {
								$tr_options = array();
								if ($vt->getVar('setting_options') != '') {
									$tmpoptions = explode(',',$vt->getVar('setting_options'));
									foreach ($tmpoptions as $k2 => $v2){
										$opt = explode('=',$v2);
										if (defined($opt[1])) $opt[1] = constant($opt[1]);
										$tr_options[$k2] = array(
											'title'=>$opt[1],
											'value'=>$opt[0]
										);
									}
								}else $tr_options = $options;
								if (count($tr_options) > 0) $translations[$vt->getVar('setting_language')] = array('value' => $vt->getVar('setting_value'),'options' => $tr_options, 'lang'=>$service->get('Language')->getCodeForId($vt->getVar('setting_language')));
								else $translations[$vt->getVar('setting_language')] = array('value' => $vt->getVar('setting_value'), 'lang'=>$service->get('Language')->getCodeForId($vt->getVar('setting_language')));
							}
						}
						//Add non-existent translations to the form.
						foreach ($langs as $kl=> $vl) {
							if (!isset($translations[$vl]) && $vl != $v->getVar('setting_language')) {
								if (count($options)) $translations[$vl] = array('value' => '','options' => $options, 'lang'=>$service->get('Language')->getCodeForId($vl));
								else $translations[$vl] = array('value' => '', 'lang'=>$service->get('Language')->getCodeForId($vl));
							}
						}
						//Reorder translations in a new aray...
						$tl = array();
						foreach($translations as $k3 => $v3){
							$tl[] = $v3;
						}
						$translations = $tl;
					}
					$params = array('title'=> $title,'help'=> $help,'tab'=>'tab'.$v->getVar('setting_cid'));
					if (count($options) > 0) $params['options'] = $options;
					if (count($translations) > 0) {
						$params['translations'] = $translations;
						$params['lang'] = $service->get('Language')->getCodeForId($service->get('Setting')->get('defaultlanguage'));
					}
					$form->add(new $fieldCls($v->getVar('setting_name'), $v->getVar('setting_value'), $params));
				}
			}
			$form->add(new HiddenFormField('op','savesetting'));
			$form->add(new SubmitFormField('submit','',array('title' => _SAVE,'width'=>2)));
			$view->setVar('form',$form);
			$form = $service->get('EventHandler')->trigger('settingmanager.onLoad',$form,true);
		}
		$view->setVar('breadcrumbs',$breadcrumbs);
		return $view;
	}

	/**
	 * Returns the corresponding URL in the language ID given
	 * or simply returns the home page url if none found
	 *
	 * @public
	 * @param string $url The url for which we need the translation
	 * @param int $langid The language ID to find the URL for
	 * @return string The translated URL in the language we asked for or false if not found.
	 */
	public function translate($langid, $objid = 0, $url=''){
		global $service;
		if ($url == $service->get('Language')->getCode().'/admin/system/settings') {
			return $service->get('Language')->getCodeForId($langid).'/admin/system/settings';
		}
		return false;
	}

    /**
     * Load necessary language files to display settings with multi lingual constants
     *
     * @access protected
     **/
    protected function getLanguageFiles(){
        global $service;
        $coms = $service->get('Ressource')->listComponents();
        $lang = $service->get('Language')->getCode();
        foreach ($coms as $k => $com) {
            if (file_exists(ROOT.'com/'.$com.'/locale/'.$lang.'.php')) {
                $service->get('Ressource')->get('com/'.$com.'/locale/'.$lang);
            }
            elseif (file_exists(ROOT.'com/'.$com.'/lang/'.$lang.'/setting.php')) {
                $service->get('Ressource')->get('com/'.$com.'/lang/'.$lang.'/setting');
            }
            elseif (file_exists(ROOT.'com/'.$com.'/lang/'.$lang.'/'.$com.'.php')) {
                $service->get('Ressource')->get('com/'.$com.'/lang/'.$lang.'/'.$com);
            }
        }
    }
}
?>
