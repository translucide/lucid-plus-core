<?php
/**
 * Download controller
 *
 * August 23, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/controller');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/download');

class DownloadController extends Controller {
	function init(){
		$this->component = 'system';
	}

	public function execute(){
		global $service;
        $request = $service->get('Request')->get();
        $view = $this->getView();
		$file = str_replace(array('.','/'),'',$request['filename']).'.'.str_replace(array('.','/'),'',$request['fileextension']);
		$loc = '';
        if ($request['fileextension'] != '' && $request['filename'] != '' && file_exists(UPLOADROOT.'download/'.$file)) {
			$loc = UPLOADURL.'download/'.$file;
            $view->setVar('delay',3);
            $view->setVar('filelocation',$loc);
        }else {
			$url = $service->get('Url')->get();
			mail(
				$service->get('Setting')->get('adminmail'),
				'Error 404',
				'Error 404 on URL: '.print_r($url,true)."\n\nSERVER: ".print_r($_SERVER,true)
			);
            @header("Location: ".URL.$service->get('Language')->getCode().'/error404');
			die();
        }
		return $view;
	}

	public function hasAccess($op){
		global $service;
		if ($service->get('User')->isAdmin()) return true;
		return false;
	}		
}
?>