<?php
/**
 * Content Manager controller
 *
 * Handles content manager requests
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Content');
$service->get('Ressource')->get('core/controller');
$service->get('Ressource')->get('core/list/tag');
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('core/display/form/converter/requesttoobject');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/contentmanager');

class ContentmanagerController extends Controller{
	/**
	 * Initializes the controller.
	 *
	 * For a controller to find its associated view object in a component
	 * folder, you need to add this line to the init method:
	 * $this->component = 'component folder name here';
	 *
	 * @access public
	 */
	function init(){
		$this->component = 'system';
	}

	/**
	 * Executes the controller
	 *
	 * Each case statement represents a page the controller can load.
	 * For ajax operations, see apiCall method below.
	 *
	 * @access public
	 *
	 * @return View The configured view object, ready for rendering.
	 */
	public function execute(){
		global $service;
		$view = $this->getView();
		$url = $service->get('Url')->get();
		$store =  new ContentStore();
		$request = $service->get('Request')->get();
		$service->get('Theme')->setTitle(_LOCATION_CONTENT);
		return $view;
	}

	/**
	 * Handles AJAX calls to this controller.
	 *
	 * Url is constructed as follow:
	 * {lang}/api/{component name}/{type}/{name/filename}/{operation name}
	 *
	 * Of which operation name is passed as parameter to this method.
	 *
	 * @access public
	 *
	 * @param string $op The current AJAX call to run
	 * @return array $ret The response, later json_encoded once that method returns. (See core/controller/api.php)
	 */
	public function apiCall($op){
		global $service;
		$url = $service->get('Url')->get();
		$request = $service->get('Request')->get();
		$ret = array();
		switch($op){
			case 'getchildren' : {
				$tree = $this->getTreeObject();
				$children = $tree->getChildren($request['id']);
				foreach ($children as $k => $v) {
					$ret[] = array(
						'attr' => array(
							'id' => 'node_'.$v['id'],
							'rel' => ($v['type'] == 'folder' || $v['type'] == '')?'folder':$v['type']
						),
						'data' => $v['title'],
						'state' => ($v['type'] == 'folder' || $v['type'] == '')?'closed':''
					);
				}
			}break;
			case 'renamenode' : {
				$tree = $this->getTreeObject();
				$type = $request['nodetype'];
				if ($type == 'section') $type = 'folder';
				if ($type != 'folder') $type = 'leaf';
				$response = $tree->rename($request['id'],$request['title'],$type);
				if ($response !== false) $ret['status'] = 1;
			}break;
			case 'removenode' : {
				$tree = $this->getTreeObject();
				$type = $request['nodetype'];
				if ($type == 'section') $type = 'folder';
				if ($type != 'folder') $type = 'leaf';
				$response = $tree->delete($request['id'],$type);
				$content = $service->get('Ressource')->getRessourceObject('content',$request['nodetype']);
				$content->delete($request['id']);
				if ($response !== false) $ret['status'] = 1;
			}break;
			case 'movenode' : {
				$tree = $this->getTreeObject();
				$type = $request['nodetype'];
				$id = $request['id'];
				if ($type == 'section') $type = 'folder';
				if ($type != 'folder') $type = 'leaf';
				if ($request['copy'] == 1) {
					//Copy record before moving element...
					$service->get('Ressource')->get('core/db/dboperation');
					$store = new ContentStore();
					$store->setOption('ignorelangs',true);
					$objs = $store->getByObjId($id);
					$ct = $service->get('Ressource')->getRessourceObject('content',$request['nodetype']);
					$dboperation = new DBOperation($objs,$ct->getInfo());
					$ids = $dboperation->copy();
					$dboperation->copyChildren($id,$ids[0]);

					//Call content copy handler
					$ct->copy($objs,$id,$ids[0]);

					$id = $ids[0];
				}
				$response = $tree->move($id,$request['ref'],intval($request['position']),$type);
				//$response = $tree->order($id,$request['ref'],$request['position'],$type);
				if ($response !== false) {
					$ret['status'] = $id;
					$ret['id'] = $id;
				}
			}break;
			case 'createnode' : {
				$tree = $this->getTreeObject();
				$response = $tree->create($request['id'],intval($request['position']),$request['title'],($request['nodetype']=='folder')?'folder':'leaf',($request['nodetype'] == 'folder')?'section':$request['nodetype']);
				if ($response > 0) {
					$ret['id'] = $response;
					$ret['status'] = 1;
					$langs = $service->get('Language')->getCodes();

					//Finalize object data completion here...
					$contentStore = new ContentStore();
					$contentStore->setOption('ignorelangs',true);
					$nodes = $contentStore->getByObjId($response);
					$langs = $service->get('Language')->getCodes();
					$idpath = $contentStore->generateIdPath($nodes[0]);
					foreach ($nodes as $k => $v) {
						//Each node represents a different language.
						$nodes[$k]->setVar('content_idpath',$idpath);
						$nodes[$k]->setVar('content_groups',array(
                            $service->get('Group')->id('admin'),
                            $service->get('Group')->id('user'),
                            $service->get('Group')->id('anonymous')
                        )); //Allow everybody to view content by default.
						$nodes[$k]->setVar('content_url',$service->get('Url')->toUrl($v->getVar('content_title')));
						$nodes[$k]->setVar('content_draft',1);
						$nodes[$k]->setVar('content_urlpath',$contentStore->generateUrlPath($v));
						$nodes[$k]->setVar('content_titlepath',$contentStore->generateTitlePath($v));
						$nodes[$k]->setVar('content_created',time());
						$nodes[$k]->setVar('content_modified',time());
						$nodes[$k]->setVar('content_modifiedby',$service->get('User')->getUid());
					}
					$contentStore->save($nodes);
				}
			}break;
			case 'edit' : {
				$service->get('Ressource')->clear();
				if ($request['nodetype'] == 'folder') $type = 'section';
				else $type = $request['nodetype'];
				$form = '';
				$content = $service->get('Ressource')->getRessourceObject('content',$type);
				$info = $content->getInfo();

				if ($content){
					$defaultlang = $service->get('Language')->getDefault();
					$langs = $service->get('Language')->getCodes();
					$table = 'content';
					$store = new ContentStore();
					$store->setOption('ignorelangs',true);

					$obj = $store->getByObjId($request['id']);
					$defobj = $store->getDefaultObj($obj);

                    if (!is_array($info['fieldsoverride'])) $info['fieldsoverride'] = array();

					$form = new Form('', 'contentform', 'Edit content', $method='POST');
					$form->add(new HiddenFormField('contenttype',$type));
					$form->setVar('layout','vertical');
					$form->setVar('tabs',array(
						array('id' => 'basic', 'title' => _BASICOPTIONS),
						array('id' => 'seo', 'title' => _SEOOPTIONS),
						array('id' => 'advanced', 'title' => _ADVANCEDOPTIONS)
					));
					$form->add(new HiddenFormField($table.'_id',$defobj->getVar($table.'_id'),array(
						'tab'=>'basic',
						'lang'=>$defaultlang['code'],
						'translations' => $form->getTranslations($obj,$table.'_id')
					)));
					$form->add(new HiddenFormField($table.'_objid',$defobj->getVar($table.'_objid'),array(
						'tab'=>'basic',
						'lang'=>$defaultlang['code'],
						'translations' => $form->getTranslations($obj,$table.'_objid')
					)));
					$form->add(new TextFormField('content_title',$defobj->getVar('content_title'),array(
						'title'=>_TITLE,
						'length'=>100,
						'tab'=>'basic',
						'lang'=>$defaultlang['code'],
						'width' => 10,
						'translations' => $form->getTranslations($obj,'content_title')
					)));
					$form->add(new YesnoFormField('displaytitle',$defobj->getVar('content_options')['displaytitle'],array(
						'tab'=> 'basic',
						'width' => 2,
						'title' => CONTENTMANAGER_OPTION_DISPLAYTITLE
					)));
					$form->add(new TextareaFormField('content_exerpt',$defobj->getVar('content_exerpt'),array(
						'title'=>_EXERPT,
						'length'=>100,
						'tab'=>'basic',
						'lang'=>$defaultlang['code'],
						'translations' => $form->getTranslations($obj,'content_exerpt')
					)));
					$form->add(new TextFormField($table.'_pagetitle',$defobj->getVar($table.'_pagetitle'),array(
						'title'=>_PAGETITLE,
						'length'=>255,
						'tab'=>'seo',
						'width' => 6,
						'lang'=>$defaultlang['code'],
						'translations' => $form->getTranslations($obj,$table.'_pagetitle')
					)));
					$form->add(new TextFormField($table.'_url',$defobj->getVar($table.'_url'),array(
						'title'=>_PAGEURL,
						'length'=>100,
						'tab'=>'seo',
						'width' => 6,
						'lang'=>$defaultlang['code'],
						'translations' => $form->getTranslations($obj,$table.'_url')
					)));
					$form->add(new TextareaFormField($table.'_description',$defobj->getVar($table.'_description'),array(
						'title'=>_DESCRIPTION,
						'length'=>255,
						'tab'=>'seo',
						'lang'=>$defaultlang['code'],
						'width' => 6,
						'translations' => $form->getTranslations($obj,$table.'_description')
					)));
					$form->add(new TextareaFormField($table.'_keywords',$defobj->getVar($table.'_keywords'),array(
						'title'=>_KEYWORDS,
						'length'=>255,
						'tab'=>'seo',
						'lang'=>$defaultlang['code'],
						'width' => 6,
						'translations' => $form->getTranslations($obj,$table.'_keywords')
					)));
					$taglist = new TagList();
					$form->add(new TagselectorFormField($table.'_tags',$defobj->getVar($table.'_tags'),array(
						'title'=>_TAGS,
						'length'=>255,
						'tab'=>'seo',
						'lang'=>$defaultlang['code'],
						'width' => 6,
						'options' => $taglist->asOptions(),
						'translations' => $form->getTranslations($obj,$table.'_tags')
					)));
					$form->add(new SelectusergroupFormField($table.'_groups',$defobj->getVar($table.'_groups'),array(
						'title'=>_VISIBILITY,
						'length'=>255,
						'tab'=>'advanced',
						'multiple' => 1,
						'width' => 6
					)));
                    if (in_array('extracss',$info['overrides']) === false) {
                        $form->add(new TextareaFormField('extracss',$defobj->getVar($table.'_options')['extracss'],array(
    						'title' => CONTENTMANAGER_EDIT_EXTRACSS,
    						'help' => CONTENTMANAGER_EDIT_EXTRACSS_DESC,
    						'tab'=>'advanced',
    						'width' => 6
    					)));
                    }
					$form->add(new TextFormField('contentclasses',$defobj->getVar($table.'_options')['contentclasses'],array(
						'tab'=>'advanced',
						'width' => 6,
						'title' => 'Class',
					)));
					$form->add(new TextFormField('contentidattr',$defobj->getVar($table.'_options')['contentidattr'],array(
						'tab'=>'advanced',
						'width' => 6,
						'title' => 'ID',
					)));
					$form = $content->edit($obj,$form);
					$form->add(new YesnoFormField('content_draft',$defobj->getVar('content_draft'),array(
						'title'=>_DRAFT,
						'length'=>1,
						'tab'=>'basic',
                        'lang'=>$defaultlang['code'],
						'width' => 6,
                        'translations' => $form->getTranslations($obj,$table.'_draft')
					)));
					$form->add(new ImageuploadFormField('content_thumb',$defobj->getVar('content_thumb'),array(
						'tab'=> 'basic',
						'title' => _THUMBNAIL,
						'width' => 6
					)));

					$form->addSaveButton('save',_SAVE);
					$form->setVar('title','<span><img src="'.URL.$service->get('Ressource')->getIcon($info).'"></span>'.$form->getVar('title').
                        (($defobj->getVar('content_urlpath'))?' <a href="'.URL.$defobj->getVar('content_urlpath').'" target="_blank" alt="'.CONTENTMANAGER_EDIT_OPENPREVIEW.'" title="'.CONTENTMANAGER_EDIT_OPENPREVIEW.'" class="preview"><span class="glyphicon glyphicon-new-window"></span></a>':''));
					$ret = array('ressources'=>true, 'success'=>1,'form'=>$form->renderArray());
				}else {
					$ret = array('success' => 0, 'message' => SYSTEM_ADMIN_CONTENT_NOTFOUND.': '.$type);
				}
			}break;
			case 'save' : {
				$type = $request['contenttype'];
				$content = $service->get('Ressource')->getRessourceObject('content',$type);
				$saveRequest = new RequestToObject('content');
				$options = $content->getInfo('saveoptions');
				$options[] = 'displaytitle';
				$options[] = 'extracss';
				$options[] = 'contentclasses';
				$options[] = 'contentidattr';
				$saveRequest->addOptions($options);
				$result = $content->save($saveRequest);
				$objs = $saveRequest->get();
				$store = new ContentStore();
				$store->setOption('ignorelangs',true);
				$tagStore = new TagStore();
                $oldUrlPaths = array();
				foreach($objs as $k => $v) {
					$idpath = $store->generateIdPath($v);
					$tags = explode(',',$v->getVar('content_tags'));
					foreach ($tags as $tk => $tv) {
						$tags[$tk] = trim($tv);
						if ($tags[$tk] == '') unset($tags[$tk]);
					}
                    $newUrlPath = $store->generateUrlPath($v);
					if ($objs[$k]->getVar('content_urlpath') != $newUrlPath) {
                        $oldUrlPaths[$k] = $objs[$k]->getVar('content_urlpath');
                    }
                    $objs[$k]->setVar('content_urlpath',$newUrlPath);
					$objs[$k]->setVar('content_idpath',$idpath);
					$objs[$k]->setVar('content_titlepath',$store->generateTitlePath($v));
					$objs[$k]->setVar('content_level',count(explode('/',$idpath)));
					$objs[$k]->setVar('content_tags',','.implode(',',$tags).',');
					if ($tags != '' && $tags != ',,') $tagStore->createTags(implode(',',$tags));
				}
				$resp = $store->save($objs);

                //Update all related content URL paths/ID Paths/Title paths on save,
                //but only for items below the items that changed (if any)
                $store = new ContentStore();
				$store->setOption('ignorelangs',true);
                $crit = new CriteriaCompo();
                foreach($oldUrlPaths as $v) $crit->add(new Criteria('content_urlpath',$v.'%','LIKE'),'OR');
                $objs = $store->get($crit);
				foreach($objs as $k => $v){
					$idpath = $store->generateIdPath($v);
					$objs[$k]->setVar('content_urlpath',$store->generateUrlPath($v));
					$objs[$k]->setVar('content_idpath',$store->generateIdPath($v));
					$objs[$k]->setVar('content_titlepath',$store->generateTitlePath($v));
					$objs[$k]->setVar('content_level',count(explode('/',$idpath)));
				}
				$resp = $resp && $store->save($objs);
				if ($resp || $resp == array()) $ret = array('success'=>1, 'message' => 'Successfully saved.');
				else $ret = array('success' => 0, 'message' => 'Error while saving!');
			}break;
		}
		return $ret;
	}

	/**
	 * Creates a pre-configured tree object for this controller tree operations
	 *
	 * @access private
	 *
	 * @return Tree Tree object, configured for content manager.
	 */
	private function getTreeObject(){
		global $service;
		$service->get('Ressource')->get('core/display/tree');
		$tree = new Tree();
		$tree->folderSettings('content','content_objid','content_parent','content_position','content_type','content_title');
		$tree->folderTypes(['section']);
		$tree->leafSettings('content','content_objid','content_parent','content_position','content_type', 'content_title');
		return $tree;
	}

	public function hasAccess($op){
		global $service;
		if ($service->get('User')->isAdmin()) return true;
		return false;
	}
}
?>
