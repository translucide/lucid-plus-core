<?php
/**
 * Default widget controller
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/controller');
$service->get('Ressource')->get('core/display/menu');
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('core/display/form/field');
$service->get('Ressource')->get('core/display/form/converter/objecttoform');
$service->get('Ressource')->get('core/display/form/converter/requesttoobject');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/menumanager');
$service->get('Ressource')->get('lib/bootbox/4.3.0');

class MenumanagerController extends Controller {
	function init(){
		$this->component = 'system';
	}

	public function execute(){
		global $service;
		$view = $this->getView();
		$service->get('Theme')->setTitle(SYSTEM_MENUMANAGER);
		return $view;
	}
/*
	public function execute(){
		global $service;
		$view = $this->getView();
		$url = $service->get('Url')->get();
		$store =  new MenuStore();
		$request = $service->get('Request')->get();
		$breadcrumbs = array(
			array('link' => URL.$service->get('Language')->getCode().'/admin', 'title' => _LOCATION_ADMINISTRATION),
			array('link' => URL.$service->get('Language')->getCode().'/admin/menu', 'title' => SYSTEM_MENUMANAGER)
		);

		switch($url['path']) {
			case 'menu':{
				$service->get('Theme')->setTitle(SYSTEM_MENUMANAGER);

				//Generate a list of all content providers / handlers
				$infos = $service->get('Ressource')->getInfos('menu');
				$view->setVar('infos',$infos);
				$form = new Form('', 'menumanagerform', SYSTEM_MENUMANAGER_MANAGE, $method='POST');
				$form->setVar('intro',_CLICKTOEDIT);
				$view->setVar('form',$form);
			}break;
		}
		$view->setVar('breadcrumbs',$breadcrumbs);
		return $view;
	}
	*/

	private function getMenuOptions($objs = false){
		global $service;
		$store =  new MenuStore();
		$request = $service->get('Request')->get();
		//Get widget options for the corresponding widget
		$store->setOption('ignorelangs',true);
		if ($objs == false) $objs = $store->get($request['objid']);
		foreach($objs as $k => $v){
			$opt = $v->getVar('menu_options');
			$options[$service->get('Language')->getCodeForId($v->getVar('menu_language'))] = $opt;
		}
		return $options;
	}

	public function apiCall($op){
		global $service;
		$request = $service->get('Request')->get();
		$store =  new MenuStore();
		$type = $request['nodetype'];
		$defaultlang = $service->get('Language')->getDefault();
		$service->get('Ressource')->get('core/display/tree');
		$tree = new Tree();
		$tree->folderSettings('menu','menu_objid','menu_parent','menu_position','menu_type','menu_title');
		$tree->folderTypes(array('folder','group'));
		$tree->leafSettings('menu','menu_objid','menu_parent','menu_position','menu_type', 'menu_title');
		$ret = array();

		switch($op) {
			case 'edit' : {
				$form = '';
				$content = $service->get('Ressource')->getRessourceObject('menu',$type);
				if (is_object($content)){
					$info = $content->getInfo();
					$store->setOption('ignorelangs',true);
					$obj = $store->getByObjId($request['id']);
					$defobj = $store->getDefaultObj($obj);
					$form = new Form('', 'menumanagerform', _EDIT, $method='POST');
					$form->setTitle($defobj->getVar('menu_title'));
					$form->setVar('tabs',array(
						array('id' => 'basic', 'title' => _BASICOPTIONS),
						array('id' => 'advanced', 'title' => _ADVANCEDOPTIONS)
					));
					$form->add(new HiddenFormField('menu_id',$defobj->getVar('menu_id'),array(
						'tab'=>'basic',
						'lang'=>$defaultlang['code'],
						'translations' => $form->getTranslations($obj,'menu_id')
					)));
					$form->add(new HiddenFormField('menu_objid',$defobj->getVar('menu_objid'),array(
						'tab'=>'basic',
						'lang'=>$defaultlang['code'],
						'translations' => $form->getTranslations($obj,'menu_objid')
					)));
					$form->add(new HiddenFormField('menu_type',$defobj->getVar('menu_type'),array(
						'tab'=>'basic',
					)));
					$form->add(new HiddenFormField('menu_position',$defobj->getVar('menu_position'),array(
						'tab'=>'basic',
					)));
					$form->add(new TextFormField('menu_title',$defobj->getVar('menu_title'),array(
						'tab'=>'basic',
						'title' => _TITLE,
						'lang'=>$defaultlang['code'],
						'translations' => $form->getTranslations($obj,'menu_title')
					)));
					$form->add(new SelectFormfield('screensizevisibility',$defobj->getVar('menu_options')['screensizevisibility'],array(
						'tab' => 'basic',
						'title' => _DEVICEVISIBILITY,
						'options' => array(
							array('value' =>'','title'=>_ALL),
							array('value' =>'mobile','title'=>_MOBILEVISIBLE),
							array('value' =>'tablet','title'=>_TABLETVISIBLE),
							array('value' =>'desktop','title'=>_DESKTOPVISIBLE),
							array('value' =>'mobilehidden','title'=>_MOBILEHIDDEN),
							array('value' =>'tablethidden','title'=>_TABLETHIDDEN),
							array('value' =>'desktophidden','title'=>_DESKTOPHIDDEN)
						)
					)));
					$form->add(new HiddenFormField('menu_language',$defobj->getVar('menu_language'),array(
						'tab'=>'basic',
						'lang'=>$defaultlang['code'],
						'translations' => $form->getTranslations($obj,'menu_language')
					)));
					$form->add(new SelectusergroupFormField('menu_groups',$defobj->getVar('menu_groups'),array(
						'title'=>_VISIBILITY,
						'length'=>255,
						'tab'=>'advanced',
						'multiple' => 1
					)));
					$form->add(new HiddenFormField('menu_site',$defobj->getVar('menu_site')));
					$form->add(new HiddenFormField('nodetype',$type));
					$form = $content->edit($obj,$form);
					$form->setVar('title','<span><img src="'.URL.'com/'.$info['component'].'/asset/icon/32x32/'.$info['icon'].'.png"></span>'.$form->getVar('title'));
					$form->addSaveButton('save',_SAVE);
					$ret = array('success'=>1,'form'=>$form->renderArray());
				}
				else {
					$ret = array('success' => 0, 'message' => SYSTEM_ADMIN_CONTENT_NOTFOUND.': '.$type);
				}
			}break;
			case 'save' : {
				$id = $request['menu_objid'];
				$type = $request['nodetype'];
				$content = $service->get('Ressource')->getRessourceObject('menu',$type);
				$saveRequest = new RequestToObject('menu');
				$options = $content->getInfo('saveoptions');
				$options[] = 'screensizevisibility';
				$saveRequest->addOptions($options);
				$saveRequest->save();
				$service->get('Ressource')->clearCache('widget');
				$resp = false;
				if ($content) $resp = $content->save();
				if ($resp || $resp == array()) $ret = array('success'=>1, 'message' => _SAVED);
				else $ret = array('success' => 0, 'message' => 'Error while saving!');
			}break;
			case 'getchildren' : {
				$children = $tree->getChildren($request['id']);
				foreach ($children as $k => $v) {
					$ret[] = array(
						'attr' => array(
							'id' => 'node_'.$v['id'],
							'rel' => ($v['type'] == 'folder' || $v['type'] == '')?'folder':$v['type']
						),
						'data' => $v['title'],
						'state' => ($v['type'] == 'folder' || $v['type'] == '')?'closed':''
					);
				}
			}break;
			case 'removenode' : {
				$response = $tree->delete($request['id'],'leaf');
				if ($response !== false) $ret['status'] = 1;
			}break;
			case 'renamenode' : {
				$type = $request['nodetype'];
				if ($type == 'section') $type = 'folder';
				if ($type != 'folder') $type = 'leaf';
				$response = $tree->rename($request['id'],$request['title'],$type);
				if ($response !== false) $ret['status'] = 1;
			}break;
			case 'movenode' : {
				if ($request['id']) {
					$type = $request['nodetype'];
					if ($type != 'folder' && $type != 'leaf') $type = 'leaf';
					$id = $request['id'];
					if ($request['copy'] == 1) {
						//Copy before moving element...
						$store = new MenuStore();
						$store->setOption('ignorelangs',true);
						$obj = $store->getByObjId($id);
						$objid = $store->getNextId();
						foreach($obj as $k => $v) {
							$obj[$k]->setVar($store->getObjIdField(),$objid);
							$obj[$k]->setVar($store->getIdField(),0);
						}
						$store->save($obj);
						$id = $objid;
					}
					$response = $tree->move($id,$request['ref'],$request['position'],$type);
				}
				if ($response !== false) $ret['status'] = 1;
			}break;
			case 'createnode' : {
				$type = ($request['nodetype'])?$request['nodetype']:'folder';
				$info = $service->get('Ressource')->getRessourceObject('menu',$type)->getInfo();
				$store->setOption('ignorelangs',true);
				$objs = $store->createMultilingual();
				$objid = $objs[0]->getVar('menu_objid');
				foreach($objs as $k => $v){
					$objs[$k]->setVar('menu_objid',$objid);
					$objs[$k]->setVar('menu_component','');
					$objs[$k]->setVar('menu_parent',$request['id']);
					$objs[$k]->setVar('menu_position',$request['position']);
					$objs[$k]->setVar('menu_type',$info['type']);
					$objs[$k]->setVar('menu_title',$request['title']);
					$objs[$k]->setVar('menu_link','');
					$objs[$k]->setVar('menu_image','');
					$objs[$k]->setVar('menu_options','');
					$objs[$k]->setVar('menu_groups',array(
						$service->get('Group')->id('admin'),
						$service->get('Group')->id('users'),
						$service->get('Group')->id('anonymous')
					));
				}
				$store->save($objs);
				$ret['status'] = 1;
				$ret['id'] = $objid;
			}break;
		}
		return $ret;
	}

	public function hasAccess($op){
		global $service;
		if ($service->get('User')->isAdmin()) return true;
		return false;
	}
}
?>
