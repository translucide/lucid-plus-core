<?php
/**
 * Default widget controller
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/controller');
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/widgetzone');
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('core/list/section');
$service->get('Ressource')->get('core/display/table');
$service->get('Ressource')->get('core/display/form/converter/objecttoform');
$service->get('Ressource')->get('core/display/form/converter/requesttoobject');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/widgetmanager');

class WidgetmanagerController extends Controller {
	function init(){
		$this->component = 'system';
	}

	public function execute(){
		global $service;
		$view = $this->getView();
		$url = $service->get('Url')->get();
		$store =  new WidgetStore();
		$request = $service->get('Request')->get();
		$breadcrumbs = array(
			array('link' => URL.$service->get('Language')->getCode().'/admin', 'title' => _LOCATION_ADMINISTRATION),
			array('link' => URL.$service->get('Language')->getCode().'/admin/widget', 'title' => SYSTEM_WIDGETS)
		);

		switch($url['path']) {
			case 'widget':{
				//Generate a list of all content providers / handlers
				$service->get('Theme')->setTitle(WIDGETMANAGER_TITLE);
				$infos = $service->get('Ressource')->getInfos('widget');
				$infos[] = array(
					'type' => 'folder',
					'icon' => $service->get('Ressource')->getIcon(['component'=>'system','icon'=>'foler'])
				);

				$view->setVar('infos',$infos);
				$form = new Form('', 'widgetform', WIDGETMANAGER_TITLE, $method='POST');
				$form->setVar('intro',_CLICKTOEDIT);
				$view->setVar('form',$form);
			}break;
		}
		$view->setVar('breadcrumbs',$breadcrumbs);
		return $view;
	}

	private function getWidgetOptions($objs = false){
		global $service;
		$store =  new WidgetStore();
		$request = $service->get('Request')->get();
		//Get widget options for the corresponding widget
		$store->setOption('ignorelangs',true);
		if ($objs == false) $objs = $store->get($request['objid']);
		foreach($objs as $k => $v){
			$opt = $v->getVar('widget_options');
			$options[$service->get('Language')->getCodeForId($v->getVar('widget_language'))] = $opt;
		}
		return $options;
	}

	public function apiCall($op){
		global $service;
		$request = $service->get('Request')->get();

		//Flush cache before any tree operation so all is in sync with DB.
		$DBCache = new DBCache(new WidgetModel());
		$DBCache->flush();

		$service->get('Ressource')->get('core/display/tree');
		$tree = new Tree();
		$tree->leafSettings('widget','widget_objid','widget_zone','widget_position','widget_name', 'widget_title');
		$ret = array();
		$store =  new WidgetStore();
		switch($op) {
			case 'getchildren' : {
				$children = array();
				if ($request['id'] == '1') {
					$children[] = array(
						'id' => 'theme',
						'type' => 'folder',
						'title' => WIDGETMANAGER_THEMEWIDGETS
					);
					$wz = new Widgetzone();
					$wz->createThemeZones();
					$zones = $wz->get();
					foreach ($zones as $k => $v) {
						$children[] = array(
							'id' => $v['name'],
							'type' => 'folder',
							'title' => $v['title']
						);
					}
				}else {
					$children = $tree->getChildren($request['id']);
				}
				foreach ($children as $k => $v) {
					$ret[] = array(
						'attr' => array(
							'id' => 'node_'.$v['id'],
							'rel' => ($v['type'] == 'folder' || $v['type'] == '')?'folder':$v['type']
						),
						'data' => $v['title'],
						'state' => ($v['type'] == 'folder' || $v['type'] == '')?'closed':''
					);
				}
			}break;
			case 'removenode' : {
				$response = $tree->delete($request['id'],'leaf');
				if ($response !== false) $ret['status'] = 1;
			}break;
			case 'movenode' : {
				if ($request['id']) {
					$type = $request['type'];
					$id = $request['id'];
					if ($request['copy'] == 1) {
						//Copy before moving element...
						$service->get('Ressource')->get('core/db/dboperation');
						$store->setOption('ignorelangs',true);
						$objs = $store->getByObjId($id);
						$widget = $service->get('Ressource')->getRessourceObject('widget',$request['nodetype']);
						$dboperation = new DBOperation($objs,$widget->getInfo());
						$ids = $dboperation->copy();
						$dboperation->copyChildren($id,$ids[0]);
						$widget->copy($objs,$id,$ids[0]);
						$id = $ids[0];
					}
					$response = $tree->move($id,$request['ref'],$request['position'],'leaf');
				}
				if ($response !== false) {
					$ret['status'] = 1;
					$ret['id'] = $id;
				}
			}break;
			case 'createnode' : {
				$zone = $request['id'];
				$position = $request['position'];
				$title = $request['title'];
				if ($request['nodetype'] != 'folder'){
					$info = $service->get('Ressource')->getRessourceObject('widget',$request['nodetype'])->getInfo();
					$store->setOption('ignorelangs',true);
					$objs = $store->createMultilingual();
					$objid = $objs[0]->getVar('widget_objid');
					$com = $service->get('Ressource')->findRessourceComponent('widget',$info['name']);
					foreach($objs as $k => $v){
						$objs[$k]->setVar('widget_component',$com);
						$objs[$k]->setVar('widget_zone',$zone);
						$objs[$k]->setVar('widget_position',$position);
						$objs[$k]->setVar('widget_title',$title);
						$objs[$k]->setVar('widget_name',$info['name']);
						$objs[$k]->setVar('widget_type',$info['type']);
						$objs[$k]->setVar('widget_options','');
						$objs[$k]->setVar('widget_controllertrigger','');
						$objs[$k]->setVar('widget_urltrigger','');
						$objs[$k]->setVar('widget_requesttrigger','');
						$objs[$k]->setVar('widget_sectiontrigger','');
						$objs[$k]->setVar('widget_groups',array(
							$service->get('Group')->id('admin'),
							$service->get('Group')->id('users'),
							$service->get('Group')->id('anonymous')
						));
					}
					$store->save($objs);
					$ret['status'] = 1;
					$ret['id'] = $objid;
				}else {
					$ret['status'] = 0;
				}
			}break;
			case 'edit' : {
				$type = $request['nodetype'];
				$defaultlang = $service->get('Language')->getDefault();
				$form = '';
				if ($type != 'folder') {
					$content = $service->get('Ressource')->getRessourceObject('widget',$type);
					$info = $content->getInfo();
					if ($content){
						$store->setOption('ignorelangs',true);
						$obj = $store->getByObjId($request['id']);
						$defobj = $store->getDefaultObj($obj);
						$form = new Form('', 'widgetform', SYSTEM_WIDGETS_EDIT, $method='POST');
						$form->setTitle($defobj->getVar('widget_title'));
						$form->setVar('layout','vertical');
						$form->setVar('tabs',array(
							array('id' => 'basic', 'title' => _BASICOPTIONS),
							array('id' => 'advanced', 'title' => _ADVANCEDOPTIONS)
						));
						$form->add(new HiddenFormField('widget_id',$defobj->getVar('widget_id'),array(
							'tab'=>'basic',
							'lang'=>$defaultlang['code'],
							'translations' => $form->getTranslations($obj,'widget_id')
						)));
						$form->add(new HiddenFormField('widget_objid',$defobj->getVar('widget_objid'),array(
							'tab'=>'basic',
							'lang'=>$defaultlang['code'],
							'translations' => $form->getTranslations($obj,'widget_objid')
						)));
						$form->add(new HiddenFormField('widget_zone',$defobj->getVar('widget_zone'),array(
							'tab'=>'basic',
							'lang'=>$defaultlang['code'],
							'translations' => $form->getTranslations($obj,'widget_zone')
						)));
						$form->add(new HiddenFormField('widget_position',$defobj->getVar('widget_position'),array(
							'tab'=>'basic',
							'lang'=>$defaultlang['code'],
							'translations' => $form->getTranslations($obj,'widget_position')
						)));
						$form->add(new HiddenFormField('widget_name',$defobj->getVar('widget_name'),array(
							'tab'=>'basic',
							'lang'=>$defaultlang['code'],
							'translations' => $form->getTranslations($obj,'widget_name')
						)));
						$form->add(new HiddenFormField('widget_type',$info['type'],array(
							'tab'=>'basic'
						)));
						$form->add(new HiddenFormField('widget_language',$defobj->getVar('widget_language'),array(
							'tab'=>'advanced',
							'lang'=>$defaultlang['code'],
							'translations' => $form->getTranslations($obj,'widget_language')
						)));

						$form->add(new TextFormField('widget_title',$defobj->getVar('widget_title'),array(
							'tab'=>'basic',
							'title' => _TITLE,
							'lang'=>$defaultlang['code'],
							'width' => 9,
							'translations' => $form->getTranslations($obj,'widget_title')
						)));
						$form->add(new YesnoFormField('displaywidgettitle',$defobj->getVar('widget_options')['displaywidgettitle'],array(
							'tab'=> 'basic',
							'width' => 3,
							'title' => _DISPLAYTITLE
						)));

						$form->add(new SelectusergroupFormField('widget_groups',$defobj->getVar('widget_groups'),array(
							'title'=>_VISIBILITY,
							'length'=>255,
							'tab'=>'advanced',
							'width' => 12,
							'multiple' => 1
						)));
						$form->add(new TextFormField('widget_urltrigger',$defobj->getVar('widget_urltrigger'),array(
							'tab'=>'advanced',
							'lang'=>$defaultlang['code'],
							'title' => WIDGETMANAGER_DISPLAYURL,
							'width' => 4,
							'translations' => $form->getTranslations($obj,'widget_urltrigger')
						)));
						$form->add(new TextFormField('widget_cancelurltrigger',$defobj->getVar('widget_cancelurltrigger'),array(
							'tab'=>'advanced',
							'width' => 4,
							'title' => WIDGETMANAGER_CANCELURL
						)));
						$sl = new SectionList();
						$form->add(new SelectFormField('widget_sectiontrigger',$defobj->getVar('widget_sectiontrigger'),array(
							'tab'=>'advanced',
							'multiple' => 1,
							'width' => 4,
							'title' => WIDGETMANAGER_DISPLAYSECTION,
							'options' => $sl->asOptions()
						)));
						$form->add(new YesnoFormField('usecache',$defobj->getVar('widget_options')['usecache'],array(
							'title'=>_CACHE,
							'length'=>1,
							'width' => 2,
							'tab'=>'advanced'
						)));
						$form->add(new TextFormField('cacheduration',$defobj->getVar('widget_options')['cacheduration'],array(
							'title'=>_CACHEDURATION,
							'length'=>4,
							'width' => 2,
							'placeholder' => _PLACEHOLDER_CACHEDURATION,
							'tab'=>'advanced'
						)));
						$form->add(new SelectcellwidthFormField('widgetwidth',$defobj->getVar('widget_options')['widgetwidth'],array(
							'tab'=>'advanced',
							'title'=>_WIDTH,
							'width' => 2
						)));
						if (!isset($info['contentype'])) {
							$form->add(new SelectFormField('widgetcontenttype',$defobj->getVar('widget_options')['widgetcontenttype'],array(
								'tab'=>'advanced',
								'title'=>'Type',
								'width' => 2,
								'options' => array(
									array('title' => 'Header','value' => 'header'),
									array('title' => 'Navigation','value' => 'nav'),
									array('title' => 'Aside','value' => 'aside'),
									array('title' => 'Article','value' => 'article'),
									array('title' => 'Section','value' => 'section'),
									array('title' => 'Footer','value' => 'footer'),
								)
							)));
						}
						$form->add(new TextFormField('widget_tag',$defobj->getVar('widget_tag'),array(
							'tab'=>'advanced',
							'width' => 4,
							'title' => WIDGETMANAGER_TAG,
						)));
						$form->add(new TextFormField('widgetclasses',$defobj->getVar('widget_options')['widgetclasses'],array(
							'tab'=>'advanced',
							'width' => 4,
							'title' => 'Class',
						)));
						$form->add(new TextFormField('widgetidattr',$defobj->getVar('widget_options')['widgetidattr'],array(
							'tab'=>'advanced',
							'width' => 4,
							'title' => 'ID',
						)));
						$form->add(new SelectstyleFormField('stylesheet',$defobj->getVar('widget_options')['stylesheet'],array(
							'tab'=> 'advanced',
							'title' => _STYLE,
							'width' => 2,
							'component' => $defobj->getVar('widget_component'),
							'type' => 'widget',
							'name' => $type
						)));

						$form->add(new SelectFormField('variant',$defobj->getVar('widget_options')['variant'],array(
							'tab'=> 'advanced',
							'title' => _VARIANT,
							'width' => 2,
							'options' => array(
								['title' => 'Positive', 'value'=>'positive'],
								['title' => 'Inverted', 'value'=>'inverted']
							)
						)));

						$form->add(new HiddenFormField('widget_site',$defobj->getVar('widget_site')));
						$form->add(new HiddenFormField('widgettype',$type));
						$form = $content->edit($obj,$form);
						$form->setVar('title','<span><img src="'.URL.$service->get('Ressource')->getIcon($info).'"></span>'.$form->getVar('title'));
						$form->addSaveButton('save',_SAVE);
						$ret = array('success'=>1,'form'=>$form->renderArray());
					}
					else {
						$ret = array('success' => 0, 'message' => SYSTEM_ADMIN_CONTENT_NOTFOUND.': '.$type);
					}
				}
				else {
					$ret = array('success' => 0, 'message' => WIDGETMANAGER_CANTEDITZONES);
				}
			}break;
			case 'save' : {
				$id = $request['widget_objid'];
				$type = $request['widgettype'];
				$widget = $service->get('Ressource')->getRessourceObject('widget',$type);
				$saveRequest = new RequestToObject('widget');
				$saveRequest->addOptions(array_merge(array('usecache','cacheduration','displaywidgettitle','widgetwidth','widgetcontenttype','widgetclasses','widgetidattr','stylesheet','variant'),$widget->getInfo('saveoptions')));
				$r1 = $saveRequest->save();
				$service->get('Ressource')->clearCaches();
				$r2 = $widget->save();
				if ($r1 !== false && $r2 !== false) $ret = array('success'=>1, 'message' => _SAVED);
				else $ret = array('success' => 0, 'message' => 'Error while saving!');
			}break;
		}
		return $ret;
	}

	public function hasAccess($op){
		global $service;
		if ($service->get('User')->isAdmin()) return true;
		return true;
	}
}
?>
