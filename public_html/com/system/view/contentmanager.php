<?php
/**
 * Content manager view
 *
 * Default view loaded on content manager page
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/view/treeview');

class ContentmanagerView extends TreeView{
	public function init(){
	}
	
	public function render(){
		global $service;
		$this->apiUrl(URL.$service->get('Language')->getCode().'/api/system/controller/contentmanager/');
		$this->breadcrumbs([
			array('link' => URL.$service->get('Language')->getCode().'/admin', 'title' => _LOCATION_ADMINISTRATION),
			array('link' => URL.$service->get('Language')->getCode().'/admin/system/widgets', 'title' => _LOCATION_CONTENT)
		]);
		$this->option('contenttype','content');
		$this->option('infotypekey','type');
		$this->option('addcontenttitle',CONTENTMANAGER_SELECTCONTENTTYPE);
		$form = new Form('', 'contentform', 'Edit content', $method='POST');
		$form->setVar('intro',_CLICKTOEDIT);
		$this->form($form);
		return parent::render();
	}
}
?>