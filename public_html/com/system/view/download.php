<?php
/**
 * Download view
 *
 * Default view loaded on download page
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;

class DownloadView extends View{
	public function init(){
	}
	
	public function render(){
		global $service;
   		$service->get('Theme')->setTitle(SYSTEM_DOWNLOAD_TITLE);
        $service->get('Ressource')->addScript("
        $(document).ready(function(){
            var countDown = function countDown(val){
                var str = \"".SYSTEM_DOWNLOAD_WAITXSECONDS."\";
                $('.countdown').eq(0).html(str.replace('%n',val));
                if (val > 0) setTimeout(countDown,1000,val-1);
                else {
                    window.location = '{$this->data['filelocation']}';
                }
            }
            setTimeout(countDown,1000,{$this->data['delay']});
        });
        ");
        $this->setTemplate('com/system/ressource/template/download.tpl');
        return $this->renderTemplate();
	}
}
?>