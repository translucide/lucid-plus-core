<?php
/**
 * Edit Settings route view
 *
 * Default view loaded on settings page request
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('com/view/default');

class SettingmanagerView extends DefaultView{
	public function init(){
	}
	
	public function render(){
		global $service;
		$service->get('Ressource')->get('core/display/interface');
		$interface = new LucidInterface([
			'id' => 'settingmanager',
			'name' => 'settingmanager',
			'childs' => [
				new LucidViewport([
					'childs' => [
						new LucidLayout([
							'childs' => [
								new LucidHBox([
									'childs' => [
										new LucidBreadCrumbs([
											'childs' => $this->data['breadcrumbs']
										])
									]
								]),
								new LucidHBox([
									'childs' => [
										new LucidPanel([
											'id' => 'formpanel',
											'html' =>  $this->data['form']->render()
										])
									]
								])
							]
						])
					]
				])
			]
		]);
		return $interface->render();
	}
}
?>