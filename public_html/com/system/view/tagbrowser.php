<?php
/**
 * Tag Browser view
 *
 * Default view loaded on tag browser page request
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/default');

class TagbrowserView extends DefaultView{
	public function init(){
	}
	
	public function render(){
		global $service;
		$service->get('Ressource')->get('core/display/pager');
		$request = $service->get('Request')->get();
		$code = '';
		$code = '
		<article id="tagbrowser" class="tagbrowser">
			<div class="content">';
			
		if (isset($request['query'])) {
			$code .= '
			<div class="row">
				<div class="col-sm-12">
					<h1>'.$this->data['tag'].'</h1>
					<p>'.sprintf(SYSTEM_TAGBROWSER_TAGGEDWITH_COUNT,count($this->data['results']),$this->data['tag']).'</p>
				</div>
			</div>
			<div class="row results">
				<div class="col-sm-12">
					<ul>';
			foreach ($this->data['results'] as $v) {
				$code .= '<li>';
				if (trim($v->getVar('content_thumb')) != '') $code .= '<img src="'.UPLOADURL.$v->getVar('content_thumb').'">';
				$code .= '<h2><a href="'.URL.$v->getVar('content_urlpath').'">'.$v->getVar('content_title').'</a></h2>';
				if ($v->getVar('content_exerpt') != '') {
					$code .= '<p class="exerpt">'.$v->getVar('content_exerpt').'</p>';
				}
				$code .= '</li>';
			}
			$code .= '
					</ul>
				</div>
			</div>';
			$code .= Pager::render($this->data['count'],10,intval($request['p']),
				URL.$service->get('Language')->getCode().'/tag?query='.$request['query'].'&p={page}');
		}
		if (isset($this->data['tags']) && is_array($this->data['tags'])) {
			$code .= '<div class="row tags"><div class="col-sm-12">
				<h2>'.SYSTEM_TAGBROWSER_AVAILABLETAGS.'</h2>
				<p>'.sprintf(SYSTEM_TAGBROWSER_AVAILABLETAGS_COUNT,count($this->data['tags'])).'</p><ul>';
			foreach ($this->data['tags'] as $t) $code .= '<li><a href="'.URL.$service->get('Language')->getCode().'/tag/'.$service->get('Url')->toUrl($t).'" class="tag">'.$t.'</a><li>';
			$code .= '</ul></div></div>';
		}
		$code .= '
			</div>
		</article>';
		return $code;
	}
}
?>