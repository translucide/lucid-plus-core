<?php
/**
 * Menu manager view
 *
 * Default view loaded on menu manager page
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/view/treeview');

class MenumanagerView extends TreeView{
	public function init(){
	}
	
	public function render(){
		global $service;
		$this->apiUrl(URL.$service->get('Language')->getCode().'/api/system/controller/menumanager/');
		$this->breadcrumbs([
			array('link' => URL.$service->get('Language')->getCode().'/admin', 'title' => _LOCATION_ADMINISTRATION),
			array('link' => URL.$service->get('Language')->getCode().'/admin/menu', 'title' => _LOCATION_MENUS)
		]);
		$this->option('contenttype','menu');
		$this->option('infotypekey','type');
		$this->option('excludedtypes',array('folder'));
		$this->option('addtypes',array());
		$form = new Form('', 'contentform', 'Edit menu', $method='POST');
		$form->setVar('intro',_CLICKTOEDIT);
		$this->form($form);
		return parent::render();
	}
}
?>