<?php
/**
 * Widget manager view
 *
 * Default view loaded on widgets management page request
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/view/treeview');

class WidgetmanagerView extends TreeView{
	public function init(){
	}
	
	public function render(){
		global $service;
		$this->apiUrl(URL.$service->get('Language')->getCode().'/api/system/controller/widgetmanager/');
		$this->breadcrumbs(array(
			array('link' => URL.$service->get('Language')->getCode().'/admin', 'title' => _LOCATION_ADMINISTRATION),
			array('link' => URL.$service->get('Language')->getCode().'/admin/widget', 'title' => WIDGETMANAGER_TITLE)
		));
		$this->option('contenttype','widget');
		$this->option('infotypekey','name');
		$this->form(new Form('', 'widgetform', WIDGETMANAGER_TITLE, $method='POST'));
		return parent::render();
	}
}
?>