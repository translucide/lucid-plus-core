try{
    var basePath = CKEDITOR.basePath;
    basePath = basePath.substr(0, basePath.indexOf("ckeditor/"));
/*
    CKEDITOR.editorConfig = function( config ) {
        config.toolbarGroups = [
            { name: 'clipboard',   groups: [ 'clipboard', 'undo', 'find', 'selection' ] },
            { name: 'links',       groups: ['image'] },
            { name: 'insert' },
            { name: 'tools' },
            { name: 'document',	   groups: [ 'mode', 'document', 'doctools','source'] },
            { name: 'others' },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
            { name: 'styles' },
        ];
        config.plugins = 'basicstyles,clipboard,contextmenu,enterkey,entities,filebrowser,floatingspace,htmlwriter,link,image,list,pastetext,maximize,removeformat,resize,specialchar,sourcearea,tab,toolbar,undo,wysiwygarea,codesnippet,format';
        config.skin='bootstrapck';
        config.removeButtons = 'Table,About,Print,NewPage,Preview,Font,FontSize,PasteFromWord,SelectAll,lightbox';
        config.extraPlugins = 'autogrow,notification,youtube,codesnippet';
        config.autoGrow_minHeight = 250;
        //config.autoGrow_maxHeight = 750;
        config.disableNativeSpellChecker = false;
        CKEDITOR.config.contentsCss = [
            'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css',
            'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css',
            '{url}style.css?file[]=core/display/theme/theme.css&file[]={themeurl}css/main.css'
        ];
        config.extraAllowedContent = 'h1(*)[*]{*};h2(*)[*]{*};h3(*)[*]{*};h4(*)[*]{*};h5(*)[*]{*};h6(*)[*]{*};p(*)[*]{*};a(*)[*]{*};div(*)[*]{*};span(*)[*]{*}';
        config.basicEntities = true;
        config.entities_latin = true;
        config.entities_greek = true;
        config.entities = true;
        config.youtube_responsive = true;
        config.youtube_autoplay = false;
        config.entities_additional = '';
    };
    */
    /**
     * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
     * For licensing, see LICENSE.md or http://ckeditor.com/license
     */

    CKEDITOR.editorConfig = function( config ) {
    	// Define changes to default configuration here.
    	// For complete reference see:
    	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

    	// The toolbar groups arrangement, optimized for two toolbar rows.
    	config.toolbarGroups = [
    		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
    		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
    		{ name: 'links' },
    		{ name: 'insert' },
    		{ name: 'forms' },
    		{ name: 'tools' },
    		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
    		{ name: 'others' },
    		'/',
    		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
    		{ name: 'styles' },
    		{ name: 'colors' },
    		{ name: 'about' }
    	];

        config.skin='bootstrapck';

    	// Remove some buttons provided by the standard plugins, which are
    	// not needed in the Standard(s) toolbar.
    	config.removeButtons = 'Underline,Subscript,Superscript';

    	// Set the most common block elements.
    	config.format_tags = 'p;h1;h2;h3;pre';

    	// Simplify the dialog windows.
    	//config.removeDialogTabs = 'image:advanced;link:advanced';
    };

}
catch(e){}
