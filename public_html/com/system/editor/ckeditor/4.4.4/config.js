var basePath = CKEDITOR.basePath;
basePath = basePath.substr(0, basePath.indexOf("ckeditor/"));   
(function() {
   //CKEDITOR.plugins.addExternal('nearestevent','../../../com/system/editor/ckeditor/4.4.4/plugins/nearestevent/', 'plugin.js');
   //CKEDITOR.plugins.addExternal('NonverBlaster','../../../com/system/editor/ckeditor/4.4.4/plugins/NonverBlaster/', 'plugin.js');
   //CKEDITOR.plugins.addExternal('montrealqualifie','../../../com/system/editor/ckeditor/4.4.4/plugins/montrealqualifie/', 'plugin.js');
   //CKEDITOR.plugins.addExternal('grid','../../../com/system/editor/ckeditor/4.4.4/plugins/grid/', 'plugin.js');
   //CKEDITOR.plugins.addExternal('herounit','../../../com/system/editor/ckeditor/4.4.4/plugins/herounit/', 'plugin.js');
   //CKEDITOR.plugins.addExternal('oembed','../../../com/system/editor/ckeditor/4.4.4/plugins/oembed/', 'plugin.js');
   //CKEDITOR.plugins.addExternal('location','../../../com/system/editor/ckeditor/4.4.4/plugins/location/', 'plugin.js');
   //CKEDITOR.plugins.addExternal('faq','../../../com/system/editor/ckeditor/4.4.4/plugins/faq/', 'plugin.js');
})();

CKEDITOR.editorConfig = function( config ) {
	config.language = 'fr';
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo', 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
		{ name: 'styles'},
	];
    /*
'a11yhelp' : 1,
		'about' : 1,
		'basicstyles' : 1,
		'bidi' : 1,
		'blockquote' : 1,
		'clipboard' : 1,
		'colorbutton' : 1,
		'colordialog' : 1,
		'contextmenu' : 1,
		'dialogadvtab' : 1,
		'div' : 1,
		'elementspath' : 1,
		'enterkey' : 1,
		'entities' : 1,
		'filebrowser' : 1,
		'find' : 1,
		'flash' : 1,
		'floatingspace' : 1,
		'font' : 1,
		'format' : 1,
		'forms' : 1,
		'glyphicons' : 1,
		'horizontalrule' : 1,
		'htmlwriter' : 1,
		'iframe' : 1,
		'image' : 1,
		'indentblock' : 1,
		'indentlist' : 1,
		'justify' : 1,
		'language' : 1,
		'link' : 1,
		'list' : 1,
		'liststyle' : 1,
		'locationmap' : 1,
		'magicline' : 1,
		'maximize' : 1,
		'newpage' : 1,
		'pagebreak' : 1,
		'pastefromword' : 1,
		'pastetext' : 1,
		'preview' : 1,
		'print' : 1,
		'removeformat' : 1,
		'resize' : 1,
		'save' : 1,
		'scayt' : 1,
		'selectall' : 1,
		'showblocks' : 1,
		'showborders' : 1,
		'smiley' : 1,
		'sourcearea' : 1,
		'specialchar' : 1,
		'stylescombo' : 1,
		'tab' : 1,
		'table' : 1,
		'tabletools' : 1,
		'templates' : 1,
		'toolbar' : 1,
		'undo' : 1,
		'widget' : 1,
		'wsc' : 1,
		'wysiwygarea' : 1
    */
	/*config.plugins = 'basicstyles,blockquote,clipboard,contextmenu,dialogadvtab,elementspath,enterkey,'+
        'entities,filebrowser,floatingspace,horizontalrule,htmlwriter,image,justify,link,list,liststyle,maximize,newpage,'+
        'pagebreak,pastetext,removeformat,resize,sourcearea,specialchar,stylescombo,tab,toolbar,undo,wysiwygarea';*/
	config.plugins = 'basicstyles,clipboard,contextmenu,dialogadvtab,enterkey,'+
        'entities,filebrowser,floatingspace,htmlwriter,justify,link,list,liststyle,'+
        'pastetext,removeformat,resize,specialchar,tab,toolbar,undo,wysiwygarea';
    //config.skin='bootstrapck';
	config.removeButtons = 'Underline,Subscript,Superscript,Table,Image,Format,About,Print,NewPage,Preview,Font,FontSize,CreateDiv,PasteFromWord,SelectAll,Replace,lightbox,Flash';
	//config.extraPlugins = 'NonverBlaster,nearestevent,faq,grid,lineutils,widget';
	//config.extraPlugins = 'grid,locationmap,herounit';
	//config.extraPlugins = 'codesnippet';
	config.extraPlugins = 'wordcount,autogrow';
	config.autoGrow_minHeight = 250;
	config.autoGrow_maxHeight = 750;
	
	config.disableNativeSpellChecker = true;
	//config.stylesSet = 'default:{themeurl}js/ckeditorstyles.js';
	//config.locationMapPath = '{url}lib/ckeditor/4.4.4/plugins/locationmap/';
	CKEDITOR.config.contentsCss = [
		'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css',
		'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css',
		'{url}style.css?file[]=core/display/theme/theme.css&file[]={themeurl}css/main.css'
	]
	config.codeSnippet_theme = 'xcode';
	config.extraAllowedContent = 'h1(*)[*]{*};h2(*)[*]{*};h3(*)[*]{*};h4(*)[*]{*};h5(*)[*]{*};h6(*)[*]{*};p(*)[*]{*};a(*)[*]{*};div(*)[*]{*};span(*)[*]{*}';
	
config.wordcount = {

    // Whether or not you want to show the Paragraphs Count
    showParagraphs: false,

    // Whether or not you want to show the Word Count
    showWordCount: true,

    // Whether or not you want to show the Char Count
    showCharCount: false,

    // Whether or not you want to count Spaces as Chars
    countSpacesAsChars: false,

    // Whether or not to include Html chars in the Char Count
    countHTML: false,
    
    // Maximum allowed Word Count, -1 is default for unlimited
    maxWordCount: -1,

    // Maximum allowed Char Count, -1 is default for unlimited
    maxCharCount: -1,

    // Add filter to add or remove element before counting (see CKEDITOR.htmlParser.filter), Default value : null (no filter)
    filter: new CKEDITOR.htmlParser.filter({
        elements: {
            div: function( element ) {
                if(element.attributes.class == 'mediaembed') {
                    return false;
                }
            }
        }
    })
};	
};
nanospell.ckeditor('all',{
    dictionary : "en",
    server : "php"
 }); 
