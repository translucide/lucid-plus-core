/**
 * lucidComments jQuery Plugin
 *
 * Userside comments functionality
 * 
 * How to use this plugin :
 *
 * $('selector').lucidComments()
 */






/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// CommonJS
		factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write

		if (value !== undefined && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setTime(+t + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {};

		// To prevent the for loop in the first place assign an empty array
		// in case there are no cookies at all. Also prevents odd result when
		// calling $.cookie().
		var cookies = document.cookie ? document.cookie.split('; ') : [];

		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			var name = decode(parts.shift());
			var cookie = parts.join('=');

			if (key && key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		if ($.cookie(key) === undefined) {
			return false;
		}

		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

}));









/**
 * LucidComments class
 *
 * @class LucidComments
 * @public
 */
(function( $ ) {	
	var methods = {
		/**
		 * Initialize the plugin
		 *
		 * @public
		 * @memberOf LucidComments
		 * @function
		 * @returns {Object} A reference to the DOM object this plugin is working on
		 */
		init: function(options) {
			var $this = $(this);
			$this.addClass('lucidcomments');
			var cobjid = (($this.attr('data-cobjid') > 0)?$this.attr('data-cobjid'):0); 
			var wid = (($this.attr('data-widgetid') > 0)?$this.attr('data-widgetid'):0); 
			var ps = (parseInt($this.attr('data-ps')) > 0)?parseInt($this.attr('data-ps')):20; 
			$this.data('options', $.extend({
				pageSize: ps,
				contentObjId: cobjid,
				widgetId: wid,
				currentEditId: 0,
				reloadOnChange: 1,
				pageParameter: 'comments_page',
                highlightedUser: 0
			}, options));
			$this.lucidComments("setup");
			return this;
		},

		/**
		 * setup: Adds the necessary elements to body.
		 *
		 * @public
		 * @memberOf LucidComments
		 */
		setup: function(){
            var $this = $(this);
			var opt = $this.data('options');
			$this.lucidComments("loadPage",0,opt.pageSize);
			$this.lucidComments("loadForm",0,0);
            var highlighteduser = $.cookie("lucidcomments_highlighteduser_v4");
            if (highlighteduser !== null) opt.highlightedUser = parseInt(highlighteduser);
            $this.data('options',opt);
            return this;
        },
		loadPage: function(p,ps) {
			var $this = $(this);
			var opt = $this.data('options');
			if (opt.reloadOnChange) {
				///$this.find('.comments_list').fadeIn();
				$this.lucidComments("addCommentsEvents");							
				$this.lucidComments("addPagerEvents");							
			}				
			else {
				$this.find('.comments_list').html("").addClass('loading');
				$.ajax({
					type: "POST",
					url: "{url}{lang}/api/system/widget/comments/getpage",
					data: {
						'page':p,
						'pagesize':opt.pageSize,
						'cobjid':opt.contentObjId,
						'wid':opt.widgetId 
					},
					success: function(data){
						if (data.success) $this.find('.comments_list').css('display','none').removeClass('loading').html(data.response).fadeIn();
						$this.lucidComments("addCommentsEvents");							
						$this.lucidComments("addPagerEvents");							
					},
					dataType: "json"
				});
			}
			return this;
		},
		loadForm: function(id,parent){
			var $this = $(this);
			var opt = $this.data('options');
			var formname = 'commentform';
			if (id === 0 && parent === 0) {
				formname = 'newcommentform';
				if ($this.find('li.comment').length === opt.pageSize && $('.commentsnav').find("li a").length > 0) {
					var active = $('.commentsnav').find("li.active a").eq(0);
					var last = $('.commentsnav').find("li:last-child").eq(0).prev().children("a");
					console.info(parseInt(last.attr('data-page')));
					console.info(parseInt(active.attr('data-page')));
					if (parseInt(last.attr('data-page')) > parseInt(active.attr('data-page'))) {
						$this.find('#newcommentform').css('display','none');
						return;
					}
					
				}
			}
			$('.comments_form').html("").addClass('loading');
			$.ajax({
				type: "POST",
				url: "{url}{lang}/api/system/widget/comments/getform",
				data: {
					'cobjid':opt.contentObjId,
					'id': id,
					'parent': parent,
					'formname':formname,
					'wid':opt.widgetId
				},
				success: function(data){
					$('.comments_form').removeClass('loading');
					if (data.success) {
						var containers = '<div id="'+formname+'_div" class="formcontainer"></div><div id="'+formname+'_messages"></div>';
						if (formname === 'newcommentform') $this.find('.comments_form').html(containers+data.response).fadeIn();
						else {
							if (id === 0) $this.find('li.id'+parent).eq(0).after('<li class="comments_form">'+containers+data.response+'</li>');
							else $this.find('li.parent'+parent+'.id'+id).eq(0).addClass('comments_form').html(containers+data.response);
						}
						$this.lucidComments("addFormEvents",id,parent);
					}
					else alert('ERROR: Unable to load comment form. Please contact the site admin.');
				},
				dataType: "json"
			});
			return this;
		},
		addFormEvents: function(id,parent) {
			var $this = $(this);
			var opt = $this.data('options');
			var formname = 'commentform';
			if (id === 0 && parent === 0) formname = 'newcommentform';
			
			if (nanospell) {
				nanospell.ckeditor('#comment_content',{
					dictionary:"en",
					server:"php"  // can be php, asp, asp.net or java
				});
			}
			
			$('.comments_form #'+formname).ajaxForm({						
				target: '#'+formname+'_messages',
				url : "{url}{lang}/api/system/widget/comments/save",
				beforeSubmit : function(data, jqForm, options){
					$('.comments_form').css('display','none');
					try {
						for (instance in CKEDITOR.instances) {
							var ckvalue = CKEDITOR.instances[instance].getData();
							ckvalue = ckvalue.replace(/&amp;/g,'&');
							$('#'+instance).val(ckvalue);
							for(i=0;i<data.length;i++) {
								if(data[i].name == instance) data[i].value = ckvalue;
							}
							CKEDITOR.instances[instance].destroy();
						}
					} catch(err) {}
					return true;
				},
				success: function(data, statusText, xhr, form){
					if (data.success) {
						if (opt.reloadOnChange) {
							var href = window.location.href;
							if(href.indexOf("#") > 0) href = href.split("#")[0];
							if (href.indexOf('?') <= 0) href += '?';
							href = href + '&s='+Math.floor(Math.random() * (1000 - 0));
							if (formname === 'newcommentform') window.location = href+'#comments_form';
							else window.location = href+'#comment-id-'+parent;
							console.info("reloaded page");
						}
						else {
							if (formname === 'newcommentform') {
								/* Re-add comment to end of comments list */
								$('div.comments_form').fadeOut().removeClass("error").html('');
								$('.commentslist').append(data.response).fadeIn();
							} else {
								$this.find('li.comments_form').fadeOut().replaceWith(data.response).fadeIn();
							}
							$this.lucidComments("loadForm",0,0);
							$this.lucidComments("addCommentsEvents");
						}
					}
				}
			});
			if (id > 0 || parent > 0) {
				$('.comments_form #'+formname+' button[type=submit]').after("<button type=\"reset\" class=\"btn btn-secondary\">Cancel</button>");
				$('.comments_form #'+formname+' button[type=reset]').click(function(e){
					e.preventDefault();
					var currentItem = $(this).closest("li.comments_form");
					if (id === 0) {
						currentItem.remove();
					}
					if (id > 0) {
						$.ajax({
							type: "POST",
							url: "{url}{lang}/api/system/widget/comments/getcomment",
							data: {
								'cobjid':opt.contentObjId,
								'id': id,
								'parent': parent,
								'wid':opt.widgetId
							},
							success: function(data){
								if (data.success) {
									currentItem.removeClass("comments_form").html(data.response);
									$this.lucidComments("addCommentsEvents");						
								}
								else alert('ERROR: Unable to load comment. Please contact the site admin.');
							},
							dataType: "json"
						});						
					}
					$this.lucidComments("loadForm",0,0);
					$this.lucidComments("addCommentsEvents");						
				});
			}
			return this;			
		},
		addCommentsEvents: function(){
			var $this = $(this);
			var opt = $this.data('options');
			$('.commentslist li').find("button").each(function(){
				$(this).off('click').click(function(){
					if ($(this).hasClass('edit')) {
						$this.lucidComments("loadForm",$(this).closest('li').attr('data-id'),$(this).closest('li').attr('data-parent'));
					}
					if ($(this).hasClass('reply')) {
						$this.lucidComments("loadForm",0,$(this).closest('li').attr('data-id'));
					}
					if ($(this).hasClass('delete')) {
						var id = $(this).closest('li').attr('data-id');
						var parent = $(this).closest('li').attr('data-parent');
						var dialog = $this.find("#deleteconfirmdialog");
						var prev = $(this).closest('li').prev().eq(0).attr('data-id');
						var ccount = parseInt($this.find('.commentslist li').length);
						dialog.find("button#dialog_ok").off('click').click(function(){
							$.ajax({
								type: "POST",
								url: "{url}{lang}/api/system/widget/comments/delete",
								data: {
									'cobjid':opt.contentObjId,
									'id': id,
									'parent': parent,
									'wid':opt.widgetId
								},
								success: function(data){
									if (data.success) {
										if (opt.reloadOnChange) {
											var href = window.location.href;
											if(href.indexOf("#") > 0) href = href.split("#")[0];
											if (href.indexOf('?') <= 0) href += '?';
											href = href + '&s='+Math.floor(Math.random() * (1000 - 0));
											if (ccount === 1 || parseInt(prev) === 0) window.location = href+'#comments_form';
											else {
												if (parseInt(prev) > 0) window.location = href+'#comment-id-'+prev;
												else window.location = href+'#comment-id-'+parent;
											}
										}
										$this.find('li.comment[data-id='+id+']').remove();
									}
									else alert('ERROR: Unable to delete comment. Please contact the site admin.');
									dialog.modal('hide');
									
									if ($this.find(".comments_form #newcommentsform").length === 0) {
										$this.lucidComments("loadForm",0,0);
									}
								},
								dataType: "json"
							});													
						});
						dialog.modal({
							show: true,
							cache: false
						});
						dialog.modal('show');
					}
					
				});
			});
			$('.commentslist li .body').find('img').each(function(){
				if ($(this).parent()[0].nodeName.toLowerCase() != 'a') {
					$(this).wrap('<a href="'+$(this).attr("src")+'" target="_blank"></a>');
				}
			});
			$('.commentslist li .body').find('a').attr('target','_blank');
			$('.commentslist li .body .content').each(function (){
				if (this.scrollHeight <=  this.clientHeight) {
					$(this).removeClass("hideContent").next(".show-more").css("display","none");
				}else{
					$(this).addClass("hideContent").next(".show-more").css("display","block");
				}
			});
			$('.commentslist li .body .show-more a').off('click').on("click", function(e) {
				e.preventDefault();
				var $this = $(this); 
				var $content = $this.parent().prev("div.content");
				var linkText = $this.text().toUpperCase();    
				if(linkText === $this.attr("data-moretxt").toUpperCase()){
					linkText = $this.attr("data-lesstxt");
					$content.removeClass("hideContent").addClass("showContent");
				} else {
					linkText = $this.attr("data-moretxt");
					$content.removeClass("showContent").addClass("hideContent");
				}
				$this.text(linkText);
			});
            $('.commentslist li h1.username').off('click').on('click',function(e){
                e.preventDefault();
                var $this = $(this);
                var uid = parseInt($this.attr("data-uid"));
                $this.lucidComments('setHighlightedUser',uid);
            });
            var uid = $.cookie('lucidcomments_highlighteduser_v4');
            $this.lucidComments('setHighlightedUser',uid);
			return this;
		},
		addPagerEvents: function() {
			var $this = $(this);
			var opt = $this.data('options');
			$('.commentsnav').find("a").each(function(){
				if (opt.reloadOnChange === false) {
					$(this).off('click').click(function(e){
						e.preventDefault();
						if (!$(this).hasClass('disabled') && !$(this).hasClass('active') && !$(this).hasClass('clearhighlight')) {
							$this.lucidComments("loadPage",$(this).attr('data-page'),opt.pageSize);
						}else {
                            if ($(this).hasClass('clearhighlight')) {
                                $this.lucidComments('clearHighlighted');
                            }else return false;
						}
					});					
				}else {
                    if ($(this).hasClass('clearhighlight')) {
                        $(this).off('click').click(function(e){
                            e.preventDefault();
                            $this.lucidComments('clearHighlighted');
                        });
                    }
                }
			});
			return this;
		},
        clearHighlighted: function(){
			var $this = $(this);
			var opt = $this.data('options');
            opt.highlightedUser = 0;
            $this.data('options',opt);
            $this.lucidComments('setHighlightedUser',0);
            $('.commentslist li.comment.highlighted').removeClass('highlighted');
            console.info("data-uid = "+$.cookie('lucidcomments_highlighteduser_v4'));
            return this;
        },
        setHighlightedUser: function(uid){
            console.info("data-uid = "+uid);
            if (uid !== null) {
                $.cookie('lucidcomments_highlighteduser_v4',uid,{path:'/'});
                $('.commentslist li.comment').removeClass('highlighted');
                $('.commentslist li h1.username[data-uid="'+uid+'"]').parents("li.comment").addClass('highlighted');
            }
        }
	};
	$.fn.lucidComments = function(method) { 
		if (methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.lucidComments' );
		}    
	};	
})(jQuery);
$( document ).ready(function() {
    $('section.widget.comments').lucidComments();
});
