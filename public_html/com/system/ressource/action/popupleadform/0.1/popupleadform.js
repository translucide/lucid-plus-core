/**
 * popupFormLead
 *
 * Popup a form in a bootstrap dialog, wait for user to fill the form and
 * sends user to a specified URL.
 *
 * @class popupFormLead
 * @public
 */
(function( $ ) {	
	var methods = {
		init: function(options) {
            var $this = $(this);
			if (!options) {
				options = new Array();
			}
			$this.data('options', $.extend({
			}, options));	
            $this.popupLeadForm("setup");
            $this.popupLeadForm("apply");
			return this;
		},
        setup: function() {
			var $this = $(this);
            var popupel = $("div#popupleadform_dialog.modal");
            if (popupel.length == 0) {
                $('body').append('<div id="popupleadform_dialog" class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title"></h4></div><div class="modal-body"></div></div></div></div>');
            }
			return this;
        },
        apply: function(){
			var $this = $(this);
            $this.each(function(i,e){
                $(e).click(function(){
                    var formid = $(e).attr("data-form");
					var subject= $(e).attr("data-subject");
					var subjectdisabled = $(e).attr("data-subject-disabled");
                    $("#popupleadform_dialog .modal-body").html('<div id="form_widget_'+formid+'_div"></div>');
                    $("#popupleadform_dialog .modal-title").html($(e).html().replace(/<[^>]*>/g, " "));
                    $.post("{url}{lang}/api/contactus/widget/form/render",{"id":$(e).attr("data-form")},function(d){
						$("#popupleadform_dialog .modal-body #form_widget_"+formid+"_div").after(d.data);
                        if (d.success) {
                            $("#popupleadform_dialog").modal({
                                show: true,
                                cache: false
                            });
							if (subject != '') {
								$("#popupleadform_dialog").find("input[name=subject]").eq(0).attr("value",subject);
							}
							if (subjectdisabled == 'true' || subjectdisabled == '1') {
								$("#popupleadform_dialog").find("input[name=subject]").eq(0).attr('readonly','readonly');
							}
                            $("#popupleadform_dialog").modal('show');
							$this.popupLeadForm("submit",e);
                        }
                    });
                });
            });
			return this;
        },
		submit: function(e){
			$this = $(this);
			var formid = $(e).attr("data-form");
			$('#popupleadform_dialog #form_widget_'+formid+'_div').on("submitSuccess",function(){
				$.post("{url}{lang}/api/system/widget/popupleadform/geturl",{'id':$(e).attr("data-id")},function(data){
					if (data.success) {
						if (data.data != '') {
							$("#popupleadform_dialog .modal-body #form_widget_"+formid+"_div").after("<h1>Please wait while you are being redirected...</h1>");
							window.location = data.data;
						}
					}else {
						$("#popupleadform_dialog .modal-body #form_widget_"+formid+"_div").after("<h1>Something went wrong. We apologize for the inconvenience. Please try again later.</h1>");
					}
				});
			});
			return this;
		}
	};
	$.fn.popupLeadForm = function(method) { 
		if (methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.popupLeadForm' );
		}    
	};	
})(jQuery);
 
 /*
  * Lazy loading snippets
  */
$( document ).ready(function() {
	$(".popupleadform").popupLeadForm();
});