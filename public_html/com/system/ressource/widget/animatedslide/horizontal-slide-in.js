$(document).ready(function(){
    $('section.widget.animatedslide.horizontal-slide-in').each(function(){
        $this = $(this);
        $this.data('inview',new Waypoint.Inview({
            element: $this,
            entered: function(direction) {
                var els = $this.children('.ct');
                els.children().removeClass("animated slideInLeft fadeIn slideOutRight");
                els.children("h1, h2").addClass("animated slideInLeft");
                els.children(".img, .text, .actions").addClass("animated fadeIn");
            },
            exited: function(direction) {
                var els = $this.children('.ct').children().removeClass("animated slideInLeft fadeIn slideOutRight").addClass("animated slideOutRight");
            }
        })); 
    });
});
