<li id="comment-id-{:$comment.comment_id:}" class="comment level{:$comment.comment_level:} id{:$comment.comment_id:} parent{:$comment.comment_parent:}" data-id="{:$comment.comment_id:}" data-parent="{:$comment.comment_parent:}">
    <header>
        {:if $comment.comment_index > 0:}<div class="index">{:$comment.comment_index:}</div>{:/if:}
        <div class="col">
            <img class="avatar" src="{:if $comment.avatar:}{:$comment.avatar:}{:/if:}" width="{:$thumbsize[0]:}" height="{:$thumbsize[1]:}">
        </div>
        <div class="col">
            <h1 class="username" data-uid="{:$comment.comment_userid:}">{:$comment.user_name:}</h1>
            <time datetime="{:$comment.comment_created_machine:}">{:$comment.comment_created_text:}</time>
        </div>
    </header>
    <div class="body">
        <div class="content hideContent">
            {:$comment.comment_content:}
            {:foreach item=a from=$comment.comment_attachments:}
                {:if $a.url != '' && $a.isimage:}<a href="{:$a.url:}" target="_blank"><img src="{:$a.url:}"></a>{:/if:}
            {:/foreach:}
        </div>
        <div class="show-more"><a href="#" data-moretxt="{:$smarty.const._SHOWMORE:}" data-lesstxt="{:$smarty.const._SHOWLESS:}">{:$smarty.const._SHOWMORE:}</a></div>
        <div class="attachments">
        {:foreach item=a from=$comment.comment_attachments:}
            {:if $a.url != '' && $a.isimage == false:}<a href="{:$a.url:}" target="_blank"><i class="glyphicon glyphicon-paperclip"></i>{:$a.name:}</a>{:/if:}
        {:/foreach:}
        </div>
    </div>
    <footer>
        {:if $closed == 0:}
        <div class="actions">
            {:if $userid == $comment.comment_userid && $userid > 0 || $isadmin:}<button class="edit btn btn-primary btn-xs" data-id="{:$comment.comment_id:}" data-parent="{:$comment.comment_id:}"><i class="glyphicon glyphicon-pencil"></i>{:$smarty.const.SYSTEM_WIDGET_COMMENTS_EDIT:}</button>{:/if:}
            <button class="reply btn btn-primary btn-xs" data-id="{:$comment.comment_id:}"><i class="glyphicon glyphicon-share-alt flip"></i>{:$smarty.const.SYSTEM_WIDGET_COMMENTS_REPLY:}</button>
            {:if ($userid == $comment.comment_userid && $userid > 0) || $isadmin:}<button class="delete btn btn-primary btn-xs" data-id="{:$comment.comment_id:}"><i class="glyphicon glyphicon-trash"></i>{:$smarty.const._DELETE:}</button>{:/if:}
        </div>
        {:/if:}
    </footer>
</li>
