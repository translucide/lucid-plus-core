<article class="download {:$class:} {:$layout:}" id="download">
	<div class="content">
        <a href="{:$filelocation:}" target="_blank">
            <h1>{:$smarty.const.SYSTEM_DOWNLOAD_WAITWHILEREDIRECT:}</h1>
            <p class="countdown"></p>
            <p>{:$smarty.const.SYSTEM_DOWNLOAD_IFYOURENOTREDIRECTED:}</p>
        </a>
    </div>
</article>