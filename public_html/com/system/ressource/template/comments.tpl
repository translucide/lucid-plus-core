<div class="commentsnav {:if $pager == '' && $count == 0:}empty{:/if:}">{:$pager:} {:if $count > 0 :}<a class="clearhighlight">Reset Highlighter</a>{:/if:}</div>
<ul class="commentslist">
{:foreach item=comment from=$comments:}
    {:include file="$root/com/system/ressource/template/comment.tpl":}
{:/foreach:}
</ul>
<div class="commentsnav {:if $pager == '' && $count == 0:}empty{:/if:}">{:$pager:} {:if $count > 0 :}<a class="clearhighlight">Reset Highlighter</a>{:/if:}</div>

<div id="deleteconfirmdialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{:$smarty.const.SYSTEM_WIDGET_COMMENTS_DELETECONFIRM:}</h4>
            </div>
            <div class="modal-body">
                <p>{:$smarty.const.SYSTEM_WIDGET_COMMENTS_DELETECONFIRMMESSAGE:}</p>
            </div>
            <div class="modal-footer">
                <button type="button" id="dialog_cancel" class="btn btn-secondary" data-dismiss="modal">{:$smarty.const._CANCEL:}</button>
                <button type="button" id="dialog_ok" class="btn btn-primary">{:$smarty.const._DELETE:}</button>
            </div>
        </div>
    </div>
</div>