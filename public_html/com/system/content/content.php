<?php
/**
 * System content handler
 *
 * Handles system generic content type
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('Ressource')->get('core/form/converter/requesttoobject');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/content');

class ContentContent extends Content{
	
	/**
	 * Returns information about this content type
	 *
	 * @public
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'content',
			'title' => SYSTEM_CONTENT_TYPE_CONTENT,
			'description' => SYSTEM_CONTENT_TYPE_CONTENT_DESC,
			'icon' => 'file'
		));
	}

	/**
	 * Returns the edit form to modify a section parameters
	 */
	public function edit($obj,$form) {
		global $service;
		$store = new ContentStore();
		$store->setOption('ignorelangs',true);
		$defaultlang = $service->get('Language')->getDefault();

		if (count($obj) == 0 || !is_array($obj)) return $form;
		$defobj = $store->getDefaultObj($obj);
		
		$form->setVar('title',_EDIT.' '.$defobj->getVar('content_title'));
		$form->add(new HtmleditorFormField('content_content',$defobj->getVar('content_content'),array(
			'title'=>_CONTENT,
			'length'=>100,
			'tab'=>'basic',
			'lang'=>$defaultlang['code'],
            'configfile' => 'config-admin',
			'translations' => $form->getTranslations($obj,'content_content')
		)));
		return $form;
	}
	
	/**
	 * Renders the section visual elements
	 */
	public function render(){
		return $this->data->getVar('content_content');
	}
	
	/**
	 * Renders the section visual elements
	 */
	public function renderExerpt(){
		return $this->data->getVar('content_exerpt');
	}
}