<?php
/**
 * System content handler
 *
 * Handles system generic content type
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('Ressource')->get('core/display/form/converter/requesttoobject');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/custom');

class CustomContent extends Content{

	/**
	 * Returns information about this content type
	 *
	 * @public
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'custom',
			'title' => SYSTEM_CONTENT_TYPE_CUSTOM,
			'description' => SYSTEM_CONTENT_TYPE_CUSTOM_DESC,
			'icon' => 'html',
			'saveoptions' => array(
				'cssfiles','jsfiles'
			)
		));
	}

	public function getInfo($info=''){
		global $service;
		$request = $service->get('Request')->get();

		//Add any request field that has _value at its end to autosave all custom fields.
		foreach ($request as $k => $v) {
			if (substr($k,-1*strlen('_value'),strlen('_value')) == '_value') $this->infos['saveoptions'][] = $k;
		}
		if (isset($this->infos[$info])) return $this->infos[$info];
		return $this->infos;
	}

	/**
	 * Returns the edit form to modify a section parameters
	 */
	public function edit($obj,$form) {
		global $service;
		$store = new ContentStore();
		$store->setOption('ignorelangs',true);
		$defaultlang = $service->get('Language')->getDefault();
		if (count($obj) == 0 || !is_array($obj)) return $form;
		$defobj = $store->getDefaultObj($obj);
		$options = $defobj->getVar('content_options');

		$form->setVar('title',_EDIT.' '.$defobj->getVar('content_title'));

		//Add dynamic form fields specified in the template....
		if (count($defobj->getVar('content_options')['fields']) > 0) {
			$fields = $defobj->getvar('content_options')['fields'];
			foreach ($fields as $k => $v) {
				$cls = ucfirst(strtolower($v['type'])).'Formfield';
				if (class_exists($cls)) {
					if ($v['multilingual']) {
						$cls = new $cls($v['tag'].'_value',$defobj->getVar('content_options')[$v['tag'].'_value'],array(
							'title' => $v['title'],
							'tab' => 'basic',
							'lang' => $defaultlang['code'],
							'translations' => $form->getTranslations($obj,'content_options',$v['tag'].'_value')
						));
					}else {
						$cls = new $cls($v['tag'].'_value',$defobj->getVar('content_options')[$v['tag'].'_value'],array(
							'title' => $v['title'],
							'tab' => 'basic',
						));
					}
					$form->add($cls);
				}
			}
		}

		$apiurl = URL.$service->get('Language')->getCode().'/api/customcontent/content/custom/';
		$form->add(new ItemmanagerFormField('fields',(isset($options['fields']))?$options['fields']:'',array(
			'title'=>'Editable form fields',
			'tab'=>'advanced',
			'parent' => $defobj->getVar('content_objid'),
			'editlabel' => _EDIT,
			'deletelabel' => _DELETE,
			'duplicatelabel' => _DUPLICATE,
			'newlabel' => CUSTOMCONTENT_ADDFIELD,
			'list' => $apiurl.'list',
			'edit' => $apiurl.'edit',
			'duplicate' => $apiurl.'duplicate',
			'save' => $apiurl.'save',
			'delete' => $apiurl.'delete',
			'move' => $apiurl.'move',
		)));
		$form->add(new TextareaFormField('content_content',str_replace('</textarea>','&lt;/textarea>',$defobj->getVar('content_content')),array(
			'title'=>'HTML',
			'tab'=>'advanced',
		)));
		$form->add(new TextareaFormField('cssfiles',(isset($options['cssfiles']))?$options['cssfiles']:'',array(
			'title'=>'CSS',
			'tab'=>'advanced',
			'ẁidth' => 6
		)));
		$form->add(new TextareaFormField('jsfiles',(isset($options['jsfiles']))?$options['jsfiles']:'',array(
			'title'=>'JS Files',
			'tab'=>'advanced',
			'ẁidth' => 6
		)));
		return $form;
	}

	/**
	 * Renders the section visual elements
	 */
	public function render(){
		global $service;

		$fields = $this->data->getVar('content_options')['fields'];
		$cssfiles = explode("\n",$this->data->getVar('content_options')['cssfiles']);
		foreach ($cssfiles as $v) {
			$service->get('Ressource')->get($v);
		}
		$jsfiles = explode("\n",$this->data->getVar('content_options')['jsfiles']);
		foreach ($jsfiles as $v) {
			$service->get('Ressource')->get($v);
		}
		$content = $this->data->getVar('content_content');
		foreach($fields as $k => $v){
			$content = str_replace('{'.$v['tag'].'}',$this->data->getVar('content_options')[$v['tag'].'_value'],$content);
		}
		$content = str_replace('™','<span class="tm">™</span>',$content);
		$content = str_replace('®','<span class="reg">®</span>',$content);
		$content = str_replace('©','<span class="copy">©</span>',$content);
		$content = str_replace('{url}',URL,$content);
		$content = str_replace('{uploadurl}',UPLOADURL,$content);
		$content = str_replace('{themeurl}',str_replace(THEMEROOT,THEMEURL,$service->get('Ressource')->getThemePath(($service->get('Url')->isAdmin())?true:false)),$content);
		return $content;
	}

	/**
	 * Renders the section visual elements
	 */
	public function renderExerpt(){
		return $this->data->getVar('content_exerpt');
	}

	public function apiCall($op){
		global $service;
		$request = $service->get('Request')->get();
		$store = new ContentStore();
		$store->setOption('ignorelangs',true);
		$parent = $request['parent'];
		$id = $request['id'];
		$obj = $store->getByObjId($parent);
		$defaultlang = $service->get('Language')->getDefault();
		$defobj = $store->getDefaultObj($obj);
		$langs = $service->get('Language')->getCodes();

		if (count($obj) == 0 || !is_object($obj[0]) || (isset($request['parent']) && $parent == 0)) {
			$ret = array('success'=>0,'data'=>array(), 'message' => CUSTOMCONTENT_SAVEFIRST);
			return $ret;
		}

		switch($op){
			case 'list' :{
				$response = array();
				foreach($defobj->getVar('content_options')['fields'] as $k => $v) {
					$response[] = array('title'=> $v['title'].' ('.$v['tag'].')','id' => $k);
				}
				$ret = array('success'=>1,'data'=>$response);
			}break;
			case 'edit' :{
				$service->get('Ressource')->get('core/display/form');
				$service->get('Ressource')->get('core/display/field');
				$form = new Form('', 'fieldsform', 'Edit Field', $method='POST');
				$form->setVar('formtag',false);
				if ($request['id']) {
					$field = $defobj->getVar('content_options')['fields'][$id];
				}else {
					$field['title'] = '';
					$field['type'] = '';
					$field['multilingual'] = '';
					$field['options'] = '';
					$field['tag'] = '';
					$field['id'] = count($defobj->getVar('content_options')['fields'])+1;
				}
				$form->add(new HiddenFormField('id',($id == 0)?count($defobj->getVar('content_options')['fields'])+1:$id));
				$form->add(new TextFormField('fieldtitle',$field['title'],array(
					'title'=>_TITLE,
					'length'=>255,
					'lang'=>$defaultlang['code'],
					'translations' => $this->getFieldTranslation($obj,'title')
				)));
				$form->add(new TextFormField('fieldtype',$field['type'],array(
					'title'=>CUSTOMCONTENT_FIELD_TYPE,
					'length'=>255
				)));
				$form->add(new TextFormField('fieldtag',$field['tag'],array(
					'title'=>CUSTOMCONTENT_FIELD_TAG,
					'length'=>255
				)));
				$form->add(new YesnoFormField('fieldmultilingual',$field['multilingual'],array(
					'title'=>CUSTOMCONTENT_FIELD_MULTILINGUAL,
					'length'=>255
				)));
				$form->add(new TextareaFormField('fieldoptions',$field['options'],array(
					'title'=>CUSTOMCONTENT_FIELD_OPTIONS,
					'lang'=>$defaultlang['code'],
					'translations' => $this->getFieldTranslation($obj,'options')
				)));
				$form->addSaveButton('save',_SAVE);
				$form->setVar('title',($field['title'] == '')?CUSTOMCONTENT_ADDFIELD:$field['title']);
				$ret = array('success'=>1,'form'=>$form->renderArray());
			}break;
			case 'duplicate' :{
				foreach($obj as $k => $v) {
					$fields = $v->getVar('content_options')['fields'];
					$fields[] = $fields[$id];
					$opt = $obj[$k]->getVar('content_options');
					$opt['fields'] = $fields;
					$obj[$k]->setVar('content_options',$opt);
				}
				$store->save($obj);
				$ret = array('success'=>1);
			}break;
			case 'save' :{
				foreach ($langs as $kl => $vl) {
					foreach ($obj as $k => $v) {
						if ($v->getVar('content_language') == $kl) {
							$opt = $v->getVar('content_options');
							$itemid = $id;
							if ($id == 0) $itemid = count($opt['fields'])+1;
							if ($defaultlang['code'] == $vl) {
								$opt['fields'][$itemid]['title'] = $request['fieldtitle'];
								$opt['fields'][$itemid]['type'] = $request['fieldtype'];
								$opt['fields'][$itemid]['options'] = $request['fieldoptions'];
								$opt['fields'][$itemid]['tag'] = $request['fieldtag'];
								$opt['fields'][$itemid]['multilingual'] = $request['fieldmultilingual'];
							}else{
								$opt['fields'][$itemid]['title'] = $request['fieldtitle_'.$vl['code']];
								$opt['fields'][$itemid]['type'] = $request['fieldtype'];
								$opt['fields'][$itemid]['tag'] = $request['fieldtag'];
								$opt['fields'][$itemid]['options'] = $request['fieldoptions_'.$vl['code']];
								$opt['fields'][$itemid]['multilingual'] = $request['fieldmultilingual'];
							}
							$obj[$k]->setVar('content_options',$opt);
						}
					}
				}
				$store->save($obj);
				$ret = array('success'=>1, 'message' => _SAVED);
			}break;
			case 'delete' :{
				foreach($obj as $k => $v) {
					$opt = $obj[$k]->getVar('content_options');
					$fields = $opt['fields'];
					unset($fields[$id]);
					$opt['fields'] = $fields;
					$obj[$k]->setVar('content_options',$opt);
				}
				$store->save($obj);
				$ret = array('success'=>1);
			}break;
			case 'move' :{
				$resp = false;
				$offset = intval($request['offset']);
				foreach($obj as $k => $v){
					$fields = $obj[$k]->getVar('content_options')['fields'];
					if ($offset == 1 || $offset == -1) {
						if ($offset == 1)   {
							$i1 = $fields[$id];
							$i2 = $fields[$id+1];
							$fields[$id] = $i2;
							$fields[$id+1] = $i1;
						}
						if ($offset == 1)   {
							$i1 = $fields[$id];
							$i2 = $fields[$id-1];
							$fields[$id] = $i2;
							$fields[$id-1] = $i1;
						}
						$opt = $obj[$k]->getVar('content_options');
						$opt['fields'] = $fields;
						$obj[$k]->setVar('content_options',$opt);
					}
				}
				$store->save($obj);
				$ret = array('success'=>1, 'message' => _SAVED);
			}break;
		}
		return $ret;
	}

	function getFieldTranslation($obj,$f) {
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$langs = $service->get('Language')->getCodes();
		$ret = array();
		foreach ($langs as $k => $v) {
			foreach ($obj as $ko => $vo) {
				if ($vo->getVar('content_language') == $k && $k != $defaultlang['id']) {
					$ret[] = array('lang' => $v, 'value' => $vo->getVar('content_options')['fields'][$f]);
				}
			}
		}
		return $ret;
	}

	public function hasAccess($op){
		global $service;
		if ($service->get('User')->isAdmin()) return true;
		return true;
	}
}
?>
