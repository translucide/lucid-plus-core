<?php
/**
 * Link content handler
 *
 * Links to another content object
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('Ressource')->get('core/form/converter/requesttoobject');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/link');

class LinkContent extends Content{
	
	/**
	 * Returns information about this content type
	 *
	 * @public
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'link',
			'title' => SYSTEM_CONTENT_LINK_TITLE,
			'description' => SYSTEM_CONTENT_LINK_TITLE_DSC,
			'icon' => 'file',
			'saveoptions' => array(
				'linktarget'
			)
		));
	}

	/**
	 * Returns the edit form to modify a section parameters
	 */
	public function edit($obj,$form) {
		global $service;
		$store = new ContentStore();
		$store->setOption('ignorelangs',true);
		$defaultlang = $service->get('Language')->getDefault();

		if (count($obj) == 0 || !is_array($obj)) return $form;
		$defobj = $store->getDefaultObj($obj);
		
		$form->setVar('title',_EDIT.' '.$defobj->getVar('content_title'));
		$form->add(new TextFormField('linktarget',$defobj->getVar('content_options')['linktarget'],array(
			'title'=>_LINK,
			'length'=>100,
			'tab'=>'basic',
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($obj,$defobj->getVar('content_options'),'linktarget')
		)));
		return $form;
	}
	
	/**
	 * Renders the section visual elements
	 */
	public function render(){
        global $service;
        $store = new ContentStore();
        $contentStore = new ContentStore();
        $contentStore->setOption('ignorelangs',1);
		$link = $this->data->getVar('content_options')['linktarget'];
		if (substr(URL,0,2) == '//') $link = str_replace(array('http://','https://'),'//',$link);
		$link = str_replace(URL,'',$link);
        $crit = new CriteriaCompo(new Criteria('content_urlpath',str_replace(URL,'',$link)),'AND');
        $crit = $contentStore->addGroupCriteria($crit);
        
        $crit2 = new CriteriaCompo();
        $crit2->add(new Criteria('content_draft',0),'OR');
        $crit2 = $contentStore->addGroupCriteria($crit2,array(1),'OR');
        $crit->add($crit2);
        $obj = $contentStore->get($crit);
        $code = '';
        foreach ($obj as $k => $v) {
            $ctObj = $service->get('Content')->getContentObject($v->getVar('content_type'),$v);
            if (is_object($ctObj)) {
                $code .= $ctObj->render();
            }
        }
		return $code;
	}
	
	/**
	 * Renders the section visual elements
	 */
	public function renderExerpt(){
		return $this->data->getVar('content_exerpt');
	}
}