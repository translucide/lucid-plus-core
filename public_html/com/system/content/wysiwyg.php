<?php
/**
 * WYSIWYG content handler
 *
 * Handles system generic content type
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('core/display/form/field');
$service->get('Ressource')->get('core/display/form/converter/objecttoform');
$service->get('Ressource')->get('core/display/form/converter/requesttoobject');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/wysiwyg');

class WysiwygContent extends Content{

	/**
	 * Returns information about this content type
	 *
	 * @public
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'wysiwyg',
			'title' => SYSTEM_CONTENT_TYPE_WYSIWYG,
			'description' => SYSTEM_CONTENT_TYPE_WYSIWYG_DESC,
			'icon' => 'wysiwyg',
			'saveoptions' => array(
				'content_content_ressources'
			),
            'overrides' => array(
                'extracss'
            )
		));
	}

	public function getInfo($info=''){
		global $service;
		$request = $service->get('Request')->get();

		//Add any request field that has _value at its end to autosave all custom fields.
		foreach ($request as $k => $v) {
			if (substr($k,-1*strlen('_value'),strlen('_value')) == '_value') $this->infos['saveoptions'][] = $k;
		}
		if (isset($this->infos[$info])) return $this->infos[$info];
		return $this->infos;
	}

	/**
	 * Returns the edit form to modify a section parameters
	 */
	public function edit($obj,$form) {
		global $service;
		$service->get('Ressource')->clear();
		$store = new ContentStore();
		$store->setOption('ignorelangs',true);
		$defaultlang = $service->get('Language')->getDefault();
		if (count($obj) == 0 || !is_array($obj)) return $form;
		$defobj = $store->getDefaultObj($obj);
		$options = $defobj->getVar('content_options');
		$form->setVar('title',_EDIT.' '.$defobj->getVar('content_title'));
		$ress = $form->getByLang($obj,'content_options','content_content_ressources');
		foreach ($ress as $k => $langress) {
			$ress[$k] = json_decode($langress,true);
			if ($ress[$k] == null) $ress[$k] = array();
			foreach ($ress[$k] as $r) {
				if ($r['priority'] != RESSOURCE_SCRIPT) $service->get('Ressource')->get($r['ressource']);
                else {
                    if ($r['type'] == RESSOURCE_CSS) $service->get('Ressource')->addStyle($r['ressource']);
                    if ($r['type'] == RESSOURCE_JS) $service->get('Ressource')->addScript($r['ressource']);
                }
			}
		}
		$form->add(new LucideditorFormField('content_content',$defobj->getVar('content_content'),array(
			'title'=>_CONTENT,
			'tab'=>'basic',
            'lang'=>$defaultlang['code'],
			'url' => URL.$service->get('Language')->getCode().'/api/system/content/wysiwyg/loaddialog',
			'renderurl' => URL.'{lang}/api/system/content/wysiwyg/render',
			'saveurl' => URL.$service->get('Language')->getCode().'/api/system/content/wysiwyg/save',
			'removeurl' => URL.$service->get('Language')->getCode().'/api/system/content/wysiwyg/remove',
			'stylesheet' => $service->get('Ressource')->getThemeUrl().'css/gridmanager',
			'translations' => $form->getTranslations($obj,'content_content'),
			'ressources' => $ress
		)));
		return $form;
	}

	/**
	 * Renders the section visual elements
	 */
	public function render(){
		global $service;

		//Add JS & CSS pushed by widgets...
		$ressources = json_decode($this->data->getVar('content_options')['content_content_ressources'],true);
		foreach ($ressources as $k => $v) {
            if ($v['priority'] != RESSOURCE_SCRIPT) $service->get('Ressource')->get($v['ressource']);
            else {
                if ($v['type'] == RESSOURCE_CSS) $service->get('Ressource')->addStyle($v['ressource']);
                if ($v['type'] == RESSOURCE_JS) $service->get('Ressource')->addScript($v['ressource']);
            }
		}
		$content = $this->data->getVar('content_content');
		return $content;
	}

	/**
	 * Renders the section visual elements
	 */
	public function renderExerpt(){
		return $this->data->getVar('content_exerpt');
	}


	public function apiCall($op){
		global $service;
		$request = $service->get('Request')->get();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);

		$type = strtolower(trim($request['widget']));
		$id = $request['id'];
		$defaultlang = $service->get('Language')->getDefault();

		switch($op){
			case 'loaddialog': {
				switch($request['dialog']) {
					case 'list': {
						//Generate a list of all widget providers / handlers
						$infos = $service->get('Ressource')->getInfos('widget');
						foreach ($infos as $k => $v) {
							if ($v['type'] != 'block') {
								unset($infos[$k]);
							}
						}
						$infos = array_values($infos);
						$content = '<input id="widget" type="hidden" name="widget">';
						$content .= '<div class="lucideditor_widgetlist_wrapper">';
						foreach($infos as $k => $v) {
							$content .= '<div class="lucideditor_widgetlist_element" onclick="$(this).parent().parent().find(\'.lucideditor_widgetlist_element\').removeClass(\'selected\');$(this).addClass(\'selected\');$(this).parent().parent().children(\'#widget\').val(\''.$v['name'].'\')">';
							$content .= '	<img src="'.URL.'com/'.$v['component'].'/asset/wireframe/'.$v['wireframe'].'.jpg" alt="'.$v['title'].'"><h1>'.$v['title'].'</h1><p>'.$v['description'].'</p>';
							$content .= '</div>';
						}
						$content .= '</div>';
						$title = SYSTEM_CONTENT_TYPE_WYSIWYG_CHOOSEWIDGET;
					}break;
					case 'edit' : {
						$form = '';
						$widget = $service->get('Ressource')->getRessourceObject('widget',$type);
						$info = $widget->getInfo();
						if ($widget){
							if ($id == 0) {
								$id = $this->createNewWidget($type);
							}
							$obj = $store->getByObjId($id);
							$defobj = $store->getDefaultObj($obj);
							$form = new Form(URL.$service->get('Language')->getCode().'/api/system/content/wysiwyg/save',
								'widgetform', SYSTEM_WIDGETS_EDIT, $method='POST');
							$form->setUseCase('ajax');
							$form->setVar('formtag',1);
							$form->setTitle($defobj->getVar('widget_title'));

                            //Support tabs from block widget
							if (get_parent_class($widget) != 'Widget') {
								$contenttab='basic';
								$isblock = true;
								$form->setVar('tabs',array(
									array('id' => 'basic', 'title' => _CONTENT),
									array('id' => 'headerfooter', 'title' => _HEADERFOOTER),
									array('id' => 'actions', 'title' => _ACTIONS),
									array('id' => 'style', 'title' => _STYLE),
									array('id' => 'advanced', 'title' => _ADVANCEDOPTIONS),
									array('id' => 'unused', 'title' => _UNUSED)
								));
							}
							else {
								$contenttab='basic';
								$isblock = false;
								$form->setVar('tabs',array(
									array('id' => 'basic', 'title' => _BASIC),
									array('id' => 'advanced', 'title' => _ADVANCED),
								));
							}
							$form->add(new HiddenFormField('widget_id',$defobj->getVar('widget_id'),array(
								'tab'=>$contenttab,
								'lang'=>$defaultlang['code'],
								'translations' => $form->getTranslations($obj,'widget_id')
							)));
							$form->add(new HiddenFormField('widget_objid',$defobj->getVar('widget_objid'),array(
								'tab'=>$contenttab,
								'lang'=>$defaultlang['code'],
								'translations' => $form->getTranslations($obj,'widget_objid')
							)));
							$form->add(new HiddenFormField('widget_zone',$defobj->getVar('widget_zone'),array(
								'tab'=>$contenttab,
								'lang'=>$defaultlang['code'],
								'translations' => $form->getTranslations($obj,'widget_zone')
							)));
							$form->add(new HiddenFormField('widget_position',$defobj->getVar('widget_position'),array(
								'tab'=> $contenttab,
								'lang'=>$defaultlang['code'],
								'translations' => $form->getTranslations($obj,'widget_position')
							)));
							$form->add(new HiddenFormField('widget_type',$info['type'],array(
								'tab'=> $contenttab
							)));
							$form->add(new HiddenFormField('widget_language',$defobj->getVar('widget_language'),array(
								'tab'=> $contenttab,
								'lang'=>$defaultlang['code'],
								'translations' => $form->getTranslations($obj,'widget_language')
							)));
							$form->add(new TextFormField('widget_title',$defobj->getVar('widget_title'),array(
								'tab'=> $contenttab,
								'title' => _TITLE,
								'width' => 10,
								'lang'=>$defaultlang['code'],
								'translations' => $form->getTranslations($obj,'widget_title')
							)));
                            /*
							$form->add(new SelectwidgetFormField('widget_name',$defobj->getVar('widget_name'),array(
								'tab'=> $contenttab,
								'title' => _TYPE,
								'width' => 2,
							)));
                            */
                            $form->add(new HiddenFormField('widget_name',$defobj->getVar('widget_name'),array(
								'tab'=> $contenttab
							)));
							$form->add(new YesnoFormField('displaywidgettitle',$defobj->getVar('widget_options')['displaywidgettitle'],array(
								'tab'=> $contenttab,
								'width' => 2,
								'title' => _DISPLAYTITLE
							)));
							$form->add(new SelectusergroupFormField('widget_groups',$defobj->getVar('widget_groups'),array(
								'tab'=>'advanced',
								'title'=>_VISIBILITY,
								'length'=>255,
								'width' => 5,
								'multiple' => 1
							)));
							$form->add(new YesnoFormField('usecache',$defobj->getVar('widget_options')['usecache'],array(
								'tab'=>'advanced',
								'title'=>_CACHE,
								'length'=>1,
								'width' => 1,
							)));
							$form->add(new TextFormField('cacheduration',$defobj->getVar('widget_options')['cacheduration'],array(
								'tab'=>'advanced',
								'title'=>_CACHEDURATION,
								'length'=>4,
								'width' => 2,
								'placeholder' => _CACHEDURATION_PLACEHOLDER,
							)));

                            //Generate a class for the widget by default.
                            //Will be used for rendering custom inline CSS per individual widget
                            $cls = uniqid('cls_');
                            if ($defobj->getVar('widget_options')['widgetclasses']) $cls = $defobj->getVar('widget_options')['widgetclasses'];
							$form->add(new TextFormField('widgetclasses',$cls,array(
								'tab'=>'advanced',
								'title'=>_CLASS,
								'length'=>4,
								'width' => 2
							)));

							$form->add(new TextFormField('widgetidattr',$defobj->getVar('widget_options')['widgetidattr'],array(
								'tab'=>'advanced',
								'width' => 2,
								'title' => 'ID',
							)));

							$form->add(new SelectstyleFormField('stylesheet',$defobj->getVar('widget_options')['stylesheet'],array(
								'tab'=> ($isblock)?'style':'advanced',
								'title' => _STYLE,
								'width' => ($isblock)?2:4,
								'component' => $defobj->getVar('widget_component'),
								'type' => 'widget',
								'name' => $type
							)));

							$form->add(new HiddenFormField('widget_site',$defobj->getVar('widget_site')));
							$form->add(new HiddenFormField('widgettype',$type));
							$form = $widget->edit($obj,$form);
							$form->setVar('title','<span><img src="'.URL.$service->get('Ressource')->getIcon($info).'"></span>'.$form->getVar('title'));
							//$form->addSaveButton('save',_SAVE);
							$content = "<div id='widgetform_div'></div>".$form->render();
							$title = SYSTEM_CONTENT_TYPE_WYSIWYG_EDITWIDGET;
						}
					}break;
				}
				echo $this->loadDialog($title,$content);
				die;
			}break;
			case 'save' : {
				$id = $request['widget_objid'];
				$type = $request['widgettype'];
				$widget = $service->get('Ressource')->getRessourceObject('widget',$type);
				$saveRequest = new RequestToObject('widget');
				$saveRequest->addOptions(array_merge(array(
					//Base fields:
					'usecache','cacheduration','widgetclasses','widgetidattr','stylesheet','displaywidgettitle'
					),$widget->getInfo('saveoptions'))
				);
				$r1 = $saveRequest->save();
				$r2 = $widget->save();
				if ($r1 !== false && $r2 !== false) $ret = array('success'=>1, 'message' => _SAVED);
				else $ret = array('success' => 0, 'message' => 'Error while saving!');
			}break;
			case 'remove' : {
				$id = $request['id'];
				$r = $store->delete($id);
				if ($r != false) $ret = array('success'=>1, 'message' => _SAVED);
				else $ret = array('success' => 0, 'message' => 'Error while saving!');
			}break;
			case 'render': {
				global $service;
				$groups = implode('-',$service->get('User')->getGroups());
				$site = $service->get('Site')->getId();
				$lang = $service->get('Language')->getId();
				$store->setOption('ignorelangs',false); // Render only one language
				$obj = $store->getByObjId($id);
				$data = $store->getDefaultObj($obj);
				if (!is_object($data)) return array('success' => 0, 'message' => 'Widget not found.');
				if ($data) {
					$widget = $service->get('Ressource')->getRessourceObject('widget',$data->getVar('widget_name'));
					if (!is_object($widget)) return array('success' => 0, 'message' => 'Widget not found.');
					$widget->setData($data);
					$info = $widget->getInfo();
					$expires = intval($data->getVar('widget_options')['cacheduration']);
					if (intval($data->getVar('widget_options')['usecache']) != 1) $expires = 0;
					$cachefilename = DATAROOT.'/cache/widget/'.$info['name'].'_'.md5($site.'-'.$lang.'-'.$groups.'-'.$data->getVar('widget_objid'));
					$service->get('Ressource')->clear();
					$code = $widget->render();
					if ($expires > 0) {
						file_put_contents($cachefilename,$code);
					}
					return array(
						'ressources' => true,
						'type' => $info['type'],
						'name' => $info['name'],
						'id' => $request['id'],
						'src' => $code,
						'title' => ($data->getVar('widget_options')['displaywidgettitle'] == 1)?$data->getVar('widget_title'):''
					);
				}
			}break;
		}
		return $ret;
	}

	function getFieldTranslation($obj,$f) {
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$langs = $service->get('Language')->getCodes();
		$ret = array();
		foreach ($langs as $k => $v) {
			foreach ($obj as $ko => $vo) {
				if ($vo->getVar('content_language') == $k && $k != $defaultlang['id']) {
					$ret[] = array('lang' => $v, 'value' => $vo->getVar('content_options')['fields'][$f]);
				}
			}
		}
		return $ret;
	}

	function loadDialog($title,$content){
		return <<<EOD
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title">$title</h4>
		</div>
		<div class="modal-body">
			$content
		</div>
		<div class="modal-footer">
			<button type="cancel" id="lucideditor_dialog_cancel" data-dismiss="modal" class="btn btn-default btn-default"><span class="glyphicon glyphicon-error"></span> Cancel</button>
			<button type="submit" id="lucideditor_dialog_ok" class="btn btn-default btn-primary"><span class="glyphicon glyphicon-floppy-open"></span> Save</button>
		</div>
EOD;
	}

	public function createNewWidget($type){
		global $service;
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$zone = 'wysiwyg_content';
		$content = $service->get('Ressource')->getRessourceObject('widget',$type);
		$info = $content->getInfo();
		$objs = $store->createMultilingual();
		$objid = 0;
		foreach($objs as $k => $v){
			$v->setVar('widget_component',$service->get('Ressource')->findRessourceComponent('widget',$info['name']));
			$v->setVar('widget_zone','wysiwyg_content');
			$v->setVar('widget_title',ucfirst($type));
			$v->setVar('widget_name',$info['name']);
			$v->setVar('widget_type',$info['type']);
			$v->setVar('widget_groups',array(
                $service->get('Group')->id('admin'),
                $service->get('Group')->id('user'),
                $service->get('Group')->id('anonymous')
            ));
			$objs[$k] = $v;
			$objid = $v->getVar('widget_objid');
		}
		if ($store->save($objs)) return $objid;
		return false;
	}

	public function hasAccess($op){
		global $service;
		if ($service->get('User')->isAdmin()) return true;
		return false;
	}
}
?>
