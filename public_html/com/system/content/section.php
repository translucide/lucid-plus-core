<?php
/**
 * System Sections content handler
 *
 * Handles system sections
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
global $service;
$service->get('Content');
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('core/display/form/field');
$service->get('Ressource')->get('core/display/form/converter/objecttoform');
$service->get('Ressource')->get('core/display/form/converter/requesttoobject');
$service->get('Ressource')->get('core/form/converter/requesttoobject');
$service->get('Ressource')->get('com/system/editor/lucideditor');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/section');

class SectionContent extends Content{

	/**
	 * Returns information about this content type
	 *
	 * @public
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'section',
			'title' => SYSTEM_SECTION_TYPE,
			'description' => SYSTEM_SECTION_TYPE_DESC,
			'icon' => 'folder',
			'saveoptions' => array(
				'footer','header','headeronly','mainleveloptions','subleveloptions','levels','displaytitle','itemsperpage','mainthumbsize','subthumbsize','width','header_ressources','footer_ressources'
			)
		));
	}

	/**
	 * Returns the edit form to modify section parameters
	 */
	public function edit($obj,$form) {
		global $service;
		$store = new ContentStore();
		$store->setOption('ignorelangs',true);
		$editor = new LucideditorEditor();
		$editor->option('multilingual',true);
		$defaultlang = $service->get('Language')->getDefault();
		if (count($obj) == 0 || !is_array($obj)) return $form;
		$defobj = $store->getDefaultObj($obj);
		$options = $defobj->getVar('content_options');

		//Clear previous ressources so the header_ressources field will not contain all lucid+ ressources
		$service->get('Ressource')->clear();
        $ress = $editor->loadRessources($form->getByLang($obj,'content_options','header_ressources'));
        $ress2 = $editor->loadRessources($form->getByLang($obj,'content_options','footer_ressources'));

		$form->setVar('title',_EDIT.' '.$defobj->getVar('content_title'));
		$form->add(new LucideditorFormField('header',$defobj->getVar('content_options')['header'],array(
			'title'=>_HEADER,
			'tab'=>'basic',
            'lang'=> $service->get('Language')->getDefault()['code'],
			'translations' => $form->getTranslations($obj,'content_options','header'),
			'ressources' => $ress
		)));
		$form->add(new YesnoFormField('headeronly',$defobj->getVar('content_options')['headeronly'],array(
			'title'=>SYSTEM_SECTION_DISPLAYONLYHEADER,
			'tab'=>'basic',
		)));
		$form->add(new CheckboxFormField('mainleveloptions',$defobj->getVar('content_options')['mainleveloptions'],array(
			'title'=>SYSTEM_SECTION_FIRSTLEVELOPTIONS,
			'length'=>1,
			'tab'=>'basic',
			'options' => array(
				array('title' => SYSTEM_SECTION_FIRSTLEVELOPTIONS_TITLE, 'value' => 1),
				array('title' => SYSTEM_SECTION_FIRSTLEVELOPTIONS_HEADER, 'value' => 6),
				array('title' => SYSTEM_SECTION_FIRSTLEVELOPTIONS_THUMB, 'value' => 2),
				array('title' => SYSTEM_SECTION_FIRSTLEVELOPTIONS_EXERPT, 'value' => 3),
				array('title' => SYSTEM_SECTION_FIRSTLEVELOPTIONS_LINK, 'value' => 4),
				array('title' => SYSTEM_SECTION_FIRSTLEVELOPTIONS_LASTMODIF, 'value' => 5),
			),
			'width' => 6
		)));
		$form->add(new CheckboxFormField('subleveloptions',$defobj->getVar('content_options')['subleveloptions'],array(
			'title'=>SYSTEM_SECTION_SUBLEVELOPTIONS,
			'length'=>1,
			'tab'=>'basic',
			'options' => array(
				array('title' => SYSTEM_SECTION_SUBLEVELOPTIONS_TITLE, 'value' => 1),
				array('title' => SYSTEM_SECTION_SUBLEVELOPTIONS_HEADER, 'value' => 6),
				array('title' => SYSTEM_SECTION_SUBLEVELOPTIONS_THUMB, 'value' => 2),
				array('title' => SYSTEM_SECTION_SUBLEVELOPTIONS_EXERPT, 'value' => 3),
				array('title' => SYSTEM_SECTION_SUBLEVELOPTIONS_LINK, 'value' => 4),
				array('title' => SYSTEM_SECTION_SUBLEVELOPTIONS_LASTMODIF, 'value' => 5)
			),
			'width' => 6
		)));

		$form->add(new RadioFormField('mainthumbsize',$defobj->getVar('content_options')['mainthumbsize'],array(
			'title'=>SYSTEM_SECTION_MAINTHUMBSIZE,
			'length'=>1,
			'tab'=>'basic',
			'options' => array(
				array('title' => SYSTEM_SECTION_THUMBSIZE_TINY, 'value' => 1),
				array('title' => SYSTEM_SECTION_THUMBSIZE_SMALL, 'value' => 2),
				array('title' => SYSTEM_SECTION_THUMBSIZE_MEDIUM, 'value' => 3),
				array('title' => SYSTEM_SECTION_THUMBSIZE_LARGE, 'value' => 4)
			),
			'width' => 6
		)));
		$form->add(new RadioFormField('subthumbsize',$defobj->getVar('content_options')['subthumbsize'],array(
			'title'=>SYSTEM_SECTION_SUBTHUMBSIZE,
			'length'=>1,
			'tab'=>'basic',
			'options' => array(
				array('title' => SYSTEM_SECTION_THUMBSIZE_TINY, 'value' => 1),
				array('title' => SYSTEM_SECTION_THUMBSIZE_SMALL, 'value' => 2),
				array('title' => SYSTEM_SECTION_THUMBSIZE_MEDIUM, 'value' => 3),
				array('title' => SYSTEM_SECTION_THUMBSIZE_LARGE, 'value' => 4)
			),
			'width' => 6
		)));
		$form->add(new TextFormField('levels',$defobj->getVar('content_options')['levels'],array(
			'title'=>SYSTEM_SECTION_NBLEVELS,
			'length'=>3,
			'tab'=>'basic',
			'help' => SYSTEM_SECTION_NBLEVELS_DESC,
			'width' => 6
		)));
		$form->add(new TextFormField('itemsperpage',$defobj->getVar('content_options')['itemsperpage'],array(
			'title'=>SYSTEM_SECTION_ITEMSPERPAGE,
			'length'=>1,
			'tab'=>'basic',
			'help' => SYSTEM_SECTION_ITEMSPERPAGE_DESC,
			'width' => 6
		)));
		$form->add(new SelectcellwidthFormField('width',$defobj->getVar('content_options')['width'],array(
			'tab'=>'basic',
			'title'=>_WIDTH,
			'width' => 2
		)));
		$form->add(new LucideditorFormField('footer',$defobj->getVar('content_options')['footer'],array(
			'title'=>_FOOTER,
			'tab'=>'basic',
            'lang'=> $service->get('Language')->getDefault()['code'],
			'translations' => $form->getTranslations($obj,'content_options','footer'),
			'ressources' => $ress2
		)));
		return $form;
	}

	/**
	 * Renders the section visual elements
	 */
	public function render(){
		global $service;
		$service->get('Ressource')->get('core/display/pager');
        $service->get('Ressource')->get('lib/lucidcore/0.1/lucidcore');
		$service->get('Ressource')->addScript('
			$("article.section article h1").matchHeight();
			$("article.section article p.exerpt").matchHeight();
		');

		$opt = $this->data->getVar('content_options');
        $itemsperpage = (isset($opt['itemsperpage']))?$opt['itemsperpage']:10;
        $levels = (isset($opt['levels']))?$opt['levels']:0;

		//Add JS & CSS pushed by widgets...
		if (isset($opt['header_ressources'])) {
            $lucidEditor = new LucideditorEditor();
            $lucidEditor->loadRessources(array($opt['header_ressources']));
		}
		//Add JS & CSS pushed by widgets...
		if (isset($opt['footer_ressources'])) {
            $lucidEditor = new LucideditorEditor();
            $lucidEditor->loadRessources(array($opt['footer_ressources']));
		}
        echo '<div class="footer_ressources hidden">'.$opt['footer_ressources'].'</div>';
		$code = '';
        if (isset($opt['header'])) $code .= $opt['header'];
		if (isset($opt['headeronly']) && $opt['headeronly'] == 1) return $code;

		$request = $service->get('Request')->get();
		if (!isset($request['page'])) $request['page'] = 1;

        $store = new ContentStore();
        $crit = new CriteriaCompo();
        $crit2 = new CriteriaCompo();

		//1. Build content level criteria
        $minlevel = $this->data->getVar('content_level') + 1;
        $maxlevel = $minlevel + $levels;

        //2. Build allowed content paths criteria
        $crit->add(new Criteria('content_idpath',(($minlevel > 2)?'%/':'').$this->data->getVar('content_objid').'/%','LIKE'));
        $crit2->add(new Criteria('content_idpath',(($minlevel > 2)?'%/':'').$this->data->getVar('content_objid').'/%','LIKE'));

        //3. Add permissions selection
        $crit = $store->addGroupCriteria($crit);
        $crit2 = $store->addGroupCriteria($crit2);

        //4. Add draft check
        $crit->add(new Criteria('content_draft',0));
        $crit2->add(new Criteria('content_draft',0));

        //5. Sorting.
        $crit->setSort('content_position','ASC');
        $crit2->setSort('content_position','ASC');

		//6. Finish building content level crit
        $crit->add(new Criteria('content_level',$minlevel));
		$count = $store->count($crit);
		$crit->setStart(($request['page']-1) * $itemsperpage);
		$crit->setLimit($itemsperpage);

        $level = new CriteriaCompo();
        $level->add(new Criteria('content_level',$minlevel,'>='),'AND');
        $level->add(new Criteria('content_level',$maxlevel,'<='),'AND');
		$crit2->add($level);

		//7. Add content path restriction for children.
		$firstlevel = $store->get($crit);
		if ($levels > 0){
			$idcrit = new CriteriaCompo();
			foreach ($firstlevel as $v) {
				$idcrit->add(new Criteria('content_idpath','%/'.$v->getVar('content_objid').'/%','LIKE'),'OR');
			}
			$crit2->add($idcrit);
			$firstlevel = array_merge($firstlevel,$store->get($crit2));
		}

        //6. Parse results.
        $objs = array_merge(array($this->data),$firstlevel);
		$pager = new Pager();
        $pager = $pager->render($count,$itemsperpage,$request['page'],
			URL.$this->data->getVar('content_urlpath').'?page={page}');
		$code .= $this->renderSection($objs,$this->data->getVar('content_parent'),$minlevel-1,$minlevel-1);
        $code .= $pager;
        if (isset($opt['footer'])) $code .= $opt['footer'];
        return $code;
	}

	/**
	 * Renders sub content / sub sections for a given section
	 *
	 * @private
	 *
	 * @param array $obj Content data to display recursively
	 * @param int $parent The parent of the items we are rendering
	 * @param int $level The level or recursion we are at, from the root of the website.
	 * @param int $baselevel The base level we started at. So $level - $baselevel = recursivity level
	 * @return string $html
	 */
	private function renderSection($obj,$parent,$level,$baselevel){
		$colscount = 0;
		$html = '';
        $opt = $this->data->getVar('content_options');
        $levels = (isset($opt['levels']))?$opt['levels']:0;
        $maxlevel = $this->data->getVar('content_level') + $levels + 1;


		foreach ($obj as $k => $v) {
            if ($v->getVar('content_level') == $level && $v->getVar('content_parent') == $parent) {
				if ($colscount == 0 && $level > $baselevel) {
					$html .= '<div class="row">';
				}
				$ctype = $v->getVar('content_type');
				if ($v->getVar('content_type') == 'section' && $level == $maxlevel) $ctype = null;
                //Render this item.
                switch($ctype) {
                    case 'section' : {
                        if ($level > $baselevel) {
							$html .= '<article id="section_'.$v->getVar('content_objid').'" class="section level'.min($level-$baselevel+1,6).' col-sm-'.$this->data->getVar('content_options')['width'].'">';
							$html .= '<h1>'.$v->getVar('content_title').'</h1>'; //min($level-$baselevel+1,6)
							if (trim(strip_tags($v->getVar('content_options')['header'])) != '') {
								if (
									($level == $baselevel + 1 && in_array(6,$this->data->getVar('content_options')['mainleveloptions']) === true) ||
									($level > $baselevel + 1 && in_array(6,$this->data->getVar('content_options')['subleveloptions']) === true)
								   ){
									$html .= '<header>'.$v->getVar('content_options')['header'].'</header>';
								}
							}
						}
                        $html .= '<section>';
                        $html .= $this->renderSection($obj,$v->getVar('content_objid'),$level+1,$baselevel);
                        $html .= '</section>';
						if (
							($level == $baselevel + 1 && in_array(6,$this->data->getVar('content_options')['mainleveloptions']) === true) ||
							($level > $baselevel + 1 && in_array(6,$this->data->getVar('content_options')['subleveloptions']) === true)
						   ){
							$html .= '<div class="actions"><a href="'.URL.$v->getVar('content_urlpath').'">'._DETAILS.'</a></div>';
						}
                        if ($level > $baselevel) $html .= '</article>';
                    }break;
                    default : {
                        $html .= '<article class="content level'.min($level-$baselevel+1,6).' col-sm-'.$this->data->getVar('content_options')['width'].'" id="content_'.$v->getVar('content_objid').'">';
						$html .= $this->renderContent($v,$level-$baselevel);
						$html .= '</article>';
                    }break;
                }
				if ($level > $baselevel) {
					$colscount++;
					if ($colscount >= (12/$this->data->getVar('content_options')['width'])) {
						$html .= '</div>';
						$colscount = 0;
					}
				}
            }
        }
		if ($colscount  > 0) {
			$html .= '</div>';
		}
		return $html;
	}

	/**
	 * Renders a content in respect to contentmode parameter
	 *
	 * @private
	 * @param ContentData $content
	 * @return string $html
	 */
	private function renderContent($content,$level){
		global $service;
		$service->get('Ressource')->get('core/display/thumbnail');
		$sizes = array('1' => 'tiny', '2' => 'small', '3' => 'medium', '4' => 'large');
		$type = ($level > 1)?'sub':'main';

		$thumb = new Thumbnail();
		$thumbsrc = dirname($content->getVar('content_thumb')).'/'.$thumb->changeSize(
			dirname($content->getVar('content_thumb')),
			array(basename($content->getVar('content_thumb'))),
			$sizes[$this->data->getVar('content_options')[$type.'thumbsize']]
		)[0];

		$html = '';
		$time = new Time();
		$renderer = $this->getContentRenderer($content->getvar('content_type'));
        $renderer->setData($content);

		$thumb = '<a href="'.URL.$content->getVar('content_urlpath').'" class="thumblink">'.'<img src="'.URL.$thumbsrc.'" class="thumb '.$sizes[$this->data->getVar('content_options')[$type.'thumbsize']].'">'.'</a>';
		$title = '<h1>'.'<a href="'.URL.$content->getVar('content_urlpath').'">'.$content->getVar('content_title').'</a>'.'</h1>';
		$exerpt = '<p class="exerpt">'.$renderer->renderExerpt().'</p>';
		$link = '<div class="actions"><a href="'.URL.$content->getVar('content_urlpath').'">'._DETAILS.'</a></div>';
		$lastmodif = '<time datetime="'.date('Y-m-d',$content->getVar('content_modified')).'">'.SYSTEM_SECTION_FIRSTLEVELOPTIONS_LASTMODIF.': '.$time->toElapsedTimeString($content->getVar('content_modified'),5).'</time>';

		if (in_array(1,$this->data->getVar('content_options')[$type.'leveloptions'])) $html .= $title;
		if (in_array(2,$this->data->getVar('content_options')[$type.'leveloptions'])) $html .= $thumb;
		if (in_array(3,$this->data->getVar('content_options')[$type.'leveloptions'])) $html .= $exerpt;
		if (in_array(5,$this->data->getVar('content_options')[$type.'leveloptions'])) $html .= $lastmodif;
		if (in_array(4,$this->data->getVar('content_options')[$type.'leveloptions'])) $html .= $link;
		return $html;
	}

	/**
	 * Renders the section visual elements
	 */
	public function renderExerpt(){
		return $this->data->getVar('content_exerpt');
	}

	/**
	 * Gets a content renderer based on its name
	 *
	 * @private
	 *
	 * @param string $contentname
	 * @return Content The content renderer object
	 */
	private function getContentRenderer($name) {
		global $service;
		$ctCls = ucfirst($name).'Content';
		$service->get('Ressource')->get($service->get('Ressource')->findRessource('content',strtolower($name)));
		if (!class_exists($ctCls)) {echo $ctCls;return null;}
		return new $ctCls();
	}

	public function hasAccess($op){
		global $service;
		if ($service->get('User')->isAdmin()) return true;
		return false;
	}
}
