<?php
/**
 * Folder Menu Item
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/display/menu');

class FolderMenu extends Menu{

	/**
	 * Returns information about this block type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'folder',
			'title' => SYSTEM_MENU_TYPE_FOLDER,
			'description' => SYSTEM_MENU_TYPE_FOLDER_DESC,
			'icon' => 'folder',
			'saveoptions' => array(
				'menu_summary','menu_columns','displaytitle','menu_icon'
			)
		));
	}

	public function render($options){
		global $service;
		$options = $this->data->getVar('menu_options');
		$title = '';
		if (isset($options['menu_icon']) && $options['menu_icon'] != '') {
			$title .= '<span class="glyphicon glyphicon-'.$options['menu_icon'].'"></span>';
		}
		if (isset($options['displaytitle']) && $options['displaytitle']) {
			$title .= $this->data->getVar('menu_title');
		}
		$code = '<li class="dropdown '.$this->data->getVar('menu_options')['screensizevisibility'];
		if (isset($options['menu_summary']) && $options['menu_summary'] != '') {
			$code .= ' hassummary';
		}
		$code .= '">';
		$code .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$title.'<b class="caret"></b></a>';
		$code .= '<ul class="dropdown-menu'.(($options['last'])?' dropdown-menu-right':'').'">';
		if (isset($options['menu_summary']) && $options['menu_summary'] != '') {
			$code .= '<li class="col summary_text">'.$this->data->getVar('menu_options')['menu_summary'].'</li><li class="col menu_items"><ul>';
		}
		//Render sub folders first...
		$store = new MenuStore();
		$crit = new Criteria('menu_parent',$this->data->getVar('menu_objid'));
		$crit->setOrder('ASC');
		$crit->setSort('menu_position');
		$objs = $store->get($crit);
		foreach ($objs as $k => $v) {
			$menuitem = $service->get('Ressource')->getRessourceObject('menu',$v->getVar('menu_type'));
			if (is_object($menuitem)) {
				$menuitem->setData($v);
				$code .= $menuitem->render();
			}
		}
		if (isset($options['menu_summary']) && $options['menu_summary'] != '') {
			$code .= '</ul></li>';
		}
		$code .= '</ul></li>';
		return $code;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new MenuStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('menu_options');
		$form->add(new TextFormField('menu_summary',$options['menu_summary'],array(
			'tab'=>'basic',
			'title' => _INTRO,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'menu_options','menu_summary')
		)));
		$form->add(new YesnoFormField('displaytitle',$options['displaytitle'],array(
			'tab'=>'basic',
			'title' => SYSTEM_MENUMANAGER_DISPLAYTITLE
		)));
		$form->add(new TextFormField('menu_icon',$options['menu_icon'],array(
			'tab'=>'basic',
			'title' => _ICON,
		)));
		return $form;
	}
}
?>
