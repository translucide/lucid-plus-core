<?php
/**
 * Search Menu Item
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/display/menu');

class SearchMenu extends Menu{

	/**
	 * Returns information about this block type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'search',
			'title' => SYSTEM_MENU_TYPE_SEARCH,
			'description' => SYSTEM_MENU_TYPE_SEARCH_DESC,
			'icon' => 'file',
			'saveoptions' => array(
				'menu_icon'
			)
		));
	}

	public function render($options){
		global $service;
		$rrequest = $service->get('Request')->get();
		$options = $this->data->getVar('menu_options');

		$icon = '<i class="glyphicon glyphicon-search"></i>';
		if (isset($options['menu_icon']) && $options['menu_icon'] != '') {
			$icon = '<i class="glyphicon glyphicon-'.$options['menu_icon'].'"></i>';
		}

		$code = '<li class="search '.$this->data->getVar('menu_options')['screensizevisibility'].'">
			<form class="form-search" method="get" action="'.URL.$service->get('Language')->getCode().'/search">
				<input type="hidden" id="p" name="p" value="'.intval($request['p']).'">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="'.$this->data->getVar('menu_title').'" name="query" id="query" value="'.$request['query'].'">
					<div class="input-group-btn">
						<button class="btn btn-default" type="submit">'.$icon.'</button>
					</div>
				</div>
			</form>
		</li>';
		return $code;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new MenuStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('menu_options');
		$form->add(new TextFormField('menu_icon',$options['menu_icon'],array(
			'tab'=>'basic',
			'title' => _ICON,
		)));

		return $form;
	}
}
?>
