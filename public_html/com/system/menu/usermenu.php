<?php
/**
 * User Menu Item
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/display/menu');

class UsermenuMenu extends Menu{

	/**
	 * Returns information about this block type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'usermenu',
			'title' => SYSTEM_MENU_TYPE_USERMENU,
			'description' => SYSTEM_MENU_TYPE_USERMENU_DESC,
			'icon' => 'user',
			'saveoptions' => array(
				'displaytitle','menu_icon'
			)
		));
	}

	public function render($options){
		global $service;
		$langcode = $service->get('Language')->getCode();
		$dt = $this->data->getvar('menu_options')['displaytitle'];
		$icon = $this->data->getVar('menu_options')['menu_icon'];
		$code = '<li';
		if ($service->get('User')->isLogged()){
			$title = $service->get('User')->get('email');
			$url = $service->get('Url')->get();
			if (trim($title) == '') $service->get('User')->get('name');
			if ($this->data->getvar('menu_options')['displaytitle'] == false) $title = '';
			if (trim($icon) != '') $title = '<span class="glyphicon glyphicon-'.$icon.'"></span>'.$title;
			$code .= ' class="dropdown usermenu"><a class="dropdown-toggle" data-toggle="dropdown" href="#">'.$title.'</a>';
			$code .= '<ul class="dropdown-menu '.(($options['last'])?' dropdown-menu-right':'').'" role="menu">';
			$code .= '<li><a href="'.URL.$langcode.'/user/profile">'._LOCATION_PROFILE.'</a></li>';
			$code .= '<li><a href="'.URL.$langcode.'/user/logout?redirect='.$url['route'].'">'._USER_LOGOUT.'</a></li>';

			if ($service->get('User')->isAdmin()) {
				$code .= '<li><a href="'.URL.$langcode.'/admin">'._LOCATION_ADMINISTRATION.'</a></li>';
			}
			$code .= '</ul></li>';
		}else {
			$code .= ' class="usermenu"><a href="'.URL.$langcode.'/user/login">'.(($icon)?'<span class="glyphicon glyphicon-'.$icon.'"></span>':'').(($dt)?_USER_LOGIN:'').'</a></li>';
		}
		return $code;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new MenuStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('menu_options');
		$form->add(new YesnoFormField('displaytitle',$options['displaytitle'],array(
			'tab'=>'basic',
			'title' => SYSTEM_MENUMANAGER_DISPLAYTITLE
		)));
		$form->add(new TextFormField('menu_icon',$options['menu_icon'],array(
			'tab'=>'basic',
			'title' => _ICON
		)));
		return $form;
	}
}
?>
