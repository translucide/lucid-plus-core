<?php
/**
 * Login Menu Item
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/display/menu');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/menus.php');

class LoginMenu extends Menu{

	/**
	 * Returns information about this block type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'login',
			'title' => SYSTEM_MENU_TYPE_LOGIN_TITLE,
			'description' => SYSTEM_MENU_TYPE_LOGIN_DESC,
			'icon' => 'user',
			'saveoptions' => array(
				'menu_icon','menu_target'
			)
		));
	}

	public function render($options){
		global $service;
		$options = $this->data->getVar('menu_options');
		$code = '<li class="login '.$this->data->getVar('menu_options')['screensizevisibility'].'">';

        $baseurl = URL.$service->get('Language')->getCode();
        if ($service->get('User')->isAnonymous()) $link = $baseurl.'/user/login';
        else $link = $baseurl.'/user/logout';

		$target = 'target="'.(($options['menu_target'] != '')?$options['menu_target']:'_self').'"';
		$code .= '<a href="'.$link.'" '.$target.'>';
		if (isset($options['menu_icon']) && $options['menu_icon'] != '') {
			$code .= '<span class="glyphicon glyphicon-'.$options['menu_icon'].'"></span><span>';
		}

		$code .= ($service->get('User')->isAnonymous())?_LOGIN:_LOGOUT;
		if (isset($options['menu_icon']) && $options['menu_icon'] != '') {
			$code .= '</span>';
		}

		$code .= '</a></li>';
		return $code;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new MenuStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('menu_options');
		$form->add(new SelectFormField('menu_target',$options['menu_target'],array(
			'tab'=>'basic',
			'title' => _TARGET,
			'options' => array(
				array('value' => '_self','title' => _TARGET_SELF),
				array('value' => '_blank','title' => _TARGET_BLANK),
				array('value' => '_parent','title' => _TARGET_PARENT)
			)
		)));
		$form->add(new TextFormField('menu_icon',$options['menu_icon'],array(
			'tab'=>'basic',
			'title' => _ICON
		)));
		return $form;
	}
}
?>
