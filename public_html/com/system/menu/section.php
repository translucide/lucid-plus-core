<?php
/**
 * Section Menu Item
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/display/menu');

class SectionMenu extends Menu{

	/**
	 * Returns information about this block type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'section',
			'title' => SYSTEM_MENU_TYPE_SECTION,
			'description' => SYSTEM_MENU_TYPE_SECTION_DESC,
			'icon' => 'file',
			'saveoptions' => array(
				'section','excludedcontent','nblevels'
			)
		));
	}

	public function render($options){
		global $service;
		$store = new ContentStore();
		$section = $store->get(new Criteria('content_objid',intval($this->data->getVar('menu_options')['section'])));
		$section = $section[0];
		if (!is_object($section)) {
			return '<li class="dropdown><a href="#" class="dropdown-toggle" data-toggle="dropdown">---<b class="caret"></b></a><ul class="dropdown-menu'.(($options['last'])?' dropdown-menu-right':'').'" role="menu"></ul></li>';
		}

		$excluded = $this->data->getVar('menu_options')['excludedcontent'];
		$levels = intval($this->data->getVar('menu_options')['nblevels']);
        $crit = new CriteriaCompo();

		//1. Build content level criteria
        $minlevel = $section->getVar('content_level') + 1;
        $maxlevel = $minlevel+$levels;
        $level = new CriteriaCompo();
        $level->add(new Criteria('content_level',$minlevel,'>='),'AND');
        $level->add(new Criteria('content_level',$maxlevel,'<='),'AND');
        $crit->add($level);

        //2. Build allowed content paths criteria
        $crit->add(new Criteria('content_idpath',(($minlevel > 2)?'%/':'').$section->getVar('content_objid').'/%','LIKE'));

        //3. Add permissions selection
        $crit = $store->addGroupCriteria($crit);

        //4. Add draft check
        $crit->add(new Criteria('content_draft',0));

		//5. Add excluded URL's
		$crit->add(new Criteria('content_url',"'".str_replace(',',"','",$excluded)."'",'NOT IN'));

        //6. Sorting.
        $crit->setSort('content_position','ASC');

        //7. Parse results.
        $objs = array_merge(array($section),$store->get($crit));
		return $this->renderSection($objs,$section->getVar('content_parent'),$minlevel-1,$minlevel-1,$this->data->getVar('menu_options'));
	}

	public function renderSection($objs,$parent,$level,$baselevel,$opt) {
		$code = '';

		foreach ($objs as $k => $v) {
			if ($v->getVar('content_level') == $level && $v->getVar('content_parent') == $parent) {
				switch($v->getVar('content_type')) {
					case 'section' : {
						$code .= '<li class="dropdown '.$opt['screensizevisibility'].'">';
						$code .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$v->getVar('content_title').'<b class="caret"></b></a>';
						$code .= '<ul class="dropdown-menu" role="menu">';
						$code .= $this->renderSection($objs,$v->getVar('content_objid'),$level+1,$baselevel,$opt);
						$code .= '</ul></li>';
					}break;
					default :{
						$code .= "<li class=\"link ".$this->data->getVar('menu_options')['screensizevisibility']."\"><a href=\"".URL.$v->getVar('content_urlpath')."\">";
						if (isset($v->getVar('content_options')['icon'])) $code .= "<span><img src=\"".URL.$v->getVar('content_options')['icon']."\"></span>";
						$code .= '<span>'.$v->getVar('content_title')."</span></a></li>";
					}break;
				}
			}
		}
		return $code;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new MenuStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('menu_options');

		//Generate a list of all content providers / handlers
		$infos = $service->get('Ressource')->getInfos('content');
		$treecontenttypes = '';
		$sectionIndex = 0;
		foreach ($infos as $k => $v) {
			if ($v['type'] != 'section') {
				$treecontenttypes .= '\''.$v['type'].'\' : {\'valid_children\' : \'none\',\'icon\' : {\'image\' : \''.URL.$v['icon'].'\'}},';
			}else {
				$sectionIndex = $k;
			}
		}
		$treecontenttypes.= '\'folder\' : {\'valid_children\' : ['.$validchilds.'\'folder\'],\'icon\' : {\'image\' : \''.URL.$infos[$sectionIndex]['icon'].'\'}}';
		$form->add(new TreeFormField('section',$options['section'],array(
			'tab'=>'basic',
			'title' => SYSTEM_MENU_SECTION,
			'dataurl' => URL.$service->get('Language')->getCode().'/api/system/controller/contentmanager/getchildren',
			'contenttypes' => $treecontenttypes,
		)));
		$form->add(new TextFormField('nblevels',$options['nblevels'],array(
			'tab' => 'advanced',
			'title' => SYSTEM_MENU_SECTION_NBLEVELS,
		)));
		$form->add(new TextFormField('excludedcontent',$options['excludedcontent'],array(
			'tab' => 'advanced',
			'title' => SYSTEM_MENU_SECTION_EXCLUDEDCONTENT,
			'helptip' => SYSTEM_MENU_SECTION_EXCLUDEDCONTENT_DESC,
			'icon' => 'help',
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'menu_options','excludedcontent')
		)));
		return $form;
	}
}
?>
