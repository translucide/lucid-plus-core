<?php
/**
 * Welcome Menu Item
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/display/menu');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/menus.php');

class WelcomeMenu extends Menu{

	/**
	 * Returns information about this block type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'welcome',
			'title' => SYSTEM_MENU_TYPE_WELCOME_TITLE,
			'description' => SYSTEM_MENU_TYPE_WELCOME_DESC,
			'icon' => 'user',
			'saveoptions' => array(
				'menu_icon','menu_target','showavatar'
			)
		));
	}

	public function render($options){
		global $service;
		$options = $this->data->getVar('menu_options');
		$service->get('Ressource')->get('core/model/profile');
		$store = new ProfileStore();
		$avatar = $store->getValue('avatar');

		if ($service->get('User')->isAnonymous()) return '';

		$code = '<li class="welcome '.$this->data->getVar('menu_options')['screensizevisibility'].'">';

        $baseurl = URL.$service->get('Language')->getCode();
        if ($service->get('User')->isAnonymous()) $link = $baseurl.'/user/login';
        else $link = $baseurl.'/user/profile';

		$target = 'target="'.(($options['menu_target'] != '')?$options['menu_target']:'_self').'"';
		$code .= '<a href="'.$link.'" '.$target.'>';
		if (isset($options['menu_icon']) && $options['menu_icon'] != '') {
			if ($options['showavatar'] != 1 || ($options['showavatar'] == 1 && trim($avatar) == '')){
				$code .= '<span class="glyphicon glyphicon-'.$options['menu_icon'].'"></span><span>';
			}
		}
		if (isset($options['showavatar']) && $options['showavatar'] == 1 && trim($avatar) != '') {
			$thumbsize = explode(',',strtolower($service->get('Setting')->get('tinypicturesize')));
			$code .= '<span class="avatar"><img class="avatar" src="'.UPLOADURL.$avatar.'" width="'.$thumbsize[0].'" height="'.$thumbsize.'"></span><span>';
		}
		$code .= str_replace('{user}',$service->get('User')->getVar('user_name'),$this->data->getVar('menu_title'));

		if (isset($options['menu_icon']) && $options['menu_icon'] != '') {
			if ($options['showavatar'] != 1 || ($options['showavatar'] == 1 && trim($avatar) == '')){
				$code .= '</span>';
			}
		}

		$code .= '</a></li>';
		return $code;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new MenuStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('menu_options');
		$form->add(new TextFormField('menu_link',$defobj->getVar('menu_link'),array(
			'tab'=>'basic',
			'title' => _LINK,
			'lang'=>$defaultlang['code'],
			'translations' => $form->getTranslations($objs,'menu_link')
		)));
		$form->add(new SelectFormField('menu_target',$options['menu_target'],array(
			'tab'=>'basic',
			'title' => _TARGET,
			'options' => array(
				array('value' => '_self','title' => _TARGET_SELF),
				array('value' => '_blank','title' => _TARGET_BLANK),
				array('value' => '_parent','title' => _TARGET_PARENT)
			)
		)));
		$form->add(new TextFormField('menu_icon',$options['menu_icon'],array(
			'tab'=>'basic',
			'title' => _ICON
		)));
		$form->add(new YesnoFormField('showavatar',$options['showavatar'],array(
			'tab'=>'basic',
			'title' => SYSTEM_MENU_TYPE_WELCOME_SHOWAVATAR
		)));
		return $form;
	}
}
?>
