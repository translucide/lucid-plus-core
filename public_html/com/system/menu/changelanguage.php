<?php
/**
 * Change language block
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/display/menu');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/changelanguage');

class ChangelanguageMenu extends Menu{

	/**
	 * Returns information about this block type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'changelanguage',
			'title' => CHANGELANGUAGE_WIDGET_TITLE,
			'description' => CHANGELANGUAGE_WIDGET_DESC,
			'icon' => 'file',
			'saveoptions' => array(
				'displayabbr','menutype'
			)
		));
	}

	public function render($options){
		global $service;
		$url = $service->get('Url')->get();
		$url = $url['path'];
		if (substr($url,0,1) == '/') $url = substr($url,1);

		$src = '';
		$lang = $service->get('Language')->getId();
		$langs = $service->get('Language')->getCodes();
		$alllangs = $service->get('Language')->getAllLanguages();
		$obj = $service->get('Route')->getController();
		$menutype = $this->data->getVar('menu_options')['menutype'];
		foreach ($langs as $k => $v) {
			if (($lang != $k && $menutype == 1) || $menutype == 2) {
				$langTitle = $v;
				if (!$this->data->getVar('menu_options')['displayabbr']) {
					if (defined('LANGTITLE_'.strtoupper($v))) {
						$langTitle = constant('LANGTITLE_'.strtoupper($v));
					}
					else {
						foreach($alllangs as $k2 => $v2) {
							if ($k == $v2->getVar('language_id')) {
								$langTitle = $v2->getVar('language_title');
							}
						}
					}
				}
				$ret = $obj->translate($k,0,$url);
				if ($ret == '') $ret = $v;
				$src .= '<li class="changelanguage '.$this->data->getVar('menu_options')['screensizevisibility'].'"><a href="'.URL.$ret.'">'.$langTitle.'</a></li>';
			}
		}
		return $src;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new MenuStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('menu_options');

		$opt = array();
		$i = 1;
		while(defined('CHANGELANGUAGE_WIDGET_TYPE_'.$i)) {
			$opt[] = array('title' => constant('CHANGELANGUAGE_WIDGET_TYPE_'.$i), 'value' => $i);
			$i++;
		}
		$form->add(new YesnoFormField('displayabbr',$options['displayabbr'],array(
			'tab'=> 'basic',
			'title' => CHANGELANGUAGE_WIDGET_DISPLAYABBR
		)));
		$form->add(new RadioFormField('menutype',$options['menutype'],array(
			'tab'=> 'basic',
			'title' => CHANGELANGUAGE_WIDGET_TYPE,
			'options' => $opt
		)));
		return $form;
	}
}
?>
