<?php
/**
 * Monthselector Menu Item
 *
 * May 6, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licenseSea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/list/section');
$service->get('Ressource')->get('core/display/menu');
$service->get('Ressource')->get('com/system/lang/'.$service->get('Language')->getCode().'/menus');

class MonthselectorMenu extends Menu{

	/**
	 * Returns information about this block type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'system',
			'type' => 'monthselector',
			'title' => SYSTEM_MENU_TYPE_MONTHSELECTOR,
			'description' => SYSTEM_MENU_TYPE_MONTHSELECTOR_DESC,
			'icon' => 'file',
			'saveoptions' => array(
				'menu_icon','section','searchpagetitle','maxnumberofmonths'
			)
		));
	}

	public function render($options){
		global $service;
		$request = $service->get('Request')->get();
		$options = $this->data->getVar('menu_options');
		if (isset($options['menu_icon']) && $options['menu_icon'] != '') {
			$icon = '<i class="glyphicon glyphicon-'.$options['menu_icon'].'"></i>';
		}
		$code = '<li class="monthselector '.$this->data->getVar('menu_options')['screensizevisibility'].'">
			<form class="form-monthselector" method="get" action="'.URL.$service->get('Language')->getCode().'/search">
				<input type="hidden" id="section" name="section" value="'.$options['section'].'">
				<input type="hidden" id="s" name="s" value="">
				<input type="hidden" id="e" name="e" value="">
				<input type="hidden" id="hsb" name="hsb" value="1">
				<input type="hidden" id="hsh" name="hsh" value="1">
				<input type="hidden" id="so" name="so" value="211">
				<input type="hidden" id="t" name="t" value="'.$options['searchpagetitle'].'">
				<input type="hidden" id="query" name="query" value="">
				<div class="input-group">';
		$code .= '<select name="m" class="form-control" onchange="if ($(this).val() != \'\') $(this).closest(\'form\').submit();"><option value="">'.SYSTEM_MENU_TYPE_MONTHSELECTOR_CHOOSEMONTH.'</option>';
		$months = array();
		for($i = 0; $i < intval($options['maxnumberofmonths']); $i++){
			$timestamp = strtotime("-$i month");
			//After july 2016
			/**
			 * @TODO : Remove later
			 */
			if ($timestamp > mktime(0,0,0,7,1,2016)) {
				$value =  date('m-Y', strtotime("-$i month"));
				$text  =  date('M Y', strtotime("-$i month"));
				$months[$value] =  $text;
			}
		}
		foreach($months as $value => $text){
			$code .= '<option value="'.$value.'"';
			if ($request['m'] == $value) $code .= ' selected';
			$code .= '>'.$text.'</option>';
		}
		$code .= '</select>';
		$code .= '</div>
			</form>
		</li>';
		return $code;
	}

	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new MenuStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('menu_options');
		$list = new SectionList();
		$form->add(new TextFormField('section',$options['section'],array(
			'tab'=>'advanced',
			'title' => _SECTION,
		)));
		$form->add(new TextFormField('searchpagetitle',$options['searchpagetitle'],array(
			'tab'=>'basic',
			'title' => SYSTEM_MENU_TYPE_MONTHSELECTOR_SEARCHPAGETITLE
		)));
		$form->add(new TextFormField('maxnumberofmonths',$options['maxnumberofmonths'],array(
			'tab'=>'basic',
			'title' => SYSTEM_MENU_TYPE_MONTHSELECTOR_MAXNUMBEROFMONTHS
		)));
		$form->add(new TextFormField('menu_icon',$options['menu_icon'],array(
			'tab'=>'basic',
			'title' => _ICON,
		)));

		return $form;
	}
}
?>
