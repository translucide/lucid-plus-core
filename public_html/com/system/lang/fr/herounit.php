<?php
/**
 * Hero Unit widget
 */
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT","Hero unit");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_DESC","Une zone pleine largeur avec bouton ou lien solicitant une action.");
define('SYSTEM_WIDGET_HEROUNIT_HEROUNIT_SUBTITLE','Sous-titre');
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_CONTENT","Contenu");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMAGE","Image");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMAGE_TABLET","Image (Tablets)");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMAGE_MOBILE","Image (Mobile)");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_BGIMAGE","Image de fond");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMAGEPOSITION","Position de l'image");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMAGEPOSITION_TABLET","Position de l'image (Tablet)");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMAGEPOSITION_MOBILE","Position de l'image (Mobile)");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_CONTENTORDER","Position du texte par rapport à l'image");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_BGPOSITION","Position de l\'image de fond");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_WIDTH","Largeur du contenu texte");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_BG","Couleur du fond");
define('SYSTEM_WIDGET_HEROUNIT_HEROUNIT_TEXTCOLOR','Couleur du texte');
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_EXTRACSS","CSS");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMGPADDING","Espacement autour de l'image");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_CONTENTPADDING","Espacement autour du contenu");
define("SYSTEM_WIDGET_HEROUNIT_HEROUNIT_IMAGESIZE","Taille de l\'image");
?>
