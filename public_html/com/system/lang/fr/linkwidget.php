<?php
define('SYSTEM_WIDGET_LINK_TITLE','Lien');
define('SYSTEM_WIDGET_LINK_DESC','Action ajoutant un bouton/lien');
define('SYSTEM_WIDGET_LINK_TARGET','Cible du lien');
define('SYSTEM_WIDGET_LINK_TARGET_SELF','M&ecirc;me Fen&ecird;tre');
define('SYSTEM_WIDGET_LINK_TARGET_BLANK','Nouvelle fen&ecirc;tre');
define('SYSTEM_WIDGET_LINK_TARGET_PARENT','Fen&ecirc;tre parente');
define('SYSTEM_WIDGET_LINK_TARGET_TOP','Toute la fen&ecirc;tre');
?>