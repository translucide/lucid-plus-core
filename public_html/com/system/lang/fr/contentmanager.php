<?php
define('CONTENT_PATH_ADMIN','Admin Home');
define('CONTENT_PATH_CONTENTMODULE','Pages & Content');
define('CONTENT_PATH_ADDCONTENT','Add content');

define('CONTENTMANAGER_EDIT_EXTRACSS','CSS Supplémentaire');
define('CONTENTMANAGER_EDIT_EXTRACSS_DESC','Utilisez la balise {id} pour référer à son ID ajoutée automatiquement à la balise, i.e.: #{id} .p{...}');

define('CONTENTMANAGER_CONTENTSTATS','Statistiques');
define('CONTENTMANAGER_LASTMODIF','Dernière modification');
define('CONTENTMANAGER_LASTMODIF_ONPAGE','à la page nommée');
define('CONTENTMANAGER_AVERAGEPOSTFREQUENCYYEAR','Pages crées');
define('CONTENTMANAGER_AVERAGEPOSTFREQUENCYMONTH','Ce mois');
define('CONTENTMANAGER_PERDAY','par jour');
define('CONTENTMANAGER_EVERYDAY','Une page chaque %s jours');
define('CONTENTMANAGER_CONTENTCOUNT','Nombre de pages');
define('CONTENTMANAGER_OPTION_DISPLAYTITLE','Afficher le titre?');
define('CONTENTMANAGER_SELECTCONTENTTYPE','Sélectionnez le type de contenu à créer');
define('CONTENTMANAGER_EDIT_OPENPREVIEW','Ouvrir la page dans un nouvel onglet');
?>
