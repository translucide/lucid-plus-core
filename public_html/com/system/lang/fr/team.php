<?php
define('SYSTEM_WIDGET_TEAM_TITLE','Employés et équipe');
define('SYSTEM_WIDGET_TEAM_NAME','Nom');
define('SYSTEM_WIDGET_TEAM_JOBTITLE','Titre ou poste');
define('SYSTEM_WIDGET_TEAM_DESC','Description');
define('SYSTEM_WIDGET_TEAM_PICTURE','Photo');
define('SYSTEM_WIDGET_TEAM_MEMBERS','Membres de l\'équipe');
define('SYSTEM_WIDGET_TEAM_MEMBERDETAILS','Détails du membre');
define('SYSTEM_WIDGET_TEAM_LINKEDIN_LINK','Lien LinkedIn');
define('SYSTEM_WIDGET_TEAM_FACEBOOK_LINK','Lien Facebook');
?>