<?php
define('WIDGETMANAGER_TITLE','Gestionnaire de Widgets');
define('WIDGETMANAGER_THEMEWIDGETS','Thème');
define("WIDGETMANAGER_DISPLAYURL","Afficher sur ces URLs");
define("WIDGETMANAGER_CANCELURL","Ne pas afficher sur ces URLs");
define("WIDGETMANAGER_DISPLAYSECTION","Afficher dans les section(s)");
define("WIDGETMANAGER_TAG","Balise thème");
define('WIDGETMANAGER_CANTEDITZONES','Seuls les widgets peuvents être modifiés. Les zones (dossiers) tel que sélectionné ne sont pas éditables.');
define('SYSTEM_WIDGETS_SELECTWIDGETTYPE','Sélectionnez le widget à ajouter');
?>