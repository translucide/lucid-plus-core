<?php
define('SYSTEM_DOWNLOAD_TITLE','Téléchargement');
define('SYSTEM_DOWNLOAD_WAITWHILEREDIRECT','Veuillez patentier pendant la redirection...');
define('SYSTEM_DOWNLOAD_WAITXSECONDS','Vous serez redirrigé(e) dans %n seconds');
define('SYSTEM_DOWNLOAD_IFYOURENOTREDIRECTED','Si vous n\'êtes pas redirrigé(e), cliquez ici.');
?>