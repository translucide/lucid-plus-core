<?php
define('SYSTEM_WIDGET_POPULARCONTENT_TITLE','Contenu populaire');
define('SYSTEM_WIDGET_POPULARCONTENT_TITLE_DSC','Affiche les items les plus populaires dans une section donnée.');
define('SYSTEM_WIDGET_POPULARCONTENT_SECTION','Section');
define('SYSTEM_WIDGET_POPULARCONTENT_NBITEMS','Nombre d\'éléments affichés');
define('SYSTEM_WIDGET_POPULARCONTENT_NBDAYS','Depuis quand?');
define('SYSTEM_WIDGET_POPULARCONTENT_DISPLAYTHUMBNAILS','Afficher les miniatures?');
define('SYSTEM_WIDGET_POPULARCONTENT_DISPLAYDETAILSLINK','Afficher les liens détails?');
define('SYSTEM_WIDGET_POPULARCONTENT_DISPLAYEXERPT','Afficher les sommaires?');
define('SYSTEM_WIDGET_POPULARCONTENT_NOCONTENTAVAILABLE','Aucun contenu disponible.');
?>