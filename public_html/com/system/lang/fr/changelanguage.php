<?php
/**
 * Change language widget
 */
define('CHANGELANGUAGE_WIDGET_TITLE','Changement de langue');
define('CHANGELANGUAGE_WIDGET_DESC','Affiche un bloc permettant de changer de langue');
define('CHANGELANGUAGE_WIDGET_DISPLAYABBR','Afficher les abbréviations des langues');
define('CHANGELANGUAGE_WIDGET_TYPE','Options d\'affichage');
define('CHANGELANGUAGE_WIDGET_TYPE_1','Ne pas afficher la langue en cours');
define('CHANGELANGUAGE_WIDGET_TYPE_2','Afficher toutes les langues');
?>