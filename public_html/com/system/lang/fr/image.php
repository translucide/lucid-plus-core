<?php
define('SYSTEM_WIDGET_IMAGE_TITLE','Image');
define('SYSTEM_WIDGET_IMAGE_DESC','Un widget présentant une image avec titre et image de fond optionnels.');
define('SYSTEM_WIDGET_IMAGE_IMAGE','Image');
define('SYSTEM_WIDGET_IMAGE_BGIMAGE','Image d\'arrière plan');
define('SYSTEM_WIDGET_IMAGE_BGPOSITION','Position de l\'arrière plan');
define('SYSTEM_WIDGET_IMAGE_BGCOLOR','Couleur de l\'arrière plan');
?>