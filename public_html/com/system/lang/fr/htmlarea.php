<?php
/**
 * HTML Area widget
 */
define("SYSTEM_WIDGET_HTMLAREA_HTMLAREA","Aire HTML");
define("SYSTEM_WIDGET_HTMLAREA_HTMLAREA_DESC","Une zone d'édition libre HTML à l'aide d'un éditeur de contenu");
define("SYSTEM_WIDGET_HTMLAREA_CONTENT","Contenu");
define("SYSTEM_WIDGET_HTMLAREA_BGIMAGE","Arrière plan");
define("SYSTEM_WIDGET_HTMLAREA_BGCOLOR","Couleur de fond");
define("SYSTEM_WIDGET_HTMLAREA_COLOR","Couleur du texte");
define("SYSTEM_WIDGET_HTMLAREA_BGPOSITION","Position de l'arrière plan");
define("SYSTEM_WIDGET_HTMLAREA_BG","Couleur d'arrière plan");
define("SYSTEM_WIDGET_HTMLAREA_EXTRACSS","CSS");
?>