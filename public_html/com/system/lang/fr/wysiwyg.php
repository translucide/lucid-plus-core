<?php
define('SYSTEM_CONTENT_TYPE_WYSIWYG','Éditeur WYSIWYG');
define('SYSTEM_CONTENT_TYPE_WYSIWYG_DESC','Édition visuelle de blocs (widgets) avec LucidEditor.');
define('SYSTEM_CONTENT_TYPE_WYSIWYG_CHOOSEWIDGET','Sélectionnez le type de contenu à ajouter');
define('SYSTEM_CONTENT_TYPE_WYSIWYG_EDITWIDGET','Configuration du Widget');
define('LUCIDEDITOR_WIDGET_CLASS','Class');
?>