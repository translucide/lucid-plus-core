<?php
define('SYSTEM_WIDGET_RECENTCONTENT_TITLE','Contenu récent');
define('SYSTEM_WIDGET_RECENTCONTENT_TITLE_DSC','Affiche le contenu récent d\'une section donnée');
define('SYSTEM_WIDGET_RECENTCONTENT_SECTION','Section');
define('SYSTEM_WIDGET_RECENTCONTENT_NBITEMS','Nombre d\'éléments à afficher');
define('SYSTEM_WIDGET_RECENTCONTENT_DISPLAYTHUMBNAILS','Afficher les miniatures?');
define('SYSTEM_WIDGET_RECENTCONTENT_DISPLAYDETAILSLINK','Afficher les liens détails?');
define('SYSTEM_WIDGET_RECENTCONTENT_DISPLAYEXERPT','Afficher les sommaires?');
define('SYSTEM_WIDGET_RECENTCONTENT_NOCONTENTAVAILABLE','Aucun contenu disponible.');
?>