<?php
define('SYSTEM_WIDGET_FEATUREDCONTENT_TITLE','Contenu en vedette');
define('SYSTEM_WIDGET_FEATUREDCONTENT_TITLE_DSC','Widget présentant le contenu en vedette');
define('SYSTEM_WIDGET_FEATUREDCONTENT_FEATURED','En vedette');
define('SYSTEM_WIDGET_FEATUREDCONTENT_DISPLAYTHUMBNAILS','Afficher les miniatures?');
define('SYSTEM_WIDGET_FEATUREDCONTENT_DISPLAYDETAILSLINK','Affichier les liens détails?');
define('SYSTEM_WIDGET_FEATUREDCONTENT_DISPLAYEXERPT','Afficher les sommaires?');
define('SYSTEM_WIDGET_FEATUREDCONTENT_NOCONTENTAVAILABLE','Aucun contenu disponible.');
?>