<?php
define('SYSTEM_WIDGET_LINKSCLOUD_TITLE','Links cloud');
define('SYSTEM_WIDGET_LINKSCLOUD_DSC','Presents a large number of links for SEO purpose');
define('SYSTEM_WIDGET_LINKSCLOUD_TITLE','Link');
define('SYSTEM_WIDGET_LINKSCLOUD_DESC','Link/button action');
define('SYSTEM_WIDGET_LINKSCLOUD_TARGET','Link target');
define('SYSTEM_WIDGET_LINKSCLOUD_TARGET_SELF','Same window');
define('SYSTEM_WIDGET_LINKSCLOUD_TARGET_BLANK','New window');
define('SYSTEM_WIDGET_LINKSCLOUD_TARGET_PARENT','Parent frame');
define('SYSTEM_WIDGET_LINKSCLOUD_TARGET_TOP','Full window');