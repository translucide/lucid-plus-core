<?php
define('SYSTEM_WIDGET_SECTIONSLIST','Liste de sections');
define('SYSTEM_WIDGET_SECTIONSLIST_DSC','Affiche une liste de sections de contenu');
define('SYSTEM_WIDGET_SECTIONSLIST_TITLE','Sections de contenu populaires');
define('SYSTEM_WIDGET_SECTIONSLIST_TITLE_DSC','Affiche les éléments présents dans une section donnée');
define('SYSTEM_WIDGET_SECTIONSLIST_SECTION','Section');
define('SYSTEM_WIDGET_SECTIONSLIST_NBITEMS','Nombre d\'éléments affichés');
define('SYSTEM_WIDGET_SECTIONSLIST_DISPLAYTHUMBNAILS','Afficher les miniatures?');
define('SYSTEM_WIDGET_SECTIONSLIST_DISPLAYDETAILSLINK','Afficher les liens détails?');
define('SYSTEM_WIDGET_SECTIONSLIST_DISPLAYEXERPT','Afficher le sommaire?');
define('SYSTEM_WIDGET_SECTIONSLIST_NOCONTENTAVAILABLE','Aucun contenu disponible.');
?>