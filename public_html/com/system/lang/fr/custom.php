<?php
/**
 * Custom content language strings
 */
define('SYSTEM_CONTENT_TYPE_CUSTOM','Contenu personnalisé (avancé)');
define('SYSTEM_CONTENT_TYPE_CUSTOM_DESC','Contenu HTML/JS/CSS personnalisé');

define('CUSTOMCONTENT_SAVEFIRST','Veuillez cliquer sur le bouton "Sauvegarder". Vous pourrez ensuite créer et modifier les champs d\'entrée de donnée.');
define('CUSTOMCONTENT_FIELD_TYPE','Type de champ');
define('CUSTOMCONTENT_FIELD_OPTIONS','Options');
define('CUSTOMCONTENT_FIELD_TAG','Libellé');
define('CUSTOMCONTENT_FIELD_MULTILINGUAL','Multilingue');
define('CUSTOMCONTENT_ADDFIELD','Ajouter un champ');
?>