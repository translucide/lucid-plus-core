<?php
define('SYSTEM_WIDGET_CUSTOMERLOGOS_TITLE','Nos clients');
define('SYSTEM_WIDGET_CUSTOMERLOGOS_DESC','Présente une liste de logos, pour une section du style "Ils nous font confiance".');
define('SYSTEM_WIDGET_CUSTOMERLOGOS_DRAGNDROP','Glisser-déplacer les logos dans la zone grise ci-dessous');
define('SYSTEM_WIDGET_CUSTOMERLOGOS_USEFNAMEASTITLE','Utiliser les noms de fichiers comme titre?');
define('SYSTEM_WIDGET_CUSTOMERLOGOS_LINK','Logos link');
?>