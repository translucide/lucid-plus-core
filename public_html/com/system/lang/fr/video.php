<?php
define("SYSTEM_WIDGET_VIDEO_TITLE","Vidéo");
define('SYSTEM_WIDGET_VIDEO_FILE','Fichier Vidéo (Format MP4');
define('SYSTEM_WIDGET_VIDEO_AUTOPLAY','Jouer automatiquement ?');
define('SYSTEM_WIDGET_VIDEO_AUTOPLAY_TIP','NOTE: Chrome bloque les vidéos qui jouent automatiquement lorsqu\'ils contiennent du son. Veuillez vous assuerer qu\'aucune piste de son n\'est présente dans votre vidéo!');
?>
