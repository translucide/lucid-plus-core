<?php
define('SYSTEM_MENUMANAGER','Gestion des menus');
define('SYSTEM_MENUMANAGER_NEWITEM','Créer un nouvel élément');
define('SYSTEM_MENUMANAGER_SELECTITEMTYPE','Choisissez le type d\'élément de menu à créer');
define('SYSTEM_MENUMANAGER_MANAGE','Gérer les menus');
define('SYSTEM_MENUMANAGER_DISPLAYTITLE','Afficher le titre?');

define('SYSTEM_MENUWIDGET_TITLE','Menu principal');
define('SYSTEM_MENUWIDGET_DESCRIPTION','Affiche un menu principal');

/**
 * System menu section
 */
define('SYSTEM_MENU','Menu');
define('SYSTEM_MENU_SECTION','Section de base à partir de laquelle dériver le menu');
define('SYSTEM_MENU_SECTION_EXCLUDEDCONTENT','ULR des items de contenu à exclure du menu');
define('SYSTEM_MENU_SECTION_EXCLUDEDCONTENT_DESC','Séparer chaque item par une virgule');
define('SYSTEM_MENU_SECTION_NBLEVELS','Nombre de sous niveaux à charger');
define('SYSTEM_MENU_MOBILELOGO','Logo présent dans l\'entête de menu pour appareils mobiles');
define('SYSTEM_MENU_MOBILELOGO_DESC','Ce logo ne sera visible que pour les tablettes en mode portrait et cellulaires');
define('SYSTEM_MENU_MOBILEPHONE','Ajouter une icone de téléphone au menu mobile');
define('SYSTEM_MENU_MOBILEPHONE_DESC','Permet aux usagers de cliquer une icone pour une numérotation rapide.');
define('SYSTEM_MENU_TYPE_FOLDER','Dossier');
define('SYSTEM_MENU_TYPE_FOLDER_DESC','Un item qui présente un menu contenant d\'autres items');
define('SYSTEM_MENU_TYPE_LINK','Lien');
define('SYSTEM_MENU_TYPE_LINK_DESC','Un item de menu ajoutant un lien à un menu.');
define('SYSTEM_MENU_TYPE_SUMMARYTEXT','Sommaire');
define('SYSTEM_MENU_TYPE_SUMMARYTEXT_DESC','Un item de menu ajoutant un sommaire. Souvent utile lorsque le menu comporte plusieurs choix.');
define('SYSTEM_MENU_TYPE_USERMENU','Menu utilisateur');
define('SYSTEM_MENU_TYPE_USERMENU_DESC','Un menu qui présente un lien vers la page de connexion et une fois connecté, un lien vers le profil de l\'usager, la page de déconnexion et, si l\'usager est administrateur, un lien vers l\'interface d\'administration.');
define('SYSTEM_MENU_TYPE_SECTION','Section');
define('SYSTEM_MENU_TYPE_SECTION_DESC','Un item qui présente le contenu d\'une section du site en tant que menu');
define('SYSTEM_MENU_TYPE','Type de menu');
define('SYSTEM_MENU_TYPE_1','Menu complet, nombre de sous-sections/liens illimité');
define('SYSTEM_MENU_TYPE_2','Liens seulement (seul le premier niveau sera affiché)');
define('SYSTEM_MENU_SOURCE','Créer un menu à partir de ce dossier');
define('SYSTEM_MENU_ALIGN','Alignment');
define('SYSTEM_MENU_ALIGN_1','Gauche');
define('SYSTEM_MENU_ALIGN_2','Centre');
define('SYSTEM_MENU_ALIGN_3','Droite');
define('SYSTEM_MENU_COLUMNS','Nombre de colonnes');
define('SYSTEM_MENU_DISABLEMOBILEMENU','Désactiver la version mobile sur les appareils mobiles?');

define('SYSTEM_MENU_TYPE_SEARCH','Bo&icirc;te de recherche');
define('SYSTEM_MENU_TYPE_SEARCH_DESC','Ajoute une bo&icirc;te de recherche &agrave; un menu');
?>