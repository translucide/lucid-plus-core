<?php
define('SYSTEM_WIDGET_TAGSLIST','Liste de libellés');
define('SYSTEM_WIDGET_TAGSLIST_DSC','Affiche une liste de libellés');
define('SYSTEM_WIDGET_TAGSLIST_NOCONTENTAVAILABLE','Aucun libellé trouvé');
define('SYSTEM_WIDGET_TAGSLIST_NBITEMS','Nombre de libellés à afficher');
?>