<?php
/**
 * System settings page constants
 */
define('SYSTEM_SETTINGS_TITLE','Préférences du site');
define('SYSTEM_SETTINGS_INTRO','Cette section vous permet de modifier les préférences globales de votre site web. N\'oubliez pas: soyez toujours prudents lorsque vous modifiez les paramêtres de cette section. Ne touchez pas à ce que vous ne comprenez pas totalement.');

/**
 * System setting categories
 */
define('SYSTEM_SETTINGS_CATEGORY_GENERAL','Options générales');
define('SYSTEM_SETTINGS_CATEGORY_USER','Utilisateur');
/**
 * System settings fields constants
 */
define('SYSTEM_SETTINGS_SITENAME','Nom du site');
define('SYSTEM_SETTINGS_SITENAME_DESCRIPTION','Le nom du site sera affiché dans le titre de la page lorsque la page n\'a pas de titre et dans les sections principales.');
define('SYSTEM_SETTINGS_SITESLOGAN','Slogan');
define('SYSTEM_SETTINGS_SITESLOGAN_DESCRIPTION','Le slogan de votre site web. Laisser vide si vous n\'en avez pas.');
define('SYSTEM_SETTINGS_SITEKEYWORDS','Mot clefs principaux');
define('SYSTEM_SETTINGS_SITEKEYWORDS_DESCRIPTION','Les mot-clefs principaux de votre site. Les pages n\'ayant pas de mot clefs se verront attribuer ceux-ci.');
define('SYSTEM_SETTINGS_ADMINMAIL','Adresse courriel par défaut');
define('SYSTEM_SETTINGS_ADMINMAIL_DESCRIPTION','Tout les mails que le site enverra aux administrateurs seront envoyés à cette adresse par défaut si aucune autre adresse n\'est présente dans le profil de l\'administrateur concerné.');
define('SYSTEM_SETTINGS_DEFAULTLANGUAGE','Langue par défaut');
define('SYSTEM_SETTINGS_DEFAULTLANGUAGE_DESCRIPTION','La langue à utiliser par défaut lorsqu\'aucune langue n\'est sélectionnée par l\'usager.');
define('SYSTEM_SETTINGS_ENABLEDLANGUAGES','Langues disponibles');
define('SYSTEM_SETTINGS_ENABLEDLANGUAGES_DESCRIPTION','Les langues que les usagers pourront choisir sur le site. Le contenu devra être disponible dans ces langues. Tout contenu non-disponible pour une ou plusieurs langues sera simplement ignoré dans ces langues.');
define('SYSTEM_SETTINGS_THEME','Thème du site web');
define('SYSTEM_SETTINGS_THEME_DESCRIPTION','Veuillez choisir un thème pour le côté public de votre site.');
define('SYSTEM_SETTINGS_ADMINTHEME','Thème de l\'interface d\'administration');
define('SYSTEM_SETTINGS_ADMINTHEME_DESCRIPTION','Veuillez choisir un thème à utiliser pour afficher l\'interface d\'administration de votre site.');
define('SYSTEM_SETTINGS_CLOSESITE','Fermer ce site ?');
define('SYSTEM_SETTINGS_CLOSESITE_DESCRIPTION','Sélectionnez OUI pour fermer ce site. Seuls les administrateurs aurront accès au site.');
define('SYSTEM_SETTINGS_SITECLOSEDMESSAGE','Message affiché à la fermeture du site');
define('SYSTEM_SETTINGS_SITECLOSEDMESSAGE_DESCRIPTION','Ce message sera visible lorsque vous fermerez votre site web.');
define('SYSTEM_SETTINGS_DEVELOPMENTMODE','Activer le mode développement');
define('SYSTEM_SETTINGS_DEVELOPMENTMODE_DESCRIPTION','Veuillez vous assurer de mettre cette option à OFF lorsque vous êtes en mode production.');
define('SYSTEM_SETTINGS_LOGLEVEL','Niveau de log');
define('SYSTEM_SETTINGS_LOGLEVEL_DESCRIPTION','Cette option définit quelles sortes de messages sont mis en log.');
define('SYSTEM_SETTINGS_LOGLEVEL_ALL','Tout'); //=0
define('SYSTEM_SETTINGS_LOGLEVEL_ERRORS','Erreurs seulement'); //=1
define('SYSTEM_SETTINGS_LOGLEVEL_WARNINGS','Erreurs et avertissements seulement'); //=2
define('SYSTEM_SETTINGS_LOGLEVEL_NOTICES','Messages, avertissements et erreurs'); //=3
define('SYSTEM_SETTINGS_LOGDESTINATION','Destination des logs');
define('SYSTEM_SETTINGS_LOGDESTINATION_DESCRIPTION','Envoyer les logs à la destination sélectionnée.');
define('SYSTEM_SETTINGS_LOGDESTINATION_SCREEN','Écran'); //0
define('SYSTEM_SETTINGS_LOGDESTINATION_FILE','Dans le répertoire des logs du site web'); //1
define('SYSTEM_SETTINGS_LOGDESTINATION_SYSLOG','Logs système (Unix/Linux uniquement)'); //2
define('SYSTEM_SETTINGS_LOGDESTINATION_MAIL','Envoi par courriel à l\'administrateur'); //3
define('SYSTEM_SETTINGS_TINYPICTURESIZE','Taille des miniatures');
define('SYSTEM_SETTINGS_TINYPICTURESIZE_DESCRIPTION','Largeur et hauteur des miniatures');
define('SYSTEM_SETTINGS_SMALLPICTURESIZE','Taille des petites images');
define('SYSTEM_SETTINGS_SMALLPICTURESIZE_DESCRIPTION','Largeur et hauteur des petites images');
define('SYSTEM_SETTINGS_MEDIUMPICTURESIZE','Taille des moyennes images');
define('SYSTEM_SETTINGS_MEDIUMPICTURESIZE_DESCRIPTION','Largeur et hauteur des moyennes images');
define('SYSTEM_SETTINGS_LARGEPICTURESIZE','Taille des images larges');
define('SYSTEM_SETTINGS_LARGEPICTURESIZE_DESCRIPTION','Largeur et hauteur des images larges');
define('SYSTEM_SETTINGS_ENLARGEDPICTURESIZE','Taille des images agrandies');
define('SYSTEM_SETTINGS_ENLARGEDPICTURESIZE_DESCRIPTION','Largeur et hauteur des images agrandies');
define('SYSTEM_SETTINGS_ACTIVATELARGECACHE','Activer le cache des pages complètes');
define('SYSTEM_SETTINGS_ACTIVATELARGECACHE_DESCRIPTION','Le code de la page sera mis en cache selon l\'adresse de la page et selon les groupes de l\'utilisateur.');
define('SYSTEM_SETTINGS_LARGECACHETIME','Durée de la mise en cache des pages');
define('SYSTEM_SETTINGS_LARGECACHETIME_DESCRIPTION','Durée en secondes de la mise en cache des pages côté utilisateur. La mise en cache des pages empêche toute modification aux pages du site d\'être visible pour la durée précisée ici.');
define('SYSTEM_SETTINGS_ALLOWEDFILETYPES','Types de fichiers alloués en upload');
define('SYSTEM_SETTINGS_ALLOWEDFILETYPES_DESCRIPTION','Soyez certain de spécifier tout les types de fichiers que nous nécessaires à votre site web ici. Séparez toutes les extensions de fichiers par une virgule. Tout les types de fichiers non-énumérés ici se verront refusés à l\'envoi.');
define('SYSTEM_SETTINGS_ACTIVATEDBCACHE','Activer le cache de base de données');
define('SYSTEM_SETTINGS_ACTIVATEDBCACHE_DESCRIPTION','Activer cette option est une bonne idée pour accélérer l\'accès aux données lorsque votre serveur de base de données est surchargé. Par contre si les disque durs de votre serveur sont trop lents, cela ralentira le tout.');
define('SYSTEM_SETTINGS_SESSIONLIFETIME','Durée des sessions');
define('SYSTEM_SETTINGS_SESSIONLIFETIME_DESCRIPTION','Nombre de minutes d\'inactivité avant que l\'usager soit automatiquement déconnecté.');
