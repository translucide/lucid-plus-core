<?php
define('SYSTEM_WIDGET_WIDGETZONE_WIDGETZONE','Zone de widgets');
define('SYSTEM_WIDGET_WIDGETZONE_WIDGETZONE_DESC','Ajoute une zone de widgets. Vous pouvez ajouter les widgets que vous désirez via la section WIDGETS prévue à cet effet.');
define('SYSTEM_WIDGET_WIDGETZONE_ZONENAME','Nom de la zone');
define('SYSTEM_WIDGET_WIDGETZONE_PERLINE','Nombre d\'éléments par ligne');
?>