<?php
define('SYSTEM_WIDGET_POPUPLEADFORM_TITLE','Formulaire en pop-up');
define('SYSTEM_WIDGET_POPUPLEADFORM_DESC','Affiche un formulaire dans une boîte de dialogue, et renvoie l\'usager vers une URL donnée lors de la soumission du formulaire.');
?>