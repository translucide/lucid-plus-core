<?php
define('SYSTEM_SECTION_TYPE','Section');
define('SYSTEM_SECTION_TYPE_DESC','Les sections permettent de grouper du contenu dans une page conçue pour représenter une liste de contenu.');

define('SYSTEM_SECTION_DISPLAYONLYHEADER','Afficher seulement l\'entête?');

define('SYSTEM_SECTION_FIRSTLEVELOPTIONS','Éléments principaux: que doit-on afficher?');
define('SYSTEM_SECTION_FIRSTLEVELOPTIONS_TITLE','Titre');
define('SYSTEM_SECTION_FIRSTLEVELOPTIONS_HEADER','Entête');
define('SYSTEM_SECTION_FIRSTLEVELOPTIONS_THUMB','Miniature');
define('SYSTEM_SECTION_FIRSTLEVELOPTIONS_EXERPT','Sommaire');
define('SYSTEM_SECTION_FIRSTLEVELOPTIONS_LINK','Lien vers la page de contenu');
define('SYSTEM_SECTION_FIRSTLEVELOPTIONS_LASTMODIF','Dernière modification');

define('SYSTEM_SECTION_SUBLEVELOPTIONS','Sous-éléments: que doit-on afficher?');
define('SYSTEM_SECTION_SUBLEVELOPTIONS_TITLE','Titre');
define('SYSTEM_SECTION_SUBLEVELOPTIONS_HEADER','Entête');
define('SYSTEM_SECTION_SUBLEVELOPTIONS_THUMB','Miniature');
define('SYSTEM_SECTION_SUBLEVELOPTIONS_EXERPT','Sommaire');
define('SYSTEM_SECTION_SUBLEVELOPTIONS_LINK','Lien vers la page de contenu');
define('SYSTEM_SECTION_SUBLEVELOPTIONS_LASTMODIF','Dernière modification');

define('SYSTEM_SECTION_NBLEVELS','Nombre de sous-niveaux à afficher');
define('SYSTEM_SECTION_NBLEVELS_DESC','Afficher les sous-sections et leur contenu jusqu\'à ce niveau. Des liens seront ensuite affichés.');

define('SYSTEM_SECTION_DISPLAYTITLE','Afficher le titre cette section');
define('SYSTEM_SECTION_DISPLAYTITLE_DESC','Affiche le titre de cette section au haut de la page de section. Cette option ne s\'applique pas aux éléments principaux et aux sous-éléments.');

define('SYSTEM_SECTION_ITEMSPERPAGE','Nombre d\'éléments principaux par page');
define('SYSTEM_SECTION_ITEMSPERPAGE_DESC','Un système de pagination sera visible si plus d\'une page est disponible.');

define('SYSTEM_SECTION_MAINTHUMBSIZE','Taille des miniatures principales');
define('SYSTEM_SECTION_SUBTHUMBSIZE','Taille des miniatures des sous-éléments');

define('SYSTEM_SECTION_THUMBSIZE_TINY','Miniature');
define('SYSTEM_SECTION_THUMBSIZE_SMALL','Petite');
define('SYSTEM_SECTION_THUMBSIZE_MEDIUM','Moyenne');
define('SYSTEM_SECTION_THUMBSIZE_LARGE','Large');
?>