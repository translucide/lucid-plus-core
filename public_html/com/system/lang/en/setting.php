<?php
/**
 * System settings page constants
 */
define('SYSTEM_SETTINGS_TITLE','Settings');
define('SYSTEM_SETTINGS_INTRO','Here you can modify site-wide preferences. Remember: always be careful when changing site preferences. Do not change things you don\'t fully understand!');

/**
 * System setting categories
 */
define('SYSTEM_SETTINGS_CATEGORY_GENERAL','General');
define('SYSTEM_SETTINGS_CATEGORY_USER','User settings');

/**
 * System settings fields constants
 */
define('SYSTEM_SETTINGS_SITENAME','Site name');
define('SYSTEM_SETTINGS_SITENAME_DESCRIPTION','The site name will appear on pages having no title or on main sections pages.');
define('SYSTEM_SETTINGS_SITESLOGAN','Site slogan');
define('SYSTEM_SETTINGS_SITESLOGAN_DESCRIPTION','Your website slogan. Leave blank for none.');
define('SYSTEM_SETTINGS_SITEKEYWORDS','Site main keywords');
define('SYSTEM_SETTINGS_SITEKEYWORDS_DESCRIPTION','Your website main keywords. Leave blank for none.');
define('SYSTEM_SETTINGS_ADMINMAIL','Site admin e-mail address');
define('SYSTEM_SETTINGS_ADMINMAIL_DESCRIPTION','All administrative e-mails will be sent to this address.');
define('SYSTEM_SETTINGS_DEFAULTLANGUAGE','Default language');
define('SYSTEM_SETTINGS_DEFAULTLANGUAGE_DESCRIPTION','The content in the default language will be loaded when the content is not available in the current user language or when no language has benn choosed by the current user.');
define('SYSTEM_SETTINGS_ENABLEDLANGUAGES','Enabled languages');
define('SYSTEM_SETTINGS_ENABLEDLANGUAGES_DESCRIPTION','The languages available on this website.');
define('SYSTEM_SETTINGS_THEME','Site theme');
define('SYSTEM_SETTINGS_THEME_DESCRIPTION','Choose a theme to use to display your website pages.');
define('SYSTEM_SETTINGS_ADMINTHEME','Admin theme');
define('SYSTEM_SETTINGS_ADMINTHEME_DESCRIPTION','Choose a theme to use to display the adminstrative area of your website.');
define('SYSTEM_SETTINGS_CLOSESITE','Close this site ?');
define('SYSTEM_SETTINGS_CLOSESITE_DESCRIPTION','Choose YES to close the site. The site will only remain accessible to administrators. All other users will be presented a message telling them the site is closed. You can customize this message using the "Site closed message" option.');
define('SYSTEM_SETTINGS_SITECLOSEDMESSAGE','Message to display when your site is closed');
define('SYSTEM_SETTINGS_SITECLOSEDMESSAGE_DESCRIPTION','This message will be displayed to all users except Administrators when you close the website.');
define('SYSTEM_SETTINGS_DEVELOPMENTMODE','Enable development mode');
define('SYSTEM_SETTINGS_DEVELOPMENTMODE_DESCRIPTION','Make sure this is set to NO in a production environment. Set to yes to enable log system and disable caching of JS files, CSS files and DB tables.');
define('SYSTEM_SETTINGS_LOGLEVEL','Log level');
define('SYSTEM_SETTINGS_LOGLEVEL_DESCRIPTION','Defines what kind of log messages are logged.');
define('SYSTEM_SETTINGS_LOGLEVEL_ALL','Everything'); //=0
define('SYSTEM_SETTINGS_LOGLEVEL_ERRORS','Errors only'); //=1
define('SYSTEM_SETTINGS_LOGLEVEL_WARNINGS','Errors and warnings only'); //=2
define('SYSTEM_SETTINGS_LOGLEVEL_NOTICES','Notices, warnings and errors'); //=3
define('SYSTEM_SETTINGS_LOGDESTINATION','Log destination');
define('SYSTEM_SETTINGS_LOGDESTINATION_DESCRIPTION','Writes log to the selected destination.');
define('SYSTEM_SETTINGS_LOGDESTINATION_SCREEN','Log to screen'); //0
define('SYSTEM_SETTINGS_LOGDESTINATION_FILE','Log to files in log directory'); //1
define('SYSTEM_SETTINGS_LOGDESTINATION_SYSLOG','Log to system log (Unix)'); //2
define('SYSTEM_SETTINGS_LOGDESTINATION_MAIL','Send log by email to administrator'); //3
define('SYSTEM_SETTINGS_TINYPICTURESIZE','Tiny picture size');
define('SYSTEM_SETTINGS_TINYPICTURESIZE_DESCRIPTION','Width and height of the tiny picture size');
define('SYSTEM_SETTINGS_SMALLPICTURESIZE','Small picture size');
define('SYSTEM_SETTINGS_SMALLPICTURESIZE_DESCRIPTION','Width and height of the small pictures');
define('SYSTEM_SETTINGS_MEDIUMPICTURESIZE','Medium picture size');
define('SYSTEM_SETTINGS_MEDIUMPICTURESIZE_DESCRIPTION','Width and height of the medium pictures');
define('SYSTEM_SETTINGS_LARGEPICTURESIZE','Large picture size');
define('SYSTEM_SETTINGS_LARGEPICTURESIZE_DESCRIPTION','Width and height of the large pictures');
define('SYSTEM_SETTINGS_ENLARGEDPICTURESIZE','Enlarged picture size');
define('SYSTEM_SETTINGS_ENLARGEDPICTURESIZE_DESCRIPTION','Width and height of the enlarged pictures');
define('SYSTEM_SETTINGS_ACTIVATELARGECACHE','Activate whole pages cache');
define('SYSTEM_SETTINGS_ACTIVATELARGECACHE_DESCRIPTION','Page output will be cached based on url and user groups when this option is activated.');
define('SYSTEM_SETTINGS_LARGECACHETIME','Pages cache duration (seconds)');
define('SYSTEM_SETTINGS_LARGECACHETIME_DESCRIPTION','Duration in seconds of page output cache. No modification made from the administrative area will be visible for that period of time');
define('SYSTEM_SETTINGS_ALLOWEDFILETYPES','Allowed file types for uploads');
define('SYSTEM_SETTINGS_ALLOWEDFILETYPES_DESCRIPTION','Be sure to add all allowed file types except pictures. Separate all file extension by a coma. All file uploads that have a file type not listed here will fail.');
define('SYSTEM_SETTINGS_ACTIVATEDBCACHE','Activate database cache');
define('SYSTEM_SETTINGS_ACTIVATEDBCACHE_DESCRIPTION','Setting this option might be a good idea if your database server is overloaded. It might not be a good idea if your file system is slow thought.');
define('SYSTEM_SETTINGS_SESSIONLIFETIME','Session duration');
define('SYSTEM_SETTINGS_SESSIONLIFETIME_DESCRIPTION','Number of minutes a user in idle mode will stay logged in before automatic logout occurs.');

/**
 * User settings
 */
define('SYSTEM_SETTINGS_USER_REGISTRATIONMETHOD','User registration method');
define('SYSTEM_SETTINGS_USER_REGISTRATIONMETHOD_DESC','Choose the way users can register on your site.');
define('SYSTEM_SETTINGS_USER_ALLOWREGISTRATION','Allow new user registration ?');
define('SYSTEM_SETTINGS_USER_ALLOWREGISTRATION_DESC','Gives users the ability to register to your site');
?>