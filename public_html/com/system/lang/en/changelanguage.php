<?php
/**
 * Change language widget
 */
define('CHANGELANGUAGE_WIDGET_TITLE','Change language');
define('CHANGELANGUAGE_WIDGET_DESC','Display a widget allowing users to change site language');
define('CHANGELANGUAGE_WIDGET_DISPLAYABBR','Display abbreviations');
define('CHANGELANGUAGE_WIDGET_TYPE','Display options');
define('CHANGELANGUAGE_WIDGET_TYPE_1','Display links to all active languages except the current one');
define('CHANGELANGUAGE_WIDGET_TYPE_2','Display links to all active languages');

?>