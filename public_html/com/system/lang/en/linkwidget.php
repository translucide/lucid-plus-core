<?php
define('SYSTEM_WIDGET_LINK_TITLE','Link');
define('SYSTEM_WIDGET_LINK_DESC','Link/button action');
define('SYSTEM_WIDGET_LINK_TARGET','Link target');
define('SYSTEM_WIDGET_LINK_TARGET_SELF','Same window');
define('SYSTEM_WIDGET_LINK_TARGET_BLANK','New window');
define('SYSTEM_WIDGET_LINK_TARGET_PARENT','Parent frame');
define('SYSTEM_WIDGET_LINK_TARGET_TOP','Full window');
?>