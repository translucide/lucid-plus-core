<?php
define('SYSTEM_WIDGET_TEAM_TITLE','Team and employees presentation');
define('SYSTEM_WIDGET_TEAM_NAME','Name');
define('SYSTEM_WIDGET_TEAM_JOBTITLE','Job title');
define('SYSTEM_WIDGET_TEAM_DESC','Description');
define('SYSTEM_WIDGET_TEAM_PICTURE','Picture');
define('SYSTEM_WIDGET_TEAM_MEMBERS','Team members');
define('SYSTEM_WIDGET_TEAM_MEMBERDETAILS','Team member details');
define('SYSTEM_WIDGET_TEAM_LINKEDIN_LINK','LinkedIn Link');
define('SYSTEM_WIDGET_TEAM_FACEBOOK_LINK','Facebook Link');
?>