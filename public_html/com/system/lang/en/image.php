<?php
define('SYSTEM_WIDGET_IMAGE_TITLE','Image');
define('SYSTEM_WIDGET_IMAGE_DESC','An image widget, with an optional title and background image');
define('SYSTEM_WIDGET_IMAGE_IMAGE','Image');
define('SYSTEM_WIDGET_IMAGE_BGIMAGE','Background image');
define('SYSTEM_WIDGET_IMAGE_BGPOSITION','Background position');
define('SYSTEM_WIDGET_IMAGE_BGCOLOR','Background color');
define('SYSTEM_WIDGET_IMAGE_TEXTALIGN','Text alignment');
?>