<?php
define('SYSTEM_MENU_TYPE_LOGIN_TITLE','Login/logout menu');
define('SYSTEM_MENU_TYPE_LOGIN_DESC','Displays a login menu');

define('SYSTEM_MENU_TYPE_WELCOME_TITLE','Welcome user message');
define('SYSTEM_MENU_TYPE_WELCOME_DESC','Displays a welcome user message when user is logged in');
define('SYSTEM_MENU_TYPE_WELCOME_SHOWAVATAR','Show user\'s avatar?');

define('SYSTEM_MENU_TYPE_MONTHSELECTOR','Month selector');
define('SYSTEM_MENU_TYPE_MONTHSELECTOR_DESC','Searches for content in selected month for a given content section. Usually used to browser archives');
define('SYSTEM_MENU_TYPE_MONTHSELECTOR_SEARCHPAGETITLE','Search page title');
define('SYSTEM_MENU_TYPE_MONTHSELECTOR_MAXNUMBEROFMONTHS','Max number of months');
define('SYSTEM_MENU_TYPE_MONTHSELECTOR_CHOOSEMONTH','Select month');
?>