<?php
define('SYSTEM_WIDGET_SECTIONSLIST','Sections list');
define('SYSTEM_WIDGET_SECTIONSLIST_DSC','Displays a list of sections');
define('SYSTEM_WIDGET_SECTIONSLIST_TITLE','Popular section content');
define('SYSTEM_WIDGET_SECTIONSLIST_TITLE_DSC','Displays popular items in a choosen section');
define('SYSTEM_WIDGET_SECTIONSLIST_SECTION','Section');
define('SYSTEM_WIDGET_SECTIONSLIST_NBITEMS','Number of displayed items');
define('SYSTEM_WIDGET_SECTIONSLIST_DISPLAYTHUMBNAILS','Display thumbnails?');
define('SYSTEM_WIDGET_SECTIONSLIST_DISPLAYDETAILSLINK','Display details link?');
define('SYSTEM_WIDGET_SECTIONSLIST_DISPLAYEXERPT','Display exerpt?');
define('SYSTEM_WIDGET_SECTIONSLIST_NOCONTENTAVAILABLE','No content available');
?>