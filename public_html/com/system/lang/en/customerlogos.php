<?php
define('SYSTEM_WIDGET_CUSTOMERLOGOS_TITLE','Customer logos');
define('SYSTEM_WIDGET_CUSTOMERLOGOS_DESC','Presents a number of customers logos, with a title like "We\'ve earned their trust".');
define('SYSTEM_WIDGET_CUSTOMERLOGOS_DRAGNDROP','Drag and drop customer logo files in the grey area below');
define('SYSTEM_WIDGET_CUSTOMERLOGOS_USEFNAMEASTITLE','Use file name as title?');
define('SYSTEM_WIDGET_CUSTOMERLOGOS_LINK','Logos link');
?>