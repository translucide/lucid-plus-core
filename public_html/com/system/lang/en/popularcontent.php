<?php
define('SYSTEM_WIDGET_POPULARCONTENT_TITLE','Popular content');
define('SYSTEM_WIDGET_POPULARCONTENT_TITLE_DSC','Displays popular items in a given section');
define('SYSTEM_WIDGET_POPULARCONTENT_SECTION','Section');
define('SYSTEM_WIDGET_POPULARCONTENT_NBITEMS','Number of displayed items');
define('SYSTEM_WIDGET_POPULARCONTENT_NBDAYS','Within last X days');
define('SYSTEM_WIDGET_POPULARCONTENT_DISPLAYTHUMBNAILS','Display thumbnails?');
define('SYSTEM_WIDGET_POPULARCONTENT_DISPLAYDETAILSLINK','Display details link?');
define('SYSTEM_WIDGET_POPULARCONTENT_DISPLAYEXERPT','Display exerpt?');
define('SYSTEM_WIDGET_POPULARCONTENT_NOCONTENTAVAILABLE','No content available.');
?>