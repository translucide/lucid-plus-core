<?php
/**
 * Title image
 */
define("SYSTEM_WIDGET_TITLEIMAGE_TITLE","A large image area, with optional title, descriptive text and link");
define("SYSTEM_WIDGET_TITLEIMAGE_ACTION","Action");
define("SYSTEM_WIDGET_TITLEIMAGE_STYLE_DOTPATTERN","Dotted screen view");
define("SYSTEM_WIDGET_TITLEIMAGE_TEXTALIGN","Text alignment");
?>