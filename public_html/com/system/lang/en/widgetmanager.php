<?php
define('WIDGETMANAGER_TITLE','Widgets manager');
define('WIDGETMANAGER_THEMEWIDGETS','Theme');
define("WIDGETMANAGER_DISPLAYURL","Display on URLs");
define("WIDGETMANAGER_CANCELURL","Do not diplay on URLs");
define("WIDGETMANAGER_DISPLAYSECTION","Display in section(s)");
define("WIDGETMANAGER_TAG","Theme Tag");
define('WIDGETMANAGER_CANTEDITZONES','Only widgets can be edited. Widget zones (folders) as you selected are not editable.');
define('SYSTEM_WIDGETS_SELECTWIDGETTYPE','Select widget to add');
?>