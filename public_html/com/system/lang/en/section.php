<?php
define('SYSTEM_SECTION_TYPE','Section');
define('SYSTEM_SECTION_TYPE_DESC','Site sections allow grouping of content.');

define('SYSTEM_SECTION_DISPLAYONLYHEADER','Display only header?');

define('SYSTEM_SECTION_FIRSTLEVELOPTIONS','Main elements: what should we display?');
define('SYSTEM_SECTION_FIRSTLEVELOPTIONS_TITLE','Title');
define('SYSTEM_SECTION_FIRSTLEVELOPTIONS_HEADER','Header');
define('SYSTEM_SECTION_FIRSTLEVELOPTIONS_THUMB','Thumbnail');
define('SYSTEM_SECTION_FIRSTLEVELOPTIONS_EXERPT','Exerpt');
define('SYSTEM_SECTION_FIRSTLEVELOPTIONS_LINK','Link to content page');
define('SYSTEM_SECTION_FIRSTLEVELOPTIONS_LASTMODIF','Last modification');

define('SYSTEM_SECTION_SUBLEVELOPTIONS','Sub elements: what should we display?');
define('SYSTEM_SECTION_SUBLEVELOPTIONS_TITLE','Title');
define('SYSTEM_SECTION_SUBLEVELOPTIONS_HEADER','Header');
define('SYSTEM_SECTION_SUBLEVELOPTIONS_THUMB','Thumbnail');
define('SYSTEM_SECTION_SUBLEVELOPTIONS_EXERPT','Exerpt');
define('SYSTEM_SECTION_SUBLEVELOPTIONS_LINK','Link to content page');
define('SYSTEM_SECTION_SUBLEVELOPTIONS_LASTMODIF','Last modification');

define('SYSTEM_SECTION_NBLEVELS','Maximum number of sub levels');
define('SYSTEM_SECTION_NBLEVELS_DESC','Main level does not count, therefore to display only main content elements with no sub levels, write 0 (zero) here.');

define('SYSTEM_SECTION_DISPLAYTITLE','Display section title');
define('SYSTEM_SECTION_DISPLAYTITLE_DESC','Will display this section title at the top of this section page. This setting does not apply to main and sub content elements.');

define('SYSTEM_SECTION_ITEMSPERPAGE','Number of main items per page');
define('SYSTEM_SECTION_ITEMSPERPAGE_DESC','A pagination system will be automatically visible if there is more that one page of elements to display.');

define('SYSTEM_SECTION_MAINTHUMBSIZE','Size of main elements thumbnails');
define('SYSTEM_SECTION_SUBTHUMBSIZE','Size of sub elements thumbnails');

define('SYSTEM_SECTION_THUMBSIZE_TINY','Tiny');
define('SYSTEM_SECTION_THUMBSIZE_SMALL','Small');
define('SYSTEM_SECTION_THUMBSIZE_MEDIUM','Medium');
define('SYSTEM_SECTION_THUMBSIZE_LARGE','Large');
?>