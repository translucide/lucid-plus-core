<?php
define('SYSTEM_WIDGET_WIDGETZONE_WIDGETZONE','Widget Zone');
define('SYSTEM_WIDGET_WIDGETZONE_WIDGETZONE_DESC','Adds a widget zone for dynamic widgets');
define('SYSTEM_WIDGET_WIDGETZONE_ZONENAME','Zone name');
define('SYSTEM_WIDGET_WIDGETZONE_PERLINE','Items per line');
?>