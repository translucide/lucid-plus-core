<?php
define('SYSTEM_TAGBROWSER_TAGGEDWITH','Articles tagged with "%s"');
define('SYSTEM_TAGBROWSER_TAGGEDWITH_COUNT','%s article(s) tagged with %s');
define('SYSTEM_TAGBROWSER_AVAILABLETAGS','Available tags');
define('SYSTEM_TAGBROWSER_AVAILABLETAGS_COUNT','%s tags available');
?>