<?php
define('SYSTEM_DOWNLOAD_TITLE','Download');
define('SYSTEM_DOWNLOAD_WAITWHILEREDIRECT','Please wait while you are being redirected...');
define('SYSTEM_DOWNLOAD_WAITXSECONDS','You will be redirected in %n seconds');
define('SYSTEM_DOWNLOAD_IFYOURENOTREDIRECTED','If you are not being redirected, click here');
?>