<?php
define('SYSTEM_WIDGET_COMMENTS','Comments area');
define('SYSTEM_WIDGET_COMMENTS_DESC','Adds an area for commenting on current page.');
define('SYSTEM_WIDGET_COMMENTS_DISPLAYMODE','Display mode');
define('SYSTEM_WIDGET_COMMENTS_DISPLAYMODE_FLAT','Flat');
define('SYSTEM_WIDGET_COMMENTS_DISPLAYMODE_THREADED','Threaded');
define('SYSTEM_WIDGET_COMMENTS_ALLOWANONYMOUS','Allow anonymous comments');
define('SYSTEM_WIDGET_COMMENTS_ALLOWATTACHMENTS','Allow attachments');
define('SYSTEM_WIDGET_COMMENTS_ATTACHMENT','Attachment');
define('SYSTEM_WIDGET_COMMENTS_COMMENTSFOUND','%s comments');
define('SYSTEM_WIDGET_COMMENTS_NOCOMMENTS','No comments found. Be the first to comment.');
define('SYSTEM_WIDGET_COMMENTS_POSTNEWCOMMENT','Post a new comment');
define('SYSTEM_WIDGET_COMMENTS_POSTCOMMENT','Post comment');
define('SYSTEM_WIDGET_COMMENTS_COMMENT','Comment');
define('SYSTEM_WIDGET_COMMENTS_REPLY','Reply');
define('SYSTEM_WIDGET_COMMENTS_EDIT','Edit');
define('SYSTEM_WIDGET_COMMENTS_NOTFOUND','ERROR: Comment not found !');
define('SYSTEM_WIDGET_COMMENTS_SAVEERROR','ERROR: Could not save new comment in DB.');
define('SYSTEM_WIDGET_COMMENTS_POSTREPLYCOMMENT','Reply to %s');
define('SYSTEM_WIDGET_COMMENTS_POSTEDITCOMMENT','Edit comment');
define('SYSTEM_WIDGET_COMMENTS_NBATTACHMENTS','Number of attachments to allow');
define('SYSTEM_WIDGET_COMMENTS_CLOSECOMMENTS','Close comments');
define('SYSTEM_WIDGET_COMMENTS_DELETECONFIRM','Do you really want to delete this comment ?');
define('SYSTEM_WIDGET_COMMENTS_DELETECONFIRMMESSAGE','You will not be able to retrieve the comment if you delete it.');
define('SYSTEM_WIDGET_COMMENTS_COMMENTSPERUSER','Number of comments allowed by the same user');
define('SYSTEM_WIDGET_COMMENTS_COMMENTSPERUSERHELP','Set to 0 or leave empty for unlimited.');
define('SYSTEM_WIDGET_COMMENTS_POSTCOMMENTBUTTONTEXT','Post comment button text');
?>