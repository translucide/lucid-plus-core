<?php
/**
 * HTML Area widget
 */
define("SYSTEM_WIDGET_HTMLAREA_HTMLAREA","HTML Area");
define("SYSTEM_WIDGET_HTMLAREA_HTMLAREA_DESC","A free editing area with an HTML lcontent editor");
define("SYSTEM_WIDGET_HTMLAREA_CONTENT","Content");
define("SYSTEM_WIDGET_HTMLAREA_BGIMAGE","Background Image");
define("SYSTEM_WIDGET_HTMLAREA_BGCOLOR","Background color");
define("SYSTEM_WIDGET_HTMLAREA_COLOR","Text color");
define("SYSTEM_WIDGET_HTMLAREA_BGPOSITION","Background position");
define("SYSTEM_WIDGET_HTMLAREA_BG","Background color");
define("SYSTEM_WIDGET_HTMLAREA_EXTRACSS","CSS");
?>