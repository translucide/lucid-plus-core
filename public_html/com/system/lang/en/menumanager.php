<?php
define('SYSTEM_MENUMANAGER','Menu Manager');
define('SYSTEM_MENUMANAGER_NEWITEM','Create a new item');
define('SYSTEM_MENUMANAGER_SELECTITEMTYPE','Choose the type of menu item you want to create');
define('SYSTEM_MENUMANAGER_MANAGE','Manage Menus');
define('SYSTEM_MENUMANAGER_DISPLAYTITLE','Display title?');

define('SYSTEM_MENUWIDGET_TITLE','Navigation menu');
define('SYSTEM_MENUWIDGET_DESCRIPTION','Displays a site navigation menu');

define('SYSTEM_SIMPLEMENUWIDGET_TITLE','Simple navigation menu');
define('SYSTEM_SIMPLEMENUWIDGET_DESCRIPTION','The most simple navigation menu.');

/**
 * System menu section
 */
define('SYSTEM_MENU','Menu');
define('SYSTEM_MENU_SECTION','Base section to create menu from');
define('SYSTEM_MENU_SECTION_EXCLUDEDCONTENT','Content URL\'s to exclude from menu');
define('SYSTEM_MENU_SECTION_EXCLUDEDCONTENT_DESC','Separate each by a coma');
define('SYSTEM_MENU_SECTION_NBLEVELS','Maximum number of nested levels to load');
define('SYSTEM_MENU_MOBILELOGO','Mobile menu logo');
define('SYSTEM_MENU_MOBILELOGO_DESC','This logo will only be visible for tablets and cellphones with smaller screens');
define('SYSTEM_MENU_MOBILEPHONE','Add a phone icon to the mobile menu');
define('SYSTEM_MENU_MOBILEPHONE_DESC','Allows mobile users to click on the phone icon to dial your company number.');
define('SYSTEM_MENU_TYPE_FOLDER','Folder');
define('SYSTEM_MENU_TYPE_FOLDER_DESC','A menu item that contains other menu items as a sub menu');
define('SYSTEM_MENU_TYPE_LINK','Link');
define('SYSTEM_MENU_TYPE_LINK_DESC','A menu item that adds a link to a menu');
define('SYSTEM_MENU_TYPE_SUMMARYTEXT','Summary text');
define('SYSTEM_MENU_TYPE_SUMMARYTEXT_DESC','A menu item that adds a summary text in a menu. Summary text can be used to present or inform users of their menu choices.');
define('SYSTEM_MENU_TYPE_USERMENU','User menu');
define('SYSTEM_MENU_TYPE_USERMENU_DESC','A menu that presents links to the login page and when the user is connected, link to its profile and logout. Also there is a link to the admin area when the current user is an administrator.');
define('SYSTEM_MENU_TYPE_SECTION','Section');
define('SYSTEM_MENU_TYPE_SECTION_DESC','A menu item that presents a section and its content as a menu');
define('SYSTEM_MENU_TYPE','Menu type');
define('SYSTEM_MENU_TYPE_1','Complete menu with infinite sub-sections/links');
define('SYSTEM_MENU_TYPE_2','Links menu (display only top level links)');
define('SYSTEM_MENU_SOURCE','Create menu based on this folder content');
define('SYSTEM_MENU_ALIGN','Alignment');
define('SYSTEM_MENU_ALIGN_1','Left');
define('SYSTEM_MENU_ALIGN_2','Center');
define('SYSTEM_MENU_ALIGN_3','Right');
define('SYSTEM_MENU_COLUMNS','Number of columns');
define('SYSTEM_MENU_DISABLEMOBILEMENU','Disable mobile version on mobile devices?');

define('SYSTEM_MENU_TYPE_SEARCH','Search box');
define('SYSTEM_MENU_TYPE_SEARCH_DESC','Adds a search box in a menu');
?>