<?php
/**
 * Custom content language strings
 */
define('SYSTEM_CONTENT_TYPE_CUSTOM','Custom content');
define('SYSTEM_CONTENT_TYPE_CUSTOM_DESC','Custom HTML/JS/CSS content');
define('CUSTOMCONTENT_SAVEFIRST','Please click save button first, then you will be able to create form fields.');
define('CUSTOMCONTENT_FIELD_TYPE','Field type');
define('CUSTOMCONTENT_FIELD_OPTIONS','Options');
define('CUSTOMCONTENT_FIELD_TAG','Tag');
define('CUSTOMCONTENT_FIELD_MULTILINGUAL','Multilingual');
define('CUSTOMCONTENT_ADDFIELD','Add field');
?>