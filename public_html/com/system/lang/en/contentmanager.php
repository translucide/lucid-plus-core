<?php
define('CONTENT_PATH_ADMIN','Admin Home');
define('CONTENT_PATH_CONTENTMODULE','Pages & Content');
define('CONTENT_PATH_ADDCONTENT','Add content');

define('CONTENTMANAGER_EDIT_EXTRACSS','Additional CSS');
define('CONTENTMANAGER_EDIT_EXTRACSS_DESC','Use {id} tag to refer to its unique article ID, i.e.: #{id} .p{...}');

define('CONTENTMANAGER_CONTENTSTATS','Page statistics');
define('CONTENTMANAGER_LASTMODIF','Last modification');
define('CONTENTMANAGER_LASTMODIF_ONPAGE','on page named');
define('CONTENTMANAGER_AVERAGEPOSTFREQUENCYYEAR','Page creation frequency');
define('CONTENTMANAGER_AVERAGEPOSTFREQUENCYMONTH','This month');
define('CONTENTMANAGER_PERDAY','per day');
define('CONTENTMANAGER_EVERYDAY','One page every %s days');
define('CONTENTMANAGER_CONTENTCOUNT','Number of pages');

define('CONTENTMANAGER_OPTION_DISPLAYTITLE','Display title?');
define('CONTENTMANAGER_SELECTCONTENTTYPE','Select content type to create');
define('CONTENTMANAGER_EDIT_OPENPREVIEW','Open preview in new window');
?>
