<?php
define('SYSTEM_WIDGET_TAGSLIST','Tags list');
define('SYSTEM_WIDGET_TAGSLIST_DSC','Displays a tags list');
define('SYSTEM_WIDGET_TAGSLIST_NOCONTENTAVAILABLE','No tags available');
define('SYSTEM_WIDGET_TAGSLIST_NBITEMS','Number of tags to display');
?>