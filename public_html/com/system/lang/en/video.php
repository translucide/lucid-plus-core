<?php
define("SYSTEM_WIDGET_VIDEO_TITLE","Video");
define('SYSTEM_WIDGET_VIDEO_FILE','Video file (MP4 format)');
define('SYSTEM_WIDGET_VIDEO_AUTOPLAY','Play automatically?');
define('SYSTEM_WIDGET_VIDEO_AUTOPLAY_TIP','Make sure you do not have sound in your video (no sound track present in the file, not just no sound playing), otherwise the autoplay feature will be blocked by the Chrome browser.');
?>
