<?php
/**
 * System search controller
 *
 * Handles system search display
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/controller');
$service->get('Ressource')->get('core/content');
$service->get('Ressource')->get('core/widget');

class SearchController extends Controller{
	function init(){
		$this->component = 'search';
	}

	public function execute(){
		global $service;
		$view = $this->getView();
		$request = $service->get('Request')->get();

		if (!isset($request['query'])) {
			return $view;
		}
		$title = $request['t'];

		//Query content providers and get back content IDs that match the query
		$providers = $service->get('Ressource')->getType('content');
		$ids = array();
		foreach($providers as $k => $v) {
			$ctCls = ucfirst(substr($v,strrpos($v,'/'+1))).'Content';
			$service->get('Ressource')->get($v);
			if (class_exists($ctCls) && $v != '' && $k != 'section') {
				$obj = new $ctCls();
				$tmpids = $obj->search($request['query']);
				if (is_array($tmpids) && count($tmpids) > 0) {
					$ids = array_merge($ids,$tmpids);
				}
			}
		}

		//Get additionnal content ids throught search.onquery event
		$ids = $service->get('Event')->trigger('search.query',$request['query'],true);
		$ids = array_unique($ids);
		foreach ($ids as $k => $v) {
			$ids[$k] = intval($v);
		}

		//Get Content
		$crit = new CriteriaCompo();
		$queryCrit = new CriteriaCompo();
		$queryCrit->add(new Criteria('content_title','%'.$request['query'].'%','LIKE'),'OR');
		$queryCrit->add(new Criteria('content_exerpt','%'.$request['query'].'%','LIKE'),'OR');
		$queryCrit->add(new Criteria('content_content','%'.$request['query'].'%','LIKE'),'OR');
		$queryCrit->add(new Criteria('content_options','%'.$request['query'].'%','LIKE'),'OR');
		$queryCrit->add(new Criteria('content_objid',implode(",",$ids),'IN'),'OR');
		if (isset($request['section']) && $request['section'] != '') {
			$scrit = new CriteriaCompo();
			$sections = explode(',',$request['section']);
			foreach ($sections as $s) if($s) $scrit->add(new Criteria('content_idpath','%/'.$s.'/%','LIKE'),'OR');
			$crit->add($scrit);
		}
		$crit->add($queryCrit);

		//Prevent unauthorized access to content
		$accessCrit = new CriteriaCompo();
		$ugroups = $service->get('User')->getGroups();
		if ($service->get('User')->isLogged() == false) $ugroups = array(3);
		foreach ($ugroups as $k => $v) {
			$accessCrit->add(new Criteria('content_groups','%,'.$v.',%','LIKE'),'OR');
		}
		$crit->add($accessCrit);

		//Prevent access to drafts
		$crit->add(new Criteria('content_draft',0));

		//Search for content created between start and end date.
		if (isset($request['s']) && strlen($request['s'])> 0 && isset($request['e']) && $request['e'] != '') {
			$s = explode('-',$request['s']);
			$e = explode('-',$request['e']);
			$betweenCrit = new CriteriaCompo();
			$betweenCrit->add(new Criteria('content_created',mktime(0,0,0,$s[0],$s[1],$s[2]),'>='));
			$betweenCrit->add(new Criteria('content_created',mktime(0,0,0,$e[0],$e[1],$e[2]),'<'));
			$crit->add($betweenCrit);
		}

		//Search for content created during a specified month
		if (isset($request['m']) && strlen($request['m']) > 0) {
			$m = explode('-',$request['m']);
			$m2 = $m;
			if ($m2[0] < 12) $m2[0] += 1;
			else {
				$m2[1] +=1 ;
				$m2[0] = 1;
			}
			$title = str_replace('{month}',unserialize(SYSTEM_LOCALE_MONTHS)[$m[0]-1],$title);
			$title = str_replace('{year}',$m[1],$title);
			$betweenCrit = new CriteriaCompo();
			$betweenCrit->add(new Criteria('content_created',mktime(0,0,0,$m[0],1,$m[1]),'>='));
			$betweenCrit->add(new Criteria('content_created',mktime(0,0,0,$m2[0],1,$m2[1]),'<'));
			$crit->add($betweenCrit);
		}

		//Apply sorting
		if (isset($request['so'])) {
			$so = $request['so'];
			if ($so == 10) $crit->setSort('content_title','ASC');
			if ($so == 11) $crit->setSort('content_title','DESC');
			if ($so == 20) $crit->setSort('content_created','ASC');
			if ($so == 21) $crit->setSort('content_created','DESC');
			if ($so == 30) $crit->setSort('content_modified','ASC');
			if ($so == 31) $crit->setSort('content_modified','DESC');
			if ($so == 211) {
				$crit->setSort('content_created','DESC');
				$crit->setSort('content_title','DESC');
			}
		}

		$store = new ContentStore();
		$count = $store->count($crit);
		$page = (isset($request['p']))?$request['p']:0;
		$crit->setLimit(10);
		$crit->setStart($page*10);

		$results = $store->get($crit);
		$view->setVar('count',$count);
		$view->setVar('results',$results);
		$view->setVar('title',$title);
		$view->setVar('hsb',$request['hsb']);
		return $view;
	}

}
