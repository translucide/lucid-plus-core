<?php
/**
 * Search route view
 *
 * Default view loaded on search page request
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/default');

class SearchView extends DefaultView{
	public function init(){
	}

	public function render(){
		global $service;
		$service->get('Ressource')->get('core/display/pager');
		$request = $service->get('Request')->get();
		$code = '';
		$code = '
		<article id="search" class="search">
			<div class="content">
				<div class="row">
					<div class="col-sm-12">
						<h1>'.$this->data['title'].'</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<form class="form-search" method="get" action="'.URL.$service->get('Language')->getCode().'/search">
						<input type="hidden" id="p" name="p" value="'.$request['p'].'">
						<input type="hidden" id="t" name="t" value="'.$request['t'].'">
						<input type="hidden" id="s" name="s" value="'.$request['s'].'">
						<input type="hidden" id="e" name="e" value="'.$request['e'].'">
						<input type="hidden" id="m" name="m" value="'.$request['m'].'">
						<input type="hidden" id="hsb" name="hsb" value="'.$request['hsb'].'">
						<input type="hidden" id="hsh" name="hsh" value="'.$request['hsh'].'">
		';
		if ($request['hsb'] < 1) {
			$code .= '
						<div class="input-group">
							<input type="text" class="form-control" placeholder="'.SEARCH_SEARCH.'" name="query" id="query" value="'.$request['query'].'">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
							</div>
						</div>';
		}
		$code .= '
						</form>
					</div>
				</div>';

		if (isset($request['query'])) {
			$code .= '
			<div class="row">
				<div class="col-sm-12">';
			if ($request['hsh'] < 1) $code .= '<h2>'.SEARCH_RESULTS_MATCHING.' "'.$request['query'].'"</h2>';
			$code .= '
					<p>'.sprintf(SEARCH_MATCHS,count($this->data['results'])).'</p>
					<ul>';
			foreach ($this->data['results'] as $v) {
				$code .= '<li>';
				if (trim($v->getVar('content_thumb')) != '') $code .= '<img src="'.UPLOADURL.$v->getVar('content_thumb').'">';
				$code .= '<h3><a href="'.URL.$v->getVar('content_urlpath').'">'.$v->getVar('content_title').'</a></h3>';
				if ($v->getVar('content_exerpt') != '') {
					$code .= '<p class="exerpt">'.$v->getVar('content_exerpt').'</p>';
				}
				$code .= '</li>';
			}
			$code .= '
					</ul>
				</div>
			</div>';
			$code .= Pager::render($this->data['count'],10,intval($request['p']),
				URL.$service->get('Language')->getCode().'/search?query='.$request['query'].'&p={page}&t='.$request['t'].'&s='.$request['s'].'&e='.$request['e'].'&m='.$request['m'].'&hsb='.$request['hsb'].'&hsh='.$request['hsh'].'&section='.$request['section'].'&so='.$request['so']);
		}
		$code .= '
			</div>
		</article>';
		return $code;
	}
}
?>
