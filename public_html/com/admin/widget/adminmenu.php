<?php
/**
 * Default adminmenu block
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('com/admin/lang/'.$service->get('Language')->getCode().'/adminmenu');
  
class AdminmenuWidget extends Widget{
	
	/**
	 * Returns information about this block type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'admin',
			'type' => 'menu',
			'name' => 'adminmenu',
			'title' => ADMINMENU_WIDGET_TITLE,
			'description' => ADMINMENU_WIDGET_DESCRIPTION,
			'icon' => 'menu'
		));
	}
	
	public function render(){
		global $service;
		$items = $service->get('EventHandler')->trigger('adminmenu.onLoad',array(
			'system' => array(
				'type' => 'dropdown',
				'title' => ADMINMENU_SYSTEM_TITLE,
				'items' => array()
			),
			'pages' => array(
				'type' => 'dropdown',
				'title' => ADMINMENU_PAGES_TITLE,
				'items' => array()
			)/*,
			'erp' => array(
				'type' => 'dropdown',
				'title' => ADMINMENU_ERP_TITLE,
				'items' => array(
					array()
				)
			)*/,
			'apps' => array(
				'type' => 'dropdown',
				'title' => ADMINMENU_APPS_TITLE,
				'items' => array(
					array(
						'type' => 'item',
						'glyphicon' => 'dashboard',
						'title' => ADMINMENU_APPS_DASHBOARD_TITLE,
						'link' => $service->get('Language')->getCode().'/admin'
					)
				)
			)
		),true);
		$code = '<ul class="nav navbar-nav">';
		foreach ($items as $k => $v) {
			$code .= $this->renderItem($v);
		}
		$code .= "</ul>";
		return $code;
	}
	
	public function renderItem($item){
		global $service;
		$code = '';
		switch($item['type']) {
			case 'dropdown' : {
				$code .= '<li class="dropdown">';
				$code .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$item['title'].'<b class="caret"></b></a>';
				$code .= '<ul class="dropdown-menu">';
				foreach ($item['items'] as $k => $v) {
					$code .= $this->renderItem($v);
				}
				$code .= '</ul></li>';
			}break;
			case 'item' : {
				$code .= "<li><a href=\"".URL.$item['link']."\">";
				if (isset($item['glyphicon'])) $code .= "<span class='glyphicon glyphicon-".$item['glyphicon']."'></span>&nbsp;";
				elseif (isset($item['fa'])) $code .= "<span class='fa fa-".$item['fa']."'></span>&nbsp;";
				else {
					if (isset($item['icon'])) $code .= "<img width=\"32\" height=\"32\" src=\"".URL.$service->get('Ressource')->getIcon($item)."\">&nbsp;";
				}
				
				$code .= $item['title']."</a></li>";							
			}break;
			case 'divider' : $code .= '<li class="divider"></li>'; break;
			case 'header' : $code .=  '<li class="nav-header">'.$item['title'].'</li>'; break;
		}
		return $code;
	}
}
?>