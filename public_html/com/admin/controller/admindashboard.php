<?php
/**
 * Default admin homepage route controller
 *
 * Default controller loaded on home page request or unknown page requests
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/controller');
  
class AdmindashboardController extends Controller{
	function init(){
		$this->component = 'admin';
	}
	
	public function execute(){
		global $service;
		$widgets = $service->get('EventHandler')->trigger('admindashboard.onLoad');
		$view = $this->getView();
		$breadcrumbs = array(
			array('link' => URL.$service->get('Language')->getCode().'/admin', 'title' => _LOCATION_ADMINISTRATION)
		);		
		$view->setVar('widgets',$widgets);
		$view->setVar('breadcrumbs',$breadcrumbs);
		return $view;
	}
}
?>