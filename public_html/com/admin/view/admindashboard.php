<?php
/**
 * Admindashboard route view
 *
 * Default view loaded on admin home page request
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/view');
$service->get('Ressource')->get('core/display/interface');

class AdmindashboardView extends DefaultView{
	public function init(){
	}
	
	public function render(){
		$content = '';
		$width = 0;
		//Sort by priority
		$tmpWidgets = array();
		for ($i=0; $i<=100; $i++) {
			foreach ($this->data['widgets'] as $v) {
				if ($v['priority'] == $i) {
					$tmpWidgets[] = $v;
				}
			}			
		}
		$this->data['widgets'] = $tmpWidgets;
		foreach ($this->data['widgets'] as $k => $v) {
			if ($width == 0) $content .= '<div class="row">';
			$content .= '<div class="col-sm-'.($v['width']*3).' admindashboard_block">';
			$content .= $v['code'];
			$content .= "</div>";
			$width += $v['width']*3;
			if ($width >= 12) {
				$content .= '</div>';
				$width = 0;
			}
		}
		if ($width != 0) $content .= "</div>";
		
		$interface = new LucidInterface([
			'id' => 'admindashboard',
			'name' => 'admindashboard',
			'childs' => [
				new LucidViewport([
					'childs' => [
						new LucidLayout([
							'childs' => [
								new LucidHBox([
									'childs' => [										
										new LucidBreadCrumbs([
											'childs' => $this->data['breadcrumbs']
										])
									]
								]),
								new LucidHBox([
									'childs' => [
										new LucidPanel([
											'id' => 'dashboard',
											'html' =>  $content
										])
									]
								]),								
								new LucidHBox([
									'childs' => [
										new LucidStatusBar(['id' => 'statusbar','html' => ''	])
									]
								]),								
							]
						])
					]
				])
			]
		]);
		$code = $interface->render();	
		return $code;
	}
}
?>