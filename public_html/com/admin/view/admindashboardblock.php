<?php
/**
 * Admindashboardblock view
 *
 * Default view loaded on admin home page request
 *
 * Jan 19, 2013
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/view');

class AdmindashboardblockView extends View{
	public function init(){
	}
	
	public function render(){
		$code = '';
		$code .= '<div class="panel panel-default">
			<div class="panel-heading">'.$this->data['title'].'</div>';
		if (!isset($this->data['type'])) {
			$this->data['type'] = '';
		}
		switch($this->data['type']) {
			case 'table' : {
				
			}break;
			case 'html' : {
				$code .= '<div class="panel-body"><ul class="items group">';
				$code .= $this->data['items'];
				$code .= '</div>';				
			}break;
			case 'icons' :
			case 'list' :
			default : {
				$code .= '<div class="panel-body"><ul class="items group">';
				foreach ($this->data['items'] as $k => $v) {
					$class = '';
					if (!isset($this->data['items'][$k+1])) {
						$class=" class=\"clearfix\"";
					}
					if (is_array($v)) {
						$code .= "<li$class><a href=\"".URL.$v['link']."\">";
						if (isset($v["glyphicon"])) $code .= "<div><span class='glyphicon glyphicon-".$v['glyphicon']."'></span></div>";
						elseif (isset($v["fa"])) $code .= "<div><span class='fa fa-".$v['fa']."'></span></div>";
						else $code .= "<div><img src='".URL.$v['icon']."'></div>";
						$code .= "<div>".$v['title']."</div>";
						$code .= "</a></li>";
					}else {
						$code .= "<li$class>".$v."</li>";
					}
				}
				$code .= '</ul></div>';
			}break;
		}
		$code .= '</div>';
		return array(
			'priority' => (isset($this->data['priority']))?$this->data['priority']:100,
			'width' => (isset($this->data['width']))?$this->data['width']:1,
			'code' => $code
		);
	}	
}
?>