<?php
/**
 * Admin menu language constants
 */
define('ADMINMENU_WIDGET_TITLE','Menu admin');
define('ADMINMENU_WIDGET_DESCRIPTION','Affiche le menu de l\'interface d\'administration du site');
define('ADMINMENU_SYSTEM_TITLE','Système');
define('ADMINMENU_PAGES_TITLE','Contenu');
define('ADMINMENU_APPS_TITLE','Apps');
define('ADMINMENU_APPS_DASHBOARD_TITLE','Dashboard');
?>