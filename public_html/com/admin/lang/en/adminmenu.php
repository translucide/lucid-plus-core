<?php
/**
 * Admin menu language constants
 */
define('ADMINMENU_WIDGET_TITLE','Admin menu');
define('ADMINMENU_WIDGET_DESCRIPTION','Provides an administration menu');
define('ADMINMENU_SYSTEM_TITLE','System');
define('ADMINMENU_PAGES_TITLE','Content');
define('ADMINMENU_APPS_TITLE','Apps');
define('ADMINMENU_APPS_DASHBOARD_TITLE','Dashboard');
?>