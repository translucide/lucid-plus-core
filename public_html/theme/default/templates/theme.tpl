<!DOCTYPE html>
<!--[if lt IE 7]><html lang="{:$lang:}" class="no-js lt-ie9 lt-ie8 lt-ie7" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 7]><html lang="{:$lang:}" class="no-js lt-ie9 lt-ie8" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 8]><html lang="{:$lang:}" class="no-js lt-ie9" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="{:$lang:}" class="no-js" prefix="og: http://ogp.me/ns#"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Fav Icons -->
	<link rel="apple-touch-icon" sizes="57x57" href="{:$themeurl:}favicon/apple-touch-icon-57x57.png?v=1">
	<link rel="apple-touch-icon" sizes="60x60" href="{:$themeurl:}favicon/apple-touch-icon-60x60.png?v=1">
	<link rel="apple-touch-icon" sizes="72x72" href="{:$themeurl:}favicon/apple-touch-icon-72x72.png?v=1">
	<link rel="apple-touch-icon" sizes="76x76" href="{:$themeurl:}favicon/apple-touch-icon-76x76.png?v=1">
	<link rel="apple-touch-icon" sizes="114x114" href="{:$themeurl:}favicon/apple-touch-icon-114x114.png?v=1">
	<link rel="apple-touch-icon" sizes="120x120" href="{:$themeurl:}favicon/apple-touch-icon-120x120.png?v=1">
	<link rel="apple-touch-icon" sizes="144x144" href="{:$themeurl:}favicon/apple-touch-icon-144x144.png?v=1">
	<link rel="apple-touch-icon" sizes="152x152" href="{:$themeurl:}favicon/apple-touch-icon-152x152.png?v=1">
	<link rel="apple-touch-icon" sizes="180x180" href="{:$themeurl:}favicon/apple-touch-icon-180x180.png?v=1">
	<link rel="icon" type="image/png" href="{:$themeurl:}favicon/favicon-32x32.png?v=1" sizes="32x32">
	<link rel="icon" type="image/png" href="{:$themeurl:}favicon/android-chrome-192x192.png?v=1" sizes="192x192">
	<link rel="icon" type="image/png" href="{:$themeurl:}favicon/favicon-96x96.png?v=1" sizes="96x96">
	<link rel="icon" type="image/png" href="{:$themeurl:}favicon/favicon-16x16.png?v=1" sizes="16x16">
	<link rel="manifest" href="{:$themeurl:}favicon/manifest.json?v=1">
	<link rel="shortcut icon" href="{:$themeurl:}favicon/favicon.ico?v=1">
	<meta name="msapplication-TileColor" content="#ff0000">
	<meta name="msapplication-TileImage" content="{:$themeurl:}favicon/mstile-144x144.png?v=1">
	<meta name="msapplication-config" content="{:$themeurl:}favicon/browserconfig.xml?v=1">
	<meta name="theme-color" content="#ffffff">

	<!-- Basic metas -->
	<title>{:$title:}</title>
	<meta name="description" content="{:$description:}">
	<meta name="keywords" content="{:$keywords:}">
	<meta name="author" content="{:$sitename:}">
	<link rel="author" href="{:$themeurl:}humans.txt">
	<meta name="content-language" content="{:if $lang == 'en':}english{:/if:}{:if $lang == 'fr':}french{:/if:}">

	<!-- All language specific page translations URLS -->
	{:$hreflang:}

	<!-- Functionality metas -->
	<link rel="canonical" href="{:$url:}{:$lang:}/{:$pageurl:}" />
	<meta http-equiv="Cache-control" content="public">
	<meta name="robots" CONTENT="index,follow">

	<!-- OpenGraph MetaData-->
	<meta property="og:title" content="{:$title:}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{:$url:}{:$lang:}/{:$pageurl:}" />
	<meta property="og:image" content="http://ia.media-imdb.com/images/rock.jpg" />
	<meta property="og:locale" content="{:$lang:}_US" />
	<meta property="og:site_name" content="{:$sitename:}" />

	<!-- Dublin core Metadata -->
	<link rel="schema.dc" href="http://purl.org/dc/elements/1.1/">
	<meta name="dc.title" content="{:$title:}" />
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
	<meta name="dcterms.creator" content="translucide.ca" />
	<meta name="dcterms.language" content="{:if $lang == 'en':}english{:/if:}{:if $lang == 'fr':}french{:/if:}" />

	{:foreach item=meta from=$metas:}{:$meta:}{:/foreach:}
	{:foreach item=cssfile from=$css:}{:if $cssfile.type == 'inline':}<style>{:$cssfile.src:}</style>{:else:}<link href="{:$cssfile.src:}" rel="stylesheet" type="text/css">{:/if:}{:/foreach:}
</head>
<body lang="{:$lang:}" class="{:$lang:} {:$layout:}">
	<!--[if lt IE 10]>
    <p class="chromeframe en">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <p class="chromeframe fr">Vous utilisez actuellement un navigateur <strong>désuet</strong>. Veuillez <a href="http://browsehappy.com/">mettre à jour votre navigateur</a> ou <a href="http://www.google.com/chromeframe/?redirect=true">activer Google Chrome Frame</a> afin d'améliorer votre expérience.</p>
    <![endif]-->
	{:if $layout == 'siteclosed':}
    <div class="siteclosed">
        <img src="{:$themeurl:}default/images/logo.jpg" alt="{:$sitename:}" title="{:$sitename:}">
        <h1>{:$smarty.const.SYSTEM_SITECLOSED:}</h1>
        <h2>{:$siteclosedmsg:}</h2>
    </div>
	{:else:}
	<div class="header clear">
		<div class="mainmenu sticktotop">
			<div class="wrapper">
				<div class="left mobilehidden">
					<a href="{:$url:}{:$lang:}"><img src="{:$themeurl:}img/header_logo.png" class="logo" alt="{:$smarty.const._HOME:}" title="{:$smarty.const._HOME:} - Logo"></a>
				</div>
				<div class="right">
					{:$mainmenu.src:}
				</div>
			</div>
		</div>
	</div>
	<div class="page">
		<div class="widgets_header">
		{:foreach item=item from=$widgets.header:}
			{:$item.src:}
		{:/foreach:}
		</div>
		<main class="pagecontent">
			{:$content:}
		</main>
		{:if $widgets.bottomcount > 0:}
		<div class="widgets_bottomzone clear">
        {:assign var=count value=0:}
		{:foreach item=item from=$widgets.bottom:}
            {:if $count == 0:}
                <div class="row">
            {:/if:}
			<div class="col-sm-{:$item.width:}">{:$item.src:}</div>
            {:assign var=count value=$count+$item.width:}
            {:if $count >= 12:}>
            </div>
            {:assign var=count value=0:}
            {:/if:}
		{:/foreach:}
            {:if $count > 0:}</div>{:/if:}
		</div>
		{:/if:}
	</div>
	<footer class="footer">
		<div class="head">
			<a href="{:$url:}{:$lang:}"><img src="{:$themeurl:}img/footer_logo.png" alt="{:$sitename:} - {:$smarty.const._HOME:}" title="{:$sitename:} - {:$smarty.const._HOME:}"></a>
		</div>
		<div class="widgets clear">
			{:if $widgets.footercount > 0:}
			<div class="widget clear">
			{:foreach item=item from=$widgets.footer:}
				{:$item.src:}
			{:/foreach:}
			</div>
			{:/if:}
		</div>
	</footer>
    {:foreach item=jsfile from=$js:}
    <script {:if $jsfile.type == 'file':}src="{:$jsfile.src:}" {:/if:}language="javascript" type="text/javascript">{:if $jsfile.type == 'inline':}
    {:$jsfile.src:}
    {:/if:}</script>
    {:/foreach:}

    {:/if:}
</body>
</html>
