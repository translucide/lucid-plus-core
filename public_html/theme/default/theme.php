<?php
/**
 * Default Lucid+ Theme
 *
 * Oct 31, 2012
 *
 * @version 	0.1
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
 global $service;
 $service->get('Ressource')->get('core/display/theme');

class DefaultTheme extends Theme
{
    public function DefaultTheme()
    {
        global $service;
        $this->setup();
        $service->get('Ressource')->get(THEMEROOT.'default/css/main');
        $this->setTemplate(new Template(THEMEROOT.'default/templates/theme.tpl'));

        //Load Admin theme widgets
        $service->get('Ressource')->get('core/widget');
    }

    /**
     * Provides information about this theme
     *
     * This method must be implemented in all Lucid+ Themes.
     */
    public function getInfo()
    {
        return array(
            /**
             * Generic information about this theme
             */
            'name' => 'default',
            'title' => 'Default Theme',
            'version' => .1,
            'company' => 'Translucide Communications',
            'developper' => 'Emmanuel Morin',
            'credits' => 'Emmanuel Morin',
            'releasedate' => '---',
            'email' => 'info@translucide.ca',
            'website' => 'translucide.ca',
            'description' => '',

            /**
             * The supported theme types
             *
             * Currently only 'user' and 'admin' are supported.
             * user : a public theme everybody sees when they visit your site.
             * admin : an admin theme, only admins see when enter the admin area
             */
            'types' => [
                'user'
            ],

            /**
             * Theme widgets
             *
             * Defines a collection of widgets to load for the theme.
             * Those widgets are editable in admin area, but cannot be deleted.
             */
            'widgets' => [
                'menu' => [
                    'component' => 'menu',
                    'widget' => 'menu',
                    'name' => 'menu'
                ]
            ],
            /**
             * The layouts this theme implements.
             *
             * Currently reserved supported options are 'default' and 'siteclosed'
             *
             * You can also define any number of extra layouts by giving them a
             * name and assigning that layout to the section of your choice in the
             * content manager from the admin area
             */
            'layouts' => [
                'default' => ['name'=>'default', 'title'=>'Default Layout', 'screenshot' => ''],
                'siteclosed' => ['name'=>'siteclosed', 'title'=>'Site closed layout', 'screenshot' => '']
            ],


            /**
             * Defines widget spots supported by this theme.
             * One widget spot must be named default
             *
             * Use allow and restrict parameters to filter the kind of content
             * that can be displayed in any widget zone. Multiple items are
             * coma separated. * means all and empty string means none. Allow
             * parameter is applied first, removing anything that is not allowed,
             * then restrict is applied second. Restrict only means something when
             * allow is set to * (all).
             *
             * For example allow=* and restrict=menu means allow any kind of
             * widget except menus to be displayed in that zone
             */
            'zones' => [
                'header' => [
                    'name' => 'header',
                    'title' => _HEADER,
                    'orientation' => 'h',
                    'allow' => '*',
                    'restrict' => ''
                ],
                'top' => [
                    'name' => 'top',
                    'title' => _TOP,
                    'orientation' => 'h',
                    'allow' => '*',
                    'restrict' => ''
                ],
                'dialogs' => [
                    'name' => 'dialogs',
                    'title' => _DIALOGS,
                    'orientation' => 'h',
                    'allow' => '*',
                    'restrict' => ''
                ],
                'bottom' => [
                    'name' => 'bottom',
                    'title' => _BOTTOM,
                    'orientation' => 'h',
                    'allow' => '*',
                    'restrict' => ''
                ],
                'left' => [
                    'name' => 'left',
                    'title' => _LEFT,
                    'orientation' => 'h',
                    'allow' => '*',
                    'restrict' => ''
                ],
                'right' => [
                    'name' => 'right',
                    'title' => _RIGHT,
                    'orientation' => 'h',
                    'allow' => '*',
                    'restrict' => ''
                ],
                'footer' => [
                    'name' => 'footer',
                    'title' => _FOOTER,
                    'orientation' => 'h',
                    'allow' => '*',
                    'restrict' => ''
                ]
            ],
			'styles' => [
				//Colors are specified by their rgb values, comma separated.
				'color' => [
					'main' => '17, 102, 0',
					'secondary' => '111, 117, 72',
					'action' => '17, 102, 0',
					'disabled' => '255,255,255'
					],
				'fonts' => [
					'main-family' => "'AkkuratLight'",
					'main-weight' => 'normal',
					'main-size' => '14px',

					'secondary-family' => "'AkkuratLight'",
					'secondary-weight' => 'normal',
					'secondary-size' => 'normal',

					'default-family' => "'Carlito','Calibri','sans-serif'",
					'default-weight' => 'normal',
					'default-size' => '14px',
					]
			]
        );
    }
}
