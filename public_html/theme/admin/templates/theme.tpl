<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{:$siteurl:}default/theme/admin/icons/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{:$siteurl:}default/theme/admin/icons/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="{:$siteurl:}default/theme/admin/icons/apple-touch-icon-57x57-precomposed.png">	
	<link rel="shortcut icon" href="{:$siteurl:}default/theme/admin/icons/favicon.ico" type="image/x-icon" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    
	<title>{:$title:}</title>
	{:foreach item=meta from=$metas:}<meta name="{:$meta.name:}" content="{:$meta.value:}">{:/foreach:}
	<meta name="viewport" content="width=device-width">
	{:foreach item=cssfile from=$css:}{:if $cssfile.type == 'inline':}<style>{:$cssfile.src:}</style>{:else:}<link href="{:$cssfile.src:}" rel="stylesheet" type="text/css">{:/if:}{:/foreach:}
</head>
<body>
	<!--[if lt IE 8]>
		<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
	<![endif]-->

	<nav class="navbar navbar-inverse" role="navigation" id="#lucid-admin-menu">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#lucid-admin-menu">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand pull-lef" href="{:$url:}{:$lang:}/admin"><img height="24" src="{:$themeurl:}img/logo.png"></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="lucid-admin-menu">
				{:foreach item=widget from=$widgets.menu:}
					{:if $widget.name == 'adminmenu':}
						{:$widget.src:}
					{:/if:}				
				{:/foreach:}
				<!--
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Notes <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="#" onclick="loadScreen('notes_list');"><span class="glyphicon glyphicon-tag"></span> All notes</a></li>
							{:if $isadmin:}<li><a href="#" onclick="loadScreen('notes_approvalslist');"><span class="glyphicon glyphicon-tag"></span> Waiting for approval</a></li>{:/if:}
						</ul>
					</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage <b class="caret"></b></a>
					<ul class="dropdown-menu">
						{:if $isadmin:}<li><a href="#" onclick="loadScreen('brokers_list');"><span class="glyphicon glyphicon-tag"></span> Brokers</a></li>{:/if:}
						<li><a href="#" onclick="loadScreen('clients_list');"><span class="glyphicon glyphicon-briefcase"></span> Clients</a></li>
						<li><a href="#" onclick="loadScreen('commodities_list');"><span class="glyphicon glyphicon-shopping-cart"></span> Commodities</a></li>
						<li><a href="#" onclick="loadScreen('terms_list');"><span class="glyphicon glyphicon-book"></span> Terms</a></li>
						{:if $isadmin:}<li><a href="#" onclick="loadScreen('users_list');"><span class="glyphicon glyphicon-user"></span> Users</a></li>{:/if:}
					</ul>
				</li>
				{:if $isadmin:}<li><a href="#" onclick="loadDialog('notes_export');"><span class="glyphicon glyphicon-tag"></span> Export</a></li>{:/if:}
				<li><a data-toggle="modal" data-target="#brokernotes_dialog" href="{:$url:}en/api/brokernotes/controller/brokernotes/loaddialog?screen=users_edit&id={:$userid:}"><span class="glyphicon glyphicon-user"></span> My account</a></li>						
				<li><a href="{:$url:}brokernotes/brokernotes/logout"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>						
				</ul>
			-->
				<ul class="nav navbar-nav pull-right">
					<li><a href="{:$url:}" title="{:$smarty.const._HOME:}"><span class="glyphicon glyphicon-home"></span></a></li>
					<li><a href="{:$url:}{:$lang:}/user/logout" title="{:$smarty.const._LOGOUT:}"><span class="glyphicon glyphicon-off"></span></a></li>
				</ul>			
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>		
	<div class="container">
		{:$content:}
	</div>
	{:foreach item=jsfile from=$js:}
	<script {:if $jsfile.type == 'file':}src="{:$jsfile.src:}" {:/if:}language="javascript" type="text/javascript">{:if $jsfile.type == 'inline':}
	{:$jsfile.src:}
	{:/if:}</script>
	{:/foreach:}	
	<script>
		var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
		g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
	</script>
</body>
</html>

