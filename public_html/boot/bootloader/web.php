<?php
/**
 * Bootloader class
 *
 * Manages booting in Web mode
 *
 * May 25, 2014

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
require_once(dirname(dirname(__FILE__)).'/bootloader.php');

class WebBootLoader extends BaseBootLoader {
	public function boot(){
		global $service,$hasTheme;
		$timerStart = microtime(true);
		$root = $this->getRoot();
		require_once ($root.'config/path.php');
		require_once ($root.'core/constants.php');

		//Some bots do not set HTTP_HOST header, do not loose ressources on them.
        if (!isset($_SERVER['HTTP_HOST'])) die;

		//Load basic configuration settings
		$site =  str_replace('www.','',$_SERVER['HTTP_HOST']);
		if ($_SERVER['PHP_SELF'] != 'index.php') $site .= str_replace('/','-',str_replace('/index.php','',$_SERVER['PHP_SELF']));
		$file = SITESROOT.$site.'/conf/config.php';
        if (!file_exists($file) && !file_exists(SITESROOT.'default/conf/config.php')) {
            echo 'Site '.$site.' not found !';
            die;
        }
		elseif(file_exists($file)) require_once($file);
		else require_once(SITESROOT.'default/conf/config.php');

        //Display errors if debug mode is enabled
        if (defined('DEBUG') && DEBUG == true) {
			require_once (ROOT.'core/debug/debug.php');
        }

		//3. Load basic objects and services
		require_once (ROOT.'core/object.php');
		require_once (ROOT.'core/service.php');
		require_once (ROOT.'core/view.php');
		require_once (ROOT.'core/controller.php');

		$service = new Service();
		$service->get('Log');
		$service->get('Ressource');

		require_once (ROOT.'core/db/model.php');
		require_once (ROOT.'core/db/datatype.php');
		require_once (ROOT.'core/db/data.php');
		require_once (ROOT.'core/db/store.php');
		require_once (ROOT.'core/db/criteria.php');

		$service->get('Url');
		$service->get('Site');
		$service->get('Setting');
		$service->get('Language');
		$service->get('Setting')->loadLangSettings();
		$service->get('EventHandler');
		$service->get('Log')->checkSettings();
		$service->get('Route');
		$service->get('Session');
		$service->get('Group');
		$service->get('User');
		$service->get('Capability');
		$service->get('Request');
		$service->get('Cache');

		$service->get('Time')->startTimer('sys.all',$timerStart);
		$service->get('Time')->startTimer('sys.boot',$timerStart);

		//Log site profile in use.
		$service->get("Log")->add("Site profile found : ".$site,__FILE__,__LINE__,E_INFO);

		//4. Check cache if a page for the same user groups and url exists...
		$groups = $service->get('User')->getGroups();
		$url = $service->get('Url')->get();
		$file = DATAROOT.'cache/pages/'.md5(implode('-',$groups).'-'.$url['language'].'-'.$url['path']);
		if (file_exists($file) && $service->get('Setting')->get('largecachetime') > 0 &&
			filemtime($file) > (time() - $service->get('Setting')->get('largecachetime'))  &&
			$service->get('Url')->isAdmin() == false && $service->get('Url')->hasTheme()) {
			echo file_get_contents($file);
			die;
		}

		//5. Get current language constants.
		$service->get('Ressource')->get('core/lang/'.$service->get('Language')->getCode());
		if (defined('SYSTEM_LOCALE')) setlocale(LC_TIME,SYSTEM_LOCALE);

		//6. Check / Update cache if needed...
		$service->get('EventHandler')->trigger('cache.onInit');
		$service->get('EventHandler')->trigger('sys.onInit');
		$service->get('Request')->triggerEvents();

		//7. Load Theme object and register theme service
		if ($service->get('Url')->isAdmin() && !$service->get('User')->isAdmin()) {
			if ($service->get('User')->isLogged()) {
				$service->get('User')->logout();
			}
			@header('Location: '.URL.$service->get('Language')->getCode().'/user/login?redirect='.$url['language'].(($url['admin'])?'/admin/':'').$url['path']);
			die;
		}
		$hasTheme = false;
		if ($service->get('Url')->hasTheme()) {
			@header('Content-Type: text/html; charset=utf-8');
			$theme = $service->get('Ressource')->getTheme($service->get('Url')->isAdmin());
			$service->get("Log")->add("Loaded theme class : ".get_class($theme),__FILE__,__LINE__,E_INFO);
			$service->register('Theme',$theme);
			$hasTheme = true;

			//Let only admins go on a site that is closed.
			if ($service->get('Setting')->get('closesite') == 1 && $service->get('User')->isAdmin() == false) {
				$theme->setLayout('siteclosed');
			}
            else {
				$route = $service->get('Route')->get();
				if ($route->getVar('route_controller') == 'content') {
					$content = $service->get('Content')->get();
					if (is_object($content)) {
						$layout = $content->getVar('content_layout');
						if ($layout != '') {
							$info = $service->get('Theme')->getInfo();
							if (in_array($layout,$info['layouts'])) {
								$service->get('Theme')->setLayout($layout);
							}
						}
					}
				}
			}
		}
		$service->get('Time')->stopTimer('sys.boot');

		//6. Load main controller
		$service->get('Time')->startTimer('sys.content');
		$controller = $service->get('Route')->getController();
		if (!is_object($controller) || !is_a($controller,'Controller')) {
			$service->get("Log")->add("No suitable controller found !",__FILE__,__LINE__,E_INFO);
			exit;
		}else {
			$service->get("Log")->add("Controller found: ".$controller->getName(),__FILE__,__LINE__,E_INFO);
		}
		$controller->init();
		$output = '';

		$view = $controller->execute();
		$output = str_replace(
			array('{url}','{uploadurl}'),
			array(URL,UPLOADURL),
			$view->render()
		);
		if ($hasTheme) {
			$output = str_replace('{themeurl}',$service->get('Ressource')->getThemePath(($service->get('Url')->isAdmin())?true:false),$output);
		}
		$service->get('Time')->stopTimer('sys.content');

		if ($hasTheme) {
			$theme->setContent($output);

			//De-activate widget system for admin side,
			//admin themes will load widget themselves.
			$service->get('Time')->startTimer('sys.widget');
			if ($service->get('Url')->isAdmin() == false){
				//7. Load widgets
				$service->get('Ressource')->get('core/widget');
				$widgets = new WidgetController();
				$widgets = $widgets->load();

				//8. Send widgets and content to theme
				$theme->setWidgets($widgets);
			}
			$service->get('Time')->stopTimer('sys.widget');
		}

		//9.Handle output
		$service->get('Time')->startTimer('sys.output');
		$service->get('EventHandler')->trigger('sys.onRender');
		if ($hasTheme) $output = $theme->render();

		//Check / Update cache if needed...
		$output = $service->get('EventHandler')->trigger('sys.onOutput',$output,true);
		if ($service->get('Setting')->get('largecachetime') > 0 && $service->get('Url')->isAdmin() == false) {
			file_put_contents(DATAROOT.'cache/pages/'.md5(implode('-',$groups).'-'.$url['language'].'-'.$url['path']),$output);
		}
		echo $output;
		$service->get('Time')->stopTimer('sys.output');
		$service->get('Time')->stopTimer('sys.all');
	}
}
?>
