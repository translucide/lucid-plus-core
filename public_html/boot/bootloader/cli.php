<?php
/**
 * Bootloader class
 *
 * Manages booting in CLI mode
 *
 * May 25, 2014

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */
require_once(dirname(dirname(__FILE__)).'/bootloader.php');

class CliBootLoader extends BaseBootLoader {
	public function boot(){
		global $service, $hasTheme;
		$timerStart = microtime(true);

		if (!defined('CLI')) define('CLI',1);

		$hasTheme = false;
		$logs = array();
		$r = $this->getRoot();
		require_once($r.'core/display/cli/textoutput.php');
		require_once($r.'config/path.php');
		require_once($r.'core/constants.php');
		$options = $this->getOptions();
		//Check site setting...
		if (!isset($options['site'])) {
			$site = 'default';
			$logs[] = "CliBootLoader::Boot: No site parameter specified! Using default site. If you want to specify on what site the operation should be run, check the --site parameter.\n\n";
		}else {
			$site =  $options['site'];
		}

		$file = SITESROOT.$site.'/conf/config.php';
		if (file_exists($file)) require_once($file);
		else {
			$logs[] .= "CliBootLoader::Boot: Site config not found, looking at this file: ".$r.'/site/'.$site."/conf/config.php\n\n";
		}

        //Display errors if debug mode is enabled
        if (defined('DEBUG') && DEBUG == true) {
			require_once (ROOT.'core/debug/debug.php');
        }

		//3. Load basic objects and services
		require_once (ROOT.'core/object.php');
		require_once (ROOT.'core/service.php');
		require_once (ROOT.'core/view.php');
		require_once (ROOT.'core/controller.php');

		$service = new Service();
		$service->get('Log');
		$service->get('Ressource');

		require_once (ROOT.'core/db/model.php');
		require_once (ROOT.'core/db/datatype.php');
		require_once (ROOT.'core/db/data.php');
		require_once (ROOT.'core/db/dbcache.php');
		require_once (ROOT.'core/db/store.php');
		require_once (ROOT.'core/db/criteria.php');

		$service->get('Url');
		$service->get('Site');
		$service->get('Setting');
		$service->get('Language');
		$service->get('Setting')->loadLangSettings();
		$service->get('EventHandler');
		$service->get('Log')->checkSettings();
		$service->get('Route');
		$service->get('Session');
		$service->get('Group');
		$service->get('User');
		$service->get('Request');
		$service->get('Cache');

		$service->get('Time')->startTimer('sys.all',$timerStart);
		$service->get('Time')->startTimer('sys.boot',$timerStart);
		$service->get('Time')->stopTimer('sys.all');

        ob_start();

		//Display settings, warnings and summary of what is to be done...
		$c = new TextOutput();
		$text = '';
		$text .= $c->h("Lucid+ Command Line Interface").
			$c->t("Lucid+ settings:").
			$c->l("Site selected: ".$site." (".SITESROOT.$site.")").
			$c->l("Running as: ".str_replace("\n","",shell_exec('whoami'))).
			$c->l("PHP version: ".phpversion()).
			$c->l("Server time: ".date("Y-m-d H:i:s")).
			$c->l("Core location: ".URL.' ('.ROOT.')').
			$c->l("Uploads location: ".UPLOADURL.' ('.UPLOADROOT.')').
			$c->l("Theme location: ".THEMEURL." (".THEMEROOT.")");
		if (count($logs) > 0) {
			$text .= $c->t("Boot messages").$c->l(implode("\n",$logs));
		}
		if (isset($options['triggerevent'])) {
			$text .= $c->t("RUNNING EVENT: ".$options['triggerevent']);
			$service->get('EventHandler')->trigger($options['triggerevent']);
		}
		if (isset($options['help'])) {
			$this->help();
		}

        $outputcontent = trim(ob_get_contents());
        ob_end_clean();
        if (strlen($outputcontent)) echo $text."\n\n".$outputcontent;
	}

	/**
	 * Prints an help message telling the supported options to use.
	 *
	 * @access private
	 * @return void
	 */
	private function help(){
		$cli = new TextOutput();
		echo
		$cli->h("Lucid+ CLI Mode").
		$cli->l("Usage: php index.php [OPTION]=[VALUE] [...]").
		$cli->t("OPTIONS").
		$cli->s("GENERAL OPTIONS").
		$cli->o("--help","Prints this help message and exits").
		$cli->o("--site","Specifies which site to operate on.","Defaults to \"default\"").
		$cli->s("EVENT MANAGEMENT OPTIONS").
		$cli->o("--triggerevent","Triggers an event by name.").
		$cli->l("");
		exit;
	}

	/**
	 *	Returns the command line options / parameters passed to this script.
	 *
	 *	@access private
	 *	@return array The command line options used.
	 */
	private function getOptions() {
		$clioptions = getopt("", array(
			"triggerevent::",
			"site::",
			"help"
		));
		return $clioptions;
	}
}
?>
