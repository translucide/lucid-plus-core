<?php
/**
 * Bootloader classes
 *
 * An interface to boot loaders and a base boot loader class
 *
 * May 25, 2014

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

global $hasTheme;
global $service;
date_default_timezone_set('UTC');
 
/**
 * Interface bootloader, will be implemented by each bootloader class
 */
interface BootLoader{
	public function boot();
}

/**
 * Basic BootLoader to derive BootLoader classes from
 */
class BaseBootLoader implements BootLoader{	
	public function boot(){
		echo "ERROR: BaseBootLoader loaded!";
		exit;
	}

	protected function getRoot(){
		return dirname(dirname(__FILE__)).'/';
	}
}
