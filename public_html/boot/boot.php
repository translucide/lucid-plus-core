<?php
/**
 * Bootloader class
 *
 * Manages which boot loader gets loaded based on a number of factors.
 *
 * Currently it supports those boot loaders :
 * 1. CLI boot loader
 * 2. Web output bootloader.
 *
 * May 25, 2014

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @license
 * @since 		0.1
 */

if (!isset($service)) $service = null; 
require_once(dirname(__FILE__).'/bootloader.php');

class Boot{
	
	/**
	 * The bootloader object we are using.
	 *
	 * @access private
	 * @var BootLoader The bootloader we are using
	 */
	private $bootLoader;
	
	/**
	 * The boot mode
	 *
	 * @access private
	 * @var String Boot mode
	 */
	private $bootmode;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		$this->Boot();
	}
	
	public function Boot($autoload=true){
		if ($this->find() && $autoload) {
			$this->bootLoader->boot();	
		}
	}
	
	/**
	 * Tries to find a suitable bootloader.
	 *
	 * If a bootloader is found, get the bootloader
	 * object in $this->bootLoader.
	 *
	 * @access public
	 * @return string/boolean BootLoader type found, otherwise false if not found. 
	 */
	public function find(){
		//Get the php SAPI name and check for 'cli'.
		//Everything else is considered a webserver use for now.
		$bootMode = strtolower(php_sapi_name());
		$type = false;
		if (defined('STDIN') || empty($_SERVER['REMOTE_ADDR']) || $bootMode == 'cli') {
			$this->load('cli');
			$type = 'cli';
		}else {
			$this->load('web');
			$type = 'web';
		}
		$this->bootmode = strtoupper($type);
		return $type;
	}

	/**
	 *	Returns currently used boot mode
	 *
	 *	@access public
	 *	@return String $bootmode
	 */
	public function getMode(){
		return $this->bootmode;
	}
	
	/**
	 * Creates a bootLoader object based on its name
	 *
	 * @access public
	 * @return BootLoader
	 */
	private function load($loaderName){
		$loaderName = ucfirst(strtolower($loaderName));
		if (file_exists(dirname(__FILE__).'/bootloader/'.strtolower($loaderName).'.php')) {
			include_once dirname(__FILE__).'/bootloader/'.strtolower($loaderName).'.php';
			$loaderName = $loaderName.'BootLoader';
			$this->bootLoader = new $loaderName();
			return $this->bootLoader;
		}else return false;
	}
}
?>