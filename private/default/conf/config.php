<?php
/**
 * Default configuration file.
 *
 * Lucid+ configuration file.
 *
 * August 18, 2017

 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2016 Translucide
 * @license
 * @since 		0.1
 */

/**
 * NOTE: All path constants have to end with a slash "/".
 */
$sn = 'mysite.com';
$core = '/path/to/mysite.com/';
$content = $core;
$private = $core.'private/';

define('DB_TYPE','mysqlpdo');
define('DB_HOST','localhost');
define('DB_PREFIX', 'TableNamePrefixWithoutUnderscore');
define('DB_USER', 'lucidplus');
define('DB_PASS', 'password');
define('DB_NAME', 'databasename');
define('DB_UUIDS',true);

define('DEBUG',false);

//Lucid+ core path & url
define('ROOT', $core);
define('URL', 'https://'.$sn.'/');

//Site folder path
define('DATAROOT', $private.$sn.'/');

//Theme folder path & url
define('THEMEROOT',$content.'theme/');
define('THEMEURL',URL.'theme/');

//Uploads folder & url
define('UPLOADROOT', $content.'upload/'.$sn.'/');
define('UPLOADURL',URL.'upload/'.$sn.'/');

//Password encryption algorythm
define('USER_CRYPTOALGORITHM','scrypt');

//Default users timezones
define('DEFAULTTIMEZONE','America/Edmonton');

//Default users timezones
define('SERVERTIMEZONE','America/New_York');

//Language constants
define('LANG_AVAILABLE','en');
define('LANG_DEFAULT','en');

//Email sending account constants
define('EMAIL_HOST','mysite.com');
define('EMAIL_PORT','587');
define('EMAIL_MODE','tls');
define('EMAIL_USER','website@mysite.com');
define('EMAIL_PASS','password');
define('EMAIL_FROM','Website <noreply@mysite.com>');
define('EMAIL_NAME','Web Site');
define('EMAIL_SELFSIGNED',true);
define('EMAIL_DEBUG',false);
?>
