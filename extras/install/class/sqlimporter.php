<?php
/**
 * Class to import SQL dump file to DB
 *
 * Credits to "Gromo" :
 * http://stackoverflow.com/questions/147821/loading-sql-files-from-within-php/15354406#comment37045776_15354406
 */

class SQLImporter{

    /**
     * File to import
     *
     * @access private
     * @var string SQL file path
     */
    private $importFile;
    
    /**
     * DB obj
     *
     * @access private
     * @var object MySQLI connection
     */
    private $db;
    
    /**
     * Constructors
     *
     * @access public
     * @param string $fileName
     */
    function __construct($host, $user, $pwd, $db, $importFile = '')
    {
        $this->SQLImporter($host, $user, $pwd, $db, $importFile);
    }
    
    function SQLImporter($host, $user, $pwd, $db, $importFile = '')
    {
        set_time_limit(0);
        if ($importFile != '') {
            $this->importFile = $importFile;
        }
        $this->db = new mysqli($host, $user, $pwd, $db);
        $this->db->set_charset("utf8");
    }
    
    /**
     * Imports the file to DB
     *
     * @access public
     * @param string $importFile SQL dump file path
     * @return bool True or False
     */
    public function import($importFile = '')
    {
        $file = $importFile;
        if ($file == '') $file = $this->importFile;
        if ($file == '') return false;

        $delimiter = ';';
        $file = fopen($file, 'r');
        $isFirstRow = true;
        $isMultiLineComment = false;
        $sql = '';
    
        while (!feof($file)) {
    
            $row = fgets($file);
    
            // remove BOM for utf-8 encoded file
            if ($isFirstRow) {
                $row = preg_replace('/^\x{EF}\x{BB}\x{BF}/', '', $row);
                $isFirstRow = false;
            }
    
            // 1. ignore empty string and comment row
            if (trim($row) == '' || preg_match('/^\s*(#|--\s)/sUi', $row)) {
                continue;
            }
    
            // 2. clear comments
            $row = trim($this->clearSQL($row, $isMultiLineComment));
    
            // 3. parse delimiter row
            if (preg_match('/^DELIMITER\s+[^ ]+/sUi', $row)) {
                $delimiter = preg_replace('/^DELIMITER\s+([^ ]+)$/sUi', '$1', $row);
                continue;
            }
    
            // 4. separate sql queries by delimiter
            $offset = 0;
            while (strpos($row, $delimiter, $offset) !== false) {
                $delimiterOffset = strpos($row, $delimiter, $offset);
                if (isQuoted($delimiterOffset, $row)) {
                    $offset = $delimiterOffset + strlen($delimiter);
                } else {
                    $sql = trim($sql . ' ' . trim(substr($row, 0, $delimiterOffset)));
                    query($sql);
    
                    $row = substr($row, $delimiterOffset + strlen($delimiter));
                    $offset = 0;
                    $sql = '';
                }
            }
            $sql = trim($sql . ' ' . $row);
        }
        if (strlen($sql) > 0) {
            $this->query($row);
        }
    
        fclose($file);
    }

    /**
     * Remove comments from sql
     *
     * @param string sql
     * @param boolean is multicomment line
     * @return string
     */
    function clearSQL($sql, &$isMultiComment)
    {
        if ($isMultiComment) {
            if (preg_match('#\*/#sUi', $sql)) {
                $sql = preg_replace('#^.*\*/\s*#sUi', '', $sql);
                $isMultiComment = false;
            } else {
                $sql = '';
            }
            if(trim($sql) == ''){
                return $sql;
            }
        }
    
        $offset = 0;
        while (preg_match('{--\s|#|/\*[^!]}sUi', $sql, $matched, PREG_OFFSET_CAPTURE, $offset)) {
            list($comment, $foundOn) = $matched[0];
            if ($this->isQuoted($foundOn, $sql)) {
                $offset = $foundOn + strlen($comment);
            } else {
                if (substr($comment, 0, 2) == '/*') {
                    $closedOn = strpos($sql, '*/', $foundOn);
                    if ($closedOn !== false) {
                        $sql = substr($sql, 0, $foundOn) . substr($sql, $closedOn + 2);
                    } else {
                        $sql = substr($sql, 0, $foundOn);
                        $isMultiComment = true;
                    }
                } else {
                    $sql = substr($sql, 0, $foundOn);
                    break;
                }
            }
        }
        return $sql;
    }
    
    /**
     * Check if "offset" position is quoted
     *
     * @param int $offset
     * @param string $text
     * @return boolean
     */
    function isQuoted($offset, $text)
    {
        if ($offset > strlen($text))
            $offset = strlen($text);
    
        $isQuoted = false;
        for ($i = 0; $i < $offset; $i++) {
            if ($text[$i] == "'")
                $isQuoted = !$isQuoted;
            if ($text[$i] == "\\" && $isQuoted)
                $i++;
        }
        return $isQuoted;
    }
    
    /**
     * Performs a query
     */
    function query($sql)
    {
        if (!$query = $this->db->query($sql)) {
            throw new Exception("Cannot execute request to the database {$sql}: " . $this->db->error);
        }
    }
}
?>