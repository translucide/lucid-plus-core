<?php
class Input{
	function getMissingArgumentsList($args) {
		$targets = array();
		foreach($args as $k => $v) {
			if (!isset($_POST[$v]) || empty($_POST[$v])) $targets[] = $v;		
		}
		return $targets;
	}
	function getArgs($args,$prefix='') {
		$ret = array();
		foreach($args as $k => $v) {
			if (isset($_POST[$v]) && $_POST[$v] != '') $ret[str_replace($prefix,'',$v)] = $_POST[$v];		
		}
		return $ret;
	}
}
?>