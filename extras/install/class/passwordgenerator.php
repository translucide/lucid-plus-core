<?php
/**
 * PHP Password generator class
 *
 * @see http://stackoverflow.com/questions/6101956/generating-a-random-password-in-php
 */
class PasswordGenerator{
    /**
     * Generates random bytes
     *
     * @param int $length Number of bytes to generate
     * @return string $bytes Bytes generated
     */
    function getRandomBytes($nbBytes = 32)
    {
        // Flag for whether a strong algorithm was used
        $strong = false; 
        $bytes = openssl_random_pseudo_bytes($nbBytes, $strong);
        if (false == $bytes || false === $strong) {
            throw new \Exception("Unable to generate secure token from OpenSSL.");
        }
        return $bytes;
    }

    /**
     * Generates a new password from the random bytes generated
     *
     * @param int $length Password length
     * @param bool $addSpecialChars Whether to add special chars to password
     */
    function generate($length,$addSpecialChars=true){
        if ($length < 8) $length = 8;
        $special = '`~!@#$%^&*()-_=+]}[{;:,./?\\|';   // Special Characters
        $password = preg_replace("/[^a-zA-Z0-9]/", "", base64_encode($this->getRandomBytes($length+20)));
        
        if ($addSpecialChars) {
            //Max number of symbols in Password...
            $nbsym = rand(1,$length-1);
            $nbspecial = strlen($special);
            
            for($i=0;$i<$nbsym;$i++){
                $sym = substr($special,rand(0,$nbspecial),1);
                $pwpos = rand(0,$length);
                $password = substr($password,0,$pwpos-1).$sym.substr($password,$pwpos+1);
            }            
        }
        
        return substr($password,0,$length);
    }
}
?>