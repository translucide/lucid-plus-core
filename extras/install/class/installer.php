<?php
/**
 * Lucid+ installer class
 *
 * Manages installation of Lucid+ using information provided
 */

class Installer {
	
	/**
	 * Hold DB information
	 *
	 * @access private
	 * @var array $db Host, user, pass, and db name
	 */
	private $db;
	
	/**
	 * Holds FTP connection information
	 *
	 * @access private
	 * @var array $ftp Server, username, password and base path.
	 */
	private $ftp;
	
	/**
	 * Holds site information
	 *
	 * @access private
	 * @var array $site Site url, root path, and data root
	 */ 
	private $site;
	
	/**
	 * Holds admin account information
	 *
	 * @access private
	 * @var array $admin Admin user name, password and email
	 */
	private $admin;

	function __construct($db,$ftp,$site,$admin){
		$this->Installer($db,$ftp,$site,$admin);
	}

	function Installer($db,$ftp,$site,$admin){
		$this->db = $db;
		$this->ftp = $ftp;
		if (!isset($this->ftp['path'])) $this->ftp['path'] = '';
		if (substr($this->ftp['path'],-1,1) == '/') {
			$this->ftp['path'] = substr($this->ftp['path'],0,-1);
		}
		if (substr($this->ftp['server'],-1,1) == '/') {
			$this->ftp['server'] = substr($this->ftp['server'],0,-1);
		}
		$this->site = $site;
		$this->admin = $admin;
	}
	
	public function testDB(){
		//1. Test connection settings
		$mysqli = new mysqli($this->db['host'], $this->db['user'], $this->db['pass'], $this->db['name']);
		if ($mysqli->connect_error) {
			$msg = 'DB Connection error (' . $mysqli->connect_errno . ') '.$mysqli->connect_error;
			$ret = array(
				'target' => array('dbhost','dbuser','dbpass','dbname'),
				'status' => false,
				'message' => $msg
			);
			return $ret;
		}
		
		//2. Test permissions : we need create, drop, alter, select, insert, update, delete, index
		//or simply ALL PRIVILEGES
		$res = $mysqli->query('SHOW GRANTS FOR CURRENT_USER');
		if (!$res) {
			$msg = 'Error fetching privileges for current DB user (' . $mysqli->error . ')';
			$ret = array(
				'target' => array('dbhost','dbuser','dbpass','dbname'),
				'status' => false,
				'message' => $msg
			);
			return $ret;
		}else {
			$all = false;
			$res = $res->fetch_all(MYSQLI_ASSOC);
			foreach ($res as $k => $v) {
				if (strstr($v,'GRANT ALL PRIVILEGES') !== false ||
				(strstr($v, 'SELECT') && strstr($v, 'INSERT') && strstr($v, 'UPDATE') &&		
				strstr($v, 'DELETE') &&	strstr($v, 'CREATE') && strstr($v, 'DROP') &&
				strstr($v, 'INDEX') && strstr($v, 'ALTER') && strstr($v, 'CREATE TEMPORARY'))) {
					$all = true;
				}
			}
			if ($all != true) {
				$msg = 'DB: user does not have sufficient privileges to install Lucid+. Make sure user have at least the following privileges : SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, CREATE TEMPORARY.';
				$ret = array(
					'target' => array(),
					'status' => false,
					'message' => $msg
				);
				return $ret;				
			}
		}
		return true;
	}
	
	public function testFTP(){
		include_once(dirname(dirname(dirname(dirname(__FILE__)))).'/lib/pemftp/1.0/ftp_class.php');
		$fc=new ftp(true, false);
		if ($fc->SetServer($this->ftp['server']) &&
			$fc->login($this->ftp['user'],$this->ftp['pass']) &&
			$fc->chdir($this->ftp['path']) &&
			$fc->is_exists($this->ftp['path'].'/'.'index.php')) {
			return true;
		}
		$msg = 'FTP Connection error! Either connection information or path to site root is not valid!';
		$ret = array(
			'target' => array('ftpserver','ftpuser','ftppass','ftppath'),
			'status' => false,
			'message' => $msg
		);
		return $ret;
	}
}
?>