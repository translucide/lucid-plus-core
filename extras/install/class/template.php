<?php
class Template {
	var $file;
	var $vars;
    
	function __construct($file) {
		$this->Template($file);
	}
	
    function Template($file){
		$this->file = $file;
        $this->vars = array();
	}
	
    function assign($var,$value){
        $vars[$var] = $value;    
    }

    function append($var,$value){
        $vars[$var] .= $value;    
    }
    
	function getTemplate() {
		if (file_exists($this->file)) {
			$header = file_get_contents(dirname(dirname(__FILE__)).'/templates/header.html');
			$footer = file_get_contents(dirname(dirname(__FILE__)).'/templates/footer.html');
			$tpl = file_get_contents($this->file);
			$code= $header.$tpl.$footer;
            foreach ($this->vars as $k => $v) {
                $code = str_replace('{:'.$k.':}',$v,$code);
            }
            return $code;
		}
		return false;
	}
	
	function printTemplate() {
		echo $this->getTemplate();
	}
}