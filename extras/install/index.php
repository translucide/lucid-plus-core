<?php
/**
 * Install script for Lucid+ CMS
 */
include_once(dirname(__FILE__).'/class/template.php');
include_once(dirname(__FILE__).'/class/installer.php');
include_once(dirname(__FILE__).'/class/input.php');
include_once(dirname(__FILE__).'/class/passwordgenerator.php');

$cmd = (isset($_POST['cmd']))? $_POST['cmd'] : '';
$input = new Input();
$domain = $_SERVER['HTTP_HOST'];
$user = (strrpos($domain,'.') !== false)?substr($_SERVER['HTTP_HOST'],0,strrpos($_SERVER['HTTP_HOST'],'.')):$domain;
$pwGen = new PasswordGenerator();
$dbpwd = $pwGen->generate(25);
$dbuser = $pwGen->generate(8,false);
$dbname = $pwGen->generate(12,false);
$vars = array(
	'dbhost' => 'localhost',
	'dbuser' => $user.'_'.$dbuser,
	'dbpass' => $dbpwd,
	'dbname' => $user.'_'.$dbuser,
	'siteurl' => str_replace('/extras/install/index.php','',$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']),
	'siteroot' => dirname(dirname(dirname(__FILE__))),
	'sitedata' => dirname(dirname(dirname(__FILE__))).'/site/'.$_SERVER['HTTP_HOST'],
	'siteuser' => 'admin',
	'sitepass' => '',
	'siteemail' => '',
	'version' => 0.1
);


switch($cmd) {
	case 'install' : {
		$importer = new SQLImporter($host,$user,$pwd,$db,dirname(__FILE__).'/sql/install.sql');
		$importer->import();
		$tpl = new Template(dirname(__FILE__).'/templates/install.html');
		$tpl->printTemplate();
	}break;
	case 'check' : {
		$installer = new Installer(
			$input->getArgs(array('dbhost','dbuser','dbpass','dbname'),'db'),
			$input->getArgs(array('ftpserver','ftpuser','ftppass','ftppath'),'ftp'),
			$input->getArgs(array('siteurl','siteroot','sitedata'),'site'),
			$input->getArgs(array('adminuser','adminpass','adminemail'),'admin')
		);
		error_reporting(0);
		//Validate information provided here.
		$response = array();
		$success = true;

		//1. Check db connection infos.
		$targets = $input->getMissingArgumentsList(array('dbhost','dbuser','dbname','dbpass'));
		if (count($targets) == 0) {
			$ret = $installer->testDB();
			if (is_array($ret)) $response['checks'][] = $ret;
		} else {
			$msg = 'Please provide all necessary DB connection settings.';
			$success = false;
			$response['checks'][] = array(
				'target' => $targets,
				'status' => false,
				'message' => $msg
			);			
		}
		
		//2. Check FTP settings
		$targets = $input->getMissingArgumentsList(array('ftpserver','ftpuser','ftppass'));
		if (count($targets) == 0) {
			$ret = $installer->testFTP();
		}else {
			$msg = 'Please provide all FTP login credentials. ';
			$response['checks'][] = array(
				'target' => $targets,
				'status' => false,
				'message' => $msg
			);			
		}
		
		//3. Check site url and paths settings
		$targets = $input->getMissingArgumentsList(array('siteurl','siteroot','sitedata'));
		if (count($targets) == 0) {
		}else {
			$msg = 'Please provide site URL, site root path and data root path.';
			$response['checks'][] = array(
				'target' => $targets,
				'status' => false,
				'message' => $msg
			);
		}
		
		//4. Check admin account settings (validate username and password against Lucid+ password policies)
		$targets = $input->getMissingArgumentsList(array('adminuser','adminpass','adminemail'));
		if (count($targets) == 0) {
		}else {
			$msg = 'Please provide the admin username, password and email you would like to use to login to your site.';
			$response['checks'][] = array(
				'target' => $targets,
				'status' => false,
				'message' => $msg
			);		
		}
		
		$response['success'] = $success;
		
		header('Content-Type: application/json');
		echo json_encode($response);
		exit;
	}break;
	case '':
	default : {
		$tpl = new Template(dirname(__FILE__).'/templates/default.html');
		$code = $tpl->getTemplate();
		foreach($vars as $k => $v) {
			$code = str_replace('{'.$k.'}',$v,$code);
		}
		echo $code;
		exit;
	}break;
}
?>