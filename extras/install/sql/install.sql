/*
LUCID+ v0.1.3 INSTALL SQL SCRIPT

Constants used by this script :
{DB_PREFIX} ==> Tables names prefix, without the _ at the end.

To generate UUIDS : https://www.fileformat.info/tool/guid.htm

{ADMIN_GROUP_ID} ==> The admin group ID
{REGISTERED_GROUP_ID} ==> The admin group ID
{ANONYMOUS_GROUP_ID} ==> The admin group ID

{DEFAULT_USERID} ==> Default admin user id created on install
{DEFAULT_TENANT} ==> Default Tenant ID
{DEFAULT_SITEID} ==> Default site ID
{DEFAULT_THEME} ==> Default theme to use when loading content
{DEFAULT_USEREMAIL} => Admin email addres, used as username when on login.
*/

-- CAPABILITY TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_capability`;
CREATE TABLE `{DB_PREFIX}_capability` (
  `capability_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `capability_id` VARBINARY(22) NULL NOT NULL DEFAULT '',
  `capability_nameid` VARCHAR(50) NULL NOT NULL DEFAULT '',
  `capability_group` VARBINARY(22) NOT NULL DEFAULT '0',
  `capability_title` VARCHAR(50) NOT NULL DEFAULT '',
  `capability_description` VARCHAR(100) NOT NULL DEFAULT '',
  `capability_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `capability_deleted` INT UNSIGNED NOT NULL DEFAULT 0,
  `capability_deletedby` VARBINARY(22) NOT NULL DEFAULT '0',
  `capability_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `capability_createdby` VARBINARY(22) NOT NULL DEFAULT '0',
  `capability_modified` INT UNSIGNED NOT NULL DEFAULT 0,
  `capability_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`capability_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;

-- COMMENT TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_comment`;
CREATE TABLE `{DB_PREFIX}_comment` (
  `comment_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` VARBINARY(22) NULL NOT NULL DEFAULT '',
  `comment_contentobjid` VARBINARY(22) NULL NOT NULL DEFAULT '',
  `comment_userid` VARBINARY(22) NOT NULL DEFAULT '0',
  `comment_parent` VARBINARY(22) NOT NULL DEFAULT '0',
  `comment_position` INT UNSIGNED NOT NULL DEFAULT 0,
  `comment_index` INT UNSIGNED NOT NULL DEFAULT 0,
  `comment_level` INT UNSIGNED NOT NULL DEFAULT 0,
  `comment_content` TEXT,
  `comment_attachments` TEXT,
  `comment_language` INT UNSIGNED NOT NULL DEFAULT 0,
  `comment_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `comment_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `comment_modified` INT UNSIGNED NOT NULL DEFAULT 0,
  `comment_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_comment` ADD INDEX `comment_contentobjid` (`comment_contentobjid`);
ALTER TABLE `{DB_PREFIX}_comment` ADD INDEX `comment_userid` (`comment_userid`);
ALTER TABLE `{DB_PREFIX}_comment` ADD INDEX `comment_position` (`comment_position`);
ALTER TABLE `{DB_PREFIX}_comment` ADD INDEX `comment_language` (`comment_language`);
ALTER TABLE `{DB_PREFIX}_comment` ADD INDEX `comment_site` (`comment_site`);

-- CONTENT TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_content`;
CREATE TABLE `{DB_PREFIX}_content` (
  `content_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `content_id` VARBINARY(22) NOT NULL DEFAULT '',
  `content_objid` VARBINARY(22) NOT NULL DEFAULT '',
  `content_component` VARCHAR(60) DEFAULT '',
  `content_parent` VARBINARY(22) NOT NULL DEFAULT '0',
  `content_position` INT UNSIGNED NOT NULL DEFAULT 0,
  `content_type` VARCHAR(100) NOT NULL DEFAULT '',
  `content_level` INT UNSIGNED DEFAULT 0,
  `content_layout` VARCHAR(100) DEFAULT 'default',
  `content_url` VARCHAR(255) NOT NULL DEFAULT '',
  `content_idpath` VARBINARY(1000),
  `content_urlpath` VARCHAR(255) NOT NULL DEFAULT '',
  `content_titlepath` VARCHAR(255) NOT NULL DEFAULT '',
  `content_pagetitle` VARCHAR(255) NOT NULL DEFAULT '',
  `content_description` TEXT,
  `content_keywords` TEXT,
  `content_thumb` VARCHAR(255) NOT NULL DEFAULT '',
  `content_title` VARCHAR(100) NOT NULL DEFAULT '',
  `content_exerpt` TEXT,
  `content_content` TEXT,
  `content_options` TEXT,
  `content_draft` INT UNSIGNED DEFAULT 0,
  `content_groups` VARBINARY(1000) DEFAULT ',{REGISTERED_GROUP_ID},{ANONYMOUS_GROUP_ID},',
  `content_tags` TEXT,
  `content_language` INT UNSIGNED NOT NULL DEFAULT 0,
  `content_site` VARBINARY(22) DEFAULT '0',
  `content_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `content_modified` INT UNSIGNED NOT NULL DEFAULT 0,
  `content_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_content` ADD INDEX `content_objid` (`content_objid`);
ALTER TABLE `{DB_PREFIX}_content` ADD INDEX `content_parent` (`content_parent`);
ALTER TABLE `{DB_PREFIX}_content` ADD INDEX `content_urlpath` (`content_urlpath`);
ALTER TABLE `{DB_PREFIX}_content` ADD INDEX `content_position` (`content_position`);
ALTER TABLE `{DB_PREFIX}_content` ADD INDEX `content_url` (`content_url`);
ALTER TABLE `{DB_PREFIX}_content` ADD INDEX `content_language` (`content_language`);
ALTER TABLE `{DB_PREFIX}_content` ADD INDEX `content_site` (`content_site`);

INSERT INTO `{DB_PREFIX}_content`
(
    `content_id`, `content_objid`, `content_component`, `content_parent`,
    `content_position`, `content_layout`, `content_idpath`, `content_urlpath`,
    `content_titlepath`, `content_level`, `content_type`, `content_url`, `content_title`,
    `content_exerpt`, `content_content`, `content_options`, `content_pagetitle`,
    `content_description`, `content_keywords`, `content_draft`, `content_groups`,
    `content_language`, `content_site`, `content_created`, `content_modified`, `content_modifiedby`
)
VALUES(
    'WktO7v4dRyuoMbN9LhHgng', 'qbJk6CjKRhyIHw3nMw5WsA', '', '1',
    '0', 'default', '', 'en',
    'Home', 1, 'section', '', 'Home',
    '', '', '', 'Home',
    '', '', '0', ',{ADMIN_GROUP_ID},{REGISTERED_GROUP_ID},{ANONYMOUS_GROUP_ID},',
    '1', '0', '0', UNIX_TIMESTAMP(now()), '{DEFAULT_USERID}'
),(
    'gFAADSRSTAGkjbRBm6KpEA', 'qbJk6CjKRhyIHw3nMw5WsA', '', '1',
    '0', 'default', '', 'fr',
    'Accueil', 1, 'section', '', 'Accueil',
    '', '', '', 'Accueil',
    '', '', '0', ',{ADMIN_GROUP_ID},{REGISTERED_GROUP_ID},{ANONYMOUS_GROUP_ID},',
    '2', '0', '0', UNIX_TIMESTAMP(now()), '{DEFAULT_USERID}'
);

-- DATA TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_data`;
CREATE TABLE `{DB_PREFIX}_data` (
  `data_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_id` VARBINARY(22) NOT NULL DEFAULT '',
  `data_objid` VARBINARY(22) NOT NULL DEFAULT '',
  `data_type` VARCHAR(50) NOT NULL DEFAULT '',
  `data_parent` VARBINARY(22) NOT NULL DEFAULT '0',
  `data_category` VARBINARY(22) NOT NULL DEFAULT '',
  `data_position` INT UNSIGNED NOT NULL DEFAULT 0,
  `data_title` VARCHAR(255) DEFAULT '',
  `data_data` TEXT NOT NULL,
  `data_language` INT UNSIGNED NOT NULL DEFAULT 0,
  `data_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `data_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `data_modified` INT UNSIGNED NOT NULL DEFAULT 0,
  `data_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`data_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_data` ADD INDEX `data_objid` (`data_objid`);
ALTER TABLE `{DB_PREFIX}_data` ADD INDEX `data_parent` (`data_parent`);
ALTER TABLE `{DB_PREFIX}_data` ADD INDEX `data_category` (`data_category`);
ALTER TABLE `{DB_PREFIX}_data` ADD INDEX `data_position` (`data_position`);
ALTER TABLE `{DB_PREFIX}_data` ADD INDEX `data_language` (`data_language`);
ALTER TABLE `{DB_PREFIX}_data` ADD INDEX `data_site` (`data_site`);

-- GROUP TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_group`;
CREATE TABLE `{DB_PREFIX}_group` (
  `group_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id`  VARBINARY(22) NOT NULL DEFAULT '',
  `group_nameid` VARCHAR(100) NOT NULL DEFAULT '',
  `group_title` VARCHAR(100) NOT NULL DEFAULT '',
  `group_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `group_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `group_modified` INT UNSIGNED NOT NULL DEFAULT 0,
  `group_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_group` ADD INDEX `group_site` (`group_site`);

INSERT INTO `{DB_PREFIX}_group`
	(`group_id`, `group_nameid`,`group_title`,`group_site`,`group_created`,`group_modified`,`group_modifiedby`)
VALUES
    ('{ADMIN_GROUP_ID}', 'admin','GROUP_ADMIN','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'{DEFAULT_USERID}'),
	('{REGISTERED_GROUP_ID}', 'user','GROUP_REGISTERED','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'{DEFAULT_USERID}'),
	('{ANONYMOUS_GROUP_ID}', 'anonymous','GROUP_ANONYMOUS','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'{DEFAULT_USERID}')
;

/**
 * Lists
 */
DROP TABLE IF EXISTS `{DB_PREFIX}_list`;
CREATE TABLE `{DB_PREFIX}_list` (
    `list_pkid` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `list_id` VARBINARY(22) NOT NULL DEFAULT '',
    `list_tenant` VARBINARY(22) NOT NULL DEFAULT '0',
    `list_type` VARCHAR(100) NOT NULL DEFAULT '',
    `list_title` VARCHAR(100) NOT NULL DEFAULT '',
    `list_value` INT(10) NOT NULL,
    `list_options` TEXT,
    `list_deleted` INT(10) UNSIGNED DEFAULT '0',
    `list_deletedby` VARBINARY(22) NOT NULL DEFAULT '0',
    `list_created` INT(10) UNSIGNED DEFAULT '0',
    `list_createdby` VARBINARY(22) NOT NULL DEFAULT '0',
    `list_modified` INT(10) UNSIGNED DEFAULT '0',
    `list_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
    PRIMARY KEY (`list_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;

-- QUEUE TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_queue`;
CREATE TABLE `{DB_PREFIX}_queue` (
  `queue_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `queue_id` VARBINARY(22) NOT NULL DEFAULT '',
  `queue_type` VARCHAR(100) NOT NULL DEFAULT '',
  `queue_name` VARCHAR(100) NOT NULL DEFAULT '',
  `queue_component` VARCHAR(60) NOT NULL DEFAULT '',
  `queue_action` VARCHAR(45) NOT NULL DEFAULT '',
  `queue_status` INT UNSIGNED NOT NULL DEFAULT 0,
  `queue_data` TEXT NOT NULL,
  `queue_options` TEXT NOT NULL,
  `queue_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `queue_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `queue_modified` INT UNSIGNED NOT NULL DEFAULT 0,
  `queue_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`queue_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;

-- MENU TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_menu`;
CREATE TABLE `{DB_PREFIX}_menu` (
  `menu_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` VARBINARY(22) NOT NULL DEFAULT '',
  `menu_objid` VARBINARY(22) NOT NULL DEFAULT '0',
  `menu_component` VARCHAR(60) NOT NULL DEFAULT '',
  `menu_parent` VARBINARY(22) NOT NULL DEFAULT '1',
  `menu_position` INT UNSIGNED NOT NULL DEFAULT 0,
  `menu_type` VARCHAR(100) NOT NULL DEFAULT '',
  `menu_title` VARCHAR(255) NOT NULL DEFAULT '',
  `menu_link` VARCHAR(255) NOT NULL DEFAULT '',
  `menu_image` VARCHAR(255) NOT NULL DEFAULT '',
  `menu_options` TEXT NOT NULL,
  `menu_groups` VARBINARY(1000) NOT NULL DEFAULT '',
  `menu_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `menu_language` INT UNSIGNED NOT NULL DEFAULT 0,
  `menu_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `menu_modified` INT UNSIGNED NOT NULL DEFAULT 0,
  `menu_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_menu` ADD INDEX `menu_site` (`menu_site`);
ALTER TABLE `{DB_PREFIX}_menu` ADD INDEX `menu_groups` (`menu_groups`);
ALTER TABLE `{DB_PREFIX}_menu` ADD INDEX `menu_parent` (`menu_parent`);
ALTER TABLE `{DB_PREFIX}_menu` ADD INDEX `menu_type` (`menu_type`);

INSERT INTO `{DB_PREFIX}_menu`
	(`menu_id`,`menu_objid`, `menu_component`, `menu_parent`,`menu_position`,`menu_type`,`menu_title`,`menu_link`,`menu_image`,`menu_options`,`menu_groups`,`menu_site`,`menu_language`,`menu_created`,`menu_modified`,`menu_modifiedby`)
VALUES
    ('heusCjyNSciXixoTuiPaDA','Dr8s1YBkT6ezk7eIV2Zgww','','1',0,'folder','Home','','','',',{ADMIN_GROUP_ID},{REGISTERED_GROUP_ID},{ANONYMOUS_GROUP_ID},',0,'1',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'{DEFAULT_USERID}'),
    ('yT240uuVSiavLIX8ARZQDg','Dr8s1YBkT6ezk7eIV2Zgww','','1',0,'folder','Accueil','','','',',{ADMIN_GROUP_ID},{REGISTERED_GROUP_ID},{ANONYMOUS_GROUP_ID},',0,'2',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'{DEFAULT_USERID}');

-- METRIC TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_metric`;
CREATE TABLE `{DB_PREFIX}_metric` (
  `metric_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `metric_id` VARBINARY(22) NOT NULL DEFAULT '',
  `metric_idstr` VARBINARY(1000) NULL COMMENT '',
  `metric_section` VARBINARY(22) NOT NULL DEFAULT '',
  `metric_content` VARBINARY(22) NOT NULL COMMENT '',
  `metric_type` VARCHAR(40) NOT NULL COMMENT '',
  `metric_name` VARCHAR(40) NULL COMMENT '',
  `metric_value` TEXT NOT NULL COMMENT '',
  `metric_metric1` INT UNSIGNED NOT NULL COMMENT '',
  `metric_metric2` INT UNSIGNED NOT NULL COMMENT '',
  `metric_metric3` INT UNSIGNED NOT NULL COMMENT '',
  `metric_metric4` INT UNSIGNED NOT NULL COMMENT '',
  `metric_metric5` INT UNSIGNED NOT NULL COMMENT '',
  `metric_time` INT UNSIGNED NOT NULL COMMENT '',
  `metric_start` INT UNSIGNED NOT NULL COMMENT '',
  `metric_duration` INT UNSIGNED NOT NULL COMMENT '',
  `metric_language` INT UNSIGNED NOT NULL DEFAULT 0,
  `metric_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `metric_created` INT UNSIGNED NOT NULL COMMENT '',
  `metric_modified` INT UNSIGNED NOT NULL COMMENT '',
  `metric_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`metric_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;

-- PLUGIN TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_plugin`;
CREATE TABLE `{DB_PREFIX}_plugin` (
    `plugin_pkid` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `plugin_id` VARBINARY(22) NOT NULL DEFAULT '',
    `plugin_component` VARCHAR(100) NOT NULL DEFAULT '',
    `plugin_title` VARCHAR(100) NOT NULL DEFAULT '',
    `plugin_description` VARCHAR(100) NOT NULL DEFAULT '',
    `plugin_version` VARCHAR(25) NOT NULL DEFAULT '',
    `plugin_url` VARCHAR(255) NOT NULL DEFAULT '',
    `plugin_author` VARCHAR(50) NOT NULL DEFAULT '',
    `plugin_authorurl` VARCHAR(255) NOT NULL DEFAULT '',
    `plugin_company` VARCHAR(50) NOT NULL DEFAULT '',
    `plugin_companyurl` VARCHAR(255) NOT NULL DEFAULT '',
    `plugin_status` INT(10) NOT NULL,
    `plugin_options` TEXT,
    `plugin_tenant` VARBINARY(22) NOT NULL DEFAULT '0',
    `plugin_site` VARBINARY(22) NOT NULL DEFAULT '0',
    `plugin_deleted` INT(10) UNSIGNED DEFAULT '0',
    `plugin_deletedby` VARBINARY(22) NOT NULL DEFAULT '0',
    `plugin_created` INT(10) UNSIGNED DEFAULT '0',
    `plugin_createdby` VARBINARY(22) NOT NULL DEFAULT '0',
    `plugin_modified` INT(10) UNSIGNED DEFAULT '0',
    `plugin_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
    PRIMARY KEY (`plugin_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;

-- PERMISSION TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_permission`;
CREATE TABLE `{DB_PREFIX}_permission` (
  `permission_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `permission_id` VARBINARY(22) NOT NULL DEFAULT '',
  `permission_groupid` VARBINARY(22) NOT NULL DEFAULT '0',
  `permission_controller` VARCHAR(255) NOT NULL DEFAULT '',
  `permission_title` VARCHAR(255) NOT NULL DEFAULT '',
  `permission_description` TEXT NOT NULL,
  `permission_rights` INT UNSIGNED NOT NULL DEFAULT 0,
  `permission_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `permission_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `permission_modified` INT UNSIGNED NOT NULL DEFAULT 0,
  `permission_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`permission_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_permission` ADD INDEX `permission_site` (`permission_site`);
ALTER TABLE `{DB_PREFIX}_permission` ADD INDEX `permission_groupid` (`permission_groupid`);
ALTER TABLE `{DB_PREFIX}_permission` ADD INDEX `permission_controller` (`permission_controller`);

-- ROUTE TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_route`;
CREATE TABLE `{DB_PREFIX}_route` (
  `route_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `route_id` VARBINARY(22) NOT NULL DEFAULT '',
  `route_component` VARCHAR(60) NOT NULL DEFAULT '',
  `route_url` VARCHAR(255) NOT NULL DEFAULT '',
  `route_controller` TEXT NOT NULL,
  `route_language` INT UNSIGNED NOT NULL DEFAULT 0,
  `route_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `route_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `route_modified` INT UNSIGNED NOT NULL DEFAULT 0,
  `route_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`route_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_route` ADD INDEX `route_url` (`route_url`);
ALTER TABLE `{DB_PREFIX}_route` ADD INDEX `route_language` (`route_language`);
ALTER TABLE `{DB_PREFIX}_route` ADD INDEX `route_site` (`route_site`);

INSERT INTO `{DB_PREFIX}_route`
	(`route_id`,`route_component`,`route_url`,`route_controller`,`route_language`,`route_site`,`route_created`,`route_modified`,`route_modifiedby`)
VALUES
    ('O9T4S4YdTQ6QrAUiANXpCQ','','','default','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
    ('JTuhul2bSDC75JZJamI6lw','','{lang}','default','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
    ('hCbBd6ZERiOuAxOqD0HnSw','','{lang}/error404','error404','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
    ('9b1SWUm6SH29NJLFhHNXNQ','','{lang}/api/{component}/{type}/{name}/{op}','api','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('qvkZObZFRK2MwVBvhR112g','admin','{lang}/admin','admindashboard','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
    ('DVrkF7FKTx6A9PloLiw5cg','system','{lang}/admin/settings','settingmanager','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('Txm5IbHXQGa4OSxkJOYtAg','system','{lang}/admin/content','contentmanager','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('AeuyE54gS4CiesX4Kfek0w','system','{lang}/admin/widget','widgetmanager','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('BgLuq40IR8addgnD8T7Alg','system','{lang}/admin/menu','menumanager','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('rVdnWMGdRbSXKUWs7MJ5Mg','','{lang}/json/imageupload','imageupload','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('RurhviytQWWLwrRfI2mUmQ','','{lang}/json/fileupload','imageupload','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('A4e2AUfnSBWSiYOICS8RpA','','{lang}/admin/system','system','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('A399XQwmRZe9jTwskTf5tA','','{lang}/json/content/preview','contentmanager','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('X6jPKjTLT2ibHgoVwhA0tQ','','{lang}/json/data/{op}','data','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('cWzpfPuuTAO0anMydjDRng','','json/data/{op}','data','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('EGAETADLRjuM91gIYLqz3g','','{lang}admin/{type}/{class}/{op}','ajax','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('SMyfXs7HSduSHeM9QvzXkg','','{lang}/api/admin/{type}/{class}/{op}','ajax','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('JrFGPRj4R0uHdByNM5FtkA','','{lang}{component}/{type}/{class}/{op}','ajax','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
    ('S7Z5GFX6RACDJmjPOMVrUw','','{lang}/json/sharer/sendmail','sharer','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
    ('2z84NByvRFC13DwLomYAOw','user','{lang}/user/login','login','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('KOObtYFoR6mfp7rcnMNvoA','user','{lang}/user/logout','login','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('wfm3off2Q5uEx56SIXSJ4A','user','{lang}/user/profile','userprofile','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('gZFldgY1TRuzmvehqBcJRA','user','{lang}/user/resetpassword','recover','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('lH1RfECOTky821izIrkjMA','user','{lang}/user/register','register','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
    ('4hZur8r2SAuV0ov041IFaQ','','{lang}/admin/banner/add','banner','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('JKOAuloLTISX6Q4ut6TzOw','','{lang}/admin/banner','banner','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('JrV0ZlSERaeQF7U4spYHbQ','','{lang}/json/banner','banner','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('2KMUNlfCR1SWKGE6Y3kHow','','{lang}/admin/banner/edit/{objid}','banner','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('zlIftOKyQFSXU9uPDAjqdA','backupmanager','{lang}/admin/backup/{op}','backupmanager','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('JQy54c55Q42MT2YvXUvTHg','','{lang}/json/admin/product/model/{op}','product','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('eX8f2thIReOdEaYKjXNWVQ','','{lang}/json/admin/product/modelitem/{op}','product','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
    ('CF3IaHywRHOyT7OkIT37uQ','search','{lang}/search','search','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
    ('H6kGQZDyQ1aOFvqRD13B1w','system','{lang}/download/{fileextension}/{filename}','download','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
    ('b6XDBLF1RlilSsvQJPe4hA','user','{lang}/user/usermanager', 'usermanager', '0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
    ('qy40CIioSYyypVNLxX34mQ', 'user', '{lang}/user/usergroupmanager', 'usergroupmanager', '0','0', UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
    ('ROEuQJy5Twmh1inwKC5fhA', 'user', '{lang}/user/usermailingmanager', 'usermailingmanager', '0','0', UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
    ('EHhQLfzRTZiHuKHJRCbJ7A','plugin', '{lang}/admin/plugins', 'plugin', '0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0');

-- SETTING TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_setting`;
CREATE TABLE `{DB_PREFIX}_setting` (
  `setting_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `setting_id` VARBINARY(22) NOT NULL DEFAULT '',
  `setting_objid` VARBINARY(22) NOT NULL DEFAULT '0',
  `setting_cid` VARBINARY(22) NOT NULL DEFAULT '0',
  `setting_name` VARCHAR(50) NOT NULL DEFAULT '',
  `setting_value` TEXT,
  `setting_type` VARCHAR(50) NOT NULL DEFAULT '',
  `setting_options` TEXT,
  `setting_title` TEXT,
  `setting_description` TEXT,
  `setting_language` INT UNSIGNED NOT NULL DEFAULT 0,
  `setting_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `setting_created` INT UNSIGNED DEFAULT 0,
  `setting_modified` INT UNSIGNED DEFAULT 0,
  `setting_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`setting_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_setting` ADD INDEX `setting_objid` (`setting_objid`);
ALTER TABLE `{DB_PREFIX}_setting` ADD INDEX `setting_cid` (`setting_cid`);
ALTER TABLE `{DB_PREFIX}_setting` ADD INDEX `setting_language` (`setting_language`);
ALTER TABLE `{DB_PREFIX}_setting` ADD INDEX `setting_site` (`setting_site`);

INSERT INTO `{DB_PREFIX}_setting`
	(`setting_id`,`setting_objid`,`setting_cid`,`setting_name`,`setting_value`,`setting_type`,`setting_options`,`setting_title`,`setting_description`,`setting_language`,`setting_site`,`setting_created`,`setting_modified`,`setting_modifiedby`)
VALUES
    ('YDS9KKkFSxeDo42Y74JDGg','3DWg9oMrSWuJF4FcHOjUQA','QnP9HwX7T6GLQGpvHrHEkw','sitename','My site','text','','SYSTEM_SETTINGS_SITENAME','SYSTEM_SETTINGS_SITENAME_DESCRIPTION','1','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('Q9dRkJtrQmCAmDJphRN2tw','3DWg9oMrSWuJF4FcHOjUQA','QnP9HwX7T6GLQGpvHrHEkw','sitename','My site','text','','SYSTEM_SETTINGS_SITENAME','SYSTEM_SETTINGS_SITENAME_DESCRIPTION','2','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('abol6xE1TNmPVJMdH3Gbag','1FdidOXNQJSaSMj5KcSE9A','QnP9HwX7T6GLQGpvHrHEkw','siteslogan','Powered by LucidCMS','text','','SYSTEM_SETTINGS_SITESLOGAN','SYSTEM_SETTINGS_SITESLOGAN_DESCRIPTION','1','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('F9FMdDPGRaiJ6ErNhDjHjg','1FdidOXNQJSaSMj5KcSE9A','QnP9HwX7T6GLQGpvHrHEkw','siteslogan','Powered by LucidCMS','text','','SYSTEM_SETTINGS_SITESLOGAN','SYSTEM_SETTINGS_SITESLOGAN_DESCRIPTION','2','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('mxUXR0gKQLywPXeKnVuiDw','r9jAkWyoQfqumUZiFR7aOQ','QnP9HwX7T6GLQGpvHrHEkw','sitekeywords','CMS,LucidCMS,Content Management System,Translucide','textarea','','SYSTEM_SETTINGS_SITEKEYWORDS','SYSTEM_SETTINGS_SITEKEYWORDS_DESCRIPTION','1','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('QprKD5j1QGqeE7cl3blkpg','r9jAkWyoQfqumUZiFR7aOQ','QnP9HwX7T6GLQGpvHrHEkw','sitekeywords','CMS,LucidCMS,Content Management System,Translucide','textarea','','SYSTEM_SETTINGS_SITEKEYWORDS','SYSTEM_SETTINGS_SITEKEYWORDS_DESCRIPTION','2','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('0EEzswDDRKqA6YZlScFinA','gRjcHc2MTXypmijinXZmeQ','QnP9HwX7T6GLQGpvHrHEkw','sitelanguages', ',1,2,', 'selectmultiple', '1=English,2=Fran&ccedil;ais,3=Italian,4=Spanish', 'SYSTEM_SETTINGS_ENABLEDLANGUAGES', 'SYSTEM_SETTINGS_ENABLEDLANGUAGES_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('CGsyQj8ERY6SZLVe4MBFIA','tMV4eiMvQGuNeAI4k0i5zw','QnP9HwX7T6GLQGpvHrHEkw','defaultlanguage','2','selectlanguage','','SYSTEM_SETTINGS_DEFAULTLANGUAGE','SYSTEM_SETTINGS_DEFAULTLANGUAGE_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('Y4wHliMgSGiStC6CLCrZ9Q','rRpIE8DXQdeiQMmMvgc5QA','QnP9HwX7T6GLQGpvHrHEkw','adminmail','{DEFAULT_USEREMAIL}','text','','SYSTEM_SETTINGS_ADMINMAIL','SYSTEM_SETTINGS_ADMINMAIL_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('wBX7XdwxQjGMOaGHJGKEnQ','g1B5CAMKRoW9mdjKTsJIMw','QnP9HwX7T6GLQGpvHrHEkw','theme','{DEFAULT_THEME}','selecttheme','','SYSTEM_SETTINGS_THEME','SYSTEM_SETTINGS_THEME_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('uEucWi62RMmzXJ16RZu0GQ','1XLPuRf4Q8Cd2DXUxXyjyQ','QnP9HwX7T6GLQGpvHrHEkw','admintheme','admin','selecttheme','','SYSTEM_SETTINGS_ADMINTHEME','SYSTEM_SETTINGS_ADMINTHEME_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('wqO5fUjxSQS7WT9Zu0HtvA','bav6ze9iRSONaM2784RSMA','QnP9HwX7T6GLQGpvHrHEkw','closesite','0','yesno','','SYSTEM_SETTINGS_CLOSESITE','SYSTEM_SETTINGS_CLOSESITE_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('cZT58XnMRxmmuidALV1d4A','2h0ahimwREq0fW0HGI0TOA','QnP9HwX7T6GLQGpvHrHEkw','siteclosedmsg','Site closed for maintenance. Please come back later.','textarea','','SYSTEM_SETTINGS_SITECLOSEDMESSAGE','SYSTEM_SETTINGS_SITECLOSEDMESSAGE_DESCRIPTION','1','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('3tf6XKFhQLyfED7mE0HcUA','2h0ahimwREq0fW0HGI0TOA','QnP9HwX7T6GLQGpvHrHEkw','siteclosedmsg','Site ferm&eacute; pour maintenance. Veuillez revenir plus tard.','textarea','','SYSTEM_SETTINGS_SITECLOSEDMESSAGE','SYSTEM_SETTINGS_SITECLOSEDMESSAGE_DESCRIPTION','2','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('dEj6xLskQFG8y6Yj4cY06w','i30Tq5ikS4+hHUTBqXLy4g','QnP9HwX7T6GLQGpvHrHEkw','developmentmode','1','yesno','','SYSTEM_SETTINGS_DEVELOPMENTMODE','SYSTEM_SETTINGS_DEVELOPMENTMODE_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('H7WVE8xZQ6qYz2qs84x97g','umaQIxmbSOeyZFgf78C5Ew','QnP9HwX7T6GLQGpvHrHEkw','loglevel','0','select','0=SYSTEM_SETTINGS_LOGLEVEL_ALL,1=SYSTEM_SETTINGS_LOGLEVEL_ERRORS,2=SYSTEM_SETTINGS_LOGLEVEL_WARNINGS,3=SYSTEM_SETTINGS_LOGLEVEL_NOTICES','SYSTEM_SETTINGS_LOGLEVEL','SYSTEM_SETTINGS_LOGLEVEL_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('eMLNBYQNSHiRZ6uTU2Eleg','bveAqmRUQ2KPpqDT74eUcg','QnP9HwX7T6GLQGpvHrHEkw','logdestination','0','select','0=SYSTEM_SETTINGS_LOGDESTINATION_SCREEN,1=SYSTEM_SETTINGS_LOGDESTINATION_FILE,2=SYSTEM_SETTINGS_LOGDESTINATION_SYSLOG,3=SYSTEM_SETTINGS_LOGDESTINATION_MAIL','SYSTEM_SETTINGS_LOGDESTINATION','SYSTEM_SETTINGS_LOGDESTINATION_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('MVX0ljPXTlqSJugtUzMulQ','7HxKe3fKQDyQYpcqqqebPg','QnP9HwX7T6GLQGpvHrHEkw','tinypicturesize','40,40','imagesize','','SYSTEM_SETTINGS_TINYPICTURESIZE','SYSTEM_SETTINGS_TINYPICTURESIZE_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('8dUMgUNXSNulz22EOBWzXw','WaRLEv7lQ5KVdJyBXxHqwg','QnP9HwX7T6GLQGpvHrHEkw','smallpicturesize','80,80','imagesize','','SYSTEM_SETTINGS_SMALLPICTURESIZE','SYSTEM_SETTINGS_SMALLPICTURESIZE_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('o4jX27gNQ3aLHbg5Y3rsEQ','TIhH0sK0Sh2Ac06UZVNmPw','QnP9HwX7T6GLQGpvHrHEkw','mediumpicturesize','200,200','imagesize','','SYSTEM_SETTINGS_MEDIUMPICTURESIZE','SYSTEM_SETTINGS_MEDIUMPICTURESIZE_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('Zi0yel8oQhOinKyGSIAKwA','lGGzPWAPTh2mDtiqqiJaHg','QnP9HwX7T6GLQGpvHrHEkw','largepicturesize','800,800','imagesize','','SYSTEM_SETTINGS_LARGEPICTURESIZE','SYSTEM_SETTINGS_LARGEPICTURESIZE_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('MGn55oSQRFWNyepwrqJqHw','mjs4pCL3QjCbEFguAu3UlQ','QnP9HwX7T6GLQGpvHrHEkw','enlargedpicturesize','800,800','imagesize','','SYSTEM_SETTINGS_ENLARGEDPICTURESIZE','SYSTEM_SETTINGS_ENLARGEDPICTURESIZE_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('5wZ2VTcrQ4yoiQ3m043fVA','j2iHOBupQ06TVhmsUP8j8w','QnP9HwX7T6GLQGpvHrHEkw','activatelargecache','0','yesno','','SYSTEM_SETTINGS_ACTIVATELARGECACHE','SYSTEM_SETTINGS_ACTIVATELARGECACHE_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('KHQ8vtvGT563JzJJPTXWcg','nBkzcylrTnqW4SYmrEwRbg','QnP9HwX7T6GLQGpvHrHEkw','largecachetime','0','text','','SYSTEM_SETTINGS_LARGECACHETIME','SYSTEM_SETTINGS_LARGECACHETIME_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('KHQ8vtvGT563JzJJPTXWcg','PZsrTxqnRhy0FCU86khEMQ','QnP9HwX7T6GLQGpvHrHEkw','activatedbcache','0','yesno','','SYSTEM_SETTINGS_ACTIVATEDBCACHE','SYSTEM_SETTINGS_ACTIVATEDBCACHE_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('FqCjdCzuQxG6Z70QoxnkHQ','3qMe9kRyRKmxJQGL3gW1nQ','QnP9HwX7T6GLQGpvHrHEkw','allowedfiletypes','backup,doc,pdf,docx,xlsx,txt,avi,mp4,mp3,m4a,iso,rar,zip,arj,ai,psd,jpg,png,gif,svg','text','','SYSTEM_SETTINGS_ALLOWEDFILETYPES','SYSTEM_SETTINGS_ALLOWEDFILETYPES_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('9mGsbDXYSMuk8Q0fVQoFFw','LghHcSq3RNOryBI4oJY95Q','QnP9HwX7T6GLQGpvHrHEkw','sessionlifetime','30','text','','SYSTEM_SETTINGS_SESSIONLIFETIME','SYSTEM_SETTINGS_SESSIONLIFETIME_DESCRIPTION','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('KbcwnIVDSQW6P2WUsJnkmQ','PCs9jWyWSnSe9ZVZnm9gkA','194Adx0eSeiVdjhba72abA','contactus_phone','1-800-MY-PHONE','text','','CONTACTUS_SETTING_PHONE','CONTACTUS_SETTING_PHONE_DESC','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('VOPftYT1SJet2VqEGZ8QwQ','hm224lw0QpKMrvXDb6Z36g','194Adx0eSeiVdjhba72abA','contactus_email','','text','','CONTACTUS_SETTING_EMAIL','CONTACTUS_SETTING_EMAIL_DESC','0','{DEFAULT_SITEID}',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0');

-- SETTINGCATEGORY TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_settingcategory`;
CREATE TABLE `{DB_PREFIX}_settingcategory` (
  `settingcategory_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `settingcategory_id` VARBINARY(22) NOT NULL DEFAULT '',
  `settingcategory_objid` VARBINARY(22) NOT NULL DEFAULT '',
  `settingcategory_name` VARCHAR(50) NOT NULL DEFAULT '',
  `settingcategory_title` VARCHAR(50) NOT NULL DEFAULT '',
  `settingcategory_language` INT UNSIGNED NOT NULL DEFAULT 0,
  `settingcategory_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `settingcategory_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `settingcategory_modified` INT UNSIGNED NOT NULL DEFAULT 0,
  `settingcategory_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`settingcategory_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_settingcategory` ADD INDEX `settingcategory_objid` (`settingcategory_objid`);
ALTER TABLE `{DB_PREFIX}_settingcategory` ADD INDEX `settingcategory_language` (`settingcategory_language`);
ALTER TABLE `{DB_PREFIX}_settingcategory` ADD INDEX `settingcategory_site` (`settingcategory_site`);

INSERT INTO `{DB_PREFIX}_settingcategory`
	(`settingcategory_id`,`settingcategory_objid`,`settingcategory_name`, `settingcategory_title`,`settingcategory_language`,`settingcategory_site`,`settingcategory_created`,`settingcategory_modified`,`settingcategory_modifiedby`)
VALUES
    ('NShMV6KjRwu40ci7ydldAQ', 'QnP9HwX7T6GLQGpvHrHEkw','general','SYSTEM_SETTINGS_CATEGORY_GENERAL','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('UGBqYEivS22xEmYe0UgyiA', 'KpuHUC5UQeypihllVYtxmQ','user','SYSTEM_SETTINGS_CATEGORY_USER','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0'),
	('atWKFN9gQAuMXNF1DnjXuA', '194Adx0eSeiVdjhba72abA','contactus','CONTACTUS_SETTINGCATEGORY','0','0',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0')
;

-- SESSION TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_session`;
CREATE TABLE `{DB_PREFIX}_session` (
  `session_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `session_id` VARBINARY(22) NOT NULL DEFAULT '',
  `session_sid` VARCHAR(150) UNIQUE,
  `session_data` BLOB NOT NULL,
  `session_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `session_modified` INT UNSIGNED NOT NULL DEFAULT 0,
  `session_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;

-- SITE TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_site`;
CREATE TABLE `{DB_PREFIX}_site` (
  `site_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `site_id` VARBINARY(22) NOT NULL DEFAULT '',
  `site_url` VARCHAR(255) NOT NULL DEFAULT '',
  `site_root` VARCHAR(255) NOT NULL DEFAULT '',
  `site_default` INT(1) UNSIGNED NOT NULL DEFAULT '0',
  `site_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `site_modified` INT UNSIGNED NOT NULL DEFAULT 0,
  `site_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`site_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;

INSERT INTO `{DB_PREFIX}_site`
	(`site_id`,`site_url`,`site_root`,`site_default`,`site_created`,`site_modified`,`site_modifiedby`)
	VALUES('{DEFAULT_SITEID}','{url}','{root}','1',UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0');

-- TAG TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_tag`;
CREATE TABLE `{DB_PREFIX}_tag` (
  `tag_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `tag_id` VARBINARY(22) NOT NULL DEFAULT '',
  `tag_objid` VARBINARY(22) NOT NULL DEFAULT '',
  `tag_title` VARCHAR(45) NULL COMMENT '',
  `tag_weight` INT UNSIGNED NOT NULL COMMENT '',
  `tag_language` INT UNSIGNED NOT NULL COMMENT '',
  `tag_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `tag_created` INT UNSIGNED NOT NULL COMMENT '',
  `tag_modified` INT UNSIGNED NOT NULL COMMENT '',
  `tag_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tag_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;

-- Tasks table
DROP TABLE IF EXISTS `{DB_PREFIX}_task`;
CREATE TABLE `{DB_PREFIX}_task` (
  `task_pkid` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` VARBINARY(22) NOT NULL DEFAULT '',
  `task_tenant` VARBINARY(22) NOT NULL DEFAULT '0',
  `task_type` varchar(45) DEFAULT NULL,
  `task_name` varchar(45) DEFAULT NULL,
  `task_component` varchar(60) DEFAULT NULL,
  `task_action` varchar(60) DEFAULT NULL,
  `task_user` VARBINARY(22) NOT NULL DEFAULT '0',
  `task_maxruns` int(10) unsigned NOT NULL DEFAULT '0',
  `task_begin` int(10) unsigned NOT NULL,
  `task_end` int(10) unsigned NOT NULL,
  `task_interval` int(10) unsigned NOT NULL,
  `task_priority` int(10) unsigned NOT NULL,
  `task_active` tinyint(1) unsigned NOT NULL,
  `task_lastduration` int(10) unsigned NOT NULL,
  `task_lastrun` int(10) unsigned NOT NULL,
  `task_data` text,
  `task_status` int(10) unsigned NOT NULL DEFAULT '0',
  `task_runcount` int(10) unsigned NOT NULL DEFAULT '0',
  `task_errorcount` int(10) unsigned NOT NULL DEFAULT '0',
  `task_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `task_created` int(10) unsigned NOT NULL DEFAULT '0',
  `task_createdby` VARBINARY(22) NOT NULL DEFAULT '0',
  `task_modified` int(10) unsigned NOT NULL DEFAULT '0',
  `task_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  `task_deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `task_deletedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`task_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;

-- TAXONOMY TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_taxonomy`;
CREATE TABLE `{DB_PREFIX}_taxonomy` (
  `taxonomy_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `taxonomy_id` VARBINARY(22) NOT NULL DEFAULT '',
  `taxonomy_objid` VARBINARY(22) NOT NULL DEFAULT '',
  `taxonomy_section` VARBINARY(22) NOT NULL DEFAULT '',
  `taxonomy_parent` VARBINARY(22) NOT NULL DEFAULT '',
  `taxonomy_type` INT UNSIGNED NOT NULL COMMENT '', /* 1 = Node, 2 = Leaf */
  `taxonomy_term` VARCHAR(50) NULL COMMENT '',
  `taxonomy_weight` INT UNSIGNED NOT NULL COMMENT '',
  `taxonomy_status` INT UNSIGNED NOT NULL COMMENT '',
  `taxonomy_language` INT UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `taxonomy_created` INT UNSIGNED NOT NULL COMMENT '',
  `taxonomy_modified` INT UNSIGNED NOT NULL COMMENT '',
  `taxonomy_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`taxonomy_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;

-- TEMPLATE TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_template`;
CREATE TABLE `{DB_PREFIX}_template` (
  `template_pkid` INT NOT NULL AUTO_INCREMENT,
  `template_id` VARBINARY(22) NOT NULL DEFAULT '',
  `template_objid` VARBINARY(22) NOT NULL DEFAULT '',
  `template_type` VARCHAR(20) NULL,
  `template_name` VARCHAR(100) NULL,
  `template_src` TEXT NULL,
  `template_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `template_created` INT UNSIGNED NULL,
  `template_modified` INT UNSIGNED NULL,
  `template_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;


-- USER TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_user`;
CREATE TABLE `{DB_PREFIX}_user` (
  `user_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` VARBINARY(22) NOT NULL DEFAULT '',
  `user_name` VARCHAR(100) NOT NULL DEFAULT '',
  `user_email` VARCHAR(100) NOT NULL DEFAULT '',
  `user_encryptionmethod` VARCHAR(10) NULL DEFAULT 'scrypt' COMMENT '',
  `user_pass` BLOB NOT NULL,
  `user_salt` BLOB NOT NULL,
  `user_groups` VARBINARY(1000) NOT NULL DEFAULT '',
  `user_lastlogin` INT NOT NULL DEFAULT 0,
  `user_timezone` VARCHAR(100) NOT NULL DEFAULT '',
  `user_failedattempts` INT(10) NOT NULL DEFAULT '0',
  `user_sid` VARCHAR(32) NOT NULL DEFAULT '',
  `user_ip` VARCHAR(32) NOT NULL DEFAULT '',
  `user_actkey` VARCHAR(32) DEFAULT '',
  `user_allowlogin` INT(1) DEFAULT '1',
  `user_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `user_tenant` VARBINARY(1000) NOT NULL DEFAULT ',{DEFAULT_TENANT},',
  `user_language`  INT UNSIGNED NOT NULL DEFAULT 0,
  `user_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `user_modified` INT  NOT NULL DEFAULT 0,
  `user_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  `user_deleted` INT  NOT NULL DEFAULT 0,
  `user_deletedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_user` ADD INDEX `user_email` (`user_email`);
ALTER TABLE `{DB_PREFIX}_user` ADD INDEX `user_groups` (`user_groups`);
ALTER TABLE `{DB_PREFIX}_user` ADD INDEX `user_site` (`user_site`);

INSERT INTO `{DB_PREFIX}_user`
	(user_id,user_name,user_email,user_pass,user_salt,user_groups,
        user_timezone,user_site,user_tenant,
        user_created,user_modified,user_modifiedby)
VALUES
    ('{DEFAULT_USERID}','admin','{DEFAULT_USEREMAIL}','','',',{ADMIN_GROUP_ID},{REGISTERED_GROUP_ID},{ANONYMOUS_GROUP_ID},',
        '','0',',{DEFAULT_TENANT},',
        UNIX_TIMESTAMP(now()),UNIX_TIMESTAMP(now()),'0');

-- PROFILE TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_profile`;
CREATE TABLE `{DB_PREFIX}_profile` (
  `profile_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `profile_id` VARBINARY(22) NOT NULL DEFAULT '0',
  `profile_userid` VARBINARY(22) NOT NULL DEFAULT '0',
  `profile_key` VARCHAR(128) NOT NULL DEFAULT '',
  `profile_value` TEXT,
  `profile_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `profile_created` INT UNSIGNED NULL DEFAULT 0,
  `profile_modified` INT UNSIGNED NULL DEFAULT 0,
  `profile_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`profile_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_profile` ADD INDEX `profile_userid` (`profile_userid`);
ALTER TABLE `{DB_PREFIX}_profile` ADD INDEX `profile_key` (`profile_key`);
ALTER TABLE `{DB_PREFIX}_profile` ADD INDEX `profile_site` (`profile_site`);

-- PROFILEFIELD TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_profilefield`;
CREATE TABLE `{DB_PREFIX}_profilefield` (
  `profilefield_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `profilefield_id` VARBINARY(22) NOT NULL DEFAULT '',
  `profilefield_objid` VARBINARY(22) NOT NULL DEFAULT '',
  `profilefield_position` INT UNSIGNED NOT NULL DEFAULT 0,
  `profilefield_section` VARBINARY(22) NOT NULL DEFAULT '0',
  `profilefield_component` VARCHAR(50) NOT NULL DEFAULT '',
  `profilefield_type` VARCHAR(30) NOT NULL DEFAULT '',
  `profilefield_key` VARCHAR(128) NOT NULL DEFAULT '',
  `profilefield_title` VARCHAR(128) NOT NULL DEFAULT '',
  `profilefield_defaultvalue` VARCHAR(128) NOT NULL DEFAULT '',
  `profilefield_options` TEXT NOT NULL,
  `profilefield_language` INT UNSIGNED NOT NULL DEFAULT 0,
  `profilefield_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `profilefield_created` INT UNSIGNED NULL DEFAULT 0,
  `profilefield_modified` INT UNSIGNED NULL DEFAULT 0,
  `profilefield_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`profilefield_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_profilefield` ADD INDEX `profilefield_type` (`profilefield_type`);
ALTER TABLE `{DB_PREFIX}_profilefield` ADD INDEX `profilefield_key` (`profilefield_key`);
ALTER TABLE `{DB_PREFIX}_profilefield` ADD INDEX `profilefield_site` (`profilefield_site`);

-- PROFILESECTION TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_profilesection`;
CREATE TABLE `{DB_PREFIX}_profilesection` (
  `profilesection_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `profilesection_id` VARBINARY(22) NOT NULL DEFAULT '',
  `profilesection_objid` VARBINARY(22) NOT NULL DEFAULT '',
  `profilesection_title` VARCHAR(128) NULL,
  `profilesection_language` INT UNSIGNED NOT NULL DEFAULT 0,
  `profilesection_site` VARBINARY(22) NOT NULL DEFAULT '',
  `profilesection_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `profilesection_modified` INT UNSIGNED NOT NULL DEFAULT 0,
  `profilesection_modifiedby` VARBINARY(22) NOT NULL DEFAULT '',
  PRIMARY KEY (`profilesection_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_profilesection` ADD INDEX `profilesection_id` (`profilesection_id`);


/*
 * Tenant Table
 **/
DROP TABLE IF EXISTS `{DB_PREFIX}_tenant`;
CREATE TABLE `{DB_PREFIX}_tenant` (
    `tenant_pkid` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `tenant_id` VARBINARY(22) NOT NULL DEFAULT '0',
    `tenant_name` VARCHAR(30) NOT NULL,
    `tenant_default` INT(1) NOT NULL DEFAULT '0',
    `tenant_created` INT(10) UNSIGNED DEFAULT 0,
    `tenant_createdby` VARBINARY(22) NOT NULL DEFAULT '0',
    `tenant_modified` INT(10) UNSIGNED DEFAULT 0,
    `tenant_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
    `tenant_deleted` INT(10) UNSIGNED DEFAULT 0,
    `tenant_deletedby` VARBINARY(22) NOT NULL DEFAULT '0',
    PRIMARY KEY (`tenant_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;

INSERT INTO `{DB_PREFIX}_tenant` (tenant_id, tenant_name, tenant_default, tenant_created, tenant_createdby, tenant_modified, tenant_modifiedby, tenant_deleted, tenant_deletedby)
VALUES ('{DEFAULT_TENANT}', 'Default', 1, UNIX_TIMESTAMP(CURDATE()), '0', UNIX_TIMESTAMP(CURDATE()), '0', '0', '0');

-- WIDGET TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_widget`;
CREATE TABLE `{DB_PREFIX}_widget` (
  `widget_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `widget_id` VARBINARY(22) NOT NULL DEFAULT '',
  `widget_objid` VARBINARY(22) NOT NULL DEFAULT '',
  `widget_parent` VARBINARY(22) NOT NULL DEFAULT '0',
  `widget_parenttype` VARCHAR(60) NOT NULL DEFAULT '',
  `widget_component` VARCHAR(60) NOT NULL DEFAULT '',
  `widget_zone` VARCHAR(60) NOT NULL DEFAULT '',
  `widget_position` INT UNSIGNED NOT NULL DEFAULT 0,
  `widget_title` VARCHAR(255) NOT NULL DEFAULT '',
  `widget_name` VARCHAR(60) NOT NULL DEFAULT '',
  `widget_tag` VARCHAR(60) NOT NULL DEFAULT '',
  `widget_type` VARCHAR(60) NOT NULL DEFAULT '',
  `widget_options` TEXT NOT NULL,
  `widget_urltrigger` TEXT NOT NULL,
  `widget_cancelurltrigger` TEXT NOT NULL,
  `widget_requesttrigger` TEXT NOT NULL,
  `widget_sectiontrigger` VARCHAR(60) NOT NULL DEFAULT '',
  `widget_controllertrigger` VARCHAR(60) NOT NULL DEFAULT '',
  `widget_language`  INT UNSIGNED NOT NULL DEFAULT 0,
  `widget_groups` VARBINARY(1000) NOT NULL DEFAULT '0',
  `widget_site` VARBINARY(22) NOT NULL DEFAULT '0',
  `widget_created` INT UNSIGNED NOT NULL DEFAULT 0,
  `widget_modified` INT UNSIGNED NOT NULL DEFAULT 0,
  `widget_modifiedby` VARBINARY(22) NOT NULL DEFAULT '0',
  PRIMARY KEY (`widget_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_widget` ADD INDEX `widget_objid` (`widget_objid`);
ALTER TABLE `{DB_PREFIX}_widget` ADD INDEX `widget_type` (`widget_type`);
ALTER TABLE `{DB_PREFIX}_widget` ADD INDEX `widget_tag` (`widget_tag`);
ALTER TABLE `{DB_PREFIX}_widget` ADD INDEX `widget_site` (`widget_site`);
ALTER TABLE `{DB_PREFIX}_widget` ADD INDEX `widget_language` (`widget_language`);

-- WIDGETZONE TABLE
DROP TABLE IF EXISTS `{DB_PREFIX}_widgetzone`;
CREATE TABLE `{DB_PREFIX}_widgetzone` (
  `widgetzone_pkid` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `widgetzone_id` VARBINARY(22) NOT NULL DEFAULT '',
  `widgetzone_objid` VARBINARY(22) NOT NULL DEFAULT '',
  `widgetzone_name` VARCHAR(60) NULL COMMENT '',
  `widgetzone_provider` VARCHAR(60) NULL COMMENT '',
  `widgetzone_title` VARCHAR(60) NULL COMMENT '',
  `widgetzone_template` INT UNSIGNED NULL COMMENT '',
  `widgetzone_options` TEXT NULL COMMENT '',
  `widgetzone_site` VARBINARY(22) NOT NULL DEFAULT '',
  `widgetzone_created` INT UNSIGNED NULL COMMENT '',
  `widgetzone_modified` INT UNSIGNED NULL COMMENT '',
  `widgetzone_modifiedby` VARBINARY(22) NOT NULL DEFAULT '',
  PRIMARY KEY (`widgetzone_pkid`)
)
CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{DB_PREFIX}_widgetzone` ADD INDEX `widgetzone_objid` (`widgetzone_objid`);
