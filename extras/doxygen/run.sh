#/bin/bash
#Get the folder this script resides in and cd to it.
SCRIPT_PATH="${BASH_SOURCE[0]}";
if ([ -h "${SCRIPT_PATH}" ]) then
  while([ -h "${SCRIPT_PATH}" ]) do SCRIPT_PATH=`readlink "${SCRIPT_PATH}"`; done
fi
pushd . > /dev/null
cd `dirname ${SCRIPT_PATH}` > /dev/null
SCRIPT_PATH=`pwd`;
popd  > /dev/null
cd $SCRIPT_PATH
rm -R -f ../doc/api/*
doxygen ./doxygen.cfg &> errors.log
ls -s ../doc/api/html/index.html ../doc
mkdir ../doc/api/html/img
cp -R ./img/* ../doc/api/html/img
