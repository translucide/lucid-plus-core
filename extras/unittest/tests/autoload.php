<?php
//Load default site profile
include_once(dirname(dirname(dirname(dirname(__FILE__)))).'/core/constants.php');
include_once(dirname(dirname(dirname(dirname(__FILE__)))).'/config/path.php');
include_once(SITESROOT.'default/conf/config.php');


spl_autoload_register(function ($class) {
    $baseDir = ROOT.'/';
    $class = strtolower($class);
    if (file_exists($baseDir.'core/'.$class.'.php')) {
        include $baseDir.'core/'.$class.'.php';  
    }
    if (file_exists($baseDir.'core/db/'.$class.'.php')) {
        include $baseDir.'core/db/'.$class.'.php';  
    }
    if (file_exists($baseDir.'core/db/datatype/'.$class.'.php')) {
        include $baseDir.'core/db/datatype/'.$class.'.php';  
    }
    if (file_exists($baseDir.'core/db/db/'.$class.'.php')) {
        include $baseDir.'core/db/db/'.$class.'.php';  
    }
    if (file_exists($baseDir.'core/user/'.$class.'.php')) {
        include $baseDir.'core/user/'.$class.'.php';  
    }
    if (file_exists($baseDir.'lib/form/'.$class.'.php')) {
        include $baseDir.'lib/form/'.$class.'.php';  
    }
    if (file_exists($baseDir.'lib/form/converter/'.$class.'.php')) {
        include $baseDir.'lib/form/converter/'.$class.'.php';  
    }
    if (file_exists($baseDir.'lib/form/field/'.$class.'.php')) {
        include $baseDir.'lib/form/field/'.$class.'.php';
    }
});
?>