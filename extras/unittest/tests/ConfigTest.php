<?php
include(dirname(__FILE__).'/autoload.php');

class ConfigTest extends PHPUnit_Framework_TestCase
{
    /**
     * Checks if current loaded config has trailing slashes
     * after each path and urls constants
     */
    public function testUrlHasTrailingSlash(){
		$this->assertEquals('/',substr(URL,-1,1));
    }

    /**
     * Checks if current loaded config has trailing slashes
     * after each path and urls constants
     */
    public function testRootHasTrailingSlash(){
		$this->assertEquals('/',substr(ROOT,-1,1));
    }

    /**
     * Checks if current loaded config has trailing slashes
     * after each path and urls constants
     */
    public function testDataRootHasTrailingSlash(){
		$this->assertEquals('/',substr(DATAROOT,-1,1));
    }
}
?>