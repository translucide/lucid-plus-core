<?php
/**
 * Url unit tests class
 *
 * Provides unittests for Url service (core/url.php
 * July 4, 2015

 * @version 	0.1
 * @package 	unittest
 * @author 		Translucide
 *
 */
include(dirname(dirname(dirname(__FILE__))).'/autoload.php');

class UrlTest extends PHPUnit_Framework_TestCase
{
    /**
     * Url::parse : missing protocol test
     *
     * Expected return value :
     * bool false
     */
    public function testMissingProtocol()
    {
        $urlObj = new Url();
        $ret = $urlObj->parse(substr(URL,strpos('//',URL)+2));
        $expected = false;
        $this->assertEquals($expected, $ret);
    }

    /**
     * Url::parse : malformed protocol test
     *
     * Expected return value :
     * bool false
     */
    public function testMalformedProtocol()
    {
        $urlObj = new Url();
        $ret = $urlObj->parse('http:/'.substr(URL,strpos(URL,'//')+2));
        $expected = false;
        $this->assertEquals($expected, $ret);
    }

    /**
     * Url::parse : invalid protocol test
     *
     * Expected return value :
     * bool false
     */
    public function testInvalidProtocol()
    {
        $urlObj = new Url();
        $ret = $urlObj->parse('http34://'.substr(URL,strpos(URL,'//')+2));
        $expected = false;
        $this->assertEquals($expected, $ret);
    }
    
    /**
     * Url::parse : unsupported protocol test
     *
     * Expected return value :
     * bool false
     */
    public function testUnsupportedProtocol()
    {
        $urlObj = new Url();
        $ret = $urlObj->parse('ftp://'.substr(URL,strpos(URL,'//')+2));
        $expected = false;
        $this->assertEquals($expected, $ret);
    }
    
    /**
     * Url::parse : url with subdomain
     *
     * Expected return value :
     * bool false
     */
    public function testUrlWithSubdomain()
    {
        $urlObj = new Url();
        $ret = $urlObj->parse('http:/subdomain.'.substr(URL,strpos(URL,'//')+2));
        $expected = false;
        $this->assertEquals($expected, $ret);
    }

    /**
     * Url::parse : Checks if Url class keeps trailing slashes in Urls
     * Expected return value :
     *
     * array[
     *      'route' => 'en/admin/controller/command/parameter/',
     *      'path' => 'controller/command/parameter',
     *      'language' => 'en',
     *      'admin' => true,
     *      'ext' => ''
     * ]
     */
    public function testWithTrailingSlash()
    {
        //With trailing shash
        $url = new Url();
        $ret = $url->parse(URL.'en/admin/controller/command/parameter/');
        $expected = array(
            'route' => 'en/admin/controller/command/parameter',
            'path' => 'controller/command/parameter',
            'language' => 'en',
            'admin' => true,
            'ext' => ''
        );
        $this->assertEquals($expected, $ret);
    }

    /**
     * Url::parse : Checks if Url class keeps trailing slashes in Urls
     * Expected return value :
     *
     * array[
     *      'route' => 'en/admin/controller/command/parameter/',
     *      'path' => 'controller/command/parameter',
     *      'language' => 'en',
     *      'admin' => true,
     *      'ext' => ''
     * ]
     */
    public function testWithoutTrailingSlash()
    {
        //With trailing shash
        $url = new Url();
        $ret = $url->parse(URL.'en/admin/controller/command/parameter');
        $expected = array(
            'route' => 'en/admin/controller/command/parameter',
            'path' => 'controller/command/parameter',
            'language' => 'en',
            'admin' => true,
            'ext' => ''
        );
        $this->assertEquals($expected, $ret);
    }
    
    /**
     * Url::parse : parse http protocol url
     * Expected return value :
     *
     * array[
     *      'route' => 'en/admin/controller/command/parameter/',
     *      'path' => 'controller/command/parameter',
     *      'language' => 'en',
     *      'admin' => true,
     *      'ext' => ''
     * ]
     */
    public function testHttpProtocol()
    {
        //With trailing shash
        $url = new Url();
        $ret = $url->parse('http://'.substr(URL,strpos(URL,'//')+2).'en/admin/controller/command/parameter');
        $expected = array(
            'route' => 'en/admin/controller/command/parameter',
            'path' => 'controller/command/parameter',
            'language' => 'en',
            'admin' => true,
            'ext' => ''
        );
        $this->assertEquals($expected, $ret);
    }
    
    /**
     * Url::parse : parse https protocol url
     * Expected return value :
     *
     * array[
     *      'route' => 'en/admin/controller/command/parameter/',
     *      'path' => 'controller/command/parameter',
     *      'language' => 'en',
     *      'admin' => true,
     *      'ext' => ''
     * ]
     */
    public function testHttpsProtocol()
    {
        //With trailing shash
        $url = new Url();
        $ret = $url->parse('https://'.substr(URL,strpos(URL,'//')+2).'en/admin/controller/command/parameter');
        $expected = array(
            'route' => 'en/admin/controller/command/parameter',
            'path' => 'controller/command/parameter',
            'language' => 'en',
            'admin' => true,
            'ext' => ''
        );
        $this->assertEquals($expected, $ret);
    }
    
    /**
     * Url::parse : parse // protocol url
     * Expected return value :
     *
     * array[
     *      'route' => 'en/admin/controller/command/parameter/',
     *      'path' => 'controller/command/parameter',
     *      'language' => 'en',
     *      'admin' => true,
     *      'ext' => ''
     * ]
     */
    public function testUnspecifiedProtocol()
    {
        //With trailing shash
        $url = new Url();
        $ret = $url->parse('//'.substr(URL,strpos(URL,'//')+2).'en/admin/controller/command/parameter');
        $expected = array(
            'route' => 'en/admin/controller/command/parameter',
            'path' => 'controller/command/parameter',
            'language' => 'en',
            'admin' => true,
            'ext' => ''
        );
        $this->assertEquals($expected, $ret);
    }

    /**
     * Url::parse : directory traversal test
     * Expected return value :
     *
     * array[
     *      'route' => 'en/admin/controller/command/parameter/',
     *      'path' => 'controller/command/parameter',
     *      'language' => 'en',
     *      'admin' => true,
     *      'ext' => ''
     * ]
     */
    public function testDirectoryTraversal()
    {
        //With trailing shash
        $url = new Url();
        $ret = $url->parse(URL.'../../../../../en/admin/controller/command/parameter');
        $expected = array(
            'route' => 'en/admin/controller/command/parameter',
            'path' => 'controller/command/parameter',
            'language' => 'en',
            'admin' => true,
            'ext' => ''
        );
        $this->assertEquals($expected, $ret);
    }

    /**
     * Url::parse : speed test
     * Expected return value :
     *
     * array[
     *      'route' => 'en/admin',
     *      'path' => '',
     *      'language' => 'en'
     *      'admin' => true,
     *      'ext' => ''
     * ]
     */    
    public function testPerformance(){
        $start = microtime();
        $url = new Url();
        $ret = $url->parse(URL.'en/admin');
        $expexted = array(
            'route' => 'en/admin',
            'path' => '',
            'language' => 'en',
            'admin' => true,
            'ext' => ''
        );
        $end = microtime();
        $delay = $end - $start;
        if ($delay < 2) $ret = true;
        else $ret = false;
        $this->assertEquals(true, $ret);
    }
}
?>